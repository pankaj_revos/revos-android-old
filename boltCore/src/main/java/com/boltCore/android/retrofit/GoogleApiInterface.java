package com.boltCore.android.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleApiInterface {

    @GET("geocode/json")
    Call<ResponseBody> reverseGeoCode(@Query("address") String address, @Query("key") String googleMapApiKey);
}
