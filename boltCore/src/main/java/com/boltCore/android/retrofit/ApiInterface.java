package com.boltCore.android.retrofit;

import org.joda.time.DateTime;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("user/register")
    Call<ResponseBody> registerUser(@Header("token") String token,
                                        @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("user/login")
    Call<ResponseBody> loginUser(@Header("token") String token,
                                      @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @GET("charger")
    Call<ResponseBody> getAvailableChargers(@Query("lat_top") double latTop,
                                            @Query("long_left") double longLeft,
                                            @Query("lat_bottom") double latBottom,
                                            @Query("long_right") double longRight,
                                            @Query("package") String packageName,
                                            @Header("token") String token,
                                            @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("charger/comingsoon")
    Call<ResponseBody> getUpcomingChargers(@Query("lat_top") double latTop,
                                            @Query("long_left") double longLeft,
                                            @Query("lat_bottom") double latBottom,
                                            @Query("long_right") double longRight,
                                            @Header("token") String token,
                                            @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("charger/{UID}")
    Call<ResponseBody> getChargerById(@Path("UID") String UID,
                                      @Header("token") String token,
                                      @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("charger/{UID}/syncinfo")
    Call<ResponseBody> fetchSyncInfoObjectForCharger(@Path("UID") String UID,
                                      @Header("token") String token,
                                      @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("user/bookings")
    Call<ResponseBody> getUserBookings(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("user/bookings")
    Call<ResponseBody> getUserBookingHistory(@Header("token") String token, @Header("Authorization") String bearerToken,@Query("stage") String stage,@Query(value = "endTime",encoded = true)  DateTime endTime,@Query(value = "startTime",encoded = true) DateTime startTime);


    @Headers("Content-Type: application/json")
    @GET("user/orders")
    Call<ResponseBody> getPastOrders(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @POST("charger/order")
    Call<ResponseBody> placeChargerOrder(@Header("token") String token,
                                   @Header("Authorization") String bearerToken,
                                   @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("charger/{UID}/book")
    Call<ResponseBody> bookCharger(@Path("UID") String UID,
                                   @Header("token") String token,
                                   @Header("Authorization") String bearerToken,
                                   @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("charger/{bookingId}/end")
    Call<ResponseBody> endBooking(@Path("bookingId") String bookingId,
                                   @Header("token") String token,
                                   @Header("Authorization") String bearerToken,
                                   @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("charger/{bookingId}/update")
    Call<ResponseBody> updateBooking(@Path("bookingId") String bookingId,
                                  @Header("token") String token,
                                  @Header("Authorization") String bearerToken,
                                  @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @GET("charger/products")
    Call<ResponseBody> getChargerModels(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @POST("charger/register")
    Call<ResponseBody> registerCharger(@Header("token") String token,
                                   @Header("Authorization") String bearerToken,
                                   @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("charger/{UID}/edit")
    Call<ResponseBody> editCharger(@Path("UID") String UID,
                                    @Header("token") String token,
                                    @Header("Authorization") String bearerToken,
                                    @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @GET("user/chargers")
    Call<ResponseBody> getMyChargers(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("user/earnings")
    Call<ResponseBody> getEarnings(@Header("token") String token, @Header("Authorization") String bearerToken);


    @Headers("Content-Type: application/json")
    @GET("user/paymentmethod")
    Call<ResponseBody> getPaymentMethod(@Header("token") String token, @Header("Authorization") String bearerToken,
                                        @Query("stage") String stage);

    @Headers("Content-Type: application/json")
    @GET("user/coupons")
    Call<ResponseBody> getCoupons(@Header("token") String token, @Header("Authorization") String bearerToken);


    @Headers("Content-Type: application/json")
    @POST("user/paymentmethod")
    Call<ResponseBody> addPaymentMethod(@Header("token") String token,
                                       @Header("Authorization") String bearerToken,
                                       @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @DELETE("user/paymentmethod/{ID}")
    Call<ResponseBody> deletePaymentMethod(@Path("ID") String ID,
                                           @Header("token") String token,
                                           @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("charger/{chargerId}/ota/{versionNo}")
    Call<ResponseBody> downloadOtaFileBuffer(@Path("chargerId") String chargerId,
                                             @Path("versionNo") String versionNo,
                                             @Header("token") String token,
                                             @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @POST("charger/{chargerId}/ota")
    Call<ResponseBody> updateDeviceOtaStatus(@Path("chargerId") String chargerId,
                                             @Header("token") String token,
                                             @Header("Authorization") String bearerToken,
                                             @Body RequestBody body);

    @Headers("Content-Type: application/json")
    @POST("charger/{chargerId}/provider")
    Call<ResponseBody> updateChargerProvider(@Path("chargerId") String chargerId,
                                             @Header("token") String token,
                                             @Header("Authorization") String bearerToken,
                                             @Body RequestBody body);


    @Headers("Content-Type: application/json")
    @POST("charger/{bleName}/ping")
    Call<ResponseBody> pingCharger(@Path("bleName") String bleName,
                                   @Header("token") String token,
                                   @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("user/subscriptions")
    Call<ResponseBody> getActiveSubscriptions(@Header("token") String token,
                                              @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @POST("subscriptions/adduser")
    Call<ResponseBody> addSubscriptionForUser(@Header("token") String token,
                                              @Header("Authorization") String bearerToken,
                                              @Body RequestBody requestBody);
}
