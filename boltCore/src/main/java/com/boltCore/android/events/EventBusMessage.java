package com.boltCore.android.events;

/**
 * Created by mohit on 16/3/17.
 */

public class EventBusMessage {

    public final String message;

    public EventBusMessage(String message) {
        this.message = message;
    }
}
