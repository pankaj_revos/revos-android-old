package com.boltCore.android.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.boltCore.android.R;
import com.boltCore.android.activities.BoltRegisterChargerActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.boltCore.android.activities.BoltRegisterChargerActivity.SETUP_BLE_STATUS_CONNECTED;
import static com.boltCore.android.activities.BoltRegisterChargerActivity.SETUP_BLE_STATUS_CONNECTING;
import static com.boltCore.android.activities.BoltRegisterChargerActivity.SETUP_BLE_STATUS_SEARCHING;

public class BoltFindAndConnectToChargerFragment extends BoltBaseFragment {
    private View mRootView;
    private TextView mTimerTextView, mStatusTextView, mHelpTextView;
    private Button mSetChargerLocationButton;
    private LottieAnimationView mLottieAnimationView;

    private int mLastKnownBleStatus;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.bolt_find_and_connect_to_charger_fragment, container, false);
        setupViews();
        initialiseAnalytics(getActivity());
        return mRootView;
    }

    private void setupViews() {
        mTimerTextView = mRootView.findViewById(R.id.bolt_find_and_connect_to_charger_timer_text_view);
        mStatusTextView = mRootView.findViewById(R.id.bolt_find_and_connect_to_charger_status_text_view);
        mSetChargerLocationButton = mRootView.findViewById(R.id.bolt_find_and_connect_to_charger_set_charger_location_button);
        mHelpTextView = mRootView.findViewById(R.id.bolt_find_and_connect_to_charger_help_text_view);
        mLottieAnimationView = mRootView.findViewById(R.id.bolt_find_and_connect_to_charger_lottie_view);

        mSetChargerLocationButton.setOnClickListener(v -> {
            if(getActivity() == null) {
                return;
            }
            ((BoltRegisterChargerActivity)getActivity()).loadSelectChargerLocationFragment();
        });

        startTimer();
    }

    private void startTimer() {

        new CountDownTimer(150000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds = (int)millisUntilFinished / 1000;
                int minutes = seconds / 60;

                mTimerTextView.setText(String.format("%02d", minutes) + ":" + String.format("%02d",seconds % 60));

                if(getActivity() == null) {
                    return;
                }

                int bleStatus = ((BoltRegisterChargerActivity)getActivity()).getBleStatus();
                if(bleStatus == SETUP_BLE_STATUS_SEARCHING) {
                    mStatusTextView.setText(getString(R.string.searching_for_charger));
                } else if(bleStatus == SETUP_BLE_STATUS_CONNECTING) {
                    mStatusTextView.setText(getString(R.string.connecting_to_charger));
                } else if(bleStatus == SETUP_BLE_STATUS_CONNECTED) {

                    mHelpTextView.setVisibility(View.INVISIBLE);
                    mTimerTextView.setVisibility(View.INVISIBLE);
                    mStatusTextView.setText(getString(R.string.connection_successful));
                    mSetChargerLocationButton.setVisibility(View.VISIBLE);

                    if(mLastKnownBleStatus != bleStatus) {
                        mLottieAnimationView.loop(false);
                        mLottieAnimationView.setAnimation(R.raw.ic_generic_message_success);
                        mLottieAnimationView.setProgress(0);
                        mLottieAnimationView.playAnimation();
                    }
                }

                mLastKnownBleStatus = bleStatus;
            }

            @Override
            public void onFinish() {
                if(getActivity() == null) {
                    return;
                }

                mTimerTextView.setVisibility(View.INVISIBLE);

                if(((BoltRegisterChargerActivity)getActivity()).getBleStatus() != SETUP_BLE_STATUS_CONNECTED) {
                    getActivity().finish();
                }

            }
        }.start();
    }
}
