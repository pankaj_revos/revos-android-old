package com.boltCore.android.fragments;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.boltCore.android.activities.BoltMainActivity;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.boltCore.android.R;
import com.boltCore.android.activities.BoltBookChargerActivity;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.events.EventBusMessage;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.boltCore.android.constants.Constants.BLE_EVENT_DEVICE_FOUND;
import static com.boltCore.android.constants.Constants.BLE_EVENT_SCAN_STOP;
import static com.boltCore.android.constants.Constants.BOLT_BOOK_ACTIVITY_LAUNCHED;
import static com.boltCore.android.constants.Constants.CHARGER_UID_KEY;
import static com.boltCore.android.constants.Constants.GEN_DELIMITER;
import static com.boltCore.android.constants.Constants.GEN_EVENT_TAB_CHANGE;
import static com.boltCore.android.constants.Constants.USAGE_PRIVATE;
import static com.boltCore.android.constants.Constants.UTILITY_PREFS;


public class BoltBookingScanQrCodeFragment extends BoltBaseFragment implements ZXingScannerView.ResultHandler {

    private View mRootView;

    private ZXingScannerView mScannerView;

    private LottieAnimationView mLottieAnimationView;

    private Button mOkayButton, mSubmitChargerIDButton, mConnectWithChargerIDButton;

    private TextView mMessageTextView, mOrSeparatorTextView;

    private LinearLayout mConnectWithChargerIDLinearLayout;

    private OtpView mChargerIdOtpView;

    private int mTabPosition;

    private boolean mIsFlashOn = false, mIsEnterChargerIDLayoutVisible = false;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.CAMERA};
    private int REQUEST_CAMERA_PERMISSION_REQ_CODE = 300;

    private final String AVAILABLE = "AVAILABLE";

    private Charger mCharger;

    private static boolean mDeviceFound = false;

    private HashSet<String> mOwnedChargerIdHashSet;

    private int CONNECT_USING_CHARGER_ID = 201;

    private String mTypedInChargerID = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.bolt_scan_qr_code_to_book_fragment, container, false);

        setupViews();

        initialiseAnalytics(getActivity());

        return mRootView;
    }

    private void setupViews() {
        mScannerView = mRootView.findViewById(R.id.bolt_scan_qr_code_to_book_scanner_view);

        mConnectWithChargerIDLinearLayout = mRootView.findViewById(R.id.bolt_scan_connect_via_charger_id_linear_layout);

        mSubmitChargerIDButton = mRootView.findViewById(R.id.bolt_submit_charger_id_button);
        mOkayButton = mRootView.findViewById(R.id.bolt_scan_qr_code_to_book_ok_button);
        mConnectWithChargerIDButton = mRootView.findViewById(R.id.bolt_scan_connect_via_charger_id_button);

        mChargerIdOtpView = mRootView.findViewById(R.id.bolt_charger_id_otp_view);

        mOrSeparatorTextView = mRootView.findViewById(R.id.bolt_scan_qr_code_or_text_view);
        mMessageTextView = mRootView.findViewById(R.id.bolt_scan_qr_code_to_book_message_text_view);

        mLottieAnimationView = mRootView.findViewById(R.id.bolt_scan_qr_code_to_book_lottie_view);


        ArrayList<BarcodeFormat> barcodeFormats = new ArrayList<>();
        barcodeFormats.add(BarcodeFormat.QR_CODE);

        // this paramter will make your HUAWEI phone works great!
        mScannerView.setAspectTolerance(0.5f);
        mScannerView.setFormats(barcodeFormats);
        mScannerView.setAutoFocus(true);

        mOkayButton.setOnClickListener(v -> {
            mOkayButton.setVisibility(View.GONE);

            mMessageTextView.setText(getString(R.string.scan_qr_code_on_poster));

            mOrSeparatorTextView.setVisibility(View.VISIBLE);

            if(!mIsEnterChargerIDLayoutVisible) {
                mConnectWithChargerIDButton.setVisibility(View.VISIBLE);
            } else {
                mConnectWithChargerIDLinearLayout.setVisibility(View.VISIBLE);

                if(mTypedInChargerID != null) {
                    mChargerIdOtpView.clearComposingText();
                }
            }

            mLottieAnimationView.setPadding(convertDipToPixels(0), convertDipToPixels(0), convertDipToPixels(0), convertDipToPixels(0));
            mLottieAnimationView.setAnimation(R.raw.ic_scan_qr);
            mLottieAnimationView.setProgress(0);
            mLottieAnimationView.playAnimation();

            checkPermissionAndStartScannerView();
        });


        mConnectWithChargerIDButton.setOnClickListener(v -> {

            mConnectWithChargerIDButton.setVisibility(View.GONE);

            mConnectWithChargerIDLinearLayout.setVisibility(View.VISIBLE);

            mIsEnterChargerIDLayoutVisible = true;
        });

        mSubmitChargerIDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mTypedInChargerID != null) {
                    fetchChargerDetails(mTypedInChargerID);
                } else {
                    showErrorMessage(getString(R.string.please_enter_charger_id));
                }
            }
        });

        mChargerIdOtpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {

                mSubmitChargerIDButton.setVisibility(View.VISIBLE);

                mTypedInChargerID = getString(R.string.bolt_charger_id_fixed_string) + otp;
            }
        });
    }

    private void checkPermissionAndStartScannerView() {
        if(getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }
                if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(mPermissionsRequired, REQUEST_CAMERA_PERMISSION_REQ_CODE);
                    return;
                }

                mScannerView.setResultHandler(BoltBookingScanQrCodeFragment.this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        //this method does not launch launch permission dialog unlike checkPermissionAndStartScannerView
        //calling checkPermissionAndStartScannerView here was leading to an infinite loop
        //this is used to start camera when we return to main activity
        startScannerViewIfPossible();

        mCharger = null;

        if(getActivity() == null) {
            return;
        }
        SharedPreferences.Editor edit = getActivity().getSharedPreferences(UTILITY_PREFS, MODE_PRIVATE).edit();
        edit.putBoolean(BOLT_BOOK_ACTIVITY_LAUNCHED, Boolean.FALSE).apply();
    }

    private void startScannerViewIfPossible() {
        if(getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mScannerView.setResultHandler(BoltBookingScanQrCodeFragment.this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Timber.v(rawResult.getText()); // Prints scan results
        Timber.v(rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        /*// If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);*/

        if(mTabPosition == 1) {
            Toast.makeText(getActivity(), getString(R.string.data_read_successfully), Toast.LENGTH_LONG).show();
            //uncomment this line in case of debugging, so that we can proceed to booking screen without checking if charger is on or not
            /*launchBookChargerActivity(rawResult.getText());*/
            fetchChargerDetails(rawResult.getText());
        } else {
            checkPermissionAndStartScannerView();
        }
    }

    private void fetchChargerDetails(String chargerUID) {
        mCharger = null;

        if(getActivity() == null) {
            return;
        }

        if(chargerUID == null || chargerUID.isEmpty()) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerById(chargerUID, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showErrorMessage(getString(R.string.server_error));
                    return;
                }

                try {
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonObject data = bodyJsonObject.get("data").getAsJsonObject();

                        String dataStr = data.toString();
                        mCharger = new Gson().fromJson(dataStr, Charger.class);

                        if(getActivity() == null) {
                            return;
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fetchMyChargers();
                            }
                        });
                    }

                } catch (Exception e) {
                    showErrorMessage(getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showErrorMessage(getString(R.string.server_error));
            }
        });
    }

    private void fetchMyChargers() {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getMyChargers(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonArray dataArray = bodyJsonObject.getAsJsonArray("data");
                        mOwnedChargerIdHashSet = new HashSet<>();

                        for(JsonElement jsonElement : dataArray) {
                            Charger charger = new Gson().fromJson(jsonElement.getAsJsonObject().toString(), Charger.class);
                            mOwnedChargerIdHashSet.add(charger.getUID());
                        }

                        if(!mCharger.getStatus().equals(AVAILABLE)) {
                            showErrorMessage(getString(R.string.charger_not_available));
                        } else {
                            if(getActivity() == null) {
                                return;
                            }

                            if(!mOwnedChargerIdHashSet.contains(mCharger.getUID()) && mCharger.getUsageType() != null && mCharger.getUsageType().equals(USAGE_PRIVATE)) {
                                showErrorMessage(getString(R.string.charger_marked_for_private_use));
                                return;
                            }

                            searchForCharger();
                        }

                    }

                } catch (Exception e) { }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                showErrorMessage(getString(R.string.server_error));
            }
        });
    }


    private void launchBookChargerActivity(Charger charger) {
        if(getActivity() == null) {
            return;
        }

        boolean previouslyStarted = getActivity().getSharedPreferences(UTILITY_PREFS, MODE_PRIVATE).getBoolean(BOLT_BOOK_ACTIVITY_LAUNCHED, false);
        if(!previouslyStarted) {
            SharedPreferences.Editor edit = getActivity().getSharedPreferences(UTILITY_PREFS, MODE_PRIVATE).edit();
            edit.putBoolean(BOLT_BOOK_ACTIVITY_LAUNCHED, Boolean.TRUE).apply();

            Intent intent = new Intent(getActivity(), BoltBookChargerActivity.class);
            intent.putExtra(CHARGER_UID_KEY, charger.getUID());
            startActivity(intent);
        }
    }

    private void toggleFlash(boolean on) {
        mScannerView.setFlash(on);
    }

    private void updateCameraFlashImageView() {
        if(getActivity() == null) {
            return;
        }

        mIsFlashOn = mScannerView.getFlash();
        int backGroundId = R.drawable.turn_flash_on_background;
        if(mIsFlashOn) {
            backGroundId = R.drawable.turn_flash_off_background;
        }

        mRootView.findViewById(R.id.bolt_scan_qr_code_to_book_toggle_flash_image_view).setBackground(ContextCompat.getDrawable(getActivity(), backGroundId));

        mRootView.findViewById(R.id.bolt_scan_qr_code_to_book_toggle_flash_image_view).setOnClickListener(v -> {
            toggleFlash(!mIsFlashOn);
            updateCameraFlashImageView();
        });
    }

    private void searchForCharger() {
        if(getActivity() == null) {
            return;
        }

        hideProgressDialog();

        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {
            mOkayButton.setVisibility(View.VISIBLE);

            mOrSeparatorTextView.setVisibility(View.GONE);
            if(mIsEnterChargerIDLayoutVisible) {
                mConnectWithChargerIDLinearLayout.setVisibility(View.GONE);
            } else {
                mConnectWithChargerIDButton.setVisibility(View.GONE);
            }

            mMessageTextView.setText(getString(R.string.please_switch_on_bluetooth));

            mLottieAnimationView.setPadding(convertDipToPixels(20), convertDipToPixels(20), convertDipToPixels(20), convertDipToPixels(20));
            mLottieAnimationView.setAnimation(R.raw.ic_generic_message_information);
            mLottieAnimationView.setProgress(0);
            mLottieAnimationView.playAnimation();

            return;
        }

        try {
            startScanHelper();
        } catch (Exception e) {
            showErrorMessage(getString(R.string.unable_to_locate_charger));
        }
    }

    private void startScanHelper() {
        if(getActivity() == null) {
            return;
        }

        showProgressDialog();

        mDeviceFound = false;

        ((BoltMainActivity)getActivity()).startScan();
    }

    private void showProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((BoltMainActivity)getActivity()).showProgressDialog();
    }

    private void hideProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((BoltMainActivity)getActivity()).hideProgressDialog();
    }

    private void showErrorMessage(String message) {

        if(getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                mOkayButton.setVisibility(View.VISIBLE);

                mOrSeparatorTextView.setVisibility(View.GONE);

                if(mIsEnterChargerIDLayoutVisible) {
                    mConnectWithChargerIDLinearLayout.setVisibility(View.GONE);
                } else {
                    mConnectWithChargerIDButton.setVisibility(View.GONE);
                }

                mMessageTextView.setText(message);

                mLottieAnimationView.setPadding(convertDipToPixels(20), convertDipToPixels(20), convertDipToPixels(20), convertDipToPixels(20));
                mLottieAnimationView.setAnimation(R.raw.ic_generic_message_error);
                mLottieAnimationView.setProgress(0);
                mLottieAnimationView.playAnimation();
            }
        });

    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_CAMERA_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            checkPermissionAndStartScannerView();
        }

    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if (event.message.contains(GEN_EVENT_TAB_CHANGE)) {
            mTabPosition = Integer.parseInt(event.message.split(GEN_DELIMITER)[1]);

            if(mTabPosition == 1) {
                updateCameraFlashImageView();
                checkPermissionAndStartScannerView();
            } else {
                toggleFlash(false);
            }
        } else if(event.message.contains(BLE_EVENT_DEVICE_FOUND)) {
            if(mCharger == null) {
                return;
            }

            String deviceAddress = event.message.split(GEN_DELIMITER)[1];
            if(deviceAddress != null && deviceAddress.trim().toUpperCase().equals(mCharger.getBleMac().toUpperCase())) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceFound = true;
                        hideProgressDialog();
                        launchBookChargerActivity(mCharger);
                    }
                });
            }
        } else if(event.message.equals(BLE_EVENT_SCAN_STOP)) {
            if(!mDeviceFound && mCharger != null) {
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(getActivity() == null) {
                            return;
                        }

                        if(!((BoltMainActivity)getActivity()).isBleScanInProgress()) {
                            showErrorMessage(getString(R.string.unable_to_locate_charger));
                        }
                    }
                }, 2000);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
