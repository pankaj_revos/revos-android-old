package com.boltCore.android.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boltCore.android.activities.BoltPaymentAccountDetailsActivity;
import com.boltCore.android.activities.BoltPaymentAccountListActivity;
import com.boltCore.android.activities.GenericMessageActivity;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.PaymentAccount;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.boltCore.android.R;
import com.boltCore.android.activities.BoltRegisterChargerActivity;
import com.boltCore.android.jsonStructures.Product;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static com.boltCore.android.activities.BoltPaymentAccountListActivity.MODE_KEY;
import static com.boltCore.android.activities.BoltPaymentAccountListActivity.SELECT_MODE;
import static com.boltCore.android.constants.Constants.USAGE_PRIVATE;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_FREE;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_PAID;

public class BoltEnterChargerDetailsFragment extends BoltBaseFragment implements OnMapReadyCallback {
    private View mRootView;

    private MapView mMapView;

    private GoogleMap mGoogleMap = null;

    private ImageView mEditLocationImageView;

    private Product mSelectedProduct;

    private RelativeLayout m16ampRelativeLayout;

    private ArrayList<Product> mProductList;

    private PaymentAccount mSelectedPaymentAccount;

    private String mUsageType = USAGE_PUBLIC_FREE;

    private RadioGroup mUsageTypeRadioGroup;

    private final int SELECT_PAYMENT_ACC_REQ_CODE = 1001;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.bolt_enter_charger_details_fragment, container, false);

        setupViews();

        setupMapViews(savedInstanceState);
        initialiseAnalytics(getActivity());
        return mRootView;
    }

    private void setupViews() {
        mEditLocationImageView = mRootView.findViewById(R.id.bolt_ecd_edit_location_image_view);
        mEditLocationImageView.setOnClickListener(v -> {
            if(getActivity() == null) {
                return;
            }

            ((BoltRegisterChargerActivity)getActivity()).loadSelectChargerLocationFragment();
        });


        m16ampRelativeLayout = mRootView.findViewById(R.id.bolt_ecd_515amp_layout);

        if(getActivity() == null) {
            return;
        }
        mProductList = ((BoltRegisterChargerActivity)getActivity()).mProductList;

        if(mProductList.isEmpty()) {
            getActivity().finish();
        }

        //select 16amp by default
        mSelectedProduct = mProductList.get(0);

        select16AmpLayout();

        ((TextView)mRootView.findViewById(R.id.bolt_ecd_515amp_name_text_view)).setText(mProductList.get(0).getName());
        ((TextView)mRootView.findViewById(R.id.bolt_ecd_515amp_power_text_view)).setText(mProductList.get(0).getConnectorType() + ", " + mProductList.get(0).getPowerRating());

        mRootView.findViewById(R.id.bolt_ecd_select_payment_account_button).setOnClickListener(v -> startPaymentAccountListActivityForResult());

        mUsageTypeRadioGroup = mRootView.findViewById(R.id.bolt_ecd_usage_type_radio_group);

        mUsageTypeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if(checkedId != R.id.bolt_ecd_public_paid_radio_button) {
                mRootView.findViewById(R.id.bolt_ecd_pricing_info_linear_layout).setVisibility(GONE);
            } else {
                mRootView.findViewById(R.id.bolt_ecd_pricing_info_linear_layout).setVisibility(View.VISIBLE);
            }

            if(checkedId == R.id.bolt_ecd_private_radio_button) {
                ((TextView)mRootView.findViewById(R.id.bolt_ecd_contact_header_text_view)).setText(getString(R.string.owner_phone));
                ((TextView)mRootView.findViewById(R.id.bolt_ecd_contact_edit_text)).setHint(getString(R.string.owner_phone));
            } else {
                ((TextView)mRootView.findViewById(R.id.bolt_ecd_contact_header_text_view)).setText(getString(R.string.customer_support_number));
                ((TextView)mRootView.findViewById(R.id.bolt_ecd_contact_edit_text)).setHint(getString(R.string.customer_support_number));
            }
        });

    }

    private void startPaymentAccountListActivityForResult() {
        Intent intent = new Intent(getActivity(), BoltPaymentAccountListActivity.class);
        intent.putExtra(MODE_KEY, SELECT_MODE);
        startActivityForResult(intent, SELECT_PAYMENT_ACC_REQ_CODE);
    }

    public void registerCharger() {
        if(getActivity() == null) {
            return;
        }

        String electricityCostPerUnitStr = ((EditText)mRootView.findViewById(R.id.bolt_ecd_cost_per_unit_electricity_edit_text)).getText().toString();
        String stationNameStr = ((EditText)mRootView.findViewById(R.id.bolt_ecd_station_name_edit_text)).getText().toString().trim();
        String addressStr = ((EditText)mRootView.findViewById(R.id.bolt_ecd_station_address_edit_text)).getText().toString().trim();
        String contactStr = ((EditText)mRootView.findViewById(R.id.bolt_ecd_contact_edit_text)).getText().toString().trim();
        String contactNameStr = ((EditText)mRootView.findViewById(R.id.bolt_ecd_contact_name_edit_text)).getText().toString().trim();
        String baseAmountStr = ((EditText) mRootView.findViewById(R.id.bolt_ecd_base_charge_edit_text)).getText().toString().trim();
        String costPerHourStr = ((EditText) mRootView.findViewById(R.id.bolt_ecd_charge_per_hour_edit_text)).getText().toString().trim();

        if(electricityCostPerUnitStr.trim().isEmpty()) {
            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_electricity_cost_per_unit));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);
            return;
        }

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("costPerUnitElectricity", electricityCostPerUnitStr);

        bodyJsonObject.addProperty("stationName", stationNameStr);
        bodyJsonObject.addProperty("address", addressStr);
        bodyJsonObject.addProperty("contact", contactStr);
        bodyJsonObject.addProperty("contactName", contactNameStr);

        bodyJsonObject.addProperty("product" , mSelectedProduct.getId());

        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_ecd_public_paid_radio_button || mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_ecd_public_free_radio_button) {
            String errorMessage = "";
            if(stationNameStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_station_name);
            }

            if(addressStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_station_address);
            }

            if(contactStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_owner_phone_number);
            }

            if(contactNameStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_owner_name);
            }

            if(!errorMessage.isEmpty()) {
                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, errorMessage);
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                return;
            }
        }

        bodyJsonObject.addProperty("stationName", stationNameStr);
        bodyJsonObject.addProperty("address", addressStr);
        bodyJsonObject.addProperty("contact", contactStr);
        bodyJsonObject.addProperty("contactName", contactNameStr);


        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_ecd_public_paid_radio_button) {

            if(baseAmountStr.isEmpty() || costPerHourStr.isEmpty()) {
                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_base_amount_and_cost_per_hour));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                return;
            }

            bodyJsonObject.addProperty("baseAmount", baseAmountStr);
            bodyJsonObject.addProperty("costPerHour", costPerHourStr);

            if(mSelectedPaymentAccount != null) {
                bodyJsonObject.addProperty("paymentMethod", mSelectedPaymentAccount.getId());
            } else {
                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_select_payment_account));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                return;
            }
        }

        String usageType = USAGE_PUBLIC_PAID;

        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_ecd_public_free_radio_button) {
            usageType = USAGE_PUBLIC_FREE;
        } else if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_ecd_private_radio_button) {
            usageType = USAGE_PRIVATE;
        }

        bodyJsonObject.addProperty("usageType" , usageType);


        ((BoltRegisterChargerActivity)getActivity()).makeRegisterChargerCall(bodyJsonObject);
    }


    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) mRootView.findViewById(R.id.bolt_ecd_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    private void select16AmpLayout() {
        if(getActivity() == null) {
            return;
        }
        m16ampRelativeLayout.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bolt_text_field_selected_background));
        mSelectedProduct = mProductList.get(0);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();

        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);

        placeChargerMarker();
    }

    private void placeChargerMarker() {
        if(mGoogleMap == null || getActivity() == null) {
            return;
        }

        LatLng chargerLatLng = ((BoltRegisterChargerActivity)getActivity()).mChargerLatLng;

        Bitmap chargerBitmap = resizeMapIcons(R.drawable.ic_charging_png_green, convertDipToPixels(32), convertDipToPixels(32));

        mGoogleMap.clear();

        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                .position(chargerLatLng)
                .anchor(0.5f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(chargerBitmap)));

        //zoom to the marker
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(marker.getPosition())
                .zoom(18)
                .build();

        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_PAYMENT_ACC_REQ_CODE) {
            if(resultCode == RESULT_OK) {
                if(data == null || getActivity() == null) {
                    return;
                }

                String jsonStr = data.getStringExtra(BoltPaymentAccountDetailsActivity.PAYMENT_ACC_JSON_KEY);
                mSelectedPaymentAccount = new Gson().fromJson(jsonStr, PaymentAccount.class);

                ((TextView)mRootView.findViewById(R.id.bolt_ecd_account_name_text_view)).setText(mSelectedPaymentAccount.getName());

            }
        }
    }
}
