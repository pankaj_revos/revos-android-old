package com.boltCore.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.joda.time.DateTime;

public class BoltBaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void initialiseAnalytics(Context mContext) {
        Bundle mData = new Bundle();
        mData.putString(FirebaseAnalytics.Param.SCREEN_NAME, String.valueOf(mContext));
        FirebaseAnalytics.getInstance(mContext).logEvent(FirebaseAnalytics.Event.SCREEN_VIEW,mData);
    }
}
