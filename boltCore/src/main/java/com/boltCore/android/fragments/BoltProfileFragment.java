package com.boltCore.android.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.boltCore.android.R;
import com.boltCore.android.activities.BoltActiveBookingListActivity;
import com.boltCore.android.activities.BoltChargerControlActivity;
import com.boltCore.android.activities.BoltEarningsActivity;
import com.boltCore.android.activities.BoltMyChargerActivity;
import com.boltCore.android.activities.BoltBookingHistoryActivity;
import com.boltCore.android.activities.BoltPaymentAccountListActivity;
import com.boltCore.android.activities.BoltSubscriptionsActivity;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.jsonStructures.User;
import com.boltCore.android.sdk.BoltSDK;
import com.boltCore.android.utilities.general.PrefUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;
import static com.boltCore.android.activities.BoltBookChargerActivity.BOOKING_OBJECT_ARG_KEY;


public class BoltProfileFragment extends BoltBaseFragment {
    private View mRootView;
    private Timer mOneSecondTimer;
    private String mBuyUrl;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.bolt_profile_fragment, container, false);
        initialiseAnalytics(getActivity());

        mBuyUrl = BoltSDK.getInstance().getBuyUrl();

        setupViews();

        setupTimer();
        return mRootView;
    }

    private void setupTimer() {
        mOneSecondTimer = new Timer();

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }

                if(!getActivity().hasWindowFocus()) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toggleActiveBookingLayout();
                    }
                });
            }
        }, 0, 1000);
    }

    private void toggleActiveBookingLayout() {
        if(getActivity() == null) {
            return;
        }

        String activeBookingsStr = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.ACTIVE_BOOKINGS_KEY, null);

        int bookingActiveBasedOnTime = 0;

        int visibility = View.GONE;

        try {

            if (activeBookingsStr == null) {
                mRootView.findViewById(R.id.bolt_profile_active_booking_card_view).setVisibility(View.GONE);
            } else {
                ArrayList<Booking> activeBookings = new Gson().fromJson(activeBookingsStr, new TypeToken<ArrayList<Booking>>() {
                }.getType());

                for (Booking booking : activeBookings) {
                    DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(booking.getEndTime());
                    DateTime currentTime = new DateTime();

                    if (currentTime.isBefore(bookingEndTime.toInstant())) {
                        ++bookingActiveBasedOnTime;
                    }
                }

                if (bookingActiveBasedOnTime > 0) {
                    visibility = View.VISIBLE;
                } else {
                    visibility = View.GONE;
                }
            }


            mRootView.findViewById(R.id.bolt_profile_active_booking_card_view).setVisibility(visibility);
        } catch (Exception e) {
            mRootView.findViewById(R.id.bolt_profile_active_booking_card_view).setVisibility(View.GONE);
        }
    }

    private void setupViews() {

        mRootView.findViewById(R.id.bolt_profile_active_booking_relative_layout).setOnClickListener(v -> {
            launchActivityBasedOnNumberOfActiveBookings();
        });

        mRootView.findViewById(R.id.bolt_profile_booking_history_relative_layout).setOnClickListener(v -> {
            launchBookingHistoryActivity();
        });

        mRootView.findViewById(R.id.bolt_profile_my_chargers_relative_layout).setOnClickListener(v -> {
            launchMyChargerActivity();
        });

        mRootView.findViewById(R.id.bolt_profile_earnings_relative_layout).setOnClickListener(v -> {
            launchEarningsActivity();
        });

        mRootView.findViewById(R.id.bolt_profile_buy_charger_relative_layout).setOnClickListener(v -> {
            launchBuyChargerActivity();
        });

        mRootView.findViewById(R.id.bolt_profile_my_subscriptions_relative_layout).setOnClickListener(v -> {
            launchMySubscriptionsActivity();
        });

        if(mBuyUrl == null || mBuyUrl.isEmpty()) {
            mRootView.findViewById(R.id.bolt_profile_buy_charger_relative_layout).setVisibility(View.GONE);
        }

        mRootView.findViewById(R.id.bolt_profile_payment_account_relative_layout).setOnClickListener(v -> {
            launchPaymentAccountActivity();
        });

        User user = PrefUtils.getInstance(getActivity()).getUserObjectFromPrefs();

        if(user == null) {
            return;
        }

        if(user.getEmail() != null && !user.getEmail().isEmpty()) {
            ((TextView)mRootView.findViewById(R.id.bolt_profile_email_text_view)).setText(user.getEmail());
        }

        if(user.getPhone() != null && !user.getPhone().isEmpty()) {
            ((TextView)mRootView.findViewById(R.id.bolt_profile_phone_text_view)).setText(user.getPhone());
        }

        String nameStr = "";
        if(user.getFirstName() != null && !user.getFirstName().isEmpty()) {
            nameStr = nameStr + user.getFirstName();
        }

        if(user.getLastName() != null && !user.getLastName().isEmpty()) {
            nameStr = nameStr + " " + user.getLastName();
        }

        ((TextView)mRootView.findViewById(R.id.bolt_profile_name_text_view)).setText(nameStr);


        ((TextView)mRootView.findViewById(R.id.bolt_profile_version_number_text_view)).setText(getString(R.string.module_version_name));

    }

    private void launchActivityBasedOnNumberOfActiveBookings() {
        String activeBookingsStr = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.ACTIVE_BOOKINGS_KEY, null);


        if(activeBookingsStr == null) {
            return;
        } else {
            ArrayList<Booking> activeBookings = new Gson().fromJson(activeBookingsStr, new TypeToken<ArrayList<Booking>>(){}.getType());

            ArrayList<Booking> bookingActiveBasedOnTimeList = new ArrayList<>();
            for(Booking booking : activeBookings) {
                DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(booking.getEndTime());
                DateTime currentTime = new DateTime();

                if (currentTime.isBefore(bookingEndTime.toInstant())) {
                    bookingActiveBasedOnTimeList.add(booking);
                }
            }

            if(bookingActiveBasedOnTimeList.size() == 1) {
                launchBoltChargerControlActivity(new Gson().toJson(bookingActiveBasedOnTimeList.get(0)));
            } else if(bookingActiveBasedOnTimeList.size() > 1) {
                launchActiveBookingActivity();
            }
        }
    }

    private void launchBoltChargerControlActivity(String bookingObjectArgKey) {
        Intent intent = new Intent(getActivity(), BoltChargerControlActivity.class);
        intent.putExtra(BOOKING_OBJECT_ARG_KEY, bookingObjectArgKey);
        startActivity(intent);
    }

    private void launchActiveBookingActivity() {
        Intent intent = new Intent(getActivity(), BoltActiveBookingListActivity.class);
        startActivity(intent);
    }

    private void launchBookingHistoryActivity() {
        Intent intent = new Intent(getActivity(), BoltBookingHistoryActivity.class);
        startActivity(intent);
    }

    private void launchMyChargerActivity() {
        Intent intent = new Intent(getActivity(), BoltMyChargerActivity.class);
        startActivity(intent);
    }

    private void launchEarningsActivity() {
        Intent intent = new Intent(getActivity(), BoltEarningsActivity.class);
        startActivity(intent);
    }

    private void launchBuyChargerActivity() {
        /*Intent intent = new Intent(getActivity(), BoltBuyChargerActivity.class);
        startActivity(intent);*/

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(mBuyUrl));
        startActivity(i);
    }

    private void launchPaymentAccountActivity() {
        Intent intent = new Intent(getActivity(), BoltPaymentAccountListActivity.class);
        startActivity(intent);
    }

    private void launchMySubscriptionsActivity() {
        Intent intent = new Intent(getActivity(), BoltSubscriptionsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }
        super.onDestroy();
    }
}
