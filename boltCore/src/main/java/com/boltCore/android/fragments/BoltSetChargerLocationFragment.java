package com.boltCore.android.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boltCore.android.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.boltCore.android.activities.BoltRegisterChargerActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import timber.log.Timber;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class BoltSetChargerLocationFragment extends  BoltBaseFragment implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {
    private View mRootView;
    MapView mMapView;
    GoogleMap mGoogleMap = null;

    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Geocoder mGeoCoder;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private int REQUEST_PERMISSION_REQ_CODE = 200;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.bolt_set_charger_location_fragment, container, false);

        setupViews();

        setupMapViews(savedInstanceState);

        setupGeocoderAndLocationClient();
        initialiseAnalytics(getActivity());

        return mRootView;
    }

    private void setupViews() {
        mRootView.findViewById(R.id.bolt_set_charger_location_fragment_confirm_button).setOnClickListener(v -> {
            if(getActivity() == null) {
                return;
            }
            ((BoltRegisterChargerActivity)getActivity()).loadEnterChargerDetailsFragment();;
        });
    }

    private void setupGeocoderAndLocationClient() {
        if (getActivity() == null) {
            return;
        }

        mGeoCoder = new Geocoder(getActivity());
        mFusedLocationProviderClient = getFusedLocationProviderClient(getActivity());
    }

    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) mRootView.findViewById(R.id.bolt_set_charger_location_fragment_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();
        mGoogleMap.setOnCameraIdleListener(this);

        setupGoogleMaps();

        if (getActivity() == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
            return;
        }

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
            //center map around current location
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(16)
                    .build();

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }

    private void setupGoogleMaps() {

        if (getActivity() == null) {
            return;
        }

        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                return;
            }

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        }
    }

    @Override
    public void onCameraIdle() {
        Timber.d("Camera Idle");
        if(mGoogleMap == null) {
            return;
        }

        if(getActivity() == null) {
            return;
        }
        ((BoltRegisterChargerActivity)getActivity()).mChargerLatLng = mGoogleMap.getCameraPosition().target;
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
