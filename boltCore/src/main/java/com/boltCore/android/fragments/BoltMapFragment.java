package com.boltCore.android.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Order;
import com.boltCore.android.jsonStructures.Pricing;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.boltCore.android.activities.BoltMainActivity;
import com.boltCore.android.activities.GenericMessageActivity;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.retrofit.ApiClient;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.USAGE_PRIVATE;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_FREE;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class BoltMapFragment extends BoltBaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener {
    private View mRootView;
    MapView mMapView;
    GoogleMap mGoogleMap = null;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private HashMap<String, Charger> mDeployedChargerHashMap;
    private HashMap<String, Order> mUpcomingChargerHashMap;
    private Geocoder mGeoCoder;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private final int REQUEST_LOCATION_PERMISSION_REQ_CODE = 200;
    private LatLngBounds mCachedLatLngBounds;
    private CardView mDeployedChargerInfoCardView, mUpcomingChargerInfoCardView;
    private TextView mDeployedChargerNameTextView, mDeployedChargerAddressTextView, mDeployedChargerStatusTextView, mDeployedChargerLastPingTextView,
            mDeployedChargerPowerTextView, mDeployedChargerSocketTypeTextView, mDeployedChargerPricingTextView, mDeployedChargerIdNumberTextView;
    private TextView mUpcomingChargerPowerTextView, mUpcomingChargerSocketTypeTextView;
    private Button mNavigateButton;
    private ImageView mCloseDeployedChargerInfoCardImageView, mDeployedChargerSocketTypeImageView, mCloseUpcomingChargerInfoCardImageView, mUpcomingChargerSocketTypeImageView;
    private final String AVAILABLE = "AVAILABLE";
    private final String INUSE = "INUSE";
    private Bitmap mGreenMarkerBitmap, mRedMarkerBitmap, mYellowMarkerBitmap;
    private boolean mLocationZoomHappenedOnce = false;
    private Timer mThreeSecondTimer;
    private boolean mPermissionRequestInProgress = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.bolt_map_fragment, container, false);
        setupMarkerBitmap();
        setupDeployedChargerInfoCardViews();
        setupUpcomingChargerInfoCardViews();
        setupMapViews(savedInstanceState);
        setupGoogleMapsAndFetchCurrentLocationDetails();
        setupTimer();
        initialiseAnalytics(getActivity());
        return mRootView;
    }

    private void setupTimer() {
        mThreeSecondTimer = new Timer();

        mThreeSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }

                if(!getActivity().hasWindowFocus()) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!mLocationZoomHappenedOnce) {
                            setupGoogleMapsAndFetchCurrentLocationDetails();
                        }
                    }
                });
            }
        }, 1000, 3000);
    }

    private void setupMarkerBitmap() {
        mGreenMarkerBitmap = resizeMapIcons(R.drawable.ic_charging_png_green, convertDipToPixels(32), convertDipToPixels(32));
        mRedMarkerBitmap = resizeMapIcons(R.drawable.ic_charging_png_red, convertDipToPixels(32), convertDipToPixels(32));
        mYellowMarkerBitmap = resizeMapIcons(R.drawable.ic_charging_png_yellow, convertDipToPixels(32), convertDipToPixels(32));
    }

    private void setupUpcomingChargerInfoCardViews() {
        mUpcomingChargerPowerTextView = mRootView.findViewById(R.id.bolt_map_upcoming_charger_power_text_view);
        mUpcomingChargerSocketTypeTextView = mRootView.findViewById(R.id.bolt_map_upcoming_socket_type_text_view);


        mUpcomingChargerInfoCardView = mRootView.findViewById(R.id.bolt_map_upcoming_charger_card_view);
        mCloseUpcomingChargerInfoCardImageView = mRootView.findViewById(R.id.bolt_map_upcoming_close_image_view);
        mUpcomingChargerSocketTypeImageView = mRootView.findViewById(R.id.bolt_map_upcoming_socket_type_image_view);

        mCloseUpcomingChargerInfoCardImageView.setOnClickListener(v -> {
            mUpcomingChargerInfoCardView.setVisibility(View.INVISIBLE);

            if(mCachedLatLngBounds != null) {
                zoomMapToBounds(mCachedLatLngBounds);
            }
        });
    }

    private void setupDeployedChargerInfoCardViews() {
        mDeployedChargerNameTextView = mRootView.findViewById(R.id.bolt_map_charger_name_text_view);
        mDeployedChargerIdNumberTextView = mRootView.findViewById(R.id.bolt_map_charger_id_text_view);
        mDeployedChargerAddressTextView = mRootView.findViewById(R.id.bolt_map_charger_address_text_view);
        mDeployedChargerStatusTextView = mRootView.findViewById(R.id.bolt_map_charger_status_text_view);
        mDeployedChargerLastPingTextView = mRootView.findViewById(R.id.bolt_map_charger_last_ping_text_view);
        mDeployedChargerPowerTextView = mRootView.findViewById(R.id.bolt_map_charger_power_text_view);
        mDeployedChargerSocketTypeTextView = mRootView.findViewById(R.id.bolt_map_socket_type_text_view);
        mDeployedChargerPricingTextView = mRootView.findViewById(R.id.bolt_map_charger_pricing_text_view);

        mDeployedChargerInfoCardView = mRootView.findViewById(R.id.bolt_map_charger_card_view);
        mNavigateButton = mRootView.findViewById(R.id.bolt_map_navigate_button);
        mCloseDeployedChargerInfoCardImageView = mRootView.findViewById(R.id.bolt_map_close_image_view);
        mDeployedChargerSocketTypeImageView = mRootView.findViewById(R.id.bolt_map_socket_type_image_view);

        mCloseDeployedChargerInfoCardImageView.setOnClickListener(v -> {
            mDeployedChargerInfoCardView.setVisibility(View.INVISIBLE);

            if(mCachedLatLngBounds != null) {
                zoomMapToBounds(mCachedLatLngBounds);
            }
        });
    }

    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) mRootView.findViewById(R.id.bolt_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    private void setupGeocoderAndLocationClient() {
        if(getActivity() == null) {
            return;
        }

        mGeoCoder = new Geocoder(getActivity());
        mFusedLocationProviderClient = getFusedLocationProviderClient(getActivity());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();
        mGoogleMap.setOnMarkerClickListener(this);

        if(getActivity() == null) {
            return;
        }

        setupGoogleMapsAndFetchCurrentLocationDetails();

    }

    private void setupGoogleMapsAndFetchCurrentLocationDetails() {
        if(getActivity() == null || mPermissionRequestInProgress) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            mPermissionRequestInProgress = true;
            requestPermissions(mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }

        //check and ask user to switch on GPS
        if (!checkForGPS()) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_gps));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        setupGoogleMaps();

        setupGeocoderAndLocationClient();

        fetchAndCenterOnLastKnownLocation();
    }

    private boolean checkForGPS() {
        if(getActivity() == null) {
            return false;
        }

        //check if GPS is on
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if(locationManager == null) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.location_manager_null));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            getActivity().startActivity(intent);

            return false;
        }
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void setupGoogleMaps() {

        if (getActivity() == null) {
            return;
        }

        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
                return;
            }

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        }
    }

    private void fetchAndCenterOnLastKnownLocation() {

        if (getActivity() == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }

        if(!checkForGPS()) {
            return;
        }


        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
            if(location == null || mLocationZoomHappenedOnce) {
                hideProgressDialog();
                return;
            }

            //center map around current location
            CameraPosition cameraPosition = new CameraPosition.Builder()
                                                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                                                    .zoom(11)
                                                    .build();

            mLocationZoomHappenedOnce = true;

            Timber.d("setting camera idle listener");

            mGoogleMap.setOnCameraIdleListener(BoltMapFragment.this);

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }

    private void zoomMapToBounds(LatLngBounds latLngBounds) {
        if(getActivity() == null || mGoogleMap == null) {
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));

                //zoom by one level
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                                                                                .zoom(mGoogleMap.getCameraPosition().zoom + 1)
                                                                                                .target(mGoogleMap.getCameraPosition().target)
                                                                                                .build()));
            }
        });
    }


    private void fetchDeployedChargers(double latTop, double lngRight, double latBottom, double lngLeft) {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }
        //if we are fetching for the first time then we should show the progress dialog
        if(mDeployedChargerHashMap == null) {
            showProgressDialog();
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getAvailableChargers(latTop, lngLeft, latBottom, lngRight, BoltSDK.getInstance().getPackage(), BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();

                if(response.code() != 200 || response.body() == null) {
                    showServerError();
                    return;
                }

                try {

                    String responseBody = response.body().string();

                    JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                    if(responseJsonObject.has("data")) {
                        JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                        String dataStr = data.toString();
                        ArrayList<Charger> chargers = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Charger>>(){}.getType());

                        if(mDeployedChargerHashMap == null) {
                            mDeployedChargerHashMap = new HashMap<>();
                        }

                        for(Charger charger : chargers) {
                            if(charger.getStatus().equals(AVAILABLE) || charger.getStatus().equals(INUSE)) {
                                if(charger.getUsageType() == null || !charger.getUsageType().equals(USAGE_PRIVATE)) {
                                    mDeployedChargerHashMap.put(charger.getUID(), charger);
                                }
                            }
                        }

                        if(mDeployedChargerHashMap.isEmpty()) {
                            showNoChargerFound();
                        }

                        updateMarkers();


                    } else {
                        showServerError();
                    }

                } catch (Exception e) {
                    //do nothing here
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void fetchUpcomingChargers(double latTop, double lngRight, double latBottom, double lngLeft) {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Timber.d("fetching upcoming chargers");

        apiInterface.getUpcomingChargers(latTop, lngLeft, latBottom, lngRight, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Timber.d("fetching upcoming response");

                if(response.code() != 200 || response.body() == null) {
                    showServerError();
                    return;
                }

                try {

                    String responseBody = response.body().string();

                    JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                    if(responseJsonObject.has("data")) {
                        JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                        String dataStr = data.toString();
                        ArrayList<Order> orders = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Order>>(){}.getType());

                        if(mUpcomingChargerHashMap == null) {
                            mUpcomingChargerHashMap = new HashMap<>();
                        }

                        for(Order order : orders) {
                            mUpcomingChargerHashMap.put(order.getId(), order);
                        }

                        updateMarkers();

                    } else {
                        showServerError();
                    }

                } catch (Exception e) {
                    //do nothing here
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void updateUpcomingChargerMarkers() {

        if(mGoogleMap == null || mUpcomingChargerHashMap == null || mUpcomingChargerHashMap.isEmpty() || getActivity() == null) {
            return;
        }

        //no need to hide progress dialog based on the result of fetchUpcomingCharger call
        //hideProgressDialog();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (Map.Entry<String, Order> entry : mUpcomingChargerHashMap.entrySet()) {
                    Order order = entry.getValue();
                    Bitmap bitmap = mYellowMarkerBitmap;

                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(order.getOrderLocation().getLatitude(), order.getOrderLocation().getLongitude()))
                            .anchor(0.5f, 1)
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                            .zIndex(21));

                    marker.setTag(order);
                }
            }
        });

    }

    private void updateMarkers() {
        if(mGoogleMap != null) {
            mGoogleMap.clear();
        }
        updateUpcomingChargerMarkers();
        updateDeployedChargerMarkers();
    }

    private void updateDeployedChargerMarkers() {

        if(mGoogleMap == null || mDeployedChargerHashMap == null || mDeployedChargerHashMap.isEmpty() || getActivity() == null) {
            return;
        }

        hideProgressDialog();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (Map.Entry<String, Charger> entry : mDeployedChargerHashMap.entrySet()) {
                    Charger charger = entry.getValue();

                    Bitmap bitmap = mGreenMarkerBitmap;

                    if(!charger.getStatus().equals(AVAILABLE)) {
                        bitmap = mRedMarkerBitmap;
                    }

                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(charger.getLatitude(), charger.getLongitude()))
                                        .anchor(0.5f, 1)
                                        .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                                        .zIndex(42));

                    marker.setTag(charger);
                }
            }
        });

    }


    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }


    private void showProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((BoltMainActivity)getActivity()).showProgressDialog();
    }

    private void hideProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((BoltMainActivity)getActivity()).hideProgressDialog();
    }

    private void showServerError() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();

                    if(mDeployedChargerHashMap != null && !mDeployedChargerHashMap.isEmpty()) {
                        //no need to show server error if we have already loaded a few chargers
                        return;
                    }

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }
    }

    private void showNoChargerFound() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_chargers_found));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onCameraIdle() {
        Timber.d("Camera Idle");
        if(mGoogleMap == null) {
            Timber.d("google map null");
            return;
        }

        LatLngBounds cameraLatLngBounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

        if(getActivity() == null) {
            Timber.d("activity null");
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mCachedLatLngBounds == null) {
                    mCachedLatLngBounds = cameraLatLngBounds;
                }
                Timber.d("fetching chargers");
                fetchDeployedChargers(cameraLatLngBounds.northeast.latitude, cameraLatLngBounds.northeast.longitude, cameraLatLngBounds.southwest.latitude, cameraLatLngBounds.southwest.longitude);
                //fetchUpcomingChargers(cameraLatLngBounds.northeast.latitude, cameraLatLngBounds.northeast.longitude, cameraLatLngBounds.southwest.latitude, cameraLatLngBounds.southwest.longitude);
            }
        });

        if(mCachedLatLngBounds.contains(cameraLatLngBounds.northeast) && mCachedLatLngBounds.contains(cameraLatLngBounds.southwest)) {
            Timber.d("inside");
        } else {
            Timber.d("outside");
            mCachedLatLngBounds = cameraLatLngBounds;
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Object tagObject = marker.getTag();

        if(tagObject == null || getActivity() == null) {
            return false;
        }

        if(tagObject instanceof Charger) {
            updateDeployedChargerInfoViews((Charger)tagObject, marker);
        } else if(tagObject instanceof Order) {
            updateUpcomingChargerInfoViews((Order)tagObject, marker);
        }

        return false;
    }

    private void updateDeployedChargerInfoViews(Charger charger, Marker marker) {
        if(getActivity() == null) {
            return;
        }

        mDeployedChargerNameTextView.setText(charger.getStationName() == null ? "" : charger.getStationName().trim());

        if(charger.getUID() != null && !charger.getUID().isEmpty()) {
            mDeployedChargerIdNumberTextView.setText(charger.getUID());
        } else {
            mDeployedChargerIdNumberTextView.setText(getString(R.string.not_available));
        }

        if(charger.getAddress() != null && !charger.getAddress().isEmpty()) {
            mDeployedChargerAddressTextView.setVisibility(View.VISIBLE);
            mDeployedChargerAddressTextView.setText(charger.getAddress());
        } else {
            mDeployedChargerAddressTextView.setVisibility(View.GONE);
        }

        mDeployedChargerPowerTextView.setText(charger.getPowerRating());
        mDeployedChargerSocketTypeTextView.setText(charger.getCurrent());

        String costPerHour = "n/a";
        String basePrice = "n/a";

        if(charger.getPricing() != null && charger.getPricing().get(0) != null) {
            Pricing pricing = charger.getPricing().get(0);
            costPerHour = String.valueOf(pricing.getUnitCost());
            basePrice = String.valueOf(pricing.getBaseAmount());
        }

        if(charger.getUsageType() != null && charger.getUsageType().equals(USAGE_PUBLIC_FREE)) {
            mDeployedChargerPricingTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.revos_green));
            mDeployedChargerPricingTextView.setText(getString(R.string.free_to_use));
        } else {
            //as bolt app is in light theme, we use this color
            mDeployedChargerPricingTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_color_light));
            mDeployedChargerPricingTextView.setText(String.format(getString(R.string.pricing_details_dynamic), costPerHour, basePrice));
        }

        if(charger.getConnectorType().equals("5A/15A Socket")) {
            mDeployedChargerSocketTypeImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_5_15_amp));
        } else if(charger.getConnectorType().equals("5A Socket")) {
            mDeployedChargerSocketTypeImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_5_amp));
        } else if(charger.getConnectorType().equals("15A Socket")) {
            mDeployedChargerSocketTypeImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_15_amp));
        }


        String statusText = "";
        statusText = charger.getStatus();
        int statusColorId = R.color.revos_green;

        if(statusText.equals(AVAILABLE)) {
            statusText = getString(R.string.available);
        } else {
            statusText = getString(R.string.in_use);
            statusColorId = R.color.color_ic_battery_low_light;

            if(charger.getAvailableFrom() != null && !charger.getAvailableFrom().isEmpty()) {
                DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(charger.getAvailableFrom());
                DateTimeFormatter dtfOut = DateTimeFormat.forPattern("h:mma");
                String availableFromStr = dtfOut.print(dateTime);

                statusText = statusText + ", " + getString(R.string.available_from) + " : " + availableFromStr;
            }
        }

        mDeployedChargerStatusTextView.setText(statusText);
        mDeployedChargerStatusTextView.setTextColor(ContextCompat.getColor(getActivity(), statusColorId));

        if(charger.getLastPing() != null) {
            try {
                DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(charger.getLastPing());

                mDeployedChargerLastPingTextView.setText(getString(R.string.reported_up_and_running) + " " + new PrettyTime().format(dateTime.toDate()));
                mDeployedChargerLastPingTextView.setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }
        } else {
            mDeployedChargerLastPingTextView.setVisibility(View.INVISIBLE);
        }

        mNavigateButton.setOnClickListener(v -> {
            String url = "https://www.google.com/maps/dir/?api=1&destination=" + String.valueOf(marker.getPosition().latitude) + "," + String.valueOf(marker.getPosition().longitude);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,  Uri.parse(url));
            startActivity(intent);
        });

        //make upcoming charger card invisble
        mUpcomingChargerInfoCardView.setVisibility(View.INVISIBLE);

        //make the card visible
        mDeployedChargerInfoCardView.setVisibility(View.VISIBLE);

        if(mGoogleMap != null) {
            //zoom to the marker
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(marker.getPosition())
                    .zoom(18)
                    .build();

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private void updateUpcomingChargerInfoViews(Order order, Marker marker) {
        if(getActivity() == null) {
            return;
        }

        mUpcomingChargerPowerTextView.setText(order.getComponents().get(0).getPrototype().getPowerRating());
        mUpcomingChargerSocketTypeTextView.setText(order.getComponents().get(0).getPrototype().getConnectorType());

        if(order.getComponents().get(0).getPrototype().getConnectorType().equals("5A/15A Socket")) {
            mUpcomingChargerSocketTypeImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_5_15_amp));
        } else if(order.getComponents().get(0).getPrototype().getConnectorType().equals("5A Socket")) {
            mUpcomingChargerSocketTypeImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_5_amp));
        } else if(order.getComponents().get(0).getPrototype().getConnectorType().equals("15A Socket")) {
            mUpcomingChargerSocketTypeImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_15_amp));
        }


        //make deployed charger card invisble
        mDeployedChargerInfoCardView.setVisibility(View.INVISIBLE);

        //make the card visible
        mUpcomingChargerInfoCardView.setVisibility(View.VISIBLE);

        if(mGoogleMap != null) {
            //zoom to the marker
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(marker.getPosition())
                    .zoom(18)
                    .build();

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_LOCATION_PERMISSION_REQ_CODE){
            mPermissionRequestInProgress = false;
            //check if all permissions are granted
            setupGoogleMapsAndFetchCurrentLocationDetails();
        }

    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        mGreenMarkerBitmap.recycle();
        mRedMarkerBitmap.recycle();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
