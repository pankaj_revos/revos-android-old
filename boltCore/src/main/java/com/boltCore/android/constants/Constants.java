package com.boltCore.android.constants;

/**
 * Created by mohit on 29/8/17.
 */

public interface Constants {
    //user prefs
    String USER_PREFS = "userPrefs";
    String USER_JSON_DATA_KEY = "user_json_data_key";
    String USER_TOKEN_KEY = "user_token_key";
    String ACTIVE_BOOKINGS_KEY = "activeChargerBookingKey";
    String USER_PAYMENT_TRANSIENT_PHONE_KEY = "user_payment_transient_phone_key";
    String USER_PAYMENT_TRANSIENT_EMAIL_KEY = "user_payment_transient_email_key";

    //charger prefs
    String CHARGER_PREFS = "chargerPrefs";

    //utility prefs
    String UTILITY_PREFS = "utilityPrefs";
    String BOLT_BOOK_ACTIVITY_LAUNCHED = "bolt_book_activity_launched";

    //Error Strings
    String USER_DOES_NOT_EXIST = "User with given UID not registered";
    String USER_UID_PHONE_COMBINATION_MISMATCH = "User phone/email & UID combination mismatch";

    //Generic error message activity arguments constants
    String GENERIC_MESSAGE_TEXT_KEY = "error_text_message";
    String GENERIC_MESSAGE_STATUS_KEY = "error_status";
    String GENERIC_MESSAGE_TIMER_KEY = "error_message_show_time";
    String GENERIC_MESSAGE_BUTTON_TEXT_KEY = "error_message_button_text_key";

    //String delimiter for messages
    String GEN_DELIMITER = "-1del-1m-1ter";

    //General Events
    String GEN_EVENT_TAB_CHANGE = "gen_event_tab_change";

    //BLE Events
    String BLE_EVENT_DEVICE_FOUND = "ble_event_device_found";
    String BLE_EVENT_SCAN_STOP = "ble_event_scan_stop";

    //intent keys
    String CHARGER_UID_KEY = "charger_uid_key";

    String GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    String PHONE_PAY_PACKAGE_NAME = "com.phonepe.app";

    //usage types
    String USAGE_PUBLIC_PAID = "PUBLIC_PAID";
    String USAGE_PUBLIC_FREE = "PUBLIC_FREE";
    String USAGE_PRIVATE = "PRIVATE";

    //version
    String VERSION_1_0_0 = "1.0.0";

}
