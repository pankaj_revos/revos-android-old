package com.boltCore.android.utilities.ble;


import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.ble.data.Data;

public interface BoltBleManagerCallbacks extends BleManagerCallbacks {

    void onCommandCharacteristicDataReceived(Data data);

    void onHouseKeepingCharacteristicDataReceived(Data data);

    void onOtaCharacteristicDataReceived(Data data);
}
