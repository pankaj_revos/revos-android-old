package com.boltCore.android.utilities.general;

import android.content.Context;
import android.content.SharedPreferences;

import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.jsonStructures.User;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;
import static com.boltCore.android.constants.Constants.ACTIVE_BOOKINGS_KEY;
import static com.boltCore.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.boltCore.android.constants.Constants.USER_PREFS;

public class PrefUtils {

    //holds the single instance
    private static PrefUtils mInstance = null;

    //private constructor
    private PrefUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static PrefUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new PrefUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    public User getUserObjectFromPrefs() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);
        String userDataJSON = sharedPreferences.getString(USER_JSON_DATA_KEY, null);
        if(userDataJSON == null) {
            return null;
        } else {
            try {
                User user = new Gson().fromJson(userDataJSON, User.class);
                return user;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public Booking getBookingObjectFromPrefs() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);
        String activeChargerBookingStr = sharedPreferences.getString(ACTIVE_BOOKINGS_KEY, null);

        if(activeChargerBookingStr == null) {
            return null;
        } else {
            try {
                Booking booking = new Gson().fromJson(activeChargerBookingStr, Booking.class);
                return booking;
            } catch (Exception e) {
                return null;
            }
        }
    }


}