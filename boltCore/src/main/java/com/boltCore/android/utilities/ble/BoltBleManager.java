package com.boltCore.android.utilities.ble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.util.Base64;

import com.boltCore.android.utilities.security.EncryptionHelper;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.ble.data.Data;
import timber.log.Timber;

public class BoltBleManager extends BleManager<BoltBleManagerCallbacks> implements BoltBleManagerCallbacks {

    private Context mContext;
    private Handler mHandler;

    private String mChargerFirmwareVersion = "";
    private int mChargerActivationState = 0;
    private String mFirmwareInfoString = "";
    private String mChargerUID = "";
    private int mChargerPowerState = 0;

    //byte arrays for handshake
    private byte[] mSessionPassword;
    private byte[] mSentEncryptedBytes;

    public static final int STATE_INITIALIZED = 1;
    public static final int STATE_ACTIVATED = 2;

    private String mDefaultAESKey = Base64.encodeToString("REVOS ESP8266KEY".getBytes(), Base64.NO_WRAP | Base64.URL_SAFE);
    private String mDeviceAESKey = null;
    private String mAesKeyToBeUsed = null;

    private boolean mIsAuthenticationInProgress = false;
    private boolean mIsAuthenticationComplete = false;

    private boolean mIsActivationWriteInProgress = false;
    private boolean mIsActivationWriteComplete = false;

    private ActivationStatusListener mActivationStatusListener;

    private OtaStatusListener mOtaStatusListener;
    private File mOtaFile;
    private boolean mIsOtaInProgress = false;
    private boolean mIsOtaComplete = false;
    private byte[] mOtaFileBytes;
    private int mCurrentBytePosition = 0;
    private int mFileBytesSentInLastPacket = 0;
    private byte mLastCheckSum = 0;

    private static long mBluetoothConnectedTimestamp;

    private final int AUTHENTICATION_TIME_LIMIT = 30000;

    private final int BOOKING_TYPE_CURRENT = 1;
    private final int BOOKING_TYPE_PREVIOUS = 2;
    private String mCurrentBookingInfoString;
    private String mPreviousBookingInfoString;

    /** Service UUID */
    private final static UUID MAIN_SERVICE_UUID = UUID.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
    /** Command characteristic UUID */
    private final static UUID UART_COMMAND_CHARACTERISTIC_UUID = UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26b8");
    /** House keeping characteristic UUID */
    private final static UUID UART_HOUSE_KEEPING_CHARACTERISTIC_UUID = UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8");
    /** Firmware info characteristic UUID */
    private final static UUID UART_FIRMWARE_INFO_CHARACTERISTIC_UUID = UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b12a4");

    /** OTA Service UUID*/
    private final static UUID OTA_SERVICE_UUID = UUID.fromString("d804b643-6ce7-4e81-9f8a-ce0f699085eb");
    private final static UUID UART_OTA_CHARACTERISTIC_UUID = UUID.fromString("d804b644-6ce7-4e81-9f8a-ce0f699085eb");


    private BluetoothGattCharacteristic mCommandCharacteristic, mHouseKeepingCharacteristic, mOtaCharacteristic, mFirmwareInfoCharacteristic;
    /**
     * A flag indicating whether Long Write can be used. It's set to false if the UART RX
     * characteristic has only PROPERTY_WRITE_NO_RESPONSE property and no PROPERTY_WRITE.
     * If you set it to false here, it will never use Long Write.
     *
     * TODO change this flag if you don't want to use Long Write even with Write Request.
     */
    private boolean mUseLongWrite = true;

    private SuccessCallback mDisconnectSuccessCallback = new SuccessCallback() {
        @Override
        public void onRequestCompleted(@NonNull BluetoothDevice device) {
            Timber.d("disconnect_successful");
        }
    };
    private FailCallback mDisconnectFailCallback = new FailCallback() {
        @Override
        public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
            Timber.d("disconnect_fail");
        }
    };

    /**
     * BluetoothGatt callbacks object.
     */
    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {
        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(mCommandCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onCommandCharacteristicDataReceived(data);
                    });

            setNotificationCallback(mHouseKeepingCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onHouseKeepingCharacteristicDataReceived(data);
                    });


            setNotificationCallback(mOtaCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onOtaCharacteristicDataReceived(data);
                    });

            requestMtu(512).enqueue();

            enableNotifications(mCommandCharacteristic).enqueue();
            enableNotifications(mHouseKeepingCharacteristic).enqueue();
            enableNotifications(mOtaCharacteristic).enqueue();

        }

        @Override
        public boolean isRequiredServiceSupported(@NonNull final BluetoothGatt gatt) {

            final BluetoothGattService mainService = gatt.getService(MAIN_SERVICE_UUID);
            if(mainService != null) {
                mCommandCharacteristic = mainService.getCharacteristic(UART_COMMAND_CHARACTERISTIC_UUID);
                mHouseKeepingCharacteristic = mainService.getCharacteristic(UART_HOUSE_KEEPING_CHARACTERISTIC_UUID);
                mFirmwareInfoCharacteristic = mainService.getCharacteristic(UART_FIRMWARE_INFO_CHARACTERISTIC_UUID);
            }

            final BluetoothGattService otaService = gatt.getService(OTA_SERVICE_UUID);
            if(otaService != null) {
                mOtaCharacteristic = otaService.getCharacteristic(UART_OTA_CHARACTERISTIC_UUID);
            }

            /*boolean writeRequest = false;
            boolean writeCommand = false;
            if(mImageCharacteristic != null) {
                final int rxProperties = mImageCharacteristic.getProperties();
                writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
                writeCommand = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) > 0;

                // Set the WRITE REQUEST type when the characteristic supports it.
                // This will allow to send long write (also if the characteristic support it).
                // In case there is no WRITE REQUEST property, this manager will divide texts
                // longer then MTU-3 bytes into up to MTU-3 bytes chunks.
                if(writeRequest)
                    mImageCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                else
                    mUseLongWrite = false;
            }*/

            return mCommandCharacteristic != null && mHouseKeepingCharacteristic != null && mOtaCharacteristic != null && mFirmwareInfoCharacteristic != null;// && (writeRequest || writeCommand);
        }

        @Override
        protected void onDeviceDisconnected() {
            mCommandCharacteristic = null;
            mUseLongWrite = true;
        }
    };

    public void startOTA(String filePath, OtaStatusListener otaStatusListener) {
        mOtaStatusListener = otaStatusListener;
        mOtaFile = new File(filePath);
        int size = (int) mOtaFile.length();
        mOtaFileBytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(mOtaFile));
            buf.read(mOtaFileBytes, 0, mOtaFileBytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            otaStatusListener.onOtaError("File not found");
            return;
        } catch (IOException e) {
            otaStatusListener.onOtaError("IO Exception");
            return;
        }

        if(mChargerPowerState == 1) {
            otaStatusListener.onOtaError("Can't do OTA if charger is ON");
            return;
        }

        mIsOtaInProgress = true;
        mIsOtaComplete = false;

        //send this byte array to housekeeping service
        byte[] startOtaByteArray = new byte[1];
        startOtaByteArray[0] = (byte)0xA4;

        writeDataToHouseKeepingCharacteristic(startOtaByteArray);
    }

    public void disconnectFromDevice(SuccessCallback successCallback, FailCallback failCallback) {
        //disable notifications
        disableNotifications(mCommandCharacteristic).enqueue();
        disableNotifications(mHouseKeepingCharacteristic).enqueue();
        disableNotifications(mOtaCharacteristic).enqueue();
        //disconnect from device
        disconnect()
                .done(successCallback)
                .fail(failCallback)
                .enqueue();
    }

    private void writeDataToHouseKeepingCharacteristic(byte[] data) {
        writeCharacteristic(mHouseKeepingCharacteristic, data)
            .done(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {
                    Timber.d("house_keeping_write_successful");
                    if(mIsActivationWriteInProgress) {
                        mIsActivationWriteInProgress = false;
                        mIsActivationWriteComplete = true;
                        mActivationStatusListener.result(true);
                    }
                }
            })
            .fail(new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                    Timber.d("house_keeping_write_fail with status : " + status);
                    if(mIsActivationWriteInProgress) {
                        mIsActivationWriteInProgress = false;
                        mIsActivationWriteComplete = false;
                        mActivationStatusListener.result(false);
                    }
                }
            })
            .enqueue();
    }

    public void turnChargerOn(String bookingId, int seconds) {
        byte[] bookingIdBytes = bookingId.getBytes();

        //length of booking id, booking id bytes, on/off charger, enable/disable timer, timer bytes
        byte[] controlByteArray = new byte[1 + bookingIdBytes.length + 1 + 1 + 4];

        //set length of bookingId
        controlByteArray[0] = (byte)bookingIdBytes.length;

        //copy bookingId bytes to controlByteArray
        System.arraycopy(bookingIdBytes, 0, controlByteArray, 1, bookingIdBytes.length);

        //turn on charger
        controlByteArray[1 + bookingIdBytes.length] = (byte)0x01;

        //enable timer
        controlByteArray[1 + bookingIdBytes.length + 1] = (byte)0x01;


        String hexStr = Long.toHexString(seconds);
        hexStr = StringUtils.leftPad(hexStr, 8, '0');

        byte[] timerArray = BleUtils.hexStringToByteArray(hexStr);

        System.arraycopy(timerArray, 0, controlByteArray, 1 + bookingIdBytes.length + 1 + 1, 4);

        try {
            byte[] encryptedBytes = EncryptionHelper.encryptBytes(controlByteArray, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));

            byte[] byteArrayToBeSent = new byte[2 + encryptedBytes.length];
            byteArrayToBeSent[0] = (byte)0xA1;
            byteArrayToBeSent[1] = (byte)encryptedBytes.length;

            System.arraycopy(encryptedBytes, 0, byteArrayToBeSent, 2, encryptedBytes.length);
            writeDataToCommandCharacteristic(byteArrayToBeSent);

            Timber.d("String sent : %s", BleUtils.ByteArraytoHex(byteArrayToBeSent));
            Timber.d("Encrypted string : %s", BleUtils.ByteArraytoHex(encryptedBytes));
        } catch (Exception e) {

        }
    }

    public void turnChargerOff(String bookingId) {
        byte[] bookingIdBytes = bookingId.getBytes();

        //length of booking id, booking id bytes, on/off charger, enable/disable timer, timer bytes
        byte[] controlByteArray = new byte[1 + bookingIdBytes.length + 1 + 1 + 4];

        //set length of bookingId
        controlByteArray[0] = (byte)bookingIdBytes.length;

        //copy bookingId bytes to controlByteArray
        System.arraycopy(bookingIdBytes, 0, controlByteArray, 1, bookingIdBytes.length);

        //turn off charger
        controlByteArray[1 + bookingIdBytes.length] = (byte)0x02;

        //disable timer
        controlByteArray[1 + bookingIdBytes.length + 1] = (byte)0x00;


        String hexStr = Long.toHexString(0);
        hexStr = StringUtils.leftPad(hexStr, 8, '0');

        byte[] timerArray = BleUtils.hexStringToByteArray(hexStr);

        System.arraycopy(timerArray, 0, controlByteArray, 1 + bookingIdBytes.length + 1 + 1, 4);

        try {
            byte[] encryptedBytes = EncryptionHelper.encryptBytes(controlByteArray, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));

            byte[] byteArrayToBeSent = new byte[2 + encryptedBytes.length];
            byteArrayToBeSent[0] = (byte)0xA1;
            byteArrayToBeSent[1] = (byte)encryptedBytes.length;

            System.arraycopy(encryptedBytes, 0, byteArrayToBeSent, 2, encryptedBytes.length);
            writeDataToCommandCharacteristic(byteArrayToBeSent);

            Timber.d("String sent : %s", BleUtils.ByteArraytoHex(byteArrayToBeSent));
            Timber.d("Encrypted string : %s", BleUtils.ByteArraytoHex(encryptedBytes));
        } catch (Exception e) {

        }

    }

    public void setCurrentBookingId(String bookingId) {
        byte[] bookingIdBytes = bookingId.getBytes();

        //length of booking id, booking id bytes, on/off charger, enable/disable timer, timer bytes
        byte[] controlByteArray = new byte[1 + bookingIdBytes.length + 1 + 1 + 4];

        //set length of bookingId
        controlByteArray[0] = (byte)bookingIdBytes.length;

        //copy bookingId bytes to controlByteArray
        System.arraycopy(bookingIdBytes, 0, controlByteArray, 1, bookingIdBytes.length);

        //set booking id command
        controlByteArray[1 + bookingIdBytes.length] = (byte)0x04;

        //disable timer
        controlByteArray[1 + bookingIdBytes.length + 1] = (byte)0x00;

        String hexStr = Long.toHexString(0);
        hexStr = StringUtils.leftPad(hexStr, 8, '0');

        byte[] timerArray = BleUtils.hexStringToByteArray(hexStr);

        System.arraycopy(timerArray, 0, controlByteArray, 1 + bookingIdBytes.length + 1 + 1, 4);

        try {
            byte[] encryptedBytes = EncryptionHelper.encryptBytes(controlByteArray, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));

            byte[] byteArrayToBeSent = new byte[2 + encryptedBytes.length];
            byteArrayToBeSent[0] = (byte)0xA1;
            byteArrayToBeSent[1] = (byte)encryptedBytes.length;

            System.arraycopy(encryptedBytes, 0, byteArrayToBeSent, 2, encryptedBytes.length);
            writeDataToCommandCharacteristic(byteArrayToBeSent);

            Timber.d("String sent : %s", BleUtils.ByteArraytoHex(byteArrayToBeSent));
            Timber.d("Encrypted string : %s", BleUtils.ByteArraytoHex(encryptedBytes));
        } catch (Exception e) {
            int j = 10;
        }

    }

    private byte[] getHeartBeatAckBytes(byte packetId) {
        byte[] data = new byte[2];
        data[0] = (byte)0xA3;
        data[1] = packetId;

        return data;
    }

    private void writeDataToCommandCharacteristic(byte[] data) {

        writeCharacteristic(mCommandCharacteristic, data)
                .done(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        Timber.d("command_write_successful");
                    }
                })
                .fail(new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        Timber.d("command_write_fail");
                    }
                })
                .enqueue();
    }

    private void writeDataToOtaCharacteristic(byte[] data) {

        writeCharacteristic(mOtaCharacteristic, data)
                .done(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        Timber.d("ota_write_successful");
                    }
                })
                .fail(new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        Timber.d("ota_write_fail");
                    }
                })
                .enqueue();
    }

    private void clearSessionRelatedVariables() {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mIsAuthenticationInProgress = false;
                mIsAuthenticationComplete = false;

                mIsOtaInProgress = false;
                mIsOtaComplete = false;

                mIsActivationWriteInProgress = false;
                mIsActivationWriteComplete = false;

                mSessionPassword = null;

                mCurrentBookingInfoString = null;
                mPreviousBookingInfoString = null;

                mChargerPowerState = 0;

                mChargerActivationState = 0;

                mChargerFirmwareVersion = "";
                mFirmwareInfoString = "";

                Timber.d("clearing session related variables");
            }
        });
    }

    public BoltBleManager(@NonNull final Context context) {
        super(context);
        mContext = context;
        mHandler = new Handler(((Activity) mContext).getMainLooper());
        setGattCallbacks(this);
    }

    public void connectToDevice(String deviceAddress, @Nullable String aesKey) {
        if(aesKey == null) {
            mDeviceAESKey = mDefaultAESKey;
        } else {
            mDeviceAESKey = aesKey;
        }

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!bluetoothAdapter.isEnabled()) {
            Timber.d("bt_adapter_disabled");
            return;
        }

        if(bluetoothAdapter.getState() != BluetoothAdapter.STATE_ON) {
            Timber.d("bt_adapter_not_on");
            return;
        }

        Timber.d("attempting to connect with : " + deviceAddress);

        if(getConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            connect(bluetoothAdapter.getRemoteDevice(deviceAddress))
                    .retry(3, 500)
                    .done(new SuccessCallback() {
                        @Override
                        public void onRequestCompleted(@NonNull BluetoothDevice device) {
                            Timber.d("connection_successful with : " + device.getName());
                        }
                    })
                    .fail(new FailCallback() {
                        @Override
                        public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                            Timber.d("connection_unsuccessful with : " + device.getName() + ", status : " + status);
                        }
                    })
                    .enqueue();
        }
    }

    private void decryptFirmwareVersionInfo(byte[] data) {
        try {
            byte[] decryptedBytes = EncryptionHelper.decryptBytes(data, mDefaultAESKey);
            mFirmwareInfoString = new String(decryptedBytes);
            String[] tokens = mFirmwareInfoString.split(",");
            mChargerFirmwareVersion = tokens[0];
            mChargerActivationState = Integer.parseInt(tokens[1]);
            mChargerUID = tokens[2];

            if(mChargerActivationState == STATE_ACTIVATED) {
                mAesKeyToBeUsed = mDeviceAESKey;
            } else {
                mAesKeyToBeUsed = mDefaultAESKey;
            }
        } catch (Exception e) {
        }
    }

    public String getChargerUID() {
        return mChargerUID;
    }

    public int getChargerActivationState() {
        return mChargerActivationState;
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @Override
    public void onDeviceConnecting(@NonNull BluetoothDevice device) {
        Timber.d("device_connecting");
    }

    @Override
    public void onDeviceConnected(@NonNull BluetoothDevice device) {
        Timber.d("device_connected");

        mBluetoothConnectedTimestamp = System.currentTimeMillis();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                readCharacteristic(mFirmwareInfoCharacteristic).with((device1, data) -> {
                    decryptFirmwareVersionInfo(data.getValue());
                    initiateHandshake();
                }).enqueue();
            }
        }, 1000);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int j = 10;
                if(mIsAuthenticationInProgress) {
                    if(System.currentTimeMillis() - mBluetoothConnectedTimestamp > AUTHENTICATION_TIME_LIMIT && mBluetoothConnectedTimestamp != 0) {
                        initiateAuthenticationFailedSequence("timeout while authentication in progress");
                    }
                } else if(!mIsAuthenticationComplete) {
                    initiateAuthenticationFailedSequence("timeout while authentication not complete flag set as false");
                }
            }
        }, AUTHENTICATION_TIME_LIMIT);
    }

    @Override
    public void onDeviceDisconnecting(@NonNull BluetoothDevice device) {
        clearSessionRelatedVariables();
        Timber.d("device_disconnecting");
    }

    @Override
    public void onDeviceDisconnected(@NonNull BluetoothDevice device) {
        if(mIsOtaInProgress && !mIsOtaComplete) {
            if(mOtaStatusListener != null) {
                mOtaStatusListener.onOtaError("OTA aborted due to device disconnect");
            }
        }
        clearSessionRelatedVariables();
        Timber.d("device_disconnected");
    }

    @Override
    public void onLinkLossOccurred(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onServicesDiscovered(@NonNull BluetoothDevice device, boolean optionalServicesFound) {

    }

    @Override
    public void onDeviceReady(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBondingRequired(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBonded(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBondingFailed(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onError(@NonNull BluetoothDevice device, @NonNull String message, int errorCode) {

    }

    @Override
    public void onDeviceNotSupported(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onCommandCharacteristicDataReceived(Data data) {
        byte[] dataBytes = data.getValue();

        if(dataBytes == null || dataBytes.length < 1) {
            return;
        }

        String dataString = data.getStringValue(0);

        Timber.d("Command byte received data : " + BleUtils.ByteArraytoHex(dataBytes));

    }

    @Override
    public void onHouseKeepingCharacteristicDataReceived(Data data) {
        byte[] dataBytes = data.getValue();

        if (dataBytes == null || dataBytes.length < 1) {
            return;
        }

        String dataString = data.getStringValue(0);
        Timber.d("HouseKeeping char received data(" + dataBytes.length + "): " + BleUtils.ByteArraytoHex(dataBytes));

        //check the first byte
        byte firstByte = dataBytes[0];

        if(firstByte == (byte)0x05) {
            initiateAuthenticationFailedSequence("auth fail byte 0x05 received ");
        } else if(dataBytes.length > 1 && dataBytes[0] == (byte)0xDA && mIsAuthenticationInProgress) {
            //this is a response packet
            byte[] responseBytes = new byte[dataBytes.length - 1];
            System.arraycopy(dataBytes, 1, responseBytes, 0, responseBytes.length);

            //first check if it is not equal to what we sent, if it is equal then disconnect
            if(responseBytes.length != mSentEncryptedBytes.length) {
                //length mismatch, disconnect
                initiateAuthenticationFailedSequence("length mismatch in handshake");
            } else {
                //check if the the arrays are equal in content, if yes, then disconnect, we expect a different encrypted string
                if(Arrays.equals(responseBytes, mSentEncryptedBytes)) {
                    initiateAuthenticationFailedSequence("length equal but content mismatch in handshake");
                } else {
                    //the arrays are equal in length but they differ in content, now check if decrypted string is the same or not
                    try {
                        byte[] originalDecryptedString = EncryptionHelper.decryptBytes(mSentEncryptedBytes, mAesKeyToBeUsed);
                        byte[] receivedDecryptedString = EncryptionHelper.decryptBytes(responseBytes, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));

                        //check if these two arrays are equal if not, then disconnect
                        if(!Arrays.equals(originalDecryptedString, receivedDecryptedString)) {
                            initiateAuthenticationFailedSequence("decrypted array content mismatch");
                        } else {
                            //yes both strings are equal, authentication complete
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    mIsAuthenticationInProgress = false;
                                    mIsAuthenticationComplete = true;
                                }
                            });
                        }

                    } catch (Exception e) {
                        initiateAuthenticationFailedSequence("exception while trying to decrypt handshake byte array");
                    }
                }
            }

        } else if(firstByte == (byte)0xB4) {
            byte[] encryptedBytes = new byte[dataBytes.length - 1];

            System.arraycopy(dataBytes, 1, encryptedBytes, 0, encryptedBytes.length);
            try {
                byte[] decryptedBytes = EncryptionHelper.decryptBytes(encryptedBytes, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));

                //it's a heartbeat packet
                //first process it and then acknowledge it
                byte packetId = decryptedBytes[0];

                //get the charger state
                mChargerPowerState = decryptedBytes[1];

                //todo::get if timer is on/off using 4th byte
                //todo::get remaining timer time using next four bytes

                //get the version number
                byte[] versionArray = Arrays.copyOfRange(decryptedBytes, 7, decryptedBytes.length);
                mChargerFirmwareVersion = new String(versionArray);

                //now acknowledge the heartbeat packet
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        writeDataToHouseKeepingCharacteristic(getHeartBeatAckBytes(packetId));
                    }
                }, 500);

                //also make a delayed call to update energy consumption variables
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateEnergyConsumedSessionVariables();
                    }
                }, 1000);
            } catch (Exception e) {
                //do nothing
            }
        } else if(firstByte == (byte)0xB1) {
            byte[] encryptedBytes = new byte[dataBytes.length - 1];
            System.arraycopy(dataBytes, 1, encryptedBytes, 0, encryptedBytes.length);

            try {
                byte[] decryptedBytes = EncryptionHelper.decryptBytes(encryptedBytes, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
                //first four bytes are charge id
                //charger state byte
                mChargerPowerState = decryptedBytes[4];
            } catch (Exception e) {
                int j =10;
                //nothing to here
            }
        } else if(firstByte == (byte)0xB5) {
            if(mIsOtaInProgress) {
                mOtaStatusListener.onOtaStarted();

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendFileDetailsPacketToOtaCharacteristic();
                    }
                }, 500);
            }
        } else if(firstByte == (byte)0xB2) {
            byte[] encryptedBytes = new byte[dataBytes.length - 1];
            System.arraycopy(dataBytes, 1, encryptedBytes, 0, encryptedBytes.length);

            try {
                byte[] decryptedBytes = EncryptionHelper.decryptBytes(encryptedBytes, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
                Timber.d("Booking decrypted string : " + BleUtils.ByteArraytoHex(decryptedBytes) + ", " + new String(decryptedBytes));

                int bookingType = decryptedBytes[0];
                int bookingIdLength = decryptedBytes[1];


                byte[] bookingIdBytes = new byte[bookingIdLength];
                //copy booking id
                System.arraycopy(decryptedBytes, 2, bookingIdBytes, 0, bookingIdLength);

                byte[] energyConsumedBytes = new byte[4];
                //copy energyConsumedBytes
                System.arraycopy(decryptedBytes, 2 + bookingIdLength, energyConsumedBytes, 0, 4);

                String infoString = new String(bookingIdBytes) + "," + String.valueOf(Long.parseLong(BleUtils.byteArrayToHexString(energyConsumedBytes), 16));

                Timber.d("Booking info string : " + String.valueOf(bookingType) + "," + infoString);

                if(bookingType == BOOKING_TYPE_CURRENT) {
                    mCurrentBookingInfoString = infoString;
                } else if(bookingType == BOOKING_TYPE_PREVIOUS) {
                    mPreviousBookingInfoString = infoString;
                }

            } catch (Exception e) {
                //nothing to here
            }
        }
    }

    public String getCurrentBookingEnergyConsumptionString() {

        return mCurrentBookingInfoString;
    }

    public String getPreviousBookingEnergyConsumptionString() {

        return mPreviousBookingInfoString;
    }


    private void updateEnergyConsumedSessionVariables() {
        byte[] commandArray = new byte[2];
        commandArray[0] = (byte)0xA2;
        commandArray[1] = (byte)1;

        //send command to get current session energy consumption
        writeDataToCommandCharacteristic(commandArray);

        commandArray[1] = (byte)2;
        //send command to get previous session energy consumption
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeDataToCommandCharacteristic(commandArray);
            }
        }, 1000);
    }


    public void activateCharger(byte[] aesKeyBytes, String chargerUID, ActivationStatusListener activationStatusListener) {
        mActivationStatusListener = activationStatusListener;
        try {
            byte[] chargerUidBytes = chargerUID.getBytes();
            //session password, aes key, activate/deactivate, checksum
            byte[] byteArrayToBeEncrypted = new byte[16 + 16 + 1 + 1 + chargerUidBytes.length + 1];


            System.arraycopy(mSessionPassword, 0, byteArrayToBeEncrypted, 0, mSessionPassword.length);
            System.arraycopy(aesKeyBytes, 0, byteArrayToBeEncrypted, mSessionPassword.length, aesKeyBytes.length);
            byte deviceStateByte = (byte)2;
            //set the device state
            byteArrayToBeEncrypted[mSessionPassword.length + aesKeyBytes.length] = deviceStateByte;
            //set the length of the charger uid
            byteArrayToBeEncrypted[mSessionPassword.length + aesKeyBytes.length + 1] = (byte)chargerUidBytes.length;
            //set the charger UID bytes
            System.arraycopy(chargerUidBytes, 0, byteArrayToBeEncrypted, mSessionPassword.length + aesKeyBytes.length + 2, chargerUidBytes.length);

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, 0, byteArrayToBeEncrypted.length);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1 + 1];
            commandArrayToBeSent[0] = (byte)(0xDF);
            commandArrayToBeSent[1] = (byte)byteArrayToBeEncrypted.length;
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 2, encryptedBytes.length);

            Timber.d("String sent : %s", BleUtils.ByteArraytoHex(byteArrayToBeEncrypted));
            Timber.d("Checksum : %s", checkSum);
            Timber.d("Encrypted string : %s", BleUtils.ByteArraytoHex(commandArrayToBeSent));

            mIsActivationWriteInProgress = true;
            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);
        } catch (Exception e) {
            activationStatusListener.result(false);
        }
    }

    private void initiateHandshake() {
        mIsAuthenticationInProgress = true;
        mSessionPassword =  generateSessionPassword();
        byte[] handShakeBytes = generateHandshakeBytes(mSessionPassword);
        Timber.d("generating password handshake bytes");
        try {
            byte[] encryptedBytes = EncryptionHelper.encryptBytes(handShakeBytes, mAesKeyToBeUsed);
            Timber.d("encrypted handshake bytes : " + BleUtils.ByteArraytoHex(encryptedBytes));
            mSentEncryptedBytes = encryptedBytes;
            byte[] arrayToBeSent = new byte[encryptedBytes.length + 1];
            arrayToBeSent[0] = (byte)0xDA;
            System.arraycopy(encryptedBytes, 0, arrayToBeSent, 1, encryptedBytes.length);
            //send this byte array to housekeeping service
            writeDataToHouseKeepingCharacteristic(arrayToBeSent);

            Timber.d("decrypted handshake bytes : " + BleUtils.ByteArraytoHex(EncryptionHelper.decryptBytes(mSentEncryptedBytes, mAesKeyToBeUsed)));

        } catch (Exception e) {
            initiateAuthenticationFailedSequence("exception while decrypting handshake byte");
        }
    }

    @Override
    public void onOtaCharacteristicDataReceived(Data data) {
        byte[] dataBytes = data.getValue();

        if(dataBytes == null || dataBytes.length < 1) {
            return;
        }

        Timber.d("OTA byte received data : " + BleUtils.ByteArraytoHex(dataBytes));

        if(dataBytes[0] == (byte)0xB6) {
            //file details have been successfully sent
            //now let's start sending the file part by part
            mCurrentBytePosition = 0;
            sendNextFilePartInBytes();
        } else if(dataBytes[0] == (byte)0xB7) {
            if(dataBytes[1] == mLastCheckSum) {
                mCurrentBytePosition += mFileBytesSentInLastPacket;
            }

            Timber.d("OTA Progress : " + mCurrentBytePosition * 100.0f / mOtaFileBytes.length + ", (" + mCurrentBytePosition + "/" + mOtaFileBytes.length + ")");

            float percentageCompleted = mCurrentBytePosition * 100.0f / mOtaFileBytes.length;

            mOtaStatusListener.onOtaProgress(percentageCompleted);

            if(mCurrentBytePosition < mOtaFileBytes.length) {
                sendNextFilePartInBytes();
            } else {
                sendOtaCompleteBytes();
                mOtaStatusListener.onOtaComplete(true);
                mIsOtaInProgress = false;
                mIsOtaComplete = true;
            }
        } else if(dataBytes[0] == (byte)0xB8) {
            if(dataBytes[1] == (byte)0x01) {
                mIsOtaInProgress = false;
                mIsOtaComplete = true;
                mOtaStatusListener.onOtaComplete(true);
            } else {
                mIsOtaInProgress = false;
                mIsOtaComplete = false;
                mOtaStatusListener.onOtaComplete(false);
            }
        }
    }

    private void sendNextFilePartInBytes() {
        if(mFileBytesSentInLastPacket != 0 && mFileBytesSentInLastPacket < (512 - 4)) {
            //this means all 512 bytes were not utilized, hence it must have the last packet
            sendOtaCompleteBytes();
            return;
        } else {
            byte[] completeByteArray;

            int numOfFileBytesToBeSent = Math.min((512 - 4), mOtaFileBytes.length - mCurrentBytePosition);

            if(numOfFileBytesToBeSent == 0) {
                sendOtaCompleteBytes();
                return;
            }

            mFileBytesSentInLastPacket = numOfFileBytesToBeSent;

            Timber.d("OTA file bytes sent : " + numOfFileBytesToBeSent);

            completeByteArray = new byte[1 + 2 + numOfFileBytesToBeSent + 1]; //marker byte, length, file bytes, check sum;

            //set the marker byte
            completeByteArray[0] = (byte)0xA7;

            //set the length including fileBytesToBeSent plus checksum
            byte[] lengthDataArray = BleUtils.hexStringToByteArray(StringUtils.leftPad(Integer.toHexString(numOfFileBytesToBeSent + 1), 4, '0'));
            System.arraycopy(lengthDataArray, 0, completeByteArray, 1, lengthDataArray.length);

            //set the file bytes array
            System.arraycopy(mOtaFileBytes, mCurrentBytePosition, completeByteArray, 3, numOfFileBytesToBeSent);

            mLastCheckSum = calculateCheckSum(completeByteArray, 3, completeByteArray.length);
            completeByteArray[completeByteArray.length - 1] = mLastCheckSum;

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    writeDataToOtaCharacteristic(completeByteArray);
                }
            }, 10);
        }
    }

    private void sendOtaCompleteBytes() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                byte[] dataArray = new byte[]{(byte)0xA8};
                writeDataToOtaCharacteristic(dataArray);
            }
        }, 10);
    }

    private byte calculateCheckSum(byte[] byteArray, int startPosition, int exclusiveEndPosition) {
        int checkSum = 0;
        for(int byteIndex = startPosition; byteIndex < exclusiveEndPosition; ++byteIndex) {
            byte currentByte = byteArray[byteIndex];
            int unsignedInt = (int)currentByte & 0xFF;
            checkSum += unsignedInt;
        }
        checkSum = checkSum & 0xFF;

        byte checkSumByte = (byte)checkSum;

        return checkSumByte;
    }

    private void sendFileDetailsPacketToOtaCharacteristic() {
        byte[] fileDetailByteArray = new byte[5];
        fileDetailByteArray[0] = (byte)0xA6;

        int lengthInBytes = (int)mOtaFile.length();

        String hexStr = Long.toHexString(lengthInBytes);
        hexStr = StringUtils.leftPad(hexStr, 8, '0');

        byte[] fileLengthArray = BleUtils.hexStringToByteArray(hexStr);

        System.arraycopy(fileLengthArray, 0, fileDetailByteArray, 1, 4);

        writeDataToOtaCharacteristic(fileDetailByteArray);
    }

    public int getChargerPowerState() {
        return mChargerPowerState;
    }

    public String getChargerFirmwareVersion() {
        return mChargerFirmwareVersion;
    }

    public boolean isAuthenticationComplete() {
        return mIsAuthenticationComplete;
    }

    private void initiateAuthenticationFailedSequence(String errorMessage) {
        Timber.d("authentication failed due to " + errorMessage);
        //todo::append error message in case of failure
        disconnectFromDevice(mDisconnectSuccessCallback, mDisconnectFailCallback);
    }

    private byte[] generateSessionPassword() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] sessionPassword = new byte[16];
        secureRandom.nextBytes(sessionPassword);

        return sessionPassword;
    }

    private byte[] generateHandshakeBytes(byte[] sessionPassword) {
        //first obtain the timestamp byte array
        byte[] epochBytes = BleUtils.constructSetEpochTimeByteArray(System.currentTimeMillis());

        //calculate checksum
        int checkSum = 0;
        for(byte currentByte : mSessionPassword) {
            int unsignedInt = (int)currentByte & 0xFF;
            checkSum += unsignedInt;
        }

        for(byte currentByte : epochBytes) {
            int unsignedInt = (int)currentByte & 0xFF;
            checkSum += unsignedInt;
        }
        checkSum = checkSum & 0xFF;

        byte checkSumByte = (byte)checkSum;

        byte[] handshakeBytes = new byte[mSessionPassword.length + epochBytes.length + 1];

        System.arraycopy(mSessionPassword, 0, handshakeBytes, 0, mSessionPassword.length);
        System.arraycopy(epochBytes, 0, handshakeBytes, mSessionPassword.length, epochBytes.length);

        handshakeBytes[handshakeBytes.length - 1] = checkSumByte;

        return handshakeBytes;
    }


}
