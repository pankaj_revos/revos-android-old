package com.boltCore.android.utilities.ble;

public interface OtaStatusListener {
    void onOtaStarted();

    void onOtaProgress(float percentage);

    void onOtaComplete(boolean success);

    void onOtaError(String errorMessage);
}
