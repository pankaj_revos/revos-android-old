package com.boltCore.android.utilities.ble;

public interface ActivationStatusListener {
    void result(boolean success);
}
