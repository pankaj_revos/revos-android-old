package com.boltCore.android.sdk;

public interface StatusCallBack {
    void onStatusChange(String message);

    void onError(String message);
}
