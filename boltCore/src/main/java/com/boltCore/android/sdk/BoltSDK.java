package com.boltCore.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.boltCore.android.activities.BoltBannerActivity;
import com.boltCore.android.activities.BoltMainActivity;
import com.boltCore.android.jsonStructures.User;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import androidx.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.content.Context.MODE_PRIVATE;
import static com.boltCore.android.activities.BoltBannerActivity.BANNER_TIME_LIMIT;
import static com.boltCore.android.constants.Constants.USER_DOES_NOT_EXIST;
import static com.boltCore.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.boltCore.android.constants.Constants.USER_PREFS;
import static com.boltCore.android.constants.Constants.USER_TOKEN_KEY;
import static com.boltCore.android.constants.Constants.USER_UID_PHONE_COMBINATION_MISMATCH;


public class BoltSDK {

    private static BoltSDK boltSDK;

    private int mTime;

    private String mApiKey;
    private Context mContext;
    private String mUid;
    private String mPhone;
    private String mEmail;
    private String mFirstName;
    private String mLastName;
    private String mFirebaseToken;
    private String mPackage;
    private StatusCallBack mStatusCallBack;
    private String mBuyUrl;
    private  BoltSDK(){}

    public static BoltSDK getInstance( ) {
        if(boltSDK == null) {
            boltSDK = new BoltSDK();
        }

        return boltSDK;
    }

    public String getApiKey() {
        return mApiKey;
    }

    public void initialize(Context context, String apiKey, String uid, StatusCallBack mStatusCallBack,
                           @Nullable String phone,
                           @Nullable String email,
                           @Nullable String firstName,
                           @Nullable String lastName,
                           @Nullable String revosFirebaseToken,
                           @Nullable String buyUrl){
        this.mApiKey = apiKey;
        this.mContext = context;
        this.mPackage = context.getPackageName();
        this.mUid = uid;
        this.mPhone = phone;
        this.mEmail = email;
        this.mFirstName = firstName;
        this.mLastName = lastName;

        if(revosFirebaseToken != null) {
            this.mFirebaseToken = revosFirebaseToken;
        }

        if(buyUrl != null) {
            this.mBuyUrl = buyUrl;
        }

        this.mStatusCallBack = mStatusCallBack;
        this.mTime = 250;

        updateVariablesBasedOnPackage();
    }

    private void updateVariablesBasedOnPackage() {
        if(this.mPackage.equals("com.revos.bolt.android")) {
            this.mTime = 0;
        }

        if(this.mPackage.equals("com.revos.bolt.android") || this.mPackage.equals("com.battre.revos.android") || this.mPackage.equals("com.kyte.revos.android") || this.mPackage.equals("com.kabira.revos.android")) {
            this.mPackage = "com.revos.android";
            this.mApiKey = "1234";
        }
    }

    private void loginWithRevos(String mPackage, String uid) {

        String phone = mPhone == null ? "" : mPhone;
        String email = mEmail == null ? "" : mEmail;
        String firstName = mFirstName == null ? "" : mFirstName;
        String lastName = mLastName == null ? "" : mLastName;

        ApiInterface loginApiInterface = ApiClient.getClient().create(ApiInterface.class);

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("UID", uid);
        bodyJsonObject.addProperty("package", mPackage);

        if(mPhone != null && !mPhone.trim().isEmpty()) {
            bodyJsonObject.addProperty("phone", phone);
        }

        if(mEmail != null && !mEmail.trim().isEmpty()) {
            bodyJsonObject.addProperty("email", email);
        }

        if(mFirstName != null && !mFirstName.trim().isEmpty()) {
            bodyJsonObject.addProperty("firstName", firstName);
        }

        if(mLastName != null && !mLastName.trim().isEmpty()) {
            bodyJsonObject.addProperty("lastName", lastName);
        }

        if(mFirebaseToken != null) {
            bodyJsonObject.addProperty("firebaseToken", mFirebaseToken);
        }

        String bodyStr = new Gson().toJson(bodyJsonObject);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        mStatusCallBack.onStatusChange("logging user in");

        loginApiInterface.loginUser(mApiKey, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.code() != 200) {
                        mStatusCallBack.onError("login request status not equal to 200");
                        return;
                    }

                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("token") && bodyJsonObject.has("user")) {
                        saveTokenAndUserDetailsToPrefs(bodyJsonObject);
                    } else if(bodyJsonObject.get("status").getAsInt() == 400 && bodyJsonObject.get("data").getAsString().equals(USER_DOES_NOT_EXIST)) {
                        registerUserWithRevos(mPackage,uid);
                    } else if(bodyJsonObject.get("status").getAsInt() == 400 && bodyJsonObject.get("data").getAsString().equals(USER_UID_PHONE_COMBINATION_MISMATCH)) {
                        registerUserWithRevos(mPackage,uid);
                    } else if(bodyJsonObject.get("status").getAsInt() == 401) {
                        registerUserWithRevos(mPackage,uid);
                    } else {
                        mStatusCallBack.onError("login unexpected response");
                    }
                } catch (Exception e) {
                    if(mStatusCallBack !=null){
                        mStatusCallBack.onError("login exception while parsing response");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(mStatusCallBack !=null){
                    mStatusCallBack.onError("login exception while Network Failure");
                }

            }
        });
    }
    private void registerUserWithRevos(String mPackage,String uid){

        String phone = mPhone == null ? "" : mPhone;
        String email = mEmail == null ? "" : mEmail;
        String firstName = mFirstName == null ? "" : mFirstName;
        String lastName = mLastName == null ? "" : mLastName;

        ApiInterface loginApiInterface = ApiClient.getClient().create(ApiInterface.class);

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("UID", uid);
        bodyJsonObject.addProperty("package", mPackage);

        if(mPhone != null && !mPhone.trim().isEmpty()) {
            bodyJsonObject.addProperty("phone", phone);
        }

        if(mEmail != null && !mEmail.trim().isEmpty()) {
            bodyJsonObject.addProperty("email", email);
        }

        if(mFirstName != null && !mFirstName.trim().isEmpty()) {
            bodyJsonObject.addProperty("firstName", firstName);
        }

        if(mLastName != null && !mLastName.trim().isEmpty()) {
            bodyJsonObject.addProperty("lastName", lastName);
        }

        String bodyStr = new Gson().toJson(bodyJsonObject);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        mStatusCallBack.onStatusChange("registering user");

        loginApiInterface.registerUser(mApiKey, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.code() != 200) {
                        mStatusCallBack.onError("register request status not equal to 200");
                        return;
                    }
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("token") && bodyJsonObject.has("user")) {
                        saveTokenAndUserDetailsToPrefs(bodyJsonObject);
                    } else {
                        mStatusCallBack.onError("register unexpected response");
                    }
                } catch (Exception e) {
                    if(mStatusCallBack !=null){
                        mStatusCallBack.onError("register exception while parsing response");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(mStatusCallBack !=null){
                    mStatusCallBack.onError("register exception while Network Failure");
                }
            }
        });
    }

    private void saveTokenAndUserDetailsToPrefs(JsonObject bodyJsonObject) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putString(USER_TOKEN_KEY, bodyJsonObject.get("token").getAsString()).apply();
        User user = new Gson().fromJson(bodyJsonObject.get("user").getAsJsonObject().toString(), User.class);
        sharedPreferences.edit().putString(USER_JSON_DATA_KEY, new Gson().toJson(user)).apply();

        launchBolt();
    }

    public void launchBolt() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);

        if(sharedPreferences.getString(USER_TOKEN_KEY, null) == null || sharedPreferences.getString(USER_JSON_DATA_KEY, null) == null) {
            loginWithRevos(mPackage, mUid);
            return;
        }

        mStatusCallBack.onStatusChange("Launching bolt");

        Intent myIntent = new Intent(mContext, BoltBannerActivity.class);
        myIntent.putExtra(BANNER_TIME_LIMIT, mTime);
        mContext.startActivity(myIntent);
    }

    public String getBuyUrl() {
        return mBuyUrl;
    }

    public String getPackage() {
        return mPackage;
    }

    public void clearContext() {
        if(mContext != null) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
        }

        mApiKey = null;
        mContext = null;
        mUid = null;
        mPhone = null;
        mEmail = null;
        mFirstName = null;
        mLastName = null;
        mFirebaseToken = null;
        mPackage = null;
        mStatusCallBack = null;
        mBuyUrl = null;
        mTime = 250;
    }
}
