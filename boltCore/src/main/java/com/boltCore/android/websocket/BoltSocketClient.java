package com.boltCore.android.websocket;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

public class BoltSocketClient extends WebSocketClient {

    private BoltSocketListener boltSocketListener;

    public BoltSocketClient(URI serverURI, BoltSocketListener boltSocketListener) {
        super(serverURI);
        this.boltSocketListener = boltSocketListener;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        boltSocketListener.onOpen(handshakedata);
    }

    @Override
    public void onMessage(String message) {
        boltSocketListener.onMessage(message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        boltSocketListener.onClose(code, reason, remote);
    }

    @Override
    public void onError(Exception ex) {
        boltSocketListener.onError(ex);
    }
}
