package com.boltCore.android.websocket;

import org.java_websocket.handshake.ServerHandshake;

public interface BoltSocketListener {
    void onOpen(ServerHandshake handshakedata);

    void onMessage(String message);

    void onClose(int code, String reason, boolean remote);

    void onError(Exception ex);
}
