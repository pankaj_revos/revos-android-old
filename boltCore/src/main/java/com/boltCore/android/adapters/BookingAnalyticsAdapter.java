package com.boltCore.android.adapters;


import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.boltCore.android.R;
import com.boltCore.android.jsonStructures.Booking;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class BookingAnalyticsAdapter extends RecyclerView.Adapter<BookingAnalyticsAdapter.MyBarChatHolder> {

    private List<Booking> mBookingList;
    private Context mContext;
    private TextView mMaxTextView,mTotalTextView,mAvgTextView,mTripType;
    private TextView mMaxType,mTotalType,mAvgType;
    private BarChart mBookingAnalyticsChart;
    private final int ANIMATION_DURATION = 500;
    private int mType;

    public BookingAnalyticsAdapter(List<Booking> mBookingList, int mType, Context context ){
        this.mBookingList = mBookingList;
        this.mType=mType;
        this.mContext = context;
    }

    @NonNull
    @Override
    public  MyBarChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bolt_earnings_barchart_view, parent, false);
        return new BookingAnalyticsAdapter.MyBarChatHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  MyBarChatHolder holder, int position) {
        if(mBookingList == null || mBookingList.isEmpty()) {
            return;
        }
        Booking  booking = mBookingList.get(position);
        if (booking == null){
            return;
        }

        if (mType == 0){
            initializeExpenseGraph((ArrayList<Booking>)  mBookingList);
        }else if (mType == 1){
            mBookingGraph((ArrayList<Booking>)  mBookingList);
        }

    }



    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyBarChatHolder extends RecyclerView.ViewHolder {

        public MyBarChatHolder(@NonNull View itemView) {
            super(itemView);

            mBookingAnalyticsChart = (BarChart)itemView.findViewById(R.id.analytics_chart);
            mMaxTextView = (TextView)itemView.findViewById(R.id.analytics_max_distance_text_view);
            mTotalTextView = (TextView)itemView.findViewById(R.id.analytics_total_distance_text_view);
            mAvgTextView = (TextView)itemView.findViewById(R.id.analytics_avg_distance_text_view);
            mTripType = (TextView)itemView.findViewById(R.id.trips_type);
            mMaxType= (TextView)itemView.findViewById(R.id.max_money);
            mTotalType= (TextView)itemView.findViewById(R.id.total_money);
            mAvgType= (TextView)itemView.findViewById(R.id.avg_money);
        }
    }

    private void initializeExpenseGraph(ArrayList<Booking> mBookingList) {
        if(mContext == null || mBookingAnalyticsChart == null || mMaxTextView == null ||mTotalTextView == null || mAvgTextView == null || mTripType == null || mMaxType == null || mAvgType == null || mTotalType == null) {
            return;
        }
        mTripType.setText(mContext.getResources().getString(R.string.analytics_expense));
        mMaxType .setText(mContext.getResources().getString(R.string.analytics_max));
        mAvgType .setText(mContext.getResources().getString(R.string.analytics_avg));
        mTotalType .setText(mContext.getResources().getString(R.string.analytics_total));

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        float maxCost = 0;
        float totalCost = 0;


        ArrayList<Booking> allEvents =mBookingList;
        ArrayList<Booking> noRepeat = new ArrayList<Booking>();

        for (Booking event : allEvents) {
            boolean isFound = false;
            for (Booking  b : noRepeat) {
                DateTime dateTime=new DateTime(b.getStartTime());
                DateTime dateTime1=new DateTime(event.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate()) || (b.equals(event))){
                    isFound = true;
                    break;
                }
            }
            if (!isFound) noRepeat.add(event);
        }

        int index=0;

        for (Booking  booking : noRepeat) {
            int amount=0;
            for (Booking  booking1:mBookingList){
                DateTime dateTime=new DateTime(booking.getStartTime());
                DateTime dateTime1=new DateTime(booking1.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate())){
                    amount+=(int)(booking1.getInvoice().getAmount());
                }
            }
            xyEntry.add(new BarEntry(index++,amount));


            if (maxCost<amount)
            {
                maxCost=amount;
            }
            totalCost +=amount;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = mContext.getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry,mContext.getString(R.string.rupee) );
        barDataSet.setColor(ContextCompat.getColor(mContext, R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mBookingAnalyticsChart.getXAxis().setDrawLabels(true);
        mBookingAnalyticsChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mBookingAnalyticsChart.getXAxis().setGranularityEnabled(true);
        mBookingAnalyticsChart.getXAxis().setLabelCount( noRepeat.size());
        mBookingAnalyticsChart.getXAxis().setLabelRotationAngle(-20f);
        mBookingAnalyticsChart.getXAxis().setGranularity(1f);
        mBookingAnalyticsChart.getXAxis().setTextColor(textColor);
        mBookingAnalyticsChart.getXAxis().setTextSize(8f);
        mBookingAnalyticsChart.getXAxis().setDrawGridLines(false);
        mBookingAnalyticsChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime( noRepeat.get(Math.round(value)).getStartTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MM";

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return mContext.getResources().getString(R.string.not_applicable);
                }
            }
        });

        mBookingAnalyticsChart.getDescription().setTextColor(textColor);
        mBookingAnalyticsChart.getDescription().setEnabled(false);

        mBookingAnalyticsChart.getLegend().setEnabled(true);
        mBookingAnalyticsChart.getLegend().setTextColor(textColor);

        mBookingAnalyticsChart.getAxisLeft().setTextColor(textColor);
        mBookingAnalyticsChart.getAxisRight().setDrawLabels(false);
        mBookingAnalyticsChart.getAxisRight().setAxisMinimum(0);
        mBookingAnalyticsChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mBookingAnalyticsChart.setData(barData);
        mBookingAnalyticsChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);


        mMaxTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(maxCost)));
        mTotalTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(totalCost)));

        int numOfDays = 0;
        DateTime firstDateTime = new DateTime(noRepeat.get(0).getStartTime());
        DateTime lastDateTime = new DateTime(noRepeat.get(noRepeat.size() -1).getStartTime());

        Days days = Days.daysBetween(firstDateTime, lastDateTime);
        numOfDays = days.getDays();

        numOfDays = Math.max(1, numOfDays);

        mAvgTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(totalCost / numOfDays)));

    }

    private void mBookingGraph(ArrayList<Booking> mBookingList) {
        if(mContext == null || mBookingAnalyticsChart == null || mMaxTextView == null ||mTotalTextView == null || mAvgTextView == null || mTripType == null || mMaxType == null || mAvgType == null || mTotalType == null) {
            return;
        }
        mTripType.setText(mContext.getResources().getString(R.string.bookings));
        mMaxType.setText(mContext.getResources().getString(R.string.analytics_max));
        mAvgType.setText(mContext.getResources().getString(R.string.analytics_avg));
        mTotalType.setText(mContext.getResources().getString(R.string.analytics_total));

        ArrayList<BarEntry> xyEntry = new ArrayList<>();
        float maxBooking = 0;
        float totalBooking = 0;


        ArrayList<Booking> allEvents =mBookingList;
        ArrayList<Booking> noRepeat = new ArrayList<Booking>();

        for (Booking event : allEvents) {
            boolean isFound = false;
            for (Booking  b : noRepeat) {
                DateTime dateTime=new DateTime(b.getStartTime());
                DateTime dateTime1=new DateTime(event.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate()) || (b.equals(event))){
                    isFound = true;
                    break;
                }
            }
            if (!isFound) noRepeat.add(event);
        }

        int index=0;

        for (Booking  booking : noRepeat) {
            int amount=0;
            for (Booking  booking1:mBookingList){
                DateTime dateTime=new DateTime(booking.getStartTime());
                DateTime dateTime1=new DateTime(booking1.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate())){
                    amount++;
                }
            }
            xyEntry.add(new BarEntry(index++,amount));


            if (maxBooking<amount)
            {
                maxBooking=amount;
            }
            totalBooking +=amount;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = mContext.getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry,mContext.getString(R.string.bookings) );
        barDataSet.setColor(ContextCompat.getColor(mContext, R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mBookingAnalyticsChart.getXAxis().setDrawLabels(true);
        mBookingAnalyticsChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mBookingAnalyticsChart.getXAxis().setGranularityEnabled(true);
        mBookingAnalyticsChart.getXAxis().setLabelCount(noRepeat.size());
        mBookingAnalyticsChart.getXAxis().setLabelRotationAngle(-20f);
        mBookingAnalyticsChart.getXAxis().setGranularity(1f);
        mBookingAnalyticsChart.getXAxis().setTextColor(textColor);
        mBookingAnalyticsChart.getXAxis().setTextSize(8f);
        mBookingAnalyticsChart.getXAxis().setDrawGridLines(false);
        mBookingAnalyticsChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime(noRepeat.get(Math.round(value)).getStartTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MMM";

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return mContext.getResources().getString(R.string.not_applicable);
                }
            }
        });

        mBookingAnalyticsChart.getDescription().setTextColor(textColor);
        mBookingAnalyticsChart.getDescription().setEnabled(false);

        mBookingAnalyticsChart.getLegend().setEnabled(true);
        mBookingAnalyticsChart.getLegend().setTextColor(textColor);

        mBookingAnalyticsChart.getAxisLeft().setTextColor(textColor);
        mBookingAnalyticsChart.getAxisRight().setDrawLabels(false);
        mBookingAnalyticsChart.getAxisRight().setAxisMinimum(0);
        mBookingAnalyticsChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mBookingAnalyticsChart.setData(barData);
        mBookingAnalyticsChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxTextView.setText(df.format(maxBooking));
        mTotalTextView.setText(df.format(totalBooking));

        int numOfDays = 0;
        DateTime firstDateTime = new DateTime(noRepeat.get(0).getStartTime());
        DateTime lastDateTime = new DateTime(noRepeat.get(noRepeat.size() -1).getStartTime());

        Days days = Days.daysBetween(firstDateTime, lastDateTime);
        numOfDays = days.getDays();

        numOfDays = Math.max(1, numOfDays);

        mAvgTextView.setText(df.format(totalBooking / numOfDays));

    }

    public static boolean isSameDay(Date date1, Date date2) {
        org.joda.time.LocalDate localDate1 = new org.joda.time.LocalDate(date1);
        org.joda.time.LocalDate localDate2 = new org.joda.time.LocalDate(date2);
        return localDate1.equals(localDate2);
    }
}
