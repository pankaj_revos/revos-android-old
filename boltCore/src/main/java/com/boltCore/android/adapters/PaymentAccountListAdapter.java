package com.boltCore.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.activities.BoltPaymentAccountDetailsActivity;
import com.boltCore.android.activities.BoltPaymentAccountListActivity;
import com.boltCore.android.jsonStructures.PaymentAccount;
import com.google.gson.Gson;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.boltCore.android.activities.BoltPaymentAccountListActivity.SELECT_MODE;

public class PaymentAccountListAdapter extends RecyclerView.Adapter<PaymentAccountListAdapter.ViewHolder> {

    private List<PaymentAccount> mPaymentAccountList;
    private Context mContext;
    private String mMode;

    public PaymentAccountListAdapter(List<PaymentAccount> paymentAccountList, Context context, String mode) {
        mPaymentAccountList = paymentAccountList;
        mContext = context;
        mMode = mode;
    }

    @NonNull
    @Override
    public PaymentAccountListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_payment_account_row_item, viewGroup, false);

        return new PaymentAccountListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView accountNumberTextView, ifscTextView, beneficiaryNameTextView, bankNameTextView, accountNickNameTextView;
        ImageView bankImageView;
        LinearLayout showAccountDetailsLinearLayout, summaryLinearLayout;


        public ViewHolder(@NonNull View view) {

            super(view);

            accountNumberTextView = view.findViewById(R.id.bolt_payment_account_number_text_view);
            ifscTextView = view.findViewById(R.id.bolt_payment_account_ifsc_code_text_view);
            bankNameTextView = view.findViewById(R.id.bolt_payment_account_bank_name_text_view);
            beneficiaryNameTextView = view.findViewById(R.id.bolt_payment_account_beneficiary_name_text_view);
            accountNickNameTextView = view.findViewById(R.id.bolt_payment_account_nick_name_text_view);

            bankImageView = view.findViewById(R.id.bolt_payment_account_bank_image_view);
            showAccountDetailsLinearLayout = view.findViewById(R.id.bolt_payment_account_show_details_layout);
            summaryLinearLayout = view.findViewById(R.id.bolt_payment_account_summary_linear_layout);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentAccountListAdapter.ViewHolder viewHolder, int position) {
        PaymentAccount paymentAccount = mPaymentAccountList.get(position);

        if(paymentAccount == null) {
            return;
        }

        viewHolder.accountNumberTextView.setText(paymentAccount.getBankDetails().getBankAccount());
        viewHolder.ifscTextView.setText(paymentAccount.getBankDetails().getIfsc());
        viewHolder.beneficiaryNameTextView.setText(paymentAccount.getBankDetails().getAccountHolder());
        viewHolder.bankNameTextView.setText(paymentAccount.getBankDetails().getBankName());
        viewHolder.accountNickNameTextView.setText(paymentAccount.getName());


        viewHolder.showAccountDetailsLinearLayout.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, BoltPaymentAccountDetailsActivity.class);
            intent.putExtra(BoltPaymentAccountDetailsActivity.PAYMENT_ACC_JSON_KEY, new Gson().toJson(paymentAccount));
            mContext.startActivity(intent);
        });

        if(mMode.equals(SELECT_MODE)) {
            viewHolder.summaryLinearLayout.setOnClickListener(v -> {
                ((BoltPaymentAccountListActivity)mContext).returnPaymentAccountInResult(paymentAccount);
            });
        }
    }

    @Override
    public int getItemCount() {
        return mPaymentAccountList.size();
    }
}
