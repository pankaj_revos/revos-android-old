package com.boltCore.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.activities.BoltScanToSyncActivity;
import com.boltCore.android.jsonStructures.Earning;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class EarningListAdapter extends RecyclerView.Adapter<EarningListAdapter.ViewHolder> {

    private List<Earning> mEarningList;
    private Context mContext;

    public EarningListAdapter(List<Earning> earningList, Context context) {
        mEarningList = earningList;
        mContext = context;
    }

    @NonNull
    @Override
    public EarningListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_earning_row_item, viewGroup, false);

        return new EarningListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView earningBookingIdTextView, chargerIdTextView, receivedInLabelTextView, receivedInTextView, amountTextView, energyTextView,
                costTextView, dateTextView, durationTextView, paymentClearanceStatusTextView;
        Button syncDataButton;

        LinearLayout paymentClearanceStatusLinearLayout;


        public ViewHolder(@NonNull View view) {
            super(view);

            earningBookingIdTextView = view.findViewById(R.id.bolt_earnings_booking_id_text_view);
            chargerIdTextView = view.findViewById(R.id.bolt_earning_charger_id_text_view);
            receivedInLabelTextView = view.findViewById(R.id.bolt_earning_received_in_label_text_view);
            receivedInTextView = view.findViewById(R.id.bolt_earning_received_in_text_view);
            amountTextView = view.findViewById(R.id.bolt_earning_amount_text_view);
            energyTextView = view.findViewById(R.id.bolt_earning_energy_consumed_text_view);
            costTextView = view.findViewById(R.id.bolt_earning_cost_text_view);
            dateTextView = view.findViewById(R.id.bolt_earning_date_text_view);
            durationTextView = view.findViewById(R.id.bolt_earning_duration_text_view);

            paymentClearanceStatusLinearLayout = view.findViewById(R.id.bolt_earning_payment_status_linear_layout);
            paymentClearanceStatusTextView = view.findViewById(R.id.bolt_earning_payment_status_text_view);

            syncDataButton = view.findViewById(R.id.bolt_earning_sync_button);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull EarningListAdapter.ViewHolder viewHolder, int position) {
        Earning earning = mEarningList.get(position);

        if(earning == null) {
            return;
        }

        if(earning.getId() != null && !earning.getId().isEmpty()) {
            String trimmedBookingId = earning.getId().substring(16);
            viewHolder.earningBookingIdTextView.setText(trimmedBookingId);
        } else {
            viewHolder.earningBookingIdTextView.setText(mContext.getString(R.string.not_available));
        }

        if(earning.getAsset() != null && earning.getAsset().getUID() != null) {
            viewHolder.chargerIdTextView.setText(earning.getAsset().getUID());
        }

        if(earning.getAmount() != null) {
            //set amount earned
            viewHolder.amountTextView.setText(earning.getAmount());

            if(earning.getStartTime() != null && !earning.getStartTime().isEmpty()) {
                DateTime bookingStartDateTime = new DateTime(earning.getStartTime());

                DateTime startingFromDateTime = new DateTime(2021, 1, 1, 0, 0, 0, 0);

                //check if start date is past 31/12/2020
                if(bookingStartDateTime.compareTo(startingFromDateTime) == 0 || bookingStartDateTime.compareTo(startingFromDateTime) > 0) {

                    int amount = Integer.parseInt(earning.getAmount());

                    if(amount > 0) {

                        //set received in details
                        viewHolder.receivedInLabelTextView.setVisibility(View.VISIBLE);
                        viewHolder.receivedInTextView.setVisibility(View.VISIBLE);

                        if(earning.getVendor() != null && earning.getVendor() != null) {
                            viewHolder.receivedInTextView.setText(earning.getVendor().getName());
                        } else if(earning.getUpi() != null) {
                            viewHolder.receivedInTextView.setText(earning.getUpi());
                        } else {
                            viewHolder.receivedInTextView.setText(mContext.getString(R.string.not_available));
                        }

                        //set payment clearance status
                        if(earning.getClearanceStatus() != null) {

                            viewHolder.paymentClearanceStatusLinearLayout.setVisibility(View.VISIBLE);

                            viewHolder.paymentClearanceStatusTextView.setText(earning.getClearanceStatus());

                            if(earning.getClearanceStatus().equals("PENDING")) {
                                viewHolder.paymentClearanceStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.orange_dark));
                            } else if(earning.getClearanceStatus().equals("PAID")) {
                                viewHolder.paymentClearanceStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.revos_green));
                            } else if(earning.getClearanceStatus().equals("COMPLETED")) {
                                viewHolder.paymentClearanceStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.revos_green));
                            }
                        } else {
                            viewHolder.paymentClearanceStatusLinearLayout.setVisibility(View.GONE);
                        }
                    } else {

                        viewHolder.receivedInLabelTextView.setVisibility(View.GONE);
                        viewHolder.receivedInTextView.setVisibility(View.GONE);
                        viewHolder.paymentClearanceStatusLinearLayout.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.receivedInLabelTextView.setVisibility(View.GONE);
                    viewHolder.receivedInTextView.setVisibility(View.GONE);
                    viewHolder.paymentClearanceStatusLinearLayout.setVisibility(View.GONE);
                }
            } else {
                viewHolder.receivedInLabelTextView.setVisibility(View.GONE);
                viewHolder.receivedInTextView.setVisibility(View.GONE);
                viewHolder.paymentClearanceStatusLinearLayout.setVisibility(View.GONE);
            }
        } else {
            viewHolder.receivedInLabelTextView.setVisibility(View.GONE);
            viewHolder.receivedInTextView.setVisibility(View.GONE);
            viewHolder.paymentClearanceStatusLinearLayout.setVisibility(View.GONE);
        }

        if(earning.getLeaseeConsumption() != null) {
            viewHolder.energyTextView.setText(String.format("%.3f", Float.parseFloat(earning.getLeaseeConsumption())) + " kWh");
        } else {
            viewHolder.energyTextView.setText("N/A");
        }

        if(earning.getLeasorCost() != null) {
            viewHolder.costTextView.setText("₹" + String.format("%.2f", Float.parseFloat(earning.getLeasorCost())));
        } else {
            viewHolder.costTextView.setText("N/A");
        }

        if(position == mEarningList.size() - 1 && !earning.getAsset().isSyncComplete()) {
            viewHolder.syncDataButton.setVisibility(View.VISIBLE);
            viewHolder.syncDataButton.setOnClickListener(v -> launchScanToSyncActivity());
        } else {
            viewHolder.syncDataButton.setVisibility(View.GONE);
        }

        //set trip date and time
        String dateFormat = "dd-MMM-yy";
        String timeFormat = "hh:mmaa";

        DateTime tripStartDateTime = new DateTime(earning.getStartTime());
        Date tripStartDate = tripStartDateTime.toDate();

        String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
        String tripStartTime = new SimpleDateFormat(timeFormat).format(tripStartDate);

        //calculate trip duration
        DateTime tripEndDateTime = new DateTime(earning.getEndTime());
        Date tripEndTime = tripEndDateTime.toDate();

        long diff = tripEndTime.getTime() - tripStartDate.getTime();
        long diffMinutes = diff / (60 * 1000);

        viewHolder.dateTextView.setText(tripDate + " | " + tripStartTime);
        viewHolder.durationTextView.setText(generateTimeString((int)diffMinutes));
    }

    private void launchScanToSyncActivity() {
        Intent intent = new Intent(mContext, BoltScanToSyncActivity.class);
        mContext.startActivity(intent);
    }

    private String generateTimeString(int minutes) {
        String hourStr = "";
        String minuteStr = "";

        int hours = minutes / 60;
        if(hours > 0) {
            hourStr = String.valueOf(minutes / 60);
            if(hours > 1) {
                hourStr += " " + mContext.getString(R.string.hourPlural);
            } else {
                hourStr += " " + mContext.getString(R.string.hourSingle);
            }
        }

        minutes = minutes % 60;
        if(minutes > 0) {
            minuteStr = String.valueOf(minutes);
            if(minutes > 1) {
                minuteStr += " " + mContext.getString(R.string.minutePlural);
            } else {
                minuteStr += " " + mContext.getString(R.string.minuteSingle);
            }
        }

        return  hourStr + " " + minuteStr;
    }

    @Override
    public int getItemCount() {
        return mEarningList.size();
    }
}
