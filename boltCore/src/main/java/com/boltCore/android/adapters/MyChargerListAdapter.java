package com.boltCore.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.activities.BoltQrCodeDisplayActivity;
import com.boltCore.android.activities.BoltScanToUpdateActivity;
import com.boltCore.android.activities.BoltMyChargerAnalytics;
import com.boltCore.android.activities.BoltServiceProviderActivity;
import com.boltCore.android.activities.BoltViewChargerDetailsActivity;
import com.boltCore.android.jsonStructures.Charger;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.boltCore.android.activities.BoltQrCodeDisplayActivity.QR_CODE_ARG;
import static com.boltCore.android.activities.BoltServiceProviderActivity.UID_ARG_KEY;
import static com.boltCore.android.constants.Constants.CHARGER_UID_KEY;

public class MyChargerListAdapter extends RecyclerView.Adapter<MyChargerListAdapter.ViewHolder> {

    private List<Charger> mChargerList;
    private Context mContext;
    private final String CHARGER_STATUS_UPDATE_REQUIRED = "UPDATE_REQUIRED";

    public MyChargerListAdapter(List<Charger> chargerList, Context context) {
        mChargerList = chargerList;
        mContext = context;
    }

    @NonNull
    @Override
    public MyChargerListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_my_charger_row_item, viewGroup, false);

        return new MyChargerListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView uidTextView, stationNameTextView, stationAddressTextView, powerAndCurrentTextView;
        ImageView socketTypeImageView;
        LinearLayout showChargerDetailsLinearLayout, showQrCodeLinearLayout ,showAnalyticsLinearLayout, showServiceProviderLinearLayout;


        Button updateChargerButton;

        public ViewHolder(@NonNull View view) {

            super(view);

            uidTextView = view.findViewById(R.id.bolt_my_charger_uid_text_view);
            stationNameTextView = view.findViewById(R.id.bolt_my_charger_recipient_station_name_text_view);
            powerAndCurrentTextView = view.findViewById(R.id.bolt_my_charger_power_and_current_text_view);
            stationAddressTextView = view.findViewById(R.id.bolt_my_charger_recipient_station_address_text_view);

            socketTypeImageView = view.findViewById(R.id.bolt_my_charger_socket_type_image_view);
            showQrCodeLinearLayout = view.findViewById(R.id.bolt_my_charger_show_qr_code_layout);
            showAnalyticsLinearLayout=view.findViewById(R.id.bolt_my_charger_show_analytics_layout);
            showChargerDetailsLinearLayout = view.findViewById(R.id.bolt_my_charger_show_details_layout);
            showServiceProviderLinearLayout = view.findViewById(R.id.bolt_my_charger_service_provider_layout);

            updateChargerButton = view.findViewById(R.id.bolt_my_charger_update_button);
        }
    }

    private void launchScanToUpdateActivity() {
        Intent intent = new Intent(mContext, BoltScanToUpdateActivity.class);
        mContext.startActivity(intent);
    }

    @Override
    public void onBindViewHolder(@NonNull MyChargerListAdapter.ViewHolder viewHolder, int position) {
        Charger charger = mChargerList.get(position);

        if(charger == null) {
            return;
        }

        if(charger.getOtaStatus() != null && charger.getOtaStatus().equals(CHARGER_STATUS_UPDATE_REQUIRED)) {
            viewHolder.updateChargerButton.setVisibility(View.VISIBLE);
            viewHolder.updateChargerButton.setOnClickListener(v -> launchScanToUpdateActivity());
        } else {
            viewHolder.updateChargerButton.setVisibility(View.GONE);
        }

        viewHolder.uidTextView.setText(charger.getUID());
        viewHolder.stationNameTextView.setText(charger.getStationName() == null ? "" : charger.getStationName().trim());
        viewHolder.stationAddressTextView.setText(charger.getAddress() == null ? "" : charger.getAddress().trim());
        viewHolder.powerAndCurrentTextView.setText(charger.getCurrent() + ", " + charger.getPowerRating());

        if(charger.getConnectorType().equals("5A/15A Socket")) {
            viewHolder.socketTypeImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_5_15_amp));
        } else if(charger.getConnectorType().equals("5A Socket")) {
            viewHolder.socketTypeImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_5_amp));
        } else if(charger.getConnectorType().equals("15A Socket")) {
            viewHolder.socketTypeImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_15_amp));
        }

        viewHolder.showQrCodeLinearLayout.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, BoltQrCodeDisplayActivity.class);
            intent.putExtra(QR_CODE_ARG, charger.getUID());
            mContext.startActivity(intent);
        });

        viewHolder.showChargerDetailsLinearLayout.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, BoltViewChargerDetailsActivity.class);
            intent.putExtra(CHARGER_UID_KEY, charger.getUID());
            mContext.startActivity(intent);
        });

        viewHolder.showAnalyticsLinearLayout.setOnClickListener(v -> {
            Intent intent = new Intent(mContext,  BoltMyChargerAnalytics.class);
            intent.putExtra(CHARGER_UID_KEY, charger.getUID());
            mContext.startActivity(intent);
        });

        viewHolder.showServiceProviderLinearLayout.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, BoltServiceProviderActivity.class);
            intent.putExtra(UID_ARG_KEY, charger.getUID());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mChargerList.size();
    }
}
