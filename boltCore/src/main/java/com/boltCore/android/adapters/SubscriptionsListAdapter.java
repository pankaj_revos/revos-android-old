package com.boltCore.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.activities.BoltSubscriptionsActivity;
import com.boltCore.android.activities.GenericMessageActivity;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Subscription;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.joda.time.DateTime;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class SubscriptionsListAdapter extends RecyclerView.Adapter<SubscriptionsListAdapter.ViewHolder> {

    private List<Subscription> mSubscriptionList;
    private Context mContext;

    public SubscriptionsListAdapter(List<Subscription> subscriptionList, Context context) {
        mContext = context;
        mSubscriptionList = subscriptionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_subscriptions_list_row_item, viewGroup, false);

        return new SubscriptionsListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView subscriptionNameTextView, subscriptionDescriptionTextView, subscriptionTypeTextView, subscriptionValidityTextView;


        public ViewHolder(@NonNull View view) {
            super(view);

            subscriptionNameTextView = view.findViewById(R.id.bolt_subscription_name_text_view);
            subscriptionDescriptionTextView = view.findViewById(R.id.bolt_subscription_description_text_view);
            subscriptionTypeTextView = view.findViewById(R.id.bolt_subscription_type_text_view);
            subscriptionValidityTextView = view.findViewById(R.id.bolt_subscription_valid_till_text_view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        Subscription subscription = mSubscriptionList.get(position);

        if(subscription == null) {
            return;
        }

        if(subscription.getSubscriptionPolicy() != null) {
            if(subscription.getSubscriptionPolicy().getName() != null && !subscription.getSubscriptionPolicy().getName().isEmpty()) {
                viewHolder.subscriptionNameTextView.setText(subscription.getSubscriptionPolicy().getName());
            }

            if(subscription.getSubscriptionPolicy().getDescription() != null && !subscription.getSubscriptionPolicy().getDescription().isEmpty()) {
                viewHolder.subscriptionDescriptionTextView.setText(subscription.getSubscriptionPolicy().getDescription());
            }

            if(subscription.getSubscriptionPolicy().getOffers() != null && subscription.getSubscriptionPolicy().getOffers().get(0).getType() != null) {
                viewHolder.subscriptionTypeTextView.setText(subscription.getSubscriptionPolicy().getOffers().get(0).getType());
            }
        } else {
            viewHolder.subscriptionNameTextView.setText(mContext.getString(R.string.not_available));
            viewHolder.subscriptionDescriptionTextView.setText(mContext.getString(R.string.not_available));
            viewHolder.subscriptionTypeTextView.setText(mContext.getString(R.string.not_available));
        }

        if(subscription.getEndTime() != null) {
            try {
                DateTime dateTime = new DateTime(subscription.getEndTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MMM-yyyy";
                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
                viewHolder.subscriptionValidityTextView.setText(tripDate);
            } catch (Exception e) {
                //nothing to do here really
            }
        }
    }

    @Override
    public int getItemCount() {
        return mSubscriptionList.size();
    }

    private void makeAddUserToSubscriptionApiCall(String subscriptionId) {

        String userToken = mContext.getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        ((BoltSubscriptionsActivity)mContext).showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("subscriptionId", subscriptionId);

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.addSubscriptionForUser(BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ((BoltSubscriptionsActivity)mContext).hideProgressDialog();

                try {

                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();

                    if(bodyJsonObject.has("data")) {

                        String bodyStr = bodyJsonObject.get("data").getAsString();

                        Subscription subscription = null;

                        if(bodyJsonObject.get("status").getAsInt() == 200) {
                            subscription = new Gson().fromJson(bodyStr, Subscription.class);
                        }

                        Intent intent = new Intent(mContext, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        if(subscription != null) {
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, mContext.getString(R.string.subscription_applied));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);

                        } else {
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, bodyStr);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        }
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                    } else {
                        ((BoltSubscriptionsActivity)mContext).showServerError();
                    }

                } catch (Exception e) {
                    ((BoltSubscriptionsActivity)mContext).showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ((BoltSubscriptionsActivity)mContext).hideProgressDialog();
                ((BoltSubscriptionsActivity)mContext).showServerError();
            }
        });
    }
}