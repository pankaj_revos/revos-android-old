package com.boltCore.android.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.boltCore.android.R;
import com.boltCore.android.jsonStructures.Earning;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class EarningAnalyticsAdapter extends RecyclerView.Adapter<EarningAnalyticsAdapter.MyBarChatHolder>{

    private List<Earning> mEarningList;
    private Context mContext;
    private TextView mMaxTextView,mTotalTextView,mAvgTextView,mTripType;
    private TextView mMaxType,mTotalType,mAvgType;
    private BarChart mEarningAnalyticsChart;
    private final int ANIMATION_DURATION = 500;
    private int mType;




    public EarningAnalyticsAdapter(List<Earning> earningList,int mType, Context context){
        this.mEarningList = earningList;
        this.mType=mType;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyBarChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bolt_earnings_barchart_view, parent, false);
        return new MyBarChatHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBarChatHolder holder, int position) {
        if(mEarningList == null || mEarningList.isEmpty()) {
            return;
        }

        Earning earning = mEarningList.get(position);

        if(earning == null) {
            return;
        }

        if (mType == 0){
            initializeEarningsGraph((ArrayList<Earning>) mEarningList);
        }else if (mType == 1){
            initializeElectricityExpenseGraph((ArrayList<Earning>) mEarningList);
        }else if (mType == 2){
            initializeBookingGraph((ArrayList<Earning>) mEarningList);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyBarChatHolder extends RecyclerView.ViewHolder {


        public MyBarChatHolder(@NonNull View itemView) {
            super(itemView);
            mEarningAnalyticsChart = (BarChart)itemView.findViewById(R.id.analytics_chart);
            mMaxTextView = (TextView)itemView.findViewById(R.id.analytics_max_distance_text_view);
            mTotalTextView = (TextView)itemView.findViewById(R.id.analytics_total_distance_text_view);
            mAvgTextView = (TextView)itemView.findViewById(R.id.analytics_avg_distance_text_view);
            mTripType = (TextView)itemView.findViewById(R.id.trips_type);
            mMaxType= (TextView)itemView.findViewById(R.id.max_money);
            mTotalType= (TextView)itemView.findViewById(R.id.total_money);
            mAvgType= (TextView)itemView.findViewById(R.id.avg_money);

        }
    }
    private void initializeEarningsGraph(ArrayList<Earning>  mEarningList) {
        if(mContext == null || mEarningAnalyticsChart == null || mMaxTextView == null ||mTotalTextView == null || mAvgTextView == null || mTripType == null || mMaxType == null || mAvgType == null || mTotalType == null) {
            return;
        }

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

         float maxRevenue = 0;
         float totalRevenue = 0;


        ArrayList<Earning> allEvents = mEarningList;
        ArrayList<Earning> noRepeat = new ArrayList<Earning>();

        for (Earning event : allEvents) {
            boolean isFound = false;
            for (Earning e : noRepeat) {
                DateTime dateTime=new DateTime(e.getStartTime());
                DateTime dateTime1=new DateTime(event.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate()) || (e.equals(event))) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) noRepeat.add(event);
        }

        int index=0;

        for (Earning earning : noRepeat) {
            float amount=0;
            for (Earning earning1:mEarningList){
                DateTime dateTime=new DateTime(earning.getStartTime());
                DateTime dateTime1=new DateTime(earning1.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate())){
                    amount +=Float.parseFloat(earning1.getAmount());
                }
            }

            xyEntry.add(new BarEntry(index++,amount));


            if (maxRevenue<amount)
            {
                maxRevenue=amount;
            }
            totalRevenue +=amount;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = mContext.getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry, mContext.getString(R.string.rupee) );
        barDataSet.setColor(ContextCompat.getColor(mContext, R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mEarningAnalyticsChart.getXAxis().setDrawLabels(true);
        mEarningAnalyticsChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mEarningAnalyticsChart.getXAxis().setGranularityEnabled(true);
        mEarningAnalyticsChart.getXAxis().setLabelCount(noRepeat.size());
        mEarningAnalyticsChart.getXAxis().setLabelRotationAngle(-20f);
        mEarningAnalyticsChart.getXAxis().setGranularity(1f);
        mEarningAnalyticsChart.getXAxis().setTextColor(textColor);
        mEarningAnalyticsChart.getXAxis().setTextSize(8f);
        mEarningAnalyticsChart.getXAxis().setDrawGridLines(false);
        mEarningAnalyticsChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                DateTime dateTime = new DateTime(noRepeat.get(Math.round(value)).getStartTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MM";
                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return mContext.getResources().getString(R.string.not_applicable);
                }
            }
        });
        mEarningAnalyticsChart.getDescription().setTextColor(textColor);
        mEarningAnalyticsChart.getDescription().setEnabled(false);
        mEarningAnalyticsChart.getLegend().setEnabled(true);
        mEarningAnalyticsChart.getLegend().setTextColor(textColor);
        mEarningAnalyticsChart.getAxisLeft().setTextColor(textColor);
        mEarningAnalyticsChart.getAxisRight().setDrawLabels(false);
        mEarningAnalyticsChart.getAxisRight().setAxisMinimum(0);
        mEarningAnalyticsChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mEarningAnalyticsChart.setData(barData);
        mEarningAnalyticsChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(maxRevenue)));
        mTotalTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(totalRevenue)));

        int numOfDays = 0;
        DateTime firstDateTime = new DateTime(noRepeat.get(0).getStartTime());
        DateTime lastDateTime = new DateTime(noRepeat.get(noRepeat.size() -1).getStartTime());

        Days days = Days.daysBetween(firstDateTime, lastDateTime);
        numOfDays = days.getDays();

        numOfDays = Math.max(1, numOfDays);

        mAvgTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(totalRevenue / numOfDays)));

    }

    private void initializeElectricityExpenseGraph(ArrayList<Earning>  mEarningList){
        if(mContext == null || mEarningAnalyticsChart == null || mMaxTextView == null ||mTotalTextView == null || mAvgTextView == null || mTripType == null || mMaxType == null || mAvgType == null || mTotalType == null) {
            return;
        }

        mTripType.setText(mContext.getResources().getString(R.string.electricity_expense));
        mMaxType.setText(mContext.getResources().getString(R.string.analytics_max));
        mAvgType.setText(mContext.getResources().getString(R.string.analytics_avg));
        mTotalType.setText(mContext.getResources().getString(R.string.analytics_total));

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        float maxCost = 0;
        float totalCost = 0;


        ArrayList<Earning> allEvents = mEarningList;
        ArrayList<Earning> noRepeat = new ArrayList<Earning>();

        for (Earning event : allEvents) {
            boolean isFound = false;
            for (Earning e : noRepeat) {
                DateTime dateTime=new DateTime(e.getStartTime());
                DateTime dateTime1=new DateTime(event.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate()) || (e.equals(event))) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) noRepeat.add(event);
        }

        int index=0;

        for (Earning earning : noRepeat) {
            float amount=0;
            for (Earning earning1:mEarningList){
                DateTime dateTime=new DateTime(earning.getStartTime());
                DateTime dateTime1=new DateTime(earning1.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate())){
                    if (earning1.getLeasorCost() != null){
                        amount+=Float.parseFloat(earning1.getLeasorCost());
                    }

                }
            }
            xyEntry.add(new BarEntry(index++,amount));
            if (maxCost<amount)
            {
                maxCost=amount;
            }
            totalCost +=amount;


        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = mContext.getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry,mContext.getString(R.string.kwh) );
        barDataSet.setColor(ContextCompat.getColor(mContext, R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mEarningAnalyticsChart.getXAxis().setDrawLabels(true);
        mEarningAnalyticsChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mEarningAnalyticsChart.getXAxis().setGranularityEnabled(true);
        mEarningAnalyticsChart.getXAxis().setLabelCount(noRepeat.size());
        mEarningAnalyticsChart.getXAxis().setLabelRotationAngle(-20f);
        mEarningAnalyticsChart.getXAxis().setGranularity(1f);
        mEarningAnalyticsChart.getXAxis().setTextColor(textColor);
        mEarningAnalyticsChart.getXAxis().setTextSize(8f);
        mEarningAnalyticsChart.getXAxis().setDrawGridLines(false);
        mEarningAnalyticsChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime(noRepeat.get(Math.round(value)).getStartTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MM";

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return mContext.getResources().getString(R.string.not_applicable);
                }
            }
        });

        mEarningAnalyticsChart.getDescription().setTextColor(textColor);
        mEarningAnalyticsChart.getDescription().setEnabled(false);
        mEarningAnalyticsChart.getLegend().setEnabled(true);
        mEarningAnalyticsChart.getLegend().setTextColor(textColor);
        mEarningAnalyticsChart.getAxisLeft().setTextColor(textColor);
        mEarningAnalyticsChart.getAxisRight().setDrawLabels(false);
        mEarningAnalyticsChart.getAxisRight().setAxisMinimum(0);
        mEarningAnalyticsChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mEarningAnalyticsChart.setData(barData);
        mEarningAnalyticsChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("0.0");

        mMaxTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(maxCost)));
        mTotalTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(totalCost)));

        int numOfDays = 0;
        DateTime firstDateTime = new DateTime(noRepeat.get(0).getStartTime());
        DateTime lastDateTime = new DateTime(noRepeat.get(noRepeat.size() -1).getStartTime());

        Days days = Days.daysBetween(firstDateTime, lastDateTime);
        numOfDays = days.getDays();

        numOfDays = Math.max(1, numOfDays);

        mAvgTextView.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee), df.format(totalCost / numOfDays)));

    }

    private void initializeBookingGraph(ArrayList<Earning> mEarningList){
        if(mContext == null || mEarningAnalyticsChart == null || mMaxTextView == null ||mTotalTextView == null || mAvgTextView == null || mTripType == null || mMaxType == null || mAvgType == null || mTotalType == null) {
            return;
        }

        mTripType.setText(mContext.getResources().getString(R.string.bookings));
        mMaxType .setText(mContext.getResources().getString(R.string.analytics_max));
        mAvgType .setText(mContext.getResources().getString(R.string.analytics_avg));
        mTotalType .setText(mContext.getResources().getString(R.string.analytics_total));

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        int maxBooking = 0;
        int totalBooking = 0;


        ArrayList<Earning> allEvents =mEarningList;
        ArrayList<Earning> noRepeat = new ArrayList<Earning>();

        for (Earning event : allEvents) {
            boolean isFound = false;
            for (Earning e : noRepeat) {
                DateTime dateTime=new DateTime(e.getStartTime());
                DateTime dateTime1=new DateTime(event.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate()) || (e.equals(event))) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) noRepeat.add(event);
        }

        int index=0;

        for (Earning earning : noRepeat) {
            int count=0;
            for (Earning earning1:mEarningList){
                DateTime dateTime=new DateTime(earning.getStartTime());
                DateTime dateTime1=new DateTime(earning1.getStartTime());
                if (isSameDay(dateTime.toDate(),dateTime1.toDate())){
                    count++;
                }
            }
            xyEntry.add(new BarEntry(index++,count));


            if (maxBooking<count)
            {
                maxBooking=count;
            }
            totalBooking +=count;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = mContext.getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry,mContext.getString(R.string.bookings) );
        barDataSet.setColor(ContextCompat.getColor(mContext, R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mEarningAnalyticsChart.getXAxis().setDrawLabels(true);
        mEarningAnalyticsChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mEarningAnalyticsChart.getXAxis().setGranularityEnabled(true);
        mEarningAnalyticsChart.getXAxis().setLabelCount(noRepeat.size());
        mEarningAnalyticsChart.getXAxis().setLabelRotationAngle(-20f);
        mEarningAnalyticsChart.getXAxis().setGranularity(1f);
        mEarningAnalyticsChart.getXAxis().setTextColor(textColor);
        mEarningAnalyticsChart.getXAxis().setTextSize(8f);
        mEarningAnalyticsChart.getXAxis().setDrawGridLines(false);
        mEarningAnalyticsChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime(noRepeat.get(Math.round(value)).getStartTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MM";

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return mContext.getResources().getString(R.string.not_applicable);
                }
            }
        });

        mEarningAnalyticsChart.getDescription().setTextColor(textColor);
        mEarningAnalyticsChart.getDescription().setEnabled(false);
        mEarningAnalyticsChart.getLegend().setEnabled(true);
        mEarningAnalyticsChart.getLegend().setTextColor(textColor);
        mEarningAnalyticsChart.getAxisLeft().setTextColor(textColor);
        mEarningAnalyticsChart.getAxisRight().setDrawLabels(false);
        mEarningAnalyticsChart.getAxisRight().setAxisMinimum(0);
        mEarningAnalyticsChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mEarningAnalyticsChart.setData(barData);
        mEarningAnalyticsChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxTextView.setText(df.format(maxBooking));
        mTotalTextView.setText(df.format(totalBooking));

        int numOfDays = 0;
        DateTime firstDateTime = new DateTime(noRepeat.get(0).getStartTime());
        DateTime lastDateTime = new DateTime(noRepeat.get(noRepeat.size() -1).getStartTime());

        Days days = Days.daysBetween(firstDateTime, lastDateTime);
        numOfDays = days.getDays();

        numOfDays = Math.max(1, numOfDays);

        mAvgTextView.setText(df.format(totalBooking / numOfDays));

    }

    public static boolean isSameDay(Date date1, Date date2) {
        org.joda.time.LocalDate localDate1 = new org.joda.time.LocalDate(date1);
        org.joda.time.LocalDate localDate2 = new org.joda.time.LocalDate(date2);
        return localDate1.equals(localDate2);
    }
}
