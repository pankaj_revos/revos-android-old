package com.boltCore.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.boltCore.android.R;
import com.boltCore.android.activities.BoltScanToSyncActivity;
import com.boltCore.android.jsonStructures.Booking;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.ViewHolder> {

    private List<Booking>  mBookingList;
    private Context mContext;

    public BookingListAdapter(List<Booking> mBookingList , Context context) {
        this.mBookingList = mBookingList;
        mContext = context;
    }

    @NonNull
    @Override
    public BookingListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_booking_row_item, viewGroup, false);

        return new BookingListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView bookingIdTextView, chargerIdTextView, recipientNameTextView, energyTextView,
                 dateTextView, durationTextView, amountTextView;

        Button syncDataButton;

        public ViewHolder(@NonNull View view) {

            super(view);

            bookingIdTextView = view.findViewById(R.id.bolt_booking_id_text_view);
            chargerIdTextView = view.findViewById(R.id.bolt_booking_charger_id_text_view);
            recipientNameTextView = view.findViewById(R.id.bolt_booking_recipient_name_text_view);
            energyTextView = view.findViewById(R.id.bolt_booking_energy_text_view);
            durationTextView = view.findViewById(R.id.bolt_booking_duration_text_view);
            dateTextView = view.findViewById(R.id.bolt_booking_date_text_view);
            amountTextView = view.findViewById(R.id.bolt_booking_amount_text_view);
            syncDataButton = view.findViewById(R.id.bolt_booking_sync_button);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull BookingListAdapter.ViewHolder viewHolder, int position) {
        Booking booking = mBookingList.get(position);

        if(booking == null) {
            return;
        }

        String trimmedBookingId = booking.getId().substring(16);
        viewHolder.bookingIdTextView.setText(trimmedBookingId);
        viewHolder.chargerIdTextView.setText(booking.getAsset().getUID());
        viewHolder.recipientNameTextView.setText(booking.getAsset().getContactName());

        if(booking.getLeaseeConsumption() != null) {
            viewHolder.energyTextView.setText(String.format("%.3f", Float.parseFloat(booking.getLeaseeConsumption())) + " kWh");
        } else {
            viewHolder.energyTextView.setText("N/A");
        }

        if(position == mBookingList.size() - 1 && !booking.getAsset().isSyncComplete()) {
            viewHolder.syncDataButton.setVisibility(View.VISIBLE);
            viewHolder.syncDataButton.setOnClickListener(v -> launchScanToSyncActivity());
        } else {
            viewHolder.syncDataButton.setVisibility(View.GONE);
        }

        NumberFormat nf = new DecimalFormat("##.#");
        viewHolder.amountTextView.setText(nf.format(booking.getInvoice().getAmount()));


        //set trip date and time
        String dateFormat = "dd-MMM-yy";
        String timeFormat = "hh:mmaa";

        DateTime tripStartDateTime = new DateTime(booking.getStartTime());
        Date tripStartDate = tripStartDateTime.toDate();

        String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
        String tripStartTime = new SimpleDateFormat(timeFormat).format(tripStartDate);

        //calculate trip duration
        DateTime tripEndDateTime = new DateTime(booking.getEndTime());
        Date tripEndTime = tripEndDateTime.toDate();

        long diff = tripEndTime.getTime() - tripStartDate.getTime();
        long diffMinutes = diff / (60 * 1000);


        viewHolder.dateTextView.setText(tripDate + " | " + tripStartTime);
        viewHolder.durationTextView.setText(generateTimeString((int)diffMinutes));

    }

    private void launchScanToSyncActivity() {
        Intent intent = new Intent(mContext, BoltScanToSyncActivity.class);
        mContext.startActivity(intent);
    }

    private String generateTimeString(int minutes) {
        String hourStr = "";
        String minuteStr = "";

        int hours = minutes / 60;
        if(hours > 0) {
            hourStr = String.valueOf(minutes / 60);
            if(hours > 1) {
                hourStr += " " + mContext.getString(R.string.hourPlural);
            } else {
                hourStr += " " + mContext.getString(R.string.hourSingle);
            }
        }

        minutes = minutes % 60;
        if(minutes > 0) {
            minuteStr = String.valueOf(minutes);
            if(minutes > 1) {
                minuteStr += " " + mContext.getString(R.string.minutePlural);
            } else {
                minuteStr += " " + mContext.getString(R.string.minuteSingle);
            }
        }

        return  hourStr + " " + minuteStr;
    }

    @Override
    public int getItemCount() {
        return mBookingList.size();
    }
}
