package com.boltCore.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.jsonStructures.Order;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PastOrderListAdapter extends RecyclerView.Adapter<PastOrderListAdapter.ViewHolder> {

    private List<Order> mOrderList;
    private Context mContext;

    public PastOrderListAdapter(List<Order> orderList, Context context) {
        mOrderList = orderList;
        mContext = context;
    }

    @NonNull
    @Override
    public PastOrderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_order_row_item, viewGroup, false);

        return new PastOrderListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView orderNumberTextView, orderDateTextView, quantityTextView, statusTextView, addressTextView;

        public ViewHolder(@NonNull View view) {

            super(view);

            orderNumberTextView = view.findViewById(R.id.bolt_order_row_item_order_number_text_view);
            orderDateTextView = view.findViewById(R.id.bolt_order_row_item_date_text_view);
            statusTextView = view.findViewById(R.id.bolt_order_row_item_status_text_view);
            quantityTextView = view.findViewById(R.id.bolt_order_row_item_quantity_text_view);
            addressTextView = view.findViewById(R.id.bolt_order_shipping_address_text_view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PastOrderListAdapter.ViewHolder viewHolder, int position) {
        Order order = mOrderList.get(position);

        if(order == null) {
            return;
        }

        if(order.getNumber() != null) {
            viewHolder.orderNumberTextView.setText(order.getNumber());
        } else {

        }

        //set trip date and time
        String dateFormat = "dd-MMM-yy";
        String timeFormat = "hh:mmaa";

        DateTime tripStartDateTime = new DateTime(order.getCreatedAt());
        Date tripStartDate = tripStartDateTime.toDate();

        String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
        String tripStartTime = new SimpleDateFormat(timeFormat).format(tripStartDate);

        viewHolder.orderDateTextView.setText(tripDate + " | " + tripStartTime);
        viewHolder.quantityTextView.setText(String.valueOf(order.getComponents().get(0).getQuantity()));
        viewHolder.statusTextView.setText(order.getStatus());
        viewHolder.addressTextView.setText(order.getOrderLocation().getAddress());
    }

    @Override
    public int getItemCount() {
        return mOrderList.size();
    }
}
