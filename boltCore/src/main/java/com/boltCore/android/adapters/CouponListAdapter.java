package com.boltCore.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.activities.BoltApplyCouponActivity;
import com.boltCore.android.jsonStructures.Coupon;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.ViewHolder> {

    private List<Coupon> mCouponList;
    private Context mContext;

    public CouponListAdapter(List<Coupon> couponList, Context context) {
        mCouponList = couponList;
        mContext = context;
    }

    @NonNull
    @Override
    public CouponListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bolt_coupon_row_item, viewGroup, false);

        return new CouponListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView couponNameTextView, couponDescriptionTextView;
        Button applyCouponButton;


        public ViewHolder(@NonNull View view) {

            super(view);

            couponNameTextView = view.findViewById(R.id.bolt_coupon_name_text_view);
            couponDescriptionTextView = view.findViewById(R.id.bolt_coupon_description_text_view);
            applyCouponButton = view.findViewById(R.id.bolt_coupon_apply_button);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CouponListAdapter.ViewHolder viewHolder, int position) {
        Coupon coupon = mCouponList.get(position);

        if(coupon == null) {
            return;
        }

        viewHolder.couponNameTextView.setText(coupon.getName());
        viewHolder.couponDescriptionTextView.setText(coupon.getDescription());

        viewHolder.applyCouponButton.setOnClickListener(v -> {
            ((BoltApplyCouponActivity)mContext).returnCouponInResult(coupon);
        });


    }

    @Override
    public int getItemCount() {
        return mCouponList.size();
    }
}
