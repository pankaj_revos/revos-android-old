package com.boltCore.android.jsonStructures;

import java.util.ArrayList;

public class SubscriptionPolicy {
    private String name;
    private String description;
    private ArrayList<SubscriptionOffers> offers;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<SubscriptionOffers> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<SubscriptionOffers> offers) {
        this.offers = offers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
