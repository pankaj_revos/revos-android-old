package com.boltCore.android.jsonStructures;

public class PaymentInfo {
    private String vendorSplit;
    private String cftoken;

    public String getVendorSplit() {
        return vendorSplit;
    }

    public void setVendorSplit(String vendorSplit) {
        this.vendorSplit = vendorSplit;
    }

    public String getCftoken() {
        return cftoken;
    }

    public void setCftoken(String cftoken) {
        this.cftoken = cftoken;
    }
}
