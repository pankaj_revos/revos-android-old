package com.boltCore.android.jsonStructures;

public class Pricing {
    String type;
    float minimumPayableAmount;
    float baseAmount;
    float unitCost;
    String unit;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getMinimumPayableAmount() {
        return minimumPayableAmount;
    }

    public void setMinimumPayableAmount(float minimumPayableAmount) {
        this.minimumPayableAmount = minimumPayableAmount;
    }

    public float getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(float baseAmount) {
        this.baseAmount = baseAmount;
    }

    public float getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
