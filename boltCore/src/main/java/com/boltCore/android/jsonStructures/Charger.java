package com.boltCore.android.jsonStructures;

import java.util.List;

public class Charger {
    private String UID;
    private String ssid;
    private String bssid;
    private String bleMac;
    private String bleName;
    private String key;
    private String type;
    private String connectorType;
    private String current;
    private String voltage;
    private float latitude;
    private float longitude;
    private String upi;
    private String powerRating;
    private String version;
    private String status;
    private String address;
    private String stationName;
    private String lastBookingTime;
    private String availableFrom;
    private String contact;
    private String contactName;
    private PaymentAccount paymentMethod;
    private String usageType;
    private float costPerUnitElectricity;
    private String otaStatus;
    private String expectedFirmware;
    private String syncComplete;
    private String firmware;
    private List<Pricing> pricing;
    private String lastPing;
    private Company company;
    private Subscription userSubscription;

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getBleMac() {
        return bleMac;
    }

    public void setBleMac(String bleMac) {
        this.bleMac = bleMac;
    }

    public String getBleName() {
        return bleName;
    }

    public void setBleName(String bleName) {
        this.bleName = bleName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConnectorType() {
        return connectorType;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getUpi() {
        return upi;
    }

    public void setUpi(String upi) {
        this.upi = upi;
    }

    public String getPowerRating() {
        return powerRating;
    }

    public void setPowerRating(String powerRating) {
        this.powerRating = powerRating;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getLastBookingTime() {
        return lastBookingTime;
    }

    public void setLastBookingTime(String lastBookingTime) {
        this.lastBookingTime = lastBookingTime;
    }

    public String getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        this.availableFrom = availableFrom;
    }

    public List<Pricing> getPricing() {
        return pricing;
    }

    public void setPricing(List<Pricing> pricing) {
        this.pricing = pricing;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public PaymentAccount getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentAccount paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public float getCostPerUnitElectricity() {
        return costPerUnitElectricity;
    }

    public void setCostPerUnitElectricity(float costPerUnitElectricity) {
        this.costPerUnitElectricity = costPerUnitElectricity;
    }

    public String getOtaStatus() {
        return otaStatus;
    }

    public void setOtaStatus(String otaStatus) {
        this.otaStatus = otaStatus;
    }

    public String getExpectedFirmware() {
        return expectedFirmware;
    }

    public void setExpectedFirmware(String expectedFirmware) {
        this.expectedFirmware = expectedFirmware;
    }

    public String getSyncComplete() {
        return syncComplete;
    }

    public void setSyncComplete(String syncComplete) {
        this.syncComplete = syncComplete;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getLastPing() {
        return lastPing;
    }

    public void setLastPing(String lastPing) {
        this.lastPing = lastPing;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Subscription getUserSubscription() {
        return userSubscription;
    }

    public void setUserSubscription(Subscription userSubscription) {
        this.userSubscription = userSubscription;
    }
}
