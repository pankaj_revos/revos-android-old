package com.boltCore.android.jsonStructures;

public class Invoice {
    private float amount;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
