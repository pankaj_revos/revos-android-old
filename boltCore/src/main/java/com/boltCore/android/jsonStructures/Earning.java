package com.boltCore.android.jsonStructures;

public class Earning {

    private String startTime;
    private String endTime;
    private String amount;
    private String leaseeName;
    private String leaseContact;
    private String leasorCost;
    private String leaseeConsumption;
    private Vendor vendor;
    private Asset asset;
    private String upi;
    private String clearanceStatus;
    private String id;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLeaseeName() {
        return leaseeName;
    }

    public void setLeaseeName(String leaseeName) {
        this.leaseeName = leaseeName;
    }

    public String getLeaseContact() {
        return leaseContact;
    }

    public void setLeaseContact(String leaseContact) {
        this.leaseContact = leaseContact;
    }

    public String getLeasorCost() {
        return leasorCost;
    }

    public void setLeasorCost(String leasorCost) {
        this.leasorCost = leasorCost;
    }

    public String getLeaseeConsumption() {
        return leaseeConsumption;
    }

    public void setLeaseeConsumption(String leaseeConsumption) {
        this.leaseeConsumption = leaseeConsumption;
    }


    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public String getUpi() {
        return upi;
    }

    public void setUpi(String upi) {
        this.upi = upi;
    }

    public String getClearanceStatus() {
        return clearanceStatus;
    }

    public void setClearanceStatus(String clearanceStatus) {
        this.clearanceStatus = clearanceStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
