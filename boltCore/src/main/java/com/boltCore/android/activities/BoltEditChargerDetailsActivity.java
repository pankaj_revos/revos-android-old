package com.boltCore.android.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.jsonStructures.PaymentAccount;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import androidx.annotation.Nullable;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.boltCore.android.activities.BoltPaymentAccountListActivity.MODE_KEY;
import static com.boltCore.android.activities.BoltPaymentAccountListActivity.SELECT_MODE;
import static com.boltCore.android.activities.BoltSetChargerLocationActivity.LAT_ARG_KEY;
import static com.boltCore.android.activities.BoltSetChargerLocationActivity.LNG_ARG_KEY;
import static com.boltCore.android.constants.Constants.USAGE_PRIVATE;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_FREE;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_PAID;

public class BoltEditChargerDetailsActivity extends  BoltBaseActivity implements OnMapReadyCallback {

    private ProgressDialog mProgressDialog;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private Charger mCharger;

    private LatLng mLatLng;

    private RadioGroup mUsageTypeRadioGroup;

    private PaymentAccount mSelectedPaymentAccount;

    public final static String CHARGER_JSON_OBJECT_KEY = "chargerJsonObjectKey";

    private final int SET_CHARGER_LOCATION_ACTIVITY_REQUEST_CODE = 100;

    private final int SELECT_PAYMENT_ACC_REQ_CODE = 1001;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(CHARGER_JSON_OBJECT_KEY, null) == null) {
            finish();
            return;
        }

        mCharger = new Gson().fromJson(bundle.getString(CHARGER_JSON_OBJECT_KEY), Charger.class);

        if(mCharger == null) {
            finish();
            return;
        }

        mLatLng = new LatLng(mCharger.getLatitude(), mCharger.getLongitude());

        if(mCharger.getPaymentMethod() != null) {
            mSelectedPaymentAccount = mCharger.getPaymentMethod();
        }

        setContentView(R.layout.bolt_edit_charger_details_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

        setupMapViews(savedInstanceState);

        populateChargerDetails(mCharger);
    }

    private boolean haveAnyChangesBeenMade() {
        if(mLatLng != null && (mLatLng.latitude != mCharger.getLatitude() || mLatLng.longitude != mCharger.getLongitude())) {
            return true;
        }

        String costPerUnitElectricity = ((EditText)findViewById(R.id.bolt_edit_charger_cost_per_unit_electricity_edit_text)).getText().toString();
        if(!costPerUnitElectricity.equals(String.valueOf(mCharger.getCostPerUnitElectricity()))) {
            return true;
        }

        String baseAmount = ((EditText)findViewById(R.id.bolt_edit_charger_base_charge_edit_text)).getText().toString();
        if(mCharger.getPricing() != null && !baseAmount.equals(String.valueOf(mCharger.getPricing().get(0).getBaseAmount()))) {
            return true;
        }

        String chargerPerHour = ((EditText)findViewById(R.id.bolt_edit_charger_charge_per_hour_edit_text)).getText().toString();
        if(mCharger.getPricing() != null && !chargerPerHour.equals(String.valueOf(mCharger.getPricing().get(0).getUnitCost()))) {
            return true;
        }


        String usageType = USAGE_PUBLIC_PAID;

        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_public_free_radio_button) {
            usageType = USAGE_PUBLIC_FREE;
        } else if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_private_radio_button) {
            usageType = USAGE_PRIVATE;
        }


        if(mCharger.getUsageType() != null) {
            if(!mCharger.getUsageType().equals(usageType)) {
                return true;
            } else if(mCharger.getUsageType().equals(USAGE_PUBLIC_PAID)) {
                if(mSelectedPaymentAccount != null && mCharger.getPaymentMethod() != null && !mSelectedPaymentAccount.getId().equals(mCharger.getPaymentMethod().getId())) {
                    return true;
                }
            }
        }


        String contactName = ((EditText)findViewById(R.id.bolt_edit_charger_contact_name_edit_text)).getText().toString().trim();
        if(!contactName.equals(mCharger.getContactName() == null ? "" : mCharger.getContactName().trim())) {
            return true;
        }

        String contact = ((EditText)findViewById(R.id.bolt_edit_charger_contact_edit_text)).getText().toString().trim();
        if(!contact.equals(mCharger.getContact() == null ? "" : mCharger.getContact().trim())) {
            return true;
        }

        String stationName = ((EditText)findViewById(R.id.bolt_edit_charger_station_name_edit_text)).getText().toString().trim();
        if(!stationName.equals(mCharger.getStationName() == null ? "" : mCharger.getStationName().trim())) {
            return true;
        }

        String address = ((EditText)findViewById(R.id.bolt_edit_charger_station_address_edit_text)).getText().toString().trim();
        if(!address.equals(mCharger.getAddress() == null ? "" : mCharger.getAddress().trim())) {
            return true;
        }

        return false;
    }

    private void setupViews() {
        findViewById(R.id.bolt_edit_charger_list_back_button_image_view).setOnClickListener(v -> {
            showConfirmationDialogIfRequired();});

        findViewById(R.id.bolt_edit_charger_submit_button).setOnClickListener(v -> {
            if(mCharger == null) {
                return;
            } else {
                updateChargerDetails(mCharger.getUID());
            }
        });

        findViewById(R.id.bolt_edit_charger_edit_location_image_view).setOnClickListener(v -> {
            Intent intent = new Intent(BoltEditChargerDetailsActivity.this, BoltSetChargerLocationActivity.class);
            startActivityForResult(intent, SET_CHARGER_LOCATION_ACTIVITY_REQUEST_CODE);
        });

        findViewById(R.id.bolt_edit_charger_select_payment_account_button).setOnClickListener(v -> startPaymentAccountListActivityForResult());


        mUsageTypeRadioGroup = findViewById(R.id.bolt_edit_charger_usage_type_radio_group);

        mUsageTypeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if(checkedId != R.id.bolt_edit_charger_public_paid_radio_button) {
                findViewById(R.id.bolt_edit_charger_pricing_info_linear_layout).setVisibility(GONE);
            } else {
                findViewById(R.id.bolt_edit_charger_pricing_info_linear_layout).setVisibility(View.VISIBLE);
            }

            if(checkedId == R.id.bolt_edit_charger_private_radio_button) {
                ((TextView)findViewById(R.id.bolt_edit_charger_contact_header_text_view)).setText(getString(R.string.owner_phone));
                ((TextView)findViewById(R.id.bolt_edit_charger_contact_edit_text)).setHint(getString(R.string.owner_phone));
            } else {
                ((TextView)findViewById(R.id.bolt_edit_charger_contact_header_text_view)).setText(getString(R.string.customer_support_number));
                ((TextView)findViewById(R.id.bolt_edit_charger_contact_edit_text)).setHint(getString(R.string.customer_support_number));
            }
        });
    }

    private void startPaymentAccountListActivityForResult() {
        Intent intent = new Intent(BoltEditChargerDetailsActivity.this, BoltPaymentAccountListActivity.class);
        intent.putExtra(MODE_KEY, SELECT_MODE);
        startActivityForResult(intent, SELECT_PAYMENT_ACC_REQ_CODE);
    }

    private void showConfirmationDialogIfRequired() {

        if(!haveAnyChangesBeenMade()) {
            finish();
        } else {
            showExitDialog();
        }

    }

    private void showExitDialog() {

        final Dialog discardCancelDialog = new Dialog(BoltEditChargerDetailsActivity.this);
        discardCancelDialog.setContentView(R.layout.discard_cancel_dialog);

        discardCancelDialog.findViewById(R.id.discard_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discardCancelDialog.dismiss();
                finish();
            }
        });

        discardCancelDialog.findViewById(R.id.cancel_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discardCancelDialog.dismiss();
            }
        });

        discardCancelDialog.show();
    }


    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) findViewById(R.id.bolt_edit_charger_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();

        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

        placeChargerMarker(mLatLng);
    }

    private void placeChargerMarker(LatLng latLng) {
        if(mGoogleMap == null) {
            return;
        }

        Bitmap chargerBitmap = resizeMapIcons(R.drawable.ic_charging_png_green, convertDipToPixels(32), convertDipToPixels(32));

        mGoogleMap.clear();

        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .anchor(0.5f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(chargerBitmap)));

        //zoom to the marker
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(marker.getPosition())
                .zoom(14)
                .build();

        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }


    private void updateChargerDetails(String chargerUID) {
        if(chargerUID == null || chargerUID.isEmpty()) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String electricityCostPerUnitStr = ((EditText)findViewById(R.id.bolt_edit_charger_cost_per_unit_electricity_edit_text)).getText().toString();
        String stationNameStr = ((EditText)findViewById(R.id.bolt_edit_charger_station_name_edit_text)).getText().toString().trim();
        String addressStr = ((EditText)findViewById(R.id.bolt_edit_charger_station_address_edit_text)).getText().toString().trim();
        String contactStr = ((EditText)findViewById(R.id.bolt_edit_charger_contact_edit_text)).getText().toString().trim();
        String contactNameStr = ((EditText)findViewById(R.id.bolt_edit_charger_contact_name_edit_text)).getText().toString().trim();
        String baseAmountStr = ((EditText) findViewById(R.id.bolt_edit_charger_base_charge_edit_text)).getText().toString().trim();
        String costPerHourStr = ((EditText) findViewById(R.id.bolt_edit_charger_charge_per_hour_edit_text)).getText().toString().trim();


        if(electricityCostPerUnitStr.isEmpty()) {
            Intent intent = new Intent(BoltEditChargerDetailsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_electricity_cost_per_unit));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);
            return;
        }


        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("costPerUnitElectricity", electricityCostPerUnitStr);

        bodyJsonObject.addProperty("stationName", stationNameStr);
        bodyJsonObject.addProperty("address", addressStr);
        bodyJsonObject.addProperty("contact", contactStr);
        bodyJsonObject.addProperty("contactName", contactNameStr);

        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_public_paid_radio_button || mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_public_free_radio_button) {
            String errorMessage = "";
            if(stationNameStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_station_name);
            }

            if(addressStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_station_address);
            }

            if(contactStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_owner_phone_number);
            }

            if(contactNameStr.isEmpty()) {
                errorMessage = getString(R.string.please_enter_owner_name);
            }

            if(!errorMessage.isEmpty()) {
                Intent intent = new Intent(BoltEditChargerDetailsActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, errorMessage);
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                return;
            }
        }


        bodyJsonObject.addProperty("latitude", String.valueOf(mLatLng.latitude));
        bodyJsonObject.addProperty("longitude", String.valueOf(mLatLng.longitude));

        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_public_paid_radio_button) {

            if(baseAmountStr.isEmpty() || costPerHourStr.isEmpty()) {
                Intent intent = new Intent(BoltEditChargerDetailsActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_base_amount_and_cost_per_hour));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                return;
            }

            bodyJsonObject.addProperty("baseAmount", ((EditText) findViewById(R.id.bolt_edit_charger_base_charge_edit_text)).getText().toString());
            bodyJsonObject.addProperty("costPerHour", ((EditText) findViewById(R.id.bolt_edit_charger_charge_per_hour_edit_text)).getText().toString());

            if(mSelectedPaymentAccount != null) {
                bodyJsonObject.addProperty("paymentMethod", mSelectedPaymentAccount.getId());
            } else {
                Intent intent = new Intent(BoltEditChargerDetailsActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_select_payment_account));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                return;
            }
        }

        String usageType = USAGE_PUBLIC_PAID;

        if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_public_free_radio_button) {
            usageType = USAGE_PUBLIC_FREE;
        } else if(mUsageTypeRadioGroup.getCheckedRadioButtonId() == R.id.bolt_edit_charger_private_radio_button) {
            usageType = USAGE_PRIVATE;
        }

        bodyJsonObject.addProperty("usageType" , usageType);

        showProgressDialog();


        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        apiInterface.editCharger(chargerUID, BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200) {

                }
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showSuccessAndFinish();
                            }
                        });
                    } else {
                        showServerErrorAndFinish();
                    }
                } catch (Exception e) {
                    showServerErrorAndFinish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private void populateChargerDetails(Charger charger) {
        ((EditText)findViewById(R.id.bolt_edit_charger_cost_per_unit_electricity_edit_text)).setText(String.valueOf(charger.getCostPerUnitElectricity()));

        if(charger.getPricing() != null) {
            ((EditText) findViewById(R.id.bolt_edit_charger_base_charge_edit_text)).setText(String.valueOf(charger.getPricing().get(0).getBaseAmount()));

            ((EditText) findViewById(R.id.bolt_edit_charger_charge_per_hour_edit_text)).setText(String.valueOf(charger.getPricing().get(0).getUnitCost()));
        }


        if(mCharger.getUsageType() != null) {
            if(mCharger.getUsageType().equals(USAGE_PUBLIC_FREE)) {
                ((RadioButton)findViewById(R.id.bolt_edit_charger_public_free_radio_button)).setChecked(true);
                findViewById(R.id.bolt_edit_charger_pricing_info_linear_layout).setVisibility(GONE);
            }

            if(mCharger.getUsageType().equals(USAGE_PRIVATE)) {
                ((RadioButton)findViewById(R.id.bolt_edit_charger_private_radio_button)).setChecked(true);
                findViewById(R.id.bolt_edit_charger_pricing_info_linear_layout).setVisibility(GONE);
            }
        }

        if(mCharger.getPaymentMethod() != null) {
            ((TextView)findViewById(R.id.bolt_edit_charger_account_name_text_view)).setText(mCharger.getPaymentMethod().getName());
        }

        ((EditText)findViewById(R.id.bolt_edit_charger_contact_name_edit_text)).setText(charger.getContactName() == null ? "" : charger.getContactName().trim());

        ((EditText)findViewById(R.id.bolt_edit_charger_contact_edit_text)).setText(charger.getContact() == null ? "" : charger.getContact().trim());

        ((EditText)findViewById(R.id.bolt_edit_charger_station_name_edit_text)).setText(charger.getStationName() == null ? "" : charger.getStationName().trim());

        ((EditText)findViewById(R.id.bolt_edit_charger_station_address_edit_text)).setText(charger.getAddress() == null ? "" : charger.getAddress().trim());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            if(requestCode == SET_CHARGER_LOCATION_ACTIVITY_REQUEST_CODE) {
                if(data != null && data.hasExtra(LAT_ARG_KEY) && data.hasExtra(LNG_ARG_KEY)) {
                    mLatLng = new LatLng(data.getDoubleExtra(LAT_ARG_KEY, 0), data.getDoubleExtra(LNG_ARG_KEY, 0));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            placeChargerMarker(mLatLng);
                        }
                    });
                }
            } else if(requestCode == SELECT_PAYMENT_ACC_REQ_CODE) {
                if(data == null) {
                    return;
                }

                String jsonStr = data.getStringExtra(BoltPaymentAccountDetailsActivity.PAYMENT_ACC_JSON_KEY);
                mSelectedPaymentAccount = new Gson().fromJson(jsonStr, PaymentAccount.class);

                ((TextView)findViewById(R.id.bolt_edit_charger_account_name_text_view)).setText(mSelectedPaymentAccount.getName());
            }
        }
    }

    private void showServerErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }


    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BoltEditChargerDetailsActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialogIfRequired();
    }
}
