package com.boltCore.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.boltCore.android.R;
import com.boltCore.android.jsonStructures.User;
import com.boltCore.android.utilities.general.PrefUtils;

import androidx.annotation.Nullable;

import static com.boltCore.android.constants.Constants.USER_PAYMENT_TRANSIENT_EMAIL_KEY;
import static com.boltCore.android.constants.Constants.USER_PAYMENT_TRANSIENT_PHONE_KEY;
import static com.boltCore.android.constants.Constants.USER_PREFS;

public class BoltEnterUserPhoneEmailForPaymentActivity extends  BoltBaseActivity {
    public static final String CFTOKEN_KEY = "cftokenKey";
    public static final String ORDER_ID_KEY = "orderIdKey";
    public static final String VENDOR_SPLIT_KEY = "vendorSplitKey";

    private String mCftoken, mOrderId, mVendorSplitString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();


        if(bundle != null) {
            mCftoken = bundle.getString(CFTOKEN_KEY, null);
            mOrderId = bundle.getString(ORDER_ID_KEY, null);
            mVendorSplitString = bundle.getString(VENDOR_SPLIT_KEY, null);
        }

        if(mCftoken == null || mOrderId == null || mVendorSplitString == null) {
            finish();
        }

        setContentView(R.layout.bolt_enter_user_phone_email_for_payment_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

    }

    private void setupViews() {
        //back button
        findViewById(R.id.bolt_eupefp_back_button_image_view).setOnClickListener(v -> {
            finish();
        });

        //done button
        findViewById(R.id.bolt_eupefp_submit_button).setOnClickListener(v -> saveDetailsToLocalPrefsAndFinish());

        //show/hide edit text based on
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        String phone = user.getPhone();
        String email = user.getEmail();

        if(phone != null && !phone.isEmpty()) {
            findViewById(R.id.bolt_eupefp_phone_layout).setVisibility(View.GONE);
        }

        if(email != null && !email.isEmpty()) {
            findViewById(R.id.bolt_eupefp_email_layout).setVisibility(View.GONE);
        }
    }

    private void saveDetailsToLocalPrefsAndFinish() {
        //save to preferences based on what's already present and what's not
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        String phone = user.getPhone();
        String email = user.getEmail();

        if(phone == null || phone.isEmpty()) {
            phone = ((EditText)findViewById(R.id.bolt_eupefp_phone_edit_text)).getText().toString();
            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_PAYMENT_TRANSIENT_PHONE_KEY, phone).apply();
        }

        if(email == null || email.isEmpty()) {
            email = ((EditText)findViewById(R.id.bolt_eupefp_email_edit_text)).getText().toString();
            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_PAYMENT_TRANSIENT_EMAIL_KEY, email).apply();
        }

        Intent resultIntent = new Intent();
        resultIntent.putExtra(CFTOKEN_KEY, mCftoken);
        resultIntent.putExtra(ORDER_ID_KEY, mOrderId);
        resultIntent.putExtra(VENDOR_SPLIT_KEY, mVendorSplitString);
        setResult(RESULT_OK, resultIntent);

        finish();
    }
}
