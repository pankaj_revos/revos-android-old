package com.boltCore.android.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.PaymentAccount;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import androidx.annotation.Nullable;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BoltPaymentAccountDetailsActivity extends  BoltBaseActivity {

    private ProgressDialog mProgressDialog;
    public final static String PAYMENT_ACC_JSON_KEY = "paymentAccJsonKey";
    private PaymentAccount mPaymentAccount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(PAYMENT_ACC_JSON_KEY, null) == null) {
            finish();
            return;
        }

        String jsonStr = bundle.getString(PAYMENT_ACC_JSON_KEY, null);

        mPaymentAccount = new Gson().fromJson(jsonStr, PaymentAccount.class);

        setContentView(R.layout.bolt_payment_account_details_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

        populateViews();

    }

    private void populateViews() {
        if(mPaymentAccount.getName() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_nick_name_text_view)).setText(mPaymentAccount.getName());
        }

        if(mPaymentAccount.getBankDetails().getBankAccount() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_number_text_view)).setText(mPaymentAccount.getBankDetails().getBankAccount());
        }

        if(mPaymentAccount.getBankDetails().getIfsc() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_ifsc_text_view)).setText(mPaymentAccount.getBankDetails().getIfsc());
        }

        if(mPaymentAccount.getBankDetails().getBankName() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_bank_name_text_view)).setText(mPaymentAccount.getBankDetails().getBankName());
        }

        if(mPaymentAccount.getBankDetails().getAccountHolder() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_beneficiary_name_text_view)).setText(mPaymentAccount.getBankDetails().getAccountHolder());
        }

        if(mPaymentAccount.getContactInfo().getPhone() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_phone_text_view)).setText(mPaymentAccount.getContactInfo().getPhone());
        }

        if(mPaymentAccount.getContactInfo().getEmail() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_email_text_view)).setText(mPaymentAccount.getContactInfo().getEmail());
        }

        String addressStr = "";

        if(mPaymentAccount.getContactInfo().getAddress1() != null) {
            addressStr += mPaymentAccount.getContactInfo().getAddress1();
        }

        if(mPaymentAccount.getContactInfo().getAddress2() != null) {
            addressStr += " " + mPaymentAccount.getContactInfo().getAddress2();
        }

        ((TextView)findViewById(R.id.bolt_payment_account_details_address_text_view)).setText(addressStr);

        if(mPaymentAccount.getContactInfo().getPanNo() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_pan_card_text_view)).setText(mPaymentAccount.getContactInfo().getPanNo());
        }

        if(mPaymentAccount.getContactInfo().getAadharNo() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_aadhar_text_view)).setText(mPaymentAccount.getContactInfo().getAadharNo());
        }

        if(mPaymentAccount.getContactInfo().getGstin() != null) {
            ((TextView)findViewById(R.id.bolt_payment_account_details_gst_text_view)).setText(mPaymentAccount.getContactInfo().getGstin());
        }

    }

    private void setupViews() {
        findViewById(R.id.bolt_payment_account_details_back_button_image_view).setOnClickListener(v -> finish());
        findViewById(R.id.bolt_payment_account_details_delete_image_view).setOnClickListener(v -> openConfirmationDialog());
    }

    private void openConfirmationDialog() {
        final Dialog deleteAccountDialog = new Dialog(BoltPaymentAccountDetailsActivity.this);
        deleteAccountDialog.setContentView(R.layout.delete_account_dialog);

        deleteAccountDialog.findViewById(R.id.delete_account_yes_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePaymentMethod();
                deleteAccountDialog.dismiss();
            }
        });

        deleteAccountDialog.findViewById(R.id.delete_account_no_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAccountDialog.dismiss();
            }
        });

        deleteAccountDialog.show();
    }

    private void deletePaymentMethod() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.deletePaymentMethod(mPaymentAccount.getId(), BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("status") && bodyJsonObject.get("status").getAsInt() == 200) {
                        showSuccessAndFinish();
                    } else {
                        showServerError();
                    }
                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }


    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltPaymentAccountDetailsActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
