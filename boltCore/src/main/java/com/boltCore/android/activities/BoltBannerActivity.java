package com.boltCore.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.boltCore.android.R;

import androidx.annotation.Nullable;

public class BoltBannerActivity extends BoltBaseActivity{
    public static String BANNER_TIME_LIMIT = "bt";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if(bundle == null) {
            finish();
            return;
        }

        int bannerTimeInMs = bundle.getInt(BANNER_TIME_LIMIT, 0);

        if(bannerTimeInMs == 0) {
            Intent myIntent = new Intent(BoltBannerActivity.this, BoltMainActivity.class);
            startActivity(myIntent);
            finish();
            return;
        }

        //Launch RevMode after 1500ms have passed
        Handler handler = new Handler(getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent myIntent = new Intent(BoltBannerActivity.this, BoltMainActivity.class);
                startActivity(myIntent);
                finish();

            }
        }, bannerTimeInMs);


        setContentView(R.layout.bolt_banner_activity);

    }
}
