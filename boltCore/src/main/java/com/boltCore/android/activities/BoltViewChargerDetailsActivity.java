package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.jsonStructures.PaymentAccount;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import androidx.annotation.Nullable;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.boltCore.android.activities.BoltEditChargerDetailsActivity.CHARGER_JSON_OBJECT_KEY;
import static com.boltCore.android.constants.Constants.CHARGER_UID_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_FREE;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_PAID;

public class BoltViewChargerDetailsActivity extends  BoltBaseActivity implements OnMapReadyCallback {

    private ProgressDialog mProgressDialog;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private String mChargerUid;
    private Charger mCharger;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(CHARGER_UID_KEY, null) == null) {
            finish();
            return;
        }

        mChargerUid = bundle.getString(CHARGER_UID_KEY, null);

        setContentView(R.layout.bolt_view_charger_details_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

        setupMapViews(savedInstanceState);
    }

    private void setupViews() {
        findViewById(R.id.bolt_view_charger_list_back_button_image_view).setOnClickListener(v -> {finish();});
        findViewById(R.id.bolt_view_charger_edit_charger_image_view).setOnClickListener(v -> {
            if(mCharger == null) {
                return;
            } else {
                Intent intent = new Intent(getApplicationContext(), BoltEditChargerDetailsActivity.class);
                intent.putExtra(CHARGER_JSON_OBJECT_KEY, new Gson().toJson(mCharger));
                startActivity(intent);
            }
        });
    }

    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) findViewById(R.id.bolt_view_charger_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();

        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    private void placeChargerMarker(LatLng latLng) {
        if(mGoogleMap == null) {
            return;
        }

        Bitmap chargerBitmap = resizeMapIcons(R.drawable.ic_charging_png_green, convertDipToPixels(32), convertDipToPixels(32));

        mGoogleMap.clear();

        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .anchor(0.5f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(chargerBitmap)));

        //zoom to the marker
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(marker.getPosition())
                .zoom(14)
                .build();

        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }


    private void fetchChargerDetails(String chargerUID) {
        if(chargerUID == null || chargerUID.isEmpty()) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerById(chargerUID, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showServerErrorAndFinish();
                    return;
                }

                try {
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonObject data = bodyJsonObject.get("data").getAsJsonObject();

                        String dataStr = data.toString();
                        mCharger = new Gson().fromJson(dataStr, Charger.class);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                populateChargerDetails(mCharger);
                            }
                        });
                    } else {
                        showServerErrorAndFinish();
                    }

                } catch (Exception e) {
                    showServerErrorAndFinish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private void populateChargerDetails(Charger charger) {
        String usageType = charger.getUsageType();
        String upi = charger.getUpi();
        PaymentAccount paymentAccount = charger.getPaymentMethod();

        ((TextView)findViewById(R.id.bolt_view_charger_cost_per_unit_electricity_text_view)).setText(String.valueOf(charger.getCostPerUnitElectricity()));

        ((TextView)findViewById(R.id.bolt_view_charger_usage_type_text_view)).setText(String.valueOf(charger.getUsageType()));

        if(charger.getPricing() != null) {

            ((TextView) findViewById(R.id.bolt_view_charger_base_charge_text_view)).setText(String.valueOf(charger.getPricing().get(0).getBaseAmount()));

            ((TextView) findViewById(R.id.bolt_view_charger_charge_per_hour_text_view)).setText(String.valueOf(charger.getPricing().get(0).getUnitCost()));
        }

        if(charger.getUpi() != null) {
            ((TextView) findViewById(R.id.bolt_view_charger_upi_text_view)).setText(charger.getUpi() == null ? "" : charger.getUpi().trim());
        }

        ((TextView)findViewById(R.id.bolt_view_charger_contact_name_text_view)).setText(charger.getContactName() == null ? "" : charger.getContactName().trim());

        ((TextView)findViewById(R.id.bolt_view_charger_contact_text_view)).setText(charger.getContact() == null ? "" : charger.getContact().trim());

        ((TextView)findViewById(R.id.bolt_view_charger_station_name_text_view)).setText(charger.getStationName() == null ? "" : charger.getStationName().trim());

        ((TextView)findViewById(R.id.bolt_view_charger_station_address_text_view)).setText(charger.getAddress() == null ? "" : charger.getAddress().trim());

        if(usageType != null && (usageType.equals(USAGE_PUBLIC_PAID) || usageType.equals(USAGE_PUBLIC_FREE))) {
            ((TextView)findViewById(R.id.bolt_view_charger_contact_header_text_view)).setText(getString(R.string.customer_support_number));
        } else if(upi != null && !upi.isEmpty()) {
            ((TextView)findViewById(R.id.bolt_view_charger_contact_header_text_view)).setText(getString(R.string.customer_support_number));
        } else {
            ((TextView)findViewById(R.id.bolt_view_charger_contact_header_text_view)).setText(getString(R.string.owner_phone));
        }

        if(paymentAccount != null) {
            findViewById(R.id.bolt_view_charger_upi_linear_layout).setVisibility(GONE);
            ((TextView)findViewById(R.id.bolt_view_charger_payment_account_text_view)).setText(paymentAccount.getName());
            findViewById(R.id.bolt_view_charger_cms_fee_message_text_view).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.bolt_view_charger_payment_account_linear_layout).setVisibility(GONE);
        }

        placeChargerMarker(new LatLng(charger.getLatitude(), charger.getLongitude()));

    }


    private void showServerErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }


    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BoltViewChargerDetailsActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();

        fetchChargerDetails(mChargerUid);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
