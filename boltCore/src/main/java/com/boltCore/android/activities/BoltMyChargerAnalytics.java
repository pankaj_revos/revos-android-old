package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.boltCore.android.R;
import com.boltCore.android.adapters.EarningAnalyticsAdapter;
import com.boltCore.android.adapters.EarningListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Earning;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.boltCore.android.constants.Constants.CHARGER_UID_KEY;

public class BoltMyChargerAnalytics extends  BoltBaseActivity implements View.OnClickListener {


    private  ArrayList<String> mDates = new ArrayList<String>();
    private  ArrayList<String> mEarningDates = new ArrayList<String>();
    private ArrayList<Earning> mEarningList = new ArrayList<>();
    private ArrayList<Earning> mEarningAnalytics = new ArrayList<>();
    private ProgressDialog mProgressDialog;
    private RecyclerView mEarningsView,mEarningsList;
    private AlertDialog mTypeDialog;
    private CardView mRevenue,mCost,mBooking;
    private TextView mSelectionType,mRevenueCount,mCostCount,mBookingCount,mElectricityCost;
    private NestedScrollView mScrollView;
    private LinearLayout mEarninghint,mExpensehint,mBookinghint;

    private String mChargerUid;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(CHARGER_UID_KEY, null) == null) {
            finish();
            return;
        }

        mChargerUid = bundle.getString(CHARGER_UID_KEY, null);
        setContentView(R.layout.bolt_earnings_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();
        getDate(7);
        fetchEarnings();
    }


    private void setupViews(){
        mRevenue=(CardView)findViewById(R.id.bolt_earning_income_card_view);
        mCost=(CardView)findViewById(R.id.bolt_earning_expense_card_view);
        mBooking=(CardView)findViewById(R.id.bolt_earning_booking_card_view);
        mSelectionType=(TextView)findViewById(R.id.selection_type);
        mRevenueCount=(TextView)findViewById(R.id.bolt_earning_income_text_view);
        mCostCount=(TextView)findViewById(R.id.bolt_earning_electricity_expense_text_view);
        mBookingCount=(TextView)findViewById(R.id.bolt_earning_booking_text_view);
        mElectricityCost=(TextView)findViewById(R.id.bolt_earning_monetary_expense_text_view);

        mEarninghint=(LinearLayout)findViewById(R.id.bolt_earning_income_hint_layout);
        mExpensehint=(LinearLayout)findViewById(R.id.bolt_earning_expense_hint_layout);
        mBookinghint=(LinearLayout)findViewById(R.id.bolt_earning_booking_hint_layout);

        mScrollView=(NestedScrollView)findViewById(R.id.scrollview);

        mRevenue.setOnClickListener(this);
        mCost.setOnClickListener(this);
        mBooking.setOnClickListener(this);

        findViewById(R.id.bolt_earnings_back_button_image_view).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.bolt_earnings_layout_filter).setOnClickListener(v -> {
            earningsFilter();
        });

    }


    private void setupApiData(ArrayList<Earning> earnings){
        //clear text views first
        mRevenueCount.setText(getString(R.string.rupee) + 0);
        mCostCount.setText("0" + getString(R.string.kwh));
        mBookingCount.setText(0 +" ");
        mElectricityCost.setText(getString(R.string.rupee) + 0);

        Collections.sort(earnings, new Comparator<Earning>() {
            @Override
            public int compare(Earning e1, Earning e2) {
                DateTime dateTime1 = new DateTime(e1.getStartTime());
                DateTime dateTime2 = new DateTime(e2.getStartTime());
                return dateTime2.compareTo(dateTime1);
            }
        });

        for (Earning earning: earnings){
            DateTime dateTime=new DateTime(earning.getStartTime());
            Date tripStartDate = dateTime.toDate();
            String dateFormat = "yyyy-MM-dd";
            String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
            mEarningDates.add(tripDate);

        }
        for (int i=0;i<arraysMatching(mEarningDates,mDates).length;i++){
            if (mChargerUid.equals(earnings.get(i).getAsset().getUID())){
                mEarningAnalytics.add(earnings.get(i));
            }
        }

        if(mEarningAnalytics == null || mEarningAnalytics.isEmpty()) {
            return;
        }

        Collections.reverse(mEarningAnalytics);
        setupData(mEarningAnalytics);
        setupBarGraph(mEarningAnalytics,0);

        ArrayList<Earning> dateSortedEarningList = mEarningAnalytics;
        Collections.sort(mEarningAnalytics, new Comparator<Earning>() {
            @Override
            public int compare(Earning e1, Earning e2) {
                DateTime dateTime1 = new DateTime(e1.getStartTime());
                DateTime dateTime2 = new DateTime(e2.getStartTime());
                return dateTime1.compareTo(dateTime2);
            }
        });

        setupEarningAdapter(dateSortedEarningList);

        mEarninghint.setVisibility(View.GONE);
        mExpensehint.setVisibility(View.VISIBLE);
        mBookinghint.setVisibility(View.VISIBLE);

    }

    private void fetchEarnings() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer" +" "+ userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getEarnings(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();

                Log.e(getApplicationContext().getPackageName(),"Earning====>"+call.request().url().toString());

                if(response.code() != 200 || response.body() == null) {
                    showServerError();
                    return;
                }

                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonArray dataArray = bodyJsonObject.getAsJsonArray("data");


                        for(JsonElement jsonElement : dataArray) {
                            Earning earning = new Gson().fromJson(jsonElement.getAsJsonObject().toString(), Earning.class);
                            mEarningList.add(earning);
                        }
                        if(mEarningList.isEmpty()) {
                            showNoBookingsFound();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setupApiData(mEarningList);
                            }
                        });
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltMyChargerAnalytics.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void earningsFilter(){
        CharSequence[] spinnerValues = {getString(R.string.last_seven_days), getString(R.string.last_two_weeks), getString(R.string.last_month)};

        AlertDialog.Builder builder = new AlertDialog.Builder(BoltMyChargerAnalytics.this);
        builder.setSingleChoiceItems(spinnerValues, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch(item) {
                    case 0:
                        mDates.clear();
                        mEarningDates.clear();
                        mEarningAnalytics.clear();
                        mEarningList.clear();
                        getDate(7);
                        mSelectionType.setText(getString(R.string.last_seven_days));
                        fetchEarnings();

                        break;
                    case 1:
                        mDates.clear();
                        mEarningDates.clear();
                        mEarningAnalytics.clear();
                        mEarningList.clear();
                        getDate(14);
                        mSelectionType.setText(getString(R.string.last_two_weeks));
                        fetchEarnings();
                        break;
                    case 2:
                        mDates.clear();
                        mEarningDates.clear();
                        mEarningAnalytics.clear();
                        mEarningList.clear();
                        getDate(30);
                        mSelectionType.setText(getString(R.string.last_month));
                        fetchEarnings();
                        break;
                }
                mTypeDialog.dismiss();
            }
        });
        mTypeDialog = builder.create();
        mTypeDialog.show();
    }

    public void getDate(int days) {

        for (int i=0;i<days;i++){
            Date date = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, - i);
            Date end = c.getTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            mDates.add(format.format(end));

        }

    }
    public static String [] arraysMatching(ArrayList<String> s1, ArrayList<String> s2) {

        ArrayList<String> storage = new ArrayList<>();
        for ( String i : s1) {
            for (String j : s2) {
                if (j .equals(i)) {
                    storage.add(j);
                    break;
                }
            }
        }

        //alternatively
        String[] returnArray = new String[storage.size()];
        for(int i=0; i< storage.size(); i++) {
            returnArray[i]=storage.get(i);
        }
        return returnArray;
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showNoBookingsFound() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_bookings_found));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void setupData(ArrayList<Earning> mEarningAnalytics){
        int count=0;
        int amount=0;
        float electricityCost=0;
        float value=0;
        for (int i=0;i<mEarningAnalytics.size();i++){
            count++;
            amount+=Integer.parseInt(mEarningAnalytics.get(i).getAmount());
            if (mEarningAnalytics.get(i).getLeasorCost() != null){
                electricityCost+=Float.parseFloat(mEarningAnalytics.get(i).getLeasorCost());
                Log.d(getPackageName(),"money1=====>"+electricityCost);
            }
            if (mEarningAnalytics.get(i).getLeaseeConsumption() != null){
                value+=Float.parseFloat(mEarningAnalytics.get(i).getLeaseeConsumption());
            }
        }
        DecimalFormat df = new DecimalFormat("0.000");
        mRevenueCount.setText(getString(R.string.rupee) + amount);
        mCostCount.setText(df.format(value) + getString(R.string.kwh));
        mBookingCount.setText(count+" ");
        mElectricityCost.setText(getString(R.string.rupee)+electricityCost);
        Log.d(getPackageName(),"money2=====>"+electricityCost);
    }
    private void setupBarGraph(ArrayList<Earning> mEarningAnalytics,int mType){
        if (mType == 0){
            mRevenue.setForeground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.card_outline_background));
            mCost.setForeground(null);
            mBooking.setForeground(null);
        }else if (mType == 1){
            mCost.setForeground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.card_outline_background));
            mRevenue.setForeground(null);
            mBooking.setForeground(null);
        }else if (mType == 2){
            mBooking.setForeground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.card_outline_background));
            mRevenue.setForeground(null);
            mCost.setForeground(null);
        }

        if(mEarningAnalytics.isEmpty()) {
            return;
        }

        mEarningsView=findViewById(R.id.bolt_earnings_bar_chart_recycler_view);
        mEarningsView.setNestedScrollingEnabled(false);
        mEarningsView.setLayoutManager(new LinearLayoutManager(BoltMyChargerAnalytics.this));
        EarningAnalyticsAdapter earningAnalyticsAdapter =new EarningAnalyticsAdapter(mEarningAnalytics,mType, BoltMyChargerAnalytics.this);
        mEarningsView.setAdapter(earningAnalyticsAdapter);
        earningAnalyticsAdapter.notifyDataSetChanged();
    }
    private void setupEarningAdapter(ArrayList<Earning> mEarningAnalytics){
        if(mEarningAnalytics.isEmpty()) {
            return;
        }
        mEarningsList = findViewById(R.id.bolt_earnings_list_recycler_view);
        mEarningsList.setNestedScrollingEnabled(false);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(BoltMyChargerAnalytics.this);
        mLinearLayoutManager.setReverseLayout(true);
        mLinearLayoutManager.setStackFromEnd(true);
        mEarningsList.setLayoutManager(mLinearLayoutManager);
        EarningListAdapter earningListAdapter = new EarningListAdapter(mEarningAnalytics, BoltMyChargerAnalytics.this);
        mEarningsList.setAdapter(earningListAdapter);
        earningListAdapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.bolt_earning_income_card_view) {
            setupBarGraph(mEarningAnalytics,0);
            mExpensehint.setVisibility(View.VISIBLE);
            mBookinghint.setVisibility(View.VISIBLE);
            mEarninghint.setVisibility(View.GONE);
            mScrollView.fullScroll(View.FOCUS_UP);
        } else if (id == R.id.bolt_earning_expense_card_view) {
            setupBarGraph(mEarningAnalytics,1);
            mEarninghint.setVisibility(View.VISIBLE);
            mBookinghint.setVisibility(View.VISIBLE);
            mExpensehint.setVisibility(View.GONE);
            mScrollView.fullScroll(View.FOCUS_UP);
        } else if (id == R.id.bolt_earning_booking_card_view){
            setupBarGraph(mEarningAnalytics,2);
            mEarninghint.setVisibility(View.VISIBLE);
            mExpensehint.setVisibility(View.VISIBLE);
            mBookinghint.setVisibility(View.GONE);
            mScrollView.fullScroll(View.FOCUS_UP);
        }
    }
}
