package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.jsonStructures.Coupon;
import com.boltCore.android.jsonStructures.Pricing;
import com.boltCore.android.jsonStructures.Subscription;
import com.boltCore.android.jsonStructures.SubscriptionOffers;
import com.boltCore.android.jsonStructures.User;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.boltCore.android.utilities.general.PrefUtils;
import com.cashfree.pg.CFPaymentService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.boltCore.android.R;
import com.shreyaspatil.easyupipayment.EasyUpiPayment;
import com.shreyaspatil.easyupipayment.listener.PaymentStatusListener;
import com.shreyaspatil.easyupipayment.model.PaymentApp;
import com.shreyaspatil.easyupipayment.model.TransactionDetails;
import com.shreyaspatil.easyupipayment.model.TransactionStatus;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.boltCore.android.activities.BoltEnterUserPhoneEmailForPaymentActivity.CFTOKEN_KEY;
import static com.boltCore.android.activities.BoltEnterUserPhoneEmailForPaymentActivity.ORDER_ID_KEY;
import static com.boltCore.android.activities.BoltEnterUserPhoneEmailForPaymentActivity.VENDOR_SPLIT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.boltCore.android.constants.Constants.GOOGLE_PAY_PACKAGE_NAME;
import static com.boltCore.android.constants.Constants.PHONE_PAY_PACKAGE_NAME;
import static com.boltCore.android.constants.Constants.USAGE_PUBLIC_FREE;
import static com.boltCore.android.constants.Constants.USER_PAYMENT_TRANSIENT_EMAIL_KEY;
import static com.boltCore.android.constants.Constants.USER_PAYMENT_TRANSIENT_PHONE_KEY;
import static com.boltCore.android.constants.Constants.USER_PREFS;
import static com.cashfree.pg.CFPaymentService.PARAM_APP_ID;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_CURRENCY;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_ID;
import static com.cashfree.pg.CFPaymentService.PARAM_VENDOR_SPLIT;

public class BoltBookChargerActivity extends BoltBaseActivity implements PaymentStatusListener {

    public final static String BOOKING_OBJECT_ARG_KEY = "bookingObjectArgKey";

    private final int ENTER_PHONE_EMAIL_REQ_CODE = 1002;

    private final int APPLY_COUPON_REQ_CODE = 2004;

    private final String ACTIVE = "ACTIVE";
    private final String PENDING_PAYMENT = "PENDING_PAYMENT";
    private final String TXN_STATUS_KEY = "txStatus";
    private final String SUCCESS = "SUCCESS";
    private final String TERMINATED_FAILED_PAYMENT = "TERMINATED_FAILED_PAYMENT";
    private final String DELIMITER = "<RD>";

    private Button mPlusButton, mMinusButton, mTimeValueButton, mPayButton;
    private int mChargingTimeInMinutes;
    private TextView mTotalPayableAmountTextView, mNetworkFeeTextView, mChargingFeeTextView,
                     mCouponDiscountAmountTextView, mSubscriptionDiscountAmountTextView;
    private ProgressDialog mProgressDialog;
    private String mChargerUID;

    private Subscription mChargerSubscription;

    private float mCreditsUsed = 0;

    private final String AVAILABLE = "AVAILABLE";
    private final String INUSE = "INUSE";

    private Charger mCharger;
    private HashSet<String> mOwnedChargerIdHashSet;

    private EasyUpiPayment mEasyUpiPayment;

    private Booking mBooking;

    private Coupon mAppliedCoupon;
    private LinearLayout mCouponDetailLinearLayout;
    private ImageView mCouponApplyRemoveImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null) {
            showInvalidQrCode();
            finish();
            return;
        }

        mChargerUID = bundle.getString(Constants.CHARGER_UID_KEY, null);
        if(mChargerUID == null || mChargerUID.isEmpty()) {
            showInvalidQrCode();
            finish();
            return;
        }

        setContentView(R.layout.bolt_book_charger_activity);
        initialiseAnalytics(getApplicationContext());
        fetchChargerDetails(mChargerUID);
    }

    private void fetchChargerDetails(String chargerUID) {
        if(chargerUID == null || chargerUID.isEmpty()) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerById(chargerUID, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showServerErrorAndFinish();
                    return;
                }

                try {
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonObject data = bodyJsonObject.get("data").getAsJsonObject();

                        String dataStr = data.toString();
                        Charger charger = new Gson().fromJson(dataStr, Charger.class);

                        if(!charger.getStatus().equals(AVAILABLE)) {
                            showChargerNotAvailable();
                            finish();
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mCharger = charger;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            fetchMyChargers();
                                        }
                                    });
                                }
                            });
                        }
                    }

                } catch (Exception e) {
                    showServerErrorAndFinish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private void fetchMyChargers() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getMyChargers(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonArray dataArray = bodyJsonObject.getAsJsonArray("data");
                        mOwnedChargerIdHashSet = new HashSet<>();

                        for(JsonElement jsonElement : dataArray) {
                            Charger charger = new Gson().fromJson(jsonElement.getAsJsonObject().toString(), Charger.class);
                            mOwnedChargerIdHashSet.add(charger.getUID());
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setupViews();
                            }
                        });
                    }
                } catch (Exception e) {
                    showServerErrorAndFinish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private void setupViews() {
        mChargingTimeInMinutes = 90;

        mPlusButton = findViewById(R.id.bolt_book_charger_plus_button);
        mMinusButton = findViewById(R.id.bolt_book_charger_minus_button);
        mTimeValueButton = findViewById(R.id.bolt_book_charger_time_value_button);
        mPayButton = findViewById(R.id.bolt_book_charger_pay_using_upi_button);

        mTotalPayableAmountTextView = findViewById(R.id.bolt_book_charger_amount_due_text_view);
        mChargingFeeTextView = findViewById(R.id.bolt_book_charger_charging_fee_text_view);
        mNetworkFeeTextView = findViewById(R.id.bolt_book_charger_network_fee_text_view);

        mCouponDetailLinearLayout = findViewById(R.id.bolt_book_charger_coupon_detail_linear_layout);
        mCouponApplyRemoveImageView = findViewById(R.id.bolt_book_charger_coupon_apply_remove_image_view);
        mCouponDiscountAmountTextView = findViewById(R.id.bolt_book_charger_coupon_discount_text_view);
        mSubscriptionDiscountAmountTextView = findViewById(R.id.bolt_book_charger_subscription_discount_text_view);


        mCouponDetailLinearLayout.setOnClickListener(v -> {
            if(mAppliedCoupon == null) {
                launchApplyCouponActivity();
            }
        });

        mCouponApplyRemoveImageView.setOnClickListener(v -> {
            if(mAppliedCoupon == null) {
                return;
            }
            removeCoupon();
        });

        //back button
        findViewById(R.id.bolt_book_charger_back_button_image_view).setOnClickListener(v -> {
            finish();
        });


        if(mCharger != null) {
            mTotalPayableAmountTextView.setText(String.valueOf(getPayableAmount()));
            mChargingFeeTextView.setText(getString(R.string.rupee) + String.valueOf(getCost()));
            mNetworkFeeTextView.setText(getString(R.string.rupee) + Math.round(Math.floor(getCost() * 0.1f)));

            if(mCharger.getUserSubscription() != null) {
                findViewById(R.id.bolt_book_charger_subscription_discount_amount_layout).setVisibility(View.VISIBLE);

                mSubscriptionDiscountAmountTextView.setText("-" + getString(R.string.rupee) + getSubscriptionDiscount(getCost()));
            }

            ((TextView)findViewById(R.id.bolt_book_charger_recipient_upi_id_text_view)).setText(mCharger.getUpi() == null ? "" : mCharger.getUpi().trim());
            ((TextView)findViewById(R.id.bolt_book_charger_recipient_name_text_view)).setText(mCharger.getContactName() == null ? "" : mCharger.getContactName().trim());
            ((TextView)findViewById(R.id.bolt_book_charger_station_name_value_text_view)).setText(mCharger.getStationName() == null ? "" : mCharger.getStationName().trim());
            ((TextView)findViewById(R.id.bolt_book_charger_station_address_value_text_view)).setText(mCharger.getAddress() == null ? "" : mCharger.getAddress().trim());
            ((TextView)findViewById(R.id.bolt_book_charger_owner_name_value_text_view)).setText(mCharger.getContactName() == null ? "" : mCharger.getContactName().trim());
            findViewById(R.id.bolt_book_charger_call_owner_image_view).setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+(mCharger.getContact() == null ? "" : mCharger.getContact().trim())));
                startActivity(intent);
            });

        } else {
            //default setup
            mTotalPayableAmountTextView.setText(String.valueOf(mChargingTimeInMinutes));
            mChargingFeeTextView.setText(getString(R.string.rupee) + String.valueOf(mChargingTimeInMinutes));
            mNetworkFeeTextView.setText(getString(R.string.rupee) + Math.round(Math.floor(mChargingTimeInMinutes * 0.1f)));
        }

        mTimeValueButton.setText(generateTimeString(mChargingTimeInMinutes));

        mPlusButton.setOnClickListener(v -> {
            mChargingTimeInMinutes = Math.min(mChargingTimeInMinutes + 15, 720);
            updateRelatedViews();
        });

        mMinusButton.setOnClickListener(v -> {
            mChargingTimeInMinutes = Math.max(mChargingTimeInMinutes - 15, 15);
            updateRelatedViews();
        });


        if(mOwnedChargerIdHashSet.contains(mCharger.getUID()) || (mCharger.getUsageType() != null && mCharger.getUsageType().equals(USAGE_PUBLIC_FREE))) {
            mPayButton.setText(getString(R.string.start_charging));
            findViewById(R.id.bolt_book_charger_bill_details_layout).setVisibility(View.GONE);
            findViewById(R.id.bolt_book_charger_apply_coupon_card_view).setVisibility(View.GONE);
        }

        //hide the upi id textview if payment method is present
        if(mCharger.getPaymentMethod() != null) {
            findViewById(R.id.bolt_book_charger_recipient_upi_id_heading_text_view).setVisibility(View.INVISIBLE);
            findViewById(R.id.bolt_book_charger_recipient_upi_id_text_view).setVisibility(View.INVISIBLE);
        }

        mPayButton.setOnClickListener(v -> {
            checkOwnerAndBookCharger();
        });
    }

    private void updateRelatedViews() {
        mTimeValueButton.setText(generateTimeString(mChargingTimeInMinutes));
        mTotalPayableAmountTextView.setText(String.valueOf(getPayableAmount()));
        mChargingFeeTextView.setText(getString(R.string.rupee) + String.valueOf(getCost()));
        mNetworkFeeTextView.setText(getString(R.string.rupee) + Math.round(Math.floor(getCost() * 0.1f)));

        if(mAppliedCoupon != null) {
            findViewById(R.id.bolt_book_charger_coupon_discount_amount_layout).setVisibility(View.VISIBLE);

            mCouponDiscountAmountTextView.setText("-" + getString(R.string.rupee) + getDiscount(getCost()));
            mCouponApplyRemoveImageView.setImageDrawable(ContextCompat.getDrawable(BoltBookChargerActivity.this, R.drawable.ic_close_circle_bg));
            ((TextView)findViewById(R.id.bolt_book_charger_coupon_name_text_view)).setText(mAppliedCoupon.getName());
            findViewById(R.id.bolt_book_charger_coupon_applied_text_view).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.bolt_book_charger_coupon_discount_amount_layout).setVisibility(View.INVISIBLE);

            mCouponApplyRemoveImageView.setImageDrawable(ContextCompat.getDrawable(BoltBookChargerActivity.this, R.drawable.ic_next));
            ((TextView)findViewById(R.id.bolt_book_charger_coupon_name_text_view)).setText(getString(R.string.apply_coupon));
            findViewById(R.id.bolt_book_charger_coupon_applied_text_view).setVisibility(View.GONE);
        }

        if(mCharger.getUserSubscription() != null) {
            findViewById(R.id.bolt_book_charger_subscription_discount_amount_layout).setVisibility(View.VISIBLE);

            mSubscriptionDiscountAmountTextView.setText("-" + getString(R.string.rupee) + getSubscriptionDiscount(getCost()));
        } else {
            findViewById(R.id.bolt_book_charger_subscription_discount_amount_layout).setVisibility(View.GONE);
        }
    }

    private void removeCoupon() {
        mAppliedCoupon = null;
        updateRelatedViews();
    }

    private void applyCoupon() {
        if(mAppliedCoupon == null) {
            return;
        }
        updateRelatedViews();
    }

    private void launchApplyCouponActivity() {
        Intent intent = new Intent(BoltBookChargerActivity.this, BoltApplyCouponActivity.class);
        startActivityForResult(intent, APPLY_COUPON_REQ_CODE);
    }

    private void checkOwnerAndBookCharger() {
        if(mOwnedChargerIdHashSet.contains(mCharger.getUID())) {
            bookCharger("Booked by owner", ACTIVE);
        } else if(mCharger.getUsageType() != null && mCharger.getUsageType().equals(USAGE_PUBLIC_FREE)){
            bookCharger("PUBLIC_FREE charger booking", ACTIVE);
        } else if(getPayableAmount() == 0){
            bookCharger("FREE charge using coupon or discount", ACTIVE);
        } else {
            checkAndLaunchRequiredPaymentFlow();
        }

    }

    private void checkAndLaunchRequiredPaymentFlow() {
        if(mCharger.getPaymentMethod() != null) {
            startCashfreePaymentSequence();
        } else if(mCharger.getUpi() != null && !mCharger.getUpi().isEmpty()) {
            startUPIActivity();
        } else {
            showNoPaymentMethodFoundError();
        }
    }

    private void startCashfreePaymentSequence() {
        generateLeaseWithPaymentPendingStatus();
    }

    private void generateLeaseWithPaymentPendingStatus() {
        bookCharger("initiating cashFree pending payment booking", PENDING_PAYMENT);
    }

    private void checkForUserPhoneEmailBeforeCashfreePayment(String cftoken, String orderId, String vendorSplitString) {
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        String phone = user.getPhone();
        String email = user.getEmail();

        if(phone == null || phone.isEmpty()) {
            //check if phone is present in transient payment prefs
            phone = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_PAYMENT_TRANSIENT_PHONE_KEY, null);

        }

        if(email == null || email.isEmpty()) {
            //check if email is present in transient payment prefs
            email = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_PAYMENT_TRANSIENT_EMAIL_KEY, null);
        }

        if(phone == null || phone.isEmpty() || email == null || email.isEmpty()) {
            launchEnterUserPhoneEmailForPaymentActivity(cftoken, orderId, vendorSplitString);
            return;
        }

        initiateCashfreePayment(cftoken, orderId, vendorSplitString, phone, email);
    }


    private void initiateCashfreePayment(String cftoken, String orderId, String vendorSplitString, String phone, String email) {

        Map<String, String> params = new HashMap<>();

        phone = phone.replaceFirst("\\+91", "");

        params.put(PARAM_APP_ID, getString(R.string.cf_app_id));
        params.put(PARAM_ORDER_ID, orderId);
        params.put(PARAM_ORDER_CURRENCY, "INR");
        params.put(PARAM_ORDER_AMOUNT, String.valueOf(getPayableAmount() * 1.0f));
        params.put(PARAM_CUSTOMER_PHONE, phone);
        params.put(PARAM_CUSTOMER_EMAIL, email);
        params.put(PARAM_VENDOR_SPLIT, vendorSplitString);

        CFPaymentService.getCFPaymentServiceInstance().doPayment(BoltBookChargerActivity.this, params, cftoken, getString(R.string.cf_stage));
    }

    private void launchEnterUserPhoneEmailForPaymentActivity(String cftoken, String orderId, String vendorSplitString) {

        Intent intent = new Intent(BoltBookChargerActivity.this, BoltEnterUserPhoneEmailForPaymentActivity.class);
        intent.putExtra(CFTOKEN_KEY, cftoken);
        intent.putExtra(ORDER_ID_KEY, orderId);
        intent.putExtra(VENDOR_SPLIT_KEY, vendorSplitString);

        startActivityForResult(intent, ENTER_PHONE_EMAIL_REQ_CODE);
    }

    private void startUPIActivity() {
        String payeeVpa = mCharger.getUpi();
        String payeeName = mCharger.getContactName();
        String transactionId = "revos-txid-" + String.valueOf(System.currentTimeMillis());
        String transactionRefId = "revos-reftxid-"+ String.valueOf(System.currentTimeMillis());
        String description = "BOLT booking payment";
        String amount = String.valueOf(getPayableAmount() * 1.0f);

        if(payeeVpa.isEmpty()) {
            Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payee_upi_not_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(payeeName.isEmpty()) {
            Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payee_name_not_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(amount.isEmpty()) {
            Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_amount_to_be_paid));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        boolean isGooglePayPresent = false;
        boolean isPhonePePresent = false;

        if(isPackageInstalled(GOOGLE_PAY_PACKAGE_NAME)) {
            isGooglePayPresent = true;
        }

        if(isPackageInstalled(PHONE_PAY_PACKAGE_NAME)) {
            isPhonePePresent = true;
        }

        if(!isGooglePayPresent && !isPhonePePresent) {
            Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.install_googlepay_phonepe));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        PaymentApp paymentApp = PaymentApp.GOOGLE_PAY;

        if(isPhonePePresent) {
            paymentApp = PaymentApp.PHONE_PE;
        }

        try {
            // START PAYMENT INITIALIZATION
            mEasyUpiPayment = new EasyUpiPayment.Builder(BoltBookChargerActivity.this)
                    .with(paymentApp)
                    .setPayeeVpa(payeeVpa)
                    .setPayeeName(payeeName)
                    .setTransactionId(transactionId)
                    .setTransactionRefId(transactionRefId)
                    .setDescription(description)
                    .setAmount(amount)
                    .build();

            // Register Listener for Events
            mEasyUpiPayment.setPaymentStatusListener(this);

            showProgressDialog();

            // START PAYMENT
            mEasyUpiPayment.startPayment();

        } catch (Exception e) {
            Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_payee_upi_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            hideProgressDialog();
        }

    }


    private void bookCharger(String remarks, String bookingStatus) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null || mCharger == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        DateTime currentTime = new DateTime();
        DateTime endTime = currentTime.plusMinutes(mChargingTimeInMinutes);

        String startTimeStr = currentTime.toString();
        String endTimeStr = endTime.toString();

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("stage", getString(R.string.cf_stage));
        bodyJsonObject.addProperty("amount", getPayableAmount());
        if(bookingStatus.equals(ACTIVE)) {
            bodyJsonObject.addProperty("amountPaid", getPayableAmount());
        }
        bodyJsonObject.addProperty("remarks", remarks);
        bodyJsonObject.addProperty("startTime", startTimeStr);
        bodyJsonObject.addProperty("endTime", endTimeStr);
        bodyJsonObject.addProperty("creditsUsed", mCreditsUsed);

        if(bookingStatus != null) {
            bodyJsonObject.addProperty("status", bookingStatus);
        }

        if(mAppliedCoupon != null) {
            bodyJsonObject.addProperty("coupon", mAppliedCoupon.getId());
        }

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        apiInterface.bookCharger(mCharger.getUID(), BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showServerErrorAndFinish();
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String bodyStr = response.body().string();
                            JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                            if(bodyJsonObject.has("data")) {
                                JsonObject dataJsonObject = bodyJsonObject.getAsJsonObject("data");
                                mBooking = new Gson().fromJson(dataJsonObject.toString(), Booking.class);

                                if(mBooking.getStatus().equals(ACTIVE)) {
                                    launchBoltChargerControlActivity(dataJsonObject.toString());
                                } else if(mBooking.getStatus().equals(PENDING_PAYMENT)) {
                                    checkForUserPhoneEmailBeforeCashfreePayment(mBooking.getPaymentInfo().getCftoken(), mBooking.getId(), mBooking.getPaymentInfo().getVendorSplit());
                                } else {
                                    showServerErrorAndFinish();
                                }
                            } else {
                                showServerErrorAndFinish();
                            }
                        } catch (Exception e) {
                            showServerErrorAndFinish();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private void updateBooking(String bookingId, String status, String remarks) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("amount", getCost());
        bodyJsonObject.addProperty("amountPaid", getPayableAmount());
        bodyJsonObject.addProperty("status", status);
        bodyJsonObject.addProperty("remarks", remarks);

        if(mAppliedCoupon != null) {
            bodyJsonObject.addProperty("coupon", mAppliedCoupon.getId());
        }


        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.updateBooking(bookingId,BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showServerErrorAndFinish();
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        try {
                            String bodyStr = response.body().string();
                            JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                            if(bodyJsonObject.has("data")) {
                                JsonObject dataJsonObject = bodyJsonObject.getAsJsonObject("data");
                                Booking booking = new Gson().fromJson(dataJsonObject.toString(), Booking.class);

                                if(booking.getStatus().equals(ACTIVE)) {
                                    launchBoltChargerControlActivity(dataJsonObject.toString());
                                } else if(booking.getStatus().equals(TERMINATED_FAILED_PAYMENT)){
                                    hideProgressDialog();
                                    Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_failed));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    showServerErrorAndFinish();
                                }
                            } else {
                                showServerErrorAndFinish();
                            }

                        } catch (Exception e) {
                            showServerErrorAndFinish();
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private void launchBoltChargerControlActivity(String bookingObjectArgKey) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BoltBookChargerActivity.this, BoltChargerControlActivity.class);
                intent.putExtra(BOOKING_OBJECT_ARG_KEY, bookingObjectArgKey);
                startActivity(intent);
                finish();
            }
        });
    }

    private int getCost() {
        if(mCharger.getPricing() == null) {
            return 0;
        }

        Pricing pricing = mCharger.getPricing().get(0);
        float units = mChargingTimeInMinutes / 60f;
        float cost = Math.max(units * pricing.getUnitCost(), pricing.getBaseAmount());

        if(mOwnedChargerIdHashSet.contains(mCharger.getUID()) || (mCharger.getUsageType() != null && mCharger.getUsageType().equals(USAGE_PUBLIC_FREE))) {
            return 0;
        } else {
            return Math.round(cost);
        }
    }

    private int getPayableAmount() {
        if(mOwnedChargerIdHashSet.contains(mCharger.getUID()) || (mCharger.getUsageType() != null && mCharger.getUsageType().equals(USAGE_PUBLIC_FREE))) {
            return 0;
        } else {
            return Math.max(0, Math.round(getCost() - getDiscount(getCost()) - getSubscriptionDiscount(getCost())));
        }
    }

    private float getDiscount(float cost) {
        float finalDiscount = 0;

        if(mAppliedCoupon != null) {
            float discountViaPercentage = Integer.MAX_VALUE;
            if(mAppliedCoupon.getDiscountPercent() > 0) {
                discountViaPercentage = cost * mAppliedCoupon.getDiscountPercent() * 0.01f;
            }

            float flatDiscount = Integer.MAX_VALUE;
            if(mAppliedCoupon.getDiscountAmount() > 0) {
                flatDiscount = mAppliedCoupon.getDiscountAmount();
            }


            if(discountViaPercentage < Integer.MAX_VALUE || flatDiscount < Integer.MAX_VALUE) {
                finalDiscount = Math.min(discountViaPercentage, flatDiscount);
            }
        }

        float discount = Math.min(finalDiscount, cost);

        return (float) (Math.round(discount * 100.0) / 100.0);
    }

    private float getSubscriptionDiscount(float cost) {
        float finalSubscriptionDiscount = 0;

        if(mCharger.getUserSubscription() != null) {
            ArrayList<SubscriptionOffers> offersList = mCharger.getUserSubscription().getSubscriptionPolicy().getOffers();


            if(offersList != null && offersList.size() != 0) {
                SubscriptionOffers offer = offersList.get(0);

                if(offer.getType().equals("DISCOUNT")) {

                    float discountViaPercentage = Integer.MAX_VALUE;
                    if(offer.getValue() > 0) {
                        discountViaPercentage = cost * offer.getValue() * 0.01f;

                        float discount = Math.min(discountViaPercentage, cost);

                        finalSubscriptionDiscount = (float) (Math.round(discount * 100.0) / 100.0);
                    }
                } else if(offer.getType().equals("CREDITS")) {
                    float flatDiscount = Integer.MAX_VALUE;
                    if(offer.getValue() > 0) {

                        float discount = Math.min(flatDiscount, cost);

                        mCreditsUsed = discount;

                        finalSubscriptionDiscount = (float) (Math.round(discount * 100.0) / 100.0);

                    }
                }
            }
        }
        return finalSubscriptionDiscount;
    }

    private String generateTimeString(int minutes) {
        String hourStr = "";
        String minuteStr = "";

        int hours = minutes / 60;
        if(hours > 0) {
            hourStr = String.valueOf(minutes / 60);
            if(hours > 1) {
                 hourStr += " " + getString(R.string.hourPlural);
            } else {
                hourStr += " " + getString(R.string.hourSingle);
            }
        }

        minutes = minutes % 60;
        if(minutes > 0) {
            minuteStr = String.valueOf(minutes);
            if(minutes > 1) {
                minuteStr += " " + getString(R.string.minutePlural);
            } else {
                minuteStr += " " + getString(R.string.minuteSingle);
            }
        }

        return  hourStr + " " + minuteStr;
    }

    private void showInvalidQrCode() {
        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_qr_code));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showChargerNotAvailable() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.charger_not_available));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showServerErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.coupon_applied_succesfully));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showNoPaymentMethodFoundError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_payment_option_found));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private boolean isPackageInstalled(String packageName) {
        PackageManager packageManager = getPackageManager();
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BoltBookChargerActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }

    @Override
    public void onTransactionCompleted(TransactionDetails transactionDetails) {
        // Transaction Completed
        Timber.d("TransactionDetails" + transactionDetails.toString());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                if(transactionDetails.getTransactionStatus() == TransactionStatus.SUCCESS) {
                    bookCharger("direct upi payment" + DELIMITER + transactionDetails.toString(), ACTIVE);
                } else {
                    Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_failed));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onTransactionCancelled() {
        hideProgressDialog();
        Intent intent = new Intent(BoltBookChargerActivity.this, GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_failed));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected  void  onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CFPaymentService.REQ_CODE) {
            //Same request code for all payment APIs.
            Timber.d("ReqCode : " + CFPaymentService.REQ_CODE);
            Timber.d("API Response : ");
            //Prints all extras. Replace with app logic.
            if (data != null) {
                Bundle bundle = data.getExtras();
                boolean txnSuccessful = false;
                if (bundle != null) {
                    for (String key : bundle.keySet()) {
                        if (bundle.getString(key) != null) {
                            Timber.d(key + " : " + bundle.getString(key));

                            if (key.equals(TXN_STATUS_KEY)) {
                                String status = bundle.getString(TXN_STATUS_KEY);

                                if (status != null && status.equals(SUCCESS) && !txnSuccessful) {
                                    txnSuccessful = true;
                                } else {
                                    txnSuccessful = false;
                                }
                            }
                        }
                    }

                    if (txnSuccessful) {
                        updateBooking(mBooking.getId(), ACTIVE, "cashfree payment success" + DELIMITER + bundle.toString() + DELIMITER + "amountPaid=" + getPayableAmount());
                    } else {
                        updateBooking(mBooking.getId(), TERMINATED_FAILED_PAYMENT, "cashfree payment failed" + DELIMITER + bundle.toString());
                    }
                } else {
                    updateBooking(mBooking.getId(), TERMINATED_FAILED_PAYMENT, "bundle null");
                }
            }
        } else if(requestCode == ENTER_PHONE_EMAIL_REQ_CODE) {
            if(resultCode == RESULT_OK) {
                if(data != null && data.getExtras() != null) {
                    Bundle bundle = data.getExtras();

                    String cftoken = bundle.getString(CFTOKEN_KEY, null);
                    String orderId = bundle.getString(ORDER_ID_KEY, null);
                    String vendorSplitString = bundle.getString(VENDOR_SPLIT_KEY, null);


                    if(cftoken != null && orderId != null && vendorSplitString != null) {
                        checkForUserPhoneEmailBeforeCashfreePayment(cftoken, orderId, vendorSplitString);
                    }
                }
            }
        } else if(requestCode == APPLY_COUPON_REQ_CODE) {
            if(resultCode == RESULT_OK) {
                if(data != null && data.getExtras() != null) {
                    Bundle bundle = data.getExtras();

                    String couponJsonKey = bundle.getString(BoltApplyCouponActivity.COUPON_JSON_KEY, null);


                    if(couponJsonKey != null && !couponJsonKey.isEmpty()) {
                        try {
                            mAppliedCoupon = new Gson().fromJson(couponJsonKey, Coupon.class);
                            showProgressDialog();
                            applyCoupon();
                            showSuccess();
                        } catch (Exception e) {
                            showErrorAndFinish();
                        }
                    }
                }
            }
        }
    }
}
