package com.boltCore.android.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.boltCore.android.R;

import net.glxn.qrgen.android.QRCode;

import androidx.annotation.Nullable;

public class BoltQrCodeDisplayActivity extends  BoltBaseActivity {
    public final static String QR_CODE_ARG = "qrCodeArg";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getExtras() == null) {
            finish();
            return;
        }

        String qrCodeString = getIntent().getExtras().getString(QR_CODE_ARG, null);
        if(qrCodeString == null || qrCodeString.isEmpty()) {
            finish();
            return;
        }

        setContentView(R.layout.bolt_qr_code_display_activity);
        initialiseAnalytics(getApplicationContext());
        findViewById(R.id.bolt_qr_code_display_back_button_image_view).setOnClickListener(v -> {
            finish();
        });

        Bitmap myBitmap = QRCode.from(qrCodeString).bitmap();
        ImageView myImage = (ImageView) findViewById(R.id.bolt_qr_code_display_image_view);
        myImage.setImageBitmap(myBitmap);

        ((TextView)findViewById(R.id.bolt_qr_code_display_charger_uid_text_view)).setText(String.format(getString(R.string.charger_uid_dynamic), qrCodeString));
    }
}
