package com.boltCore.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.TextView;

import com.boltCore.android.R;

import androidx.annotation.Nullable;

public class BoltWifiHelp extends  BoltBaseActivity {
    public static final String SSID_KEY = "ssidKey";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        String ssid = null;

        if(bundle != null) {
            ssid = bundle.getString(SSID_KEY, null);
        }

        setContentView(R.layout.bolt_wifi_help);
        initialiseAnalytics(getApplicationContext());
        if(ssid != null) {
            ((TextView)findViewById(R.id.bolt_wifi_help_ssid_text_view)).setText(ssid);
        }

        findViewById(R.id.bolt_wifi_help).setOnClickListener(v -> {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            finish();
        });

        //back button
        findViewById(R.id.bolt_wifi_help_back_button_image_view).setOnClickListener(v -> {
            finish();
        });
    }
}
