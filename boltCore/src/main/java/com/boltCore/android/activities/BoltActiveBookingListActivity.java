package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.boltCore.android.R;
import com.boltCore.android.adapters.ActiveBookingListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BoltActiveBookingListActivity extends BoltBaseActivity {
    private final String ACTIVE = "ACTIVE";
    private final String PENDING_PAYMENT = "PENDING_PAYMENT";
    private final String ENDED = "ENDED";
    private final String TERMINATED = "TERMINATED";
    private  ArrayList<Booking> mProcessedBookings = new ArrayList<>();
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_active_booking_activity);
        initialiseAnalytics(getApplicationContext());
        fetchBookings();
    }

    private void fetchBookings() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getUserBookings(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.code() != 200 || response.body() == null) {
                            showServerError();
                            return;
                        }

                        try {

                            String responseBody = response.body().string();

                            JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                            if(responseJsonObject.has("data")) {
                                JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                                String dataStr = data.toString();
                                ArrayList<Booking> bookings = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Booking>>(){}.getType());


                                for(Booking booking : bookings) {
                                    if(booking.getStatus().equals(ACTIVE)) {
                                        mProcessedBookings.add(booking);
                                    }
                                }

                                if(mProcessedBookings.isEmpty()) {
                                    showNoBookingsFound();
                                } else {
                                    refreshActiveBookingList(mProcessedBookings);
                                }
                            }

                        } catch (Exception e) {
                            //do nothing here
                        }

                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void refreshActiveBookingList(ArrayList<Booking> bookings) {


        Collections.sort(bookings, new Comparator<Booking>() {
            @Override
            public int compare(Booking b1, Booking b2) {
                DateTime dateTime1 = new DateTime(b1.getStartTime());
                DateTime dateTime2 = new DateTime(b2.getStartTime());
                return dateTime1.compareTo(dateTime2);
            }
        });

        setupBookingAdapter(bookings);
    }

    private void showNoBookingsFound() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_bookings_found));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltActiveBookingListActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void setupBookingAdapter(ArrayList<Booking> bookings){
        ActiveBookingListAdapter activeBookingListAdapter = new ActiveBookingListAdapter(bookings, BoltActiveBookingListActivity.this);
        RecyclerView activeBookingRecyclerView = findViewById(R.id.bolt_active_booking_list_recycler_view);
        activeBookingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        activeBookingRecyclerView.setAdapter(activeBookingListAdapter);
    }
}