package com.boltCore.android.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.boltCore.android.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import timber.log.Timber;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class BoltSetChargerLocationActivity extends  BoltBaseActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {
    MapView mMapView;
    GoogleMap mGoogleMap = null;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private int REQUEST_PERMISSION_REQ_CODE = 200;
    public static String LAT_ARG_KEY = "latArgKey";
    public static String LNG_ARG_KEY = "lngArgKey";

    private LatLng mLatLng;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_set_charger_location_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

        setupMapViews(savedInstanceState);

        setupLocationClient();
    }

    private void setupViews() {
        findViewById(R.id.bolt_set_charger_location_activity_confirm_button).setOnClickListener(v -> {
            if(mLatLng == null) {
                setResult(RESULT_CANCELED);
                finish();
            } else {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(LAT_ARG_KEY, mLatLng.latitude);
                resultIntent.putExtra(LNG_ARG_KEY, mLatLng.longitude);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

        findViewById(R.id.bolt_set_charger_location_activity_back_button_image_view).setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });
    }

    private void setupLocationClient() {
        mFusedLocationProviderClient = getFusedLocationProviderClient(BoltSetChargerLocationActivity.this);
    }

    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) findViewById(R.id.bolt_set_charger_location_activity_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();
        mGoogleMap.setOnCameraIdleListener(this);

        setupGoogleMaps();


        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BoltSetChargerLocationActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
            return;
        }

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(BoltSetChargerLocationActivity.this, location -> {
            //center map around current location
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(16)
                    .build();

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }

    private void setupGoogleMaps() {

        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BoltSetChargerLocationActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                return;
            }

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        }
    }

    @Override
    public void onCameraIdle() {
        Timber.d("Camera Idle");
        if(mGoogleMap == null) {
            return;
        }

        mLatLng = mGoogleMap.getCameraPosition().target;
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
