package com.boltCore.android.activities;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.analytics.FirebaseAnalytics;

public class BoltBaseActivity extends AppCompatActivity {

    private FirebaseAnalytics  mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public void initialiseAnalytics(Context mContext) {
        Bundle mData = new Bundle();
        mData.putString(FirebaseAnalytics.Param.SCREEN_NAME, String.valueOf(mContext));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW,mData);
    }
}
