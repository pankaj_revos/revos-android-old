package com.boltCore.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.SyncInfoObject;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.boltCore.android.utilities.ble.BoltBleManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class BoltChargerSyncInfoActivity extends  BoltBaseActivity {
    public final static String SYNC_INFO_OBJECT_ARG_KEY = "syncInfoObjectArgKey";


    private Timer mOneSecondTimer;

    private TextView mStatusTextView;
    private LottieAnimationView mWaitLottieView;


    private ProgressDialog mProgressDialog;


    private BoltBleManager mBoltBleManager;

    /**Variables and handler for scanner*/
    private Handler mHandler;
    private BluetoothLeScannerCompat mScanner;
    private static boolean mIsScanning = false;
    private static boolean mDeviceFound = false;
    private final long SCAN_DURATION = 30000;

    private boolean mIsPreviousSessionEnergyConsumedUpdated, mIsCurrentSessionEnergyConsumedUpdated;

    private final String INVALID_CHARGE_ID = "DEFAULTCHARGEID";

    private final String ACTIVE = "ACTIVE";

    private SyncInfoObject mSyncInfoObject;

    private static boolean mForceDisconnect = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_charger_sync_info_activity);
        initialiseAnalytics(getApplicationContext());
        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(SYNC_INFO_OBJECT_ARG_KEY, null) == null) {
            finish();
            return;
        }

        mSyncInfoObject = new Gson().fromJson(bundle.getString(SYNC_INFO_OBJECT_ARG_KEY), SyncInfoObject.class);

        if(mSyncInfoObject == null) {
            finish();
            return;
        }

        setupRelatedBleVariables();

        setupViews();

        setupTimer();

    }

    private void setupRelatedBleVariables() {
        mHandler = new Handler(getMainLooper());
        mBoltBleManager = new BoltBleManager(BoltChargerSyncInfoActivity.this);
        mScanner = BluetoothLeScannerCompat.getScanner();
    }

    private void setupTimer() {
        mOneSecondTimer = new Timer();

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!hasWindowFocus()) {
                            return;
                        }
                        updateViews();
                        connectToDeviceIfRequired();
                        updateCurrentBookingEnergyConsumedIfRequired();
                        updatePreviousBookingEnergyConsumedIfRequired();

                        if(mIsCurrentSessionEnergyConsumedUpdated && mIsPreviousSessionEnergyConsumedUpdated) {
                            showSuccessAndFinish();
                        }
                    }
                });

            }
        }, 100, 1000);
    }


    private void updateCurrentBookingEnergyConsumedIfRequired() {
        String infoString = mBoltBleManager.getCurrentBookingEnergyConsumptionString();

        if(infoString == null || infoString.isEmpty() || mIsCurrentSessionEnergyConsumedUpdated) {
            return;
        }

        String[] tokens = null;
        String bookingId = null;
        String energyConsumedStr = null;
        float energyInKWh;

        try {
            tokens = infoString.split(",");
            bookingId = tokens[0];
            energyConsumedStr = tokens[1];

            float energyInWh = Long.parseLong(energyConsumedStr) / 100.0f;

            energyInKWh = energyInWh / 1000;

            if(bookingId.contains(INVALID_CHARGE_ID)) {
                return;
            }

        } catch (Exception e) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("energyConsumed", String.valueOf(energyInKWh));
        bodyJsonObject.addProperty("syncComplete", true);

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.updateBooking(bookingId, BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    Timber.d("Current booking energyUpdated : " + str);
                    ((TextView)findViewById(R.id.bolt_sync_info_current_server_text_view)).setText("Current booking energyUpdated : " + str);
                    mIsCurrentSessionEnergyConsumedUpdated = true;
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void updatePreviousBookingEnergyConsumedIfRequired() {
        String infoString = mBoltBleManager.getPreviousBookingEnergyConsumptionString();

        if(infoString == null || infoString.isEmpty() || mIsPreviousSessionEnergyConsumedUpdated) {
            return;
        }

        String[] tokens = null;
        String bookingId = null;
        String energyConsumedStr = null;
        float energyInKWh;

        try {
            tokens = infoString.split(",");
            bookingId = tokens[0];
            energyConsumedStr = tokens[1];

            float energyInWh = Long.parseLong(energyConsumedStr) / 100.0f;

            energyInKWh = energyInWh / 1000;

            if(bookingId.contains(INVALID_CHARGE_ID)) {
                return;
            }

        } catch (Exception e) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("energyConsumed", String.valueOf(energyInKWh));
        bodyJsonObject.addProperty("syncComplete", true);

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.updateBooking(bookingId,BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    Timber.d("previous booking energyUpdated : " + str);
                    ((TextView)findViewById(R.id.bolt_sync_info_previous_server_text_view)).setText("Previous booking energyUpdated : " + str);
                    mIsPreviousSessionEnergyConsumedUpdated = true;
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    private void connectToDeviceIfRequired() {
        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {
            return;
        }

        connectToDeviceHelper();
    }

    private void setupViews()  {
        mStatusTextView = findViewById(R.id.bolt_sync_info_status_text_view);
        mWaitLottieView = findViewById(R.id.bolt_sync_info_wait_lottie_view);


        ((TextView)findViewById(R.id.bolt_sync_info_charger_id_text_view)).setText(getString(R.string.charger_id_with_colon)+ " " + mSyncInfoObject.getUID());

    }


    private void updateViews() {
        if(mIsScanning && !mBoltBleManager.isConnected()) {
            mStatusTextView.setText(getString(R.string.searching_for_charger));

            mStatusTextView.setVisibility(View.VISIBLE);
            mWaitLottieView.setVisibility(View.VISIBLE);
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {

            //we're connected to the correct charger, now check if authentication is complete or not
            if(!mBoltBleManager.isAuthenticationComplete()) {
                mStatusTextView.setText(getString(R.string.connecting_to_charger));

                mWaitLottieView.setVisibility(View.VISIBLE);
                mStatusTextView.setVisibility(View.VISIBLE);
            } else {
                mStatusTextView.setText(getString(R.string.syncing_charger_data));
            }


            if(mBoltBleManager.getCurrentBookingEnergyConsumptionString() != null) {
                ((TextView)findViewById(R.id.bolt_sync_info_current_text_view)).setText("Current : " + mBoltBleManager.getCurrentBookingEnergyConsumptionString());
            }

            if(mBoltBleManager.getPreviousBookingEnergyConsumptionString() != null) {
                ((TextView)findViewById(R.id.bolt_sync_info_previous_text_view)).setText("Previous : " + mBoltBleManager.getPreviousBookingEnergyConsumptionString());
            }
        }
    }


    private void connectToDeviceHelper() {
        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {
            return;
        }

        try {
            startScan();
        } catch (Exception e) {
            if(mBoltBleManager != null) {
                mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {

                    }
                }, new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                    }
                });
            }

            finish();
        }
    }


    /**
     * Scan for 5 seconds and then stop scanning when a BluetoothLE device is found then mLEScanCallback
     * is activated This will perform regular scan for custom BLE Service UUID and then filter out.
     * using class ScannerServiceParser
     */
    private synchronized void startScan() {

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            //todo::send message to show a dialog
            return;
        }

        if(mIsScanning || mBoltBleManager.isConnected()) {
            return;
        }

        mDeviceFound = false;
        mIsScanning = true;

        mScanner.startScan(scanCallback);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScanHelper();
                }
            }
        }, SCAN_DURATION);
    }

    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final ScanResult result) {
            Timber.d("Device found : " + result.getDevice().getAddress());
            if(result.getDevice().getAddress().trim().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceFound = true;
                        stopScanHelper();
                        if(mBoltBleManager != null && !mBoltBleManager.isConnected() && !mForceDisconnect) {
                            mBoltBleManager.connectToDevice(mSyncInfoObject.getBleMac().toUpperCase(), mSyncInfoObject.getKey());
                        }
                    }
                });
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };

    private void stopScanHelper() {
        mScanner.stopScan(scanCallback);
        mIsScanning = false;
    }

    @Override
    protected void onDestroy() {
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mForceDisconnect = true;
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mForceDisconnect = true;
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();
        super.onBackPressed();

    }

    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }


    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltChargerSyncInfoActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mForceDisconnect = false;
    }
}
