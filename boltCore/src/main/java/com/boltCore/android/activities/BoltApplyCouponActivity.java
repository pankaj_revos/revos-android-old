package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.boltCore.android.R;
import com.boltCore.android.adapters.CouponListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Coupon;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BoltApplyCouponActivity extends  BoltBaseActivity {
    private ProgressDialog mProgressDialog;
    private RecyclerView mAvailableCouponListRecyclerView;

    public final static String COUPON_JSON_KEY = "coupon_json_key";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_apply_coupon_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

    }

    private void setupViews() {

        mAvailableCouponListRecyclerView = findViewById(R.id.bolt_apply_coupon_list_recycler_view);
        mAvailableCouponListRecyclerView.setLayoutManager(new LinearLayoutManager(BoltApplyCouponActivity.this));

        findViewById(R.id.bolt_apply_coupon_list_back_button_image_view).setOnClickListener(v -> {finish();});
    }

    public void returnCouponInResult(Coupon coupon) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(COUPON_JSON_KEY, new Gson().toJson(coupon));
        setResult(RESULT_OK, resultIntent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        fetchAvailableCoupons();
    }


    private void fetchAvailableCoupons() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getCoupons(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonArray dataArray = bodyJsonObject.getAsJsonArray("data");
                        ArrayList<Coupon> coupons = new ArrayList<>();

                        for(JsonElement jsonElement : dataArray) {
                            Coupon coupon = new Gson().fromJson(jsonElement.getAsJsonObject().toString(), Coupon.class);
                                coupons.add(coupon);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                refreshAvailableCouponList(coupons);
                            }
                        });
                    } else {
                        showServerError();
                    }
                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void refreshAvailableCouponList(ArrayList<Coupon> coupons) {
        CouponListAdapter couponListAdapter = new CouponListAdapter(coupons, BoltApplyCouponActivity.this);
        mAvailableCouponListRecyclerView.setAdapter(couponListAdapter);
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltApplyCouponActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
