package com.boltCore.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.zxing.Result;
import com.boltCore.android.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class BoltScanQrCodeToRegisterChargerActivity extends  BoltBaseActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private final int VALID_QR_CODE_LENGTH = 11;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.CAMERA};
    private int REQUEST_CAMERA_PERMISSION_REQ_CODE = 300;

    private boolean mIsFlashOn;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_scan_qr_code_to_register_charger_activity);
        initialiseAnalytics(getApplicationContext());
        mScannerView = findViewById(R.id.bolt_scan_qr_code_to_register_charger_scanner_view);
    }

    private void checkPermissionAndStartScannerView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(BoltScanQrCodeToRegisterChargerActivity.this, mPermissionsRequired, REQUEST_CAMERA_PERMISSION_REQ_CODE);
                    return;
                }

                updateCameraFlashImageView();

                mScannerView.setResultHandler(BoltScanQrCodeToRegisterChargerActivity.this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        checkPermissionAndStartScannerView();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Timber.v(rawResult.getText()); // Prints scan results
        Timber.v(rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        /*// If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);*/

        String result = rawResult.getText();

        if(result.length() == VALID_QR_CODE_LENGTH && result.contains("BOLT_")) {
            Toast.makeText(this, getString(R.string.data_read_successfully), Toast.LENGTH_LONG).show();
            fetchChargerDetails(result);
        } else {
            showInvalidQrCode();
            checkPermissionAndStartScannerView();
        }
    }

    private void toggleFlash(boolean on) {
        mScannerView.setFlash(on);
    }

    private void updateCameraFlashImageView() {

        mIsFlashOn = mScannerView.getFlash();
        int backGroundId = R.drawable.turn_flash_on_background;
        if(mIsFlashOn) {
            backGroundId = R.drawable.turn_flash_off_background;
        }

        findViewById(R.id.bolt_scan_qr_code_to_register_toggle_flash_image_view).setBackground(ContextCompat.getDrawable(getApplicationContext(), backGroundId));

        findViewById(R.id.bolt_scan_qr_code_to_register_toggle_flash_image_view).setOnClickListener(v -> {
            toggleFlash(!mIsFlashOn);
            updateCameraFlashImageView();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_CAMERA_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            checkPermissionAndStartScannerView();
        }

    }

    private void fetchChargerDetails(String chargerUID) {
        if(chargerUID == null || chargerUID.isEmpty()) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerById(chargerUID, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showServerError();
                    return;
                }

                try {
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonObject data = bodyJsonObject.get("data").getAsJsonObject();

                        String dataStr = data.toString();
                        Charger charger = new Gson().fromJson(dataStr, Charger.class);

                        //this mean charger already exists
                        showChargerUidExistsError();
                    }

                } catch (Exception e) {
                    //this means the charger uid is not registered
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            launchRegisterChargerActivity(chargerUID);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }


    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BoltScanQrCodeToRegisterChargerActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }


    private void launchRegisterChargerActivity(String chargerUid) {
        Intent intent = new Intent(this, BoltRegisterChargerActivity.class);
        intent.putExtra(Constants.CHARGER_UID_KEY, chargerUid);
        startActivity(intent);
        finish();
    }

    private void showChargerUidExistsError() {
        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.charger_uid_exists_error_message));
        bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void showInvalidQrCode() {
        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_qr_code));
        bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}