package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.boltCore.android.R;
import com.boltCore.android.adapters.SubscriptionsListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Subscription;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class BoltSubscriptionsActivity extends BoltBaseActivity {

    private ProgressDialog mProgressDialog;
    private RecyclerView mSubscriptionsListRecyclerView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_subscriptions_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

    }

    private void setupViews() {

        mSubscriptionsListRecyclerView = findViewById(R.id.bolt_subscriptions_list_recycler_view);
        mSubscriptionsListRecyclerView.setLayoutManager(new LinearLayoutManager(BoltSubscriptionsActivity.this));

        findViewById(R.id.bolt_subscriptions_list_back_button_image_view).setOnClickListener(v -> {finish();});
    }


    @Override
    protected void onResume() {
        super.onResume();

        fetchActiveSubscriptions();
    }


    private void fetchActiveSubscriptions() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getActiveSubscriptions(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();
                try {

                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();

                    if(bodyJsonObject.has("data")) {

                        JsonObject dataObject = bodyJsonObject.getAsJsonObject("data");

                        if(dataObject != null && dataObject.size() > 0) {

                            ArrayList<Subscription> subscriptionArrayList = new ArrayList<>();

                            Subscription subscription = new Gson().fromJson(dataObject.toString(), Subscription.class);
                            subscriptionArrayList.add(subscription);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    refreshSubscriptionsList(subscriptionArrayList);
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressDialog();
            }
        });
    }

    public void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void refreshSubscriptionsList(ArrayList<Subscription> subscriptionsList) {
        SubscriptionsListAdapter subscriptionsListAdapter = new SubscriptionsListAdapter(subscriptionsList, BoltSubscriptionsActivity.this);
        mSubscriptionsListRecyclerView.setAdapter(subscriptionsListAdapter);
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltSubscriptionsActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}