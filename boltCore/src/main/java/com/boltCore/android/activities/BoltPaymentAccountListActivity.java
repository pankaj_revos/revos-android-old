package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.adapters.PaymentAccountListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.PaymentAccount;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BoltPaymentAccountListActivity extends  BoltBaseActivity {
    private ProgressDialog mProgressDialog;
    private RecyclerView mPaymentAccountListRecyclerView;

    public static final String MODE_KEY = "modeKey";
    public static final String VIEW_MODE = "viewMode";
    public static final String SELECT_MODE = "selectMode";

    private String mMode = VIEW_MODE;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

        if(bundle == null) {
            mMode = VIEW_MODE;
        } else {
            mMode = bundle.getString(MODE_KEY, VIEW_MODE);
        }

        setContentView(R.layout.bolt_payment_account_list_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

    }

    private void setupViews() {
        findViewById(R.id.bolt_payment_account_list_add_account_image_view).setOnClickListener(v -> {
            Intent intent = new Intent(this, BoltAddPaymentAccountActivity.class);
            startActivity(intent);
        });

        if(mMode.equals(SELECT_MODE)) {
            ((TextView)findViewById(R.id.bolt_payment_account_list_header_text_view)).setText(getString(R.string.tap_to_select_account));
        }

        mPaymentAccountListRecyclerView = findViewById(R.id.bolt_payment_account_list_recycler_view);
        mPaymentAccountListRecyclerView.setLayoutManager(new LinearLayoutManager(BoltPaymentAccountListActivity.this));

        findViewById(R.id.bolt_payment_account_list_back_button_image_view).setOnClickListener(v -> {finish();});
    }

    public void returnPaymentAccountInResult(PaymentAccount paymentAccount) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(BoltPaymentAccountDetailsActivity.PAYMENT_ACC_JSON_KEY, new Gson().toJson(paymentAccount));
        setResult(RESULT_OK, resultIntent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        fetchPaymentMethods();
    }


    private void fetchPaymentMethods() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getPaymentMethod(BoltSDK.getInstance().getApiKey(), bearerToken, getString(R.string.cf_stage)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonArray dataArray = bodyJsonObject.getAsJsonArray("data");
                        ArrayList<PaymentAccount> paymentAccounts = new ArrayList<>();

                        for(JsonElement jsonElement : dataArray) {
                            PaymentAccount paymentAccount = new Gson().fromJson(jsonElement.getAsJsonObject().toString(), PaymentAccount.class);
                            if(!paymentAccount.getStatus().equals("DELETED") && paymentAccount.getStage() != null && paymentAccount.getStage().equals(getString(R.string.cf_stage))) {
                                paymentAccounts.add(paymentAccount);
                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                refreshPaymentAccountList(paymentAccounts);
                            }
                        });
                    } else {
                        showServerError();
                    }
                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void refreshPaymentAccountList(ArrayList<PaymentAccount> paymentAccounts) {
        PaymentAccountListAdapter paymentAccountListAdapter = new PaymentAccountListAdapter(paymentAccounts, BoltPaymentAccountListActivity.this, mMode);
        mPaymentAccountListRecyclerView.setAdapter(paymentAccountListAdapter);
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltPaymentAccountListActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
