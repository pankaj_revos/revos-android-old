package com.boltCore.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Product;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.shreyaspatil.easyupipayment.EasyUpiPayment;
import com.shreyaspatil.easyupipayment.listener.PaymentStatusListener;
import com.shreyaspatil.easyupipayment.model.PaymentApp;
import com.shreyaspatil.easyupipayment.model.TransactionDetails;
import com.shreyaspatil.easyupipayment.model.TransactionStatus;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.boltCore.android.constants.Constants.GOOGLE_PAY_PACKAGE_NAME;
import static com.boltCore.android.constants.Constants.PHONE_PAY_PACKAGE_NAME;

public class BoltBuyChargerActivity extends BoltBaseActivity implements OnMapReadyCallback, PaymentStatusListener {
    private ProgressDialog mProgressDialog;
    private ArrayList<Product> mProductList;
    private int mOrderQuantity = 1;
    private Button mPlusButton, mMinusButton, mQuantityValueButton, mPlaceOrderButton, mPastOrdersButton;
    private TextView mCostTextView, mItemTotalTextView;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private int REQUEST_LOCATION_PERMISSION_REQ_CODE = 200;
    private EasyUpiPayment mEasyUpiPayment;
    private LatLng mCurrentLatLng = null;
    private final int CHARGER_PRICE_PER_UNIT = 3000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_buy_charger_activity);
        initialiseAnalytics(getApplicationContext());
        setupMapViews(savedInstanceState);
        fetchChargerModels();
    }

    private void setupViews() {
        findViewById(R.id.bolt_buy_charger_list_back_button_image_view).setOnClickListener(v -> {
            finish();
        });

        mPlusButton = findViewById(R.id.bolt_buy_charger_plus_button);
        mMinusButton = findViewById(R.id.bolt_buy_charger_minus_button);
        mQuantityValueButton = findViewById(R.id.bolt_buy_charger_quantity_value_button);
        mPlaceOrderButton = findViewById(R.id.bolt_buy_charger_place_order_button);
        mPastOrdersButton = findViewById(R.id.bolt_buy_charger_past_order_button);

        mCostTextView = findViewById(R.id.bolt_buy_charger_cost_text_view);
        mItemTotalTextView = findViewById(R.id.bolt_buy_item_total_text_view);

        mQuantityValueButton.setText(String.valueOf(mOrderQuantity));
        mCostTextView.setText(String.valueOf(getCost()));
        mItemTotalTextView.setText(getString(R.string.rupee) + String.valueOf(mOrderQuantity * CHARGER_PRICE_PER_UNIT));

        mPlusButton.setOnClickListener(v -> {
            mOrderQuantity = Math.min(mOrderQuantity + 1, 10);
            mQuantityValueButton.setText(String.valueOf(mOrderQuantity));
            mCostTextView.setText(String.valueOf(getCost()));
            mItemTotalTextView.setText(getString(R.string.rupee) + String.valueOf(mOrderQuantity * CHARGER_PRICE_PER_UNIT));
        });

        mMinusButton.setOnClickListener(v -> {
            mOrderQuantity = Math.max(mOrderQuantity - 1, 1);
            mQuantityValueButton.setText(String.valueOf(mOrderQuantity));
            mCostTextView.setText(String.valueOf(getCost()));
            mItemTotalTextView.setText(getString(R.string.rupee) + String.valueOf(mOrderQuantity * CHARGER_PRICE_PER_UNIT));
        });

        mPlaceOrderButton.setOnClickListener(v -> {
            String phone = ((EditText)findViewById(R.id.bolt_buy_charger_phone_edit_text)).getText().toString();
            String address = ((EditText)findViewById(R.id.bolt_buy_charger_address_edit_text)).getText().toString();

            if(phone.trim().isEmpty() || address.trim().isEmpty()) {
                showPhoneEmailError();
                return;
            } else {
                startUPIActivity();
            }
        });

        mPastOrdersButton.setOnClickListener(v -> {
            Intent intent = new Intent(BoltBuyChargerActivity.this, BoltPastOrdersActivity.class);
            startActivity(intent);
        });

        ((TextView)findViewById(R.id.bolt_buy_charger_model_details_name_text_view)).setText(mProductList.get(0).getName());
        ((TextView)findViewById(R.id.bolt_buy_charger_model_details_power_text_view)).setText(mProductList.get(0).getConnectorType() + ", " + mProductList.get(0).getPowerRating());
    }

    private void makePlaceOrderCall(String paymentRemarks) {
        String DELIMITER = "<RD>";
        String phone = ((EditText)findViewById(R.id.bolt_buy_charger_phone_edit_text)).getText().toString();
        String address = ((EditText)findViewById(R.id.bolt_buy_charger_address_edit_text)).getText().toString();

        if(phone.trim().isEmpty() || address.trim().isEmpty()) {
            showPhoneEmailError();
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        JsonObject componentJsonObject = new JsonObject();
        componentJsonObject.addProperty("quantity", mOrderQuantity);
        componentJsonObject.addProperty("prototype", mProductList.get(0).getId());

        JsonArray componentJsonArray = new JsonArray();
        componentJsonArray.add(componentJsonObject);

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.add("components", componentJsonArray);
        bodyJsonObject.addProperty("remarks", phone.trim() + DELIMITER + getCost() + DELIMITER + paymentRemarks);
        bodyJsonObject.addProperty("address", address.trim());
        bodyJsonObject.addProperty("latitude", mCurrentLatLng.latitude);
        bodyJsonObject.addProperty("longitude", mCurrentLatLng.longitude);


        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        apiInterface.placeChargerOrder(BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showServerError();
                    return;
                }

                try {
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("status") && bodyJsonObject.get("status").getAsInt() == 200) {
                        showSuccessAndFinish();
                    } else {
                        showServerError();
                    }
                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void startUPIActivity() {
        String payeeVpa = getString(R.string.revos_upi_id);
        String payeeName = getString(R.string.revos_upi_name);
        String transactionId = "revos-txid-" + String.valueOf(System.currentTimeMillis());
        String transactionRefId = "revos-reftxid-"+ String.valueOf(System.currentTimeMillis());
        String description = "BOLT booking payment";
        String amount = String.valueOf(getCost() * 1.0f);

        if(payeeVpa.isEmpty()) {
            Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payee_upi_not_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(payeeName.isEmpty()) {
            Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payee_name_not_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(amount.isEmpty()) {
            Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_amount_to_be_paid));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        boolean isGooglePayPresent = false;
        boolean isPhonePePresent = false;

        if(isPackageInstalled(GOOGLE_PAY_PACKAGE_NAME)) {
            isGooglePayPresent = true;
        }

        if(isPackageInstalled(PHONE_PAY_PACKAGE_NAME)) {
            isPhonePePresent = true;
        }

        if(!isGooglePayPresent && !isPhonePePresent) {
            Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.install_googlepay_phonepe));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        PaymentApp paymentApp = PaymentApp.GOOGLE_PAY;

        if(isPhonePePresent) {
            paymentApp = PaymentApp.PHONE_PE;
        }

        try {
            // START PAYMENT INITIALIZATION
            mEasyUpiPayment = new EasyUpiPayment.Builder(BoltBuyChargerActivity.this)
                    .with(paymentApp)
                    .setPayeeVpa(payeeVpa)
                    .setPayeeName(payeeName)
                    .setTransactionId(transactionId)
                    .setTransactionRefId(transactionRefId)
                    .setDescription(description)
                    .setAmount(amount)
                    .build();

            // Register Listener for Events
            mEasyUpiPayment.setPaymentStatusListener(this);

            showProgressDialog();

            // START PAYMENT
            mEasyUpiPayment.startPayment();

        } catch (Exception e) {
            Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_payee_upi_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            hideProgressDialog();
        }

    }

    private boolean isPackageInstalled(String packageName) {
        PackageManager packageManager = getPackageManager();
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = (MapView) findViewById(R.id.bolt_buy_charger_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    private int getCost() {
        return mOrderQuantity * (CHARGER_PRICE_PER_UNIT);
    }

    private void fetchChargerModels() {

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerModels(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200) {
                    showServerError();
                } else {
                    try {
                        hideProgressDialog();
                        String bodyStr = response.body().string();
                        JsonObject responseJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                        if(responseJsonObject.has("data")) {
                            JsonArray productArray = responseJsonObject.get("data").getAsJsonArray();

                            mProductList = new ArrayList<>();
                            for(JsonElement jsonElement : productArray) {
                                JsonObject productJsonObject = jsonElement.getAsJsonObject();

                                Product product = new Gson().fromJson(productJsonObject.toString(), Product.class);
                                mProductList.add(product);
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setupViews();
                                }
                            });
                        } else {
                            showServerError();
                        }
                    } catch (Exception e) {
                        showServerError();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void showPhoneEmailError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.address_phone_cannot_be_empty));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltBuyChargerActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        setupGoogleMapsAndFetchCurrentLocationDetails();
    }

    private void setupGoogleMapsAndFetchCurrentLocationDetails() {

        if (ActivityCompat.checkSelfPermission(this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }
        setupGoogleMaps();

        centerMapOnLastKnownLocation();
    }

    private void centerMapOnLastKnownLocation() {

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }

        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            //center map around current location
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                                    .zoom(16)
                                    .build();

                            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                });
    }


    private void setupGoogleMaps() {

        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
                return;
            }

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

            mGoogleMap.setOnCameraIdleListener(() -> {
                mCurrentLatLng = mGoogleMap.getCameraPosition().target;
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_LOCATION_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            setupGoogleMapsAndFetchCurrentLocationDetails();
        }

    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onTransactionCancelled() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_failed));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onTransactionCompleted(@NotNull TransactionDetails transactionDetails) {
// Transaction Completed
        Timber.d("TransactionDetails" + transactionDetails.toString());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                if(transactionDetails.getTransactionStatus() == TransactionStatus.SUCCESS) {
                    makePlaceOrderCall(transactionDetails.toString());
                } else {
                    Intent intent = new Intent(BoltBuyChargerActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_failed));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }
}
