package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boltCore.android.adapters.BookingAnalyticsAdapter;
import com.boltCore.android.adapters.BookingListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.boltCore.android.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.joda.time.DateTime;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BoltBookingHistoryActivity extends BoltBaseActivity implements View.OnClickListener {
    private final String ACTIVE = "ACTIVE";
    private final String PENDING_PAYMENT = "PENDING_PAYMENT";
    private final String ENDED = "ENDED";
    private final String TERMINATED = "TERMINATED";
    private  ArrayList<String> mDates = new ArrayList<String>();
    private  ArrayList<String> mBookingDates = new ArrayList<String>();
    private  ArrayList<Booking> mProcessedBookings = new ArrayList<>();
    private  ArrayList<Booking> mBookingAnalytics = new ArrayList<>();
    private ProgressDialog mProgressDialog;
    private RecyclerView mBookingView,mBookingList;
    private CardView mRevenue,mBooking;
    private TextView mSelectionType, mExpenseTextView, mBookingCountTextView;
    private AlertDialog mTypeDialog;
    private LinearLayout mExpenseHint, mBookingHint;
    private NestedScrollView scrollView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_booking_history_activity);
        setupViews();
        initialiseAnalytics(this);
        fetchBookings();

    }

    private void setupViews(){
        mRevenue=(CardView)findViewById(R.id.bolt_booking_history_expense_card_view);
        mBooking=(CardView)findViewById(R.id.bolt_booking_history_booking_count_card_view);
        mSelectionType=(TextView)findViewById(R.id.selection_type);
        mExpenseTextView =(TextView)findViewById(R.id.bolt_booking_history_expense_text_view);
        mBookingCountTextView =(TextView)findViewById(R.id.bolt_booking_history_booking_count_text_view);
        mExpenseHint =(LinearLayout)findViewById(R.id.bolt_booking_history_expense_hint_layout);
        mBookingHint =(LinearLayout)findViewById(R.id.bolt_booking_history_booking_hint_layout);
        scrollView=(NestedScrollView)findViewById(R.id.scrollview);
        mRevenue.setOnClickListener(this);
        mBooking.setOnClickListener(this);


        findViewById(R.id.bolt_booking_history_list_back_button_image_view).setOnClickListener(v -> {finish();});

        findViewById(R.id.bolt_booking_history_layout_filter).setOnClickListener(v -> {
            mBookingsFilter();
        });

    }

    private void fetchBookings() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getUserBookings(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.code() != 200 || response.body() == null) {
                            showServerError();
                            return;
                        }

                        try {

                            String responseBody = response.body().string();

                            JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                            if(responseJsonObject.has("data")) {
                                JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                                String dataStr = data.toString();
                                ArrayList<Booking> bookings = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Booking>>(){}.getType());


                                for(Booking booking : bookings) {
                                    if(booking.getStatus().equals(ENDED) || booking.getStatus().equals(TERMINATED)) {
                                        mProcessedBookings.add(booking);
                                    }
                                }

                                if(mProcessedBookings.isEmpty()) {
                                    showNoBookingsFound();
                                } else {
                                    setupApiData(mProcessedBookings);
                                }
                            }

                        } catch (Exception e) {
                            //do nothing here
                        }

                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void setupApiData(ArrayList<Booking> bookings) {

        //clear text views first
        mExpenseTextView.setText(getString(R.string.rupee) + 0 +" ");
        mBookingCountTextView.setText(0 + " ");

        Collections.sort(bookings, new Comparator<Booking>() {
            @Override
            public int compare(Booking b1, Booking b2) {
                DateTime dateTime1 = new DateTime(b1.getStartTime());
                DateTime dateTime2 = new DateTime(b2.getStartTime());
                return dateTime2.compareTo(dateTime1);
            }
        });

        getDate(7);
        for (Booking  booking: bookings){
            DateTime dateTime=new DateTime(booking.getStartTime());
            Date tripStartDate = dateTime.toDate();
            String dateFormat = "yyyy-MM-dd";
            String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
            mBookingDates.add(tripDate);
        }
        for (int i=0;i<arraysMatching(mBookingDates,mDates).length;i++){
            mBookingAnalytics.add(bookings.get(i));
        }

        if(mBookingAnalytics == null || mBookingAnalytics.isEmpty()) {
            return;
        }

        Collections.reverse(mBookingAnalytics);
        setupData(mBookingAnalytics);
        setupBarGraph(mBookingAnalytics,0);

        ArrayList<Booking> dateSortedBookingList = mBookingAnalytics;

        Collections.sort(dateSortedBookingList, new Comparator<Booking>() {
            @Override
            public int compare(Booking b1, Booking b2) {
                DateTime dateTime1 = new DateTime(b1.getStartTime());
                DateTime dateTime2 = new DateTime(b2.getStartTime());
                return dateTime1.compareTo(dateTime2);
            }
        });

        setupBookingAdapter(dateSortedBookingList);
        mExpenseHint.setVisibility(View.GONE);
        mBookingHint.setVisibility(View.VISIBLE);

    }

    private void showNoBookingsFound() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_bookings_found));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltBookingHistoryActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void  mBookingsFilter(){
        CharSequence[] spinnerValues = {getString(R.string.last_seven_days), getString(R.string.last_two_weeks), getString(R.string.last_month)};
        AlertDialog.Builder builder = new AlertDialog.Builder( BoltBookingHistoryActivity.this);
        builder.setSingleChoiceItems(spinnerValues, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch(item)
                {
                    case 0:
                        mDates.clear();
                        mBookingDates.clear();
                        mBookingAnalytics.clear();
                        mProcessedBookings.clear();
                        getDate(7);
                        mSelectionType.setText(getString(R.string.last_seven_days));
                        fetchBookings();
                        break;
                    case 1:
                        mDates.clear();
                        mBookingDates.clear();
                        mBookingAnalytics.clear();
                        mProcessedBookings.clear();
                        getDate(14);
                        mSelectionType.setText(getString(R.string.last_two_weeks));
                        fetchBookings();
                        break;
                    case 2:
                        mDates.clear();
                        mBookingDates.clear();
                        mBookingAnalytics.clear();
                        mProcessedBookings.clear();
                        getDate(30);
                        mSelectionType.setText(getString(R.string.last_month));
                        fetchBookings();
                        break;
                }
                mTypeDialog.dismiss();
            }
        });
        mTypeDialog = builder.create();
        mTypeDialog.show();
    }

    public void getDate(int days) {

        for (int i=0;i<days;i++){
            Date date = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, - i);
            Date end = c.getTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            mDates.add(format.format(end));

        }

    }
    public static String[] arraysMatching(ArrayList<String> s1, ArrayList<String> s2) {

        ArrayList<String> storage = new ArrayList<>();
        for ( String i : s1) {
            for (String j : s2) {
                if (j .equals(i)) {
                    storage.add(j);
                    break;
                }
            }
        }
        //alternatively
        String[] returnArray = new String[storage.size()];
        for(int i=0; i< storage.size(); i++) {
            returnArray[i]=storage.get(i);
        }

        return returnArray;
    }

    private void setupData(ArrayList<Booking> mBookingAnalytics){
        int mCount=0,mAmount=0;
        for (Booking booking:mBookingAnalytics){
            mCount++;
            if (booking.getInvoice().getAmount() != 0){
                mAmount+=(int)booking.getInvoice().getAmount();
            }
        }
        mExpenseTextView.setText(getString(R.string.rupee) + mAmount+" ");
        mBookingCountTextView.setText(mCount+" ");
    }

    private void setupBookingAdapter(ArrayList<Booking> mBookingAnalytics){
        mBookingList = findViewById(R.id.bolt_booking_history_list_recycler_view);
        mBookingList.setNestedScrollingEnabled(false);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(BoltBookingHistoryActivity.this);
        mLinearLayoutManager.setReverseLayout(true);
        mLinearLayoutManager.setStackFromEnd(true);
        mBookingList.setLayoutManager(mLinearLayoutManager);
        BookingListAdapter bookingListAdapter = new BookingListAdapter(mBookingAnalytics, BoltBookingHistoryActivity.this);
        mBookingList.setAdapter(bookingListAdapter);
        bookingListAdapter.notifyDataSetChanged();
    }

    private void setupBarGraph(ArrayList<Booking> mBookingAnalytics,int mtype){
        if (mtype == 0){
            mRevenue.setForeground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.card_outline_background));
            mBooking.setForeground(null);
        }else {
            mBooking.setForeground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.card_outline_background));
            mRevenue.setForeground(null);
        }
        mBookingView=findViewById(R.id.bolt_booking_history_bar_chart_recycler_view);
        mBookingView.setNestedScrollingEnabled(false);
        mBookingView.setLayoutManager(new LinearLayoutManager(BoltBookingHistoryActivity.this));
        BookingAnalyticsAdapter  bookingAnalyticsAdapter=new BookingAnalyticsAdapter(mBookingAnalytics,mtype,BoltBookingHistoryActivity.this);
        mBookingView.setAdapter(bookingAnalyticsAdapter);
        bookingAnalyticsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.bolt_booking_history_expense_card_view) {
            setupBarGraph(mBookingAnalytics,0);
            mExpenseHint.setVisibility(View.GONE);
            mBookingHint.setVisibility(View.VISIBLE);
            scrollView.fullScroll(View.FOCUS_UP);
        } else if (id == R.id.bolt_booking_history_booking_count_card_view) {
            setupBarGraph(mBookingAnalytics,1);
            mBookingHint.setVisibility(View.GONE);
            mExpenseHint.setVisibility(View.VISIBLE);
            scrollView.fullScroll(View.FOCUS_UP);
        }
    }
}