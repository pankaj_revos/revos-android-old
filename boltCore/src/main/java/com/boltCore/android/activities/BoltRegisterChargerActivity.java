package com.boltCore.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.boltCore.android.constants.Constants;
import com.boltCore.android.fragments.BoltEnterChargerDetailsFragment;
import com.boltCore.android.fragments.BoltFindAndConnectToChargerFragment;
import com.boltCore.android.fragments.BoltSetChargerLocationFragment;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.jsonStructures.Product;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.boltCore.android.R;
import com.boltCore.android.utilities.ble.ActivationStatusListener;
import com.boltCore.android.utilities.ble.BoltBleManager;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTING;
import static com.boltCore.android.utilities.ble.BoltBleManager.STATE_ACTIVATED;
import static com.boltCore.android.utilities.ble.BoltBleManager.STATE_INITIALIZED;

public class BoltRegisterChargerActivity extends  BoltBaseActivity {
    private String mChargerUID;
    private TextView mHeadingTextView;
    private Button mSubmitButton;

    public static int SETUP_BLE_STATUS_SEARCHING = 1;
    public static int SETUP_BLE_STATUS_CONNECTING = 2;
    public static int SETUP_BLE_STATUS_CONNECTED = 3;

    private int mBleStatus;

    private ProgressDialog mProgressDialog;

    private BoltBleManager mBoltBleManager;

    /**Variables and handler for scanner*/
    private Handler mHandler;
    private BluetoothLeScannerCompat mScanner;
    private static boolean mIsScanning = false;
    private final long SCAN_DURATION = 30000;

    public LatLng mChargerLatLng;

    private HashSet<String> mRejectedChargerNameSet;

    private final int VALID_CHARGER_NAME_LENGTH = 17;

    private Timer mOneSecondTimer;

    private Fragment mFindAndConnectToChargerFragment, mSetChargerLocationFragment, mEnterChargerDetailsFragment;

    public ArrayList<Product> mProductList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null) {
            showInvalidQrCode();
            finish();
            return;
        }

        mChargerUID = bundle.getString(Constants.CHARGER_UID_KEY, null);
        if(mChargerUID == null || mChargerUID.isEmpty()) {
            showInvalidQrCode();
            finish();
            return;
        }

        setContentView(R.layout.bolt_register_charger_activity);
        initialiseAnalytics(getApplicationContext());
        fetchChargerModels();

        setupViews();

        setupRelatedBleVariables();

        setupTimer();

        loadFindAndConnectToChargerFragment();
    }

    private void setupViews() {
        mHeadingTextView = findViewById(R.id.bolt_register_charger_heading_text_view);
        mSubmitButton = findViewById(R.id.bolt_register_charger_submit_button);
        mSubmitButton.setOnClickListener(v -> {
            ((BoltEnterChargerDetailsFragment)mEnterChargerDetailsFragment).registerCharger();
        });

        findViewById(R.id.bolt_register_charger_list_back_button_image_view).setOnClickListener(v -> {
            finish();
        });
    }

    private void loadFindAndConnectToChargerFragment() {
        if(mFindAndConnectToChargerFragment == null) {
            mFindAndConnectToChargerFragment = new BoltFindAndConnectToChargerFragment();
        }
        mHeadingTextView.setText(getString(R.string.connect_to_charger));
        mSubmitButton.setVisibility(View.INVISIBLE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.bolt_register_charger_fragment_container_view, mFindAndConnectToChargerFragment)
                .commit();
    }

    public void loadSelectChargerLocationFragment() {
        if(mSetChargerLocationFragment == null) {
            mSetChargerLocationFragment = new BoltSetChargerLocationFragment();
        }
        mHeadingTextView.setText(getString(R.string.set_charger_location));
        mSubmitButton.setVisibility(View.INVISIBLE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.bolt_register_charger_fragment_container_view, mSetChargerLocationFragment)
                .commit();
    }

    public void loadEnterChargerDetailsFragment() {
        if(mEnterChargerDetailsFragment == null) {
            mEnterChargerDetailsFragment = new BoltEnterChargerDetailsFragment();
        }
        mHeadingTextView.setText(getString(R.string.enter_charger_details));
        mSubmitButton.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.bolt_register_charger_fragment_container_view, mEnterChargerDetailsFragment)
                .commit();
    }

    private void showInvalidQrCode() {
        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_qr_code));
        bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public int getBleStatus() {
        return mBleStatus;
    }

    private void connectToDeviceIfRequired() {
        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null) {
            return;
        }

        connectToDeviceHelper();
    }

    private void setupRelatedBleVariables() {
        mRejectedChargerNameSet = new HashSet<>();
        mHandler = new Handler(getMainLooper());
        mBoltBleManager = new BoltBleManager(BoltRegisterChargerActivity.this);
        mScanner = BluetoothLeScannerCompat.getScanner();
    }

    private void connectToDeviceHelper() {
        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null) {
            return;
        }

        try {
            startScan();
        } catch (Exception e) {
            if(mBoltBleManager != null) {
                mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {

                    }
                }, new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                    }
                });
            }

            finish();
        }
    }

    private void setupTimer() {
        mOneSecondTimer = new Timer();

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!hasWindowFocus()) {
                            return;
                        }
                        connectToDeviceIfRequired();
                        updateBleStatus();
                        disconnectFromDeviceIfRequired();
                    }
                });

            }
        }, 100, 1000);
    }

    private void disconnectFromDeviceIfRequired() {
        if(mBoltBleManager.isConnected() && mBoltBleManager.getChargerActivationState() == STATE_ACTIVATED) {
            Timber.d("rejecting charger with mac : " + mBoltBleManager.getBluetoothDevice().getName());
            mRejectedChargerNameSet.add(mBoltBleManager.getBluetoothDevice().getName());
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
    }

    private void updateBleStatus() {
        if(mBoltBleManager.getConnectionState() == STATE_CONNECTING) {
            mBleStatus = SETUP_BLE_STATUS_CONNECTING;
        } else if(mBoltBleManager.isConnected() && mBoltBleManager.getChargerActivationState() == STATE_INITIALIZED){
            mBleStatus = SETUP_BLE_STATUS_CONNECTED;
        } else if(mIsScanning) {
            mBleStatus = SETUP_BLE_STATUS_SEARCHING;
        }
    }

    private synchronized void startScan() {

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            //todo::send message to show a dialog
            return;
        }

        if(mIsScanning || mBoltBleManager.isConnected()) {
            return;
        }

        mIsScanning = true;

        mScanner.startScan(scanCallback);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScanHelper();
                }
            }
        }, SCAN_DURATION);
    }

    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final no.nordicsemi.android.support.v18.scanner.ScanResult result) {
            Timber.d("Device found : " + result.getDevice().getAddress() + ", " + result.getDevice().getName());


            if(result.getDevice().getName() != null &&
                    !result.getDevice().getName().isEmpty() &&
                    result.getDevice().getName().contains("BOLT_") &&
                    result.getDevice().getName().length() == VALID_CHARGER_NAME_LENGTH &&
                    !mRejectedChargerNameSet.contains(result.getDevice().getName())) {
                Timber.d("Device found : " + result.getDevice().getAddress() + ", " + result.getDevice().getName());

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        stopScanHelper();
                        if(mBoltBleManager != null && !mBoltBleManager.isConnected() && mBoltBleManager.getConnectionState() != STATE_CONNECTING) {
                            mBoltBleManager.connectToDevice(result.getDevice().getAddress(), null);
                        }
                    }
                });
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };

    private void stopScanHelper() {
        mScanner.stopScan(scanCallback);
        mIsScanning = false;
    }

    private void fetchChargerModels() {

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerModels(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200) {
                    showServerErrorAndFinish();
                } else {
                    try {
                        hideProgressDialog();
                        String bodyStr = response.body().string();
                        JsonObject responseJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                        if(responseJsonObject.has("data")) {
                            JsonArray productArray = responseJsonObject.get("data").getAsJsonArray();

                            mProductList = new ArrayList<>();
                            for(JsonElement jsonElement : productArray) {
                                JsonObject productJsonObject = jsonElement.getAsJsonObject();

                                Product product = new Gson().fromJson(productJsonObject.toString(), Product.class);
                                mProductList.add(product);
                            }
                        } else {
                            showServerErrorAndFinish();
                        }
                    } catch (Exception e) {
                        showServerErrorAndFinish();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();
        super.onBackPressed();

    }

    private void showServerErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showErrorAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.activation_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltRegisterChargerActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void makeRegisterChargerCall(JsonObject bodyJsonObject) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        bodyJsonObject.addProperty("UID", mChargerUID);
        bodyJsonObject.addProperty("bleMac", mBoltBleManager.getBluetoothDevice().getAddress());
        bodyJsonObject.addProperty("bleName", mBoltBleManager.getBluetoothDevice().getName());
        bodyJsonObject.addProperty("latitude", String.valueOf(mChargerLatLng.latitude));
        bodyJsonObject.addProperty("longitude", String.valueOf(mChargerLatLng.longitude));
        bodyJsonObject.addProperty("key", generateAesKey());
        bodyJsonObject.addProperty("version", mBoltBleManager.getChargerFirmwareVersion());

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        apiInterface.registerCharger(BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200) {

                }
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    int j = 10;
                    if(bodyJsonObject.has("data")) {
                        Charger registeredCharger = new Gson().fromJson(bodyJsonObject.get("data").getAsJsonObject().toString(), Charger.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activateCharger(registeredCharger);
                            }
                        });
                    } else {
                        showServerErrorAndFinish();
                    }
                } catch (Exception e) {
                    showServerErrorAndFinish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerErrorAndFinish();
            }
        });
    }

    private String generateAesKey() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] aesKeyBytes = new byte[16];
        secureRandom.nextBytes(aesKeyBytes);
        return Base64.encodeToString(aesKeyBytes, Base64.NO_WRAP | Base64.URL_SAFE);
    }

    private void activateCharger(Charger registeredCharger) {
        byte[] keyBytes = Base64.decode(registeredCharger.getKey(), Base64.NO_WRAP | Base64.URL_SAFE);
        mBoltBleManager.activateCharger(keyBytes, registeredCharger.getUID(), new ActivationStatusListener() {
            @Override
            public void result(boolean success) {
                if(success) {
                    showSuccessAndFinish();
                    finish();
                } else {
                    showErrorAndFinish();
                }
            }
        });
    }
}
