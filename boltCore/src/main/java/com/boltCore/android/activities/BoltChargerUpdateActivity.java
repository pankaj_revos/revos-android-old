package com.boltCore.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.SyncInfoObject;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.boltCore.android.utilities.ble.BoltBleManager;
import com.boltCore.android.utilities.ble.OtaStatusListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import at.grabner.circleprogress.CircleProgressView;
import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class BoltChargerUpdateActivity extends  BoltBaseActivity {
    public final static String SYNC_INFO_OBJECT_ARG_KEY = "syncInfoObjectArgKey";

    private File mOtaFile;


    private String mExpectedOtaVersion;

    private Timer mOneSecondTimer;

    private TextView mStatusTextView, mMessageTextView;
    private LottieAnimationView mLottieView;
    private Button mStartUpdateButton;

    private ProgressDialog mProgressDialog;

    private boolean mOtaInProgess, mOtaIsComplete;
    private BoltBleManager mBoltBleManager;

    private CircleProgressView mCircleProgressView;

    /**Variables and handler for scanner*/
    private Handler mHandler;
    private BluetoothLeScannerCompat mScanner;
    private static boolean mIsScanning = false;
    private static boolean mDeviceFound = false;
    private final long SCAN_DURATION = 30000;

    private final String INVALID_CHARGE_ID = "DEFAULTCHARGEID";

    private final String ACTIVE = "ACTIVE";

    private SyncInfoObject mSyncInfoObject;

    private static boolean mForceDisconnect = false;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_REQ_CODE = 300;

    private final String STATUS_UPDATED = "UPDATED";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_charger_update_activity);
        initialiseAnalytics(getApplicationContext());
        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(SYNC_INFO_OBJECT_ARG_KEY, null) == null) {
            finish();
            return;
        }


        mSyncInfoObject = new Gson().fromJson(bundle.getString(SYNC_INFO_OBJECT_ARG_KEY), SyncInfoObject.class);
        mExpectedOtaVersion = mSyncInfoObject.getExpectedFirmware();

        if(mSyncInfoObject == null || mExpectedOtaVersion == null) {
            finish();
            return;
        }

        setupRelatedBleVariables();

        setupViews();

        setupTimer();

    }

    private void setupRelatedBleVariables() {
        mHandler = new Handler(getMainLooper());
        mBoltBleManager = new BoltBleManager(BoltChargerUpdateActivity.this);
        mScanner = BluetoothLeScannerCompat.getScanner();
    }

    private void setupTimer() {
        mOneSecondTimer = new Timer();

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!hasWindowFocus()) {
                            return;
                        }
                        updateViews();
                        connectToDeviceIfRequired();
                    }
                });

            }
        }, 100, 1000);
    }


    private void connectToDeviceIfRequired() {
        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {
            return;
        }

        connectToDeviceHelper();
    }

    private void setupViews()  {
        mStatusTextView = findViewById(R.id.bolt_charger_update_status_text_view);
        mMessageTextView = findViewById(R.id.bolt_charger_update_message_text_view);
        mLottieView = findViewById(R.id.bolt_charger_update_lottie_view);
        mStartUpdateButton = findViewById(R.id.bolt_charger_start_update_button);

        mStartUpdateButton.setOnClickListener(v -> {startOtaSequence();});

        mCircleProgressView = findViewById(R.id.bolt_charger_circle_progress_view);
    }

    private void startOtaSequence() {
        if(!checkAndObtainExternalWritePermission()) {
            return;
        }
        mOtaInProgess = true;
        downloadOta();
    }

    private boolean checkAndObtainExternalWritePermission() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BoltChargerUpdateActivity.this, mPermissionsRequired, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_REQ_CODE);
            return false;
        } else {
            return true;
        }
    }

    private void downloadOta() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        //hide the button
        mStartUpdateButton.setVisibility(View.INVISIBLE);

        mMessageTextView.setText(getString(R.string.downloading_update));

        mLottieView.setAnimation(R.raw.ic_cloud_download);
        mLottieView.setProgress(0);
        mLottieView.playAnimation();
        mLottieView.loop(true);

        apiInterface.downloadOtaFileBuffer(mBoltBleManager.getChargerUID(), mExpectedOtaVersion, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showErrorAndFinish(getString(R.string.server_error));
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String bodyStr = response.body().string();
                            JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                            if(bodyJsonObject.has("data")) {

                                String otaStr = bodyJsonObject.get("data").getAsString();
                                mOtaFile = new File(getExternalFilesDir("ota"), "ota.bin");


                                byte[] byteArray = Base64.decode(otaStr, Base64.DEFAULT);
                                if(writeByteArrayAsFile(byteArray, mOtaFile)) {
                                    mLottieView.setAnimation(R.raw.ic_gear);
                                    mLottieView.setProgress(0);
                                    mLottieView.playAnimation();
                                    mLottieView.loop(true);

                                    mBoltBleManager.startOTA(mOtaFile.getAbsolutePath(), new OtaStatusListener() {
                                        @Override
                                        public void onOtaStarted() {
                                            mLottieView.setAnimation(R.raw.ic_gear);
                                            mLottieView.setProgress(0);
                                            mLottieView.playAnimation();
                                            mLottieView.loop(true);
                                            mMessageTextView.setText(getString(R.string.ota_description));
                                            mStatusTextView.setText(getString(R.string.ota_started));
                                        }

                                        @Override
                                        public void onOtaProgress(float percentage) {
                                            mCircleProgressView.stopSpinning();
                                            mCircleProgressView.setVisibility(View.VISIBLE);
                                            mCircleProgressView.setValueAnimated(percentage);
                                            mCircleProgressView.invalidate();

                                            DecimalFormat df = new DecimalFormat("#.##");
                                            df.setRoundingMode(RoundingMode.CEILING);
                                            mStatusTextView.setText(getString(R.string.updating_charger_software) + " : " + df.format(percentage) + "%" );
                                        }

                                        @Override
                                        public void onOtaComplete(boolean success) {
                                            if(success) {
                                                mStatusTextView.setText(getString(R.string.success));
                                                markDeviceStatus();
                                            } else {
                                                mStatusTextView.setText(getString(R.string.error));
                                                showErrorAndFinish(getString(R.string.error));
                                            }

                                        }

                                        @Override
                                        public void onOtaError(String errorMessage) {
                                            mStatusTextView.setText(getString(R.string.error));
                                            showErrorAndFinish(errorMessage);
                                        }
                                    });
                                }
                            } else {
                                showErrorAndFinish(getString(R.string.server_error));
                            }

                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorAndFinish(getString(R.string.server_error));
                                }
                            });
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showErrorAndFinish(getString(R.string.server_error));
            }

        });
    }

    private void markDeviceStatus() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);


        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("status", STATUS_UPDATED);
        bodyJsonObject.addProperty("version", mSyncInfoObject.getExpectedFirmware());

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        showProgressDialog();

        apiInterface.updateDeviceOtaStatus(mBoltBleManager.getChargerUID(), BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        if(response.code() != 200) {
                            showErrorAndFinish(getString(R.string.server_error));
                        } else {
                            showSuccessAndFinish();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        showErrorAndFinish(getString(R.string.server_error));
                    }
                });
            }
        });

    }

    private boolean writeByteArrayAsFile(byte[] data, File file) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
            return true;
        } catch (IOException e) {
        }
        return false;
    }


    private void updateViews() {
        if(mIsScanning && !mBoltBleManager.isConnected()) {
            mStatusTextView.setText(getString(R.string.searching_for_charger));

            mStatusTextView.setVisibility(View.VISIBLE);
            mLottieView.setVisibility(View.VISIBLE);
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {

            //we're connected to the correct charger, now check if authentication is complete or not
            if(!mBoltBleManager.isAuthenticationComplete()) {
                mStatusTextView.setText(getString(R.string.connecting_to_charger));

                mLottieView.setVisibility(View.VISIBLE);
                mStatusTextView.setVisibility(View.VISIBLE);
            } else if(!mOtaInProgess){

                if(!mStatusTextView.getText().toString().contains(getString(R.string.connection_successful))) {
                    mLottieView.setAnimation(R.raw.ic_generic_message_success);
                    mLottieView.playAnimation();
                    mLottieView.loop(false);
                }

                String msg = getString(R.string.connection_successful);
                String firmwareVersion = mBoltBleManager.getChargerFirmwareVersion();

                if(firmwareVersion != null) {
                    msg += ", Software Version : " + firmwareVersion;
                }
                mStatusTextView.setText(msg);

                mMessageTextView.setVisibility(View.VISIBLE);
                mStartUpdateButton.setVisibility(View.VISIBLE);
            }
        }
    }


    private void connectToDeviceHelper() {
        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {
            return;
        }

        try {
            startScan();
        } catch (Exception e) {
            if(mBoltBleManager != null) {
                mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {

                    }
                }, new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                    }
                });
            }

            finish();
        }
    }


    /**
     * Scan for 5 seconds and then stop scanning when a BluetoothLE device is found then mLEScanCallback
     * is activated This will perform regular scan for custom BLE Service UUID and then filter out.
     * using class ScannerServiceParser
     */
    private synchronized void startScan() {

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            //todo::send message to show a dialog
            return;
        }

        if(mIsScanning || mBoltBleManager.isConnected()) {
            return;
        }

        mDeviceFound = false;
        mIsScanning = true;

        mScanner.startScan(scanCallback);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScanHelper();
                }
            }
        }, SCAN_DURATION);
    }

    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final ScanResult result) {
            Timber.d("Device found : " + result.getDevice().getAddress());
            if(result.getDevice().getAddress().trim().toUpperCase().equals(mSyncInfoObject.getBleMac().toUpperCase())) {

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceFound = true;
                        stopScanHelper();
                        if(mBoltBleManager != null && !mBoltBleManager.isConnected() && !mForceDisconnect) {
                            mBoltBleManager.connectToDevice(mSyncInfoObject.getBleMac().toUpperCase(), mSyncInfoObject.getKey());
                        }
                    }
                });
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };

    private void stopScanHelper() {
        mScanner.stopScan(scanCallback);
        mIsScanning = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            startOtaSequence();
        }

    }

    @Override
    protected void onDestroy() {
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mForceDisconnect = true;
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();

        if(mOtaFile != null && mOtaFile.exists()) {
            mOtaFile.delete();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        //do nothing, this activity will finish on it's own

    }

    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showErrorAndFinish(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, error);
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }


    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltChargerUpdateActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mForceDisconnect = false;
    }
}
