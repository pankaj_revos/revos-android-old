package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import androidx.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BoltAddPaymentAccountActivity extends BoltBaseActivity {
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_add_payment_account_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

    }

    private void setupViews() {

        findViewById(R.id.bolt_add_payment_account_back_button_image_view).setOnClickListener(v -> finish());
        findViewById(R.id.bolt_add_payment_account_submit_button).setOnClickListener(v -> addPaymentAccount());
    }


    private void addPaymentAccount() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }


        JsonObject bodyJsonObject = validateDataAndConstructBodyJsonObject();
        if(bodyJsonObject == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        String bodyStr = new Gson().toJson(bodyJsonObject);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.addPaymentMethod(BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();

                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();

                    if(bodyJsonObject.has("status")) {

                        if (bodyJsonObject.get("status").getAsInt() == 400) {

                            if (bodyJsonObject.has("data") && bodyJsonObject.getAsJsonObject("data").has("data")) {
                                JsonObject innerDataJsonObject = bodyJsonObject.getAsJsonObject("data").getAsJsonObject("data");
                                if (innerDataJsonObject.has("status") && innerDataJsonObject.get("status").getAsString().equals("ERROR")) {
                                    showError(innerDataJsonObject.get("message").getAsString());
                                } else {
                                    showServerError();
                                }
                            } else {
                                showServerError();
                            }
                        } else if(bodyJsonObject.get("status").getAsInt() == 201) {
                            showSuccessAndFinish();
                        }
                    } else {
                        showServerError();
                    }

                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressDialog();
                showServerError();
            }
        });
    }

    private void showError(String errorMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, errorMessage);
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 5000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }


    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showSuccessAndFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private JsonObject validateDataAndConstructBodyJsonObject() {
        JsonObject bodyJsonObject = new JsonObject();

        //bank account details
        String nickName = ((EditText)findViewById(R.id.bolt_add_payment_account_account_nick_name_text_view)).getText().toString().trim();
        String primaryAccountNumber = ((EditText)findViewById(R.id.bolt_add_payment_account_primary_account_number_text_view)).getText().toString().trim();
        String confirmAccountNumber = ((EditText)findViewById(R.id.bolt_add_payment_account_confirm_account_number_text_view)).getText().toString().trim();
        String ifscCode = ((EditText)findViewById(R.id.bolt_add_payment_account_ifsc_code_text_view)).getText().toString().trim();
        String bankName = ((EditText)findViewById(R.id.bolt_add_payment_account_bank_name_text_view)).getText().toString().trim();
        String beneficiaryName = ((EditText)findViewById(R.id.bolt_add_payment_account_beneficiary_name_text_view)).getText().toString().trim();

        //contact information
        String beneficiaryEmail = ((EditText)findViewById(R.id.bolt_add_payment_account_beneficiary_email_text_view)).getText().toString().trim();
        String beneficiaryPhone = ((EditText)findViewById(R.id.bolt_add_payment_account_beneficiary_phone_text_view)).getText().toString().trim();
        String addressLine1 = ((EditText)findViewById(R.id.bolt_add_payment_account_address_line_1_text_view)).getText().toString().trim();
        String addressLine2 = ((EditText)findViewById(R.id.bolt_add_payment_account_address_line_2_text_view)).getText().toString().trim();
        String city = ((EditText)findViewById(R.id.bolt_add_payment_account_city_text_view)).getText().toString().trim();
        String state = ((EditText)findViewById(R.id.bolt_add_payment_account_state_text_view)).getText().toString().trim();
        String pinCode = ((EditText)findViewById(R.id.bolt_add_payment_account_pin_code_text_view)).getText().toString().trim();

        //optional details
        String aadharCard = ((EditText)findViewById(R.id.bolt_add_payment_account_aadhar_text_view)).getText().toString().trim();
        String panCard = ((EditText)findViewById(R.id.bolt_add_payment_account_pan_text_view)).getText().toString().trim();
        String gstNo = ((EditText)findViewById(R.id.bolt_add_payment_account_gst_text_view)).getText().toString().trim();

        if(nickName.isEmpty() || primaryAccountNumber.isEmpty() || confirmAccountNumber.isEmpty() || ifscCode.isEmpty() || bankName.isEmpty() || beneficiaryName.isEmpty()
            || beneficiaryEmail.isEmpty() || beneficiaryPhone.isEmpty() || addressLine1.isEmpty() || addressLine2.isEmpty() || city.isEmpty() || state.isEmpty() || pinCode.isEmpty()) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_account_provide_all_details));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);
            return null;
        }

        if(!primaryAccountNumber.equals(confirmAccountNumber)) {
            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.account_number_dont_match));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);
            return null;
        }

        bodyJsonObject.addProperty("stage", getString(R.string.cf_stage));
        bodyJsonObject.addProperty("name", nickName);
        bodyJsonObject.addProperty("phone", beneficiaryPhone);
        bodyJsonObject.addProperty("email", beneficiaryEmail);
        bodyJsonObject.addProperty("bankAccount", confirmAccountNumber);
        bodyJsonObject.addProperty("accountHolder", beneficiaryName);
        bodyJsonObject.addProperty("ifsc", ifscCode);
        bodyJsonObject.addProperty("address1", addressLine1);
        bodyJsonObject.addProperty("address2", addressLine2);
        bodyJsonObject.addProperty("city", city);
        bodyJsonObject.addProperty("state", state);
        bodyJsonObject.addProperty("pincode", pinCode);
        bodyJsonObject.addProperty("bankName", bankName);

        bodyJsonObject.addProperty("panNo", panCard);
        bodyJsonObject.addProperty("aadharNo", aadharCard);
        bodyJsonObject.addProperty("gstin", gstNo);


        return bodyJsonObject;
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltAddPaymentAccountActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
