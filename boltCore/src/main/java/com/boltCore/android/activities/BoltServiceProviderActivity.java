package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.boltCore.android.R;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Charger;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import androidx.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class BoltServiceProviderActivity extends  BoltBaseActivity {
    public final static String UID_ARG_KEY = "uidArgKey";

    private ProgressDialog mProgressDialog;
    private String mChargerUid;

    private TextView mServiceProviderTextView;
    private EditText mServiceProviderEditText;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getExtras() == null) {
            finish();
            return;
        }

        mChargerUid = getIntent().getExtras().getString(UID_ARG_KEY, null);
        if(mChargerUid == null || mChargerUid.isEmpty()) {
            finish();
            return;
        }

        setContentView(R.layout.bolt_service_provider_activity);
        initialiseAnalytics(getApplicationContext());

        setupViews();

        fetchChargerDetailsAndRefreshViews(mChargerUid);
    }

    private void setupViews() {
        mServiceProviderTextView = findViewById(R.id.bolt_service_provider_text_view);
        mServiceProviderEditText = findViewById(R.id.bolt_service_provider_edit_text);

        findViewById(R.id.bolt_service_provider_back_button_image_view).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.bolt_service_provider_submit_button).setOnClickListener(v -> {
            String providerCode = mServiceProviderEditText.getText().toString().trim();
            if(providerCode.isEmpty()) {
                showErrorAndFinish(getString(R.string.service_provider_code_cannot_be_empty));
            } else {
                updateServiceProvider(providerCode);
            }
        });
    }

    private void updateServiceProvider(String providerCode) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("companyCode", providerCode);
        String bodyStr = new Gson().toJson(bodyJsonObject);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.updateChargerProvider(mChargerUid, BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 400) {
                    showErrorAndFinish(getString(R.string.invalid_service_provider_code));
                    return;
                } else if(response.code() != 200 || response.body() == null) {
                    showErrorAndFinish(getString(R.string.server_error));
                    return;
                }

                showSuccess();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fetchChargerDetailsAndRefreshViews(mChargerUid);
                    }
                });

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showErrorAndFinish(getString(R.string.server_error));
            }
        });

    }

    private void fetchChargerDetailsAndRefreshViews(String chargerUID) {
        mServiceProviderEditText.setText("");

        if(chargerUID == null || chargerUID.isEmpty()) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getChargerById(chargerUID, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    showErrorAndFinish(getString(R.string.server_error));
                    return;
                }

                try {
                    String bodyStr = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonObject data = bodyJsonObject.get("data").getAsJsonObject();

                        String dataStr = data.toString();
                        Charger charger = new Gson().fromJson(dataStr, Charger.class);

                        hideProgressDialog();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String maintainedByStr = getString(R.string.charger_maintained_by);
                                if(charger.getCompany() != null && charger.getCompany().getName() != null) {
                                   String text = "<font color=#000000>" + maintainedByStr + " </font> <font color=#6cd387>" + charger.getCompany().getName() + "</font>";
                                    mServiceProviderTextView.setText(Html.fromHtml(text));

                                    findViewById(R.id.bolt_service_provider_message_card_view).setVisibility(View.VISIBLE);
                                } else {
                                    findViewById(R.id.bolt_service_provider_message_card_view).setVisibility(View.GONE);

                                }

                            }
                        });

                    }

                } catch (Exception e) {
                    showErrorAndFinish(getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showErrorAndFinish(getString(R.string.server_error));
            }
        });

    }

    private void showErrorAndFinish(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BoltServiceProviderActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }
}
