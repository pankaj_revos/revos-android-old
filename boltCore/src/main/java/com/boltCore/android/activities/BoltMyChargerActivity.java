package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.boltCore.android.adapters.MyChargerListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.boltCore.android.R;
import com.boltCore.android.jsonStructures.Charger;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.boltCore.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class BoltMyChargerActivity extends  BoltBaseActivity {
    private ProgressDialog mProgressDialog;
    private RecyclerView mChargerListRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_my_chargers_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

    }

    private void setupViews() {
        findViewById(R.id.bolt_my_charger_list_add_charger_image_view).setOnClickListener(v -> {
            Intent intent = new Intent(this, BoltScanQrCodeToRegisterChargerActivity.class);
            startActivity(intent);
        });

        mChargerListRecyclerView = findViewById(R.id.bolt_my_charger_list_recycler_view);
        mChargerListRecyclerView.setLayoutManager(new LinearLayoutManager(BoltMyChargerActivity.this));

        findViewById(R.id.bolt_my_charger_list_back_button_image_view).setOnClickListener(v -> {finish();});
    }


    @Override
    protected void onResume() {
        super.onResume();

        fetchMyChargers();
    }


    private void fetchMyChargers() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getMyChargers(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressDialog();
                try {
                    String str = response.body().string();
                    JsonObject bodyJsonObject = new JsonParser().parse(str).getAsJsonObject();
                    if(bodyJsonObject.has("data")) {
                        JsonArray dataArray = bodyJsonObject.getAsJsonArray("data");
                        ArrayList<Charger> chargers = new ArrayList<>();

                        for(JsonElement jsonElement : dataArray) {
                            Charger charger = new Gson().fromJson(jsonElement.getAsJsonObject().toString(), Charger.class);
                            chargers.add(charger);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                refreshChargerList(chargers);
                            }
                        });
                    }
                } catch (Exception e) {
                    showServerError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void refreshChargerList(ArrayList<Charger> chargers) {
        MyChargerListAdapter myChargerListAdapter = new MyChargerListAdapter(chargers, BoltMyChargerActivity.this);
        mChargerListRecyclerView.setAdapter(myChargerListAdapter);
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltMyChargerActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
