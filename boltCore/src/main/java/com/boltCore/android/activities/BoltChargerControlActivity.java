package com.boltCore.android.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.boltCore.android.utilities.general.Version;
import com.google.gson.Gson;
import com.boltCore.android.R;
import com.boltCore.android.utilities.ble.BoltBleManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import at.grabner.circleprogress.CircleProgressView;
import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.boltCore.android.activities.BoltBookChargerActivity.BOOKING_OBJECT_ARG_KEY;
import static com.boltCore.android.constants.Constants.VERSION_1_0_0;

public class BoltChargerControlActivity extends  BoltBaseActivity {
    private CircleProgressView mCircleProgressView;
    private Timer mOneSecondTimer;
    private Button mStartStopButton;

    private TextView mTimeRemainingTextView, mStatusTextView, mEnergyDispensedTextView, mActiveBookingIdTextView;
    private LottieAnimationView mWaitLottieView, mChargingLottieView, mDischargingLottieView;

    private final int CHARGER_STATUS_UNKNOWN = 0;
    private final int CHARGER_STATUS_ON = 1;
    private final int CHARGER_STATUS_OFF = 2;

    private int count = 0;

    private ProgressDialog mProgressDialog;

    private int mChargerPowerState;

    private Booking mBooking;

    private BoltBleManager mBoltBleManager;

    private boolean mForceShowLottieViewForASecond = false;

    /**Variables and handler for scanner*/
    private Handler mHandler;
    private BluetoothLeScannerCompat mScanner;
    private static boolean mIsScanning = false;
    private static boolean mDeviceFound = false;
    private final long SCAN_DURATION = 30000;

    private boolean mIsPreviousSessionEnergyConsumedUpdated, mIsCurrentBookingIdSet;
    private long mCurrentSessionEnergyConsumedUpdatedTimeStamp, mChargerPingedTimeStamp;

    private final String INVALID_CHARGE_ID = "DEFAULTCHARGEID";

    private final String ACTIVE = "ACTIVE";

    private int REQUEST_LOCATION_PERMISSION_REQ_CODE = 200;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};


    private static boolean mForceDisconnect = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_charger_control_activity);
        initialiseAnalytics(getApplicationContext());

        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(BOOKING_OBJECT_ARG_KEY, null) == null) {
            finish();
            return;
        }

        mBooking = new Gson().fromJson(bundle.getString(BOOKING_OBJECT_ARG_KEY), Booking.class);

        if(mBooking == null) {
            finish();
            return;
        }

        setupRelatedBleVariables();

        setupViews();

        setupTimer();

    }

    private void setupRelatedBleVariables() {
        mHandler = new Handler(getMainLooper());
        mBoltBleManager = new BoltBleManager(BoltChargerControlActivity.this);
        mScanner = BluetoothLeScannerCompat.getScanner();
    }

    private void setupTimer() {
        mOneSecondTimer = new Timer();

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!hasWindowFocus()) {
                            return;
                        }
                        updateChargerState();
                        updateViews();
                        connectToDeviceIfRequired();
                        endActivityIfRequired();
                        updateCurrentBookingIdInChargerIfRequired();
                        updateCurrentBookingEnergyConsumedIfRequired();
                        updatePreviousBookingEnergyConsumedIfRequired();
                        pingChargerIfRequired();
                    }
                });

            }
        }, 100, 1000);
    }

    private void updateCurrentBookingIdInChargerIfRequired() {
        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())
                && mBoltBleManager.getChargerPowerState() != CHARGER_STATUS_UNKNOWN
                && !mIsCurrentBookingIdSet) {

            mBoltBleManager.setCurrentBookingId(mBooking.getId());

            mIsCurrentBookingIdSet = true;
        }
    }

    private void pingChargerIfRequired() {
        if(!mBoltBleManager.isConnected() || mBoltBleManager.getBluetoothDevice() == null || (System.currentTimeMillis() - mChargerPingedTimeStamp) < 60000) {
            return;
        }

        String name = mBoltBleManager.getBluetoothDevice().getName();

        //decide and add this BleNameHashSet
        if(name != null && !name.isEmpty()) {
            String[] tokens = name.split("_");

            if (tokens.length == 2 && tokens[0].equals("BOLT") && tokens[1].length() == 12) {
                pingCharger(name);
            }
        }
    }

    private void pingCharger(String bleName) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Timber.d("pinging for : " + bleName);

        apiInterface.pingCharger(bleName, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String bodyStr = response.body().string();
                    Timber.d("ping response : " + bodyStr);
                    mChargerPingedTimeStamp = System.currentTimeMillis();
                } catch (Exception e) {
                    //do nothing
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void updateCurrentBookingEnergyConsumedIfRequired() {
        String infoString = mBoltBleManager.getCurrentBookingEnergyConsumptionString();

        if(infoString == null || infoString.isEmpty() || (System.currentTimeMillis() - mCurrentSessionEnergyConsumedUpdatedTimeStamp) < 60000) {
            return;
        }

        String[] tokens = null;
        String bookingId = null;
        String energyConsumedStr = null;
        float energyInKWh;

        try {
            tokens = infoString.split(",");
            bookingId = tokens[0];
            energyConsumedStr = tokens[1];

            float energyInWh = Long.parseLong(energyConsumedStr) / 100.0f;

            energyInKWh = energyInWh / 1000;

            if(bookingId.contains(INVALID_CHARGE_ID)) {
                return;
            }

        } catch (Exception e) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("energyConsumed", String.valueOf(energyInKWh));

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.updateBooking(bookingId,BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    Timber.d("Current booking energyUpdated : " + str);
                    mCurrentSessionEnergyConsumedUpdatedTimeStamp = System.currentTimeMillis();
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void updatePreviousBookingEnergyConsumedIfRequired() {
        String infoString = mBoltBleManager.getPreviousBookingEnergyConsumptionString();

        if(infoString == null || infoString.isEmpty() || mIsPreviousSessionEnergyConsumedUpdated) {
            return;
        }

        String[] tokens = null;
        String bookingId = null;
        String energyConsumedStr = null;
        float energyInKWh;

        try {
            tokens = infoString.split(",");
            bookingId = tokens[0];
            energyConsumedStr = tokens[1];

            float energyInWh = Long.parseLong(energyConsumedStr) / 100.0f;

            energyInKWh = energyInWh / 1000;

            if(bookingId.contains(INVALID_CHARGE_ID)) {
                return;
            }

        } catch (Exception e) {
            return;
        }

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("energyConsumed", String.valueOf(energyInKWh));

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.updateBooking(bookingId, BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    Timber.d("previous booking energyUpdated : " + str);
                    mIsPreviousSessionEnergyConsumedUpdated = true;
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    private void updateChargerState() {
        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())) {

            mChargerPowerState = mBoltBleManager.getChargerPowerState();
        } else {
            mChargerPowerState = CHARGER_STATUS_UNKNOWN;
        }
    }

    private void endActivityIfRequired() {
        DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(mBooking.getEndTime());

        DateTime currentTime = new DateTime();

        if(currentTime.plusSeconds(3).isAfter(bookingEndTime.toInstant())) {
            mForceDisconnect = true;
            //booking has expired disconnect and end activity
            if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
                mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        endActivity();
                    }
                }, new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        endActivity();
                    }
                });
            } else {
                endActivity();
            }
        }
    }

    private void connectToDeviceIfRequired() {
        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())) {
            return;
        }

        connectToDeviceHelper();
    }

    private void setupViews()  {
        mActiveBookingIdTextView = findViewById(R.id.bolt_control_active_booking_id_text_view);
        mTimeRemainingTextView = findViewById(R.id.bolt_control_time_remaining_text_view);
        mEnergyDispensedTextView = findViewById(R.id.bolt_control_energy_dispensed_text_view);
        mStatusTextView = findViewById(R.id.bolt_control_status_text_view);
        mWaitLottieView = findViewById(R.id.bolt_control_wait_lottie_view);
        mChargingLottieView = findViewById(R.id.bolt_control_charging_lottie_view);
        mDischargingLottieView = findViewById(R.id.bolt_control_discharging_lottie_view);

        mCircleProgressView = findViewById(R.id.bolt_control_circle_progress_view);

        mStartStopButton = findViewById(R.id.bolt_control_button);

        mStartStopButton.setOnClickListener(v -> {
            if(mBoltBleManager != null
                    && mBoltBleManager.isConnected()
                    && mBoltBleManager.getBluetoothDevice() != null
                    && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())
                    && mBoltBleManager.isAuthenticationComplete()) {
                if(mChargerPowerState == CHARGER_STATUS_OFF || mChargerPowerState == CHARGER_STATUS_UNKNOWN) {
                    //total charge time in seconds
                    DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(mBooking.getEndTime());
                    DateTime currentTime = new DateTime();
                    long remainingTime = bookingEndTime.getMillis() - currentTime.getMillis();

                    mBoltBleManager.turnChargerOn(mBooking.getId(), (int)(remainingTime / 1000));
                } else if(mChargerPowerState == CHARGER_STATUS_ON) {
                    mBoltBleManager.turnChargerOff(mBooking.getId());
                }
            }

            mForceShowLottieViewForASecond = true;
            mWaitLottieView.setVisibility(View.VISIBLE);
            mStartStopButton.setVisibility(View.INVISIBLE);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mForceShowLottieViewForASecond = false;
                    mWaitLottieView.setVisibility(View.INVISIBLE);
                    mStartStopButton.setVisibility(View.VISIBLE);
                }
            }, 1000);
        });

        findViewById(R.id.bolt_control_end_booking_button).setOnClickListener(v -> {
            showEndBookingDialog();
        });

        ((TextView)findViewById(R.id.bolt_control_charger_id_text_view)).setText(getString(R.string.charger_id_with_colon)+ " " + mBooking.getAsset().getUID());

        String trimmedBookingId = mBooking.getId().substring(16);
        mActiveBookingIdTextView.setText(getString(R.string.booking_id_with_colon) + " " + trimmedBookingId);

        findViewById(R.id.bolt_control_call_owner_button).setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+mBooking.getAsset().getContact()));
            startActivity(intent);
        });
    }

    private void showEndBookingDialog() {
        final Dialog endBookingDialog = new Dialog(BoltChargerControlActivity.this);
        endBookingDialog.setContentView(R.layout.end_booking_dialog);

        endBookingDialog.findViewById(R.id.end_booking_yes_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endBooking();
                endBookingDialog.dismiss();
            }
        });

        endBookingDialog.findViewById(R.id.end_booking_no_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endBookingDialog.dismiss();
            }
        });

        endBookingDialog.show();
    }

    private void endBooking() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null || mBooking == null) {
            return;
        }

        showProgressDialog();

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("remarks", "endedByUser");

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        apiInterface.endBooking(mBooking.getId(), BoltSDK.getInstance().getApiKey(), bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200) {
                    showServerError();
                } else {
                    //clear booking
                    if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
                        mBoltBleManager.turnChargerOff(mBooking.getId());
                        mForceDisconnect = true;
                        mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                            @Override
                            public void onRequestCompleted(@NonNull BluetoothDevice device) {
                                endActivity();
                            }
                        }, new FailCallback() {
                            @Override
                            public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                                endActivity();
                            }
                        });
                    } else {
                        endActivity();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });

    }

    private void updateViews() {
        if(mIsScanning && !mBoltBleManager.isConnected()) {
            mStatusTextView.setText(getString(R.string.searching_for_charger));

            mStartStopButton.setVisibility(View.INVISIBLE);
            mStatusTextView.setVisibility(View.VISIBLE);
            mWaitLottieView.setVisibility(View.VISIBLE);
        }

        //update bolt views

        if(mChargerPowerState == CHARGER_STATUS_ON) {
            mChargingLottieView.setVisibility(View.VISIBLE);
            mDischargingLottieView.setVisibility(View.INVISIBLE);
        } else if(mChargerPowerState == CHARGER_STATUS_OFF) {
            mChargingLottieView.setVisibility(View.INVISIBLE);
            mDischargingLottieView.setVisibility(View.VISIBLE);
        } else {
            mChargingLottieView.setVisibility(View.INVISIBLE);
            mDischargingLottieView.setVisibility(View.INVISIBLE);
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())) {

            //we're connected to the correct charger, now check if authentication is complete or not
            if(!mBoltBleManager.isAuthenticationComplete() || mBoltBleManager.getChargerPowerState() == CHARGER_STATUS_UNKNOWN || !mIsCurrentBookingIdSet) {
                mStatusTextView.setText(getString(R.string.connecting_to_charger));

                mStartStopButton.setVisibility(View.INVISIBLE);
                mStartStopButton.setVisibility(View.INVISIBLE);
                mWaitLottieView.setVisibility(View.VISIBLE);
                mStatusTextView.setVisibility(View.VISIBLE);
            } else if(mChargerPowerState == CHARGER_STATUS_ON) {
                mStartStopButton.setText(getString(R.string.stop_charging));
                mStartStopButton.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.stop_charging_button_filled_background));

                if(!mForceShowLottieViewForASecond) {
                    mStartStopButton.setVisibility(View.VISIBLE);
                    mWaitLottieView.setVisibility(View.INVISIBLE);
                    mStatusTextView.setVisibility(View.INVISIBLE);
                }
            } else if(mChargerPowerState == CHARGER_STATUS_OFF) {
                mStartStopButton.setText(getString(R.string.start_charging));
                mStartStopButton.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.start_charging_button_filled_background));

                if(!mForceShowLottieViewForASecond) {
                    mStartStopButton.setVisibility(View.VISIBLE);
                    mWaitLottieView.setVisibility(View.INVISIBLE);
                    mStatusTextView.setVisibility(View.INVISIBLE);
                }
            }
        }


        DateTime bookingStartTime = ISODateTimeFormat.dateTimeParser().parseDateTime(mBooking.getStartTime());
        DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(mBooking.getEndTime());

        DateTime currentTime = new DateTime();

        long totalTime = bookingEndTime.getMillis() - bookingStartTime.getMillis();
        long remainingTime = bookingEndTime.getMillis() - currentTime.getMillis();

        float progress = (remainingTime * 100f / totalTime);

        int seconds = ((int)(remainingTime / 1000)) % 60 ;
        int minutes = ((int)(remainingTime / (1000*60)) % 60);
        int hours   = ((int)(remainingTime / (1000*60*60)) % 24);

        String hourStr = "";
        String minuteStr = "";
        String secondStr = "";

        int suffixId;

        suffixId = R.string.hourSingle;
        if(hours > 0) {
            if(hours > 1) {
                suffixId = R.string.hourPlural;
            }
            hourStr = String.valueOf(hours) + " " + getString(suffixId) + " ";
        }

        suffixId = R.string.minuteSingle;
        if(minutes > 0) {
            if(minutes > 1) {
                suffixId = R.string.minutePlural;
            }
            minuteStr = String.valueOf(minutes) + " " + getString(suffixId) + " ";
        }

        suffixId = R.string.secondSingle;
        if(seconds > 0) {
            if(seconds > 1) {
                suffixId = R.string.secondPlural;
            }
            secondStr = String.valueOf(seconds) + " " + getString(suffixId) + " ";
        }

        mCircleProgressView.setValueAnimated(progress);

        if(hours > 0) {
            mTimeRemainingTextView.setText(hourStr + minuteStr);
        } else if(minutes > 0){
            mTimeRemainingTextView.setText(minuteStr);
        } else {
            mTimeRemainingTextView.setText(secondStr);
        }

        String infoString = mBoltBleManager.getCurrentBookingEnergyConsumptionString();

        if(!mBoltBleManager.isConnected()) {
            mEnergyDispensedTextView.setText("");
            return;
        }

        String[] tokens = null;
        String bookingId = null;
        String energyConsumedStr = null;
        float energyInKWh;

        try {
            tokens = infoString.split(",");
            bookingId = tokens[0];
            energyConsumedStr = tokens[1];

            float energyInWh = Long.parseLong(energyConsumedStr) / 100.0f;

            energyInKWh = energyInWh / 1000;

            mEnergyDispensedTextView.setText(getString(R.string.energy_dispensed) + " : " + String.format("%.3f", energyInKWh) + " kWh");

        } catch (Exception e) {
            //nothing to do here
        }

        if(mBoltBleManager.getChargerFirmwareVersion() != null
                && !mBoltBleManager.getChargerFirmwareVersion().isEmpty()
                && new Version(mBoltBleManager.getChargerFirmwareVersion()).compareTo(new Version(VERSION_1_0_0)) > 0) {
            mEnergyDispensedTextView.setVisibility(View.VISIBLE);
        } else {
            mEnergyDispensedTextView.setVisibility(View.INVISIBLE);
        }
    }




    private void connectToDeviceHelper() {
        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
            bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(mBoltBleManager != null
                && mBoltBleManager.isConnected()
                && mBoltBleManager.getBluetoothDevice() != null
                && mBoltBleManager.getBluetoothDevice().getAddress().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())) {
            return;
        }

        try {
            startScan();
        } catch (Exception e) {
            if(mBoltBleManager != null) {
                mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {

                    }
                }, new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                    }
                });
            }

            finish();
        }
    }


    /**
     * Scan for 5 seconds and then stop scanning when a BluetoothLE device is found then mLEScanCallback
     * is activated This will perform regular scan for custom BLE Service UUID and then filter out.
     * using class ScannerServiceParser
     */
    private synchronized void startScan() {

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            //todo::send message to show a dialog
            return;
        }

        if(mIsScanning || mBoltBleManager.isConnected()) {
            return;
        }

        mDeviceFound = false;
        mIsScanning = true;

        mScanner.startScan(scanCallback);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScanHelper();
                }
            }
        }, SCAN_DURATION);
    }

    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final no.nordicsemi.android.support.v18.scanner.ScanResult result) {
            Timber.d("Device found : " + result.getDevice().getAddress());
            if(result.getDevice().getAddress().trim().toUpperCase().equals(mBooking.getAsset().getBleMac().toUpperCase())) {

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceFound = true;
                        stopScanHelper();
                        if(mBoltBleManager != null && mBoltBleManager.getConnectionState() == BluetoothProfile.STATE_DISCONNECTED && !mForceDisconnect) {
                            mBoltBleManager.connectToDevice(mBooking.getAsset().getBleMac().toUpperCase(), mBooking.getAsset().getKey());
                        }
                    }
                });
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };

    private void stopScanHelper() {
        mScanner.stopScan(scanCallback);
        mIsScanning = false;
    }

    @Override
    protected void onDestroy() {
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mForceDisconnect = true;
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if(mBoltBleManager != null && mBoltBleManager.isConnected()) {
            mForceDisconnect = true;
            mBoltBleManager.disconnectFromDevice(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {

                }
            }, new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {

                }
            });
        }
        stopScanHelper();
        super.onBackPressed();

    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void endActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fetchCurrentBookingAndEndActivityIfRequired(true);
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltChargerControlActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mForceDisconnect = false;

        fetchCurrentBookingAndEndActivityIfRequired(false);

        showLocationPermissionDialogIfRequired();
    }

    private void showLocationPermissionDialogIfRequired() {
        if (ActivityCompat.checkSelfPermission(BoltChargerControlActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BoltChargerControlActivity.this, mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_LOCATION_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            showLocationPermissionDialogIfRequired();
        }

    }

    private void fetchCurrentBookingAndEndActivityIfRequired(boolean forceFinish) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getUserBookings(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() != 200 || response.body() == null) {
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String responseBody = null;
                        try {
                            responseBody = response.body().string();


                            JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                            if (responseJsonObject.has("data")) {
                                JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                                String dataStr = data.toString();
                                ArrayList<Booking> bookings = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Booking>>() {
                                }.getType());

                                getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).edit().putString(Constants.ACTIVE_BOOKINGS_KEY, null).apply();

                                ArrayList<Booking> activeBookings = new ArrayList<>();
                                for(Booking booking : bookings) {
                                    if(booking.getStatus().equals(ACTIVE)) {
                                        activeBookings.add(booking);
                                    }
                                }

                                getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).edit().putString(Constants.ACTIVE_BOOKINGS_KEY, new Gson().toJson(activeBookings)).apply();
                            }

                            String latestActiveBookingStr = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.ACTIVE_BOOKINGS_KEY, null);

                            if (latestActiveBookingStr == null) {
                                //no active booking, finish this activity
                                finish();
                            }

                            if(forceFinish) {
                                finish();
                            }

                        } catch (Exception e) {
                            if(forceFinish) {
                                finish();
                            }
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(forceFinish) {
                    finish();
                }
            }
        });
    }

}
