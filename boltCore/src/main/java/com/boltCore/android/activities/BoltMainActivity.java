package com.boltCore.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boltCore.android.adapters.BoltMainActivityViewPagerAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.events.EventBusMessage;
import com.boltCore.android.jsonStructures.Booking;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.boltCore.android.R;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.boltCore.android.activities.BoltBookChargerActivity.BOOKING_OBJECT_ARG_KEY;
import static com.boltCore.android.constants.Constants.BLE_EVENT_DEVICE_FOUND;
import static com.boltCore.android.constants.Constants.BLE_EVENT_SCAN_STOP;
import static com.boltCore.android.constants.Constants.GEN_DELIMITER;

public class BoltMainActivity extends BoltBaseActivity implements TabLayout.OnTabSelectedListener {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    /** drawables for the tabs*/
    private int[] drawablesIds;

    private LinearLayout mActiveBookingLayout;
    private TextView mActiveBookingTextView;

    private ProgressDialog mProgressDialog;

    private final String ACTIVE = "ACTIVE";

    /** titles for the tabs*/
    private CharSequence mTitles[] = {"Search", "Scan", "Profile"};

    /**Variables and handler for scanner*/
    private BluetoothLeScannerCompat mScanner;
    private Handler mHandler;
    private static boolean mIsScanning = false;
    private final long SCAN_DURATION = 30000;

    private HashSet<String> mBleNameHashSet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bolt_main_activity);
        initialiseAnalytics(getApplicationContext());
        setupRelatedBleVariables();
        setupViews();
    }

    private void setupViews() {
        mActiveBookingLayout = findViewById(R.id.bolt_main_active_booking_layout);
        mActiveBookingTextView = findViewById(R.id.bolt_main_active_booking_text_view);

        mActiveBookingLayout.setOnClickListener(v -> {
            launchActivityBasedOnNumberOfActiveBookings();
        });

        setupSlidingTabLayout();
    }

    private void launchActivityBasedOnNumberOfActiveBookings() {
        String activeBookingsStr = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.ACTIVE_BOOKINGS_KEY, null);


        if(activeBookingsStr == null) {
            return;
        } else {
            ArrayList<Booking> activeBookings = new Gson().fromJson(activeBookingsStr, new TypeToken<ArrayList<Booking>>(){}.getType());

            ArrayList<Booking> bookingActiveBasedOnTimeList = new ArrayList<>();
            for(Booking booking : activeBookings) {
                DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(booking.getEndTime());
                DateTime currentTime = new DateTime();

                if (currentTime.isBefore(bookingEndTime.toInstant())) {
                    bookingActiveBasedOnTimeList.add(booking);
                }
            }

            if(bookingActiveBasedOnTimeList.size() == 1) {
                launchBoltChargerControlActivity(new Gson().toJson(bookingActiveBasedOnTimeList.get(0)));
            } else if(bookingActiveBasedOnTimeList.size() > 1) {
                launchActiveBookingActivity();
            }
        }
    }


    private void setupSlidingTabLayout() {
        //initialize the drawable id
        drawablesIds = new int[]{R.drawable.ic_svg_charging_pin_green, R.drawable.ic_scan_green, R.drawable.ic_bullet_list_green};

        //Initializing the tablayout
        mTabLayout = (TabLayout) findViewById(R.id.bolt_main_tab_layout);

        //Adding the tabs using addTab() method
        for (int tabIndex = 0; tabIndex < mTitles.length; ++tabIndex) {
            View customView = getLayoutInflater().inflate(R.layout.tab_custom_view, null);
            customView.findViewById(R.id.icon).setBackgroundResource(drawablesIds[tabIndex]);
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(customView));

            //mTabLayout.addTab(mTabLayout.newTab().setIcon(drawablesIds[tabIndex]));
        }

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        mViewPager = (ViewPager) findViewById(R.id.bolt_main_view_pager);
        mViewPager.setOffscreenPageLimit(2);

        //Creating our pager adapter
        BoltMainActivityViewPagerAdapter adapter = new BoltMainActivityViewPagerAdapter(getSupportFragmentManager(), mTitles, mTabLayout.getTabCount(), drawablesIds, BoltMainActivity.this);

        //Adding adapter to pager
        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //Adding onTabSelectedListener to swipe views
        mTabLayout.addOnTabSelectedListener(this);

        //setting up the tab indicator color
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorSelectedTabIndicator, typedValue, true);
        @ColorInt int selectedTabIndicatorColor = typedValue.data;
        mTabLayout.setSelectedTabIndicatorColor(selectedTabIndicatorColor);


        //setting the obd tab as default
        mTabLayout.getTabAt(0).select();

        //set the vertical divider between the tabs
        View root = mTabLayout.getChildAt(0);
        if(root instanceof LinearLayout) {
            theme.resolveAttribute(R.attr.colorTabDivider, typedValue, true);
            @ColorInt int tabDividerColor = typedValue.data;

            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(tabDividerColor);
            drawable.setSize(2, 1);
            //((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition(), true);

        EventBus.getDefault().post(new EventBusMessage(Constants.GEN_EVENT_TAB_CHANGE + GEN_DELIMITER + tab.getPosition()));

        if(tab.getPosition() != 0) {
            toggleAndRefreshActiveBookingLayout(false);
        } else {
            toggleAndRefreshActiveBookingLayout(true);
        }

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void fetchCurrentBooking() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        toggleAndRefreshActiveBookingLayout(true);

        toggleProgressBarLayout(true);

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getUserBookings(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toggleProgressBarLayout(false);
                    }
                });

                if(response.code() != 200 || response.body() == null) {
                    return;
                }

                try {

                    String responseBody = response.body().string();

                    JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                    if(responseJsonObject.has("data")) {
                        JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                        String dataStr = data.toString();
                        ArrayList<Booking> bookings = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Booking>>(){}.getType());

                        getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).edit().putString(Constants.ACTIVE_BOOKINGS_KEY, null).apply();


                        ArrayList<Booking> activeBookings = new ArrayList<>();
                        for(Booking booking : bookings) {
                            if(booking.getStatus().equals(ACTIVE)) {
                                activeBookings.add(booking);
                            }
                        }

                        getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).edit().putString(Constants.ACTIVE_BOOKINGS_KEY, new Gson().toJson(activeBookings)).apply();
                    }

                    toggleAndRefreshActiveBookingLayout(true);

                } catch (Exception e) {
                    //do nothing here
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toggleProgressBarLayout(false);
                    }
                });
            }
        });
    }

    private void launchBoltChargerControlActivity(String bookingObjectArgKey) {
        Intent intent = new Intent(this, BoltChargerControlActivity.class);
        intent.putExtra(BOOKING_OBJECT_ARG_KEY, bookingObjectArgKey);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        fetchCurrentBooking();

        //switch on bluetooth
        if(BluetoothAdapter.getDefaultAdapter() != null) {
            if(!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                BluetoothAdapter.getDefaultAdapter().enable();
            }
        }

        startScan();
    }

    private void toggleProgressBarLayout(boolean showLayout) {
        int visibility = showLayout ? View.VISIBLE : View.INVISIBLE;
        findViewById(R.id.bolt_main_progress_bar_layout).setVisibility(visibility);
    }

    private void toggleAndRefreshActiveBookingLayout(boolean showLayout) {
        int visibility = showLayout ? View.VISIBLE : View.INVISIBLE;


        String activeBookingsStr = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.ACTIVE_BOOKINGS_KEY, null);

        int bookingActiveBasedOnTime = 0;

        try {

            if (activeBookingsStr == null) {
                mActiveBookingLayout.setVisibility(View.INVISIBLE);
            } else {
                ArrayList<Booking> activeBookings = new Gson().fromJson(activeBookingsStr, new TypeToken<ArrayList<Booking>>() {
                }.getType());

                for (Booking booking : activeBookings) {
                    DateTime bookingEndTime = ISODateTimeFormat.dateTimeParser().parseDateTime(booking.getEndTime());
                    DateTime currentTime = new DateTime();

                    if (currentTime.isBefore(bookingEndTime.toInstant())) {
                        ++bookingActiveBasedOnTime;
                    }
                }

                if (bookingActiveBasedOnTime > 0) {
                    mActiveBookingTextView.setText(String.valueOf(bookingActiveBasedOnTime));
                    visibility = View.VISIBLE;
                } else {
                    visibility = View.INVISIBLE;
                }
            }

            if (mTabLayout != null && mTabLayout.getSelectedTabPosition() != 0) {
                visibility = View.INVISIBLE;
            }

            mActiveBookingLayout.setVisibility(visibility);
        } catch (Exception e) {
            mActiveBookingLayout.setVisibility(View.INVISIBLE);
        }
    }


    private void launchActiveBookingActivity() {
        Intent intent = new Intent(getApplicationContext(), BoltActiveBookingListActivity.class);
        startActivity(intent);
    }

    /**
     * Scan for 30 seconds and then stop scanning when a BluetoothLE device is found then mLEScanCallback
     * is activated This will perform regular scan for custom BLE Service UUID and then filter out.
     * using class ScannerServiceParser
     */
    public synchronized void startScan() {

        //switch on bluetooth
        if(BluetoothAdapter.getDefaultAdapter() != null) {
            if(!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                BluetoothAdapter.getDefaultAdapter().enable();

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startScan();
                    }
                }, 5000);

                return;
            }
        } else {
            return;
        }

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            //todo::send message to show a dialog
            return;
        }

        stopScanHelper();

        mIsScanning = true;

        mBleNameHashSet = new HashSet<>();

        mScanner.startScan(scanCallback);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mIsScanning) {
                    stopScanHelper();
                }
            }
        }, SCAN_DURATION);
    }

    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final no.nordicsemi.android.support.v18.scanner.ScanResult result) {
            Timber.d("Device found : " + result.getDevice().getAddress());
            //broadcast this result to

            if(result.getDevice() == null || result.getDevice().getAddress() == null || result.getDevice().getAddress().isEmpty()) {
                return;
            }

            String address = result.getDevice().getAddress();
            String name = result.getDevice().getName();

            //decide and add this BleNameHashSet
            if(name != null && !name.isEmpty()) {
                String[] tokens = name.split("_");

                if (tokens.length == 2 && tokens[0].equals("BOLT") && tokens[1].length() == 12) {
                    if(mBleNameHashSet == null) {
                        mBleNameHashSet = new HashSet<>();
                    }

                    if(!mBleNameHashSet.contains(name)) {
                        pingCharger(name);
                    }

                    mBleNameHashSet.add(name);
                }
            }

            EventBus.getDefault().post(new EventBusMessage(BLE_EVENT_DEVICE_FOUND + GEN_DELIMITER + address));
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };

    private void stopScanHelper() {
        mScanner.stopScan(scanCallback);
        mIsScanning = false;
        EventBus.getDefault().post(new EventBusMessage(BLE_EVENT_SCAN_STOP));
    }

    private void setupRelatedBleVariables() {

        mHandler = new Handler(getMainLooper());
        mScanner = BluetoothLeScannerCompat.getScanner();
    }

    private void pingCharger(String bleName) {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Timber.d("pinging for : " + bleName);

        apiInterface.pingCharger(bleName, BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String bodyStr = response.body().string();
                    Timber.d("ping response : " + bodyStr);
                } catch (Exception e) {
                    //do nothing
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public boolean isBleScanInProgress() {
        return mIsScanning;
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BoltMainActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onPause() {
        hideProgressDialog();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        stopScanHelper();
        super.onDestroy();
    }
}
