package com.boltCore.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.boltCore.android.R;
import com.boltCore.android.adapters.PastOrderListAdapter;
import com.boltCore.android.constants.Constants;
import com.boltCore.android.jsonStructures.Order;
import com.boltCore.android.retrofit.ApiClient;
import com.boltCore.android.retrofit.ApiInterface;
import com.boltCore.android.sdk.BoltSDK;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BoltPastOrdersActivity extends  BoltBaseActivity {
    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bolt_past_order_activity);
        initialiseAnalytics(getApplicationContext());
        setupViews();

        fetchOrders();
    }

    private void setupViews() {
        findViewById(R.id.bolt_past_order_support_button).setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:+917204490952"));
            startActivity(intent);
        });

        findViewById(R.id.bolt_past_order_list_back_button_image_view).setOnClickListener(v -> {
            finish();
        });
    }

    private void fetchOrders() {
        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        showProgressDialog();

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getPastOrders(BoltSDK.getInstance().getApiKey(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.code() != 200 || response.body() == null) {
                            showServerError();
                            return;
                        }

                        try {

                            String responseBody = response.body().string();

                            JsonObject responseJsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
                            if(responseJsonObject.has("data")) {
                                JsonArray data = responseJsonObject.get("data").getAsJsonArray();

                                String dataStr = data.toString();
                                ArrayList<Order> orderList = new Gson().fromJson(dataStr, new TypeToken<ArrayList<Order>>(){}.getType());

                                if(orderList.isEmpty()) {
                                    showNoOrdersFound();
                                    return;
                                }


                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setupViews(orderList);
                                    }
                                });
                            }

                        } catch (Exception e) {
                            showServerError();
                        }

                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showServerError();
            }
        });
    }

    private void setupViews(ArrayList<Order> orderList) {

        //sort orders based on date, latest first
        Collections.sort(orderList, new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {
                DateTime dateTime1 = new DateTime(o1.getCreatedAt());
                DateTime dateTime2 = new DateTime(o2.getCreatedAt());

                return dateTime2.compareTo(dateTime1);
            }
        });


        findViewById(R.id.bolt_past_order_list_back_button_image_view).setOnClickListener(v -> {finish();});

        mRecyclerView = findViewById(R.id.bolt_past_order_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(BoltPastOrdersActivity.this));

        PastOrderListAdapter pastOrderListAdapter = new PastOrderListAdapter(orderList, BoltPastOrdersActivity.this);
        mRecyclerView.setAdapter(pastOrderListAdapter);
    }



    private void showNoOrdersFound() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_past_orders));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showServerError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BoltPastOrdersActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }

}
