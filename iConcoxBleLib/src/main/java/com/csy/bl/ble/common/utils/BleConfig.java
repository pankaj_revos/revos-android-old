package com.csy.bl.ble.common.utils;

import com.csy.bl.ble.menggou.utils.Constants;

import java.util.UUID;

/**
 * des it399.com
 *@author ChenShouYin
 *@time 2019\1\21 0021 14:13
 */
public class BleConfig {

    private static BleConfig instance;
    private UUID[] mServiceUuids;//扫描指定服务的设备
    //扫描设备的名字以及是否模糊扫描
    private String[] mDeviceNames;
    private boolean mFuzzy;
    //扫描指定Mac地址的设备
    private String mDeviceMac;
    private int scanTimeout = 10*1000;//扫描超时时间（毫秒）
    private int sendCmdTimeOut = 10*1000;//命令发送超时时间（毫秒）



    private int connectTimeOut = 10*1000;//连接超时时间（毫秒）
    private int connectRetryCount = 3;//连接失败重试次数
    private int connectRetryInterval = 10*1000;//连接失败重试时间间隔
    private boolean  connectRetryAllTime = false;
    private   UUID SERVICE_UUID = UUID
            .fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");

    public UUID getSERVICE_UUID() {
        return SERVICE_UUID;
    }



    public UUID getUUID_READ() {
        return UUID_READ;
    }



    public UUID getUUID_WRITE() {
        return UUID_WRITE;
    }



    private   UUID UUID_READ = UUID
            .fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
    private   UUID UUID_WRITE = UUID
            .fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");

//    private int connectTimeout = DEFAULT_CONN_TIME;//连接超时时间（毫秒）
//    private int operateTimeout = DEFAULT_OPERATE_TIME;//数据操作超时时间（毫秒）
//    private int connectRetryCount = DEFAULT_RETRY_COUNT;//连接重试次数
//    private int connectRetryInterval = DEFAULT_RETRY_INTERVAL;//连接重试间隔（毫秒）
//    private int operateRetryCount = DEFAULT_RETRY_COUNT;//数据操作重试次数
//    private int operateRetryInterval = DEFAULT_RETRY_INTERVAL;//数据操作重试间隔时间（毫秒）
//    private int maxConnectCount = DEFAULT_MAX_CONNECT_COUNT;//最大连接数量

    private BleConfig() {
    }

    public static BleConfig getInstance() {
        if (instance == null) {
            synchronized (BleConfig.class) {
                if (instance == null) {
                    instance = new BleConfig();
                }
            }
        }
        return instance;
    }

    /**
     * 获取发送数据超时时间
     *
     * @return 返回发送数据超时时间
     */
//    public int getOperateTimeout() {
//        return operateTimeout;
//    }

//    /**
//     * 设置发送数据超时时间
//     *
//     * @param operateTimeout 发送数据超时时间
//     * @return 返回ViseBle
//     */
//    public BleConfig setOperateTimeout(int operateTimeout) {
//        this.operateTimeout = operateTimeout;
//        return this;
//    }
//
//    /**
//     * 获取连接超时时间
//     *
//     * @return 返回连接超时时间
//     */
//    public int getConnectTimeout() {
//        return connectTimeout;
//    }
//
//    /**
//     * 设置连接超时时间
//     *
//     * @param connectTimeout 连接超时时间
//     * @return 返回ViseBle
//     */
//    public BleConfig setConnectTimeout(int connectTimeout) {
//        this.connectTimeout = connectTimeout;
//        return this;
//    }
//


    /**
     * 设置扫描超时时间
     * @param scanTimeout 扫描超时时间
     * @return 返回BleConfig
     */
    public BleConfig setScanTimeout(int scanTimeout) {
        this.scanTimeout = scanTimeout;
        return this;
    }


    public BleConfig setServiceUuid(UUID SERVICE_UUID) {
        this.SERVICE_UUID = SERVICE_UUID;
        return this;
    }

    public BleConfig setReadUuid(UUID UUID_READ) {
        this.UUID_READ = UUID_READ;
        return this;
    }

    public BleConfig setWriteUuid(UUID UUID_WRITE) {
        this.UUID_WRITE = UUID_WRITE;
        return this;
    }

    public BleConfig setScanTimeout(UUID SERVICE_UUID) {
        this.SERVICE_UUID = SERVICE_UUID;
        return this;
    }


    /**
     * 设置发送命令超时时间
     * @param sendCmdTimeOut 扫描超时时间
     * @return 返回BleConfig
     */
    public BleConfig setSendCmdTimeOut(int sendCmdTimeOut) {
        this.sendCmdTimeOut = sendCmdTimeOut;
        return this;
    }

    /**
     * 发送命令超时时间
     *
     * @return 返回扫描超时时间
     */
    public int getSendCmdTimeOut() {
        return sendCmdTimeOut;
    }

    /**
     * 获取扫描超时时间
     *
     * @return 返回扫描超时时间
     */
    public int getScanTimeout() {
        return scanTimeout;
    }

    /**
     * 设置扫描指定服务的设备
     * @param mServiceUuids
     * @return
     */
    public BleConfig setServiceUuids(UUID[] mServiceUuids) {
        this.mServiceUuids = mServiceUuids;
        return this;
    }

    public UUID[] getServiceUuids() {
        return mServiceUuids;
    }


    /**
     * 是否指定
     * @param mFuzzy 是否模糊扫描,true全匹配,false只要包含该名字就匹配
     * @param mDeviceNames
     * @return
     */
    public BleConfig setDeviceName(boolean mFuzzy, String... mDeviceNames) {
        this.mFuzzy = mFuzzy;
        this.mDeviceNames = mDeviceNames;
        return this;
    }

    /**
     * 设置扫描指定Mac地址的设备
     * @param mDeviceMac
     * @return
     */
    public BleConfig setDeviceMac(String mDeviceMac) {
        this.mDeviceMac = mDeviceMac;
        return this;
    }


    public boolean ismFuzzy() {
        return mFuzzy;
    }

    public String[] getmDeviceNames() {
        return mDeviceNames;
    }

    public String getmDeviceMac() {
        return mDeviceMac;
    }


    /**
     * 获取连接超时时间
     * @return
     */
    public int getConnectTimeOut() {
        return connectTimeOut;
    }

    /**
     * 设置连接超时时间
     * @param connectTimeOut
     */
    public BleConfig setConnectTimeOut(int connectTimeOut) {
        this.connectTimeOut = connectTimeOut;
        return this;
    }


    /**
     * 获取连接重试次数
     *
     * @return
     */
    public int getConnectRetryCount() {
        return connectRetryCount;
    }

    /**
     * 设置连接重试次数
     *
     * @param connectRetryCount
     * @return
     */
    public BleConfig setConnectRetryCount(int connectRetryCount) {
        this.connectRetryCount = connectRetryCount;
        return this;
    }

    /**
     * 获取连接重试间隔时间
     *
     * @return
     */
    public int getConnectRetryInterval() {
        return connectRetryInterval;
    }

    /**
     * 设置连接重试间隔时间
     *
     * @param connectRetryInterval
     * @return
     */

    public BleConfig setConnectRetryInterval(int connectRetryInterval) {
        this.connectRetryInterval = connectRetryInterval;
        return this;
    }

    public boolean isConnectRetryAllTime() {
        return connectRetryAllTime;
    }

    /**
     *
     * @param connectRetryAllTime  是否永远重试
     * @return
     */
    public BleConfig setConnectRetryAllTime(boolean connectRetryAllTime) {
        this.connectRetryAllTime = connectRetryAllTime;
        return this;
    }

    /**
     * 设置是否输出日志
     * 默认关闭
     *
     * @param enableLog true打开  false关闭
     * @return
     */

    public BleConfig setEnableLog(boolean enableLog) {
        Dbug.openDebug(enableLog);
        return this;
    }

//    /**
//     * 获取连接重试次数
//     *
//     * @return
//     */
//    public int getConnectRetryCount() {
//        return connectRetryCount;
//    }
//
//    /**
//     * 设置连接重试次数
//     *
//     * @param connectRetryCount
//     * @return
//     */
//    public BleConfig setConnectRetryCount(int connectRetryCount) {
//        this.connectRetryCount = connectRetryCount;
//        return this;
//    }
//
//    /**
//     * 获取连接重试间隔时间
//     *
//     * @return
//     */
//    public int getConnectRetryInterval() {
//        return connectRetryInterval;
//    }
//
//    /**
//     * 设置连接重试间隔时间
//     *
//     * @param connectRetryInterval
//     * @return
//     */
//    public BleConfig setConnectRetryInterval(int connectRetryInterval) {
//        this.connectRetryInterval = connectRetryInterval;
//        return this;
//    }
//
//    /**
//     * 获取最大连接数量
//     *
//     * @return
//     */
//    public int getMaxConnectCount() {
//        return maxConnectCount;
//    }
//
//    /**
//     * 设置最大连接数量
//     *
//     * @param maxConnectCount
//     * @return
//     */
//    public BleConfig setMaxConnectCount(int maxConnectCount) {
//        this.maxConnectCount = maxConnectCount;
//        return this;
//    }
//
//    /**
//     * 获取操作数据重试次数
//     *
//     * @return
//     */
//    public int getOperateRetryCount() {
//        return operateRetryCount;
//    }
//
//    /**
//     * 设置操作数据重试次数
//     *
//     * @param operateRetryCount
//     * @return
//     */
//    public BleConfig setOperateRetryCount(int operateRetryCount) {
//        this.operateRetryCount = operateRetryCount;
//        return this;
//    }
//
//    /**
//     * 获取操作数据重试间隔时间
//     *
//     * @return
//     */
//    public int getOperateRetryInterval() {
//        return operateRetryInterval;
//    }
//
//    /**
//     * 设置操作数据重试间隔时间
//     *
//     * @param operateRetryInterval
//     * @return
//     */
//    public BleConfig setOperateRetryInterval(int operateRetryInterval) {
//        this.operateRetryInterval = operateRetryInterval;
//        return this;
//    }
}
