package com.csy.bl.ble.menggou.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by tianlun on 2015/12/25.
 */
public class DateToStringUtils {
    /**
     * 时间戳转换成日期格式字符串
     *
     * @param seconds 精确到秒的字符串
     * @param format
     * @return
     */
    public static String timeStamp2Date(long seconds, String format) {
        if (format == null || format.isEmpty()) format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(seconds));
    }

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param date_str 字符串日期
     * @param format   如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String date2TimeStamp(String date_str, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(date_str).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 取得当前时间戳（精确到秒）
     *
     * @return
     */
    public static String timeStamp() {
        long time = System.currentTimeMillis();
        String t = String.valueOf(time / 1000);
        return t;
    }

    /**
     * 将传进来的秒值转换成 “时：分：秒” 格式的字符串
     * @param millis 传入时间长度长几秒
     * @return  转换个时候的字符
     */
    public static String longToHourMinAndSecondStr(long millis) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss" , Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        Date date = new Date(millis * 1000);
        String dateStr = sdf.format(date);
        return dateStr;
    }

    /**
     * 精确到分
     *
     * @param date
     * @return
     */
    public static String strToHourAndMinStr1(String date) {
        return dateStr1ToDateStr2(date, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
    }

    public static String dateStr1ToDateStr2(String dateStr, String srcFormat,
                                            String toFormat) {
        Date dd = dateStr2Date(dateStr, srcFormat);
        return date2Str(dd, toFormat);
    }

    /**
     * 将指定格式的字符串转成Date
     *
     * @param dateStr 日期字符串
     * @param format  日期字符串的格式 <B>例如:<br>
     *                如果 dateStr="2015-11-12"则format="yy-MM-dd"<br>
     *                如果
     *                dateStr="2015-11-12 23:12:56"则format="yy-MM-dd HH:mm:ss"</B>
     * @return
     */
    public static Date dateStr2Date(String dateStr, String format) {
        Date dd = null;
        try {
            dd = new SimpleDateFormat(format).parse(dateStr);
            return dd;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     *
     * @param millis
     *            毫秒
     * @param format
     *            格式"yy-MM-dd"或者"yy-MM-dd HH:mm:ss"等
     * @return
     */
    public static String longToDateFormat(long millis, String format) {
        Date date = new Date(millis);
        SimpleDateFormat sf = new SimpleDateFormat(format);
        String dateStr = sf.format(date);
        return dateStr;
    }

    /**
     * 将日期按指定格式转换成字符串
     *
     * @param d
     * @param format
     * @return
     */
    public static String date2Str(Date d, String format) {
        String str = new SimpleDateFormat(format).format(d);
        return str;
    }

    /**
     * @param dateStr 日期字符串
     *                日期字符串的格式 <B>例如:<br>
     *                如果 dateStr="2015-11-12"则format="yy-MM-dd"<br>
     *                如果
     *                dateStr="2015-11-12 23:12:56"则format="yy-MM-dd HH:mm:ss"</B>
     * @return
     */
    public static long dateStr2Long(String dateStr) {
        Date d = dateStr2Date(dateStr, "yyyy-MM-dd HH:mm:ss");
        if (d == null) {
            return System.currentTimeMillis();
        } else {
            return d.getTime();
        }
    }

    /**
     * @param dateStr 日期字符串如:dateStr="2015-11-12 23:12"则format="yy-MM-dd HH:mm"</B>
     * @return 对应的时间戳
     */
    public static long dateStr2LongNos(String dateStr) {
        Date d = dateStr2Date(dateStr, "yyyy-MM-dd HH:mm");
        if (d == null) {
            return System.currentTimeMillis();
        } else {
            return d.getTime();
        }
    }

    /**
     * 格式化字符串返回 年月日
     *
     * @param dateStr 日期字符串
     *                日期字符串的格式 <B>例如:<br>
     * @return
     */
    public static String dateStr2StrYMD(String dateStr) {
        return dateStr1ToDateStr2(dateStr, "yyyy-MM-dd", "yyyy-MM-dd");
    }

}
