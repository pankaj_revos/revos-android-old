package com.csy.bl.ble.menggou.utils;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */

public class CmdConstants {
    //秘钥，外部可选修改,所以给外部一个设置的方法
    private static  String CMD_KEY = "3A60432A5C01211F291E0F4E0C132825";

    public static String getCmdKey() {
        return CMD_KEY;
    }

    public static void setCmdKey(String cmdKey) {
        CMD_KEY = cmdKey;
    }

    //获取令牌
    public static String CMD_GET_TOKEN = "060101012D1A683D48271A18316E471A";

    public static String getToken() {
        return Token;
    }

    public static void setToken(String token) {
        Token = token;
        resetCMD();
    }
    public static void resetCMD() {
        CMD_SEND_OPEN_LOCK = "05010101" + Token + "1122334455667788";
        //主机发送关锁通信帧
        CMD_SEND_CLOSE_LOCK = "05010201" + Token + "1122334455667788";
        //查询锁状态
        CMD_SEND_GET_LOCK_STATUS = "05020101" + Token + "1122334455667788";
        //设置借车状态
        CMD_SEND_GET_TAKE_CAR_STATUS = "05030101" + Token + "1122334455667788";
        CMD_SEND_GET_RETURN_CAR_STATUS = "050302" + CMD_SEND_GET_RETURN_CAR_RESON + Token + "1122334455667788";
        //查询梦购车的工作模式
        CMD_SEND_GET_CAR_WORK_MODE = "05040101" + Token + "1122334455667788";
        //设置梦购车的工作模式
        CMD_SEND_SET_CAR_WORK_MODE_01 = "05040201" + "01" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_02 = "05040201" + "02" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_03 = "05040201" + "03" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_04 = "05040201" + "04" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_05 = "05040201" + "05" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_06 = "05040201" + "06" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_07 = "05040201" + "07" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_08 = "05040201" + "08" + Token + "11223344556677";
        CMD_SEND_SET_CAR_WORK_MODE_09 = "05040201" + "09" + Token + "11223344556677";
        //获取电量
        CMD_SEND_GET_BATTERY = "05050101" + Token + "1122334455667788";
        //充电宝功能激活
        CMD_SEND_CHARGE_ENABLE = "05060101" + Token + "1122334455667788";
        //充电宝功能关闭
        CMD_SEND_CHARGE_DISABLE = "05060201" + Token + "1122334455667788";
        //充电宝充电开始
        CMD_SEND_CHARGE_START = "05060301" + Token + "1122334455667788";
        //充电宝功能结束
        CMD_SEND_CHARGE_END = "05060401" + Token + "1122334455667788";
        //获取Mac地址
        CMD_SEND_GET_MAC_ADDRESS = "05070101" + Token + "1122334455667788";
        //获取Imei
        CMD_SEND_GET_IMEI = "05080101" + Token + "1122334455667788";
    }
    //需要获取到Token之后再设置
    public static String Token ;
    //主机发送开锁通信帧
    public static String CMD_SEND_OPEN_LOCK;
    //主机发送关锁通信帧
    public static String CMD_SEND_CLOSE_LOCK;
    //查询锁状态
    public static String CMD_SEND_GET_LOCK_STATUS;
    //设置借车状态
    public static String CMD_SEND_GET_TAKE_CAR_STATUS;

    //设置还车状态 CMD_SEND_GET_RETURN_CAR_RESON还车原因
    public static String CMD_SEND_GET_RETURN_CAR_RESON = "01";
    public static void setCmdSendGetReturnCarReson(String cmdSendGetReturnCarReson) {
        CMD_SEND_GET_RETURN_CAR_RESON = cmdSendGetReturnCarReson;
    }
    public static String CMD_SEND_GET_RETURN_CAR_STATUS;
    //查询梦购车的工作模式
    public static String CMD_SEND_GET_CAR_WORK_MODE;
    //设置梦购车的工作模式
    public static String CMD_SEND_SET_CAR_WORK_MODE_01;
    public static String CMD_SEND_SET_CAR_WORK_MODE_02 ;
    public static String CMD_SEND_SET_CAR_WORK_MODE_03;
    public static String CMD_SEND_SET_CAR_WORK_MODE_04;
    public static String CMD_SEND_SET_CAR_WORK_MODE_05;
    public static String CMD_SEND_SET_CAR_WORK_MODE_06;
    public static String CMD_SEND_SET_CAR_WORK_MODE_07;
    public static String CMD_SEND_SET_CAR_WORK_MODE_08;
    public static String CMD_SEND_SET_CAR_WORK_MODE_09;
    //获取电量
    public static String CMD_SEND_GET_BATTERY ;
    //充电宝功能激活
    public static String CMD_SEND_CHARGE_ENABLE;
    //充电宝功能关闭
    public static String CMD_SEND_CHARGE_DISABLE;
    //充电宝充电开始
    public static String CMD_SEND_CHARGE_START;
    //充电宝功能结束
    public static String CMD_SEND_CHARGE_END;
    //获取Mac地址
    public static String CMD_SEND_GET_MAC_ADDRESS;
    //获取Imei
    public static String CMD_SEND_GET_IMEI;
}
