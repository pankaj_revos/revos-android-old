package com.csy.bl.ble.menggou.utils;

import com.csy.bl.ble.common.utils.TypeConvert;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import static com.csy.bl.ble.common.utils.TypeConvert.bytesToHexString;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */
public class AESUtils {

    /**
     * AES-128 数据加密
     *
     * @param sSrc
     * @param sKey
     * @return
     * @throws Exception
     */
    public static byte[] Encrypt(byte[] sSrc, byte[] sKey) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(sKey, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        //cipher.init(Cipher.ENCRYET_MODE, skeySpec);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(sSrc);
        return encrypted;
    }

    /**
     * AES-128 数据解密
     *
     * @param sSrc
     * @param sKey
     * @return
     * @throws Exception
     */
    public static byte[] Decrypt(byte[] sSrc, byte[] sKey) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(sKey, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        //cipher.init(Cipher.DECRYET_MODE, skeySpec);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] dncrypted = cipher.doFinal(sSrc);
        return dncrypted;
    }


    public static void main(String[] args) {
//        byte[] result;
//        try {
//            //加密
//            result = Encrypt(TypeConvert.hexStringToBytes("060101012D1A683D48271A18316E471A"),
//                    TypeConvert.hexStringToBytes("3A60432A5C01211F291E0F4E0C132825"));
//            System.out.println("====>加密结果:" + TypeConvert.bytesToHexString(result));
//
//            //解密
//            result = Decrypt(TypeConvert.hexStringToBytes("A56C7D7548DEFFEFE7AC1EA9BCCE66E6"), TypeConvert.hexStringToBytes(CmdConstants.getCmdKey()));
//            System.out.println("====>解密结果:" + bytesToHexString(result));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



//        key
//        2b7e151628aed2a6abf7158809cf4f3c
//
//        16位
//
//        ECB in text: 3ad77bb40d7a3660a89ecaf32466ef97   //16位明文
//        ECB out text: 6bc1bee22e409f96e93d7e117393172a   //16位密文
//        ECB decrypt out text: 3ad77bb40d7a3660a89ecaf32466ef97 //16位解密明文
//
//        5位
//        ECB in5 text: 3ad77bb40d  //5位明文
//        ECB encrypt out text: 2249a2638c//5位密文
//        ECB out text: 3ad77bb40d  //5位解密明文
        //String key = NativeControl.getDataEncodeKey();


        testEncode();

    }

    public static void testEncode() {
        //java方式
//        String key =  "2b7e151628aed2a6abf7158809cf4f3c";
//        byte[] result;
//        try {
//            //加密
//            //6bc1bee22e409f96e93d7e117393172a
//            String strToEncode = "6bc1bee22e409f96e93d7e117393172a";
//            System.out.println("====>待加密明文:6bc1bee22e409f96e93d7e117393172a");
//            result = Encrypt(TypeConvert.hexStringToBytes(strToEncode), TypeConvert.hexStringToBytes(key));
//            System.out.println("====>加密结果:" + bytesToHexString(result));
//            System.out.println("====>预期结果:" + "3ad77bb40d7a3660a89ecaf32466ef97");
//            //解密
//            result = Decrypt(TypeConvert.hexStringToBytes(bytesToHexString(result)), TypeConvert.hexStringToBytes(key));
//            System.out.println("====>解密结果:" + bytesToHexString(result));
//            System.out.println("====>预期解密结果:" +"6bc1bee22e409f96e93d7e117393172a");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        String key =  "2b7e151628aed2a6abf7158809cf4f3c";
        byte[] result;
        try {
            //加密
            //6bc1bee22e409f96e93d7e117393172a
            String strToEncode = "060101012d1a683d48271a18316e471a";
            System.out.println("====>待加密明文:060101012d1a683d48271a18316e471a");
            result = Encrypt(TypeConvert.hexStringToBytes(strToEncode), TypeConvert.hexStringToBytes(key));
            System.out.println("====>加密结果:" + bytesToHexString(result));
//            System.out.println("====>预期结果:" + "3ad77bb40d7a3660a89ecaf32466ef97");
//            //解密
//            result = Decrypt(TypeConvert.hexStringToBytes(bytesToHexString(result)), TypeConvert.hexStringToBytes(key));
//            System.out.println("====>解密结果:" + bytesToHexString(result));
//            System.out.println("====>预期解密结果:" +"6bc1bee22e409f96e93d7e117393172a");
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}