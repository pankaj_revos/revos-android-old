package com.csy.bl.ble.menggou.net;

import android.os.AsyncTask;

import com.csy.bl.ble.common.utils.Dbug;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by chenshouyin on 2017/11/21.
 * Email:shouyinchen@gmail.com
 */

public class HttpUtlis {
    /**
     *get请求封装
     */

    public void getRequest(String url, Map<String,String> params, String encode, OnResponseListner listner) {
        StringBuffer sb = new StringBuffer(url);
        sb.append("?");
        if (params!=null && !params.isEmpty()){
            for (Map.Entry<String,String> entry:params.entrySet()) {    //增强for遍历循环添加拼接请求内容
                sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
            sb.deleteCharAt(sb.length()-1);
            if (listner!=null) {
                try {
                    URL path = new URL(sb.toString());
                    if (path!=null) {
                        HttpURLConnection con = (HttpURLConnection) path.openConnection();
                        con.setRequestMethod("GET");    //设置请求方式
                        con.setConnectTimeout(10*1000);    //链接超时3秒
                        con.setDoOutput(true);
                        con.setDoInput(true);
                        OutputStream os = con.getOutputStream();
                        os.write(sb.toString().getBytes(encode));
                        os.close();
                        if (con.getResponseCode() == 200) {    //应答码200表示请求成功
                            onSucessResopond(encode, listner, con);
                        }else{
                            onError(listner, "ResponseCode:"+con.getResponseCode());
                        }
                    }
                } catch (Exception error) {
                    error.printStackTrace();
                    onError(listner, error.toString());
                }
            }
        }
    }

    /**
     * POST请求
     */
    public void postRequest(String url,Map<String,Object> params,String encode,OnResponseListner listner){
        StringBuffer sb = new StringBuffer();
        if (params!=null && !params.isEmpty()){
            for (Map.Entry<String,Object> entry: params.entrySet()) {
                sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
            sb.deleteCharAt(sb.length()-1);
        }
        Dbug.d(Dbug.getHeadStr(),"postRequest url:"+url + "/"+sb);
        if (listner!=null) {
            try {
                URL path = new URL(url);
                if (path!=null){
                    HttpURLConnection con = (HttpURLConnection) path.openConnection();
                    con.setRequestMethod("POST");   //设置请求方法POST
                    con.setConnectTimeout(10*1000);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    byte[] bytes = sb.toString().getBytes();
                    OutputStream outputStream = con.getOutputStream();
                    outputStream.write(bytes);
                    outputStream.close();
                    if (con.getResponseCode()==200){
                        onSucessResopond(encode, listner,  con);
                    }else{
                        onError(listner, "ResponseCode:"+con.getResponseCode());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                onError(listner, ""+e.toString());
            }
        }
    }

    /**
     * POST请求
     * 注意回调在子线程,更新UI的时候需要切换到主线程
     */
    public void postRequestAsynTask(String url,Map<String,Object> params,String encode,OnResponseListner listner){
        Object[] obj = new Object[]{url,params,encode,listner};
        new MyAsynTask().execute(obj);
    }

    private void onError(OnResponseListner listner,String erro) {
        listner.onError(erro);
    }

    private void onSucessResopond(String encode, OnResponseListner listner, HttpURLConnection con) throws IOException {
        InputStream inputStream = con.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//创建内存输出流
        int len = 0;
        byte[] bytes = new byte[1024];
        if (inputStream != null) {
            while ((len = inputStream.read(bytes)) != -1) {
                baos.write(bytes, 0, len);
            }
            String str = new String(baos.toByteArray(), encode);
            listner.onSucess(str);
            inputStream.close();
            baos.close();
        }
    }

    public interface OnResponseListner {
        void onSucess(String response);
        void onError(String error);
    }


    private class MyAsynTask extends AsyncTask{

        @Override
        protected Integer doInBackground(Object[] args) {
            if (args==null){
                return -1;
            }
            String url = (String) args[0];
            Map<String,Object> params = (Map<String, Object>) args[1];
            String encode = (String) args[2];
            OnResponseListner listner = (OnResponseListner) args[3];
            StringBuffer sb = new StringBuffer();
            if (params!=null && !params.isEmpty()){
                for (Map.Entry<String,Object> entry: params.entrySet()) {
                    sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }
                sb.deleteCharAt(sb.length()-1);
            }
            Dbug.d(Dbug.getHeadStr(),"postRequest url:"+url + "/"+sb);
            if (listner!=null) {
                OutputStream outputStream = null;
                try {
                    URL path = new URL(url);
                    if (path!=null){
                        HttpURLConnection con = (HttpURLConnection) path.openConnection();
                        con.setRequestMethod("POST");   //设置请求方法POST
                        con.setConnectTimeout(10*1000);
                        con.setDoOutput(true);
                        con.setDoInput(true);
                        byte[] bytes = sb.toString().getBytes();
                        outputStream = con.getOutputStream();
                        outputStream.write(bytes);
                        outputStream.close();
                        if (con.getResponseCode()==200){
                            onSucessResopond(encode, listner,  con);
                            return 1;
                        }else{
                            onError(listner, "ResponseCode:"+con.getResponseCode());
                            return -1;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    onError(listner, ""+e.toString());
                }finally {
                    if (null != outputStream){
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return -1;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            int num = (int) o;
            if (num==1){
                Dbug.d(Dbug.getHeadStr(),"onPostExecute==success");
            }else{
                Dbug.d(Dbug.getHeadStr(),"onPostExecute==fail");
            }
        }
    }

}