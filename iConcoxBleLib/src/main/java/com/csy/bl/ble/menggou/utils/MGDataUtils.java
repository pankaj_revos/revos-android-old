package com.csy.bl.ble.menggou.utils;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.csy.bl.ble.common.utils.Dbug;
import com.csy.bl.ble.common.utils.TypeConvert;

/**
 * Created by chenshouyin on 2017/11/15.
 * Email:shouyinchen@gmail.com
 * <p>
 * 解析蓝牙设备返回的数据，结果通过mHandler返回
 */

public class MGDataUtils {
    private  Handler mHandler;
    //msg.what
    public final static int MSG_GET_TOKEN = -201;
    public final static int MSG_GET_OPEN_LOCK = -202;
    public final static int MSG_GET_CLOSE_LOCK = -203;
    public final static int MSG_GET_LOCK_STATUS = -204;
    public final static int MSG_GET_TAKE_CAR_STATUS = -205;
    public final static int MSG_GET_RETURN_CAR_STATUS = -206;
    public final static int MSG_GET_CAR_WORK_MODE = -207;
    public final static int MSG_GET_SET_CAR_WORK_MODE = -208;
    public final static int MSG_GET_BATTERY = -209;
    public final static int MSG_GET_IMEI = -211;
    public final static int MSG_GET_CHARGE_ENABLE = -212;
    public final static int MSG_GET_CHARGE_DIANABLE = -213;
    public final static int MSG_GET_CHARGE_START = -214;
    public final static int MSG_GET_CHARGE_END = -215;
    public final static int MSG_GET_MAC_ADDRESS = -216;

    public final static int MSG_LOG_SEND_CMD = -217;
    public final static int MSG_LOG_GET_CMD = -218;
    public final static int MSG_GET_MAC_BY_DEVICE_ID_SUCCESS = -219;
    public final static int MSG_GET_MAC_BY_DEVICE_ID_FAIL = -220;
    public MGDataUtils(Handler mHandler) {
        this.mHandler = mHandler;
    }


    public  String[] getData(byte[] data) {


        String hexString = TypeConvert.bytesToHexString(data);

        if (hexString == null || "".equals(hexString)) {
            Dbug.d("MGDataUtils", "===>>解析数据失败==hexString == null || hexString == ");
            return null;
        }
        String[] result = null;
        Message msg = mHandler.obtainMessage();
        if (hexString.startsWith("0602")) {
            //解析获取Token返回的通信帧
            result = getTokenData(hexString);
            msg.what = MSG_GET_TOKEN;
        }else if (hexString.startsWith("080101")){
            //解析开锁返回通信帧
            result = getOpenLockData(hexString);
            msg.what = MSG_GET_OPEN_LOCK;
        }else if (hexString.startsWith("080102")){
            //解析关锁返回通信帧
            result = getCloseLockData(hexString);
            msg.what = MSG_GET_CLOSE_LOCK;
        }else if (hexString.startsWith("080201")){
            //解析查询锁状态返回通信帧
            result = getLockStatusData(hexString);
            msg.what = MSG_GET_LOCK_STATUS;
        }else if (hexString.startsWith("080301")){
            //解析设置借车状态通信帧
            result = getTakeCarStatusData(hexString);
            msg.what = MSG_GET_TAKE_CAR_STATUS;
        }else if (hexString.startsWith("080302")){
            //解析设置还车状态通信帧
            result = getReturnCarStatusData(hexString);
            msg.what = MSG_GET_RETURN_CAR_STATUS;
        }else if (hexString.startsWith("080401")){
            //解析查询工作模式通信帧解析
            result = getCarWorkModeData(hexString);
            msg.what = MSG_GET_CAR_WORK_MODE;
        }else if (hexString.startsWith("080402")){
            //解析设置工作模式通信帧解析
            result = getSetCarWorkModeData(hexString);
            msg.what = MSG_GET_SET_CAR_WORK_MODE;
        }else if (hexString.startsWith("080501")){
            //电量通信帧解析
            result = getBatteryData(hexString);
            msg.what = MSG_GET_BATTERY;
        }else if (hexString.startsWith("080601")){
            //充电宝功能激活
            result = getChargeEnable(hexString);
            msg.what = MSG_GET_CHARGE_ENABLE;
        }else if (hexString.startsWith("080602")){
            //充电宝功能激活关闭
            result = getChargeDisable(hexString);
            msg.what = MSG_GET_CHARGE_DIANABLE;
        }else if (hexString.startsWith("080603")){
            //充电宝充电开始
            result = getChargeStart(hexString);
            msg.what = MSG_GET_CHARGE_START;
        }else if (hexString.startsWith("080604")){
            //充电宝充电结束
            result = getChargeEnd(hexString);
            msg.what = MSG_GET_CHARGE_END;
        }else if (hexString.startsWith("080701")){
            //获取Mac地址数据解析
            result = getMacAddressData(hexString);
            msg.what = MSG_GET_MAC_ADDRESS;
        }else if (hexString.startsWith("080801")){
            //获取imei数据解析
            result = getImeiData(hexString);
            msg.what = MSG_GET_IMEI;
        }
        msg.obj = result;
        mHandler.sendMessage(msg);
        return result;
    }


    /**
     * 解析获取Token返回的通信帧
     * String[0] 有效字节数
     * String[1] token
     * String[2] 芯片类型
     * String[3] 版本号
     * String[4] 产品编号
     *
     * @param hexString String[]
     * @return
     */
    public  String[] getTokenData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            Dbug.d("getTokenData", "===>>hexString == null || \"\".equals(hexString)" );
            return null;
        }
        if (hexString.startsWith("0602")){
            //2个字节代表一位
            int offset = 2;
            // 06 02 固定为令牌返回标识
            String[] result = new String[5];
            //第三个字节为后续有效字节数
            String num = hexString.substring(2 * offset, 3 * offset);
            result[0] = num;
            Dbug.d("MGDataUtils", "===>>有效字节数:" + num);
            //token
            String token = hexString.substring(3 * offset, 7 * offset);
            Dbug.d("MGDataUtils", "===>>token:" + token);
            result[1] = token;
            //芯片类型
            String cpuType = hexString.substring(7 * offset, 8 * offset);
            result[2] = cpuType;
            Dbug.d("MGDataUtils", "===>>芯片类型:" + cpuType);
            //版本号
            String versionNum = hexString.substring(8 * offset, 10 * offset);
            result[3] = versionNum;
            Dbug.d("MGDataUtils", "===>>版本号:" + versionNum);
            //产品编号
            String produceNum = hexString.substring(10 * offset, 12 * offset);
            result[4] = produceNum;
            Dbug.d("MGDataUtils", "===>>产品编号:" + produceNum);
            return result;
        }
        return null;
    }


    /**
     * result[0] 如果是00,则开锁成功
     * result[0] 如果是01,则开锁失败,取result[1],作为失败提示
     * 解析开锁返回通信帧
     *
     * @param hexString
     * @return
     */
    public  String[] getOpenLockData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080101")) {
            String[] result = new String[2];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            //开锁状态返回，00 表示开锁成功，01表示开锁失败
            if ("00".equals(state)) {
                Dbug.d("MGDataUtils", "===>>开锁成功:");
            } else if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>开锁失败:" + state);
            }
            //开锁失败错误码，在开锁成功时，为00
            String num = hexString.substring(4 * offset, 5 * offset);
            result[1] = num;
            return result;
        }
        return null;
    }

    /**
     * 开锁返回通信帧里面的开锁错误码解析
     *
     * @param num
     * @return
     */
    public   String dealWithOpenLockErroCode(String num) {
        String erroCode = "";
        byte[] code = TypeConvert.hexStringToBytes(num);
        String strs[] = TypeConvert.byteToBitStrings(code[0]);
        if ("1".equals(strs[0])) {
            erroCode = "电池电压过低";
        }
        if ("1".equals(strs[1])) {
            if ("".equals(erroCode)) {
                erroCode = "非尾车";
            } else {
                erroCode = erroCode + "|非尾车";
            }
        }
        if ("1".equals(strs[2])) {
            if ("".equals(erroCode)) {
                erroCode = "充电宝功能未激活";
            } else {
                erroCode = erroCode + "|充电宝功能未激活";
            }
        }
        if ("1".equals(strs[3])) {
            if ("".equals(erroCode)) {
                erroCode = "非运营工作模式";
            } else {
                erroCode = erroCode + "|非运营工作模式";
            }
        }
        if ("1".equals(strs[7])) {
            if ("".equals(erroCode)) {
                erroCode = "非运营工作模式";
            } else {
                erroCode = erroCode + "|功能异常";
            }
        }
        //默认值
        if ("".equals(erroCode)) {
            erroCode = "开锁失败,未知原因";
        }
        return erroCode;
    }

    /**
     * 关锁返回通信帧里面的关锁错误码解析
     * result[0] 如果是00,则关锁成功
     * result[0] 如果是01,则关锁失败,取result[1],作为失败提示,预留,只返回code
     *
     * @param hexString
     * @return
     */
    public  String[] getCloseLockData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080102")) {
            String[] result = new String[2];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            if ("00".equals(state)) {
                Dbug.d("MGDataUtils", "===>>关锁成功:");
            } else if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>关锁失败:" + state);
            }
            //关锁失败错误码，在关锁成功时为00,失败时错误码（预留）
            String num = hexString.substring(4 * offset, 5 * offset);
            result[1] = num;
            return result;
        }
        return null;
    }


    /**
     * 返回通信帧里面的查询锁状态数据解析
     * result[0] 00 表示开启状态，01表示关闭状态
     *
     * @param hexString
     * @return result[0]
     */
    public  String[] getLockStatusData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080201")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            if ("00".equals(state)) {
                Dbug.d("MGDataUtils", "===>>开启状态:");
            } else if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>关闭状态:" + state);
            }
            return result;
        }
        return null;
    }


    /**
     * 返回通信帧里面的设置借车状态数据解析
     * result[0] 00 表示成功，01表示失败
     *
     * @param hexString
     * @return result[0]
     */
    public  String[] getTakeCarStatusData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080301")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            if ("00".equals(state)) {
                Dbug.d("MGDataUtils", "===>>设置借车状态成功:");
            } else if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>设置借车状态失败:" + state);
            }
            return result;
        }
        return null;
    }


    /**
     *
     * @param hexString
     * @return result[0]  还车原因 result[1] 还车是否成功 result[2]  还车失败错误码
     */
    public  String[] getReturnCarStatusData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080302")) {
            String[] result = new String[3];
            String reson = hexString.substring(3 * offset, 4 * offset);
            result[0] = reson;
            if ("01".equals(reson)){
                Dbug.d("MGDataUtils", "===>>还车原因:客户插入尾车还车");
            }else if ("02".equals(reson)){
                Dbug.d("MGDataUtils", "===>>乱停车被统一还车");
            }else if ("FF".equals(reson)){
                Dbug.d("MGDataUtils", "===>>专用运维app强制还车");
            }
            String state = hexString.substring(4 * offset,5 * offset);
            result[1] = state;
            if ("00".equals(state)) {
                Dbug.d("MGDataUtils", "===>>设置还车状态成功");
            } else if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>设置还车状态失败:" + state );
            }
            String erroCode = hexString.substring(5 * offset,6 * offset);
            Dbug.d("MGDataUtils", "===>>设置还车状态失败原因:"+ dealWithReturnCarErroCode(erroCode));
            result[2] = erroCode;
            return result;
        }
        return null;
    }

    /**
     * 还车失败原因
     *
     * @param num
     * @return
     */
    public  String dealWithReturnCarErroCode(String num) {
        String erroCode = "";
        byte[] code = TypeConvert.hexStringToBytes(num);
        String strs[] = TypeConvert.byteToBitStrings(code[0]);
        if ("1".equals(strs[0])) {
            erroCode = "非已还车";
        }
        if ("1".equals(strs[1])) {
            if ("".equals(erroCode)) {
                erroCode = "非尾车";
            } else {
                erroCode = erroCode + "|非尾车";
            }
        }
        if ("1".equals(strs[2])) {

            if ("".equals(erroCode)) {
                erroCode = "本地通讯异常";
            } else {
                erroCode = erroCode + "|本地通讯异常";
            }
        }
        if ("1".equals(strs[7])) {
            if ("".equals(erroCode)) {
                erroCode = "功能异常";
            } else {
                erroCode = erroCode + "|功能异常";
            }
        }
        //默认值
        if ("".equals(erroCode)) {
            erroCode = "还车失败,未知原因";
        }
        return erroCode;
    }




    /**
     * 返回通信帧里面的查询工作模式数据解析
     01: 出厂模式(默认)
     02: 充电模式
     03: 运营模式
     04: 运输模式
     05: 库存模式
     06: 运维模式
     07: 归集桩模式
     08: 故障模式
     09: OTA模式
     * @param hexString
     * @return result[0] 只返回底层code码,外部参照上述对应
     */
    public  String[] getCarWorkModeData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080401")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:出厂模式(默认)" + state);
            }else if ("02".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:充电模式" + state);
            }else if ("03".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:运营模式" + state);
            }else if ("04".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:运输模式" + state);
            }else if ("05".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:库存模式" + state);
            }else if ("06".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:运维模式" + state);
            }else if ("07".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:归集桩模式" + state);
            }else if ("08".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:故障模式" + state);
            }else if ("09".equals(state)) {
                Dbug.d("MGDataUtils", "===>>工作模式:OTA模式" + state);
            }else {
                Dbug.d("MGDataUtils", "===>>工作模式:其它模式" + state);
            }
            return result;
        }
        return null;
    }


    /**
     * 本功能只有运维app 或服务器后台才可以管理
     *
     * 返回通信帧里面的设置工作模式数据解析
     * @param hexString
     * @return result[0]   00成功 01失败    result[1] 错误码预留
     */
    public  String[] getSetCarWorkModeData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080402")) {
            String[] result = new String[2];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            //开锁状态返回，00 表示开启状态，01表示关闭状态
            if ("00".equals(state)) {
                Dbug.d("MGDataUtils", "===>>设置工作模式成功:");
            } else if ("01".equals(state)) {
                Dbug.d("MGDataUtils", "===>>设置工作模式失败:" + state);
            }
            //失败时错误码（预留）
            String num = hexString.substring(4 * offset, 5 * offset);
            result[1] = num;
            return result;
        }
        return null;
    }



    /**
     * 电量解析
     * @param hexString
     * @return result[0]   返回0~64也就是10进制0~100,其它值表示获取电量失败
     */
    public  String[] getBatteryData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080501")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            result[0] = state;
            Dbug.d("MGDataUtils", "===>>获取到16进制电量:"+state);
            return result;
        }
        return null;
    }

    /**
     * 充电宝功能激活
     * @param hexString
     * @return result[0]  00设置成功  01设置失败
     */
    public  String[] getChargeEnable(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080601")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            if ("00".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝功能激活成功");
            }else if ("01".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝功能激活失败:"+state);
            }
            result[0] = state;
            return result;
        }
        return null;
    }


    /**
     * 充电宝功能激活关闭
     * @param hexString
     * @return result[0]  00设置成功  01设置失败
     */
    public  String[] getChargeDisable(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080602")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            if ("00".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝功能激活关闭成功");
            }else if ("01".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝功能激活关闭失败:"+state);
            }
            result[0] = state;
            return result;
        }
        return null;
    }

    /**
     * 充电宝充电开始
     * @param hexString
     * @return result[0]  00设置成功  01设置失败
     */
    public  String[] getChargeStart(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080603")) {
            String[] result = new String[2];
            String state = hexString.substring(3 * offset, 4 * offset);
            if ("00".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝充电开始成功");
            }else if ("01".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝充电开始失败:"+state);
            }
            result[0] = state;
            //充电宝充电开始失败错误码，在成功时，为00
            String num = hexString.substring(4 * offset, 5 * offset);
            result[1] = num;
            return result;
        }
        return null;
    }

    /**
     * 充电开始错误码和开锁开锁失败一样
     * result[0] 如果是00,则充电开始成功
     * result[0] 如果是01,则充电开始失败,取result[1],作为失败提示
     *
     * @param num
     * @return
     */
    public   String dealWithChargeStartErroCode(String num) {
        String erroCode = "";
        byte[] code = TypeConvert.hexStringToBytes(num);
        String strs[] = TypeConvert.byteToBitStrings(code[0]);
        if ("1".equals(strs[0])) {
            erroCode = "电池电压过低";
        }
        if ("1".equals(strs[1])) {
            if ("".equals(erroCode)) {
                erroCode = "非尾车";
            } else {
                erroCode = erroCode + "|非尾车";
            }
        }
        if ("1".equals(strs[2])) {
            if ("".equals(erroCode)) {
                erroCode = "充电宝功能未激活";
            } else {
                erroCode = erroCode + "|充电宝功能未激活";
            }
        }
        if ("1".equals(strs[3])) {
            if ("".equals(erroCode)) {
                erroCode = "非运营工作模式";
            } else {
                erroCode = erroCode + "|非运营工作模式";
            }
        }
        if ("1".equals(strs[7])) {
            if ("".equals(erroCode)) {
                erroCode = "非运营工作模式";
            } else {
                erroCode = erroCode + "|功能异常";
            }
        }
        //默认值
        if ("".equals(erroCode)) {
            erroCode = "开锁失败,错误码:" + num;
        }
        return erroCode;
    }

    /**
     * 充电宝充电结束
     * @param hexString
     * @return result[0]  00设置成功  01设置失败
     */
    public  String[] getChargeEnd(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080604")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 4 * offset);
            if ("00".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝功能结束成功");
            }else if ("01".equals(state)){
                Dbug.d("MGDataUtils", "===>>充电宝功能结束失败:"+state);
            }
            result[0] = state;
            return result;
        }
        return null;
    }


//    /**
//     * 修改秘钥返回数据解析
//     * @param hexString
//     * @return result[0]   00修改成功  01修改失败
//     */
//    public  String[] getChangePassWordData(String hexString) {
//        if (hexString == null || "".equals(hexString)) {
//            return null;
//        }
//        //2个字节代表一位
//        int offset = 2;
//        if (hexString.startsWith("080604")) {
//            String[] result = new String[1];
//            String state = hexString.substring(3 * offset, 4 * offset);
//            result[0] = state;
//            Dbug.d("MGDataUtils", "===>>修改密码状态:"+state);
//            return result;
//        }
//        return null;
//    }

    /**
     * 获取梦购车的 蓝牙 MAC 地址返回数据解析
     * @param hexString
     * @return result[0]   为Mac地址
     */
    public  String[] getMacAddressData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080701")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 9 * offset);
            result[0] = state;
            Dbug.d("MGDataUtils", "===>>获取到设备Mac地址:"+state);
            return result;
        }
        return null;
    }



    /**
     * 获取Imei返回数据解析
     * @param hexString
     * @return result[0]   为imei
     */
    public  String[] getImeiData(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return null;
        }
        //2个字节代表一位
        int offset = 2;
        if (hexString.startsWith("080801")) {
            String[] result = new String[1];
            String state = hexString.substring(3 * offset, 11 * offset);
            result[0] = state;
            Dbug.d("MGDataUtils", "===>>获取到IMEI:"+state);
            return result;
        }
        return null;
    }


    public  void main(String[] args) {

        //1.测试获取Token解析返回的数据
//        String[] result = getTokenData("0602885BBA71E701010399998620180A");
//        System.out.println("===>>有效字节数:"+result[0]);
//        System.out.println("===>>token:"+result[1]);
//        System.out.println("===>>芯片类型:"+result[2]);
//        System.out.println("===>>版本号:"+result[3]);
//        System.out.println("===>>产品编号:"+result[4]);


//        //2.测试开锁
//        //模拟成功
//        //String dataOpenLock = "0801010000001122334455667788990011";
//        //模拟失败
//        String dataOpenLock = "08010101AA001122334455667788990011";
//
//        String result[] = getOpenLockData(dataOpenLock);
//        //开锁状态返回，00 表示开锁成功，01表示开锁失败
//        if ("00".equals(result[0])) {
//            Dbug.d("MGDataUtils", "===>>开锁成功,无错误码:");
//        } else if ("01".equals(result[0])){
//            String erroMsg = dealWithOpenLockErroCode(result[1]);
//            Dbug.d("MGDataUtils", "===>>开锁失败,错误码:" + result[1] + "==erroMsg=="+erroMsg);
//        }


        //模拟关锁
//        String dataCloseLock = "08010201011122334455667788990011";
//        String result[] = getCloseLockData(dataCloseLock);
//        if ("00".equals(result[0])) {
//            Dbug.d("MGDataUtils", "===>>关锁成功,无错误码:");
//        } else if ("01".equals(result[0])){
//            //失败错误码预留,暂不解析
//            String erroMsg = result[1];
//            Dbug.d("MGDataUtils", "===>>关锁失败,错误码:" + result[1] + "==预留erroMsg暂不处理=="+erroMsg);
//        }


        //查询锁状态
//        String data = "080201112233445566778899001122";
//        String result[] = getLockStatusData(data);
//        if ("00".equals(result[0])) {
//            Dbug.d("MGDataUtils", "===>>锁开启状态");
//        } else if ("01".equals(result[0])) {
//            Dbug.d("MGDataUtils", "===>>锁关闭状态");
//        }

//        String data = "080201112233445566778899001122";
//        String result[] = getData(TypeConvert.hexStringToBytes(data));
//        if ("00".equals(result[0])) {
//            Dbug.d("MGDataUtils", "===>>锁开启状态");
//        } else if ("01".equals(result[0])) {
//            Dbug.d("MGDataUtils", "===>>锁关闭状态");
//        }

    }
}
