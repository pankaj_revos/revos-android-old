package com.csy.bl.ble.common.utils;

import android.os.Build;

/**
 * Created by chenshouyin on 2017/11/14.
 * Email:shouyinchen@gmail.com
 */
public class VersionTool {
	public static boolean isUnder4_3(){
		if(Build.VERSION.SDK_INT<Build.VERSION_CODES.JELLY_BEAN_MR2 ){
			return true;
		}
		return false;
	}
	
}
