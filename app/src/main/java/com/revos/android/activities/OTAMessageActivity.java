package com.revos.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.revos.android.R;
import com.revos.android.services.NordicBleService;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static com.revos.android.activities.OtaActivity.OTA_DEVICE_ID_KEY;
import static com.revos.android.activities.OtaActivity.OTA_VERSION_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class OTAMessageActivity extends AppCompatActivity {

    private Button mOkButton;
    private TextView mOTADescriptionTextView;
    private LottieAnimationView mOTAMessageLottieImageView;
    private ImageView mConfirmImageView, mDiscardImageView;
    private LinearLayout mLinearLayout;
    private String mExpectedFirmwareVersion;
    private String mDeviceId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ota_message_activity);

        Intent intent = getIntent();

        if(intent == null) {
            return;
        }

        mExpectedFirmwareVersion = intent.getStringExtra(OTA_VERSION_KEY);
        mDeviceId = intent.getStringExtra(OTA_DEVICE_ID_KEY);

        setupViews();
    }

    private void setupViews() {

        mOTAMessageLottieImageView = (LottieAnimationView) findViewById(R.id.ota_message_lottie_image_view);
        mOTADescriptionTextView = (TextView) findViewById(R.id.ota_message_description_text_view);
        mOkButton = (Button) findViewById(R.id.ota_message_ok_button);
        mLinearLayout = (LinearLayout) findViewById(R.id.ota_message_linear_layout);
        mConfirmImageView = (ImageView) findViewById(R.id.ota_message_confirm_image_view);
        mDiscardImageView = (ImageView) findViewById(R.id.ota_message_discard_image_view);

        mOTAMessageLottieImageView.setAnimation(R.raw.ic_surprise);

        if(NordicBleService.getConnectionState() == STATE_CONNECTED) {
            mOkButton.setVisibility(View.GONE);
            mOTADescriptionTextView.setText(getString(R.string.ota_update_available_connected));

        } else {
            mLinearLayout.setVisibility(View.GONE);
            mOTADescriptionTextView.setText(getString(R.string.ota_update_available_disconnected));
        }

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mConfirmImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchOTAActivity();
                finish();
            }
        });

        mDiscardImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void launchOTAActivity() {
        Intent intent = new Intent(getApplicationContext(), OtaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(OTA_VERSION_KEY, mExpectedFirmwareVersion);
        bundle.putString(OTA_DEVICE_ID_KEY, mDeviceId);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}