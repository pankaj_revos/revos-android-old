package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.utilities.adapters.RentalBookingHistoryAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.FetchLeaseForUserQuery;
import com.revos.scripts.type.LeaseStatus;
import com.revos.scripts.type.LeaseWhereInput;
import com.revos.scripts.type.TxNodeType;
import com.revos.scripts.type.TxNodeWhereInput;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class BookingHistoryActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.booking_history_activity);

        setupViews();

        fetchPreviousLeases();
    }

    private void fetchPreviousLeases() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(BookingHistoryActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        LeaseWhereInput leaseWhereInput = LeaseWhereInput
                .builder()
                .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
                .status(LeaseStatus.ENDED)
                .build();

        FetchLeaseForUserQuery fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build();

        apolloClient.query(fetchLeaseForUserQuery).enqueue(new ApolloCall.Callback<FetchLeaseForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchLeaseForUserQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        if(response.getData() != null && response.getData().lease() != null && response.getData().lease().getLeasesForUser() != null) {

                            List<FetchLeaseForUserQuery.GetLeasesForUser> fetchedLeaseForUserList = response.getData().lease().getLeasesForUser();

                            if(fetchedLeaseForUserList != null && !fetchedLeaseForUserList.isEmpty()) {
                                    populateRecyclerView(fetchedLeaseForUserList);

                            } else {
                                showNoPreviousBookingsAvailable();
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                showNoPreviousBookingsAvailable();
            }
        });
    }

    private void setupViews() {
        mRecyclerView = findViewById(R.id.booking_history_recycler_view);
        findViewById(R.id.bookings_history_back_button_image_view).setOnClickListener(v -> {
            finish();
        });
    }

    private void populateRecyclerView(List<FetchLeaseForUserQuery.GetLeasesForUser> getLeasesForUserList) {

        if(getLeasesForUserList.isEmpty()) {
            return;
        }

        ArrayList<FetchLeaseForUserQuery.GetLeasesForUser> modifiableList = new ArrayList<>(getLeasesForUserList);

        Collections.reverse(modifiableList);

        RentalBookingHistoryAdapter mRentalBookingHistoryAdapter = new RentalBookingHistoryAdapter(modifiableList, BookingHistoryActivity.this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(BookingHistoryActivity.this));
        mRecyclerView.setAdapter(mRentalBookingHistoryAdapter);
    }

    private void showNoPreviousBookingsAvailable() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BookingHistoryActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_bookings_found));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);

                finish();
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BookingHistoryActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
