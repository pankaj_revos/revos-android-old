package com.revos.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.GeoFencing;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.RemoveGeoFenceMutation;
import com.revos.scripts.SetGeoFenceMutation;
import com.revos.scripts.type.FenceType;
import com.revos.scripts.type.InputFenceData;
import com.revos.scripts.type.InputVertex;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import timber.log.Timber;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_EVENT_PHONE_LOCATION_UPDATED;
import static com.revos.android.constants.Constants.GEO_FENCING_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.GEO_FENCING_PREFS;

public class SetGeoFencingActivity extends AppCompatActivity
                                implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap mGoogleMap;
    private Marker mPolygonMarker;
    private Circle mCircle;
    private Polygon mPolygon;
    private LatLng mCircleCentreLatLng;
    private double mCircleRadius = 500;
    private LatLng mCircleCentre;

    private RadioGroup mGeoFencingTypeRadioGroup;
    private RadioButton mCircleRadioButton, mPolygonRadioButton;
    private LinearLayout mCircleGeoFenceLinearLayout, mPolygonGeoFenceLinearLayout, mRemoveGeoFenceLinearLayout;
    private SeekBar mCircleRadiusSeekBar;
    private TextView mSeekBarValueTextView;
    private Button mApplyPolygonChangesButton, mApplyCircleGeofenceButton, mTurnGeoFenceOffApplyButton;

    private final int REQUEST_PERMISSION_REQ_CODE = 200;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private ArrayList<LatLng> mMarkersOnMapClickArrayList = null;
    private ArrayList<Marker> mPolygonMarkersArrayList = null;

    private ProgressDialog mProgressDialog;

    private boolean mIsMapRecenteredOnce = false;

    private String mVin = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.set_geo_fencing_activity);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                            .findFragmentById(R.id.set_geo_fencing_map_fragment);

        if(mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        setupViews();
    }

    private void setupViews() {

        mGeoFencingTypeRadioGroup = findViewById(R.id.geo_fencing_radio_group);
        mCircleRadioButton = findViewById(R.id.geo_fencing_circle_radio_button);
        mPolygonRadioButton = findViewById(R.id.geo_fencing_polygon_radio_button);

        mCircleGeoFenceLinearLayout = findViewById(R.id.geo_fencing_circle_feo_fence_linear_layout);
        mPolygonGeoFenceLinearLayout = findViewById(R.id.geo_fencing_polygon_geo_fence_linear_layout);
        mRemoveGeoFenceLinearLayout = findViewById(R.id.geo_fencing_off_linear_layout);

        mCircleRadiusSeekBar = findViewById(R.id.geo_fencing_circle_seek_bar);
        mSeekBarValueTextView = findViewById(R.id.radius_seek_bar_value_text_view);

        mApplyPolygonChangesButton = findViewById(R.id.apply_polygon_markers_button);
        mApplyCircleGeofenceButton = findViewById(R.id.apply_circle_geo_fence_button);
        mTurnGeoFenceOffApplyButton = findViewById(R.id.apply_geo_fence_off_button);

        mSeekBarValueTextView.setText(String.format("%s %s", String.valueOf((int)mCircleRadius), getString(R.string.meters)));

        mGeoFencingTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int checkedRadioButtonId = mGeoFencingTypeRadioGroup.getCheckedRadioButtonId();

                if(checkedRadioButtonId == R.id.geo_fencing_circle_radio_button) {

                    //clear map and setup location pin again
                    mGoogleMap.clear();
                    setupGoogleMaps();

                    mCircleRadius = 500;
                    mCircleCentreLatLng = null;
                    mSeekBarValueTextView.setText(String.format("%s %s", String.valueOf(mCircleRadius), getString(R.string.meters)));
                    mCircleRadiusSeekBar.setProgress(0);
                    //set polygon markers latlng list and markers list as null
                    mMarkersOnMapClickArrayList = null;
                    mPolygonMarkersArrayList = null;

                    mCircleGeoFenceLinearLayout.setVisibility(View.VISIBLE);
                    mPolygonGeoFenceLinearLayout.setVisibility(View.GONE);
                    mRemoveGeoFenceLinearLayout.setVisibility(View.GONE);

                } else if(checkedRadioButtonId == R.id.geo_fencing_polygon_radio_button) {
                    //clear map and setup location pin again
                    mGoogleMap.clear();
                    setupGoogleMaps();

                    mPolygonGeoFenceLinearLayout.setVisibility(View.VISIBLE);
                    mCircleGeoFenceLinearLayout.setVisibility(View.GONE);
                    mRemoveGeoFenceLinearLayout.setVisibility(View.GONE);
                    mMarkersOnMapClickArrayList = new ArrayList<>();
                    mPolygonMarkersArrayList = new ArrayList<>();

                } else if(checkedRadioButtonId == R.id.geo_fencing_off_radio_button) {
                    mGoogleMap.clear();
                    setupGoogleMaps();

                    mRemoveGeoFenceLinearLayout.setVisibility(View.VISIBLE);
                    mCircleGeoFenceLinearLayout.setVisibility(View.GONE);
                    mPolygonGeoFenceLinearLayout.setVisibility(View.GONE);
                }
            }
        });

        mCircleRadiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(seekBar.getProgress() == 0){
                    mCircleRadius = 500;
                    mSeekBarValueTextView.setText(String.format("%s %s", String.valueOf((int) mCircleRadius), getString(R.string.meters)));
                } else if(seekBar.getProgress() == seekBar.getMax()) {
                    mCircleRadius = seekBar.getProgress()*100;
                    mSeekBarValueTextView.setText(String.format("%s %s", String.valueOf(70), getString(R.string.kilometers)));
                } else {
                    mCircleRadius = 500 + (seekBar.getProgress()*100);

                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.FLOOR);
                    String radiusString = mCircleRadius < 1000 ? String.valueOf(mCircleRadius) : df.format(mCircleRadius / 1000);
                    String radiusUnit = mCircleRadius < 1000 ? getString(R.string.meters) : getString(R.string.kilometers);

                    mSeekBarValueTextView.setText(String.format("%s %s", radiusString, radiusUnit));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                setCircleGeoFence();
            }
        });

        mApplyPolygonChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mMarkersOnMapClickArrayList != null && mMarkersOnMapClickArrayList.size() < 3) {
                    Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.need_atleast_three_markers));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    SetGeoFencingActivity.this.startActivity(intent);

                    return;
                }

                //set geo fence
                setGeoFenceCall(FenceType.POLYGON);
            }
        });

        mApplyCircleGeofenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //set geo fence
                setGeoFenceCall(FenceType.RADIAL);
            }
        });

        mTurnGeoFenceOffApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog();
                SharedPreferences sharedPreferences = getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE);
                String geoFencingDataStr = sharedPreferences.getString(GEO_FENCING_JSON_DATA_KEY, null);

                if(geoFencingDataStr == null || geoFencingDataStr.isEmpty()) {
                    hideProgressDialog();

                    Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_geo_fencing_set));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    SetGeoFencingActivity.this.startActivity(intent);

                    return;
                }
                //remove geo fence
                removeGeoFenceCall();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Timber.v("On Map Ready");

        mGoogleMap = googleMap;

        setupGoogleMaps();

        mGoogleMap.setOnMapClickListener(SetGeoFencingActivity.this);
    }

    @Override
    public void onMapClick(LatLng latLng) {

        if(mCircleRadioButton != null && mCircleRadioButton.isChecked()) {

            mCircleCentreLatLng = latLng;

            if(mPolygonMarker != null) {
                mPolygonMarker.remove();
            }

            mPolygonMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latLng.latitude, latLng.longitude))
                    .zIndex(4)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            setCircleGeoFence();

        } else if(mPolygonRadioButton != null && mPolygonRadioButton.isChecked()) {

            mMarkersOnMapClickArrayList.add(latLng);

            mPolygonMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latLng.latitude, latLng.longitude))
                    .zIndex(4)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            mPolygonMarkersArrayList.add(mPolygonMarker);

            if(mPolygonMarkersArrayList != null && !mPolygonMarkersArrayList.isEmpty() && mPolygonMarkersArrayList.size() > 2) {
                if(mPolygonMarkersArrayList.size() == 3) {
                    //set polygon geo fence by  using the markers' list
                    setPolygonGeoFence();
                } else {
                    //if markers are more than 3 then, update the polygon geo fence by updating the markers' LATLNG list
                    mPolygon.setPoints(mMarkersOnMapClickArrayList);
                }
            }
        }
    }

    private void setCircleGeoFence() {
        //remove existing geo fence if any
        if (mCircle != null) { mCircle.remove(); }

        if (mCircleCentreLatLng != null) {
            mCircleCentre = mCircleCentreLatLng;
        } else {
            Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.select_a_center_for_the_fence_first));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3500);
            intent.putExtras(bundle);
            SetGeoFencingActivity.this.startActivity(intent);

            return;
        }

        CircleOptions circleOptions = new CircleOptions()
                                            .center(mCircleCentre)
                                            .strokeColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green))
                                            .strokeWidth(5.0f)
                                            .fillColor(Color.argb(100, 150, 150, 150))
                                            .radius(mCircleRadius);

        mCircle = mGoogleMap.addCircle(circleOptions);

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(circleOptions.getCenter(),
                                                                    getZoomLevel(mCircle)));
    }

    private void setPolygonGeoFence() {

        PolygonOptions polygonOptions = new PolygonOptions();

        polygonOptions.addAll(mMarkersOnMapClickArrayList);

        mPolygon = mGoogleMap.addPolygon(polygonOptions
                                .strokeColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green))
                                .fillColor(Color.argb(100, 150, 150, 150))
                                .strokeWidth(5.0f));

    }

    private void setGeoFenceCall(FenceType type) {
        showProgressDialog();

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null) {
            mVin = vehicle.getVin();
        } else {
            hideProgressDialog();

            Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vin_for_geofence));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 4000);
            intent.putExtras(bundle);
            SetGeoFencingActivity.this.startActivity(intent);

            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();
        if(apolloClient == null) {

            Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            SetGeoFencingActivity.this.startActivity(intent);

            return;
        }

        InputFenceData inputFenceData = null;
        if(type.equals(FenceType.RADIAL)) {

            InputVertex inputVertex = InputVertex.builder().latitude(mCircleCentre.latitude).longitude(mCircleCentre.longitude).build();

            inputFenceData = InputFenceData.builder().type(type).centre(inputVertex).radius(mCircleRadius).build();

        } else {

            List<InputVertex> inputVertexList = new ArrayList<>();

            for(int index = 0 ; index < mMarkersOnMapClickArrayList.size() ; index++) {
                InputVertex inputVertex = InputVertex
                                            .builder()
                                            .latitude(mMarkersOnMapClickArrayList.get(index).latitude)
                                            .longitude(mMarkersOnMapClickArrayList.get(index).longitude)
                                            .build();

                inputVertexList.add(inputVertex);
            }

            inputFenceData = InputFenceData
                                .builder()
                                .type(type)
                                .vertices(inputVertexList)
                                .build();
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(mVin).build();

        SetGeoFenceMutation setGeoFenceMutation = SetGeoFenceMutation
                                                    .builder()
                                                    .data(inputFenceData)
                                                    .where(vehicleWhereUniqueInput)
                                                    .build();

        apolloClient.mutate(setGeoFenceMutation).enqueue(new ApolloCall.Callback<SetGeoFenceMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SetGeoFenceMutation.Data> response) {
                hideProgressDialog();
                if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                    //TODO: show appropriate message here
                    Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    SetGeoFencingActivity.this.startActivity(intent);

                } else {
                    if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().setFence() != null) {
                        //clear prefs before saving
                        getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply();

                        GeoFencing geoFencing = new GeoFencing();

                        //check type
                        FenceType fenceType = response.getData().vehicles().setFence().type();

                        if(fenceType.equals(FenceType.RADIAL)) {
                            geoFencing.setGeoFenceType(fenceType);
                            geoFencing.setCircleRadius(response.getData().vehicles().setFence().radius());
                            LatLng centre = new LatLng(response.getData().vehicles().setFence().centre().latitude(), response.getData().vehicles().setFence().centre().longitude());
                            geoFencing.setCircleGeoFenceCentre(centre);

                        } else {
                            geoFencing.setGeoFenceType(fenceType);
                            List<SetGeoFenceMutation.Vertex> setGeoFenceVerticesList = response.getData().vehicles().setFence().vertices();
                            ArrayList<LatLng> polygonVerticesArrayList = new ArrayList<>();
                            for(int index = 0 ; index < setGeoFenceVerticesList.size() ; index++) {
                                double latitude = setGeoFenceVerticesList.get(index).latitude();
                                double longitude = setGeoFenceVerticesList.get(index).longitude();
                                polygonVerticesArrayList.add(new LatLng(latitude, longitude));
                            }
                            geoFencing.setPolygonVerticesList(polygonVerticesArrayList);
                        }

                        geoFencing.setGeoFenceId(response.getData().vehicles().setFence().id());

                        //save to prefs
                        getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, new Gson().toJson(geoFencing)).apply();

                        Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.geo_fencing_changes_applied_successfully));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        SetGeoFencingActivity.this.startActivity(intent);

                        finish();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        SetGeoFencingActivity.this.startActivity(intent);
                    }
                });
            }
        });
    }

    private void removeGeoFenceCall() {
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        if(vehicle != null ) {
            mVin = vehicle.getVin();
        } else {
            hideProgressDialog();

            Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vin_for_geofence));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 4000);
            intent.putExtras(bundle);
            SetGeoFencingActivity.this.startActivity(intent);

            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 1500);
            intent.putExtras(bundle);
            SetGeoFencingActivity.this.startActivity(intent);
            return;
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(mVin).build();
        RemoveGeoFenceMutation removeGeoFenceMutation = RemoveGeoFenceMutation.builder().vehicleWhereUniqueInput(vehicleWhereUniqueInput).build();

        apolloClient.mutate(removeGeoFenceMutation).enqueue(new ApolloCall.Callback<RemoveGeoFenceMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<RemoveGeoFenceMutation.Data> response) {
                hideProgressDialog();

                if(response != null && response.getData() != null && response.getData().vehicles() != null) {
                    //clear geo fencing prefs
                    getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.geo_fence_removed_successfully));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 1500);
                            intent.putExtras(bundle);
                            SetGeoFencingActivity.this.startActivity(intent);

                            finish();
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                hideProgressDialog();

                Intent intent = new Intent(SetGeoFencingActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                intent.putExtras(bundle);
                SetGeoFencingActivity.this.startActivity(intent);
            }
        });

    }

    public int getZoomLevel(Circle circle) {
        int zoomLevel = 11;
        if (circle != null) {
            double radius = circle.getRadius() + circle.getRadius() / 2;
            double scale = radius / 500;
            zoomLevel = (int) (16 - Math.log(scale) / Math.log(2));
        }
        return zoomLevel;
    }

    private void setupGoogleMaps() {

        if(mGoogleMap != null) {

            if (ActivityCompat.checkSelfPermission(SetGeoFencingActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SetGeoFencingActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                return;
            }

            recenterMapOnce();

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_REQ_CODE) {
            //check if all permissions are granted
            boolean permissionGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                } else {
                    permissionGranted = false;
                    break;
                }
            }
            if (permissionGranted) {
                setupGoogleMaps();
            }
        }
    }

    private void recenterMapOnce() {
        if(NordicBleService.mCurrentLocation != null && !mIsMapRecenteredOnce) {

            mIsMapRecenteredOnce = centerMap(NordicBleService.mCurrentLocation);
        }
    }

    private boolean centerMap(Location location) {
        if (location == null || mGoogleMap == null) {
            return false;
        }
        LatLng currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 17f));

        return true;
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(GEN_EVENT_PHONE_LOCATION_UPDATED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recenterMapOnce();
                }
            });
        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(SetGeoFencingActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
