package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.MarkPassbookTransactionStatusMutation;
import com.revos.scripts.type.InputMarkPtx;
import com.revos.scripts.type.PassbookTxStatus;
import com.revos.scripts.type.PassbookTxWhereUniqueInput;
import com.shreyaspatil.easyupipayment.EasyUpiPayment;
import com.shreyaspatil.easyupipayment.listener.PaymentStatusListener;
import com.shreyaspatil.easyupipayment.model.PaymentApp;
import com.shreyaspatil.easyupipayment.model.TransactionDetails;
import com.shreyaspatil.easyupipayment.model.TransactionStatus;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import timber.log.Timber;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class UPIPaymentsActivity extends AppCompatActivity implements PaymentStatusListener {

    private ImageView mUpiPaymentsBackButtonImageView;

    private TextView mAmountDueTextView, mRecipientNameTextView, mRecipientUpiIdTextView;

    private Button mPayWithUpiButton;

    private ProgressDialog mProgressDialog;

    private EasyUpiPayment mEasyUpiPayment;

    public static final String RENTAL_PASSBOOK_TRANSACTION_ID = "rental_passbook_transaction_id";
    public static final String RENTAL_PAYEE_UPI_ID = "rental_payee_upi_id";
    public static final String RENTAL_PAYEE_NAME = "rental_payee_name";
    public static final String RENTAL_AMOUNT_TO_BE_PAID = "rental_amount_to_be_paid";
    public static final String RENTAL_EXISTING_REMARK = "rental_existing_remark";

    private String mPassbookTxId = null;
    private String mUpiId = null;
    private String mPayeeName = null;
    private String mAmountPayable = null;
    private String mRemark = null;

    private TransactionDetails mTransactionDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if(bundle.containsKey(RENTAL_PASSBOOK_TRANSACTION_ID)) {
                mPassbookTxId = bundle.getString(RENTAL_PASSBOOK_TRANSACTION_ID);
            }
            if (bundle.containsKey(RENTAL_PAYEE_UPI_ID)) {
                mUpiId = bundle.getString(RENTAL_PAYEE_UPI_ID);
            }
            if(bundle.containsKey(RENTAL_PAYEE_NAME)) {
                mPayeeName = bundle.getString(RENTAL_PAYEE_NAME);
            }
            if(bundle.containsKey(RENTAL_AMOUNT_TO_BE_PAID)) {
                mAmountPayable = bundle.getString(RENTAL_AMOUNT_TO_BE_PAID);
            }
            if(bundle.containsKey(RENTAL_EXISTING_REMARK)) {
                mRemark = bundle.getString(RENTAL_EXISTING_REMARK);
            }
        }
        setContentView(R.layout.upi_payments_activity);

        setupViews();

        populateViews();
    }

    private void setupViews() {

        mUpiPaymentsBackButtonImageView = findViewById(R.id.upi_payments_back_button_image_view);
        mAmountDueTextView = findViewById(R.id.upi_payments_amount_due_text_view);
        mRecipientNameTextView = findViewById(R.id.upi_payments_recipient_name_text_view);
        mRecipientUpiIdTextView = findViewById(R.id.upi_payments_recipient_upi_id_text_view);
        mPayWithUpiButton = findViewById(R.id.upi_payments_pay_using_upi_button);

        mUpiPaymentsBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: Dialog to confirm exit
                finish();
            }
        });
        mPayWithUpiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay();
            }
        });
    }

    private void populateViews() {
        if(mAmountPayable != null && !mAmountPayable.isEmpty()) {
            mAmountDueTextView.setText(mAmountPayable);
        } else {
            mAmountDueTextView.setText(getString(R.string.not_available));
        }

        if(mPayeeName != null && !mPayeeName.isEmpty()) {
            mRecipientNameTextView.setText(mPayeeName);
        } else {
            mRecipientNameTextView.setText(getString(R.string.not_available));
        }

        if(mUpiId != null && !mUpiId.isEmpty()) {
            mRecipientUpiIdTextView.setText(mUpiId);
        } else {
            mRecipientUpiIdTextView.setText(getString(R.string.not_available));
        }
    }

    private void pay() {
        String payeeVpa = mRecipientUpiIdTextView.getText().toString();;
        String payeeName = mRecipientNameTextView.getText().toString();
        String transactionId = "revos-txid-" + String.valueOf(System.currentTimeMillis());
        String transactionRefId = "revos-reftxid-"+ String.valueOf(System.currentTimeMillis());
        String description = "REVOS RENTAL INVOICE";
        String amount = mAmountPayable;

        PaymentApp paymentApp = PaymentApp.ALL;

        if(payeeVpa.isEmpty()) {
            Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payee_upi_not_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(payeeName.isEmpty()) {
            Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payee_name_not_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(amount.isEmpty()) {
            Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_amount_to_be_paid));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        try {
            // START PAYMENT INITIALIZATION
            mEasyUpiPayment = new EasyUpiPayment.Builder(this)
                    .with(paymentApp)
                    .setPayeeVpa(payeeVpa)
                    .setPayeeName(payeeName)
                    .setTransactionId(transactionId)
                    .setTransactionRefId(transactionRefId)
                    .setDescription(description)
                    .setAmount(amount)
                    .build();

            // Register Listener for Events
            mEasyUpiPayment.setPaymentStatusListener(this);

            showProgressDialog();

            // START PAYMENT
            mEasyUpiPayment.startPayment();

        } catch (Exception e) {
            Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_payee_upi_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            hideProgressDialog();
            finish();
        }
    }

    private void markTransactionStatus(TransactionDetails transactionDetails, PassbookTxStatus status) {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        PassbookTxWhereUniqueInput passbookTxWhereUniqueInput = PassbookTxWhereUniqueInput.builder().id(mPassbookTxId).build();


        String remarkStr = "to : " + mUpiId + ", " + transactionDetails.toString();
        String remarkStrInput = mRemark + "######" + remarkStr;

        InputMarkPtx inputMarkPtxData = InputMarkPtx.builder().status(status).remark(remarkStrInput).build();

        MarkPassbookTransactionStatusMutation markPassbookTransactionStatusMutation = MarkPassbookTransactionStatusMutation
                .builder()
                .passbookTxWhereInputVar(passbookTxWhereUniqueInput)
                .passBookTxInputData(inputMarkPtxData)
                .build();

        apolloClient.mutate(markPassbookTransactionStatusMutation).enqueue(new ApolloCall.Callback<MarkPassbookTransactionStatusMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<MarkPassbookTransactionStatusMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getData() != null && response.getData().finance() != null && response.getData().finance().markPassbookTx() != null
                                && response.getData().finance().markPassbookTx() != null) {

                            if(response.getData().finance().markPassbookTx().status() != null && response.getData().finance().markPassbookTx().status().equals(PassbookTxStatus.PAID)) {
                                Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_successful));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.payment_failed));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        } else {
                            Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }

                        finish();
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(UPIPaymentsActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                        finish();
                    }
                });
            }
        });
    }

    @Override
    public void onTransactionCompleted(TransactionDetails transactionDetails) {
        // Transaction Completed
        Timber.d("TransactionDetails" + transactionDetails.toString());

        mTransactionDetails = transactionDetails;

        PassbookTxStatus status = PassbookTxStatus.PENDING;
        if(mTransactionDetails.getTransactionStatus() == TransactionStatus.SUCCESS) {
            status = PassbookTxStatus.PAID;
        }

        markTransactionStatus(mTransactionDetails, status);
    }

    @Override
    public void onTransactionCancelled() {
        hideProgressDialog();
        // Payment Failed
        PassbookTxStatus status = PassbookTxStatus.PENDING;
        markTransactionStatus(mTransactionDetails, status);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mEasyUpiPayment != null) {
            mEasyUpiPayment.removePaymentStatusListener();
        }

        hideProgressDialog();
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(UPIPaymentsActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!UPIPaymentsActivity.this.isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
