package com.revos.android.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

/**
 * Created by mohit on 25/8/17.
 */

public class ProminentDisclosureActivity extends AppCompatActivity {

    /**Firebase auth object*/
    private FirebaseAuth mAuth;

    private final int PERMISSION_CALLBACK_CONSTANT = 200;

    private String[] mPermissionArray ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.prominent_disclosure_activity);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        findViewById(R.id.prominent_disclosure_allow_button).setOnClickListener(view -> {

            mPermissionArray = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION };

            if(Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                //Android 10 requests both permissions at once
                mPermissionArray = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION };
            }

            ActivityCompat.requestPermissions(ProminentDisclosureActivity.this, mPermissionArray, PERMISSION_CALLBACK_CONSTANT);

        });
    }


    private void decideAndLaunchNextActivity() {

        if(getSharedPreferences(Constants.APP_PREFS, MODE_PRIVATE).getBoolean(Constants.IS_APP_IN_GUEST_MODE, false)) {
            launchMainActivity();
            finish();
            return;
        }

        if(mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
            launchLoginActivity();
            finish();
            return;
        }

        //check if user is logged in or not
        if(mAuth != null && mAuth.getCurrentUser() == null) {
            launchLoginActivity();
            finish();
            return;
        }

        //check if we have an already existing user in prefs
        if(mAuth.getCurrentUser() != null) {
            if(!doUserPrefsExist()) {
                launchLoginActivity();
                finish();
                return;
            }
        }

        //check if all user details are present else launch user details activity
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(user == null
                || user.getFirstName() == null
                || user.getLastName() == null
                || user.getEmail() == null
                || user.getDob() == null
                || user.getGenderType() == null
                || user.getFirstName().trim().isEmpty()
                || user.getLastName().trim().isEmpty()
                || user.getEmail().trim().isEmpty()
                || user.getDob().trim().isEmpty()) {
            launchUserDetails();
            finish();
            return;
        }

        //check if all permissions are provided, otherwise launch appintro activity where we ask for permissions

        //check for simple permissions
        if(ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED) {

            launchAppIntro();
            finish();
            return;
        }

        //check for notification access
        String notificationListenerList = Settings.Secure.getString(this.getContentResolver(),"enabled_notification_listeners");
        if(notificationListenerList == null || !(notificationListenerList.contains(getApplicationContext().getPackageName()))) {
            launchAppIntro();
            finish();
            return;
        }

        //check for screen draw permission
        //commenting this out for now as we don't need screen draw permissions
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!Settings.canDrawOverlays(this)) {
                launchAppIntro();
                finish();
                return;
            }
        }*/

        //check if user phone and emergency contact are present
        user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(user == null) {
            //there has been an error, this situation should not occur
            launchLoginActivity();
            finish();
            return;
        } else {
            if(user.getPhone() == null || user.getAltPhone1() == null || user.getPhone().isEmpty() || user.getAltPhone1().isEmpty()) {
                launchAppIntro();
                finish();
                return;
            }
        }
        
        //refresh data
        refreshData();
        //launch the main activity
        launchMainActivity();
        finish();
    }

    private void refreshData() {
        NetworkUtils.getInstance(getApplicationContext()).refreshUserDetails();
        NetworkUtils.getInstance(getApplicationContext()).refreshTripData();
    }

    private void launchUserDetails() {
        startActivity(new Intent(getApplicationContext(), UserDetailActivity.class));
    }

    //checks if a user already exists in prefs
    private boolean doUserPrefsExist() {
        //read info from shared prefs
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE);


        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        if(user == null) {
            return false;
        }

        String userId = user.getId();
        String userFirstName = user.getFirstName();
        String userLastName = user.getLastName();

        if(!userId.trim().isEmpty() && !userFirstName.trim().isEmpty() && !userLastName.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }


    private void launchLoginActivity() {
        Intent myIntent = new Intent(ProminentDisclosureActivity.this, LoginActivity.class);
        ProminentDisclosureActivity.this.startActivity(myIntent);
    }

    private void launchAppIntro() {
        Intent myIntent = new Intent(ProminentDisclosureActivity.this, AppIntroActivity.class);
        ProminentDisclosureActivity.this.startActivity(myIntent);
    }

    private void launchMainActivity() {
        Intent myIntent = new Intent(ProminentDisclosureActivity.this, MainActivity.class);
        ProminentDisclosureActivity.this.startActivity(myIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT){
            //check if all permissions are granted
            boolean allGranted = false;

            if(Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                for(int i=0;i<grantResults.length;i++) {
                    if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                        allGranted = true;
                    } else {
                        allGranted = false;
                        break;
                    }
                }
            } else if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Both foreground and background permissions have been granted check
                if(ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED
                        && (Build.VERSION.SDK_INT > Build.VERSION_CODES.P
                        && ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[1]) == PackageManager.PERMISSION_GRANTED)) {

                    allGranted = true;
                }
            }

            if(allGranted) {
                decideAndLaunchNextActivity();
            } else if(ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                    || (Build.VERSION.SDK_INT > Build.VERSION_CODES.P
                        && ActivityCompat.shouldShowRequestPermissionRationale(ProminentDisclosureActivity.this, Constants.permissionsRequired[1]))) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ProminentDisclosureActivity.this);
                builder.setTitle(R.string.need_following_permissions);

                if(ActivityCompat.shouldShowRequestPermissionRationale(ProminentDisclosureActivity.this, mPermissionArray[0])) {
                    String fineLocationPermissionMessage = getString(R.string.access_fine_location_permission_description) + "\n" + getString(R.string.access_fine_location_permission_description);
                    builder.setMessage(fineLocationPermissionMessage);
                } else if(Build.VERSION.SDK_INT > Build.VERSION_CODES.P && ActivityCompat.shouldShowRequestPermissionRationale(ProminentDisclosureActivity.this, Constants.permissionsRequired[1])) {
                    builder.setMessage(R.string.background_location_permission_required);
                }

                builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                        String[] permissionPending = getPermissionToBeRequestedArray();

                        if(permissionPending != null && permissionPending.length > 0) {
                            ActivityCompat.requestPermissions(ProminentDisclosureActivity.this, permissionPending,PERMISSION_CALLBACK_CONSTANT);
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

            } else {
                Intent intent = new Intent(ProminentDisclosureActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_get_permission));
                bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 3000);
                intent.putExtras(bundle);
                ProminentDisclosureActivity.this.startActivity(intent);

            }
        }
    }

    private String[] getPermissionToBeRequestedArray() {

        if(ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, mPermissionArray[0]) != PackageManager.PERMISSION_GRANTED) {
            return mPermissionArray;
        } else if(Build.VERSION.SDK_INT > Build.VERSION_CODES.Q
            && ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED) {
            return new String[]{ Manifest.permission.ACCESS_BACKGROUND_LOCATION };
        } else if(Build.VERSION.SDK_INT == Build.VERSION_CODES.Q
                && ActivityCompat.checkSelfPermission(ProminentDisclosureActivity.this, Constants.permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED){
            return mPermissionArray;
        } else {
            return null;
        }
    }
}
