package com.revos.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.boltCore.android.constants.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.DataFeedVehicleSnapshot;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.GeoFencing;
import com.revos.android.jsonStructures.User;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.geofence.GeoFenceUtil;
import com.revos.scripts.FetchGeoFenceQuery;
import com.revos.scripts.type.FenceType;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_EVENT_PHONE_LOCATION_UPDATED;
import static com.revos.android.constants.Constants.GEO_FENCING_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.GEO_FENCING_PREFS;
import static com.revos.android.services.NordicBleService.mCurrentLocation;

public class GeoFencingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;

    private Button mSetGeoFenceButton;
    private TextView mVehicleGeoFenceStatusTextView;

    private GeoFencing mGeoFencing;
    private Circle mCircleGeoFence = null;

    private final int REQUEST_PERMISSION_REQ_CODE = 200;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private List<LatLng> mPolygonVerticesArrayList = null;

    private ProgressDialog mProgressDialog;

    private boolean geoFencingMessageFlag = false;
    private boolean mIsMapCenteredOnce = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.geo_fence_activity);

         mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                                            .findFragmentById(R.id.geo_fencing_map_fragment);

        setupViews();
    }

    private void setupViews() {

        ImageView mGeoFenceBackButtonImageView = findViewById(R.id.geo_fencing_back_button_image_view);
        mSetGeoFenceButton = findViewById(R.id.geo_fence_set_button);
        mVehicleGeoFenceStatusTextView = findViewById(R.id.geo_fence_vehicle_status_text_view);

        toggleSetGeoFenceButtonBg();

        mGeoFenceBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mSetGeoFenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkIfCurrentUserIsOwner()) {
                    Intent intent = new Intent(GeoFencingActivity.this, SetGeoFencingActivity.class);
                    GeoFencingActivity.this.startActivity(intent);
                } else {

                    Intent intent = new Intent(GeoFencingActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.only_owner_can_set_edit_geo_fence));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    GeoFencingActivity.this.startActivity(intent);
                }
            }
        });
    }

    private boolean checkIfCurrentUserIsOwner() {

        String deviceOwnerId = null, currentUserId = null;
        User loggedInUserDetails;


        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        loggedInUserDetails = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        if(device == null || loggedInUserDetails == null) {
            return false;
        }

        deviceOwnerId = device.getVehicle().getOwnerId();
        currentUserId = loggedInUserDetails.getId();

        if(currentUserId != null && deviceOwnerId != null) {
            if (currentUserId.equals(deviceOwnerId)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private void toggleSetGeoFenceButtonBg() {
        if(mSetGeoFenceButton != null) {
            if(!checkIfCurrentUserIsOwner()) {
                mSetGeoFenceButton.setBackground(getDrawable(R.drawable.capsule_button_background_grey));
                mSetGeoFenceButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_grey));
            } else {
                mSetGeoFenceButton.setBackground(getDrawable(R.drawable.capsule_button_green_filled_background));

            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.clear();

        setupGoogleMaps();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                fetchGeoFenceData();
            }
        });
    }

    private void drawGeoFence() {

        SharedPreferences sharedPreferences = getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE);
        String geoFencingDataStr = sharedPreferences.getString(GEO_FENCING_JSON_DATA_KEY, null);

        if(geoFencingDataStr != null) {
            mSetGeoFenceButton.setText(getString(R.string.edit_geo_fence_area));
            mGeoFencing = new Gson().fromJson(geoFencingDataStr, GeoFencing.class);
        } else {
            Intent intent = new Intent(GeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_geo_fencing_set));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            GeoFencingActivity.this.startActivity(intent);

            return;
        }

        FenceType geoFenceType = mGeoFencing.getGeoFenceType();

        if(geoFenceType.equals(FenceType.RADIAL)) {

            double radius = mGeoFencing.getCircleRadius();
            LatLng circleGeoFenceCentre = null;
            if(mGeoFencing.getCircleGeoFenceCentre() != null) {
                circleGeoFenceCentre = mGeoFencing.getCircleGeoFenceCentre();
            }
            drawCircleGeoFence(radius, circleGeoFenceCentre);

        } else if(geoFenceType.equals(FenceType.POLYGON)) {

            mPolygonVerticesArrayList = new ArrayList<>();
            mPolygonVerticesArrayList = mGeoFencing.getPolygonVerticesList();
            drawPolygonGeoFence();
        }
    }

    private void drawCircleGeoFence(double radius, LatLng circleCentre) {

        if (mCircleGeoFence != null) { mCircleGeoFence.remove(); }

        //adding marker at the circle center
        mMap.addMarker(new MarkerOptions()
            .position(circleCentre)
            .anchor(0.33f, 1)
            .zIndex(4)
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        CircleOptions circleOptions = new CircleOptions()
                                            .center(circleCentre)
                                            .strokeColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green))
                                            .strokeWidth(5.0f)
                                            .fillColor(Color.argb(100, 150, 150, 150))
                                            .radius(radius);

        if(mMap != null) {
            mCircleGeoFence = mMap.addCircle(circleOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(circleOptions.getCenter(), getZoomLevel(mCircleGeoFence)));
        }
    }

    private void drawPolygonGeoFence() {

        if(mPolygonVerticesArrayList != null && !mPolygonVerticesArrayList.isEmpty() && mPolygonVerticesArrayList.size() > 2) {

            PolygonOptions polygonOptions = new PolygonOptions();

            polygonOptions.addAll(mPolygonVerticesArrayList);

            Polygon polygon = mMap.addPolygon(polygonOptions
                                    .strokeColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green))
                                    .fillColor(Color.argb(100, 150, 150, 150))
                                    .strokeWidth(5.0f));

            //LatLng builder
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int index = 0; index < mPolygonVerticesArrayList.size() ; index++) {
                builder.include(mPolygonVerticesArrayList.get(index));
            }
            LatLngBounds bounds = builder.build();

            int padding = 80; // offset from edges of the map in pixels
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);

            mMap.moveCamera(cameraUpdate);
        }
    }

    private void setVehicleCurrentLocationMarker(double lat, double lng) {

        boolean isInside = false;

        //add vehicle location marker
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .anchor(0.33f, 1)
                .zIndex(4)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_bike_unselected, convertDipToPixels(48), convertDipToPixels(48)))));

        //vehicle marker and geo fence zoomed at an appropriate level
        SharedPreferences sharedPreferences = getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE);
        String geoFencingDataStr = sharedPreferences.getString(GEO_FENCING_JSON_DATA_KEY, null);

        GeoFencing geoFencing = null;
        if(geoFencingDataStr != null) {
            geoFencing = new Gson().fromJson(geoFencingDataStr, GeoFencing.class);
        }

        if(geoFencing != null) {
            LatLng vehicleLatLng = new LatLng(lat, lng);

            if(geoFencing.getGeoFenceType().equals(FenceType.RADIAL)) {

                //check if vehicle is inside the geofence
                isInside = GeoFenceUtil.isInsideCircle(vehicleLatLng, geoFencing.getCircleGeoFenceCentre(), geoFencing.getCircleRadius());
                if(isInside) {
                    mVehicleGeoFenceStatusTextView.setText(getString(R.string.vehicle_within_geo_fence));
                    return;
                } else {
                    mVehicleGeoFenceStatusTextView.setText(getString(R.string.vehicle_is_outside_geo_fence));
                }

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(geoFencing.getCircleGeoFenceCentre());
                builder.include(new LatLng(lat, lng));
                LatLngBounds bounds = builder.build();

                int padding = 80;
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                mMap.moveCamera(cameraUpdate);

            } else if(geoFencing.getGeoFenceType().equals(FenceType.POLYGON)) {
                //check if vehicle is inside the geofence
                isInside = GeoFenceUtil.isInsidePolygon(vehicleLatLng, geoFencing.getPolygonVerticesList());
                if(isInside) {
                    mVehicleGeoFenceStatusTextView.setText(getString(R.string.vehicle_within_geo_fence));
                    return;
                } else {
                    mVehicleGeoFenceStatusTextView.setText(getString(R.string.vehicle_is_outside_geo_fence));
                }

                //zoom as per latlng bounds
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (int index = 0; index < geoFencing.getPolygonVerticesList().size() ; index++) {
                    builder.include(geoFencing.getPolygonVerticesList().get(index));
                }
                builder.include(new LatLng(lat, lng));
                LatLngBounds bounds = builder.build();

                int padding = 80; // offset from edges of the map in pixels
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                mMap.moveCamera(cameraUpdate);

            }

        } else {
            mVehicleGeoFenceStatusTextView.setText("");
        }
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    private void fetchGeoFenceData() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            hideProgressDialog();

            Intent intent = new Intent(GeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vin_for_geofence));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            GeoFencingActivity.this.startActivity(intent);

            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            hideProgressDialog();
            return;
        }

        String vin = vehicle.getVin();

        if(vin == null || vin.isEmpty()) {
            hideProgressDialog();

            Intent intent = new Intent(GeoFencingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vin_for_geofence));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            GeoFencingActivity.this.startActivity(intent);

            return;
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vin).build();

        FetchGeoFenceQuery fetchGeoFenceQuery = FetchGeoFenceQuery.builder().where(vehicleWhereUniqueInput).build();

        apolloClient.query(fetchGeoFenceQuery).enqueue(new ApolloCall.Callback<FetchGeoFenceQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchGeoFenceQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response != null && response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getFence() == null) {
                            if(!geoFencingMessageFlag) {
                                Intent intent = new Intent(GeoFencingActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_geo_fencing_set));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                intent.putExtras(bundle);
                                GeoFencingActivity.this.startActivity(intent);

                                geoFencingMessageFlag = true;

                                mSetGeoFenceButton.setText(R.string.set_geo_fencing_area);
                            }
                        } else if(response != null && response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getFence() != null) {
                            //clear prefs before saving
                            getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply();

                            GeoFencing geoFencing = new GeoFencing();

                            //check type
                            FenceType fenceType = response.getData().vehicles().getFence().type();

                            if(fenceType.equals(FenceType.RADIAL)) {
                                geoFencing.setGeoFenceType(fenceType);
                                geoFencing.setCircleRadius(response.getData().vehicles().getFence().radius());
                                LatLng centre = new LatLng(response.getData().vehicles().getFence().centre().latitude(), response.getData().vehicles().getFence().centre().longitude());
                                geoFencing.setCircleGeoFenceCentre(centre);

                            } else {
                                geoFencing.setGeoFenceType(fenceType);
                                List<FetchGeoFenceQuery.Vertex> getGeoFenceVerticesList = response.getData().vehicles().getFence().vertices();
                                ArrayList<LatLng> polygonVerticesArrayList = new ArrayList<>();
                                for(int index = 0 ; index < getGeoFenceVerticesList.size() ; index++) {
                                    double latitude = getGeoFenceVerticesList.get(index).latitude();
                                    double longitude = getGeoFenceVerticesList.get(index).longitude();
                                    polygonVerticesArrayList.add(new LatLng(latitude, longitude));
                                }
                                geoFencing.setPolygonVerticesList(polygonVerticesArrayList);
                            }

                            geoFencing.setGeoFenceId(response.getData().vehicles().getFence().id());

                            //save to prefs
                            getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, new Gson().toJson(geoFencing)).apply();

                            drawGeoFence();
                            fetchVehicleLocation();

                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                //TODO:: show message server error unable to fetch geo fence
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(GeoFencingActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        GeoFencingActivity.this.startActivity(intent);
                    }
                });
            }
        });
    }

    private void fetchVehicleLocation() {

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle != null) {

            String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

            if(userToken == null) {
                return;
            }

            String bearerToken = "Bearer " + userToken;

            DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

            findViewById(R.id.geo_fencing_refreshing_vehicle_location_linear_layout).setVisibility(View.VISIBLE);

            dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.geo_fencing_refreshing_vehicle_location_linear_layout).setVisibility(View.INVISIBLE);

                            try {
                                String responseBody = response.body().string();

                                JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                                int status = jsonObject.get("status").getAsInt();

                                if (status != 200) {
                                    return;
                                }

                                String dataStr = jsonObject.get("data").toString();

                                DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                                if(vehicleSnapshot != null && vehicleSnapshot.getLocation() != null
                                        && vehicleSnapshot.getLocation().getLatitude() != 0 && vehicleSnapshot.getLocation().getLatitude() != 0) {

                                    if(mMap != null) {
                                        mMap.clear();
                                        setupGoogleMaps();

                                        drawGeoFence();
                                        setVehicleCurrentLocationMarker(vehicleSnapshot.getLocation().getLatitude(),
                                                vehicleSnapshot.getLocation().getLongitude());
                                    }
                                }
                            } catch (Exception e) {

                            }
                        }
                    });
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.geo_fencing_refreshing_vehicle_location_linear_layout).setVisibility(View.INVISIBLE);
                        }
                    });
                }
            });
        }
    }

    public int getZoomLevel(Circle circle) {
        int zoomLevel = 11;
        if (circle != null) {
            double radius = circle.getRadius() + circle.getRadius() / 2;
            double scale = radius / 500;
            zoomLevel = (int) (16 - Math.log(scale) / Math.log(2));
        }
        return zoomLevel;
    }

    private void setupGoogleMaps() {

        if(mMap != null) {
            if (ActivityCompat.checkSelfPermission(GeoFencingActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(GeoFencingActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                return;
            }

            recenterMapOnce();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
    }

    private void recenterMapOnce() {
        if(NordicBleService.mCurrentLocation == null || mIsMapCenteredOnce) {
            return;
        }
        mIsMapCenteredOnce = centerMap(mCurrentLocation);
    }

    private boolean centerMap(Location location) {

        if (location == null || mMap == null) {
            return false;
        }

        LatLng currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 17f));

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_REQ_CODE) {
            //check if all permissions are granted
            boolean permissionGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                } else {
                    permissionGranted = false;
                    break;
                }
            }
            if (permissionGranted) {
                setupGoogleMaps();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgressDialog();

        toggleSetGeoFenceButtonBg();

        if (mMapFragment != null) {
            mMapFragment.getMapAsync(this);
        }
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(GEN_EVENT_PHONE_LOCATION_UPDATED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recenterMapOnce();
                }
            });
        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(GeoFencingActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
