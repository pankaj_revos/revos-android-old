package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Device;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.SetPinMutation;
import com.revos.scripts.type.InputSetPin;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_DEVICE_SYNC;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_DISCONNECT;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class SetPinActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private String mPin, mVin, mPhone;

    private OtpView mOtpView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.set_pin_activity);

        setupViews();
    }

    private void setupViews() {
        mOtpView = ((OtpView)findViewById(R.id.setup_pin_enter_pin_otp_view));

        mOtpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                mPin = otp;
            }
        });

        findViewById(R.id.set_pin_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVin = ((EditText)findViewById(R.id.setup_pin_enter_vin_edit_text)).getText().toString().trim();
                mPhone = ((EditText)findViewById(R.id.setup_pin_enter_phone_edit_text)).getText().toString().trim();

                if(mVin == null || mPhone == null || mPin == null || mVin.trim().isEmpty() || mPhone.trim().isEmpty() || mPin.trim().isEmpty()) {

                    Intent intent = new Intent(SetPinActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_fill_all_the_details));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    SetPinActivity.this.startActivity(intent);

                    return;
                }

                makeSetPinCall(mPhone, mVin, mPin);
            }
        });
    }

    private void makeSetPinCall(String phone, String vin, String pin) {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(SetPinActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_set_pin));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            SetPinActivity.this.startActivity(intent);

            return;
        }

        showProgressDialog();

        InputSetPin inputSetPin = InputSetPin
                                    .builder()
                                    .phone(phone)
                                    .vin(vin)
                                    .pin(pin)
                                    .build();

        SetPinMutation setPinMutation = SetPinMutation
                                        .builder()
                                        .inputSetPin(inputSetPin)
                                        .build();

        apolloClient.mutate(setPinMutation).enqueue(new ApolloCall.Callback<SetPinMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SetPinMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(SetPinActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_set_pin));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            SetPinActivity.this.startActivity(intent);

                            clearPrefs();
                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));

                            setResult(RESULT_CANCELED);
                            MainActivity.mSetPinActivityLaunched = false;
                            finish();

                        } else {
                            //update the local copy of the device
                            //todo::robust error checking and handling and data verification
                            if(response.getData() != null && response.getData().device() != null && response.getData().device().setPin() != null
                                && response.getData().device().setPin().device() != null) {

                                SetPinMutation.Device1 cloudDevice = response.getData().device().setPin().device();

                                //we're not using this variable but this call is still required, we can remove this variable if we want to
                                Device device = PrefUtils.getInstance(getApplicationContext())
                                                         .saveDeviceVariablesInPrefs(cloudDevice);

                                //save device pin
                                if(cloudDevice.pin() != null) {
                                    getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, cloudDevice.pin()).apply();
                                } else {
                                    getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, null).apply();
                                }

                                NordicBleService.setDeviceName(null);

                                MetadataDecryptionHelper.getInstance(getApplicationContext()).clearLocalCache();

                                //clear previous metadata
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply();

                                NetworkUtils.getInstance(getApplicationContext()).refreshControllerMetadata(NordicBleService.getDeviceFirmwareVersion());

                                //request sync device
                                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DEVICE_SYNC));

                                setResult(RESULT_OK);
                                MainActivity.mSetPinActivityLaunched = false;
                                finish();
                            } else {

                                Intent intent = new Intent(SetPinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_set_pin));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                SetPinActivity.this.startActivity(intent);

                                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));
                                setResult(RESULT_CANCELED);
                                finish();
                            }
                        }
                    }
                });

            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(SetPinActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        SetPinActivity.this.startActivity(intent);

                        clearPrefs();
                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));

                        setResult(RESULT_CANCELED);
                        MainActivity.mSetPinActivityLaunched = false;
                        finish();
                    }
                });
            }
        });
    }

    private void clearPrefs() {
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(SetPinActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
