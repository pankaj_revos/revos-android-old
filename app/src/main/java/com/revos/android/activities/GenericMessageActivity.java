package com.revos.android.activities;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.revos.android.R;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class GenericMessageActivity extends AppCompatActivity {

    private Button mOkButton;
    private TextView mErrorMessageTextView;
    private LottieAnimationView mErrorMessageLottieImageView;

    private String mErrorMessage, mErrorMessageButtonText;
    private int mShowErrorMessageTimer;
    int mErrorStatus;

    public static final int STATUS_NONE = -1;
    public static final int STATUS_SUCCESS = 0;
    public static final int STATUS_WARNING = 1;
    public static final int STATUS_ERROR = 2;
    public static final int STATUS_INFO = 3;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generic_message_activity);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null) {
            return;
        }

        mErrorMessage = bundle.getString(GENERIC_MESSAGE_TEXT_KEY, "N/A");
        mErrorStatus = bundle.getInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_NONE);
        mShowErrorMessageTimer = bundle.getInt(GENERIC_MESSAGE_TIMER_KEY, -1);
        mErrorMessageButtonText = bundle.getString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, null);

        setupViews();

        populateViews();

    }

    private void setupViews() {

        mErrorMessageLottieImageView = (LottieAnimationView) findViewById(R.id.generic_message_lottie_image_view);
        mErrorMessageTextView = (TextView) findViewById(R.id.generic_message_text_view);
        mOkButton = (Button) findViewById(R.id.generic_message_ok_button);
    }

    private void populateViews() {

        if(mErrorMessage != null) {
            mErrorMessageTextView.setText(mErrorMessage);
        }

        switch (mErrorStatus) {

            case STATUS_SUCCESS:   mErrorMessageLottieImageView.setAnimation(R.raw.ic_generic_message_success);
                                   mErrorMessageLottieImageView.setVisibility(View.VISIBLE);
                                    break;
            case STATUS_WARNING:   mErrorMessageLottieImageView.setAnimation(R.raw.ic_generic_message_warning);
                                   mErrorMessageLottieImageView.setVisibility(View.VISIBLE);
                                    break;
            case STATUS_ERROR:     mErrorMessageLottieImageView.setAnimation(R.raw.ic_generic_message_error);
                                   mErrorMessageLottieImageView.setVisibility(View.VISIBLE);
                                    break;
            case STATUS_INFO:      mErrorMessageLottieImageView.setAnimation(R.raw.ic_generic_message_information);
                                   mErrorMessageLottieImageView.setVisibility(View.VISIBLE);
                                    break;
            case STATUS_NONE:      mErrorMessageLottieImageView.setVisibility(View.GONE);
                                    break;
            default:               mErrorMessageLottieImageView.setVisibility(View.GONE);
                                    break;
        }

        if(mErrorMessageButtonText != null) {

            mOkButton.setVisibility(View.VISIBLE);
            mOkButton.setText(mErrorMessageButtonText);

            //TODO: take action based on the button text value
            //for now finish() the activity
            mOkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        if(mShowErrorMessageTimer > 0) {

            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                    overridePendingTransition(0, android.R.anim.fade_out);
                }
            }, mShowErrorMessageTimer);
        }

    }
}