package com.revos.android.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.fragments.PendingPaymentsFragment;
import com.revos.android.fragments.RentalInvoiceFragment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

public class InvoiceActivity extends AppCompatActivity {

    private Button mViewInvoiceButton, mViewPassbooksButton;
    private TextView mTapToViewInvoiceListTextView, mTapToViewPendingPaymentsTextView;
    private Fragment mInvoicePassbooksFragment;
    private ImageView mInvoiceActivityBackButtonImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //set theme before activity starts
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.invoice_activity);

        setupViews();

        setupAndLoadFragment(R.id.invoice_button);

    }

    private void setupViews() {
        mViewInvoiceButton = findViewById(R.id.invoice_button);
        mViewPassbooksButton = findViewById(R.id.invoice_activity_pending_txn_button);
        mTapToViewInvoiceListTextView = findViewById(R.id.tap_to_view_invoice_text_view);
        mTapToViewPendingPaymentsTextView = findViewById(R.id.tap_to_view_passbooks_text_view);
        mInvoiceActivityBackButtonImageView = findViewById(R.id.invoice_activity_back_button_image_view);

        mInvoiceActivityBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mViewInvoiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //set button style
                mViewInvoiceButton.setBackground(ContextCompat.getDrawable(InvoiceActivity.this, R.drawable.capsule_button_green_filled_background));
                mViewInvoiceButton.setTextAppearance(InvoiceActivity.this, R.style.StyleButtonFilledText16sp);
                mViewPassbooksButton.setBackground(ContextCompat.getDrawable(InvoiceActivity.this, R.drawable.capsule_button_stroke_background));
                mViewPassbooksButton.setTextAppearance(InvoiceActivity.this, R.style.StyleButtonStrokeText16sp);

                mTapToViewInvoiceListTextView.setVisibility(View.INVISIBLE);
                mTapToViewPendingPaymentsTextView.setVisibility(View.VISIBLE);

                setupAndLoadFragment(R.id.invoice_button);
            }
        });

        mViewPassbooksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //set button style
                mViewInvoiceButton.setBackground(ContextCompat.getDrawable(InvoiceActivity.this, R.drawable.capsule_button_stroke_background));
                mViewInvoiceButton.setTextAppearance(InvoiceActivity.this, R.style.StyleButtonStrokeText16sp);
                mViewPassbooksButton.setBackground(ContextCompat.getDrawable(InvoiceActivity.this, R.drawable.capsule_button_green_filled_background));
                mViewPassbooksButton.setTextAppearance(InvoiceActivity.this, R.style.StyleButtonFilledText16sp);

                mTapToViewInvoiceListTextView.setVisibility(View.VISIBLE);
                mTapToViewPendingPaymentsTextView.setVisibility(View.INVISIBLE);

                setupAndLoadFragment(R.id.invoice_activity_pending_txn_button);
            }
        });
    }

    private void setupAndLoadFragment(int callingView) {

        if(callingView == R.id.invoice_button) {
            mInvoicePassbooksFragment = new RentalInvoiceFragment();
        } else if(callingView == R.id.invoice_activity_pending_txn_button) {
            mInvoicePassbooksFragment = new PendingPaymentsFragment();
        }

        getSupportFragmentManager().beginTransaction()
                .add(R.id.invoice_activity_fragment_container_view, mInvoicePassbooksFragment)
                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
