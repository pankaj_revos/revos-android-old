package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.jsonStructures.KycAssociation;
import com.revos.android.utilities.adapters.KycAssociationApprovedListAdapter;
import com.revos.android.utilities.adapters.KycAssociationPendingListAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.FetchAllKYCAssociationsQuery;
import com.revos.scripts.FetchUserKYCDocsQuery;
import com.revos.scripts.UploadFileMutation;
import com.revos.scripts.type.FileType;
import com.revos.scripts.type.KYCStatus;
import com.revos.scripts.type.UploadInput;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;

import static com.revos.android.activities.CameraActivity.PIC_TYPE_KYC_DL_SIDE_1;
import static com.revos.android.activities.CameraActivity.PIC_TYPE_KYC_DL_SIDE_2;
import static com.revos.android.activities.CameraActivity.PIC_TYPE_KYC_PROFILE;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.IMAGE_PREFS;
import static com.revos.android.constants.Constants.KYC_DL_SIDE_1_IMG_BUFFER_KEY;
import static com.revos.android.constants.Constants.KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.KYC_DL_SIDE_2_IMG_BUFFER_KEY;
import static com.revos.android.constants.Constants.KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.KYC_PROFILE_IMG_BUFFER_KEY;
import static com.revos.android.constants.Constants.KYC_PROFILE_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;


public class RentalKycActivity extends AppCompatActivity {

    public static String RENTAL_KYC_VEHICLE_OBJECT_KEY = "rental_kyc_vehicle_object_key";

    private LottieAnimationView mSelfieDocumentStatusLottieView, mDLSide1DocumentStatusLottieView, mDLSide2DocumentStatusLottieView;

    private Button mShareKYCDocsButton, mPendingAssociationsButton, mApprovedAssociationsButton;

    private RelativeLayout mSelfieRelativeLayout, mDLSide1RelativeLayout, mDLSide2RelativeLayout;

    private TextView mUploadProfilePicTextView, mUploadDLSide1PicTextView, mUploadDLSide2PicTextView,
                     mNoKycAssociationsTextView;

    private RecyclerView mKycAssociationsRecyclerView;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private boolean mKycDownloadAttempted = false;

    private HashMap<String, KycAssociation> mPendingKycAssociationHashMap = null, mApprovedKycAssociationHashMap = null;

    private final int KYC_ASSOCIATION_TYPE_PENDING = 101;
    private final int KYC_ASSOCIATION_TYPE_APPROVED = 102;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.rental_kyc_activity);

        setupViews();
    }

    private void setupViews() {

        ImageView mKycBackButtonImageView = findViewById(R.id.rental_kyc_activity_back_button_image_view);

        mSelfieDocumentStatusLottieView = findViewById(R.id.rental_kyc_activity_selfie_lottie_image_view);
        mDLSide1DocumentStatusLottieView = findViewById(R.id.rental_kyc_activity_dl_side1_lottie_image_view);
        mDLSide2DocumentStatusLottieView = findViewById(R.id.rental_kyc_activity_dl_side2_lottie_image_view);

        mSelfieRelativeLayout = findViewById(R.id.rental_kyc_selfie_verification_relative_layout);
        mDLSide1RelativeLayout = findViewById(R.id.rental_kyc_dl_front_verification_relative_layout);
        mDLSide2RelativeLayout = findViewById(R.id.rental_kyc_dl_back_verification_relative_layout);

        mUploadProfilePicTextView = findViewById(R.id.rental_kyc_activity_upload_profile_pic_text_view);
        mUploadDLSide1PicTextView = findViewById(R.id.kyc_dl_front_pic_text_view);
        mUploadDLSide2PicTextView = findViewById(R.id.kyc_dl_back_pic_text_view);
        mNoKycAssociationsTextView = findViewById(R.id.kyc_associations_not_done_text_view);

        mShareKYCDocsButton = findViewById(R.id.rental_kyc_share_docs_button);
        mPendingAssociationsButton = findViewById(R.id.rental_pending_kyc_associations_button);
        mApprovedAssociationsButton = findViewById(R.id.rental_approved_kyc_associations_button);

        mKycAssociationsRecyclerView = findViewById(R.id.kyc_associations_recycler_view);

        mKycBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mSelfieRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraActivity(PIC_TYPE_KYC_PROFILE);
            }
        });

        mDLSide1RelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraActivity(PIC_TYPE_KYC_DL_SIDE_1);
            }
        });

        mDLSide2RelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraActivity(PIC_TYPE_KYC_DL_SIDE_2);
            }
        });

        mShareKYCDocsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RentalKycActivity.this, ScanForRentalKYCActivity.class);
                startActivity(intent);
            }
        });

        mPendingAssociationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mPendingAssociationsButton.setBackground(getDrawable(R.drawable.capsule_button_filled_background));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mPendingAssociationsButton.setTextAppearance(R.style.StyleButtonFilledText14sp);
                }

                mApprovedAssociationsButton.setBackground(getDrawable(R.drawable.capsule_button_stroke_background));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mApprovedAssociationsButton.setTextAppearance(R.style.StyleButtonStrokeText14sp);
                }

                populateKycAssociations(KYC_ASSOCIATION_TYPE_PENDING);
            }
        });

        mApprovedAssociationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mApprovedAssociationsButton.setBackground(getDrawable(R.drawable.capsule_button_filled_background));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mApprovedAssociationsButton.setTextAppearance(R.style.StyleButtonFilledText14sp);
                }

                mPendingAssociationsButton.setBackground(getDrawable(R.drawable.capsule_button_stroke_background));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mPendingAssociationsButton.setTextAppearance(R.style.StyleButtonStrokeText14sp);
                }

                populateKycAssociations(KYC_ASSOCIATION_TYPE_APPROVED);
            }
        });
    }

    private void launchCameraActivity(int imageTypeCode) {
        Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
        intent.putExtra(CameraActivity.PIC_TYPE_ARG, imageTypeCode);
        startActivityForResult(intent, imageTypeCode);
    }

    private void fetchKycDocs() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();
        if(apolloClient == null) {
            return;
        }

        showProgressDialog();

        FetchUserKYCDocsQuery fetchUserKYCDocsQuery = FetchUserKYCDocsQuery.builder().build();

        apolloClient.query(fetchUserKYCDocsQuery).enqueue(new ApolloCall.Callback<FetchUserKYCDocsQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchUserKYCDocsQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Timber.d("RentalKycActivity: fetch kyc activity onResponse");

                        hideProgressDialog();

                        mKycDownloadAttempted = true;

                        if(response.getData() != null && response.getData().profile() != null
                                && response.getData().profile().kyc() != null && response.getData().profile().kyc().documents() != null) {

                            List<FetchUserKYCDocsQuery.Document> kycDocumentsList = response.getData().profile().kyc().documents();

                            //write this data to prefs
                            SharedPreferences sharedPreferences = getSharedPreferences(USER_PREFS, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();

                            if(kycDocumentsList != null && !kycDocumentsList.isEmpty()) {

                                for(int index = 0 ; index < kycDocumentsList.size() ; index++) {
                                    FileType fileType = kycDocumentsList.get(index).type();

                                    if (kycDocumentsList.get(index).file() != null && !kycDocumentsList.get(index).file().name().isEmpty() && fileType != null) {

                                        if (fileType.equals(FileType.KYC_PROFILE)) {
                                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(KYC_PROFILE_UPLOAD_FILE_NAME_KEY, kycDocumentsList.get(index).file().name()).apply();
                                        }

                                        if (fileType.equals(FileType.KYC_DL_SIDE_1)) {
                                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, kycDocumentsList.get(index).file().name()).apply();

                                        }

                                        if (fileType.equals(FileType.KYC_DL_SIDE_2)) {
                                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, kycDocumentsList.get(index).file().name()).apply();

                                        }
                                        editor.apply();
                                    }
                                }

                                toggleKycDocumentViews();
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mKycDownloadAttempted = true;

                        Timber.d("RentalKycActivity: fetch kyc activity onFailure");
                        hideProgressDialog();

                        Intent intent = new Intent(RentalKycActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                        return;
                    }
                });
            }
        });
    }

    private void toggleKycDocumentViews() {

        String profileFileNameKey = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(KYC_PROFILE_UPLOAD_FILE_NAME_KEY, null);
        String dLSide1FileNameKey = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, null);
        String dLSide2FileNameKey = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, null);

        //toggle button
        if((profileFileNameKey != null && !profileFileNameKey.isEmpty())
                || (dLSide1FileNameKey != null && !dLSide1FileNameKey.isEmpty())
                || (dLSide2FileNameKey != null && !dLSide2FileNameKey.isEmpty())) {

            mShareKYCDocsButton.setVisibility(View.VISIBLE);

        } else {
            mShareKYCDocsButton.setVisibility(View.GONE);
        }

        //toggle document upload status
        if(profileFileNameKey != null && !profileFileNameKey.isEmpty()) {
            mUploadProfilePicTextView.setText(R.string.pic_uploaded);
            mSelfieRelativeLayout.setEnabled(false);
            mSelfieDocumentStatusLottieView.setVisibility(View.VISIBLE);
        } else {
            mUploadProfilePicTextView.setText(R.string.tap_here_to_upload);
            mSelfieDocumentStatusLottieView.setVisibility(View.INVISIBLE);
        }

        if(dLSide1FileNameKey != null && !dLSide1FileNameKey.isEmpty()) {
            mUploadDLSide1PicTextView.setText(R.string.pic_uploaded);
            mDLSide1RelativeLayout.setEnabled(false);
            mDLSide1DocumentStatusLottieView.setVisibility(View.VISIBLE);
        } else {
            mUploadDLSide1PicTextView.setText(R.string.tap_here_to_upload);
            mDLSide1DocumentStatusLottieView.setVisibility(View.INVISIBLE);
        }

        if(dLSide2FileNameKey != null && !dLSide2FileNameKey.isEmpty()) {
            mUploadDLSide2PicTextView.setText(R.string.pic_uploaded);
            mDLSide2RelativeLayout.setEnabled(false);
            mDLSide2DocumentStatusLottieView.setVisibility(View.VISIBLE);
        } else {
            mUploadDLSide2PicTextView.setText(R.string.tap_here_to_upload);
            mDLSide2DocumentStatusLottieView.setVisibility(View.INVISIBLE);
        }
    }

    private void fetchAllKycAssociations() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();
        if(apolloClient == null) {
            return;
        }

        FetchAllKYCAssociationsQuery fetchAllKYCAssociationsQuery = FetchAllKYCAssociationsQuery.builder().build();

        Timber.d("RentalKycActivity: fetching all kyc associations for user");

        apolloClient.query(fetchAllKYCAssociationsQuery).enqueue(new ApolloCall.Callback<FetchAllKYCAssociationsQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchAllKYCAssociationsQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Timber.d("RentalKycActivity: getKycAssociationForRentalQuery onResponse");

                        if(response.getData() != null && response.getData().account() != null && response.getData().account().me() != null
                                && response.getData().account().me().kyc() != null && response.getData().account().me().kyc().associations()!= null) {

                            List<FetchAllKYCAssociationsQuery.Association> kycAssociationsList = response.getData().account().me().kyc().associations();

                            //TODO:: make KYC associations list and populate
                            Timber.d("RentalKycActivity: getKycAssociationForRentalQuery onResponse not null");

                            for(int index = 0 ; index < kycAssociationsList.size() ; index++) {

                                KycAssociation kycAssociation = new KycAssociation();

                                if (kycAssociationsList.get(index).status() != null) {
                                    kycAssociation.setKycAssociationStatus(kycAssociationsList.get(index).status());
                                }

                                if (kycAssociationsList.get(index).company() != null) {
                                    if (kycAssociationsList.get(index).company().id() != null && !kycAssociationsList.get(index).company().id().isEmpty()) {

                                        kycAssociation.setRentalProviderId(kycAssociationsList.get(index).company().id());
                                    }

                                    if (kycAssociationsList.get(index).company().name() != null && !kycAssociationsList.get(index).company().name().isEmpty()) {
                                        kycAssociation.setRentalProviderName(kycAssociationsList.get(index).company().name());
                                    }
                                }

                                if (kycAssociationsList.get(index).documentStatus() != null && !kycAssociationsList.get(index).documentStatus().isEmpty()) {
                                    kycAssociation.setDocumentList(kycAssociationsList.get(index).documentStatus());
                                }

                                if (kycAssociationsList.get(index).id() != null && !kycAssociationsList.get(index).id().isEmpty()) {
                                    kycAssociation.setKycAssociationId(kycAssociationsList.get(index).id());
                                }

                                //save details in list
                                if (kycAssociationsList.get(index).status().equals(KYCStatus.APPROVED)) {
                                    mApprovedKycAssociationHashMap.put(kycAssociation.getRentalProviderId(), kycAssociation);
                                } else {
                                    mPendingKycAssociationHashMap.put(kycAssociation.getRentalProviderId(), kycAssociation);
                                }
                            }
                        }

                        populateKycAssociations(KYC_ASSOCIATION_TYPE_PENDING);
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Timber.d("RentalKycActivity: getKycAssociationForRentalQuery on failure");
                        hideProgressDialog();
                    }
                });
            }
        });
    }

    private void populateKycAssociations(int kycAssociationCode) {

        if(kycAssociationCode == KYC_ASSOCIATION_TYPE_PENDING) {

            List<KycAssociation> kycAssociationList = new ArrayList<>();

            if(mPendingKycAssociationHashMap != null && !mPendingKycAssociationHashMap.isEmpty()) {
                for(HashMap.Entry<String, KycAssociation> entry : mPendingKycAssociationHashMap.entrySet()) {
                    kycAssociationList.add(entry.getValue());
                }
            }

            if(!kycAssociationList.isEmpty()) {
                mKycAssociationsRecyclerView.setVisibility(View.VISIBLE);
                mNoKycAssociationsTextView.setVisibility(View.GONE);

                KycAssociationPendingListAdapter kycAssociationPendingListAdapter = new KycAssociationPendingListAdapter(kycAssociationList, RentalKycActivity.this);
                mKycAssociationsRecyclerView.setLayoutManager(new LinearLayoutManager(RentalKycActivity.this));

                mKycAssociationsRecyclerView.setAdapter(kycAssociationPendingListAdapter);

            } else {
                mKycAssociationsRecyclerView.setVisibility(View.GONE);
                mNoKycAssociationsTextView.setText(getString(R.string.no_pending_kyc_associations));
                mNoKycAssociationsTextView.setVisibility(View.VISIBLE);
            }

        } else if(kycAssociationCode == KYC_ASSOCIATION_TYPE_APPROVED) {

            List<KycAssociation> kycAssociationList = new ArrayList<>();
            if(mApprovedKycAssociationHashMap != null && !mApprovedKycAssociationHashMap.isEmpty()) {
                for(HashMap.Entry<String, KycAssociation> entry : mApprovedKycAssociationHashMap.entrySet()) {
                    kycAssociationList.add(entry.getValue());
                }
            }

            if(!kycAssociationList.isEmpty()) {
                mKycAssociationsRecyclerView.setVisibility(View.VISIBLE);
                mNoKycAssociationsTextView.setVisibility(View.GONE);

                KycAssociationApprovedListAdapter kycAssociationApprovedListAdapter = new KycAssociationApprovedListAdapter(kycAssociationList, RentalKycActivity.this);
                mKycAssociationsRecyclerView.setLayoutManager(new LinearLayoutManager(RentalKycActivity.this));

                mKycAssociationsRecyclerView.setAdapter(kycAssociationApprovedListAdapter);

            } else {
                mKycAssociationsRecyclerView.setVisibility(View.GONE);
                mNoKycAssociationsTextView.setText(getString(R.string.no_approved_kyc_associations));
                mNoKycAssociationsTextView.setVisibility(View.VISIBLE);
            }
        }

        updateDocumentStatusViewsIfRequired();
    }

    private void updateDocumentStatusViewsIfRequired() {

        if(mPendingKycAssociationHashMap != null && !mPendingKycAssociationHashMap.isEmpty()) {

            for(HashMap.Entry<String, KycAssociation> entry : mPendingKycAssociationHashMap.entrySet()) {
                List<FetchAllKYCAssociationsQuery.DocumentStatus> documentStatusList = entry.getValue().getDocumentList();

                for(int index = 0 ; index < documentStatusList.size() ; index++) {
                    if(documentStatusList.get(index).document() != null && documentStatusList.get(index).document().type() != null) {

                        FileType fileType = documentStatusList.get(index).document().type();
                        KYCStatus kycStatus = documentStatusList.get(index).status();

                        if(fileType.equals(FileType.KYC_PROFILE)) {
                            if(kycStatus == null || kycStatus.equals(KYCStatus.REJECTED)) {

                                mSelfieDocumentStatusLottieView.setVisibility(View.INVISIBLE);

                                if(kycStatus == null) {
                                    mUploadProfilePicTextView.setText(getString(R.string.tap_here_to_upload));
                                } else {
                                    mUploadProfilePicTextView.setText(getString(R.string.verification_rejected_tap_to_upload));
                                }

                                mSelfieRelativeLayout.setEnabled(true);
                            }
                        } else if(fileType.equals(FileType.KYC_DL_SIDE_1)) {
                            if(kycStatus == null || kycStatus.equals(KYCStatus.REJECTED)) {

                                mDLSide1DocumentStatusLottieView.setVisibility(View.INVISIBLE);

                                if(kycStatus == null) {
                                    mUploadDLSide1PicTextView.setText(getString(R.string.tap_here_to_upload));
                                } else {
                                    mUploadDLSide1PicTextView.setText(getString(R.string.verification_rejected_tap_to_upload));
                                }

                                mDLSide1RelativeLayout.setEnabled(true);
                            }
                        } else if(fileType.equals(FileType.KYC_DL_SIDE_2)) {
                            if(kycStatus == null || kycStatus.equals(KYCStatus.REJECTED)) {

                                mDLSide2DocumentStatusLottieView.setVisibility(View.INVISIBLE);

                                if(kycStatus == null) {
                                    mUploadDLSide2PicTextView.setText(getString(R.string.tap_here_to_upload));
                                } else {
                                    mUploadDLSide2PicTextView.setText(getString(R.string.verification_rejected_tap_to_upload));
                                }

                                mDLSide2RelativeLayout.setEnabled(true);
                            }
                        }
                    }
                }
            }
        }
    }

    private void uploadImage(final FileType fileType) {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if (apolloClient == null) {
            Intent intent = new Intent(RentalKycActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_upload_file));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String bufferKey = KYC_PROFILE_IMG_BUFFER_KEY;
        if(fileType == FileType.KYC_DL_SIDE_1) {
            bufferKey = KYC_DL_SIDE_1_IMG_BUFFER_KEY;
        }
        if(fileType == FileType.KYC_DL_SIDE_2) {
            bufferKey = KYC_DL_SIDE_2_IMG_BUFFER_KEY;
        }

        String fileBuffer = getSharedPreferences(IMAGE_PREFS, MODE_PRIVATE).getString(bufferKey, null);

        if(fileBuffer == null) {
            return;
        }

        showProgressDialog();

        UploadInput uploadInput = UploadInput
                .builder()
                .uploadType(fileType)
                .fileData(fileBuffer)
                .build();

        UploadFileMutation uploadFileMutation = UploadFileMutation
                .builder()
                .uploadInputVar(uploadInput)
                .build();

        Timber.d("RentalKycActivity: UploadFileMutation file upload attempt");

        apolloClient.mutate(uploadFileMutation).enqueue(new ApolloCall.Callback<UploadFileMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UploadFileMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            Timber.d("RentalKycActivity: UploadFileMutation onResponse has errors");

                            Intent intent = new Intent(RentalKycActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_upload_file));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            RentalKycActivity.this.startActivity(intent);

                        } else if(response.getData() != null && response.getData().files() != null && response.getData().files().upload() != null) {

                            Timber.d("RentalKycActivity: UploadFileMutation onResponse not null");

                            if(response.getData().files().upload().type() != null) {

                                if(response.getData().files().upload().type() == FileType.KYC_PROFILE) {

                                    getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(KYC_PROFILE_UPLOAD_FILE_NAME_KEY, response.getData().files().upload().name()).apply();

                                } else if(response.getData().files().upload().type() == FileType.KYC_DL_SIDE_1) {

                                    getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, response.getData().files().upload().name()).apply();

                                } else if(response.getData().files().upload().type() == FileType.KYC_DL_SIDE_2) {

                                    getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, response.getData().files().upload().name()).apply();
                                }

                                showImageUploadSuccessMessage(response.getData().files().upload().type());
                            }

                            toggleKycDocumentViews();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        //todo::handle server error scenarios at a granular level
                        Timber.d("RentalKycActivity: UploadFileMutation onFailure");

                        Intent intent = new Intent(RentalKycActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        RentalKycActivity.this.startActivity(intent);

                    }
                });
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PIC_TYPE_KYC_PROFILE || requestCode == PIC_TYPE_KYC_DL_SIDE_1 || requestCode == PIC_TYPE_KYC_DL_SIDE_2) {
            if(resultCode == RESULT_OK) {
                FileType fileType = FileType.KYC_PROFILE;
                if (requestCode == PIC_TYPE_KYC_DL_SIDE_1) {
                    fileType = FileType.KYC_DL_SIDE_1;
                }
                if (requestCode == PIC_TYPE_KYC_DL_SIDE_2) {
                    fileType = FileType.KYC_DL_SIDE_2;
                }
                uploadImage(fileType);
            }
        }
    }

    private void showImageUploadSuccessMessage(FileType fileType) {
        String message = getString(R.string.image_upload_successful);
        if(fileType.equals(FileType.KYC_PROFILE)) {
            message = getString(R.string.photo_id) + "\n" + getString(R.string.image_upload_successful);
        } else if(fileType.equals(FileType.KYC_DL_SIDE_1)) {
            message = getString(R.string.dl_front) + "\n" + getString(R.string.image_upload_successful);
        } else if(fileType.equals(FileType.KYC_DL_SIDE_2)) {
            message = getString(R.string.dl_back) + "\n" + getString(R.string.image_upload_successful);
        }

        Intent intent = new Intent(RentalKycActivity.this, GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }

        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(RentalKycActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!mKycDownloadAttempted) {
            fetchKycDocs();
        }

        mPendingKycAssociationHashMap = new HashMap<>();
        mApprovedKycAssociationHashMap = new HashMap<>();

        fetchAllKycAssociations();
    }
}
