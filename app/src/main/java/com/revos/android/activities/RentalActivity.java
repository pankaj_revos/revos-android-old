package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.utilities.adapters.RentalActivityViewPagerAdapter;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import static com.revos.android.constants.Constants.GEN_EVENT_VEHICLE_RENTAL_TAB_CHANGE;

public class RentalActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    /** drawables for the tabs*/
    private int[] drawablesIds;

    private ProgressDialog mProgressDialog;

    /** titles for the tabs*/
    private CharSequence mTitles[] = {"Search", "Scan", "Profile"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.rental_activity);

        setupSlidingTabLayout();
    }


    private void setupSlidingTabLayout() {
        //initialize the drawable id
        drawablesIds = new int[]{R.drawable.ic_icon_rental_find_vehicle, R.drawable.ic_scan_green, R.drawable.ic_bullet_list};

        //Initializing the tablayout
        mTabLayout = (TabLayout) findViewById(R.id.rental_activity_main_tab_layout);

        //Adding the tabs using addTab() method
        for (int tabIndex = 0; tabIndex < mTitles.length; ++tabIndex) {
            View customView = getLayoutInflater().inflate(R.layout.rental_tab_custom_view, null);
            customView.findViewById(R.id.icon).setBackgroundResource(drawablesIds[tabIndex]);
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(customView));
        }

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        mViewPager = (ViewPager) findViewById(R.id.rental_activity_main_view_pager);
        mViewPager.setOffscreenPageLimit(2);

        //Creating our pager adapter
        RentalActivityViewPagerAdapter adapter = new RentalActivityViewPagerAdapter(getSupportFragmentManager(), mTitles, mTabLayout.getTabCount(), drawablesIds, RentalActivity.this);

        //Adding adapter to pager
        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //Adding onTabSelectedListener to swipe views
        mTabLayout.addOnTabSelectedListener(this);

        //setting up the tab indicator color
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorSelectedTabIndicator, typedValue, true);
        @ColorInt int selectedTabIndicatorColor = typedValue.data;
        mTabLayout.setSelectedTabIndicatorColor(selectedTabIndicatorColor);


        mTabLayout.getTabAt(0).select();

        //set the vertical divider between the tabs
        View root = mTabLayout.getChildAt(0);
        if(root instanceof LinearLayout) {
            theme.resolveAttribute(R.attr.colorTabDivider, typedValue, true);
            @ColorInt int tabDividerColor = typedValue.data;

            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(tabDividerColor);
            drawable.setSize(2, 1);
            //((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition(), true);

        EventBus.getDefault().post(new EventBusMessage(GEN_EVENT_VEHICLE_RENTAL_TAB_CHANGE + Constants.GEN_DELIMITER + tab.getPosition()));
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        hideProgressDialog();
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(RentalActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int BOOKING_ACTIVITY_FROM_DELIVERABLE_LIST_REQUEST_CODE = 1002;
        if(requestCode == BOOKING_ACTIVITY_FROM_DELIVERABLE_LIST_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                Intent intent = new Intent(RentalActivity.this, ActiveBookingActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
