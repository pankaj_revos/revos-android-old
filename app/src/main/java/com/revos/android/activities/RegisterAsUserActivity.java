package com.revos.android.activities;

import android.os.Bundle;
import com.github.paolorotolo.appintro.AppIntro;
import com.revos.android.R;
import com.revos.android.fragments.RegisterAppFeatureFragment;
import com.revos.android.fragments.RegisterHelpFragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by mohit on 26/8/17.
 */

public class RegisterAsUserActivity extends AppIntro{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //set wizard mode to true so that we don't lose state
        setWizardMode(true);

        //decide and add required fragments
        setDoneText(getString(R.string.done));

        addSlide(new RegisterAppFeatureFragment());
        addSlide(new RegisterHelpFragment());
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
