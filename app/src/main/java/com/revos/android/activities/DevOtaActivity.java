package com.revos.android.activities;

import android.Manifest;
import android.app.NotificationManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.services.DfuService;

import java.io.File;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import timber.log.Timber;

import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.services.NordicBleService.mIsScanning;

public class DevOtaActivity extends AppCompatActivity {

    private CircleProgressView mCircleProgressView;
    private TextView mStatusTextView;
    /** Flag set to true in {@link #onRestart()} and to false in {@link #onPause()}. */
    private boolean mResumed;
    /** Flag set to true if DFU operation was completed while {@link #mResumed} was false. */
    private boolean mDfuCompleted;
    /** The error message received from DFU service while {@link #mResumed} was false. */
    private String mDfuError;
    /**Variables and handler for scanner*/
    private final Handler mHandler = new Handler();
    private BluetoothDevice mSelectedDevice;
    private String mFilePath;
    private Uri mFileStreamUri;
    private static final int SELECT_FILE_REQ = 1;
    private String mLastConnectedDeviceMacAddress, mLastConnectedDeviceName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.dev_ota_activity);

        setupViews();

        setupVariables();

        registerDFUProgressListener();

        startScan();
    }

    private void setupVariables() {
        SharedPreferences sharedPreferences = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
        mLastConnectedDeviceMacAddress = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null);
        mLastConnectedDeviceName = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null);
    }

    private void startScan() {

        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale
            //todo::send message to show a dialog
            return;
        }

        final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();

        if(mIsScanning) {
            return;
        }

        mStatusTextView.setText(getString(R.string.scanning_with_ellipsis));
        mIsScanning = true;
        scanner.startScan(scanCallback);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScan();
                    mIsScanning = false;
                }
            }
        }, 5000);
    }

    private void stopScan() {
        if(mIsScanning) {
            final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
            scanner.stopScan(scanCallback);

            mIsScanning = false;

        }
    }


    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final no.nordicsemi.android.support.v18.scanner.ScanResult result) {
            Timber.d(result.getDevice().getName() + ", " + result.getDevice().getAddress());

            if(result.getDevice().getAddress().equals(mLastConnectedDeviceMacAddress)) {
                mStatusTextView.setText("Device found");
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
            mStatusTextView.setText(getString(R.string.no_devices_found));
        }
    };


    private void setupViews() {
        findViewById(R.id.dev_ota_select_file_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();

            }
        });

        findViewById(R.id.dev_ota_do_ota_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doOTA();
            }
        });

        mCircleProgressView = (CircleProgressView)findViewById(R.id.dev_ota_circle_progress_view);
        mCircleProgressView.setMaxValue(100);
        mCircleProgressView.setAutoTextSize(false);
        mStatusTextView = (TextView) findViewById(R.id.dev_ota_status_text_view);
    }

    private void openFileChooser() {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.setType("*/*");
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, SELECT_FILE_REQ);
    }

    private void registerDFUProgressListener() {
        DfuServiceListenerHelper.registerProgressListener(this, mDfuProgressListener);
    }

    private void unregisterDFUProgressListener() {
        DfuServiceListenerHelper.unregisterProgressListener(this, mDfuProgressListener);
    }

    private void doOTA() {

        if(mLastConnectedDeviceMacAddress == null || mLastConnectedDeviceName == null
                || mLastConnectedDeviceMacAddress.isEmpty() || mLastConnectedDeviceName.isEmpty()) {
            Intent intent = new Intent(DevOtaActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, "No saved device found");
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }


        final DfuServiceInitiator starter = new DfuServiceInitiator(mLastConnectedDeviceMacAddress)
                .setDeviceName(mLastConnectedDeviceName)
                .setKeepBond(false)
                .setForceDfu(false)
                .setForeground(false)
                .setPacketsReceiptNotificationsEnabled(false)
                .setPacketsReceiptNotificationsValue(12)
                .setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true);

        starter.setZip(mFileStreamUri, mFilePath);

        mCircleProgressView.setVisibility(View.VISIBLE);
        mCircleProgressView.spin();

        starter.start(this, DfuService.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterDFUProgressListener();
    }

    private final DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter() {
        @Override
        public void onDeviceConnecting(final String deviceAddress) {
            mStatusTextView.setText(R.string.dfu_status_connecting);
        }

        @Override
        public void onDfuProcessStarting(final String deviceAddress) {
            mStatusTextView.setText(R.string.dfu_status_starting);
        }

        @Override
        public void onEnablingDfuMode(final String deviceAddress) {
            mStatusTextView.setText(R.string.dfu_status_switching_to_dfu);
        }

        @Override
        public void onFirmwareValidating(final String deviceAddress) {
            mStatusTextView.setText(R.string.dfu_status_validating);
        }

        @Override
        public void onDeviceDisconnecting(final String deviceAddress) {
            mStatusTextView.setText(R.string.dfu_status_disconnecting);
        }

        @Override
        public void onDfuCompleted(final String deviceAddress) {
            mCircleProgressView.setVisibility(View.INVISIBLE);
            findViewById(R.id.dev_ota_do_ota_button).setVisibility(View.INVISIBLE);
            mStatusTextView.setText(R.string.dfu_status_completed);
            if (mResumed) {
                // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
                new Handler().postDelayed(() -> {

                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(427);
                }, 200);
            } else {
                // Save that the DFU process has finished
                mDfuCompleted = true;
            }
        }

        @Override
        public void onDfuAborted(final String deviceAddress) {
            mStatusTextView.setText(R.string.dfu_status_aborted);
            // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
            new Handler().postDelayed(() -> {

                // if this activity is still open and upload process was completed, cancel the notification
                final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(427);
            }, 200);
        }

        @Override
        public void onProgressChanged(final String deviceAddress, final int percent, final float speed, final float avgSpeed, final int currentPart, final int partsTotal) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCircleProgressView.stopSpinning();
                    mCircleProgressView.setValueAnimated(percent, 100);
                    mCircleProgressView.setText(percent + "%");
                    mCircleProgressView.invalidate();
                    if (partsTotal > 1)
                        mStatusTextView.setText(getString(R.string.dfu_status_uploading_part, currentPart, partsTotal));
                    else
                        mStatusTextView.setText(R.string.dfu_status_uploading);
                }
            });
        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType, final String message) {
            if (mResumed) {

                // We have to wait a bit before canceling notification. This is called before DfuService creates the last notification.
                new Handler().postDelayed(() -> {
                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(427);
                }, 200);
            } else {
                mDfuError = message;
            }
        }
    };

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case SELECT_FILE_REQ: {
                mFileStreamUri = data.getData();
                if(mFileStreamUri != null) {
                    mFilePath = mFileStreamUri.getPath();
                    mStatusTextView.setText("File : " + new File(mFileStreamUri.getPath()).getName());
                    findViewById(R.id.dev_ota_do_ota_button).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.dev_ota_do_ota_button).setVisibility(View.INVISIBLE);
                }

                break;
            }
            default:
                break;
        }
    }

}
