package com.revos.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.revos.android.R;
import com.revos.android.jsonStructures.TripDataFeed;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.utilities.adapters.TripHistoryInfoAdapter;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.type.ModelProtocol;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.TRIP_DATA_IS_REFRESHING_KEY;
import static com.revos.android.constants.Constants.TRIP_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_TOKEN_KEY;
import static com.revos.android.fragments.AnalyticsFragment.MINIMUM_TRIP_DISTANCE;

public class TripHistoryInfoActivity extends AppCompatActivity {

    private RecyclerView mTripHistoryRecyclerView;
    private LinearLayout mRefreshingTripInfoLinearLayout;
    private Timer mTimer;
    private TripHistoryInfoAdapter mTripHistoryInfoAdapter;

    private ArrayList<TripDataFeed> mProcessedTripList = null;
    private HashMap<String, TripDataFeed> mTripListHashMap = null;

    private final int NO_OF_TRIPS_TO_BE_SHOWN = 10;

    private int mSkipTripsCount = 10;

    boolean isLoading = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //set theme before activity starts
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.trip_history_info_activity);

        setupViews();

        mProcessedTripList = new ArrayList<>();
        mTripListHashMap = new HashMap<>();

        fetchTripData();

        initializeTimer();
    }

    private void setupViews() {

        mRefreshingTripInfoLinearLayout = findViewById(R.id.refreshing_trip_info_linear_layout);
        mTripHistoryRecyclerView = findViewById(R.id.trips_information_list_recycler_view);

        findViewById(R.id.trips_information_back_button_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mTripHistoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mProcessedTripList.size() - 1) {

                        //bottom of list
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        //show progress bar
        mProcessedTripList.add(null);
        mTripHistoryInfoAdapter.notifyItemInserted(mProcessedTripList.size() - 1);

        fetchMoreTrips();
    }

    private void fetchTripData() {
        //api call to refresh trip data
        NetworkUtils.getInstance(getApplicationContext()).refreshTripData();

        String tripInfoList = getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).getString(TRIP_DATA_KEY, null);

        if(tripInfoList == null) {
            return;
        }

        ArrayList<TripDataFeed> tripList = new Gson().fromJson(tripInfoList, new TypeToken<ArrayList<TripDataFeed>>(){}.getType());

        Vehicle vehicle = PrefUtils.getInstance(this).getVehicleObjectFromPrefs();

        //logic for what counts as a trip taken
        if(vehicle != null && vehicle.getModel() != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            for(TripDataFeed trip : tripList) {
                if(trip.getDistance() > MINIMUM_TRIP_DISTANCE && trip.getTripId() != null) {
                    mTripListHashMap.put(trip.getTripId(), trip);
                }
            }
        } else {
            for(TripDataFeed trip : tripList) {
                if(trip.getDistance() > MINIMUM_TRIP_DISTANCE && trip.getEnergy() > 0 && trip.getTripId() != null) {
                    mTripListHashMap.put(trip.getTripId(), trip);
                }
            }
        }

        for(HashMap.Entry<String, TripDataFeed> entry : mTripListHashMap.entrySet()) {
            mProcessedTripList.add(entry.getValue());
        }

        //sort trips by latest date first
        Collections.sort(mProcessedTripList, new Comparator<TripDataFeed>() {
            @Override
            public int compare(TripDataFeed t1, TripDataFeed t2) {
                DateTime dateTime1 = new DateTime(t1.getStartTime());
                DateTime dateTime2 = new DateTime(t2.getStartTime());

                return dateTime2.compareTo(dateTime1);
            }
        });

        //save latest 10 trips in the array list if the tripList's size is more than 10
        if(mProcessedTripList.size() > NO_OF_TRIPS_TO_BE_SHOWN) {
            mProcessedTripList = new ArrayList<>(mProcessedTripList.subList(0, NO_OF_TRIPS_TO_BE_SHOWN));
        }

        populateTripList(mProcessedTripList);
    }

    private void fetchMoreTrips() {

        if(mSkipTripsCount % 10 != 0) {
            //TODO:: show end of the list text hide circle progress view and return
            Intent intent = new Intent(TripHistoryInfoActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, "You've reached the end of the list");
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            //TODO:: hide progress dialogue
            Intent intent = new Intent(TripHistoryInfoActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vin_not_found_try_again));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String userToken = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_TOKEN_KEY, null);

        if(userToken == null) {
            //TODO:: hide progress dialogue
            Intent intent = new Intent(TripHistoryInfoActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_token));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getAllTripsForVehicle(vehicle.getVin(), 10, mSkipTripsCount, bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.code() != 200 || response.body() == null) {
                            return;
                        }

                        try {

                            String responseBody = response.body().string();

                            JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                            JsonArray jsonArray = jsonObject.getAsJsonArray("trips");

                            if(jsonArray != null && jsonArray.size() > 0) {

                                mSkipTripsCount += jsonArray.size();

                                String dataStr = jsonArray.toString();
                                ArrayList<TripDataFeed> tripArrayList = new Gson().fromJson(dataStr, new TypeToken<ArrayList<TripDataFeed>>(){}.getType());

                                for(TripDataFeed fetchedTrip : tripArrayList) {

                                    if(fetchedTrip != null) {

                                        //decide and add to trip hash map
                                        if(vehicle.getModel() != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                                            if(fetchedTrip.getDistance() > MINIMUM_TRIP_DISTANCE && fetchedTrip.getTripId() != null) {
                                                mTripListHashMap.put(fetchedTrip.getTripId(), fetchedTrip);
                                            }
                                        } else {
                                            if(fetchedTrip.getDistance() > MINIMUM_TRIP_DISTANCE && fetchedTrip.getEnergy() > 0 && fetchedTrip.getTripId() != null) {
                                                mTripListHashMap.put(fetchedTrip.getTripId(), fetchedTrip);
                                            }
                                        }
                                    }
                                }

                                decideAndUpdateTripList();
                            }

                        } catch (Exception e) {}
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(TripHistoryInfoActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });
    }

    private void decideAndUpdateTripList() {
        mProcessedTripList.clear();

        for(HashMap.Entry<String, TripDataFeed> entry : mTripListHashMap.entrySet()) {
            mProcessedTripList.add(entry.getValue());
        }

        //sort trips by latest date first
        Collections.sort(mProcessedTripList, new Comparator<TripDataFeed>() {
            @Override
            public int compare(TripDataFeed t1, TripDataFeed t2) {
                DateTime dateTime1 = new DateTime(t1.getStartTime());
                DateTime dateTime2 = new DateTime(t2.getStartTime());

                return dateTime2.compareTo(dateTime1);
            }
        });

        //set mark isLoading as false
        isLoading = false;

        // notify data set
        mTripHistoryInfoAdapter.updateTripList(mProcessedTripList);
    }

    private void initializeTimer() {
        if(mTimer == null) {
            mTimer = new Timer();
        }

        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!hasWindowFocus()) {
                            return;
                        }

                        boolean isTripDataRefreshing = getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).getBoolean(TRIP_DATA_IS_REFRESHING_KEY, false);
                        if(isTripDataRefreshing) {
                            mRefreshingTripInfoLinearLayout.setVisibility(View.VISIBLE);
                        } else {
                            mRefreshingTripInfoLinearLayout.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
            }
        }, 0,1000);
    }

    private void populateTripList(ArrayList<TripDataFeed> tripArrayList) {

        if(tripArrayList != null) {

            mTripHistoryInfoAdapter = new TripHistoryInfoAdapter(tripArrayList, TripHistoryInfoActivity.this);

            mTripHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(TripHistoryInfoActivity.this));

            mTripHistoryRecyclerView.setAdapter(mTripHistoryInfoAdapter);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
