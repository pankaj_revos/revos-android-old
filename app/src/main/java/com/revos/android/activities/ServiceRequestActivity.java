package com.revos.android.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import com.revos.android.R;
import com.revos.android.fragments.ServiceRequestFragment;
import com.revos.android.fragments.SetupVehicleFragment;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.ListServiceRequestQuery;

import java.util.List;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;

public class ServiceRequestActivity extends AppCompatActivity {

    public static String SERVICE_REQUEST_ACTIVITY = "service_request_activity";

    public List<ListServiceRequestQuery.GetAll> mCachedServiceRequestList;

    private Fragment mServiceRequestFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //set theme before activity starts
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        loadAppropriateServiceRequestFragment();

    }

    public void loadAppropriateServiceRequestFragment() {
        if(PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs() != null) {
            loadServiceRequestFragment();
        } else {

            Intent intent = new Intent(ServiceRequestActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.setup_vehicle_to_place_service_request));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            ServiceRequestActivity.this.startActivity(intent);

            loadSetupVehicleFragment();
        }

    }

    private void loadServiceRequestFragment() {
        if(mServiceRequestFragment == null) {
            mServiceRequestFragment = new ServiceRequestFragment();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, mServiceRequestFragment)
                .commit();

    }

    private void loadSetupVehicleFragment() {
        Fragment mSetupVehicleFragment = new SetupVehicleFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, mSetupVehicleFragment, SERVICE_REQUEST_ACTIVITY)
                .commit();
    }

}