package com.revos.android.activities;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.revos.android.BuildConfig;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;

import java.io.File;

import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.CRASH_TIMESTAMP_KEY;
import static com.revos.android.constants.Constants.IS_APP_IN_GUEST_MODE;
import static com.revos.android.constants.Constants.NETWORKING_PREFS;
import static com.revos.android.constants.Constants.SHARED_PREFS_CLEARED_VERSION_CODE_KEY;
import static com.revos.android.constants.Constants.CONTACT_LIST_BUILT_FLAG_KEY;
import static com.revos.android.constants.Constants.GENERAL_PREFS;
import static com.revos.android.constants.Constants.KILL_SWITCH_FLAG_KEY;
import static com.revos.android.constants.Constants.SMS_SENT_TIMESTAMP_KEY;
import static com.revos.android.constants.Constants.permissionsRequired;

/**
 * Created by mohit on 25/8/17.
 */

public class SplashScreenActivity extends AppCompatActivity {

    /**Firebase auth object*/
    private FirebaseAuth mAuth;

    private ImageView logoImageView, poweredByRevosImageView;

    private TextView poweredByRevosTextView;

    private final boolean CLEAR_SHARED_PREFS_ONCE = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if(CLEAR_SHARED_PREFS_ONCE) {
            int sharedPrefClearedVersionCode = getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).getInt(SHARED_PREFS_CLEARED_VERSION_CODE_KEY, 0);

            int currentVersionCode = BuildConfig.VERSION_CODE;

            if(sharedPrefClearedVersionCode != currentVersionCode) {
                PrefUtils.getInstance(getApplicationContext()).clearAllPrefs();
            }
        }

        //clear last crash timestamp
        getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putLong(CRASH_TIMESTAMP_KEY, 0).apply();

        //clear sms sent timestamp
        getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().putLong(SMS_SENT_TIMESTAMP_KEY, 0).apply();

        //clear the kill switch flag
        getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(KILL_SWITCH_FLAG_KEY, false).apply();

        //clear the contact list build flag
        getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(CONTACT_LIST_BUILT_FLAG_KEY, false).apply();

        //clear notification message shown flag
        MainActivity.mNotificationConnectedMessageShown = false;


        setContentView(R.layout.splash_screen_activity);

        //switch on bluetooth
        if(BluetoothAdapter.getDefaultAdapter() != null) {
            BluetoothAdapter.getDefaultAdapter().enable();
        }

        //setup the views
        logoImageView = (ImageView) findViewById(R.id.splash_logo_image_view);
        poweredByRevosImageView = (ImageView) findViewById(R.id.splash_powered_by_revos_image_view);
        poweredByRevosTextView = (TextView) findViewById(R.id.splash_powered_by_revos_text_view);

        //update logo if needed
        updateLogoIfRequired();

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        //Launch RevMode after 1500ms have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //check and launch app intro if required else launch the main app
                decideAndLaunchNextActivity();

            }
        }, 1500);
    }

    private void updateLogoIfRequired() {
        //see if bike logo is available
        //read info from shared prefs
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.GENERAL_PREFS, Context.MODE_PRIVATE);

        String oemLogoFilePath = sharedPreferences.getString(Constants.COMPANY_LOGO_FILE_PATH_KEY, null);

        RelativeLayout.LayoutParams logoViewLayoutParams = (RelativeLayout.LayoutParams) logoImageView.getLayoutParams();

        if(oemLogoFilePath != null) {
            File file = new File(oemLogoFilePath);
            if(file.exists()) {
                //TODO::uncomment this
                /*Bitmap logoBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                logoImageView.setImageBitmap(logoBitmap);*/
                //TODO::comment this
                logoViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                logoViewLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
                int marginTop = dpToPx(72);
                logoViewLayoutParams.setMargins(0, marginTop, 0, 0);
                logoImageView.setBackgroundColor(Color.parseColor("#00FF00"));
                logoImageView.setVisibility(View.VISIBLE);

                poweredByRevosImageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logo_bottom));
                poweredByRevosImageView.setVisibility(View.VISIBLE);
                poweredByRevosTextView.setVisibility(View.VISIBLE);
                return;
            }
        }

        //center revos logo in the center
        logoViewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        logoImageView.setLayoutParams(logoViewLayoutParams);
        logoImageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.logo_center));
        logoImageView.setVisibility(View.VISIBLE);

        //hide powered by views
        poweredByRevosImageView.setVisibility(View.INVISIBLE);
        poweredByRevosTextView.setVisibility(View.INVISIBLE);

    }

    private void decideAndLaunchNextActivity() {

        if(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getBoolean(IS_APP_IN_GUEST_MODE, false)) {
            launchMainActivity();
            finish();
            return;
        }

        //check if permissions are present
        if(ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Constants.permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || (Build.VERSION.SDK_INT > Build.VERSION_CODES.P
                && ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Constants.permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED)) {
            launchProminentDisclosureActivity();
            finish();
            return;
        }

        if(mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
            launchLoginActivity();
            finish();
            return;
        }

        //check if user is logged in or not
        if(mAuth != null && mAuth.getCurrentUser() == null) {
            launchLoginActivity();
            finish();
            return;
        }

        //check if we have an already existing user in prefs
        if(mAuth.getCurrentUser() != null) {
            if(!doUserPrefsExist()) {
                launchLoginActivity();
                finish();
                return;
            }
        }

        //check if all user details are present else launch user details activity
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(user == null
                || user.getFirstName() == null
                || user.getLastName() == null
                || user.getEmail() == null
                || user.getDob() == null
                || user.getFirstName().trim().isEmpty()
                || user.getLastName().trim().isEmpty()
                || user.getEmail().trim().isEmpty()
                || user.getDob().trim().isEmpty()) {
            launchUserDetails();
            finish();
            return;
        }

        //check if all permissions are provided, otherwise launch appintro activity where we ask for permissions

        //check for simple permissions
        if(ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED) {

            launchAppIntro();
            finish();
            return;
        }

        //check for notification access
        String notificationListenerList = Settings.Secure.getString(this.getContentResolver(),"enabled_notification_listeners");
        if(notificationListenerList == null || !(notificationListenerList.contains(getApplicationContext().getPackageName()))) {
            launchAppIntro();
            finish();
            return;
        }

        //check for screen draw permission
        //commenting this out for now as we don't need screen draw permissions
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!Settings.canDrawOverlays(this)) {
                launchAppIntro();
                finish();
                return;
            }
        }*/

        //check if user phone and emergency contact are present
        user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(user == null) {
            //there has been an error, this situation should not occur
            launchLoginActivity();
            finish();
            return;
        } else {
            if(user.getPhone() == null || user.getAltPhone1() == null || user.getPhone().isEmpty() || user.getAltPhone1().isEmpty()) {
                launchAppIntro();
                finish();
                return;
            }
        }
        
        //refresh data
        refreshData();
        //launch the main activity
        launchMainActivity();
        finish();
    }

    private void refreshData() {
        NetworkUtils.getInstance(getApplicationContext()).refreshUserDetails();
        NetworkUtils.getInstance(getApplicationContext()).refreshTripData();
    }

    private void launchUserDetails() {
        startActivity(new Intent(getApplicationContext(), UserDetailActivity.class));
    }

    //checks if a user already exists in prefs
    private boolean doUserPrefsExist() {
        //read info from shared prefs
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE);


        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        if(user == null) {
            return false;
        }

        String userId = user.getId();
        String userFirstName = user.getFirstName();
        String userLastName = user.getLastName();

        if(!userId.trim().isEmpty() && !userFirstName.trim().isEmpty() && !userLastName.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if(serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void launchProminentDisclosureActivity() {
        Intent myIntent = new Intent(SplashScreenActivity.this, ProminentDisclosureActivity.class);
        SplashScreenActivity.this.startActivity(myIntent);
    }

    private void launchLoginActivity() {
        Intent myIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
        SplashScreenActivity.this.startActivity(myIntent);
    }

    private void launchAppIntro() {
        Intent myIntent = new Intent(SplashScreenActivity.this, AppIntroActivity.class);
        SplashScreenActivity.this.startActivity(myIntent);
    }

    private void launchMainActivity() {
        Intent myIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
        SplashScreenActivity.this.startActivity(myIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private int dpToPx(int dp) {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;

        return  Math.round(dp * density);
    }
}
