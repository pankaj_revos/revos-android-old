package com.revos.android.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Device;
import com.revos.android.services.DfuService;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.DownloadOTAQuery;
import com.revos.scripts.MarkOTAStatusMutation;
import com.revos.scripts.type.DeviceUpdateStatus;
import com.revos.scripts.type.DeviceWhereUniqueInput;
import com.revos.scripts.type.InputOtaStatus;
import com.revos.scripts.type.OtaInput;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import at.grabner.circleprogress.CircleProgressView;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;
import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.OTA_DIR_PATH;

public class OtaActivity extends AppCompatActivity {

    private Button mButton;
    private LottieAnimationView mLottieAnimationView;
    private CircleProgressView mCircleProgressView;
    private TextView mStatusTextView;

    private String mOtaVersion;
    private String mDeviceId;

    private File mOtaFile;

    /** Flag set to true in {@link #onRestart()} and to false in {@link #onPause()}. */
    private boolean mResumed;
    /** Flag set to true if DFU operation was completed while {@link #mResumed} was false. */
    private boolean mDfuCompleted;
    /** The error message received from DFU service while {@link #mResumed} was false. */
    private String mDfuError;

    public static String OTA_VERSION_KEY = "ota_version_key";
    public static String OTA_DEVICE_ID_KEY = "ota_device_id_key";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.ota_activity);

        Intent intent = getIntent();

        if(intent == null) {
            return;
        }

        if(NordicBleService.getConnectionState() != STATE_CONNECTED) {
            finish();
            return;
        }

        mOtaVersion = intent.getStringExtra(OTA_VERSION_KEY);

        if(mOtaVersion == null || mOtaVersion.isEmpty()) {
            finish();
            return;
        }

        mDeviceId = intent.getStringExtra(OTA_DEVICE_ID_KEY);

        if(mDeviceId == null || mDeviceId.isEmpty()) {
            finish();
            return;
        }

        setupViews();

        registerDFUProgressListener();

    }

    private void setupViews() {
        mButton = (Button)findViewById(R.id.ota_button);
        mLottieAnimationView = (LottieAnimationView)findViewById(R.id.ota_lottie_animation_view);
        mCircleProgressView = (CircleProgressView)findViewById(R.id.ota_circle_progress_view);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadOTA();
            }
        });

        mStatusTextView = (TextView)findViewById(R.id.ota_status_text_view);
    }

    private void downloadOTA() {
        OtaInput otaInput = OtaInput.builder().version(mOtaVersion).build();
        DownloadOTAQuery downloadOTAQuery = DownloadOTAQuery.builder().otaInputVar(otaInput).build();

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            finish();
            return;
        }

        //hide the button
        mButton.setVisibility(View.INVISIBLE);

        mCircleProgressView.setVisibility(View.INVISIBLE);
        mCircleProgressView.spin();

        mStatusTextView.setVisibility(View.VISIBLE);
        mStatusTextView.setText(getString(R.string.downloading_update));

        mLottieAnimationView.setAnimation(R.raw.ic_cloud_download);
        mLottieAnimationView.setProgress(0);
        mLottieAnimationView.playAnimation();

        apolloClient.query(downloadOTAQuery).enqueue(new ApolloCall.Callback<DownloadOTAQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<DownloadOTAQuery.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty() || response.getData() == null || response.getData().metadata() == null || response.getData().metadata().ota() == null) {
                            mCircleProgressView.stopSpinning();
                            mCircleProgressView.setVisibility(View.INVISIBLE);

                            mLottieAnimationView.setAnimation(R.raw.ic_generic_message_error);
                            mLottieAnimationView.setProgress(0);
                            mLottieAnimationView.playAnimation();

                            mStatusTextView.setText(getString(R.string.server_error));

                            mButton.setText(getString(R.string.retry));
                            mButton.setVisibility(View.VISIBLE);
                        } else {
                            String otaStr = response.getData().metadata().ota();
                            mOtaFile = new File(getExternalFilesDir(OTA_DIR_PATH), "ota.zip");
                            byte[] byteArray = Base64.decode(otaStr, Base64.DEFAULT);
                            if(writeByteArrayAsFile(byteArray, mOtaFile)) {
                                startDFU();
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCircleProgressView.stopSpinning();
                        mCircleProgressView.setVisibility(View.INVISIBLE);

                        mLottieAnimationView.setAnimation(R.raw.ic_generic_message_error);
                        mLottieAnimationView.setProgress(0);
                        mLottieAnimationView.playAnimation();

                        mStatusTextView.setText(getString(R.string.server_error));

                        mButton.setText(getString(R.string.retry));
                        mButton.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

    }

    private void startDFU() {
        if(NordicBleService.getDeviceAddress() == null || NordicBleService.getDeviceAddress().isEmpty()) {
            return;
        }

        mStatusTextView.setText(R.string.applying_update);

        mCircleProgressView.setVisibility(View.INVISIBLE);

        mLottieAnimationView.setAnimation(R.raw.ic_gear);
        mLottieAnimationView.playAnimation();
        mLottieAnimationView.loop(true);

        final DfuServiceInitiator starter = new DfuServiceInitiator(NordicBleService.getDeviceAddress())
                .setDeviceName(NordicBleService.getDeviceName())
                .setKeepBond(false)
                .setForceDfu(false)
                .setForeground(false)
                .setPacketsReceiptNotificationsEnabled(false)
                .setPacketsReceiptNotificationsValue(12)
                .setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true);

        starter.setZip(Uri.fromFile(mOtaFile), mOtaFile.getPath());

        starter.start(this, DfuService.class);
    }

    private boolean writeByteArrayAsFile(byte[] data, File file) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
            return true;
        } catch (IOException e) {
        }
        return false;
    }

    private void registerDFUProgressListener() {
        DfuServiceListenerHelper.registerProgressListener(this, mDfuProgressListener);
    }

    private void unregisterDFUProgressListener() {
        DfuServiceListenerHelper.unregisterProgressListener(this, mDfuProgressListener);
    }

    private void updateDeviceOtaStatus() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        DeviceWhereUniqueInput deviceWhereUniqueInput = DeviceWhereUniqueInput.builder()
                                                        .deviceId(mDeviceId)
                                                        .build();

        InputOtaStatus inputOtaStatus = InputOtaStatus.builder()
                                        .status(DeviceUpdateStatus.UPDATED)
                                        .version(mOtaVersion)
                                        .build();

        MarkOTAStatusMutation markOTAStatusMutation = MarkOTAStatusMutation.builder()
                                                        .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
                                                        .inputOtaStatusVar(inputOtaStatus)
                                                        .build();

        apolloClient.mutate(markOTAStatusMutation).enqueue(new ApolloCall.Callback<MarkOTAStatusMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<MarkOTAStatusMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty()
                                || response.getData() == null
                                || response.getData().device() == null
                                || response.getData().device().markOtaStatus() == null
                                || response.getData().device().markOtaStatus().firmware() == null
                                || response.getData().device().markOtaStatus().expectedFirmware() == null
                                || response.getData().device().markOtaStatus().updateStatus() == null) {
                            mCircleProgressView.stopSpinning();
                            mCircleProgressView.setVisibility(View.INVISIBLE);

                            mLottieAnimationView.setAnimation(R.raw.ic_generic_message_error);
                            mLottieAnimationView.setProgress(0);
                            mLottieAnimationView.playAnimation();

                            mStatusTextView.setText(getString(R.string.server_error));

                            mButton.setText(getString(R.string.retry));
                            mButton.setVisibility(View.VISIBLE);
                        } else {
                            mCircleProgressView.stopSpinning();
                            mCircleProgressView.setVisibility(View.INVISIBLE);

                            mLottieAnimationView.setAnimation(R.raw.ic_generic_message_success);
                            mLottieAnimationView.setProgress(0);
                            mLottieAnimationView.playAnimation();

                            mStatusTextView.setText(getString(R.string.done));

                            MetadataDecryptionHelper.getInstance(getApplicationContext()).clearLocalCache();

                            //clear previous metadata
                            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply();
                            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply();
                            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply();
                            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply();
                            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply();


                            SharedPreferences sharedPreferences = getSharedPreferences(Constants.DEVICE_PREFS, Context.MODE_PRIVATE);
                            Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
                            if(cloudDevice != null) {
                                cloudDevice.setDeviceUpdateStatus(response.getData().device().markOtaStatus().updateStatus());
                                cloudDevice.setFirmware(response.getData().device().markOtaStatus().firmware());
                                cloudDevice.setExpectedFirmware(response.getData().device().markOtaStatus().expectedFirmware());
                                sharedPreferences.edit().putString(DEVICE_JSON_DATA_KEY, new Gson().toJson(cloudDevice)).apply();
                            }

                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE));

                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 2500);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCircleProgressView.stopSpinning();
                        mCircleProgressView.setVisibility(View.INVISIBLE);

                        mLottieAnimationView.setAnimation(R.raw.ic_generic_message_error);
                        mLottieAnimationView.setProgress(0);
                        mLottieAnimationView.playAnimation();

                        mStatusTextView.setText(getString(R.string.server_error));

                        mButton.setText(getString(R.string.retry));
                        mButton.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }

    private final DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter() {
        @Override
        public void onDeviceConnecting(final String deviceAddress) {
        }

        @Override
        public void onDfuProcessStarting(final String deviceAddress) {
        }

        @Override
        public void onEnablingDfuMode(final String deviceAddress) {
        }

        @Override
        public void onFirmwareValidating(final String deviceAddress) {
        }

        @Override
        public void onDeviceDisconnecting(final String deviceAddress) {
        }

        @Override
        public void onDfuCompleted(final String deviceAddress) {
            if (mResumed) {
                // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
                new Handler().postDelayed(() -> {

                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(427);
                }, 200);
            } else {
                if(mOtaFile.exists()) {
                    mOtaFile.delete();
                }
                updateDeviceOtaStatus();
                // Save that the DFU process has finished
                mDfuCompleted = true;
            }
        }

        @Override
        public void onDfuAborted(final String deviceAddress) {
            mLottieAnimationView.setAnimation(R.raw.ic_generic_message_error);
            mLottieAnimationView.setProgress(0);
            mLottieAnimationView.playAnimation();

            mStatusTextView.setText(R.string.dfu_status_aborted);

            mButton.setText(getString(R.string.retry));
            mButton.setVisibility(View.VISIBLE);

            if(mOtaFile.exists()) {
                mOtaFile.delete();
            }

            // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
            new Handler().postDelayed(() -> {

                // if this activity is still open and upload process was completed, cancel the notification
                final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(427);
            }, 200);
        }

        @Override
        public void onProgressChanged(final String deviceAddress, final int percent, final float speed, final float avgSpeed, final int currentPart, final int partsTotal) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCircleProgressView.stopSpinning();
                    mCircleProgressView.setVisibility(View.VISIBLE);
                    mCircleProgressView.setValueAnimated(percent);
                    mCircleProgressView.invalidate();
                    if (partsTotal > 1)
                        mStatusTextView.setText(getString(R.string.dfu_status_uploading_part, currentPart, partsTotal));
                    else
                        mStatusTextView.setText(R.string.dfu_status_uploading);
                }
            });
        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType, final String message) {
            if (mResumed) {

                // We have to wait a bit before canceling notification. This is called before DfuService creates the last notification.
                new Handler().postDelayed(() -> {
                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(427);
                }, 200);
            } else {
                mDfuError = message;
            }
        }
    };

    @Override
    protected void onDestroy() {
        if(mOtaFile.exists()) {
            mOtaFile.delete();
        }

        super.onDestroy();
        unregisterDFUProgressListener();
    }

    @Override
    public void onBackPressed() {
        //do nothing, this activity will finish on it's own
    }

}
