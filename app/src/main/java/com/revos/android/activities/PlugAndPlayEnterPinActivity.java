package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.revos.android.R;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.VerifyPinQuery;
import com.revos.scripts.type.InputVerifyPin;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class PlugAndPlayEnterPinActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private String mPin;

    private OtpView mOtpView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        Log.d("plug","1");
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        Log.d("plug","2");

        setContentView(R.layout.enter_pin_activity);

        Log.d("plug","3");

        setupViews();

        MetadataDecryptionHelper.getInstance(getApplicationContext()).clearLocalCache();
    }

    @Override
    public void onBackPressed() {
        clearPrefs();
        super.onBackPressed();
    }

    private void clearPrefs() {
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();

        getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();
    }

    private void setupViews() {
        mOtpView = ((OtpView)findViewById(R.id.enter_pin_otp_view));

        mOtpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                mPin = otp;
            }
        });

        findViewById(R.id.enter_pin_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verifyPinFromServer(mPin);
            }
        });
    }

    private void verifyPinFromServer(String mPin) {

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            Intent intent = new Intent(PlugAndPlayEnterPinActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        VerifyPinQuery verifyPinQuery = VerifyPinQuery.builder()
                                        .inputVerifyPinVar(InputVerifyPin.builder().pin(mPin).vin(vehicle.getVin()).build())
                                        .build();

        apolloClient.query(verifyPinQuery).enqueue(new ApolloCall.Callback<VerifyPinQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<VerifyPinQuery.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            Intent intent = new Intent(PlugAndPlayEnterPinActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        } else {
                            if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().verifyPin() != null) {
                                if(response.getData().vehicles().verifyPin()) {
                                    Intent intent = new Intent(PlugAndPlayEnterPinActivity.this, GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);

                                    getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();
                                    getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, mPin).apply();

                                    MainActivity.mPlugAndPlayEnterPinActivityLaunched = false;

                                    finish();
                                } else {

                                    Intent intent = new Intent(PlugAndPlayEnterPinActivity.this, GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.incorrect_pin));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            } else {

                                Intent intent = new Intent(PlugAndPlayEnterPinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(PlugAndPlayEnterPinActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }


    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(PlugAndPlayEnterPinActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

}