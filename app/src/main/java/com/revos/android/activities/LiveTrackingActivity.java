package com.revos.android.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.ramotion.fluidslider.FluidSlider;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.DataFeedVehicleLiveLog;
import com.revos.android.jsonStructures.DataFeedVehicleSnapshot;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.DateTimeUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.type.ModelProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;

import static com.revos.android.constants.Constants.HISTORICAL_DATA_LIST_KEY;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_LAST_KNOWN_LOCATION_UPDATED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LATITUDE;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class LiveTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private boolean mIsMapCenteredOnce = false;

    private TextView mCurrentSpeedValueTextView, mIgnitionStatusHeadingTextView, mIgnitionStatusValueTextView, mLastUpdatedAtValueTextView;
    private LinearLayout mRefreshLiveLocationLinearLayout;

    private FluidSlider mFluidSlider;

    private double mLat, mLng;

    private Marker mVehicleCurrentLocationMarker;

    private Timer mFiveSecondTimer;

    private Vehicle mVehicle;
    private int REQUEST_PERMISSION_REQ_CODE = 200;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(MainActivity.mCurrentThemeID);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.live_tracking_activity);

        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.live_tracking_map_fragment);

        mVehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        setupViews();

        if(mMapFragment != null) {
            mMapFragment.getMapAsync(this);
        }

        populateLiveTrackingDetailViews();

        NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLastKnownLocation();
        NetworkUtils.getInstance(getApplicationContext()).refreshVehicleHistoricalData();
        NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLiveLogData();

        if(mFiveSecondTimer == null) {
            mFiveSecondTimer = new Timer();
        }

        mFiveSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshVehicleLiveLogDataIfRequired();
                            refreshVehicleLastKnownLocationIfRequired();
                        }
                    });
                } catch (Exception e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            }
        },100, 5000);
    }

    private void refreshVehicleLiveLogDataIfRequired() {
        if(!hasWindowFocus()) {
            return;
        }

        if(mRefreshLiveLocationLinearLayout != null) {
            mRefreshLiveLocationLinearLayout.setVisibility(View.VISIBLE);
        }
        NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLiveLogData();
    }

    private void refreshVehicleLastKnownLocationIfRequired() {
        if(!hasWindowFocus()) {
            return;
        }
        NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLastKnownLocation();
    }

    private void setupViews() {
        ImageView liveTrackingBackButtonImageView = findViewById(R.id.live_tracking_back_button_image_view);
        mCurrentSpeedValueTextView = findViewById(R.id.live_tracking_current_speed_value_text_view);
        mIgnitionStatusHeadingTextView = findViewById(R.id.live_tracking_ignition_status_heading_text_view);
        mIgnitionStatusValueTextView = findViewById(R.id.live_tracking_ignition_status_value_text_view);
        mLastUpdatedAtValueTextView = findViewById(R.id.live_tracking_last_updated_at_value_text_view);
        mRefreshLiveLocationLinearLayout = findViewById(R.id.live_tracking_refreshing_vehicle_location_linear_layout);

        if(mVehicle != null && mVehicle.getModel() != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mIgnitionStatusHeadingTextView.setVisibility(View.VISIBLE);
            mIgnitionStatusValueTextView.setVisibility(View.VISIBLE);
        } else {
            mIgnitionStatusHeadingTextView.setVisibility(View.GONE);
            mIgnitionStatusValueTextView.setVisibility(View.GONE);
        }

        liveTrackingBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setupFluidSlider();

    }

    private void setupFluidSlider() {
        mFluidSlider = findViewById(R.id.live_tracking_fluid_slider);
        mFluidSlider.setBubbleText("|||");

        String batteryADCStr = getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getString(HISTORICAL_DATA_LIST_KEY, null);
        if(batteryADCStr == null || batteryADCStr.isEmpty()) {
            mFluidSlider.setVisibility(View.INVISIBLE);
            return;
        }

        ArrayList<DataFeedVehicleLiveLog> liveTrackingLogList = new Gson().fromJson(batteryADCStr, new TypeToken<ArrayList<DataFeedVehicleLiveLog>>(){}.getType());

        if(liveTrackingLogList == null || liveTrackingLogList.isEmpty()) {
            mFluidSlider.setVisibility(View.INVISIBLE);
            return;
        }

        Collections.reverse(liveTrackingLogList);

        ArrayList<DataFeedVehicleLiveLog> locationDataArrayList = new ArrayList<>();

        for(int index = 0 ; index < liveTrackingLogList.size() ; index++) {
            if(liveTrackingLogList.get(index).getLatitude() != 0 && liveTrackingLogList.get(index).getLongitude() != 0) {
                locationDataArrayList.add(liveTrackingLogList.get(index));
            }
        }

        if(locationDataArrayList.isEmpty()) {
            mFluidSlider.setVisibility(View.INVISIBLE);
            return;
        }

        mFluidSlider.setVisibility(View.VISIBLE);

        mFluidSlider.setBeginTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                return Unit.INSTANCE;
            }
        });

        mFluidSlider.setEndTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                if(mFluidSlider.getPosition() <= 0.5) {
                    mFluidSlider.setEndText(getString(R.string.fluid_slider_hint));
                    mFluidSlider.setStartText("");
                } else {
                    mFluidSlider.setStartText(getString(R.string.fluid_slider_hint));
                    mFluidSlider.setEndText("");
                }

                mFluidSlider.setBubbleText("|||");
                return Unit.INSTANCE;
            }
        });

        mFluidSlider.setPositionListener(pos -> {

            int arrayPosition = Math.min(locationDataArrayList.size() - 1, Math.round((locationDataArrayList.size() - 1) * pos));

            String timeStampStr = locationDataArrayList.get(arrayPosition).getTimestamp();
            DateTime dateTime = new DateTime(timeStampStr);
            // Format for output
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("h:mma");
            String bubbleText = dtfOut.print(dateTime);

            DateTime now = new DateTime();
            Period period = new Period(dateTime, now);
            if(period.getDays() != 0) {
                bubbleText = DateTimeUtils.getInstance(getApplicationContext()).generateTimeAgoString(dateTime);
            }

            mFluidSlider.setBubbleText(bubbleText.split(" ")[0]);

            double lat = locationDataArrayList.get(arrayPosition).getLatitude();
            double lng = locationDataArrayList.get(arrayPosition).getLongitude();

            if(mVehicleCurrentLocationMarker != null) {
                repositionMarker(mVehicleCurrentLocationMarker, lat, lng);
            }


            if(mMap != null && !mMap.getProjection().getVisibleRegion().latLngBounds.contains(new LatLng(lat, lng)) && locationDataArrayList.size() > 1) {
                LatLngBounds.Builder allLocationLatLngBoundsBuilder = LatLngBounds.builder();

                for(DataFeedVehicleLiveLog livePacket : locationDataArrayList) {
                    allLocationLatLngBoundsBuilder.include(new LatLng(livePacket.getLatitude(), livePacket.getLongitude()));
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(allLocationLatLngBoundsBuilder.build(), convertDipToPixels(50)));
            }

            return Unit.INSTANCE;
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        setupGoogleMaps();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                drawVehicleLocationMarkerAndUpdateViews();
                updateVehicleHeatMap();
                recenterMapOnce();
            }
        });
    }

    private void fetchVehicleLocationFromPrefs() {
        String latStr = getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_LAST_KNOWN_LATITUDE, null);
        String lngStr = getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_LAST_KNOWN_LONGITUDE, null);

        if(latStr != null && lngStr != null) {
            mLat = Double.parseDouble(latStr);
            mLng = Double.parseDouble(lngStr);
        }
    }

    private void drawVehicleLocationMarkerAndUpdateViews() {
        fetchVehicleLocationFromPrefs();

        if(mLat != 0 && mLng != 0) {
            if(mVehicleCurrentLocationMarker == null) {
                mVehicleCurrentLocationMarker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mLat, mLng))
                        .anchor(0.33f, 1)
                        .zIndex(4)
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_bike_unselected, convertDipToPixels(48), convertDipToPixels(48)))));

            } else {
                repositionVehicleLocationMarker();
            }
            mRefreshLiveLocationLinearLayout.setVisibility(View.GONE);

            centerMap(new LatLng(mLat, mLng));

            mFluidSlider.setPosition(1);
            mFluidSlider.setStartText(getString(R.string.fluid_slider_hint));
            mFluidSlider.setEndText("");
            mFluidSlider.setBubbleText("|||");
        }
    }

    private void repositionVehicleLocationMarker() {
        fetchVehicleLocationFromPrefs();

        if(mLat != 0 && mLng != 0) {
            if(mVehicleCurrentLocationMarker == null) {
                mVehicleCurrentLocationMarker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mLat, mLng))
                        .anchor(0.33f, 1)
                        .zIndex(4)
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_bike_unselected, convertDipToPixels(48), convertDipToPixels(48)))));
            }
            repositionMarker(mVehicleCurrentLocationMarker, mLat, mLng);
            mRefreshLiveLocationLinearLayout.setVisibility(View.GONE);
        }
    }

    private void repositionMarker(Marker marker, double lat, double lng) {
        if(marker == null) {
            return;
        }
        marker.setPosition(new LatLng(lat, lng));
    }

    private void populateLiveTrackingDetailViews() {
        DataFeedVehicleSnapshot livePacket = PrefUtils.getInstance(getApplicationContext()).getLivePacketObjectFromPrefs();
        if(livePacket == null) {
            return;
        }

        if(mVehicle != null && mVehicle.getModel() != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mCurrentSpeedValueTextView.setText(String.format("%s %s", String.valueOf(livePacket.getLocation().getGpsSpeed()), getString(R.string.kilometer_per_hour)));
        } else {

            Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
            if(mVehicle == null || mVehicle.getModel() == null || device.getVehicle() == null || device.getVehicle().getModel() == null || device.getVehicle().getModel().getSpeedDivisor() == 0) {
                return;
            }
            int vehicleSpeed = (int) Math.floor(livePacket.getUart().getWheelRpm() / device.getVehicle().getModel().getSpeedDivisor());
            mCurrentSpeedValueTextView.setText(String.format("%s %s", String.valueOf(vehicleSpeed), getString(R.string.kilometer_per_hour)));
        }
        if(livePacket.getIgnition().getIgnition() == 0) {
            mIgnitionStatusValueTextView.setText(getString(R.string.ignition_off));
            mIgnitionStatusValueTextView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_ic_battery_low_dark));
        } else {
            mIgnitionStatusValueTextView.setText(getString(R.string.ignition_on));
            mIgnitionStatusValueTextView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green));
        }

        DateTime dateTime = new DateTime(livePacket.getLocation().getTimestamp());
        String updatedAt = DateTimeUtils.getInstance(getApplicationContext()).generateTimeAgoString(dateTime);
        // Format for output
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("d-MMM-yyyy, h:mm:ss a");

        mLastUpdatedAtValueTextView.setText(dtfOut.print(dateTime));
    }

    private void updateVehicleHeatMap() {
        String batteryADCStr = getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getString(HISTORICAL_DATA_LIST_KEY, null);
        if(batteryADCStr == null || batteryADCStr.isEmpty()) {
            return;
        }

        ArrayList<DataFeedVehicleLiveLog> liveTrackingLogList = new Gson().fromJson(batteryADCStr, new TypeToken<ArrayList<DataFeedVehicleLiveLog>>(){}.getType());
        if(liveTrackingLogList == null || liveTrackingLogList.isEmpty()) {
            return;
        }
        Collections.reverse(liveTrackingLogList);

        ArrayList<LatLng> vehicleLocationArrayList = new ArrayList<>();

        for(int index = 0 ; index < liveTrackingLogList.size() ; index++) {
            if(liveTrackingLogList.get(index).getLatitude() != 0 && liveTrackingLogList.get(index).getLongitude() != 0) {
                double lat = liveTrackingLogList.get(index).getLatitude();
                double lng = liveTrackingLogList.get(index).getLongitude();
                vehicleLocationArrayList.add(new LatLng(lat, lng));
            }
        }

        if(vehicleLocationArrayList.size() > 0) {

            // Create a heat map tile provider, passing it the latlngs of the police stations.
            HeatmapTileProvider heatmapTileProvider = new HeatmapTileProvider.Builder()
                    .data(vehicleLocationArrayList)
                    .build();

            // Add a tile overlay to the map, using the heat map tile provider.
            TileOverlay tileOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));
        }

        mRefreshLiveLocationLinearLayout.setVisibility(View.GONE);
    }

    private void setupGoogleMaps() {

        if(mMap != null) {
            if (ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(LiveTrackingActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                return;
            }

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
    }

    private void recenterMapOnce() {
        if(mIsMapCenteredOnce) {
            return;
        }

        mIsMapCenteredOnce = centerMap(new LatLng(mLat, mLng));
    }

    private boolean centerMap(LatLng location) {

        if (location == null || mMap == null || (mLat == 0 && mLng == 0)) {
            return false;
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 17f));

        return true;
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(NETWORK_EVENT_VEHICLE_LAST_KNOWN_LOCATION_UPDATED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    drawVehicleLocationMarkerAndUpdateViews();
                }
            });
        } else if(event.message.equals(NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setupFluidSlider();
                    updateVehicleHeatMap();
                }
            });
        } else if(event.message.equals(NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    repositionVehicleLocationMarker();
                    populateLiveTrackingDetailViews();
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION_REQ_CODE) {
            boolean permissionGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                } else {
                    permissionGranted = false;
                    break;
                }
            }
            if (permissionGranted) {
                setupGoogleMaps();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        performExitCleanup();
        super.onDestroy();
    }

    private void performExitCleanup() {
        if(mFiveSecondTimer != null) {
            mFiveSecondTimer.cancel();
            mFiveSecondTimer.purge();
        }
    }
}
