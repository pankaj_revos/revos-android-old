package com.revos.android.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.boltCore.android.constants.Constants;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.revos.android.R;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.RentalVehicleCompany;
import com.revos.android.jsonStructures.RentalVehicleModel;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.ApiClient;
import com.revos.android.retrofit.ApiInterface;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.EndRentalBookingMutation;
import com.revos.scripts.FetchLeaseForUserQuery;
import com.revos.scripts.FetchModelImageQuery;
import com.revos.scripts.type.DeviceType;
import com.revos.scripts.type.LeaseStatus;
import com.revos.scripts.type.LeaseWhereInput;
import com.revos.scripts.type.LeaseWhereUniqueInput;
import com.revos.scripts.type.ModelProtocol;
import com.revos.scripts.type.ModelWhereUniqueInput;
import com.revos.scripts.type.TxNodeType;
import com.revos.scripts.type.TxNodeWhereInput;
import com.tbit.tbitblesdk.Bike.TbitBle;
import com.tbit.tbitblesdk.bluetooth.BleClient;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;
import static com.revos.android.activities.ConnectUsingVinActivity.CONNECT_TO_RENTAL_VEHICLE_VIN_KEY;
import static com.revos.android.constants.Constants.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEO_FENCING_PREFS;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class ActiveBookingActivity extends AppCompatActivity {

    private TextView mModelTextView, mRentalVehicleVinTextView, mBookingAmountPaidTextView,
                    mBookingAmountRecipientNameTextView, mBookingAmountRecipientUpiTextView,
                    mActiveBookingDurationTextView, mActiveBookingStartTextView, mActiveBookingEndsOnTextView,
                    mRentalProviderNameTextView, mRentalProviderPhoneNumberTextView;

    private Button mConnectToRentalVehicleButton, mEndBookingButton;

    private ImageView mBackButtonImageView, mActiveBookingModelImageView;

    private ProgressDialog mProgressDialog;

    private String mActiveLeaseId = null; //to be used for ending an active lease

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.active_bookings_activity);

        setupViews();

        fetchLeaseForUserQuery();

        fetchActiveBookingsForUser();
    }

    private void setupViews() {

        mModelTextView = findViewById(R.id.active_booking_model_text_view);
        mRentalVehicleVinTextView = findViewById(R.id.active_booking_rental_vehicle_vin_text_view);
        mBookingAmountPaidTextView = findViewById(R.id.active_booking_amount_paid_text_view);
        mBookingAmountRecipientNameTextView = findViewById(R.id.active_booking_recipient_name_text_view);
        mBookingAmountRecipientUpiTextView = findViewById(R.id.active_booking_recipient_upi_id_text_view);
        mActiveBookingDurationTextView = findViewById(R.id.active_booking_duration_text_view);
        mActiveBookingStartTextView = findViewById(R.id.active_booking_start_date_text_view);
        mActiveBookingEndsOnTextView = findViewById(R.id.active_booking_end_date_text_view);
        mRentalProviderNameTextView = findViewById(R.id.active_booking_rental_provider_text_view);
        mRentalProviderPhoneNumberTextView = findViewById(R.id.active_booking_rental_provider_phone_text_view);

        mBackButtonImageView = findViewById(R.id.active_bookings_back_button_image_view);
        mActiveBookingModelImageView = findViewById(R.id.active_booking_model_image_view);

        mConnectToRentalVehicleButton = findViewById(R.id.active_bookings_connect_to_vehicle_button);
        mEndBookingButton = findViewById(R.id.active_bookings_end_booking_button);

        mBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mConnectToRentalVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(NordicBleService.getConnectionState() != STATE_CONNECTED) {
                    startConnectToVehicleActivity();
                }
            }
        });

        mEndBookingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEndBookingDialog();
            }
        });
    }

    private void fetchLeaseForUserQuery() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            showNoActiveBookingsFound();
            return;
        }

        showProgressDialog();

        LeaseWhereInput leaseWhereInput = LeaseWhereInput
                .builder()
                .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
                .status(LeaseStatus.ACTIVE)
                .build();

        FetchLeaseForUserQuery fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build();

        apolloClient.query(fetchLeaseForUserQuery).enqueue(new ApolloCall.Callback<FetchLeaseForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchLeaseForUserQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        if(response.getData() != null && response.getData().lease() != null && response.getData().lease().getLeasesForUser() != null) {

                            List<FetchLeaseForUserQuery.GetLeasesForUser> fetchedLeaseForUserList = response.getData().lease().getLeasesForUser();

                            if(fetchedLeaseForUserList != null && !fetchedLeaseForUserList.isEmpty()) {
                                if(fetchedLeaseForUserList.get(0) != null) {

                                    mActiveLeaseId = fetchedLeaseForUserList.get(0).id();

                                    populateViews(fetchedLeaseForUserList.get(0));

                                } else {
                                    showNoActiveBookingsFound();
                                }
                            } else {
                                showNoActiveBookingsFound();
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                showNoActiveBookingsFound();
            }
        });
    }

    private void fetchActiveBookingsForUser() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        LeaseWhereInput leaseWhereInput = LeaseWhereInput
                .builder()
                .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
                .status(LeaseStatus.ACTIVE)
                .build();

        showProgressDialog();

        FetchLeaseForUserQuery fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build();

        apolloClient.query(fetchLeaseForUserQuery).enqueue(new ApolloCall.Callback<FetchLeaseForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull com.apollographql.apollo.api.Response<FetchLeaseForUserQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });

                if(response.getData() != null && response.getData().lease() != null && response.getData().lease().getLeasesForUser() != null) {

                    List<FetchLeaseForUserQuery.GetLeasesForUser> fetchedLeaseForUserList = response.getData().lease().getLeasesForUser();

                    if(fetchedLeaseForUserList != null && !fetchedLeaseForUserList.isEmpty()) {
                        if(fetchedLeaseForUserList.get(0) != null) {

                            RentalVehicle rentalVehicle = new RentalVehicle();

                            /*if(fetchedLeaseForUserList.get(0).asset().type().equals(TxNodeType.VEHICLE)) {
                                rentalVehicle.setRentalAssetType(fetchedLeaseForUserList.get(0).asset().type());
                            }*/

                            if(fetchedLeaseForUserList.get(0).asset() != null && fetchedLeaseForUserList.get(0).asset().vehicle() != null) {

                                if(fetchedLeaseForUserList.get(0).asset().vehicle().vin() != null
                                        && !fetchedLeaseForUserList.get(0).asset().vehicle().vin().isEmpty()) {
                                    rentalVehicle.setVin(fetchedLeaseForUserList.get(0).asset().vehicle().vin());
                                }

                                if(fetchedLeaseForUserList.get(0).asset().vehicle().model() != null
                                        && fetchedLeaseForUserList.get(0).asset().vehicle().model().name() != null
                                        && !fetchedLeaseForUserList.get(0).asset().vehicle().model().name().isEmpty()) {

                                    RentalVehicleModel model = new RentalVehicleModel();

                                    model.setName(fetchedLeaseForUserList.get(0).asset().vehicle().model().name());
                                    rentalVehicle.setModel(model);
                                }

                                if(fetchedLeaseForUserList.get(0).asset().vehicle().company() != null
                                        && fetchedLeaseForUserList.get(0).asset().vehicle().company().name() != null
                                        && !fetchedLeaseForUserList.get(0).asset().vehicle().company().name().isEmpty()) {

                                    RentalVehicleCompany company = new RentalVehicleCompany();
                                    company.setName(fetchedLeaseForUserList.get(0).asset().vehicle().company().name());

                                    rentalVehicle.setCompany(company);
                                }
                            }

                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, new Gson().toJson(rentalVehicle)).apply();

                        } else {
                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
                        }
                    } else {
                        getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
                    }
                } else {
                    getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        });
    }


    private void populateViews(FetchLeaseForUserQuery.GetLeasesForUser activeLeaseDetails) {

        if(activeLeaseDetails == null) {
            return;
        }

        if(activeLeaseDetails.asset() != null && activeLeaseDetails.asset().vehicle() != null) {
            mRentalVehicleVinTextView.setText(activeLeaseDetails.asset().vehicle().vin());
        } else {
            mRentalVehicleVinTextView.setText(getString(R.string.not_available));
        }

        if(activeLeaseDetails.leasor() != null && activeLeaseDetails.leasor().company() != null) {
            if(activeLeaseDetails.leasor().company().name() != null && !activeLeaseDetails.leasor().company().name().isEmpty()) {
                mRentalProviderNameTextView.setText(activeLeaseDetails.leasor().company().name());
            } else {
                mRentalProviderNameTextView.setText(getString(R.string.not_available));
            }
        } else {
            mRentalProviderNameTextView.setText(getString(R.string.not_available));
        }

        if(activeLeaseDetails.leasor() != null && activeLeaseDetails.leasor().company() != null) {
            if(activeLeaseDetails.leasor().company().phone() != null && !activeLeaseDetails.leasor().company().phone().isEmpty()) {
                String sanitizedPhoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(activeLeaseDetails.leasor().company().phone().trim());

                mRentalProviderPhoneNumberTextView.setText(sanitizedPhoneNumber);
            } else {
                mRentalProviderPhoneNumberTextView.setText(getString(R.string.not_available));
            }
        } else {
            mRentalProviderPhoneNumberTextView.setText(getString(R.string.not_available));
        }

        if(activeLeaseDetails.asset() != null && activeLeaseDetails.asset().vehicle() != null && activeLeaseDetails.asset().vehicle().model() != null) {
            mModelTextView.setText(activeLeaseDetails.asset().vehicle().model().name());
        } else {
            mModelTextView.setText(getString(R.string.not_available));
        }

        //TODO:: how to find amount paid?
        if(activeLeaseDetails.contract() != null && activeLeaseDetails.contract().txInfo() != null
                && activeLeaseDetails.contract().txInfo().invoice() != null && activeLeaseDetails.contract().txInfo().invoice().passbook() != null
                && !activeLeaseDetails.contract().txInfo().invoice().passbook().isEmpty()) {

            List<FetchLeaseForUserQuery.Passbook> passbookList = activeLeaseDetails.contract().txInfo().invoice().passbook();
            if(passbookList != null && !passbookList.isEmpty()) {

                if(passbookList.get(0).amount() != null) {
                    mBookingAmountPaidTextView.setText(String.valueOf(passbookList.get(0).amount()));
                } else {
                    mBookingAmountPaidTextView.setText(getString(R.string.not_available));
                }
            } else {
                mBookingAmountPaidTextView.setText(getString(R.string.not_available));
            }
        } else {
            mBookingAmountPaidTextView.setText(getString(R.string.not_available));
        }

        //set trip date and time
        String dateFormat = "dd-MMM-yy";
        String timeFormat = "hh:mmaa";

        if(activeLeaseDetails.bookingTime() != null && !activeLeaseDetails.bookingTime().isEmpty()) {
            DateTime tripStartDateTime = new DateTime(activeLeaseDetails.bookingTime());
            Date tripStartDate = tripStartDateTime.toDate();

            String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
            String tripStartTime = new SimpleDateFormat(timeFormat).format(tripStartDate);
            mActiveBookingStartTextView.setText(tripDate + " | " + tripStartTime);
        } else {
            mActiveBookingStartTextView.setText(getString(R.string.not_available));
        }

        if(activeLeaseDetails.endTime() != null && !activeLeaseDetails.endTime().isEmpty()) {

            DateTime bookingEndDateTime = new DateTime(activeLeaseDetails.endTime());
            Date bookingEndDate = bookingEndDateTime.toDate();

            String bookingEndDateStr = new SimpleDateFormat(dateFormat).format(bookingEndDate);
            String bookingEndTime = new SimpleDateFormat(timeFormat).format(bookingEndDate);
            mActiveBookingEndsOnTextView.setText(bookingEndDateStr + " | " + bookingEndTime);

        } else {
            mActiveBookingEndsOnTextView.setText(getString(R.string.not_available));
        }

        if(activeLeaseDetails.asset() != null && activeLeaseDetails.asset().vehicle() != null && activeLeaseDetails.asset().vehicle().model() != null) {

            if(isModelImageDownloadRequired(activeLeaseDetails.asset().vehicle().model().id())) {

                downloadModelImage(activeLeaseDetails.asset().vehicle().model().id());
            } else {

                populateModelImage(activeLeaseDetails.asset().vehicle().model().id());
            }
        }
    }

    private boolean isModelImageDownloadRequired(String modelId) {

        HashMap<String, String> modelImagesHashMap = PrefUtils.getInstance(getApplicationContext()).getModelImageHashMapObjectFromPrefs();

        if(modelImagesHashMap == null || modelImagesHashMap.isEmpty()) {
            return true;
        } else
            return modelImagesHashMap.get(modelId) == null || modelImagesHashMap.get(modelId).isEmpty();

    }

    private void populateModelImage(String modelId) {

        HashMap<String, String> modelImagesHashMap = PrefUtils.getInstance(getApplicationContext()).getModelImageHashMapObjectFromPrefs();

        if(modelImagesHashMap == null) {
            mActiveBookingModelImageView.setImageDrawable(getDrawable(R.drawable.ic_scooter_front_view));
            return;
        }

        String modelImageBuffer = modelImagesHashMap.get(modelId);

        if(modelImageBuffer != null && !modelImageBuffer.isEmpty()) {

            Glide.with(this)
                    .load(modelImageBuffer)
                    .fitCenter()
                    .into(mActiveBookingModelImageView);
        } else {
            mActiveBookingModelImageView.setImageDrawable(getDrawable(R.drawable.ic_scooter_front_view));
        }
    }

    private void downloadModelImage(String modelId) {
        if(modelId == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        ModelWhereUniqueInput modelWhereUniqueInput = ModelWhereUniqueInput.builder().id(modelId).build();
        FetchModelImageQuery fetchModelImageQuery = FetchModelImageQuery.builder().modelWhereUniqueInput(modelWhereUniqueInput).build();

        apolloClient.query(fetchModelImageQuery).enqueue(new ApolloCall.Callback<FetchModelImageQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchModelImageQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getData() != null && response.getData().models() != null && response.getData().models().get() != null
                                && response.getData().models().get().image() != null) {

                            PrefUtils.getInstance(getApplicationContext()).updateModelImageHashMap(modelId, response.getData().models().get().image());
                        }

                        populateModelImage(modelId);
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) { }
        });
    }

    private void showEndBookingDialog() {

        final Dialog endBookingDialog = new Dialog(ActiveBookingActivity.this);
        endBookingDialog.setContentView(R.layout.end_booking_dialog);

        endBookingDialog.findViewById(R.id.end_booking_yes_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeEndActiveBookingCall();

                endBookingDialog.dismiss();
            }
        });

        endBookingDialog.findViewById(R.id.end_booking_no_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endBookingDialog.dismiss();
            }
        });

        endBookingDialog.show();
    }

    private void startConnectToVehicleActivity() {
        String vin = mRentalVehicleVinTextView.getText().toString().trim();

        if(!vin.equals("") && !vin.isEmpty()) {
            Intent intent = new Intent(ActiveBookingActivity.this, ConnectUsingVinActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(CONNECT_TO_RENTAL_VEHICLE_VIN_KEY, vin);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(ActiveBookingActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_rental_vin));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    private void makeEndActiveBookingCall() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            showUnableToEndActiveBooking();
            return;
        }

        LeaseWhereUniqueInput leaseWhereUniqueInput = LeaseWhereUniqueInput.builder().id(mActiveLeaseId).build();

        EndRentalBookingMutation endRentalBookingMutation = EndRentalBookingMutation
                .builder()
                .endBookingWhereInput(leaseWhereUniqueInput)
                .build();

        showProgressDialog();

        apolloClient.mutate(endRentalBookingMutation).enqueue(new ApolloCall.Callback<EndRentalBookingMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<EndRentalBookingMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            Intent intent = new Intent(ActiveBookingActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_end_active_booking));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        } else {
                            if(response.getData() != null && response.getData().lease() != null && response.getData().lease().endC2ULease() != null) {

                                if(response.getData().lease().endC2ULease().status() != null && response.getData().lease().endC2ULease().status().equals(LeaseStatus.ENDED)) {

                                    Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

                                    String vin = null;
                                    if(vehicle != null) {
                                        vin = vehicle.getVin();
                                    }

                                    if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                                        //check if device type is TBIT and we're connected or not
                                        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
                                        if (device != null && device.getDeviceType() != null
                                                && device.getDeviceType() == DeviceType.TBIT
                                                && (TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED))) {
                                            TbitBle.disConnect();
                                        }
                                    }

                                    String pin = getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

                                    //clear device, vehicle and trip data and refresh fragments
                                    getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().clear().apply();
                                    getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
                                    getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();
                                    //clear geo fence prefs
                                    getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().clear().apply();

                                    //clear active rental vehicle data
                                    getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();

                                    makeLockVehicleOnRentalEndApiCall(vin, pin);

                                } else {
                                    showUnableToEndActiveBooking();
                                }
                            } else {
                                showUnableToEndActiveBooking();
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
               runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(ActiveBookingActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void makeLockVehicleOnRentalEndApiCall(String vin, String pin) {

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }
        if(vin == null || vin.isEmpty() || pin == null || pin.isEmpty()) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        //lock command string
        String LOCK = "LOCK";

        showProgressDialog();

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("command", LOCK);
        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        apiInterface.vehicleExecuteCommand(vin, "1234", bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();
                        showBookingEndedSuccessfully();

                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        showBookingEndedSuccessfully();
                    }
                });
            }
        });
    }


    private void showNoActiveBookingsFound() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ActiveBookingActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_active_booking_found));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);

                finish();
            }
        });
    }

    private void showUnableToEndActiveBooking() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ActiveBookingActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_end_active_booking));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void showBookingEndedSuccessfully() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ActiveBookingActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.booking_ended_successfully));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);

                finish();
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(ActiveBookingActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
