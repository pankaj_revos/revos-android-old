package com.revos.android.activities;
import android.os.Bundle;
import android.view.View;

import com.ncorti.slidetoact.SlideToActView;
import com.revos.android.R;
import com.revos.android.utilities.networking.NetworkUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import at.grabner.circleprogress.CircleProgressView;

import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.CRASH_TIMESTAMP_KEY;
import static com.revos.android.constants.Constants.SOS_COOL_OFF;

public class SOSActivity extends AppCompatActivity {

    /**Timer for polling*/
    private Timer mOneSecondTimer;
    private long mCrashTimestamp;

    private CircleProgressView mCircleProgressView;
    private SlideToActView mSlideToActView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.sos_activity);

        setupViews();

        if(mOneSecondTimer == null) {
            mOneSecondTimer = new Timer();
        }

        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCrashTimestamp = getSharedPreferences(APP_PREFS, MODE_PRIVATE).getLong(CRASH_TIMESTAMP_KEY, 0);

                        updateUi();
                    }
                });
            }
        }, 0, 1000);
    }

    private void setupViews() {
        mCircleProgressView = (CircleProgressView)findViewById(R.id.sos_circle_progress_view);
        mSlideToActView = (SlideToActView)findViewById(R.id.sos_slide_to_act_view);

        mSlideToActView.setOnSlideCompleteListener(new SlideToActView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(@NotNull SlideToActView slideToActView) {
                getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putLong(CRASH_TIMESTAMP_KEY, 0).apply();

                mCircleProgressView.setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green));
                mCircleProgressView.setValueAnimated(60, 200);
                mCircleProgressView.setText(getString(R.string.okay));
                findViewById(R.id.sos_text_view).setVisibility(View.INVISIBLE);

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        });
                    }
                }, 2500);
            }
        });
    }

    private void updateUi() {
        long currentTimestamp = System.currentTimeMillis();

        if(currentTimestamp - mCrashTimestamp >= 0 && mCrashTimestamp != 0) {
            int progressValue = SOS_COOL_OFF - Math.round((currentTimestamp - mCrashTimestamp) / 1000.0f);

            if(progressValue >= 0) {
                mCircleProgressView.setValueAnimated(progressValue, 200);
                String generatedText = String.format(getResources().getString(R.string.sending_sms_dynamic), String.valueOf(progressValue));
                mCircleProgressView.setText(generatedText);
            }
        }

        if(NetworkUtils.getInstance(getApplicationContext()).isSosSMSInProgress()) {
            mCircleProgressView.setText(getString(R.string.sending_sms));
            mCircleProgressView.setShowTextWhileSpinning(true);
            mCircleProgressView.spin();
        }

        if(NetworkUtils.getInstance(getApplicationContext()).isSosSMSSent() && !mSlideToActView.isCompleted()) {
            mCircleProgressView.stopSpinning();
            mCircleProgressView.setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.version_code_blue));
            mCircleProgressView.setValueAnimated(SOS_COOL_OFF);
            mCircleProgressView.setText(getString(R.string.sms_sent));
        }

    }

    @Override
    protected void onDestroy() {
        performExitCleanup();
        super.onDestroy();
    }

    private void performExitCleanup() {

        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }

    }

}