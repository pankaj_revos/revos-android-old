package com.revos.android.activities;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.revos.android.R;

import java.util.Objects;

import static com.revos.android.constants.Constants.GENERIC_TEMP_IMAGE_KEY;
import static com.revos.android.constants.Constants.TEMP_IMAGE_PREFS;

/**
 * Created by mohit on 18/9/17.
 */

public class PhotoViewActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.photo_view);

        String uriStr = getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).getString(GENERIC_TEMP_IMAGE_KEY, null);

        if(uriStr == null) {
            finish();
            return;
        }

        Uri imageUri = Uri.parse(uriStr);

        Glide.with(getApplicationContext()).load(imageUri).into((PhotoView)findViewById(R.id.photo_view));

    }
}
