package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.FetchAllRentalCompaniesQuery;
import com.revos.scripts.FetchKYCAssociationForUserQuery;
import com.revos.scripts.ShareKycDocsMutation;
import com.revos.scripts.type.CompanyWhereUniqueInput;
import com.revos.scripts.type.KYCStatus;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import timber.log.Timber;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class ShareKYCForRentalActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup mRentalProvidersRadioGroup;

    private HashMap<Integer, FetchAllRentalCompaniesQuery.GetAllRentalCompany> mRentalCompaniesHashMap;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private LinearLayout mSearchBarLinearLayout;

    public static String SCANNED_RESULT_FOR_KYC_PROVIDER_ID_KEY = "scanned_result_for_kyc_provider_id_key";
    public static String SCANNED_RESULT_FOR_KYC_PROVIDER_NAME_KEY = "scanned_result_for_kyc_provider_name_key";

    private String mScannedProviderId = null, mScannedProviderName = null;

    private AutoCompleteTextView mSearchByProviderAutoCompleteTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.share_kyc_for_rental_activity);

        setupViews();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(bundle.containsKey(SCANNED_RESULT_FOR_KYC_PROVIDER_ID_KEY)) {
                mScannedProviderId = bundle.getString(SCANNED_RESULT_FOR_KYC_PROVIDER_ID_KEY);
            }

            if(bundle.containsKey(SCANNED_RESULT_FOR_KYC_PROVIDER_NAME_KEY)) {
                mScannedProviderName = bundle.getString(SCANNED_RESULT_FOR_KYC_PROVIDER_NAME_KEY);
            }
        }

        if(mScannedProviderId != null) {
            mSearchBarLinearLayout.setVisibility(View.GONE);
            populateScannedProviderDetail();
        } else {
            mSearchBarLinearLayout.setVisibility(View.VISIBLE);
        }

        mRentalCompaniesHashMap = new HashMap<>();
    }

    private void setupViews() {

        Button shareKycButton = findViewById(R.id.share_kyc_docs_button);

        ImageView backButtonImageView = findViewById(R.id.share_kyc_for_rental_activity_back_button_image_view);

        mRentalProvidersRadioGroup = findViewById(R.id.share_kyc_select_providers_radio_group);

        mSearchBarLinearLayout = findViewById(R.id.share_kyc_for_rental_auto_complete_linear_layout);
        mSearchByProviderAutoCompleteTextView = findViewById(R.id.share_kyc_for_rental_auto_complete_text_view);

        backButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish activity
                finish();
            }
        });

        shareKycButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mRentalProvidersRadioGroup.getCheckedRadioButtonId() != -1) {

                    checkKycAssociationForSelectedCompany();
                } else {
                    Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_choose_a_rental_provider));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    private void setupSearchVinAutoCompleteTextView() {

        List<String> providersList = new ArrayList<>();

        for(Map.Entry<Integer, FetchAllRentalCompaniesQuery.GetAllRentalCompany> entry : mRentalCompaniesHashMap.entrySet()) {
            providersList.add(entry.getValue().name());
        }

        ArrayAdapter<String> adapter;
        if (providersList != null && !providersList.isEmpty()) {
            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, providersList);
            mSearchByProviderAutoCompleteTextView.setAdapter(adapter);
        }

        mSearchByProviderAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String selectedVIN = (String) adapterView.getItemAtPosition(position);
                List<String> searchedProviderName = new ArrayList<>();

                for(int index = 0 ; index < providersList.size() ; index++) {

                    if(selectedVIN.equals(providersList.get(index))) {
                        searchedProviderName.add(providersList.get(index));
                    }
                }

                mRentalProvidersRadioGroup.removeAllViews();

                for(int index = 0 ; index < searchedProviderName.size() ; index++) {
                    RadioButton radioButton = new RadioButton(ShareKYCForRentalActivity.this);
                    int radioButtonId = 1;
                    radioButton.setId(radioButtonId);

                    radioButton.setText(searchedProviderName.get(index));
                    if(index == 0) {
                        radioButton.setChecked(true);
                    }

                    radioButton.setOnClickListener(ShareKYCForRentalActivity.this);
                    mRentalProvidersRadioGroup.addView(radioButton);
                }
            }
        });
    }

    private void populateScannedProviderDetail() {

        mRentalProvidersRadioGroup.removeAllViews();

        RadioButton radioButton = new RadioButton(this);
        int radioButtonId = 1;
        radioButton.setId(radioButtonId);

        radioButton.setText(mScannedProviderName);
        radioButton.setChecked(true);

        radioButton.setOnClickListener(this);
        mRentalProvidersRadioGroup.addView(radioButton);
    }

    private String getRentalProviderIdBasedOnRadioButton() {
        //check if QR code is scanned to share KYC docs
        if(mScannedProviderId != null) {
            return mScannedProviderId;
        } else {
            int checkedRadioButtonId = mRentalProvidersRadioGroup.getCheckedRadioButtonId();

            FetchAllRentalCompaniesQuery.GetAllRentalCompany rentalCompany = mRentalCompaniesHashMap.get(checkedRadioButtonId);

            if(rentalCompany == null) {
                return null;
            }

            return rentalCompany.id();
        }
    }

    private void fetchAllRentalCompanies() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if (apolloClient == null) {

            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_rental_providers_for_share_kyc));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        FetchAllRentalCompaniesQuery fetchAllRentalCompaniesQuery = FetchAllRentalCompaniesQuery.builder().build();

        apolloClient.query(fetchAllRentalCompaniesQuery).enqueue(new ApolloCall.Callback<FetchAllRentalCompaniesQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchAllRentalCompaniesQuery.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_rental_providers_for_share_kyc));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        } else {
                            if(response.getData() != null && response.getData().company() != null && response.getData().company().getAllRentalCompanies() != null) {

                                List<FetchAllRentalCompaniesQuery.GetAllRentalCompany> allRentalCompaniesList = response.getData().company().getAllRentalCompanies();

                                populateRentalCompaniesHashMap(allRentalCompaniesList);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                        Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void populateRentalCompaniesHashMap(List<FetchAllRentalCompaniesQuery.GetAllRentalCompany> rentalCompaniesList) {

        if(rentalCompaniesList == null || rentalCompaniesList.isEmpty()) {

            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        for(int index = 0 ; index < rentalCompaniesList.size() ; index++) {
            FetchAllRentalCompaniesQuery.GetAllRentalCompany rentalCompany = rentalCompaniesList.get(index);

            mRentalCompaniesHashMap.put(index+1, rentalCompany);
        }

        populateRentalCompaniesRadioButtons();
    }

    private void populateRentalCompaniesRadioButtons() {

        if(mRentalCompaniesHashMap == null || mRentalCompaniesHashMap.isEmpty()) {
            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_rental_providers_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        mSearchBarLinearLayout.setVisibility(View.VISIBLE);

        mRentalProvidersRadioGroup.removeAllViews();

        boolean isDefaultRadioButtonChecked = false;

        for(Map.Entry<Integer, FetchAllRentalCompaniesQuery.GetAllRentalCompany> entry : mRentalCompaniesHashMap.entrySet()) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setId(entry.getKey());

            radioButton.setText(entry.getValue().name());

            if(!isDefaultRadioButtonChecked) {

                radioButton.setChecked(true);
                isDefaultRadioButtonChecked = true;
            }

            radioButton.setOnClickListener(this);
            mRentalProvidersRadioGroup.addView(radioButton);
        }

        setupSearchVinAutoCompleteTextView();
    }

    private void checkKycAssociationForSelectedCompany() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        CompanyWhereUniqueInput companyWhereUniqueInput = CompanyWhereUniqueInput
                .builder()
                .id(getRentalProviderIdBasedOnRadioButton())
                .build();

        FetchKYCAssociationForUserQuery fetchKYCAssociationForUserQuery = FetchKYCAssociationForUserQuery
                .builder()
                .companyWhereUniqueInput(companyWhereUniqueInput)
                .build();

        apolloClient.query(fetchKYCAssociationForUserQuery).enqueue(new ApolloCall.Callback<FetchKYCAssociationForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchKYCAssociationForUserQuery.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getData() != null && response.getData().account() != null && response.getData().account().getKYC() != null) {
                            Timber.d("KYC association not null");

                            if(response.getData().account().getKYC().status().equals(KYCStatus.APPROVED)) {

                                Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.kyc_verified_for_provider));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                startActivity(intent);

                                hideProgressDialog();

                                fetchAllRentalCompanies();

                            } else {
                                makeShareKycCall();
                            }
                        } else {
                            makeShareKycCall();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        makeShareKycCall();
                    }
                });
            }
        });
    }

    private void makeShareKycCall() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.kyc_submit_to_provider_failed));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            hideProgressDialog();
            return;
        }

        CompanyWhereUniqueInput companyWhereUniqueInput = CompanyWhereUniqueInput.builder().id(getRentalProviderIdBasedOnRadioButton()).build();

        ShareKycDocsMutation shareKycDocsMutation = ShareKycDocsMutation.builder().shareKycDocsRentalUniqueInput(companyWhereUniqueInput).build();

        apolloClient.mutate(shareKycDocsMutation).enqueue(new ApolloCall.Callback<ShareKycDocsMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<ShareKycDocsMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        } else if(response.getData() != null && response.getData().account() != null && response.getData().account().shareKYC() != null) {

                            if(response.getData().account().shareKYC().status() != null) {
                                Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.kyc_shared_awaiting_approval));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                startActivity(intent);

                                finish();
                            }
                        } else {
                            Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.kyc_submit_to_provider_failed));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                        Intent intent = new Intent(ShareKYCForRentalActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(ShareKYCForRentalActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mScannedProviderId == null) {
            fetchAllRentalCompanies();
        }
    }

    @Override
    public void onClick(View view) {

    }
}
