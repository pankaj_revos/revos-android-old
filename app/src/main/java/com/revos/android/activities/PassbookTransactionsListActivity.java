package com.revos.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.BuildConfig;
import com.revos.android.R;
import com.revos.android.jsonStructures.PassbookTxn;
import com.revos.android.utilities.adapters.PassbookTransactionListAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.FetchPassbookAndPaymentInfoQuery;
import com.revos.scripts.type.InputUserPaymentInfo;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class PassbookTransactionsListActivity extends AppCompatActivity {

    private RecyclerView mPassbookRecyclerView;
    private ImageView mPassbookBackButtonImageView;
    private LinearLayout mRefreshPassbookTxnListLinearLayout;

    private String mInvoiceID;

    public static final String RENTAL_INVOICE_ID = "rental_invoice_id";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        //set theme before activity starts
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null) {
            return;
        }

        mInvoiceID = bundle.getString(RENTAL_INVOICE_ID);

        setContentView(R.layout.passbook_transactions_list_layout);

        setupViews();

    }

    private void setupViews() {

        mPassbookBackButtonImageView = findViewById(R.id.passbooks_transactions_list_back_button_image_view);
        mRefreshPassbookTxnListLinearLayout = findViewById(R.id.passbooks_transactions_list_refreshing_linear_layout);
        mPassbookRecyclerView = findViewById(R.id.passbooks_transactions_list_list_recycler_view);

        mPassbookBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }

    private void fetchPassbookList() {

        if(mInvoiceID == null || mInvoiceID.isEmpty()) {
            Intent intent = new Intent(PassbookTransactionsListActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_passbook_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            intent.putExtras(bundle);
            startActivity(intent);

            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
            }
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            Intent intent = new Intent(PassbookTransactionsListActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String packageName = BuildConfig.APPLICATION_ID;
        InputUserPaymentInfo inputUserPaymentInfo = InputUserPaymentInfo.builder().invoice(mInvoiceID).package_(packageName).build();

        FetchPassbookAndPaymentInfoQuery fetchPassbookAndPaymentInfoQuery = FetchPassbookAndPaymentInfoQuery.builder().inputUserPaymentInfo(inputUserPaymentInfo).build();

        if(mRefreshPassbookTxnListLinearLayout.getVisibility() != View.VISIBLE) {
            mRefreshPassbookTxnListLinearLayout.setVisibility(View.VISIBLE);
        }

        apolloClient.query(fetchPassbookAndPaymentInfoQuery).enqueue(new ApolloCall.Callback<FetchPassbookAndPaymentInfoQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchPassbookAndPaymentInfoQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getData() != null && response.getData().account() != null && response.getData().account().paymentInfo() != null && response.getData().account().paymentInfo().invoice() != null) {

                            FetchPassbookAndPaymentInfoQuery.PaymentInfo paymentInfo = response.getData().account().paymentInfo();

                            if(paymentInfo == null) {
                                return;
                            }

                            FetchPassbookAndPaymentInfoQuery.Payee payeeDetails = null;

                            if(paymentInfo.invoice() != null && paymentInfo.invoice().payee() != null && paymentInfo.invoice().payee().company() != null) {
                                payeeDetails = paymentInfo.invoice().payee();
                            }

                            String payeeName = null;
                            String payeeUpiId = null;

                            if(payeeDetails != null && payeeDetails.company() != null) {
                                payeeName = payeeDetails.company().name();

                                List<FetchPassbookAndPaymentInfoQuery.App> appsList = payeeDetails.company().apps();
                                if(appsList != null && !appsList.isEmpty()) {
                                    for(int appsListindex = 0 ; appsListindex < appsList.size() ; appsListindex++) {
                                        if(appsList.get(appsListindex).paymentDetails() != null) {
                                            if(appsList.get(appsListindex).paymentDetails().address() != null && !appsList.get(appsListindex).paymentDetails().address().isEmpty()) {

                                                payeeUpiId = appsList.get(appsListindex).paymentDetails().address();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            String vin = null;
                            if(paymentInfo.invoice() != null && paymentInfo.invoice().asset() != null) {
                                FetchPassbookAndPaymentInfoQuery.Asset asset = paymentInfo.invoice().asset();
                                if(asset != null) {
                                    if(asset.vehicle() != null) {
                                        vin = asset.vehicle().vin();
                                    }
                                }
                            }

                            List<PassbookTxn> passbookTxnList = new ArrayList<>();
                            List<FetchPassbookAndPaymentInfoQuery.Passbook> passbookList = paymentInfo.invoice().passbook();

                            for(int index = 0 ; index < passbookList.size() ; index++) {
                                PassbookTxn passbookTxn = new PassbookTxn();

                                passbookTxn.setId(passbookList.get(index).id());
                                if(passbookList.get(index).amount() != null) {
                                    passbookTxn.setAmount(passbookList.get(index).amount().toString());
                                } else {
                                    passbookTxn.setAmount("");
                                }
                                passbookTxn.setDueDate(passbookList.get(index).dueDate());
                                passbookTxn.setPaymentDate(passbookList.get(index).paymentDate());
                                passbookTxn.setClearanceDate(passbookList.get(index).clearanceDate());
                                passbookTxn.setRemark(passbookList.get(index).remark());
                                passbookTxn.setPassbookTxStatus(passbookList.get(index).status());
                                passbookTxn.setPassbookTxType(passbookList.get(index).type());
                                passbookTxn.setCreationDate(passbookList.get(index).createdAt());
                                passbookTxn.setPayeeUpiId(payeeUpiId);
                                passbookTxn.setVin(vin);

                                passbookTxnList.add(passbookTxn);
                            }

                            mRefreshPassbookTxnListLinearLayout.setVisibility(View.GONE);
                            populateRentalInvoiceList(payeeName, passbookTxnList);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mRefreshPassbookTxnListLinearLayout.setVisibility(View.GONE);

                        Intent intent = new Intent(PassbookTransactionsListActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });
    }

    private void populateRentalInvoiceList(String payeeName, List<PassbookTxn> passbookList) {

        if(passbookList == null || passbookList.isEmpty()) {
            Intent intent = new Intent(PassbookTransactionsListActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invoice_details_not_available));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            finish();
        }

        //sort invoices by latest date first
        Collections.sort(passbookList, new Comparator<PassbookTxn>() {
            @Override
            public int compare(PassbookTxn t1, PassbookTxn t2) {
                DateTime dateTime1 = new DateTime(t1.getCreationDate());
                DateTime dateTime2 = new DateTime(t2.getCreationDate());

                return dateTime2.compareTo(dateTime1);
            }
        });

        PassbookTransactionListAdapter passbookTransactionsListAdapter = new PassbookTransactionListAdapter(payeeName, passbookList, PassbookTransactionsListActivity.this);
        mPassbookRecyclerView.setLayoutManager(new LinearLayoutManager(PassbookTransactionsListActivity.this));
        mPassbookRecyclerView.setAdapter(passbookTransactionsListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchPassbookList();
    }
}