package com.revos.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.revos.android.R;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.FetchDeviceQuery;
import com.revos.scripts.type.DeviceWhereUniqueInput;
import com.revos.scripts.type.RentalStatus;
import com.revos.scripts.type.VehicleStatus;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import timber.log.Timber;

import static com.revos.android.activities.ShareKYCForRentalActivity.SCANNED_RESULT_FOR_KYC_PROVIDER_ID_KEY;
import static com.revos.android.activities.ShareKYCForRentalActivity.SCANNED_RESULT_FOR_KYC_PROVIDER_NAME_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.REVOS_DEVICE_ID_LENGTH;


public class ScanForRentalKYCActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    private boolean mIsFlashOn;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.CAMERA};

    private int REQUEST_CAMERA_PERMISSION_REQ_CODE = 300;

    private ImageView mFlashImageView;

    private LinearLayout mChooseProviderFromListLinearLayout;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.scan_for_rental_kyc_activity);

        setupViews();
    }

    private void setupViews() {
        mScannerView = findViewById(R.id.scan_for_rental_kyc_scanner_view);
        mFlashImageView = findViewById(R.id.scan_for_rental_kyc_flash_image_view);
        mChooseProviderFromListLinearLayout = findViewById(R.id.scan_for_kyc_choose_from_list_linear_layout);

        mFlashImageView.setOnClickListener(v -> {
            toggleFlash(!mIsFlashOn);
            updateCameraFlashImageView();
        });

        ArrayList<BarcodeFormat> barcodeFormats = new ArrayList<>();
        barcodeFormats.add(BarcodeFormat.AZTEC);
        barcodeFormats.add(BarcodeFormat.CODABAR);
        barcodeFormats.add(BarcodeFormat.CODE_39);
        barcodeFormats.add(BarcodeFormat.CODE_93);
        barcodeFormats.add(BarcodeFormat.CODE_128);
        barcodeFormats.add(BarcodeFormat.DATA_MATRIX);
        barcodeFormats.add(BarcodeFormat.EAN_8);
        barcodeFormats.add(BarcodeFormat.EAN_13);
        barcodeFormats.add(BarcodeFormat.ITF);
        barcodeFormats.add(BarcodeFormat.MAXICODE);
        barcodeFormats.add(BarcodeFormat.PDF_417);
        barcodeFormats.add(BarcodeFormat.QR_CODE);
        barcodeFormats.add(BarcodeFormat.RSS_14);
        barcodeFormats.add(BarcodeFormat.RSS_EXPANDED);
        barcodeFormats.add(BarcodeFormat.UPC_A);
        barcodeFormats.add(BarcodeFormat.UPC_E);
        barcodeFormats.add(BarcodeFormat.UPC_EAN_EXTENSION);

        // this parameter will make your HUAWEI phone works great!
        mScannerView.setAspectTolerance(0.5f);
        mScannerView.setFormats(barcodeFormats);
        mScannerView.setAutoFocus(true);

        mChooseProviderFromListLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScanForRentalKYCActivity.this, ShareKYCForRentalActivity.class);
                startActivity(intent);
            }
        });
    }

    private void checkPermissionAndStartScannerView() {

        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void run() {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(mPermissionsRequired, REQUEST_CAMERA_PERMISSION_REQ_CODE);
                    return;
                }

                mScannerView.setResultHandler(ScanForRentalKYCActivity.this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();
            }
        });
    }

    public void onResume() {
        super.onResume();

        checkPermissionAndStartScannerView();
        updateCameraFlashImageView();

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    private void toggleFlash(boolean on) {
        mScannerView.setFlash(on);
    }

    private void updateCameraFlashImageView() {
        mIsFlashOn = mScannerView.getFlash();
        int backGroundId = R.drawable.turn_flash_on_background;
        if(mIsFlashOn) {
            backGroundId = R.drawable.turn_flash_off_background;
        }

        mFlashImageView.setBackground(ContextCompat.getDrawable(getApplicationContext(), backGroundId));

        mFlashImageView.setOnClickListener(v -> {
            toggleFlash(!mIsFlashOn);
            updateCameraFlashImageView();
        });
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Timber.v(rawResult.getText()); // Prints scan results
        Timber.v(rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        /*// If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);*/

        Toast.makeText(getApplicationContext(), getString(R.string.data_read_successfully), Toast.LENGTH_LONG).show();

        showProgressDialog();

        processScannedText(rawResult.getText());
    }

    private void processScannedText(String scannedText) {

        if (scannedText != null && !scannedText.isEmpty()) {

            Intent intent = new Intent(ScanForRentalKYCActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.data_read_successfully));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 1000);
            intent.putExtras(bundle);
            startActivity(intent);

            //check it's first character, if it's 2 then it's device id and string length = 37
            if (scannedText.length() == REVOS_DEVICE_ID_LENGTH && scannedText.charAt(0) == '2') {

                //it's a device id, it's validity will be checked by a call to the server
                String deviceId = scannedText.substring(1);

                fetchDevice(deviceId);

            } else {
                //it might be a generic device id, send it as a whole
                fetchDevice(scannedText);
            }
        } else {
            hideProgressDialog();
            showInvalidQrCode();
        }
    }

    private void fetchDevice(String deviceId) {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            unableToFetchVehicleDetails();
            return;
        }

        DeviceWhereUniqueInput deviceWhereUniqueInput = DeviceWhereUniqueInput
                .builder()
                .deviceId(deviceId)
                .build();

        FetchDeviceQuery fetchDeviceQuery = FetchDeviceQuery
                .builder()
                .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
                .build();

        apolloClient.query(fetchDeviceQuery).enqueue(new ApolloCall.Callback<FetchDeviceQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchDeviceQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        if (response.hasErrors()) {

                            showGenericMessage(false, getString(R.string.unable_to_fetch_device_details), GenericMessageActivity.STATUS_ERROR);

                        } else if (response.getData() != null && response.getData().device() != null && response.getData().device().get() != null) {

                            FetchDeviceQuery.Get fetchedDevice = response.getData().device().get();

                            if (fetchedDevice != null && fetchedDevice.vehicle() != null && fetchedDevice.vehicle().status() == VehicleStatus.RENTAL) {
                                if (fetchedDevice.vehicle().rentalStatus() != null && fetchedDevice.vehicle().rentalStatus() == RentalStatus.AVAILABLE) {


                                    String providerId = null, providerName = null;

                                    //set provider id and provider name
                                    if(fetchedDevice.vehicle().company() != null) {

                                        if(fetchedDevice.vehicle().company().id() != null) {
                                            providerId = fetchedDevice.vehicle().company().id();
                                        }
                                        if(fetchedDevice.vehicle().company().name() != null) {
                                            providerName = fetchedDevice.vehicle().company().name();
                                        }

                                    } else if(fetchedDevice.vehicle().oem() != null) {

                                        if(fetchedDevice.vehicle().oem().id() != null) {
                                            providerId = fetchedDevice.vehicle().oem().id();
                                        }
                                        if(fetchedDevice.vehicle().oem().name() != null) {
                                            providerName = fetchedDevice.vehicle().oem().name();
                                        }
                                    }

                                    if(providerId == null) {
                                        showGenericMessage(false, getString(R.string.unable_to_fetch_rental_providers_for_share_kyc), GenericMessageActivity.STATUS_ERROR);
                                        return;
                                    }

                                    Intent intent = new Intent(ScanForRentalKYCActivity.this, ShareKYCForRentalActivity.class);
                                    intent.putExtra(SCANNED_RESULT_FOR_KYC_PROVIDER_ID_KEY, providerId);
                                    intent.putExtra(SCANNED_RESULT_FOR_KYC_PROVIDER_NAME_KEY, providerName);
                                    startActivity(intent);

                                } else {
                                    showVehicleNotAvailable();
                                }
                            } else {
                                showVehicleNotAvailable();
                            }
                        } else {
                            showVehicleNotAvailable();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        showGenericMessage(false, getString(R.string.server_error), GenericMessageActivity.STATUS_ERROR);
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_CAMERA_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            checkPermissionAndStartScannerView();
        }
    }

    private void showInvalidQrCode() {
        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_qr_code));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void unableToFetchVehicleDetails() {
        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vehicle_details));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showVehicleNotAvailable() {
        hideProgressDialog();

        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_not_available_for_renting));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);

        checkPermissionAndStartScannerView();
    }

    private void showGenericMessage(boolean makeMsgPersistant, String messsage, int messageType) {

        Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, messsage);
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, messageType);

        if(makeMsgPersistant) {
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));

        } else {
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        }

        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }

        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(ScanForRentalKYCActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}