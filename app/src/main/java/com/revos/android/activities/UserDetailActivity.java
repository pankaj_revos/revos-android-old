package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.UpdateUserMutation;
import com.revos.scripts.type.Role;
import com.revos.scripts.type.UserUpdateInput;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_VERIFIED_PHONE_NUMBER;
import static com.revos.android.constants.Constants.permissionsRequired;

public class UserDetailActivity extends AppCompatActivity {

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private String mDOB;

    private User mUser;

    private TextView mUserDetailsDobTextView;

    private EditText mUserDetailsFirstNameEditText, mUserDetailsLastNameEditText, mUserDetailsEmailEditText;

    private CheckBox mUserDetailEulaCheckBox;

    private Button mUserDetailsSubmitButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_detail_activity);

        setupViews();

        populateViews();
    }

    private void setupViews() {

        mUserDetailsFirstNameEditText = (EditText) findViewById(R.id.user_detail_first_name_edit_text);
        mUserDetailsLastNameEditText = (EditText) findViewById(R.id.user_detail_last_name_edit_text);
        mUserDetailsEmailEditText = (EditText) findViewById(R.id.user_detail_email_edit_text);
        mUserDetailsDobTextView = (TextView) findViewById(R.id.user_detail_dob_text_view);
        mUserDetailsSubmitButton = (Button) findViewById(R.id.user_detail_submit_button);
        mUserDetailEulaCheckBox = (CheckBox) findViewById(R.id.user_detail_eula_check_box);


        String generatedText = String.format(getResources().getString(R.string.eula_message_dynamic), getString(R.string.terms_and_condition_link));

        ((TextView)findViewById(R.id.user_detail_eula_text_view)).setMovementMethod(LinkMovementMethod.getInstance());
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            ((TextView)findViewById(R.id.user_detail_eula_text_view)).setText(Html.fromHtml(generatedText));
        } else {
            ((TextView)findViewById(R.id.user_detail_eula_text_view)).setText(Html.fromHtml(generatedText, Html.FROM_HTML_MODE_COMPACT));
        }


        mUserDetailEulaCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked) {
                    mUserDetailsSubmitButton.setVisibility(View.VISIBLE);
                } else {
                    mUserDetailsSubmitButton.setVisibility(View.INVISIBLE);
                }
            }
        });

        //setup dob text view
        mUserDetailsDobTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SpinnerDatePickerDialogBuilder()
                        .context(UserDetailActivity.this)
                        .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);

                                Date date = calendar.getTime();
                                String dateFormat = "dd/MMM/yyyy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                                mUserDetailsDobTextView.setText(sdf.format(calendar.getTime()));
                                mDOB = new DateTime(calendar.getTime()).toString();
                            }
                        })
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });

        //setup submit button
        mUserDetailsSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserObject();
            }
        });
    }

    private void updateUserObject() {
        showProgressDialog();
        mUser = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        if(mUser == null) {
            hideProgressDialog();

            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.user_null));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            UserDetailActivity.this.startActivity(intent);

            return;
        }

        //if user has signed up using phone number, then the number is already verified and ready to be saved to user details
        String preSanitizedSignInPhoneNumber = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_VERIFIED_PHONE_NUMBER, null);
        String sanitizedSignInPhoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(preSanitizedSignInPhoneNumber);


        String firstName = mUserDetailsFirstNameEditText.getText().toString().trim();
        String lastName = mUserDetailsLastNameEditText.getText().toString().trim();
        String email = mUserDetailsEmailEditText.getText().toString().trim();
        String dob = String.valueOf(mDOB);

        if(firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || dob.isEmpty() || dob == null || dob.equals("null")) {

            hideProgressDialog();

            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_fill_all_the_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            UserDetailActivity.this.startActivity(intent);

            return;
        }

        //calculate user age
        DateTime currentDateTime = new DateTime();
        DateTime dobDateTime = new DateTime(dob);

        int ageInYears = new Period(dobDateTime, currentDateTime).getYears();
        if(ageInYears >= 16){
            mDOB = dob;
        } else {
            hideProgressDialog();

            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.user_must_be_16_years_or_above));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        mUser.setFirstName(firstName);
        mUser.setLastName(lastName);
        mUser.setEmail(email);
        mUser.setDob(mDOB);
        if(sanitizedSignInPhoneNumber != null && !sanitizedSignInPhoneNumber.isEmpty()) {
            mUser.setPhone(sanitizedSignInPhoneNumber);
        }

        if(mUser.getRole() == Role.USER || mUser.getRole() == Role.UNAUTHORISED) {
            mUser.setRole(Role.RIDER);
        }
        makeUpdateUserCall();
    }

    private void makeUpdateUserCall() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            UserDetailActivity.this.startActivity(intent);

            return;
        }

        showProgressDialog();

        UserUpdateInput userUpdateInput = null;

        if(mUser.getPhone() != null && !mUser.getPhone().isEmpty()) {
            userUpdateInput = UserUpdateInput
                            .builder()
                            .firstName(mUser.getFirstName())
                            .lastName(mUser.getLastName())
                            .email(mUser.getEmail())
                            .dob(mUser.getDob())
                            .gender(mUser.getGenderType())
                            .role(mUser.getRole())
                            .phone(mUser.getPhone())
                            .build();
        } else {
            userUpdateInput = UserUpdateInput
                            .builder()
                            .firstName(mUser.getFirstName())
                            .lastName(mUser.getLastName())
                            .email(mUser.getEmail())
                            .dob(mUser.getDob())
                            .gender(mUser.getGenderType())
                            .role(mUser.getRole())
                            .build();
        }

        if(userUpdateInput == null) {

            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_set_user_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            UserDetailActivity.this.startActivity(intent);

            return;
        }

        UpdateUserMutation updateUserMutation = UpdateUserMutation.builder().data(userUpdateInput).build();

        apolloClient.mutate(updateUserMutation).enqueue(new ApolloCall.Callback<UpdateUserMutation.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<UpdateUserMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            String message = getString(R.string.unable_to_set_user_details);
                            if(response.getErrors().get(0) != null) {
                                BigDecimal statusCode = (BigDecimal)response.getErrors().get(0).getCustomAttributes().get("statusCode");
                                if(statusCode.intValue() == 409) {
                                    message = response.getErrors().get(0).getMessage();
                                }
                            }

                            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            UserDetailActivity.this.startActivity(intent);

                        } else {
                            //store user object in prefs
                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_JSON_DATA_KEY, new Gson().toJson(mUser)).apply();
                            decideAndLaunchNextActivity();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        //todo::handle server error scenarios at a granular level

                        Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        UserDetailActivity.this.startActivity(intent);

                    }
                });
            }
        });
    }

    private void decideAndLaunchNextActivity() {
        SharedPreferences sharedPreferences;
        //check if all user details are present else launch user details activity

        //check for location permissions
        if(ActivityCompat.checkSelfPermission(UserDetailActivity.this, Constants.permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || (Build.VERSION.SDK_INT > Build.VERSION_CODES.P
                && ActivityCompat.checkSelfPermission(UserDetailActivity.this, Constants.permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED)) {
            launchProminentDisclosureActivity();
        }

        //check for simple permissions
        if(ActivityCompat.checkSelfPermission(UserDetailActivity.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(UserDetailActivity.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED) {

            launchAppIntro();
            finish();
            return;
        }

        //check for notification access
        String notificationListenerList = Settings.Secure.getString(this.getContentResolver(),"enabled_notification_listeners");
        if(notificationListenerList == null || !(notificationListenerList.contains(getApplicationContext().getPackageName()))) {
            launchAppIntro();
            finish();
            return;
        }


        //check if bike prefs are present
        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(device == null) {
            launchAppIntro();
            finish();
            return;
        }

        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(user == null) {

            Intent intent = new Intent(UserDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.user_null));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            UserDetailActivity.this.startActivity(intent);

            finish();
            return;
        } else if(user.getPhone() == null || user.getAltPhone1() == null || user.getPhone().isEmpty() || user.getAltPhone1().isEmpty()) {
            launchAppIntro();
            finish();
            return;
        }

        launchMainActivity();
        finish();
    }

    private void launchAppIntro() {
        hideProgressDialog();
        Intent myIntent = new Intent(UserDetailActivity.this, AppIntroActivity.class);
        UserDetailActivity.this.startActivity(myIntent);
    }

    private void launchMainActivity() {
        hideProgressDialog();
        Intent myIntent = new Intent(UserDetailActivity.this, MainActivity.class);
        UserDetailActivity.this.startActivity(myIntent);
    }

    private void launchProminentDisclosureActivity() {
        Intent myIntent = new Intent(UserDetailActivity.this, ProminentDisclosureActivity.class);
        UserDetailActivity.this.startActivity(myIntent);
    }


    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(UserDetailActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    private void populateViews() {
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        if(user != null) {
            if(user.getFirstName() != null) {
                mUserDetailsFirstNameEditText.setText(user.getFirstName());
            }
            if(user.getLastName() != null) {
                mUserDetailsLastNameEditText.setText(user.getLastName());
            }
            if(user.getEmail() != null) {
                mUserDetailsEmailEditText.setText(user.getEmail());
            }
            if(user.getDob() != null) {
                DateTime dateTime = new DateTime(user.getDob());
                Date dob = dateTime.toDate();
                String dateFormat = "dd/MMM/yyyy"; //In which you need put here

                mDOB = dateTime.toString();
                mUserDetailsDobTextView.setText(new SimpleDateFormat(dateFormat).format(dob));

            }
        }
    }
}
