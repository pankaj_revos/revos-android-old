package com.revos.android.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import com.revos.android.fragments.OwnersVehicleListFragment;

public class VehiclesAndDevicesListActivity extends AppCompatActivity {

    public static String VEHICLE_DETAILS_ACTIVITY ="vehicle_details_activity";

    Fragment mOwnersVehicleListFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        //set theme before activity starts
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        loadOwnersVehiclesListFragment();
    }

    public void loadAppropriateVehicleFragment() {
        //TODO:: decide and open vehicles list and devices list fragment
    }

    private void loadOwnersVehiclesListFragment() {

        mOwnersVehicleListFragment = new OwnersVehicleListFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, mOwnersVehicleListFragment)
                .commit();
    }

}