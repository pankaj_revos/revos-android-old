package com.revos.android.activities;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.utilities.adapters.DiagnosticsActivityViewPagerAdapter;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameterParser;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameters;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.utilities.vehicleDataTransfer.SpeedometerDataExchanger.PACKET_TYPE_LIVE;

public class DiagnosticsActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.diagnostics_activity);

        findViewById(R.id.diagnostics_back_button_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tabLayout = (TabLayout)findViewById(R.id.diagnostics_tab_layout);
        viewPager = (ViewPager)findViewById(R.id.diagnostics_view_pager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout.addTab(tabLayout.newTab().setText("Graph"));
        tabLayout.addTab(tabLayout.newTab().setText("Text"));
        tabLayout.addTab(tabLayout.newTab().setText("Control"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final DiagnosticsActivityViewPagerAdapter adapter = new DiagnosticsActivityViewPagerAdapter(this,getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}