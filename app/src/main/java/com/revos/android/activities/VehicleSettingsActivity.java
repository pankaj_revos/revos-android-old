package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.User;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.ble.BleUtils;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.Version;
import com.revos.android.utilities.vehicleDataTransfer.ControllerWriteCommandHelper;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.vehicleDataTransfer.VehicleControlHelper;
import com.revos.scripts.SetVehicleConfigMutation;
import com.revos.scripts.type.ModelAccessType;
import com.revos.scripts.type.VehicleConfigCreateInput;
import com.revos.scripts.type.VehicleWhereUniqueInput;
import com.suke.widget.SwitchButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import static android.view.View.GONE;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_LOCKED;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_LOCK_HANDLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_UNLOCK_HANDLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_UNLOCK_SEAT;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_DISCONNECTED;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.constants.Constants.VER_1_1;
import static com.revos.android.services.NordicBleService.getDeviceFirmwareVersion;

public class VehicleSettingsActivity extends AppCompatActivity {

    private RadioButton mAntiTheftModeSilentRadioButton, mAntiTheftModeNormalRadioButton, mAntiTheftSensitivityLowRadioButton,
            mAntiTheftSensitivityMediumRadioButton, mAntiTheftSensitivityHighRadioButton,
                        mHeadlampOnFiveSecondsRadioButton, mHeadlampOnTenSecondsRadioButton, mHeadlampOnFifteenSecondsRadioButton,
                        mChildSpeedLockTwentyFiveRadioButton, mChildSpeedLockFiftyRadioButton, mChildSpeedLockSeventyFiveRadioButton;

    private RadioGroup mAntiTheftModeRadioGroup, mAntiTheftSensitivityRadioGroup, mFollowMeHomeHeadlampRadioGroup, mChildSpeedLockRadioGroup;

    private com.suke.widget.SwitchButton mHazardLightsOnSwitch, mAntiTheftSwitch, mFollowMeHomeHeadlampSwitch, mChildSpeedLockSwitch;

    private LinearLayout mVehicleSettingsControlLinearLayout, mAntiTheftModeLinearLayout, mAntiTheftSensitivityLinearLayout, mAntiTheftLinearLayout, mFollowMeHeadlampLinearLayout;

    private boolean mIsVehicleLocked = true;

    private TextView mVehicleSettingsControlInfoTextView;

    private Button mApplyChangesButton;

    /**Progress dialog*/
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.vehicle_settings_activity);

        setupViews();

        updateVehicleSettingViewsVisibility();
    }

    private void setupViews() {
        findViewById(R.id.vehicle_settings_back_button_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mVehicleSettingsControlInfoTextView = findViewById(R.id.vehicle_settings_control_info_text_view);

        mVehicleSettingsControlLinearLayout = findViewById(R.id.vehicle_settings_control_linear_layout);

        mHazardLightsOnSwitch =  findViewById(R.id.vehicle_settings_hazard_lights_switch);

        mAntiTheftSwitch = findViewById(R.id.vehicle_settings_anti_theft_switch);
        mAntiTheftLinearLayout = findViewById(R.id.vehicle_settings_anti_theft_linear_layout);
        mAntiTheftModeLinearLayout = findViewById(R.id.vehicle_settings_anti_theft_mode_linear_layout);
        mAntiTheftModeRadioGroup = findViewById(R.id.vehicle_settings_anti_theft_mode_radio_group);
        mAntiTheftModeNormalRadioButton = findViewById(R.id.vehicle_settings_anti_theft_mode_normal);
        mAntiTheftModeSilentRadioButton = findViewById(R.id.vehicle_settings_anti_theft_mode_silent);
        mAntiTheftSensitivityLowRadioButton = findViewById(R.id.vehicle_settings_anti_theft_sensitivity_low);
        mAntiTheftSensitivityMediumRadioButton = findViewById(R.id.vehicle_settings_anti_theft_sensitivity_medium);
        mAntiTheftSensitivityHighRadioButton = findViewById(R.id.vehicle_settings_anti_theft_sensitivity_high);
        mAntiTheftSensitivityLinearLayout = findViewById(R.id.vehicle_settings_anti_theft_sensitivity_linear_layout);
        mAntiTheftSensitivityRadioGroup = findViewById(R.id.vehicle_settings_anti_theft_sensitivity_radio_group);

        mChildSpeedLockSwitch = findViewById(R.id.child_speed_lock_switch);
        mChildSpeedLockRadioGroup = findViewById(R.id.vehicle_settings_child_speed_lock_radio_group);
        mChildSpeedLockTwentyFiveRadioButton = findViewById(R.id.child_speed_lock_at_twenty_five);
        mChildSpeedLockFiftyRadioButton = findViewById(R.id.child_speed_lock_at_fifty);
        mChildSpeedLockSeventyFiveRadioButton = findViewById(R.id.child_speed_lock_at_seventy_five);

        mFollowMeHeadlampLinearLayout = findViewById(R.id.vehicle_settings_follow_me_home_linear_layout);
        mFollowMeHomeHeadlampSwitch = findViewById(R.id.follow_me_home_headlamp_switch);
        mFollowMeHomeHeadlampRadioGroup = findViewById(R.id.vehicle_settings_follow_me_home_radio_group);
        mHeadlampOnFiveSecondsRadioButton = findViewById(R.id.follow_me_home_five_seconds);
        mHeadlampOnTenSecondsRadioButton = findViewById(R.id.follow_me_home_ten_seconds);
        mHeadlampOnFifteenSecondsRadioButton = findViewById(R.id.follow_me_home_fifteen_seconds);

        mApplyChangesButton = findViewById(R.id.vehicle_settings_apply_changes_button);


        mAntiTheftSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(isChecked) {
                    mAntiTheftLinearLayout.setVisibility(View.VISIBLE);
                    mAntiTheftModeLinearLayout.setVisibility(View.VISIBLE);
                    //if firmware version greater than 1.1
                    String firmwareVersion = getDeviceFirmwareVersion();
                    if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
                        mAntiTheftSensitivityLinearLayout.setVisibility(View.VISIBLE);
                    } else {
                        mAntiTheftSensitivityLinearLayout.setVisibility(GONE);
                    }
                } else {
                    mAntiTheftLinearLayout.setVisibility(GONE);
                }
            }
        });

        mApplyChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressDialog();
                constructAndWriteChildSpeedLockSettingsParameters();

                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        constructAndSetVehicleSettingsParameters();
                        hideProgressDialog();
                        uploadSettings();
                    }
                }, 1000);
            }
        });

        mFollowMeHomeHeadlampSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(view != null && isChecked) {
                    mFollowMeHomeHeadlampRadioGroup.setVisibility(View.VISIBLE);
                } else{
                    mFollowMeHomeHeadlampRadioGroup.setVisibility(GONE); }
            }
        });

        mChildSpeedLockSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(view != null && isChecked) {
                    mChildSpeedLockRadioGroup.setVisibility(View.VISIBLE);
                } else {
                    mChildSpeedLockRadioGroup.setVisibility(GONE);
                }
            }
        });

        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(device == null) {
            return;
        }

        if(device.getVehicle().getModel().getModelAccessType() == ModelAccessType.KEYLESS) {
            findViewById(R.id.vehicle_settings_handle_seat_linear_layout).setVisibility(View.VISIBLE);

            findViewById(R.id.vehicle_settings_unlock_handle_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideHandleControlLayout(5000);
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_UNLOCK_HANDLE));
                }
            });

            findViewById(R.id.vehicle_settings_lock_handle_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideHandleControlLayout(5000);
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_LOCK_HANDLE));
                }
            });

            findViewById(R.id.vehicle_settings_unlock_seat_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_UNLOCK_SEAT));
                }
            });
        }
    }

    private void hideHandleControlLayout(int timeInMilliSeconds) {
        findViewById(R.id.vehicle_settings_handle_control_layout).setVisibility(View.INVISIBLE);
        findViewById(R.id.vehicle_settings_handle_control_lottie_view).setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.vehicle_settings_handle_control_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.vehicle_settings_handle_control_lottie_view).setVisibility(View.INVISIBLE);

            }
        }, timeInMilliSeconds);
    }

    private void updateVehicleSettingViewsVisibility() {

        if(mIsVehicleLocked) {
            mVehicleSettingsControlInfoTextView.setVisibility(View.VISIBLE);
            mVehicleSettingsControlLinearLayout.setAlpha(0.25f);
            mHazardLightsOnSwitch.setEnabled(false);
            mAntiTheftSwitch.setEnabled(false);
            mChildSpeedLockSwitch.setEnabled(false);
            mFollowMeHomeHeadlampSwitch.setEnabled(false);
            mApplyChangesButton.setEnabled(false);
        } else {
            mVehicleSettingsControlInfoTextView.setVisibility(View.INVISIBLE);
            mVehicleSettingsControlLinearLayout.setAlpha(1);
            mHazardLightsOnSwitch.setEnabled(true);
            mAntiTheftSwitch.setEnabled(true);
            mChildSpeedLockSwitch.setEnabled(true);
            mFollowMeHomeHeadlampSwitch.setEnabled(true);
            mApplyChangesButton.setEnabled(true);
        }

        String firmwareVersion = getDeviceFirmwareVersion();
        if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
            mFollowMeHeadlampLinearLayout.setVisibility(View.VISIBLE);
            mAntiTheftSensitivityLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mFollowMeHeadlampLinearLayout.setVisibility(GONE);
            mAntiTheftSensitivityLinearLayout.setVisibility(GONE);
        }
    }

    private void populateVehicleSettingViews() {
        if(NordicBleService.mVehicleLiveParameters == null) {
            return;
        }

        String firmwareVersion = getDeviceFirmwareVersion();

        //populate hazard light view
        if(NordicBleService.mVehicleLiveParameters.isLeftIndicatorOn() && NordicBleService.mVehicleLiveParameters.isRightIndicatorOn()) {
            mHazardLightsOnSwitch.setChecked(true);
        } else {
            mHazardLightsOnSwitch.setChecked(false);
        }

        //populate anti-theft views
        long antiTheftMode = NordicBleService.mVehicleLiveParameters.getAntiTheftMode();
        if(antiTheftMode == 1 || antiTheftMode == -1) {
            mAntiTheftSwitch.setChecked(false);
            mAntiTheftLinearLayout.setVisibility(GONE);
        } else {
            mAntiTheftSwitch.setChecked(true);

            mAntiTheftLinearLayout.setVisibility(View.VISIBLE);

            mAntiTheftModeLinearLayout.setVisibility(View.VISIBLE);
            //select appropriate mode radio button
            if(antiTheftMode == 2) {
                mAntiTheftModeSilentRadioButton.setChecked(true);
            } else if(antiTheftMode == 3) {
                mAntiTheftModeNormalRadioButton.setChecked(true);
            }

            //if firmware version greater than 1.1
            if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
                mAntiTheftSensitivityLinearLayout.setVisibility(View.VISIBLE);
                //select appropriate sensitivity group
                if(NordicBleService.mVehicleLiveParameters.getAntiTheftSensitivity() == 0) {
                    mAntiTheftSensitivityMediumRadioButton.setChecked(true);
                } else if(NordicBleService.mVehicleLiveParameters.getAntiTheftSensitivity() == 1) {
                    mAntiTheftSensitivityLowRadioButton.setChecked(true);
                } else if(NordicBleService.mVehicleLiveParameters.getAntiTheftSensitivity() == 2) {
                    mAntiTheftSensitivityHighRadioButton.setChecked(true);
                }
            } else {
                mAntiTheftSensitivityLinearLayout.setVisibility(GONE);
            }
        }

        //populate child speed lock views
        long speedLimit = NordicBleService.mVehicleLiveParameters.getSpeedCheckByte();
        mChildSpeedLockRadioGroup.setVisibility(View.VISIBLE);
        if(speedLimit == 25) {
            mChildSpeedLockSwitch.setChecked(true);
            mChildSpeedLockTwentyFiveRadioButton.setChecked(true);
        } else if(speedLimit == 50) {
            mChildSpeedLockSwitch.setChecked(true);
            mChildSpeedLockFiftyRadioButton.setChecked(true);
        }  else if(speedLimit == 75) {
            mChildSpeedLockSwitch.setChecked(true);
            mChildSpeedLockSeventyFiveRadioButton.setChecked(true);
        } else {
            mChildSpeedLockSwitch.setChecked(false);
            mChildSpeedLockRadioGroup.setVisibility(GONE);
        }

        //populate follow me headlamp views
        if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
            long headlampOnTime = NordicBleService.mVehicleLiveParameters.getFollowMeHomeHeadlampOnTime();

            mFollowMeHomeHeadlampRadioGroup.setVisibility(View.VISIBLE);
            if(headlampOnTime == 0) {
                mFollowMeHomeHeadlampSwitch.setChecked(false);
                mFollowMeHomeHeadlampRadioGroup.setVisibility(GONE);
            } else if(headlampOnTime == 5) {
                mFollowMeHomeHeadlampSwitch.setChecked(true);
                mHeadlampOnFiveSecondsRadioButton.setChecked(true);
            } else if(headlampOnTime == 10) {
                mFollowMeHomeHeadlampSwitch.setChecked(true);
                mHeadlampOnTenSecondsRadioButton.setChecked(true);
            } else if(headlampOnTime == 15) {
                mFollowMeHomeHeadlampSwitch.setChecked(true);
                mHeadlampOnFifteenSecondsRadioButton.setChecked(true);
            }
        } else {
            mFollowMeHeadlampLinearLayout.setVisibility(GONE);
        }

    }

    private void constructAndSetVehicleSettingsParameters() {
        if(getApplication() == null) {
            return;
        }

        //3 bytes are needed for hazard light and anti theft mode
        int vehicleSettingsByteArraySize = 3;

        String firmwareVersion = getDeviceFirmwareVersion();

        if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
            //we need to include bytes for anti theft sensitivity and followMeHeadlamp
            vehicleSettingsByteArraySize += 2 + 2;
        }

        byte[] vehicleSettingsByteArray = new byte[vehicleSettingsByteArraySize];

        VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getApplicationContext());

        // check hazard light switch
        if(mHazardLightsOnSwitch.isChecked()) {
            vehicleSettingsByteArray[0] = vehicleControlHelper.getLeftIndicatorControlByte(true);
            vehicleSettingsByteArray[1] = vehicleControlHelper.getRightIndicatorControlByte(true);
        } else {
            vehicleSettingsByteArray[0] = vehicleControlHelper.getLeftIndicatorControlByte(false);
            vehicleSettingsByteArray[1] = vehicleControlHelper.getRightIndicatorControlByte(false);
        }

        // check anti theft switch
        if(mAntiTheftSwitch.isChecked()) {
            if(mAntiTheftModeSilentRadioButton.isChecked()) {
                vehicleSettingsByteArray[2] = vehicleControlHelper.getAntiTheftSilentByte();
            } else if(mAntiTheftModeNormalRadioButton.isChecked()) {
                vehicleSettingsByteArray[2] = vehicleControlHelper.getAntiTheftNormalByte();
            }

            //check anti theft sensitivity for firmware version 1.1 and above
            if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
                //1 - low, 0 - medium(default), 2 - high
                byte[] antiTheftSensitivityByteArray = vehicleControlHelper.getAntiTheftSensitivityByteArray(0);
                if(mAntiTheftSensitivityLowRadioButton.isChecked()) {
                    antiTheftSensitivityByteArray = vehicleControlHelper.getAntiTheftSensitivityByteArray(1);
                } else if(mAntiTheftSensitivityHighRadioButton.isChecked()) {
                    antiTheftSensitivityByteArray = vehicleControlHelper.getAntiTheftSensitivityByteArray(2);
                }
                System.arraycopy(antiTheftSensitivityByteArray, 0, vehicleSettingsByteArray, 3, antiTheftSensitivityByteArray.length);
            }

        } else {
            vehicleSettingsByteArray[2] = vehicleControlHelper.getAntiTheftOffByte();
        }

        //check followMeHeadlamp
        if(new Version(firmwareVersion).compareTo(new Version(VER_1_1)) > 0) {
            byte[] followMeHeadlampControlByteArray = vehicleControlHelper.getFollowMeHeadlampControlByteArray(0);
            if(mFollowMeHomeHeadlampSwitch.isChecked()) {
                followMeHeadlampControlByteArray = vehicleControlHelper.getFollowMeHeadlampControlByteArray(5);
                if(mHeadlampOnTenSecondsRadioButton.isChecked()) {
                    followMeHeadlampControlByteArray = vehicleControlHelper.getFollowMeHeadlampControlByteArray(10);
                } else if(mHeadlampOnFifteenSecondsRadioButton.isChecked()) {
                    followMeHeadlampControlByteArray = vehicleControlHelper.getFollowMeHeadlampControlByteArray(15);
                }
            }
            System.arraycopy(followMeHeadlampControlByteArray, 0, vehicleSettingsByteArray, 5, followMeHeadlampControlByteArray.length);
        }

        String constructedVehicleSettingsString = "";

        for (byte currentByte : vehicleSettingsByteArray) {
            constructedVehicleSettingsString += BleUtils.singleByteToHexString(currentByte) + " ";
        }
        constructedVehicleSettingsString = constructedVehicleSettingsString.trim();

        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS + GEN_DELIMITER + constructedVehicleSettingsString));

    }

    private void constructAndWriteChildSpeedLockSettingsParameters() {
        if(NordicBleService.mVehicleLiveParameters == null) {
            return;
        }

        int speedLimitPercentage = 100;
        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

        if(mChildSpeedLockSwitch.isChecked()) {
            speedLimitPercentage = 25;
            if(mChildSpeedLockFiftyRadioButton.isChecked()) {
                speedLimitPercentage = 50;
            } else if(mChildSpeedLockSeventyFiveRadioButton.isChecked()) {
                speedLimitPercentage = 75;
            }
        } else {
            if(device != null && device.getVehicle() != null && device.getVehicle().getModel() != null) {
                speedLimitPercentage = device.getVehicle().getModel().getSpeedLimit();
            }
        }

        ControllerWriteCommandHelper controllerWriteCommandHelper = new ControllerWriteCommandHelper(NordicBleService.mVehicleLiveParameters, VehicleSettingsActivity.this);
        controllerWriteCommandHelper.setSpeedPercent(speedLimitPercentage);
        byte[] commandArray = controllerWriteCommandHelper.constructWriteCommandByteArray();
        String constructedChildSpeedLockString = "";

        for (byte currentByte : commandArray) {
            constructedChildSpeedLockString += BleUtils.singleByteToHexString(currentByte) + " ";
        }
        constructedChildSpeedLockString = constructedChildSpeedLockString.trim();

        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS + GEN_DELIMITER + constructedChildSpeedLockString));
    }

    private void uploadSettings() {

        String vin = null;

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(VehicleSettingsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            VehicleSettingsActivity.this.startActivity(intent);

            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle != null) {
            vin = vehicle.getVin();
        }

        if(vin == null || vin.isEmpty()) {
            Intent intent = new Intent(VehicleSettingsActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vin_not_found_try_again));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            VehicleSettingsActivity.this.startActivity(intent);

            return;
        }

        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        int speedValue = 100;
        if(mChildSpeedLockSwitch.isChecked()) {
            speedValue = 25;
            if(mChildSpeedLockFiftyRadioButton.isChecked()) {
                speedValue = 50;
            } else if(mChildSpeedLockSeventyFiveRadioButton.isChecked()) {
                speedValue = 75;
            }
        } else {
            if(device != null && device.getVehicle() != null && device.getVehicle().getModel() != null) {
                speedValue = device.getVehicle().getModel().getSpeedLimit();
            }
        }


        int followMeHeadlampTime = 0;
        if(mFollowMeHomeHeadlampSwitch.isChecked()) {
            followMeHeadlampTime = 5;
            if(mHeadlampOnTenSecondsRadioButton.isChecked()) {
                followMeHeadlampTime = 10;
            } else if(mHeadlampOnFifteenSecondsRadioButton.isChecked()) {
                followMeHeadlampTime = 15;
            }
        }

        showProgressDialog();

        VehicleConfigCreateInput vehicleConfigCreateInput = VehicleConfigCreateInput
                                                                .builder()
                                                                .childSpeedLock(speedValue)
                                                                .followMeHomeHeadLamp(followMeHeadlampTime)
                                                                .build();

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vin).build();

        SetVehicleConfigMutation setVehicleConfigMutation = SetVehicleConfigMutation.builder()
                                                                                    .data(vehicleConfigCreateInput)
                                                                                    .where(vehicleWhereUniqueInput)
                                                                                    .build();

        apolloClient.mutate(setVehicleConfigMutation).enqueue(new ApolloCall.Callback<SetVehicleConfigMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SetVehicleConfigMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response != null) {
                            if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                                //TODO: show appropriate error message here
                                Intent intent = new Intent(VehicleSettingsActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                VehicleSettingsActivity.this.startActivity(intent);
                                return;
                            }

                            if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().setConfig() != null) {

                                SetVehicleConfigMutation.SetConfig vehicleConfigMutationResponse = response.getData().vehicles().setConfig();
                                //saving device details in pref utils
                                Device device = PrefUtils.getInstance(getApplicationContext()).saveDeviceVariablesInPrefs(vehicleConfigMutationResponse);

                                Intent intent = new Intent(VehicleSettingsActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.settings_applied));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                VehicleSettingsActivity.this.startActivity(intent);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Intent intent = new Intent(VehicleSettingsActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        VehicleSettingsActivity.this.startActivity(intent);
                    }
                });
            }
        });
    }

    private boolean checkIfCurrentUserIsOwner() {

        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        User loggedInUserDetails = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        String deviceOwnerId = null;
        String currentUserId = null;

        if(cloudDevice != null && cloudDevice.getVehicle().getOwnerId() != null) {
            deviceOwnerId = cloudDevice.getVehicle().getOwnerId();
        }

        if(loggedInUserDetails != null && loggedInUserDetails.getId() != null) {
            currentUserId = loggedInUserDetails.getId();
        }

        if (currentUserId != null && currentUserId.equals(deviceOwnerId)) {
            return true;
        }

        return false;
    }


    @Override
    public void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onEventBusMessage(final EventBusMessage event) {
        if (event.message.contains(OBD_EVENT_LIVE_DATA)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final String dataString = event.message.split(GEN_DELIMITER)[1];
                    if(mIsVehicleLocked) {
                        mIsVehicleLocked = false;
                        //populate vehicle setting views, only when state goes from locked to unlocked
                        updateVehicleSettingViewsVisibility();
                        populateVehicleSettingViews();
                    }

                    mIsVehicleLocked = false;
                }
            });
        } else if(event.message.contains(BT_EVENT_DEVICE_LOCKED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!mIsVehicleLocked) {
                        mIsVehicleLocked = true;
                        //update vehicle setting views, only when state goes from unlocked to locked
                        updateVehicleSettingViewsVisibility();
                    }

                    mIsVehicleLocked = true;
                }
            });
        } else if(event.message.equals(BT_EVENT_STATE_DISCONNECTED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(VehicleSettingsActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}