package com.revos.android.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.graphQL.GraphQLDateTimeCustomAdapter;
import com.revos.android.graphQL.GraphQLInterceptor;
import com.revos.android.jsonStructures.Device;
import com.revos.android.services.DfuService;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.DownloadOTAQuery;
import com.revos.scripts.MarkOTAStatusMutation;
import com.revos.scripts.type.CustomType;
import com.revos.scripts.type.DeviceUpdateStatus;
import com.revos.scripts.type.DeviceWhereUniqueInput;
import com.revos.scripts.type.InputOtaStatus;
import com.revos.scripts.type.OtaInput;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import at.grabner.circleprogress.CircleProgressView;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import okhttp3.OkHttpClient;
import timber.log.Timber;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;
import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE;
import static com.revos.android.constants.Constants.CONTACT_IN_ACTION_KEY;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.GENERAL_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.GEN_EVENT_CHANGE_HAMBURGER_TO_BACK;
import static com.revos.android.constants.Constants.GEN_EVENT_NAV_BANNER_ANIM_UPDATED;
import static com.revos.android.constants.Constants.NAV_EVENT_TRIP_ADVICE_DATA_UPDATED;
import static com.revos.android.constants.Constants.OTA_DIR_PATH;
import static com.revos.android.constants.Constants.PHONE_EVENT_CONTACT_LIST_BUILDING;
import static com.revos.android.constants.Constants.PHONE_EVENT_CONTACT_LIST_BUILT;
import static com.revos.android.constants.Constants.START_STM_EXTERNAL_OTA;
import static com.revos.android.constants.Constants.START_STM_INTERNAL_OTA;
import static com.revos.android.constants.Constants.STM_OTA_STATUS_MESSAGE;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_TOKEN_KEY;

public class StmOtaActivity extends AppCompatActivity {

    private Button mButton;
    private LottieAnimationView mLottieAnimationView;
    private CircleProgressView mCircleProgressView;
    private TextView mStatusTextView;

    private String mOtaVersion;
    private String mDeviceId;

    private File mOtaFile;


    /** Flag set to true in {@link #onRestart()} and to false in {@link #onPause()}. */
    private boolean mResumed;
    /** Flag set to true if DFU operation was completed while {@link #mResumed} was false. */
    private boolean mDfuCompleted;
    /** The error message received from DFU service while {@link #mResumed} was false. */
    private String mDfuError;

    public static String OTA_VERSION_KEY = "ota_version_key";
    public static String OTA_DEVICE_ID_KEY = "ota_device_id_key";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.stm_ota_activity);

        findViewById(R.id.stm_ota_internal_ota_button).setOnClickListener(v -> {
            EventBus.getDefault().post(new EventBusMessage(START_STM_INTERNAL_OTA));
        });

        findViewById(R.id.stm_ota_external_ota_button).setOnClickListener(v -> {
            EventBus.getDefault().post(new EventBusMessage(START_STM_EXTERNAL_OTA));
        });

        mStatusTextView = findViewById(R.id.stm_ota_text_view);
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if (event.message.contains(STM_OTA_STATUS_MESSAGE)) {
            String message = event.message.split(GEN_DELIMITER)[1];
            mStatusTextView.append("\n" + message);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
