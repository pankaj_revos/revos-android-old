package com.revos.android.activities;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.revos.android.R;

import java.util.Objects;

import static com.revos.android.constants.Constants.GENERIC_TEMP_IMAGE_KEY;
import static com.revos.android.constants.Constants.TEMP_IMAGE_PREFS;

/**
 * Created by priya on 27/11/19.
 */

public class SimpleImageViewActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.simple_image_view_using_glide);

        String uriStr = getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).getString(GENERIC_TEMP_IMAGE_KEY, null);

        if(uriStr == null) {
            finish();
            return;
        }

        Uri imageUri = Uri.parse(uriStr);

        Glide.with(getApplicationContext()).load(imageUri).into((ImageView) findViewById(R.id.simple_image_view));
    }
}