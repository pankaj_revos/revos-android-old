package com.revos.android.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.revos.android.R;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import timber.log.Timber;

/**
 * Created by mohit on 29/6/18.
 */

public class ZXingActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    public static String ZXING_RESULT_KEY = "zxing_result_key";

    private ZXingScannerView mScannerView;

    private String TAG = "MainActivity";


    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view

        ArrayList<BarcodeFormat> barcodeFormats = new ArrayList<>();
        barcodeFormats.add(BarcodeFormat.AZTEC);
        barcodeFormats.add(BarcodeFormat.CODABAR);
        barcodeFormats.add(BarcodeFormat.CODE_39);
        barcodeFormats.add(BarcodeFormat.CODE_93);
        barcodeFormats.add(BarcodeFormat.CODE_128);
        barcodeFormats.add(BarcodeFormat.DATA_MATRIX);
        barcodeFormats.add(BarcodeFormat.EAN_8);
        barcodeFormats.add(BarcodeFormat.EAN_13);
        barcodeFormats.add(BarcodeFormat.ITF);
        barcodeFormats.add(BarcodeFormat.MAXICODE);
        barcodeFormats.add(BarcodeFormat.PDF_417);
        barcodeFormats.add(BarcodeFormat.QR_CODE);
        barcodeFormats.add(BarcodeFormat.RSS_14);
        barcodeFormats.add(BarcodeFormat.RSS_EXPANDED);
        barcodeFormats.add(BarcodeFormat.UPC_A);
        barcodeFormats.add(BarcodeFormat.UPC_E);
        barcodeFormats.add(BarcodeFormat.UPC_EAN_EXTENSION);
        
        // this paramter will make your HUAWEI phone works great!
        mScannerView.setAspectTolerance(0.5f);
        mScannerView.setFormats(barcodeFormats);
        mScannerView.setAutoFocus(true);
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Timber.v(rawResult.getText()); // Prints scan results
        Timber.v(rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        /*// If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);*/

        Toast.makeText(getApplicationContext(), getString(R.string.data_read_successfully), Toast.LENGTH_LONG).show();


        Intent resultIntent = new Intent();
        resultIntent.putExtra(ZXING_RESULT_KEY, rawResult.getText());

        setResult(RESULT_OK, resultIntent);
        finish();
    }

}
