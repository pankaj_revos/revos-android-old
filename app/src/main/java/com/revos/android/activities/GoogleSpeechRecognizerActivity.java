package com.revos.android.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import com.revos.android.R;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import timber.log.Timber;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.SPEECH_RECOGNITION_RESULT;


public class GoogleSpeechRecognizerActivity extends AppCompatActivity implements RecognitionListener {

    private static final int REQUEST_RECORD_PERMISSION = 100;
    private int ACTIVITY_MAX_RUN_DURATION = 5000;
    private TextView mSpeechResultTextView;
    private PulsatorLayout mPulsatorLayout;
    private SpeechRecognizer mSpeechRecognizer = null;
    private Intent recognizerIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_speech_recognizer_activity);

        setupViews();

        setupVoiceRecognition();

        startVoiceRecognition();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finish();
            }
        }, ACTIVITY_MAX_RUN_DURATION);

    }

    private void setupViews() {

        mSpeechResultTextView = (TextView) findViewById(R.id.google_speech_recognizer_result_text_view);
        mPulsatorLayout = (PulsatorLayout)findViewById(R.id.google_speech_recognizer_pulsator);
        mPulsatorLayout.start();
    }

    private void setupVoiceRecognition() {

        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        Timber.i("isRecognitionAvailable: %s", SpeechRecognizer.isRecognitionAvailable(this));
        mSpeechRecognizer.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_recognizer_prompt_heading));
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
    }

    private void startVoiceRecognition() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ActivityCompat.requestPermissions
                        (GoogleSpeechRecognizerActivity.this,
                                new String[]{Manifest.permission.RECORD_AUDIO},
                                REQUEST_RECORD_PERMISSION);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Timber.i("startListening");
                    mSpeechRecognizer.startListening(recognizerIntent);
                } else {

                    //TODO :: Request permission
                    Intent intent = new Intent(GoogleSpeechRecognizerActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.permission_denied));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    GoogleSpeechRecognizerActivity.this.startActivity(intent);

                }
        }
    }

    @Override
    public void onBeginningOfSpeech() {
        Timber.i("onBeginningOfSpeech");
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Timber.i("onBufferReceived: %s", buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Timber.i("onEndOfSpeech");
        mPulsatorLayout.stop();
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Timber.d("FAILED %s", errorMessage);
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Timber.i("onEvent");
    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Timber.i("onPartialResults");

        ArrayList data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String word = null;
        if (data != null) {
            word = (String) data.get(data.size() - 1);
        }
        mSpeechResultTextView.setText(word);
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Timber.i("onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Timber.i("onResults");
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        if(matches == null) {
            Intent resultIntent = new Intent();
            setResult(RESULT_CANCELED, resultIntent);
            finish();

        } else if(matches != null) {

            mSpeechResultTextView.setText(matches.get(0));
            Intent resultIntent = new Intent();
            resultIntent.putExtra(SPEECH_RECOGNITION_RESULT, matches);
            setResult(RESULT_OK, resultIntent);
            finish();
        }
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Timber.i("onRmsChanged: %s", rmsdB);
        //progressBar.setProgress((int) rmsdB);
    }

    private String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPulsatorLayout.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPulsatorLayout.stop();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.destroy();
            Timber.i("destroy");
        }
    }

}