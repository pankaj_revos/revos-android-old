package com.revos.android.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revos.android.R;
import com.revos.android.jsonStructures.Contact;
import com.revos.android.jsonStructures.GenericPhoneEntry;
import com.revos.android.utilities.adapters.PickContactToCallAdapter;

import java.util.ArrayList;

import static com.revos.android.constants.Constants.ENTRY_TYPE_CONTACT_SEARCH_RESULT;
import static com.revos.android.constants.Constants.PHONE_EVENT_MULTIPLE_MATCHED_RESULTS;


public class PickContactToCallActivity extends AppCompatActivity {

    private RecyclerView mPickContactRecyclerView;
    private TextView mPickContactTimerTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        String contactResultsString = null;
        if (bundle != null) {
            contactResultsString = bundle.getString(PHONE_EVENT_MULTIPLE_MATCHED_RESULTS);
        }

        ArrayList<Contact> contactResultsList = new Gson().fromJson(contactResultsString, new TypeToken<ArrayList<Contact>>(){}.getType());

        setContentView(R.layout.pick_contact_to_call_activity);

        setupViews();

        if (contactResultsList != null) {
            showSearchResults(contactResultsList);
        }

    }

    private void setupViews() {

        mPickContactRecyclerView = findViewById(R.id.pick_contact_to_call_recycler_view);
        mPickContactTimerTextView = findViewById(R.id.pick_contact_countdown_timer_text_view);

    }

    private void showSearchResults(ArrayList<Contact> contactList) {

        ArrayList<GenericPhoneEntry> genericList = new ArrayList<>();
        //convert this list to a generic list for the adapter to consume
        for(int entryIndex = 0; entryIndex < contactList.size(); ++entryIndex) {
            Contact currentEntry = contactList.get(entryIndex);

            for(int contactNumberIndex = 0; contactNumberIndex < currentEntry.getPhoneNumbers().size(); ++contactNumberIndex) {
                GenericPhoneEntry genericPhoneEntry = new GenericPhoneEntry();
                genericPhoneEntry.setName(currentEntry.getName());
                genericPhoneEntry.setPhoneNumber(currentEntry.getPhoneNumbers().get(contactNumberIndex));
                genericPhoneEntry.setEntryType(ENTRY_TYPE_CONTACT_SEARCH_RESULT);
                genericList.add(genericPhoneEntry);
            }
        }

        PickContactToCallAdapter pickContactToCallAdapter = new PickContactToCallAdapter(genericList, PickContactToCallActivity.this);
        mPickContactRecyclerView.setLayoutManager(new LinearLayoutManager(PickContactToCallActivity.this));

        mPickContactRecyclerView.setAdapter(pickContactToCallAdapter);
        startTimer();

    }

    private void startTimer() {

        new CountDownTimer(11000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mPickContactTimerTextView.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                finish();
            }
        }.start();
    }
}