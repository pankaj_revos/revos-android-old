package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.FetchVehicleQuery;
import com.revos.scripts.type.DeviceStatus;
import com.revos.scripts.type.DeviceType;
import com.revos.scripts.type.ModelProtocol;
import com.revos.scripts.type.VehicleStatus;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import static com.revos.android.constants.Constants.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_ICONCOX;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TBIT;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_DEVICE;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class ConnectUsingVinActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private EditText mEnterVinEditTextView;

    public static String CONNECT_USING_VIN_KEY = "connect_using_vin_key";
    private String mConnectUsingVinVehicleDetailsVin = null;

    public static String CONNECT_TO_RENTAL_VEHICLE_VIN_KEY = "connect_to_rental_vehicle_vin_key";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(bundle.containsKey(CONNECT_USING_VIN_KEY)) {
                mConnectUsingVinVehicleDetailsVin = bundle.getString(CONNECT_USING_VIN_KEY);
            } else if(bundle.containsKey(CONNECT_TO_RENTAL_VEHICLE_VIN_KEY)) {
                mConnectUsingVinVehicleDetailsVin = bundle.getString(CONNECT_TO_RENTAL_VEHICLE_VIN_KEY);
                if(mConnectUsingVinVehicleDetailsVin != null && !mConnectUsingVinVehicleDetailsVin.isEmpty()) {
                    connectUsingVin(mConnectUsingVinVehicleDetailsVin);
                }
            }
        }

        setContentView(R.layout.connect_using_vin_activity);

        setupViews();
    }

    private void setupViews() {

        Button mConnectUsingVinConnectButton = findViewById(R.id.cuv_connect_button);
        mEnterVinEditTextView = findViewById(R.id.cuv_enter_vin_edit_text);

        if(mConnectUsingVinVehicleDetailsVin != null && !mConnectUsingVinVehicleDetailsVin.isEmpty()) {
            mEnterVinEditTextView.setText(mConnectUsingVinVehicleDetailsVin);
        }

        mConnectUsingVinConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String vin = mEnterVinEditTextView.getText().toString().trim();
                if(!vin.isEmpty()) {
                   connectUsingVin(vin);
                } else {
                    showGenericMessage(true, getString(R.string.please_enter_a_vin), GenericMessageActivity.STATUS_ERROR);
                }
            }
        });
    }

    private void clearVehicleAndDeviceRelatedPrefs() {
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();

        getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

    }


    private void connectUsingVin(String vin) {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            showGenericMessage(false, getString(R.string.error), GenericMessageActivity.STATUS_ERROR);
            return;
        }

        showProgressDialog();

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput
                                                            .builder()
                                                            .vin(vin)
                                                            .build();

        FetchVehicleQuery fetchVehicleQuery = FetchVehicleQuery
                                                .builder()
                                                .vehicleWhereInputVar(vehicleWhereUniqueInput)
                                                .build();

        apolloClient.query(fetchVehicleQuery).enqueue(new ApolloCall.Callback<FetchVehicleQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchVehicleQuery.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            showGenericMessage(true, getString(R.string.incorrect_vin), GenericMessageActivity.STATUS_ERROR);

                        } else {

                            if(response.getData() != null && response.getData().vehicles() != null &&
                                    response.getData().vehicles().get() != null && response.getData().vehicles().get().device() != null) {

                                Device device = PrefUtils.getInstance(getApplicationContext()).saveDeviceVariablesInPrefs(response.getData().vehicles().get());

                                if(device == null) {
                                    return;
                                }

                                if(device.getStatus() == DeviceStatus.INITIALIZED && device.getVehicle() != null && device.getVehicle().getStatus() != VehicleStatus.SOLD) {

                                    showGenericMessage(false, getString(R.string.vehicle_not_marked_sold), GenericMessageActivity.STATUS_ERROR);

                                    clearVehicleAndDeviceRelatedPrefs();
                                    return;
                                }

                                if(device.getStatus() == DeviceStatus.ACTIVATED && device.getVehicle() != null && device.getVehicle().getStatus() != VehicleStatus.SOLD) {

                                    if(device.getVehicle().getStatus().equals(VehicleStatus.RENTAL)) {

                                        String activeBookingPrefString = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);

                                        if(activeBookingPrefString == null) {

                                            showGenericMessage(true, getString(R.string.vehicle_marked_for_rental_go_to_rental), GenericMessageActivity.STATUS_INFO);

                                            clearVehicleAndDeviceRelatedPrefs();
                                            return;
                                        }

                                        RentalVehicle rentalVehicle = new Gson().fromJson(activeBookingPrefString, RentalVehicle.class);

                                        if(rentalVehicle != null && rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {

                                            if(!rentalVehicle.getVin().equals(device.getVehicle().getVin())) {

                                                showGenericMessage(true, getString(R.string.vehicle_marked_for_rental_go_to_rental), GenericMessageActivity.STATUS_INFO);

                                                clearVehicleAndDeviceRelatedPrefs();
                                                return;
                                            }
                                        } else {
                                            clearVehicleAndDeviceRelatedPrefs();
                                            //invalid data return;
                                            return;
                                        }
                                    } else {

                                        showGenericMessage(false, getString(R.string.vehicle_not_marked_sold), GenericMessageActivity.STATUS_INFO);
                                    }
                                }

                                //clear previous PIN if any
                                getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, null).apply();

                                //save device pin if it's not null
                                if(response.getData().vehicles().get().device().pin() != null) {
                                    getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, response.getData().vehicles().get().device().pin()).apply();
                                } else {
                                    getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, null).apply();
                                }

                                NordicBleService.setDeviceName(null);

                                MetadataDecryptionHelper.getInstance(getApplicationContext()).clearLocalCache();

                                //clear previous metadata
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply();


                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

                                //clear previous tracking data
                                getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

                                //refresh live log and geo fence data
                                NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLiveLogData();
                                NetworkUtils.getInstance(getApplicationContext()).refreshGeoFenceData();

                                if(device.getVehicle() != null && device.getVehicle().getModel() != null && device.getVehicle().getModel().getProtocol() != null
                                        && device.getVehicle().getModel().getProtocol() == ModelProtocol.PNP) {

                                    //send request to connect to tbit, if device is tbit
                                    if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {
                                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT));
                                    } else if(device != null && device.getDeviceType() != null  && device.getDeviceType().equals(DeviceType.ICONCOX)) {
                                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX));
                                    }

                                } else {
                                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.getMacId()));
                                }

                                finish();
                            } else {
                                showGenericMessage(true, getString(R.string.vin_not_found_try_again), GenericMessageActivity.STATUS_ERROR);

                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        hideProgressDialog();

                        showGenericMessage(false, getString(R.string.server_error), GenericMessageActivity.STATUS_ERROR);
                    }
                });
            }
        });
    }

    private void showGenericMessage(boolean makeMsgPersistent, String message, int messageType) {

        Intent intent = new Intent(ConnectUsingVinActivity.this, GenericMessageActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, messageType);

        if(makeMsgPersistent) {
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));

        } else {
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        }

        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(ConnectUsingVinActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
