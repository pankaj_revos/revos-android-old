package com.revos.android.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.hbb20.CountryCodePicker;
import com.revos.android.BuildConfig;
import com.revos.android.R;
import com.revos.android.graphQL.GraphQLDateTimeCustomAdapter;
import com.revos.scripts.RegisterGuestMutation;
import com.revos.scripts.type.CustomType;
import com.revos.scripts.type.GenderType;
import com.revos.scripts.type.InputRegisterGuest;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.core.app.ActivityCompat;
import okhttp3.OkHttpClient;

import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.IS_APP_IN_GUEST_MODE;
import static com.revos.android.constants.Constants.permissionsRequired;

public class GuestDetailActivity extends AppCompatActivity {
    private int REQUEST_PERMISSION_REQ_CODE = 200;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private String mDOB;

    private TextView mGuestDetailsDobTextView;

    private EditText mGuestDetailsFirstNameEditText, mGuestDetailsLastNameEditText, mGuestDetailsEmailEditText, mGuestDetailsPhoneNumberEditText;

    private RadioGroup mUserGenderRadioGroup;
    private RadioButton mUserRadioButtonMale, mUserRadioButtonFemale, mUserRadioButtonOther;

    private CheckBox mUserDetailEulaCheckBox;

    private Button mGuestDetailsSubmitButton;

    private CountryCodePicker mPhoneNumCcp;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guest_detail_activity);

        setupViews();
    }

    private void setupViews() {

        mGuestDetailsFirstNameEditText = (EditText) findViewById(R.id.guest_detail_first_name_edit_text);
        mGuestDetailsLastNameEditText = (EditText) findViewById(R.id.guest_detail_last_name_edit_text);
        mGuestDetailsEmailEditText = (EditText) findViewById(R.id.guest_detail_email_edit_text);
        mGuestDetailsDobTextView = (TextView) findViewById(R.id.guest_detail_dob_text_view);
        mUserGenderRadioGroup = (RadioGroup) findViewById(R.id.guest_detail_gender_radio_group);
        mUserRadioButtonMale = (RadioButton) findViewById(R.id.guest_detail_radio_button_male);
        mUserRadioButtonFemale = (RadioButton) findViewById(R.id.guest_detail_radio_button_female);
        mUserRadioButtonOther = (RadioButton) findViewById(R.id.guest_detail_radio_button_other);
        mGuestDetailsSubmitButton = (Button) findViewById(R.id.guest_detail_submit_button);
        mUserDetailEulaCheckBox = (CheckBox) findViewById(R.id.guest_detail_eula_check_box);

        //phone number edit text view
        mGuestDetailsPhoneNumberEditText = (EditText) findViewById(R.id.guest_detail_phone_number_edit_text);
        mPhoneNumCcp = (CountryCodePicker)findViewById(R.id.guest_detail_country_code_picker);
        mPhoneNumCcp.registerCarrierNumberEditText(mGuestDetailsPhoneNumberEditText);


        String generatedText = String.format(getResources().getString(R.string.eula_message_dynamic), getString(R.string.terms_and_condition_link));

        ((TextView)findViewById(R.id.guest_detail_eula_text_view)).setMovementMethod(LinkMovementMethod.getInstance());
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            ((TextView)findViewById(R.id.guest_detail_eula_text_view)).setText(Html.fromHtml(generatedText));
        } else {
            ((TextView)findViewById(R.id.guest_detail_eula_text_view)).setText(Html.fromHtml(generatedText, Html.FROM_HTML_MODE_COMPACT));
        }


        mUserDetailEulaCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked) {
                    mGuestDetailsSubmitButton.setVisibility(View.VISIBLE);
                } else {
                    mGuestDetailsSubmitButton.setVisibility(View.INVISIBLE);
                }
            }
        });

        //setup dob text view
        mGuestDetailsDobTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SpinnerDatePickerDialogBuilder()
                        .context(GuestDetailActivity.this)
                        .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);

                                Date date = calendar.getTime();
                                String dateFormat = "dd/MMM/yyyy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                                mGuestDetailsDobTextView.setText(sdf.format(calendar.getTime()));
                                mDOB = new DateTime(calendar.getTime()).toString();
                            }
                        })
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        .build()
                        .show();
            }
        });

        //setup submit button
        mGuestDetailsSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGuestRegistrationSequence();
            }
        });
    }

    private void startGuestRegistrationSequence() {
        if (ActivityCompat.checkSelfPermission(GuestDetailActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(GuestDetailActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
            return;
        }

        LocationServices.getFusedLocationProviderClient(getApplicationContext()).getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                registerGuest(location);
            }
        });
    }

    private void registerGuest(Location location) {
        showProgressDialog();

        String firstName = mGuestDetailsFirstNameEditText.getText().toString().trim();
        String lastName = mGuestDetailsLastNameEditText.getText().toString().trim();
        String email = mGuestDetailsEmailEditText.getText().toString().trim();
        String dob = String.valueOf(mDOB);
        int checkedRadioButtonId = mUserGenderRadioGroup.getCheckedRadioButtonId();
        GenderType genderType = GenderType.OTHERS;
        if(checkedRadioButtonId == R.id.guest_detail_radio_button_male) {
            genderType = GenderType.MALE;
        } else if(checkedRadioButtonId == R.id.guest_detail_radio_button_female) {
            genderType = GenderType.FEMALE;
        } else {
            genderType = GenderType.OTHERS;
        }

        if(firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || dob.isEmpty() || dob == null || dob.equals("null") || mGuestDetailsPhoneNumberEditText.getText().toString().trim().isEmpty()) {

            hideProgressDialog();

            Intent intent = new Intent(GuestDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_fill_all_the_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            GuestDetailActivity.this.startActivity(intent);

            return;
        }

        //calculate user age
        DateTime currentDateTime = new DateTime();
        DateTime dobDateTime = new DateTime(dob);

        int ageInYears = new Period(dobDateTime, currentDateTime).getYears();
        if(ageInYears >= 16){
            mDOB = dob;
        } else {
            hideProgressDialog();

            Intent intent = new Intent(GuestDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.user_must_be_16_years_or_above));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(!mPhoneNumCcp.isValidFullNumber()) {
            hideProgressDialog();

            Intent intent = new Intent(GuestDetailActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_valid_phone_number));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        makeRegisterUserCall(location);
    }

    private void makeRegisterUserCall(Location location) {

        showProgressDialog();

        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .build();

        ApolloClient apolloClient = ApolloClient.builder()
                .serverUrl(getString(R.string.server_link))
                .okHttpClient(okHttpClient)
                .addCustomTypeAdapter(CustomType.DATETIME, new GraphQLDateTimeCustomAdapter())
                .build();


        String firstName = mGuestDetailsFirstNameEditText.getText().toString().trim();
        String lastName = mGuestDetailsLastNameEditText.getText().toString().trim();
        String email = mGuestDetailsEmailEditText.getText().toString().trim();
        String dob = String.valueOf(mDOB);
        int checkedRadioButtonId = mUserGenderRadioGroup.getCheckedRadioButtonId();
        GenderType genderType = GenderType.OTHERS;
        if(checkedRadioButtonId == R.id.guest_detail_radio_button_male) {
            genderType = GenderType.MALE;
        } else if(checkedRadioButtonId == R.id.guest_detail_radio_button_female) {
            genderType = GenderType.FEMALE;
        } else {
            genderType = GenderType.OTHERS;
        }
        String phone = mPhoneNumCcp.getFullNumberWithPlus();

        double lat = 0;
        double lng = 0;
        if(location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        }

        InputRegisterGuest inputRegisterGuest = InputRegisterGuest
                .builder()
                .application(BuildConfig.APPLICATION_ID)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .phone(phone)
                .dob(dob)
                .gender(genderType)
                .latitude(lat)
                .longitude(lng)
                .build();

        RegisterGuestMutation registerGuestMutation = RegisterGuestMutation.builder().data(inputRegisterGuest).build();

        apolloClient.mutate(registerGuestMutation).enqueue(new ApolloCall.Callback<RegisterGuestMutation.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<RegisterGuestMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            hideProgressDialog();

                            String message = getString(R.string.unable_to_set_user_details);
                            if(response.getErrors().get(0) != null) {
                                BigDecimal statusCode = (BigDecimal)response.getErrors().get(0).getCustomAttributes().get("statusCode");
                                if(statusCode.intValue() == 409) {
                                    message = response.getErrors().get(0).getMessage();
                                }
                            }

                            Intent intent = new Intent(GuestDetailActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            GuestDetailActivity.this.startActivity(intent);

                        } else {
                            hideProgressDialog();
                            launchMainActivity();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        //todo::handle server error scenarios at a granular level

                        Intent intent = new Intent(GuestDetailActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        GuestDetailActivity.this.startActivity(intent);

                    }
                });
            }
        });
    }

    private void launchMainActivity() {
        hideProgressDialog();
        getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(IS_APP_IN_GUEST_MODE, true).apply();
        Intent myIntent = new Intent(GuestDetailActivity.this, MainActivity.class);
        GuestDetailActivity.this.startActivity(myIntent);
        finish();
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(GuestDetailActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            boolean allGranted = false;
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if(allGranted){
                startGuestRegistrationSequence();
            } else if(ActivityCompat.checkSelfPermission(GuestDetailActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {

                if(ActivityCompat.shouldShowRequestPermissionRationale(GuestDetailActivity.this, mPermissionsRequired[0])) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(GuestDetailActivity.this);
                    builder.setTitle(R.string.need_following_permissions);

                    String fineLocationPermissionRequiredMessage = getString(R.string.access_to_device_location_permission)
                            + "\n" + getString(R.string.access_fine_location_permission_description);
                    builder.setMessage(fineLocationPermissionRequiredMessage);


                    builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(GuestDetailActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
            } else {

                Intent intent = new Intent(GuestDetailActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_get_permission));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                intent.putExtras(bundle);
                GuestDetailActivity.this.startActivity(intent);

            }
        }
    }
}
