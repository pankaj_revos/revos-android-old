package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.fragments.EmailLoginFragment;
import com.revos.android.fragments.LoginMainFragment;
import com.revos.android.fragments.PhoneAuthFragment;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.User;
import com.revos.android.graphQL.GraphQLDateTimeCustomAdapter;
import com.revos.android.retrofit.FirebaseClient;
import com.revos.android.retrofit.FirebaseInterface;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.LoginWithFirebaseMutation;
import com.revos.scripts.UserUploadsQuery;
import com.revos.scripts.type.CustomType;
import com.revos.scripts.type.FileType;
import com.revos.scripts.type.FileWhereInput;
import com.revos.scripts.type.GenderType;
import com.revos.scripts.type.Role;
import com.revos.scripts.type.StatusType;
import com.revos.scripts.type.UserWhereInput;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.IS_APP_IN_GUEST_MODE;
import static com.revos.android.constants.Constants.IS_USER_PHONE_NUMBER_VERIFIED;
import static com.revos.android.constants.Constants.KILL_SWITCH;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_EMAIL;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_FB;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_GOOGLE;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_GUEST;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_PHONE;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_PROFILE_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_TOKEN_KEY;
import static com.revos.android.constants.Constants.USER_VERIFIED_PHONE_NUMBER;
import static com.revos.android.constants.Constants.permissionsRequired;
import static com.revos.android.fragments.PhoneAuthFragment.LOGIN_SCREEN_COUNTRY_CODE;
import static com.revos.android.fragments.PhoneAuthFragment.LOGIN_SCREEN_PHONE_NUMBER;

/**
 * Created by mohit on 29/8/17.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    /**firebase state handling*/
    private static boolean isFirebaseCalledOnce = false;

    /**Firebase auth object*/
    private FirebaseAuth mAuth;

    /**Firebase authstate listener*/
    private FirebaseAuth.AuthStateListener mAuthListener;

    private GoogleApiClient mGoogleApiClient;

    /**Facebook callback*/
    private CallbackManager mCallbackManager;

    //Google Sign in
    private static final int RC_SIGN_IN = 9001;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    public String mCountryPhoneCode = null;

    private final String MAIN_FRAGMENT_TAG = "main_frag_tag";
    private final String PHONE_FRAGMENT_TAG = "phone_frag_tag";
    private final String EMAIL_FRAGMENT_TAG = "email_frag_tag";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //kill nordic ble service if it's running
        EventBus.getDefault().post(new EventBusMessage(KILL_SWITCH));

        setContentView(R.layout.login_activity);

        setupFirebase();

        showPrimarySignInLayout();
    }


    private void setupFirebase() {
        mAuth = FirebaseAuth.getInstance();

        //logout any previous user
        performSignOut();

        setupGoogleAPIClient();

        setupFacebookAPIClient();

        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null) {
                    // User is signed in
                    if(!isFirebaseCalledOnce) {
                        isFirebaseCalledOnce = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginUserWithFirebase(true);
                            }
                        });
                        //getUserTokenAndGetUser(true);
                    }
                    Timber.d("onAuthStateChanged:signed_in:");
                } else {
                    hideProgressDialog();
                    showPrimarySignInLayout();
                    isFirebaseCalledOnce = false;
                    // User is signed out
                    Timber.d("onAuthStateChanged:signed_out");
                }
            }
        };
        // [END auth_state_listener]

        mAuth.addAuthStateListener(mAuthListener);
    }

    private void getCountryCode() {
        TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);

        if(telephonyManager == null) {
            mCountryPhoneCode = Constants.countryCodes.get("in");
        } else {
            String countryNameCode= telephonyManager.getSimCountryIso();
            mCountryPhoneCode = String.format("+%s", Constants.countryCodes.get(countryNameCode));
        }
    }

    private void loginUserWithFirebase(boolean forceRefreshToken) {
        showProgressDialog();

        mAuth.getCurrentUser().getIdToken(forceRefreshToken).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            public void onComplete(@NonNull Task<GetTokenResult> task) {
                if(task.isSuccessful()) {
                    String idToken = task.getResult().getToken();
                    getUser(idToken);
                } else {
                    // Handle error -> task.getException();
                    Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_token));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    LoginActivity.this.startActivity(intent);

                    performSignOut();
                }
            }
        });
    }

    private void getUser(String authToken) {

        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .build();

        ApolloClient apolloClient = ApolloClient.builder()
                .serverUrl(getString(R.string.server_link))
                .okHttpClient(okHttpClient)
                .addCustomTypeAdapter(CustomType.DATETIME, new GraphQLDateTimeCustomAdapter())
                .build();

        LoginWithFirebaseMutation loginWithFirebaseMutation = LoginWithFirebaseMutation
                                                                .builder()
                                                                .accessTokenVar(authToken)
                                                                .appPackageVar(getApplicationContext().getPackageName())
                                                                .build();

        apolloClient.mutate(loginWithFirebaseMutation).enqueue(new ApolloCall.Callback<LoginWithFirebaseMutation.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<LoginWithFirebaseMutation.Data> response) {

                if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            performSignOut();

                            Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_login));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            LoginActivity.this.startActivity(intent);

                        }
                    });

                } else {

                    if(response.getData() == null
                            || response.getData().account() == null
                            || response.getData().account().loginWithFirebase() == null
                            || response.getData().account().loginWithFirebase().user() == null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                performSignOut();

                                Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.response_null));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                LoginActivity.this.startActivity(intent);

                            }
                        });

                        return;
                    }

                    LoginWithFirebaseMutation.LoginWithFirebase loginWithFirebase = response.getData().account().loginWithFirebase();
                    LoginWithFirebaseMutation.User firebaseUser = response.getData().account().loginWithFirebase().user();

                    String token = loginWithFirebase.token();
                    String id = firebaseUser.id();
                    Role role = firebaseUser.role();
                    String firstName = firebaseUser.firstName();
                    String lastName = firebaseUser.lastName();
                    String email = firebaseUser.email();
                    String phoneNo = firebaseUser.phone();
                    String altPhone1 = firebaseUser.altPhone1();
                    String altPhone2 = firebaseUser.altPhone2();
                    StatusType statusType = firebaseUser.status();
                    String dob = firebaseUser.dob();

                    FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                String firebaseToken = task.getException().getMessage();
                                Timber.e("FCM TOKEN Failed=====>" +task.getException());

                            } else {
                                String firebaseToken = task.getResult().getToken();
                                sendRegistrationToServer(phoneNo,email,token,firebaseToken);
                                Timber.e("FCM TOKEN=====>" +firebaseToken);
                            }
                        }
                    });

                    if(token != null) {
                        //store this token in shared prefs
                        getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_TOKEN_KEY, token).apply();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                performSignOut();

                                Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.auth_token_null));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                LoginActivity.this.startActivity(intent);

                            }
                        });
                        return;
                    }

                    //check if user status is active
                    if(statusType == null || statusType.equals(StatusType.DELETED) || statusType.equals(StatusType.INACTIVE)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                performSignOut();

                                Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_renew_subscription));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                                intent.putExtras(bundle);
                                LoginActivity.this.startActivity(intent);

                            }
                        });
                        return;
                    } else {
                        //create and store user object
                        User user = new User();
                        user.setId(id);
                        user.setRole(role);
                        user.setFirstName(firstName);
                        user.setLastName(lastName);
                        user.setEmail(email);
                        user.setPhone(phoneNo);
                        user.setAltPhone1(altPhone1);
                        user.setAltPhone2(altPhone2);
                        user.setDob(dob);
                        user.setStatusType(statusType);

                        String userJSONStr = new Gson().toJson(user);

                        getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_JSON_DATA_KEY, userJSONStr).apply();

                        getUserUploads();
                    }
                }

            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        performSignOut();

                        Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        LoginActivity.this.startActivity(intent);

                    }
                });
            }
        });
    }

    private void getUserUploads() {
        //get user id
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

        if(user == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    performSignOut();

                    Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_token));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    LoginActivity.this.startActivity(intent);

                }
            });
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();
        if(apolloClient == null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    performSignOut();

                    Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error_in_getting_user_uploads));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    LoginActivity.this.startActivity(intent);
                }
            });
            return;
        }

        UserWhereInput userWhereInput = UserWhereInput.builder()
                                        .id(user.getId())
                                        .build();

        FileWhereInput fileWhereInput = FileWhereInput.builder()
                                            .user(userWhereInput)
                                            .build();

        UserUploadsQuery userUploadsQuery = UserUploadsQuery
                                            .builder()
                                            .fileInputVar(fileWhereInput)
                                            .build();

        apolloClient.query(userUploadsQuery).enqueue(new ApolloCall.Callback<UserUploadsQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<UserUploadsQuery.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            performSignOut();

                            Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error_in_getting_user_uploads));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            LoginActivity.this.startActivity(intent);

                        } else {

                            if(response.getData() != null && response.getData().files() != null) {

                                if(response.getData().files().list() != null) {
                                    SharedPreferences sharedPreferences = getSharedPreferences(USER_PREFS, MODE_PRIVATE);

                                    List<UserUploadsQuery.List> fileList = response.getData().files().list();
                                    for (int fileIndex = 0; fileIndex < fileList.size(); ++fileIndex) {
                                        FileType fileType = fileList.get(fileIndex).type();
                                        String fileName = fileList.get(fileIndex).name();

                                        if (fileType == FileType.PROFILE) {
                                            sharedPreferences.edit().putString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, fileName).apply();
                                        } else if (fileType == FileType.DL_SIDE_1) {
                                            sharedPreferences.edit().putString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, fileName).apply();
                                        } else if (fileType == FileType.DL_SIDE_2) {
                                            sharedPreferences.edit().putString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, fileName).apply();
                                        }
                                    }
                                }
                                decideAndLaunchNextActivity();

                            } else {
                                performSignOut();

                                Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_get_user_uploads));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                LoginActivity.this.startActivity(intent);

                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        performSignOut();

                        Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        LoginActivity.this.startActivity(intent);

                    }
                });
            }
        });
    }


    private void launchUserDetails() {
        startActivity(new Intent(getApplicationContext(), UserDetailActivity.class));
    }

    private void launchGuestDetails() {
        startActivity(new Intent(getApplicationContext(), GuestDetailActivity.class));
    }

    private void decideAndLaunchNextActivity() {
        //clear the guest mode flag
        getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(IS_APP_IN_GUEST_MODE, false).apply();

        SharedPreferences sharedPreferences;
        //check if all user details are present else launch user details activity
        User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(user == null
                || user.getFirstName() == null
                || user.getLastName() == null
                || user.getEmail() == null
                || user.getDob() == null
                || user.getFirstName().trim().isEmpty()
                || user.getLastName().trim().isEmpty()
                || user.getEmail().trim().isEmpty()
                || user.getDob().trim().isEmpty()) {
            launchUserDetails();
            finish();
            return;
        }

        //check for location permissions
        if(ActivityCompat.checkSelfPermission(LoginActivity.this, Constants.permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || (Build.VERSION.SDK_INT > Build.VERSION_CODES.P
                && ActivityCompat.checkSelfPermission(LoginActivity.this, Constants.permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED)) {
            launchProminentDisclosureActivity();
        }

        //check for simple permissions
        if(ActivityCompat.checkSelfPermission(LoginActivity.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(LoginActivity.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED) {

            launchAppIntro();
            finish();
            return;
        }

        //check for notification access
        String notificationListenerList = Settings.Secure.getString(this.getContentResolver(),"enabled_notification_listeners");
        if(notificationListenerList == null || !(notificationListenerList.contains(getApplicationContext().getPackageName()))) {
            launchAppIntro();
            finish();
            return;
        }


        //check if bike prefs are present
        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(device == null) {
            launchAppIntro();
            finish();
            return;
        }

        //check if user phone and emergency contact are present
        if( PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs() == null) {
            //there has been an error, this situation should not occur
            performSignOut();

            Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            LoginActivity.this.startActivity(intent);

        } else {
            user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
            if(user.getPhone() == null || user.getAltPhone1() == null || user.getPhone().isEmpty() || user.getAltPhone1().isEmpty()) {
                launchAppIntro();
                finish();
                return;
            }
        }

        launchMainActivity();
        finish();
    }

    private void launchAppIntro() {
        hideProgressDialog();
        Intent myIntent = new Intent(LoginActivity.this, AppIntroActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    private void launchProminentDisclosureActivity() {
        Intent myIntent = new Intent(LoginActivity.this, ProminentDisclosureActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    private void launchMainActivity() {
        hideProgressDialog();
        Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    private void showPrimarySignInLayout() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager != null) {
            fragmentManager.beginTransaction().replace(R.id.login_frame_layout, new LoginMainFragment(), MAIN_FRAGMENT_TAG).commitAllowingStateLoss();
        }
    }

    private void performSignOut() {
        AuthUI.getInstance().signOut(getApplicationContext());

        hideProgressDialog();
        showPrimarySignInLayout();

        signOutGoogle();
        signOutFacebook();

        //clear the prefs
        PrefUtils.getInstance(getApplicationContext()).clearAllPrefs();

        if(mAuth != null) {
            mAuth.signOut();
        }
    }

    //setup google api client
    private void setupGoogleAPIClient() {
        // [START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    //setup facebook login
    private void setupFacebookAPIClient() {
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();


        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.d("facebook:onSuccess:%s", loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                int j = 10;
                ++j;
                Timber.d("facebook:onCancel");
                // [START_EXCLUDE]
                performSignOut();
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException error) {
                int j = 10;
                ++j;
                Timber.d("facebook:onError" + error);
                // [START_EXCLUDE]
                performSignOut();
                // [END_EXCLUDE]
            }
        });
    }

    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {
        Timber.d("handleFacebookAccessToken:%s", token);
        // [START_EXCLUDE silent]
        //showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Timber.d("signInWithCredential:onComplete:%s", task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if(!task.isSuccessful()) {
                            Timber.w(task.getException(),"signInWithCredential");

                            performSignOut();

                            String message = getString(R.string.authentication_failed);
                            if(task.getException() instanceof FirebaseAuthUserCollisionException) {
                                message = getString(R.string.firebase_user_collision_message);
                            }

                            Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            LoginActivity.this.startActivity(intent);

                        }
                    }
                });
    }

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Timber.d("firebaseAuthWithGoogle:%s", acct.getId());
        // [START_EXCLUDE silent]
        //showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Timber.d("signInWithCredential:onComplete:%s", task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if(!task.isSuccessful()) {
                            Timber.w(task.getException(),"signInWithCredential");

                            performSignOut();

                            String message = getString(R.string.authentication_failed);
                            if(task.getException() instanceof FirebaseAuthUserCollisionException) {
                                message = getString(R.string.firebase_user_collision_message);
                            }

                            Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            LoginActivity.this.startActivity(intent);

                        }
                    }
                });
    }
    // [END auth_with_google]

    // [START signin]
    void signInWithGoogle() {
        showProgressDialog();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void signOutGoogle() {

        if(mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            return;
        }

        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        showPrimarySignInLayout();
                    }
                });

    }

    private void signInWithFacebook() {
        showProgressDialog();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
    }

    private void signOutFacebook() {

        LoginManager.getInstance().logOut();
        showPrimarySignInLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCountryCode();
    }


    private void launchPhoneAuthFragment(String phoneNumber) {
        Bundle bundle = new Bundle();
        bundle.putString(LOGIN_SCREEN_PHONE_NUMBER, phoneNumber);
        bundle.putString(LOGIN_SCREEN_COUNTRY_CODE, mCountryPhoneCode);
        PhoneAuthFragment phoneAuthFragment = new PhoneAuthFragment();
        phoneAuthFragment.setArguments(bundle);


        if(getSupportFragmentManager() != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.login_frame_layout, phoneAuthFragment, PHONE_FRAGMENT_TAG)
                    .commitAllowingStateLoss();
        }
    }

    private void launchLoginViaEmailFragment() {
        if(getSupportFragmentManager() != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.login_frame_layout, new EmailLoginFragment(), EMAIL_FRAGMENT_TAG)
                    .commitAllowingStateLoss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                if (account != null) {
                    firebaseAuthWithGoogle(account);
                }
            } else {
                // [START_EXCLUDE]
                performSignOut();
                // [END_EXCLUDE]
            }
        } else {
            // Pass the activity result back to the Facebook SDK
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(LoginActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void saveVerifiedPhoneNumberToPrefs(String phoneNumber) {
        if(phoneNumber != null) {
            getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
            .edit()
            .putString(USER_VERIFIED_PHONE_NUMBER, phoneNumber)
            .apply();

            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putBoolean(IS_USER_PHONE_NUMBER_VERIFIED, true).apply();
        }
    }

    public void clearVerifiedPhoneNumberFromPrefs() {
        getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                .edit()
                .putString(USER_VERIFIED_PHONE_NUMBER, null)
                .apply();

        getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putBoolean(IS_USER_PHONE_NUMBER_VERIFIED, false).apply();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Timber.d("onConnectionFailed:%s", connectionResult);

        Intent intent = new Intent(LoginActivity.this, GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.google_play_services_error));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        intent.putExtras(bundle);
        LoginActivity.this.startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MAIN_FRAGMENT_TAG);
        if(fragment != null && fragment.isVisible()) {
            super.onBackPressed();
        } else {
            showPrimarySignInLayout();
        }
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(LOGIN_EVENT_SIGN_IN_GOOGLE)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    signInWithGoogle();
                }
            });
        } else if(event.message.equals(LOGIN_EVENT_SIGN_IN_FB)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    signInWithFacebook();
                }
            });
        } else if(event.message.contains(LOGIN_EVENT_SIGN_IN_PHONE)) {
            String phoneNumber = null;

            if(event.message.contains(GEN_DELIMITER)) {
                phoneNumber = event.message.split(GEN_DELIMITER)[1];
            }
            String finalPhoneNumber = phoneNumber;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    launchPhoneAuthFragment(finalPhoneNumber);
                }
            });

        } else if(event.message.equals(LOGIN_EVENT_SIGN_IN_EMAIL)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    launchLoginViaEmailFragment();
                }
            });
        } else if(event.message.equals(LOGIN_EVENT_SIGN_IN_GUEST)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    launchGuestDetails();
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    public void sendRegistrationToServer(String phoneNo, String email, String authToken, String firebaseToken){

        if(authToken == null) {
            //TODO:: hide progress dialogue
            Intent intent = new Intent(  LoginActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_token));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String bearerToken = "Bearer " + authToken;

        if(firebaseToken == null) {
            //TODO:: hide progress dialogue
            Intent intent = new Intent(  LoginActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_firebase_token));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }



        if (phoneNo == null ||  email == null){
            return;
        }


        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("token",firebaseToken);
        bodyJsonObject.addProperty("email",email);
        bodyJsonObject.addProperty("phone",phoneNo);
        String bodyStr = new Gson().toJson(bodyJsonObject);

        Timber.e("email=====>" +email);
        Timber.e("FirebaseToken=====>" +firebaseToken);
        Timber.e("auth=====>" +bearerToken);
        Timber.e("phone=====>" +phoneNo);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        FirebaseInterface firebaseInterface= FirebaseClient.getClient().create(FirebaseInterface.class);
        firebaseInterface.sendFirebaseToken(bearerToken,requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    return;
                }

                Timber.e("status====>"+response.code());
                Timber.e("status====>"+response.errorBody());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(  LoginActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });
    }
}
