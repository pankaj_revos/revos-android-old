package com.revos.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.ramotion.fluidslider.FluidSlider;
import com.revos.android.R;
import com.revos.android.jsonStructures.DataFeedVehicleLiveLog;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.type.ModelProtocol;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class TripPlayBackActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnCameraMoveListener {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private TextView mSpeedValueTextView, mVoltageValueTextView, mCurrentValueTextView, mModeValueTextView;

    private ImageView mPlayPauseImageView;

    private FluidSlider mFluidSlider;

    private Marker mVehicleCurrentLocationMarker;
    private Polyline mRoutePolyline;

    private Vehicle mVehicle;

    private ProgressDialog mProgressDialog;

    private String mVin;
    private String mStartTime;
    private String mEndTime;
    private String mTripId;

    public final static String ARG_VIN = "arg_vin";
    public final static String ARG_TRIP_ID = "arg_trip_id";
    public final static String ARG_START_TIME = "arg_start_time";
    public final static String ARG_END_TIME = "arg_end_time";

    private ArrayList<DataFeedVehicleLiveLog> mLogPacketList, mProcessedLogPacketList;

    private int mCurrentLogPacketIndex = 0;

    private LatLng mAnimationPreviousLatLng;

    private boolean mPlayAnimation = true;

    private float mDistanceCoveredInAnimation = 0;

    private int REQUEST_PERMISSION_REQ_CODE = 200;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(MainActivity.mCurrentThemeID);
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if(bundle.containsKey(ARG_VIN)) {
                mVin = bundle.getString(ARG_VIN, "");
            }
            if(bundle.containsKey(ARG_TRIP_ID)) {
                mTripId = bundle.getString(ARG_TRIP_ID, "");
            }
            if (bundle.containsKey(ARG_START_TIME)) {
                mStartTime = bundle.getString(ARG_START_TIME, "");
            }
            if(bundle.containsKey(ARG_END_TIME)) {
                mEndTime = bundle.getString(ARG_END_TIME, "");
            }
        } else {
            finish();
        }

        if(mVin == null || mTripId == null || mStartTime == null || mEndTime == null || mVin.isEmpty() || mTripId.isEmpty() || mStartTime.isEmpty() || mEndTime.isEmpty()) {
            finish();
        }

        setContentView(R.layout.trip_playback_activity);

        //get the vehicle object
        mVehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.trip_playback_map_fragment);

        if(mMapFragment != null) {
            mMapFragment.getMapAsync(this);
        }

    }

    private void fetchVehicleLogs() {

        String userToken = getSharedPreferences(com.boltCore.android.constants.Constants.USER_PREFS, MODE_PRIVATE).getString(com.boltCore.android.constants.Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getVehicleLiveLogWithoutCount(mVin, mStartTime, mEndTime, mTripId, true, bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                //TODO:: read response
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }


                    String dataStr = jsonObject.get("data").toString();

                    ArrayList<DataFeedVehicleLiveLog> vehicleTripLogList = new Gson().fromJson(dataStr, new TypeToken<ArrayList<DataFeedVehicleLiveLog>>(){}.getType());

                    if(vehicleTripLogList == null) {
                        return;
                    }

                    mLogPacketList = new ArrayList<>();

                    for(DataFeedVehicleLiveLog liveLog: vehicleTripLogList) {

                        if(liveLog != null) {
                            mLogPacketList.add(liveLog);
                        }
                    }


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrayList<DataFeedVehicleLiveLog> processedList = new ArrayList<>();
                            for(DataFeedVehicleLiveLog livePacket :  mLogPacketList) {

                                double speed = 0;
                                if(mVehicle != null && mVehicle.getModel() != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                                    speed = livePacket.getGpsSpeed();
                                } else {
                                    Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
                                    if(mVehicle == null || mVehicle.getModel() == null || device.getVehicle() == null || device.getVehicle().getModel() == null || device.getVehicle().getModel().getSpeedDivisor() == 0) {
                                        continue;
                                    }
                                    speed = (int) Math.floor(livePacket.getWheelRpm() / device.getVehicle().getModel().getSpeedDivisor());
                                }

                                if(livePacket.getLatitude() == 0 && livePacket.getLongitude() == 0) {
                                    continue;
                                } else if(speed < 5){
                                    continue;
                                } else {
                                    processedList.add(livePacket);
                                }
                            }

                            Collections.reverse(processedList);

                            mProcessedLogPacketList = processedList;

                            setupViews();

                            drawStartEndMarkers(mProcessedLogPacketList);

                            drawPolyLine(mProcessedLogPacketList);

                            setupFluidSlider(mProcessedLogPacketList);

                            startPlayBack(mProcessedLogPacketList);
                        }
                    });

                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        });
    }

    private void drawStartEndMarkers(ArrayList<DataFeedVehicleLiveLog> logList) {
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(logList.get(0).getLatitude(), logList.get(0).getLongitude()))
                .zIndex(5)
                .anchor(0.33f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_flag_start, convertDipToPixels(28), convertDipToPixels(28)))));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(logList.get(logList.size() - 1).getLatitude(), logList.get(logList.size() - 1).getLongitude()))
                .zIndex(5)
                .anchor(0.33f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_flag_end, convertDipToPixels(28), convertDipToPixels(28)))));
    }

    private void drawPolyLine(ArrayList<DataFeedVehicleLiveLog> liveLogList) {
        if(liveLogList == null || liveLogList.isEmpty() || liveLogList.size() < 2) {
            return;
        }

        ArrayList<LatLng> locationList = new ArrayList<>();

        for(DataFeedVehicleLiveLog livePacket : liveLogList) {
            LatLng latLng = new LatLng(livePacket.getLatitude(), livePacket.getLongitude());
            locationList.add(latLng);
        }


        PolylineOptions polylineOptions = new PolylineOptions().clickable(false);
        polylineOptions.addAll(locationList);
        polylineOptions.width(16.0f);
        polylineOptions.color(ContextCompat.getColor(getApplicationContext(), R.color.revos_green_dark));
        if(mRoutePolyline != null) {
            mRoutePolyline.remove();
        }
        mRoutePolyline = mMap.addPolyline(polylineOptions);
    }

    private void startPlayBack(ArrayList<DataFeedVehicleLiveLog> liveLogList) {
        if(liveLogList == null || liveLogList.isEmpty() || liveLogList.size() < 2) {
            return;
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(liveLogList.get(mCurrentLogPacketIndex).getLatitude(), liveLogList.get(mCurrentLogPacketIndex).getLongitude())));

        mPlayAnimation = true;
        animateCameraForLogPacket(mCurrentLogPacketIndex, liveLogList);
    }

    private void animateCameraForLogPacket(int logPacketIndex, ArrayList<DataFeedVehicleLiveLog> liveLogList) {
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        if(liveLogList == null || liveLogList.isEmpty() || liveLogList.size() < 2) {
            return;
        }

        if(logPacketIndex == liveLogList.size() - 1) {
            return;
        }

        LatLng latLng = new LatLng(liveLogList.get(logPacketIndex).getLatitude(), liveLogList.get(logPacketIndex).getLongitude());
        LatLng latLngNext = new LatLng(liveLogList.get(logPacketIndex + 1).getLatitude(), liveLogList.get(logPacketIndex + 1).getLongitude());

        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);

        Location locationNext = new Location("");
        locationNext.setLatitude(latLngNext.latitude);
        locationNext.setLongitude(latLngNext.longitude);

        float bearing = location.bearingTo(locationNext);
        if(bearing == 0) {
            bearing = mMap.getCameraPosition().bearing;
        }


        mDistanceCoveredInAnimation += location.distanceTo(locationNext);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(new CameraPosition
                .Builder()
                .target(latLng)
                .bearing(bearing)
                .tilt(60)
                .zoom(mMap.getMaxZoomLevel() - 3)
                .build());

        CameraUpdate cameraUpdateNext = CameraUpdateFactory.newCameraPosition(new CameraPosition
                .Builder()
                .target(latLngNext)
                .bearing(bearing)
                .tilt(60)
                .zoom(mMap.getMaxZoomLevel() - 3)
                .build());

        //mMap.moveCamera(cameraUpdate);


        if(mRoutePolyline == null) {
            //drawPolyLine(liveLogList, logPacketIndex + 1);
        }

        mMap.animateCamera(cameraUpdateNext, 1000, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!mPlayAnimation) {
                            return;
                        }

                        ++mCurrentLogPacketIndex;
                        mFluidSlider.setPosition((mCurrentLogPacketIndex * 1.0f) / (liveLogList.size() - 1));
                        animateCameraForLogPacket(mCurrentLogPacketIndex, liveLogList);
                    }
                });
            }

            @Override
            public void onCancel() {
            }
        });
    }

    private void setupViews() {
        ImageView liveTrackingBackButtonImageView = findViewById(R.id.trip_playback_back_button_image_view);
        mSpeedValueTextView = findViewById(R.id.trip_playback_speed_value_text_view);
        mVoltageValueTextView = findViewById(R.id.trip_playback_voltage_value_text_view);
        mCurrentValueTextView = findViewById(R.id.trip_playback_current_value_text_view);
        mModeValueTextView = findViewById(R.id.trip_playback_mode_value_text_view);
        mPlayPauseImageView = findViewById(R.id.trip_playback_play_pause_image_view);

        updateAnimationControlViews();
        mPlayPauseImageView.setOnClickListener(v -> {
            mPlayAnimation = !mPlayAnimation;
            updateAnimationControlViews();
        });

        if(mVehicle != null && mVehicle.getModel() != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            findViewById(R.id.trip_playback_current_mode_linear_layout).setVisibility(View.GONE);
        }

        liveTrackingBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void updateAnimationControlViews() {
        mPlayPauseImageView.setVisibility(View.VISIBLE);
        int drawableId = R.drawable.ic_trip_playback_play_grey;
        if(mPlayAnimation) {
            drawableId = R.drawable.ic_trip_playback_pause_grey;
        }

        mPlayPauseImageView.setImageDrawable(ContextCompat.getDrawable(TripPlayBackActivity.this, drawableId));

        if(mPlayAnimation) {
            startPlayBack(mProcessedLogPacketList);
        } else {
            mMap.getUiSettings().setAllGesturesEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
        }
    }

    private void setupFluidSlider(ArrayList<DataFeedVehicleLiveLog> locationDataArrayList) {
        mFluidSlider = findViewById(R.id.trip_playback_fluid_slider);

        mFluidSlider.setVisibility(View.VISIBLE);

        mFluidSlider.setBeginTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                //set play animation flag to false
                mPlayAnimation = false;

                mMap.getUiSettings().setAllGesturesEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);

                updateAnimationControlViews();
                return Unit.INSTANCE;
            }
        });

        mFluidSlider.setEndTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                int arrayPosition = Math.min(locationDataArrayList.size() - 1, Math.round((locationDataArrayList.size() - 1) * mFluidSlider.getPosition()));
                updateFluidSliderText(arrayPosition, locationDataArrayList);
                updateDataPointTextViews(arrayPosition);
                return Unit.INSTANCE;
            }
        });

        mFluidSlider.setPositionListener(pos -> {

            int arrayPosition = Math.min(locationDataArrayList.size() - 1, Math.round((locationDataArrayList.size() - 1) * pos));

            updateFluidSliderText(arrayPosition, locationDataArrayList);
            updateDataPointTextViews(arrayPosition);
            mCurrentLogPacketIndex = arrayPosition;

            double lat = locationDataArrayList.get(arrayPosition).getLatitude();
            double lng = locationDataArrayList.get(arrayPosition).getLongitude();

            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mProcessedLogPacketList.get(mCurrentLogPacketIndex).getLatitude(), mProcessedLogPacketList.get(mCurrentLogPacketIndex).getLongitude())));

            return Unit.INSTANCE;
        });


        //set the initial state of the fluid slider
        mFluidSlider.setPosition(0);
    }

    private void updateDataPointTextViews(int position) {
        DataFeedVehicleLiveLog livePacket = mProcessedLogPacketList.get(position);

        if(mVehicle != null && mVehicle.getModel() != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mSpeedValueTextView.setText(String.format("%s %s", String.valueOf(livePacket.getGpsSpeed()), getString(R.string.kilometer_per_hour)));
            mVoltageValueTextView.setText(String.format("%s %s", String.valueOf(livePacket.getBatteryVoltageAdc()), "V"));
        } else {

            Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
            if(mVehicle == null || mVehicle.getModel() == null || device.getVehicle() == null || device.getVehicle().getModel() == null || device.getVehicle().getModel().getSpeedDivisor() == 0) {
                return;
            }
            int vehicleSpeed = (int) Math.floor(livePacket.getWheelRpm() / device.getVehicle().getModel().getSpeedDivisor());
            mSpeedValueTextView.setText(String.format("%s %s", String.valueOf(vehicleSpeed), getString(R.string.kilometer_per_hour)));
            mVoltageValueTextView.setText(String.format("%s %s", String.valueOf(livePacket.getDeviceBatteryVoltage()), "V"));
            mCurrentValueTextView.setText(String.format("%s %s", String.valueOf(livePacket.getBatteryCurrent()), "A"));

            int strId = R.string.economy_mode;
            if(livePacket.getMode() == 1) {
                strId = R.string.ride_mode;
            }
            if(livePacket.getMode() == 2) {
                strId = R.string.sport_mode;
            }
            mModeValueTextView.setText(getString(strId));
        }
    }

    private void updateFluidSliderText(int position, ArrayList<DataFeedVehicleLiveLog> liveLogList) {
        if(mFluidSlider == null) {
            return;
        }

        mFluidSlider.setBubbleText(DateTimeFormat.forPattern("h:mma").print(new DateTime(liveLogList.get(position).getTimestamp())).split(" ")[0]);
        if(mFluidSlider.getPosition() <= 0.5) {
            mFluidSlider.setEndText(getString(R.string.fluid_slider_hint));
            mFluidSlider.setStartText("");
        } else {
            mFluidSlider.setStartText(getString(R.string.fluid_slider_hint));
            mFluidSlider.setEndText("");
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        setupGoogleMaps();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.setOnCameraMoveListener(TripPlayBackActivity.this);
                fetchVehicleLogs();
            }
        });
    }

    private void repositionVehicleMarker(double lat, double lng) {
        if(mVehicleCurrentLocationMarker == null) {
            mVehicleCurrentLocationMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .anchor(0.5f, 1)
                    .zIndex(4)
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_bike_unselected, convertDipToPixels(48), convertDipToPixels(48)))));
        }
        mVehicleCurrentLocationMarker.setPosition(new LatLng(lat, lng));
    }

    private void setupGoogleMaps() {
        if(mMap != null) {
            if (ActivityCompat.checkSelfPermission(TripPlayBackActivity.this, mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(TripPlayBackActivity.this, mPermissionsRequired, REQUEST_PERMISSION_REQ_CODE);
                return;
            }
        }
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION_REQ_CODE) {
            boolean permissionGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                } else {
                    permissionGranted = false;
                    break;
                }
            }
            if (permissionGranted) {
                setupGoogleMaps();
            }
        }
    }

    @Override
    public void onCameraMove() {
        LatLng latLng = mMap.getCameraPosition().target;

        if(mPlayAnimation) {
            //animate the marker
            repositionVehicleMarker(latLng.latitude, latLng.longitude);

            if (mAnimationPreviousLatLng == null) {
                mAnimationPreviousLatLng = latLng;
                return;
            }

            Location previousLocation = new Location("");
            previousLocation.setLatitude(mAnimationPreviousLatLng.latitude);
            previousLocation.setLongitude(mAnimationPreviousLatLng.longitude);

            Location currentLocation = new Location("");
            currentLocation.setLatitude(latLng.latitude);
            currentLocation.setLongitude(latLng.longitude);


            mAnimationPreviousLatLng = latLng;
        } else {
            repositionVehicleMarker(mProcessedLogPacketList.get(mCurrentLogPacketIndex).getLatitude(), mProcessedLogPacketList.get(mCurrentLogPacketIndex).getLongitude());
        }

    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(TripPlayBackActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
