package com.revos.android.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import com.revos.android.events.EventBusMessage;
import com.revos.android.fragments.EditUserInfoFragment;
import com.revos.android.fragments.UserInfoFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.revos.android.constants.Constants.USER_PROFILE_EVENT_BACK_PRESSED;
import static com.revos.android.constants.Constants.USER_PROFILE_FRAG_EVENT_LOAD_EDIT_USER_FRAGMENT;
import static com.revos.android.constants.Constants.USER_PROFILE_FRAG_EVENT_USER_UPDATED;

/**
 * Created by mohit on 15/9/17.
 */

public class UserProfileActivity extends AppCompatActivity{
    /**fragments*/
    private Fragment mUserInfoFragment, mEditUserInfoFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //set theme before activity starts
        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        loadUserProfileFragment();
    }

    private void loadUserProfileFragment() {
        if(mUserInfoFragment == null) {
            mUserInfoFragment = new UserInfoFragment();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, mUserInfoFragment).commit();

    }

    private void loadEditUserProfileFragment() {
        if(mEditUserInfoFragment == null) {
            mEditUserInfoFragment = new EditUserInfoFragment();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, mEditUserInfoFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if(mEditUserInfoFragment != null && mEditUserInfoFragment.isVisible()) {
            EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_EVENT_BACK_PRESSED));
        } else {
            super.onBackPressed();
        }
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(USER_PROFILE_FRAG_EVENT_USER_UPDATED)) {
            //start recognizer
            loadUserProfileFragment();
        } else if(event.message.equals(USER_PROFILE_FRAG_EVENT_LOAD_EDIT_USER_FRAGMENT)) {
            loadEditUserProfileFragment();
        }
    }
}
