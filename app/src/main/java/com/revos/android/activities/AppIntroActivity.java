package com.revos.android.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.github.paolorotolo.appintro.AppIntro;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.fragments.ContactInfoFragment;
import com.revos.android.fragments.PermissionFragment;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.UpdateUserMutation;
import com.revos.scripts.type.UserUpdateInput;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.Arrays;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.IS_USER_PHONE_NUMBER_VERIFIED;
import static com.revos.android.constants.Constants.LAST_GOOGLE_NOTIFICATION_TIMESTAMP;
import static com.revos.android.constants.Constants.NAV_PREFS;
import static com.revos.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_VERIFIED_PHONE_NUMBER;
import static com.revos.android.constants.Constants.permissionsRequired;

/**
 * Created by mohit on 26/8/17.
 */

public class AppIntroActivity extends AppIntro {

    /**user contact info handling*/
    public String mUserPhoneNumber;

    private final int PERMISSION_CALLBACK_CONSTANT = 200;

    /**booleans for deciding which fragments to show*/
    private boolean mAddPermissionFragment, mAddContactInfoFragment;

    /**variables for referencing the fragments*/
    private Fragment mPermissionFragment, mContactInfoFragment;

    /**boolean to check if all permissions have been granted*/
    private boolean mAllPermissionsGranted;

    private boolean mIsPermissionRequestInProgress = false;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    /**private local copy of user*/
    private User mUser;

    private String[] mPermissionRequiredArray = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mAddPermissionFragment = mAddContactInfoFragment = mIsPermissionRequestInProgress = false;

        mUserPhoneNumber = "";

        mContactInfoFragment = mPermissionFragment = null;

        //set wizard mode to true so that we don't lose state
        setWizardMode(true);

        //decide and add required fragments
        decideAndAddRequiredFragments();

        setDoneText(getString(R.string.done));
    }

    private void decideAndAddRequiredFragments() {
        //check if permissions are present
        if(ActivityCompat.checkSelfPermission(AppIntroActivity.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(AppIntroActivity.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED) {
            mAddPermissionFragment = true;
        }

        mAllPermissionsGranted = !mAddPermissionFragment;

        //check if user phone and emergency contact are present
        mUser = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(mUser == null) {
            //there has been an error, this situation should not occur
            finish();

            Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            AppIntroActivity.this.startActivity(intent);

        } else {
            String userPhoneNumberPreSanitized = mUser.getPhone();
            String userPhoneNumber ="";
            if(userPhoneNumberPreSanitized != null) {
               userPhoneNumber  = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(userPhoneNumberPreSanitized);
            }

            //if user has not signed up via phone number, then the phone number is null and not verified
            if(userPhoneNumber == null || userPhoneNumber.isEmpty()) {
                mAddContactInfoFragment = true;
            }
        }

        if(mAddPermissionFragment) {
            mPermissionFragment = new PermissionFragment();
            addSlide(mPermissionFragment);
        }

        if(mAddContactInfoFragment) {
            mContactInfoFragment= new ContactInfoFragment();
            addSlide(mContactInfoFragment);
        }

        if(!mAddPermissionFragment && !mAddContactInfoFragment) {
            launchMainActivity();
        }

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_grey));
        showSeparator(false);
        setIndicatorColor(ContextCompat.getColor(getApplicationContext(), R.color.revos_green),
                            ContextCompat.getColor(getApplicationContext(), R.color.revos_green_dark));
        setColorDoneText(ContextCompat.getColor(getApplicationContext(), R.color.revos_green));
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);

        if(oldFragment != null && oldFragment.getClass() == PermissionFragment.class) {
            if(!mIsPermissionRequestInProgress) {
                mIsPermissionRequestInProgress = true;

                mPermissionRequiredArray = Arrays.copyOfRange(permissionsRequired, 2, 4);

                ActivityCompat.requestPermissions(AppIntroActivity.this, mPermissionRequiredArray, PERMISSION_CALLBACK_CONSTANT);
            }
        }
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        handleDonePressed();
    }

    public void handleDonePressed() {
        ContactInfoFragment contactInfoFragment = (ContactInfoFragment)mContactInfoFragment;
        if(mContactInfoFragment != null ) {

            //TODO::check from prefs if number is verified or not
            String userVerifiedPhoneNumber = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_VERIFIED_PHONE_NUMBER, null);
            if(userVerifiedPhoneNumber != null && !userVerifiedPhoneNumber.isEmpty()) {
                mUserPhoneNumber = userVerifiedPhoneNumber;
            }
        }

        if(mAddPermissionFragment && !mAllPermissionsGranted) {
            if(!mIsPermissionRequestInProgress) {
                Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_grant_permission));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                AppIntroActivity.this.startActivity(intent);

                mIsPermissionRequestInProgress = true;

                ActivityCompat.requestPermissions(AppIntroActivity.this, mPermissionRequiredArray, PERMISSION_CALLBACK_CONSTANT);
            }
            return;
        }

        if(mAddContactInfoFragment) {
            boolean isPhoneNumberVerified = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getBoolean(IS_USER_PHONE_NUMBER_VERIFIED, false);

            String userPhoneNumber = contactInfoFragment.getCountryCodePicker().getFullNumberWithPlus();
            if(userPhoneNumber == null || userPhoneNumber.isEmpty()) {

                Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_your_phone_number));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                AppIntroActivity.this.startActivity(intent);

                //smooth scroll to fragment
                getPager().setCurrentItem(2, true);
                return;
            } else if(!isPhoneNumberVerified) {

                Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_valid_phone_number));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                AppIntroActivity.this.startActivity(intent);

                //smooth scroll to fragment
                getPager().setCurrentItem(2, true);
                return;
            }
        }

        if((mAddContactInfoFragment && !mUserPhoneNumber.isEmpty())) {
            //update user profile
            //show progress dialog

            if(mAddContactInfoFragment && !mUserPhoneNumber.isEmpty()) {
                mUser.setPhone(mUserPhoneNumber);
            }

            makeUpdateUserCall();

            return;
        }

        decideAndLaunchMainActivity();
    }

    private void makeUpdateUserCall() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        UserUpdateInput userUpdateInput = UserUpdateInput
                .builder()
                .firstName(mUser.getFirstName())
                .lastName(mUser.getLastName())
                .email(mUser.getEmail())
                .phone(mUser.getPhone())
                .dob(mUser.getDob())
                .gender(mUser.getGenderType())
                .role(mUser.getRole())
                .build();

        UpdateUserMutation updateUserMutation = UpdateUserMutation.builder().data(userUpdateInput).build();

        apolloClient.mutate(updateUserMutation).enqueue(new ApolloCall.Callback<UpdateUserMutation.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<UpdateUserMutation.Data> response) {
                if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            hideProgressDialog();

                            String message = getString(R.string.unable_to_set_user_details);
                            if(response.getErrors().get(0) != null) {
                                BigDecimal statusCode = (BigDecimal)response.getErrors().get(0).getCustomAttributes().get("statusCode");
                                if(statusCode.intValue() == 409) {
                                    message = response.getErrors().get(0).getMessage();
                                }
                            }

                            Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //store user object in prefs
                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_JSON_DATA_KEY, new Gson().toJson(mUser)).apply();
                            hideProgressDialog();
                            launchMainActivity();
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        //todo::handle server error scenarios at a granular level

                        Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        AppIntroActivity.this.startActivity(intent);

                        launchLoginActivity();
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT){
            //check if all permissions are granted
            boolean allGranted = false;
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if(allGranted) {
                mIsPermissionRequestInProgress = false;
                mAllPermissionsGranted = true;
                decideAndLaunchMainActivity();
            } else if(ActivityCompat.checkSelfPermission(AppIntroActivity.this, mPermissionRequiredArray[0]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(AppIntroActivity.this, mPermissionRequiredArray[1]) != PackageManager.PERMISSION_GRANTED) {

                AlertDialog.Builder builder = new AlertDialog.Builder(AppIntroActivity.this);
                builder.setTitle(R.string.need_following_permissions);

                if(ActivityCompat.shouldShowRequestPermissionRationale(AppIntroActivity.this, mPermissionRequiredArray[0])) {
                    String contactsAccessMessage = getString(R.string.access_to_contacts_permission) + "\n" + getString(R.string.access_to_contacts_permission_description);
                    builder.setMessage(contactsAccessMessage);
                } else if(ActivityCompat.shouldShowRequestPermissionRationale(AppIntroActivity.this, mPermissionRequiredArray[1])) {
                    String externalStoragePermissionMessage = getString(R.string.access_to_external_storage_permission) + "\n" + getString(R.string.write_external_storage_permission_description);
                    builder.setMessage(externalStoragePermissionMessage);
                }

                builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        mIsPermissionRequestInProgress = true;
                        ActivityCompat.requestPermissions(AppIntroActivity.this, mPermissionRequiredArray, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {

                Intent intent = new Intent(AppIntroActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_get_permission));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                intent.putExtras(bundle);
                AppIntroActivity.this.startActivity(intent);

            }
        }
    }

    private void decideAndLaunchMainActivity() {
         if(!mAddContactInfoFragment) {
            launchMainActivity();
            finish();
        }
    }

    private void launchMainActivity() {
        //reset google navigation prefs that are used to display the navigation banner
        getSharedPreferences(NAV_PREFS, MODE_PRIVATE).edit().putLong(LAST_GOOGLE_NOTIFICATION_TIMESTAMP, 0).apply();
        Intent myIntent = new Intent(AppIntroActivity.this, MainActivity.class);
        AppIntroActivity.this.startActivity(myIntent);
        finish();
    }

    private void launchLoginActivity() {
        hideProgressDialog();
        Intent myIntent = new Intent(AppIntroActivity.this, LoginActivity.class);
        AppIntroActivity.this.startActivity(myIntent);
        finish();
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(AppIntroActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
