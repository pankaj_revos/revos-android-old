package com.revos.android.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.revos.android.R;
import com.revos.android.utilities.ble.BleUtils;

import java.io.IOException;
import java.util.Arrays;

import timber.log.Timber;

public class HexFileGeneratorActivity extends AppCompatActivity{

    private TextView mOutputTextView;
    private ImageView mOutputImageView;
    private Button mSelectFileButton;
    private EditText mHeightEditText, mWidthEditText;
    private static final int SELECT_FILE_REQ = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.hex_file_gen_activity);

        setupViews();
    }

    private void setupViews() {
        mOutputImageView = (ImageView) findViewById(R.id.hex_output_image_view);
        mOutputTextView = (TextView)findViewById(R.id.hex_output_text_view);
        mSelectFileButton = (Button) findViewById(R.id.hex_select_file_button);
        mHeightEditText = (EditText) findViewById(R.id.hex_height);
        mWidthEditText = (EditText) findViewById(R.id.hex_width);

        mHeightEditText.setText("32");
        mWidthEditText.setText("48");

        mSelectFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();
            }
        });
    }

    private void openFileChooser() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , SELECT_FILE_REQ);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case SELECT_FILE_REQ:
                processFile(data.getData());
                break;

            default:
                break;
        }
    }

    private void processFile(Uri uri) {
        generateHexCode(uri);

    }

    private void generateHexCode(Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        } catch (IOException e) {
            //todo::handle exception
            return;
        }

        Bitmap changedBitMap = Bitmap.createScaledBitmap(bitmap, Integer.parseInt(mWidthEditText.getText().toString()), Integer.parseInt(mHeightEditText.getText().toString()), false);
        changedBitMap.setHasAlpha(true);
        changedBitMap.setDensity(bitmap.getDensity());

        Bitmap finalBitmap = changedBitMap.copy(Bitmap.Config.ARGB_8888, true);
        finalBitmap.setHasAlpha(true);

        int BUFFER_LENGTH = changedBitMap.getHeight() * changedBitMap.getWidth() / 8;

        byte[] hexValByteArray = new byte[BUFFER_LENGTH];

        byte tempByte = 0;

        int pixelCount = 0;

        for(int y = 0; y < changedBitMap.getHeight(); y = y + 8) {
            for(int x = 0; x < changedBitMap.getWidth(); ++x) {

                if(pixelCount > 0 && pixelCount % 8 == 0) {
                    hexValByteArray[Math.round((pixelCount - 1) / 8)] = tempByte;
                    tempByte = 0;
                }


                for(int bitIndex = 7; bitIndex >= 0; --bitIndex) {
                    int pixel = changedBitMap.getPixel(x,y + bitIndex);
                    //Timber.d("pixel value : " + x + ", " + (y+bitIndex));
                    int alpha = Color.alpha(pixel);

                    Timber.d("PIXEL : " + alpha + ", Count : " + pixelCount);

                    if(alpha < 0xff) {
                        tempByte = (byte)(tempByte << 1);
                        Timber.d("transparent");
                        finalBitmap.setPixel(x, y + bitIndex, 0xffff0000);
                    } else {
                        if(pixel == 0xff000000) {
                            tempByte = (byte) ((tempByte << 1) | 0x01);
                            Timber.d("black, %s", alpha);
                            finalBitmap.setPixel(x, y + bitIndex, 0xff000000);
                        } else {
                            tempByte = (byte) (tempByte << 1);
                            finalBitmap.setPixel(x, y + bitIndex, 0xffff0000);
                        }
                    }

                    ++pixelCount;
                }

            }
        }

        hexValByteArray[BUFFER_LENGTH - 1] = tempByte;

        Timber.d("pixel_count : %s", pixelCount);
        Timber.d("arrayData : %s", Arrays.toString(hexValByteArray));

        String hexString = BleUtils.byteArrayToHexString(hexValByteArray);

        String formattedHexString = "";

        for(int index = 0; index < hexString.length(); index = index + 2) {
            formattedHexString = formattedHexString + "0x" + hexString.charAt(index) + hexString.charAt(index + 1) + ", ";
        }

        Timber.d("hex_string : %s", formattedHexString);

        mOutputTextView.setText(formattedHexString);

        Glide.with(getApplicationContext()).load(finalBitmap).into(mOutputImageView);

    }

}
