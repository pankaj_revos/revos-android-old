package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.Version;
import com.revos.android.utilities.general.VersionUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;


import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_DISCONNECT;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;
import static com.revos.android.constants.Constants.VER_1_0;

public class BluetoothEnterPinActivity extends AppCompatActivity {
    public static String VERSION_NUMBER_KEY = "version_no_key";

    private ProgressDialog mProgressDialog;
    private String mPin;

    private OtpView mOtpView;

    private Timer mTimer = null;

    private boolean mMetadataDownloadComplete = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.enter_pin_activity);

        setupViews();

        Bundle bundle = getIntent().getExtras();

        if(bundle == null || bundle.getString(VERSION_NUMBER_KEY, null) == null) {
            MainActivity.mBluetoothEnterPinActivityLaunched = false;
            finish();
            return;
        }
        //download metadata
        NetworkUtils.getInstance(getApplicationContext()).refreshControllerMetadata(bundle.getString(VERSION_NUMBER_KEY));

        MetadataDecryptionHelper.getInstance(getApplicationContext()).clearLocalCache();
    }

    @Override
    public void onBackPressed() {
        NordicBleService.clearStaticDeviceVariables();
        clearPrefs();
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));
        super.onBackPressed();
    }

    private void clearPrefs() {
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();

        getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

    }

    private void setupViews() {
        mOtpView = ((OtpView)findViewById(R.id.enter_pin_otp_view));

        mOtpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                mPin = otp;
            }
        });

        findViewById(R.id.enter_pin_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, mPin).apply();

                if(mMetadataDownloadComplete) {
                    checkPin();
                    return;
                }

                showProgressDialog();
                if(mTimer != null) {
                    return;
                }
                mTimer = new Timer();
                mTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                toggleProgressDialog();
                            }
                        });
                    }
                }, 0, 1000);
            }
        });
    }

    private void toggleProgressDialog() {
        NetworkUtils networkUtils = NetworkUtils.getInstance(getApplicationContext());
        String completeFirmwareVersion = NordicBleService.getDeviceFirmwareVersion();
        String noPatchFirmwareVersion = VersionUtils.removePatchVersion(completeFirmwareVersion);

        if(noPatchFirmwareVersion == null) {
            hideProgressDialog();
            return;
        }

        if(networkUtils.isReadDataDownloadComplete()
                && networkUtils.isParamWriteDataDownloadComplete()
                && networkUtils.isVehicleControlDataDownloadComplete()
                && networkUtils.isLogDataDownloadComplete()) {

            if(noPatchFirmwareVersion != null &&
                    !noPatchFirmwareVersion.isEmpty() &&
                    new Version(noPatchFirmwareVersion).equals(new Version(VER_1_0))) {
                hideProgressDialog();
                if(!mMetadataDownloadComplete) {
                    mMetadataDownloadComplete = true;
                    checkPin();
                }
            } else if(networkUtils.isBatteryAdcLogDataDownloadComplete()){
                hideProgressDialog();
                if(!mMetadataDownloadComplete) {
                    mMetadataDownloadComplete = true;
                    checkPin();
                }
            }
        }
        //todo::handle failure scenario at granular level
    }

    private void checkPin() {

        MetadataDecryptionHelper metadataDecryptionHelper = MetadataDecryptionHelper.getInstance(getApplicationContext());

        if(metadataDecryptionHelper.decryptControllerReadMetadata() == null ||
            metadataDecryptionHelper.decryptControllerLogMetadata() == null ||
            metadataDecryptionHelper.decryptControllerWriteMetadata() == null ||
            metadataDecryptionHelper.decryptVehicleControlMetadata() == null) {

            mOtpView.setText("");
            clearPrefs();
            NordicBleService.clearStaticDeviceVariables();
            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, true).apply();
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));

            Intent intent = new Intent(BluetoothEnterPinActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.incorrect_pin));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);
            MainActivity.mBluetoothEnterPinActivityLaunched = false;
            finish();
        } else {

            getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();

            Intent intent = new Intent(BluetoothEnterPinActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);
            MainActivity.mBluetoothEnterPinActivityLaunched = false;
            finish();
        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(BluetoothEnterPinActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        super.onDestroy();
    }

}