package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.boltCore.android.constants.Constants;
import com.cashfree.pg.CFPaymentService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.R;
import com.revos.android.jsonStructures.DataFeedVehicleSnapshot;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.User;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.CreateNewLeaseMutation;
import com.revos.scripts.CreateNewLeaseMutationWithCashfreeMutation;
import com.revos.scripts.FetchLeaseForUserQuery;
import com.revos.scripts.UpdateLeaseMutation;
import com.revos.scripts.type.InputRegisterC2ULease;
import com.revos.scripts.type.LeaseStatus;
import com.revos.scripts.type.LeaseType;
import com.revos.scripts.type.LeaseWhereInput;
import com.revos.scripts.type.LeaseWhereUniqueInput;
import com.revos.scripts.type.PriceInfoCreateOneInput;
import com.revos.scripts.type.PriceInfoWhereUniqueInput;
import com.revos.scripts.type.Stage;
import com.revos.scripts.type.TxNodeType;
import com.revos.scripts.type.TxNodeWhereInput;
import com.revos.scripts.type.UnitType;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.cashfree.pg.CFPaymentService.PARAM_APP_ID;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_CURRENCY;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_ID;
import static com.cashfree.pg.CFPaymentService.PARAM_VENDOR_SPLIT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class BookRentalVehicleActivity extends AppCompatActivity{

    public static String BOOK_RENTAL_VEHICLE_OBJECT_KEY = "book_rental_vehicle_object_key";

    private int mRentalDurationMinimumValue, mRentalDurationMaximumValue, mRentalDurationValue;

    private RadioButton mHourlyRentalRadioButton, mDaysRentalRadioButton, mWeeksRentalRadioButton, mMonthlyRentalRadioButton;

    private TextView mRentalVehicleModelNameTextView, mRentalVehicleVinTextView, mRentalVehicleBatteryVoltageTextView,
                     mRentalVehicleBatterySOCTextView, mRentalAmountTextView, mRecipientNameTextView, mRecipientUpiIdTextView,
                     mRentalProviderNameTextView;

    private ImageView mCallRentalProviderImageView;

    private Button mRentalDurationPlusButton, mRentalDurationMinusButton, mRentalDurationValueButton, mPayButton;

    private ProgressDialog mProgressDialog;

    private String mRentalVehicleObjectString;

    private RentalVehicle mRentalVehicle;

    private UnitType mRentalDurationType;

    private String mRentalProviderUpiId;

    private double mAmountPayable;
    private float mCostPerUnit;

    private List<UnitType> mAvailableUnitTypeList;

    private final String TXN_STATUS_KEY = "txStatus";
    private final String SUCCESS = "SUCCESS";
    private final String DELIMITER = "<RD>";

    private String mLeaseId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            finish();
            return;
        }

        mRentalVehicleObjectString = bundle.getString(BOOK_RENTAL_VEHICLE_OBJECT_KEY, null);

        mRentalVehicle = new Gson().fromJson(mRentalVehicleObjectString, RentalVehicle.class);

        if (mRentalVehicle == null) {
            finish();
            return;
        }

        setContentView(R.layout.book_rental_vehicle_activity);

        setupAvailableUnitTypeList();

        setupViews();

        setupDefaultRentalDurationRadioButton();

        populateAmountValueTextView();

        fetchVehicleLiveLogForBatteryDetails();

        populateVehicleDetails();

        fetchCurrentBooking();
    }

    private void setupAvailableUnitTypeList() {

        mAvailableUnitTypeList = new ArrayList<>();

        if (mRentalVehicle.getRentalPricingInfoList() != null && !mRentalVehicle.getRentalPricingInfoList().isEmpty()) {

            for (int index = 0; index < mRentalVehicle.getRentalPricingInfoList().size(); index++) {

                if (mRentalVehicle.getRentalPricingInfoList().get(index).unit() != null) {
                    mAvailableUnitTypeList.add(mRentalVehicle.getRentalPricingInfoList().get(index).unit());
                }
            }
        }
    }

    private void setupViews() {

        mRentalVehicleModelNameTextView = findViewById(R.id.rental_book_vehicle_model_name_value_text_view);
        mRentalVehicleVinTextView = findViewById(R.id.rental_book_vehicle_vin_value_text_view);
        mRentalVehicleBatteryVoltageTextView = findViewById(R.id.rental_book_vehicle_battery_voltage_text_view);
        mRentalVehicleBatterySOCTextView = findViewById(R.id.rental_book_vehicle_battery_soc_text_view);
        mRentalAmountTextView = findViewById(R.id.rental_book_vehicle_amount_due_text_view);
        mRecipientNameTextView = findViewById(R.id.rental_book_vehicle_recipient_name_text_view);
        mRecipientUpiIdTextView = findViewById(R.id.rental_book_vehicle_recipient_upi_id_text_view);
        mRentalProviderNameTextView = findViewById(R.id.rental_book_vehicle_provider_name_value_text_view);

        mHourlyRentalRadioButton = findViewById(R.id.rental_book_vehicle_hours_radio_button);
        mHourlyRentalRadioButton.setClickable(false);

        mDaysRentalRadioButton = findViewById(R.id.rental_book_vehicle_days_radio_button);
        mDaysRentalRadioButton.setClickable(false);

        mWeeksRentalRadioButton = findViewById(R.id.rental_book_vehicle_weeks_radio_button);
        mWeeksRentalRadioButton.setClickable(false);

        mMonthlyRentalRadioButton = findViewById(R.id.rental_book_vehicle_months_radio_button);
        mMonthlyRentalRadioButton.setClickable(false);


        mRentalDurationPlusButton = findViewById(R.id.rental_book_vehicle_duration_plus_button);
        mRentalDurationMinusButton = findViewById(R.id.rental_book_vehicle_duration_minus_button);
        mRentalDurationValueButton = findViewById(R.id.rental_book_vehicle_time_period_value_button);
        mPayButton = findViewById(R.id.rental_book_vehicle_pay_using_upi_button);

        mCallRentalProviderImageView = findViewById(R.id.rental_book_vehicle_call_owner_image_view);


        //back button
        findViewById(R.id.rental_book_vehicle_back_button_image_view).setOnClickListener(v -> {
            finish();
        });

        setRadioButtonListenerForAvailableUnitType();

        mRentalDurationPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mRentalDurationValue < mRentalDurationMaximumValue) {

                    mRentalDurationValue++;
                    mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationValue), getRentalDurationTimeUnit()));
                    populateAmountValueTextView();
                }
            }
        });

        mRentalDurationMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mRentalDurationValue > mRentalDurationMinimumValue) {

                    mRentalDurationValue--;
                    mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationValue), getRentalDurationTimeUnit()));
                    populateAmountValueTextView();
                }
            }
        });

        mPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCashfreePaymentSequence();
            }
        });

        mCallRentalProviderImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCallToRentalProvider();
            }
        });
    }

    private void fetchVehicleLiveLogForBatteryDetails() {

        String userToken = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        if(userToken == null || mRentalVehicle.getVin() == null || mRentalVehicle.getVin().isEmpty()) {
            return;
        }

        dataFeedApiInterface.getVehicleLocationSnapshot(mRentalVehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }

                    String dataStr = jsonObject.get("data").toString();

                    DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                    if(vehicleSnapshot != null && vehicleSnapshot.getBattery() != null) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateBatteryVoltageDetails(vehicleSnapshot.getBattery().getBatteryVoltageAdc());
                            }
                        });
                    }
                } catch (Exception e) { }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {}
        });
    }

    private void updateBatteryVoltageDetails(double batteryADCVoltage) {

        double maxVoltage = mRentalVehicle.getModel().getConfig().getBatteryMaxVoltage();
        double minVoltage = mRentalVehicle.getModel().getConfig().getBatteryMinVoltage();

        long socValue = 0;
        double batteryPercentage = 0;

        if (batteryADCVoltage != 0 && maxVoltage != 0 && minVoltage != 0) {
            batteryPercentage = ((batteryADCVoltage - minVoltage) / (maxVoltage - minVoltage)) * 100;
            socValue = Math.round(Math.floor(batteryPercentage));
        }

        if(batteryADCVoltage <= 0) {
            batteryADCVoltage = 0;
        } else if(batteryADCVoltage > 100) {
            batteryADCVoltage = 100;
        }

        if(socValue <= 0) {
            socValue = 0;
        } else if(socValue > 100) {
            socValue = 100;
        }

        mRentalVehicleBatteryVoltageTextView.setText(String.valueOf(Math.round(Math.floor(batteryADCVoltage))).concat(" V"));
        mRentalVehicleBatterySOCTextView.setText(String.valueOf(socValue).concat("%"));

    }

    private void makeCallToRentalProvider() {

        String phoneNumber = null;

        if(mRentalVehicle.getCompany() != null && mRentalVehicle.getCompany().getPhone() != null
                && !mRentalVehicle.getCompany().getPhone().isEmpty()) {
            phoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(mRentalVehicle.getCompany().getPhone().trim());
        } else if(mRentalVehicle.getOem() != null && mRentalVehicle.getCompany().getPhone() != null
                && !mRentalVehicle.getOem().getPhone().isEmpty()) {
            phoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(mRentalVehicle.getOem().getPhone().trim());
        } else if(mRentalVehicle.getDistributor() != null && mRentalVehicle.getDistributor().getPhone() != null
                && !mRentalVehicle.getDistributor().getPhone().isEmpty()) {
            phoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(mRentalVehicle.getDistributor().getPhone().trim());
        } else if(mRentalVehicle.getModel() != null && mRentalVehicle.getModel().getCompany() != null
                && mRentalVehicle.getModel().getCompany().getPhone() != null && !mRentalVehicle.getModel().getCompany().getPhone().isEmpty()) {
            phoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(mRentalVehicle.getModel().getCompany().getPhone().trim());
        }

        String sanitizedPhoneNumber = PhoneUtils.getInstance(getApplicationContext()).sanitizePhoneNumber(phoneNumber);

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ sanitizedPhoneNumber));
        startActivity(intent);
    }

    private void setRadioButtonListenerForAvailableUnitType() {

        for(int index = 0 ; index < mAvailableUnitTypeList.size() ; index++) {

            if (mAvailableUnitTypeList.get(index).equals(UnitType.HOURS)) {

                mHourlyRentalRadioButton.setVisibility(View.VISIBLE);
                mHourlyRentalRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateCheckedRadioButton(R.id.rental_book_vehicle_hours_radio_button);
                    }
                });
            } else if (mAvailableUnitTypeList.get(index).equals(UnitType.DAYS)) {

                mDaysRentalRadioButton.setVisibility(View.VISIBLE);
                mDaysRentalRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateCheckedRadioButton(R.id.rental_book_vehicle_days_radio_button);
                    }
                });
            } else if (mAvailableUnitTypeList.get(index).equals(UnitType.WEEKS)) {

                mWeeksRentalRadioButton.setVisibility(View.VISIBLE);
                mWeeksRentalRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateCheckedRadioButton(R.id.rental_book_vehicle_weeks_radio_button);
                    }
                });
            } else if (mAvailableUnitTypeList.get(index).equals(UnitType.MONTHS)) {

                mMonthlyRentalRadioButton.setVisibility(View.VISIBLE);
                mMonthlyRentalRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateCheckedRadioButton(R.id.rental_book_vehicle_months_radio_button);
                    }
                });
            }
        }
    }

    private void setupDefaultRentalDurationRadioButton() {

        if(mAvailableUnitTypeList != null && !mAvailableUnitTypeList.isEmpty()) {

            if(mAvailableUnitTypeList.get(0).equals(UnitType.HOURS)) {
                mHourlyRentalRadioButton.setChecked(true);

                mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 24;

                mRentalDurationValue = mRentalDurationMinimumValue;

                mRentalDurationType = UnitType.HOURS;

            } else if(mAvailableUnitTypeList.get(0).equals(UnitType.DAYS)) {
                mDaysRentalRadioButton.setChecked(true);

                mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 30;

                mRentalDurationValue = mRentalDurationMinimumValue;

                mRentalDurationType = UnitType.DAYS;

            } else if(mAvailableUnitTypeList.get(0).equals(UnitType.WEEKS)) {

                mWeeksRentalRadioButton.setChecked(true);

                mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 6;

                mRentalDurationValue = mRentalDurationMinimumValue;

                mRentalDurationType = UnitType.WEEKS;

            } else if(mAvailableUnitTypeList.get(0).equals(UnitType.MONTHS)) {

                mMonthlyRentalRadioButton.setChecked(true);

                mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 24;

                mRentalDurationValue = mRentalDurationMinimumValue;

                mRentalDurationType = UnitType.MONTHS;
            }
        }
    }

    private void populateAmountValueTextView() {

        if(mHourlyRentalRadioButton.isChecked()) {
            mRentalDurationType = UnitType.HOURS;
        } else if (mDaysRentalRadioButton.isChecked()) {
            mRentalDurationType = UnitType.DAYS;
        } else if (mWeeksRentalRadioButton.isChecked()) {
            mRentalDurationType = UnitType.WEEKS;
        } else if(mMonthlyRentalRadioButton.isChecked()) {
            mRentalDurationType = UnitType.MONTHS;
        }

        if(mRentalVehicle.getRentalPricingInfoList() != null && !mRentalVehicle.getRentalPricingInfoList().isEmpty()) {

            for(int index = 0 ; index < mRentalVehicle.getRentalPricingInfoList().size() ; index++) {

                if(mRentalDurationType.equals(mRentalVehicle.getRentalPricingInfoList().get(index).unit())) {

                    if(mRentalVehicle.getRentalPricingInfoList().get(index).costPerUnit() != null) {

                        mCostPerUnit = mRentalVehicle.getRentalPricingInfoList().get(index).costPerUnit().floatValue();
                        break;
                    } else {

                        mCostPerUnit = mRentalVehicle.getRentalPricingInfoList().get(index).minimumPayableAmount().floatValue();
                        break;
                    }
                }
            }

            for(int index = 0 ; index < mRentalVehicle.getRentalPricingInfoList().size() ; index++) {

                if(mRentalVehicle.getRentalPricingInfoList().get(index).paymentMeta() != null
                        && mRentalVehicle.getRentalPricingInfoList().get(index).paymentMeta().address() != null
                        && !mRentalVehicle.getRentalPricingInfoList().get(index).paymentMeta().address().isEmpty()) {
                    mRentalProviderUpiId = mRentalVehicle.getRentalPricingInfoList().get(index).paymentMeta().address();
                    break;
                }
            }
        }

        if(mRentalProviderUpiId != null && !mRentalProviderUpiId.isEmpty()) {
            mRecipientUpiIdTextView.setText(mRentalProviderUpiId);
        } else  {
            mRecipientUpiIdTextView.setText(getString(R.string.not_available));
        }

        mAmountPayable = mRentalDurationValue * mCostPerUnit;

        mRentalAmountTextView.setText(String.valueOf(mAmountPayable));

        mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationValue),getRentalDurationTimeUnit()));
    }

    private void updateCheckedRadioButton(int selectedRadioButtonId) {

        switch (selectedRadioButtonId) {

            case R.id.rental_book_vehicle_hours_radio_button : mHourlyRentalRadioButton.setChecked(true);
                                                               mDaysRentalRadioButton.setChecked(false);
                                                               mWeeksRentalRadioButton.setChecked(false);
                                                               mMonthlyRentalRadioButton.setChecked(false);
                                                               break;
            case R.id.rental_book_vehicle_days_radio_button : mDaysRentalRadioButton.setChecked(true);
                                                              mHourlyRentalRadioButton.setChecked(false);
                                                              mWeeksRentalRadioButton.setChecked(false);
                                                              mMonthlyRentalRadioButton.setChecked(false);
                                                              break;
            case R.id.rental_book_vehicle_weeks_radio_button : mWeeksRentalRadioButton.setChecked(true);
                                                               mHourlyRentalRadioButton.setChecked(false);
                                                               mDaysRentalRadioButton.setChecked(false);
                                                               mMonthlyRentalRadioButton.setChecked(false);
                                                               break;

            case R.id.rental_book_vehicle_months_radio_button : mMonthlyRentalRadioButton.setChecked(true);
                                                                mHourlyRentalRadioButton.setChecked(false);
                                                                mDaysRentalRadioButton.setChecked(false);
                                                                mWeeksRentalRadioButton.setChecked(false);
                                                                break;
        }

        setRentalDurationType(selectedRadioButtonId);
    }

    private void setRentalDurationType(int selectedRadioButton) {

        switch(selectedRadioButton) {

            case R.id.rental_book_vehicle_hours_radio_button : mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 24;
                mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationMinimumValue), getString(R.string.hour_single)));
                mRentalDurationType = UnitType.HOURS;
                break;
            case R.id.rental_book_vehicle_days_radio_button : mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 30;
                mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationMinimumValue), getString(R.string.day_single)));
                mRentalDurationType = UnitType.DAYS;
                break;
            case R.id.rental_book_vehicle_weeks_radio_button : mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 6;
                mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationMinimumValue), getString(R.string.week_single)));
                mRentalDurationType = UnitType.WEEKS;
                break;

            case R.id.rental_book_vehicle_months_radio_button : mRentalDurationMinimumValue = 1;
                mRentalDurationMaximumValue = 24;
                mRentalDurationValueButton.setText(String.format("%s %s", String.valueOf(mRentalDurationMinimumValue), getString(R.string.month_single)));
                mRentalDurationType = UnitType.MONTHS;
                break;
        }

        mRentalDurationValue = mRentalDurationMinimumValue;

        populateAmountValueTextView();
    }

    private String getRentalDurationTimeUnit() {

        if(mHourlyRentalRadioButton.isChecked()) {

            if(mRentalDurationValue == mRentalDurationMinimumValue) {
                return getString(R.string.hour_single);
            } else {
                return getString(R.string.hour_plural);
            }

        } else if(mDaysRentalRadioButton.isChecked()) {

            if(mRentalDurationValue == mRentalDurationMinimumValue) {
                return getString(R.string.day_single);
            } else {
                return getString(R.string.day_plural);
            }

        } else if(mWeeksRentalRadioButton.isChecked()) {

            if(mRentalDurationValue == mRentalDurationMinimumValue) {
                return getString(R.string.week_single);
            } else {
                return getString(R.string.week_plural);
            }

        } else {

            if(mRentalDurationValue == mRentalDurationMinimumValue) {
                return getString(R.string.month_single);
            } else {
                return getString(R.string.month_plural);
            }
        }
    }

    private void populateVehicleDetails() {

        if(mRentalVehicle.getModel() != null && mRentalVehicle.getModel().getName() != null && !mRentalVehicle.getModel().getName().isEmpty()) {
            mRentalVehicleModelNameTextView.setText(mRentalVehicle.getModel().getName());
        } else {
            mRentalVehicleModelNameTextView.setText(getString(R.string.not_available));
        }

        if(mRentalVehicle.getVin() != null) {
            mRentalVehicleVinTextView.setText(mRentalVehicle.getVin());
        } else {
            mRentalVehicleVinTextView.setText(getString(R.string.not_available));
        }

        String providerName = null, providerPhone = null;
        if(mRentalVehicle.getCompany() != null) {
            if(mRentalVehicle.getCompany().getName() != null && !mRentalVehicle.getCompany().getName().isEmpty()) {
                providerName = mRentalVehicle.getCompany().getName();
            }

            if(mRentalVehicle.getCompany().getPhone() != null && !mRentalVehicle.getCompany().getPhone().isEmpty()) {
                providerPhone = mRentalVehicle.getCompany().getPhone();
            }
        } else if(mRentalVehicle.getOem() != null) {

            if(mRentalVehicle.getOem().getName() != null && !mRentalVehicle.getOem().getName().isEmpty()) {
                providerName = mRentalVehicle.getOem().getName();
            }

            if(mRentalVehicle.getOem().getPhone() != null && !mRentalVehicle.getOem().getPhone().isEmpty()) {
                providerPhone = mRentalVehicle.getOem().getPhone();
            }
        } else if(mRentalVehicle.getDistributor() != null) {

            if(mRentalVehicle.getDistributor().getName() != null && !mRentalVehicle.getDistributor().getName().isEmpty()) {
                providerName = mRentalVehicle.getDistributor().getName();
            }

            if(mRentalVehicle.getDistributor().getPhone() != null && !mRentalVehicle.getDistributor().getPhone().isEmpty()) {
                providerPhone = mRentalVehicle.getDistributor().getPhone();
            }
        }

        if(providerName != null && !providerName.isEmpty()) {
            mRentalProviderNameTextView.setText(providerName);
            mRecipientNameTextView.setText(providerName);
        } else {
            mRentalProviderNameTextView.setText(getString(R.string.not_available));
            mRecipientNameTextView.setText(getString(R.string.not_available));
        }

        if(providerPhone != null && !providerPhone.isEmpty()) {
            mCallRentalProviderImageView.setVisibility(View.VISIBLE);
        } else {
            mCallRentalProviderImageView.setVisibility(View.INVISIBLE);
        }
    }

    private void fetchCurrentBooking() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            showErrorAndFinish(getString(R.string.error));

            return;
        }

        showProgressDialog();

        LeaseWhereInput leaseWhereInput = LeaseWhereInput
                .builder()
                .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
                .status(LeaseStatus.ACTIVE)
                .build();

        FetchLeaseForUserQuery fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build();

        apolloClient.query(fetchLeaseForUserQuery).enqueue(new ApolloCall.Callback<FetchLeaseForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchLeaseForUserQuery.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        if(response.getData() != null && response.getData().lease() != null && response.getData().lease().getLeasesForUser() != null) {

                            List<FetchLeaseForUserQuery.GetLeasesForUser> fetchedLeaseForUserList = response.getData().lease().getLeasesForUser();

                            if(fetchedLeaseForUserList != null && !fetchedLeaseForUserList.isEmpty()) {
                                //show end current booking message
                                showEndCurrentBooking();
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                showErrorAndFinish(getString(R.string.server_error));
            }
        });
    }

    private void showEndCurrentBooking() {
        Intent intent = new Intent(BookRentalVehicleActivity.this, GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.end_current_booking));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        intent.putExtras(bundle);
        startActivity(intent);

        finish();
    }


    private void startCashfreePaymentSequence() {
        generateLeaseWithPaymentPendingStatus("initiating cashFree pending payment booking");
    }

    private void generateLeaseWithPaymentPendingStatus(String remarks) {
        String DELIMITER = "<RD>";

        DateTime currentTime = new DateTime();
        DateTime endTime;
        if(mRentalDurationType.equals(UnitType.HOURS)) {
            endTime = currentTime.plusHours(mRentalDurationValue);
        } else if(mRentalDurationType.equals(UnitType.DAYS)) {
            endTime = currentTime.plusDays(mRentalDurationValue);
        } else if(mRentalDurationType.equals(UnitType.WEEKS)) {
            endTime = currentTime.plusWeeks(mRentalDurationValue);
        } else if(mRentalDurationType.equals(UnitType.MONTHS)) {
            endTime = currentTime.plusMonths(mRentalDurationValue);
        } else {
            endTime = currentTime.plusHours(mRentalDurationValue);
        }


        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            Intent intent = new Intent(BookRentalVehicleActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_book_vehicle));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        PriceInfoCreateOneInput priceInfoCreateOneInput = null;

        if(mRentalVehicle.getRentalPricingInfoList() != null && !mRentalVehicle.getRentalPricingInfoList().isEmpty()) {

            for(int index = 0 ; index < mRentalVehicle.getRentalPricingInfoList().size() ; index++) {

                if(mRentalDurationType.equals(mRentalVehicle.getRentalPricingInfoList().get(index).unit())) {

                    priceInfoCreateOneInput = PriceInfoCreateOneInput
                            .builder()
                            .connect(PriceInfoWhereUniqueInput
                                    .builder()
                                    .id(mRentalVehicle.getRentalPricingInfoList().get(index).id()).build())
                            .build();
                    break;
                }
            }

        } else {
            Intent intent = new Intent(BookRentalVehicleActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_book_vehicle));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(priceInfoCreateOneInput == null) {
            Intent intent = new Intent(BookRentalVehicleActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_book_vehicle));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        Stage stage = Stage.TEST;
        if(getString(R.string.cf_stage).equals("PROD")) {
            stage = Stage.PROD;
        }

        LeaseStatus leaseStatus = LeaseStatus.PENDING_PAYMENT;
        //if amount payable is zero then directly start the booking
        if(mAmountPayable == 0) {
            leaseStatus = LeaseStatus.ACTIVE;
        }

        InputRegisterC2ULease inputRegisterC2ULease = InputRegisterC2ULease
                .builder()
                .status(leaseStatus)
                .endTime(endTime.toString())
                .vehicle(VehicleWhereUniqueInput.builder().vin(mRentalVehicle.getVin()).build())
                .amount(mAmountPayable)
                .type(LeaseType.SHORT_TERM)
                .remarks(remarks)
                .priceInfo(priceInfoCreateOneInput)
                .stage(stage)
                .build();

        CreateNewLeaseMutationWithCashfreeMutation createNewLeaseMutationWithCashfree = CreateNewLeaseMutationWithCashfreeMutation
                .builder()
                .leaseDataInput(inputRegisterC2ULease)
                .build();

        apolloClient.mutate(createNewLeaseMutationWithCashfree).enqueue(new ApolloCall.Callback<CreateNewLeaseMutationWithCashfreeMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<CreateNewLeaseMutationWithCashfreeMutation.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            //unable to book vehicle
                            showErrorAndFinish(getString(R.string.server_error));
                        } else if(response.getData() != null && response.getData().lease() != null
                                && response.data().lease().newRegisterC2ULease() != null && response.getData().lease().newRegisterC2ULease().status() != null) {

                            String cfToken = response.getData().lease().newRegisterC2ULease().cftoken();
                            mLeaseId = response.getData().lease().newRegisterC2ULease().id();

                            //if amount payable is 0 then just finish
                            if(mAmountPayable == 0) {
                                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                intent.putExtras(bundle);
                                startActivity(intent);

                                setResult(RESULT_OK);
                                finish();
                                return;
                            }

                            if(response.getData().lease().newRegisterC2ULease().status() == LeaseStatus.PENDING_PAYMENT && cfToken != null && mLeaseId != null) {
                                User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
                                if(user != null && user.getPhone() != null && user.getEmail() != null) {
                                    initiateCashfreePayment(cfToken, mLeaseId, user.getPhone(), user.getEmail(), response.getData().lease().newRegisterC2ULease().vendorSplit());
                                }
                            } else {
                                showErrorAndFinish(getString(R.string.server_error));
                            }
                        } else {
                            showErrorAndFinish(getString(R.string.server_error));
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showErrorAndFinish(getString(R.string.server_error));

                        hideProgressDialog();
                    }
                });
            }
        });
    }

    private void updateLease(String leaseId, LeaseStatus leaseStatus, String remarks) {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        InputRegisterC2ULease inputRegisterC2ULease = InputRegisterC2ULease.builder().status(leaseStatus).amount(mAmountPayable).remarks(remarks).build();
        LeaseWhereUniqueInput leaseWhereUniqueInput = LeaseWhereUniqueInput.builder().id(leaseId).build();

        UpdateLeaseMutation updateLeaseMutation = UpdateLeaseMutation.builder()
                                                    .inputRegisterC2ULeaseVar(inputRegisterC2ULease)
                                                    .leaseWhereUniqueInputVar(leaseWhereUniqueInput)
                                                    .build();

        showProgressDialog();

        apolloClient.mutate(updateLeaseMutation).enqueue(new ApolloCall.Callback<UpdateLeaseMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateLeaseMutation.Data> response) {
                hideProgressDialog();

                if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                    //unable to book vehicle
                    showErrorAndFinish(getString(R.string.server_error));
                } else if(response.getData() != null && response.getData().lease() != null
                        && response.getData().lease().updateLease() != null
                        && response.getData().lease().updateLease().status() == LeaseStatus.ACTIVE) {

                    Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    setResult(RESULT_OK);
                    finish();
                } else {
                    showErrorAndFinish(getString(R.string.payment_failed));
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                showErrorAndFinish(getString(R.string.server_error));
            }
        });
    }


    private void initiateCashfreePayment(String cftoken, String orderId, String phone, String email, String vendorSplit) {

        Map<String, String> params = new HashMap<>();

        phone = phone.replaceFirst("\\+91", "");

        params.put(PARAM_APP_ID, getString(R.string.cf_app_id));
        params.put(PARAM_ORDER_ID, orderId);
        params.put(PARAM_ORDER_CURRENCY, "INR");
        params.put(PARAM_ORDER_AMOUNT, String.valueOf(mAmountPayable * 1.0f));
        params.put(PARAM_CUSTOMER_PHONE, phone);
        params.put(PARAM_CUSTOMER_EMAIL, email);

        if(vendorSplit != null && !vendorSplit.isEmpty()) {
            params.put(PARAM_VENDOR_SPLIT, vendorSplit);
        }

        CFPaymentService.getCFPaymentServiceInstance().doPayment(BookRentalVehicleActivity.this, params, cftoken, getString(R.string.cf_stage));
    }



    private void bookRentalVehicle(String remarks) {
        String DELIMITER = "<RD>";

        DateTime currentTime = new DateTime();
        DateTime endTime;
        if(mRentalDurationType.equals(UnitType.HOURS)) {
            endTime = currentTime.plusHours(mRentalDurationValue);
        } else if(mRentalDurationType.equals(UnitType.DAYS)) {
            endTime = currentTime.plusDays(mRentalDurationValue);
        } else if(mRentalDurationType.equals(UnitType.WEEKS)) {
            endTime = currentTime.plusWeeks(mRentalDurationValue);
        } else if(mRentalDurationType.equals(UnitType.MONTHS)) {
            endTime = currentTime.plusMonths(mRentalDurationValue);
        } else {
            endTime = currentTime.plusHours(mRentalDurationValue);
        }


        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();

            return;
        }

        PriceInfoCreateOneInput priceInfoCreateOneInput = null;

        if(mRentalVehicle.getRentalPricingInfoList() != null && !mRentalVehicle.getRentalPricingInfoList().isEmpty()) {

            for(int index = 0 ; index < mRentalVehicle.getRentalPricingInfoList().size() ; index++) {

                if(mRentalDurationType.equals(mRentalVehicle.getRentalPricingInfoList().get(index).unit())) {

                    priceInfoCreateOneInput = PriceInfoCreateOneInput
                            .builder()
                            .connect(PriceInfoWhereUniqueInput
                                    .builder()
                                    .id(mRentalVehicle.getRentalPricingInfoList().get(index).id()).build())
                            .build();
                    break;
                }
            }

        } else {
            Intent intent = new Intent(BookRentalVehicleActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_book_vehicle));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(priceInfoCreateOneInput == null) {
            Intent intent = new Intent(BookRentalVehicleActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_book_vehicle));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();


        InputRegisterC2ULease inputRegisterC2ULease = InputRegisterC2ULease
                                                        .builder()
                                                        .status(LeaseStatus.ACTIVE)
                                                        .endTime(endTime.toString())
                                                        .vehicle(VehicleWhereUniqueInput.builder().vin(mRentalVehicle.getVin()).build())
                                                        .amount(mAmountPayable)
                                                        .amountPaid(mAmountPayable)
                                                        .type(LeaseType.SHORT_TERM)
                                                        .remarks(mRecipientUpiIdTextView.getText().toString().trim() + DELIMITER + remarks)
                                                        .priceInfo(priceInfoCreateOneInput)
                                                        .build();

        CreateNewLeaseMutation createNewLeaseMutation = CreateNewLeaseMutation
                                                        .builder()
                                                        .leaseDataInput(inputRegisterC2ULease)
                                                        .build();

        apolloClient.mutate(createNewLeaseMutation).enqueue(new ApolloCall.Callback<CreateNewLeaseMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<CreateNewLeaseMutation.Data> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            //unable to book vehicle
                            showErrorAndFinish(getString(R.string.server_error));
                        } else if(response.getData() != null && response.getData().lease() != null && response.getData().lease().registerC2ULease() != null) {

                            Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.success));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                            setResult(RESULT_OK);
                            finish();
                        } else {
                            showErrorAndFinish(getString(R.string.server_error));
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showErrorAndFinish(getString(R.string.server_error));

                        hideProgressDialog();
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CFPaymentService.REQ_CODE) {
            //Same request code for all payment APIs.
            Timber.d("ReqCode : " + CFPaymentService.REQ_CODE);
            Timber.d("API Response : ");
            //Prints all extras. Replace with app logic.
            if (data != null) {
                Bundle bundle = data.getExtras();
                boolean txnSuccessful = false;
                if (bundle != null) {
                    for (String key : bundle.keySet()) {
                        if (bundle.getString(key) != null) {
                            Timber.d(key + " : " + bundle.getString(key));

                            if (key.equals(TXN_STATUS_KEY)) {
                                String status = bundle.getString(TXN_STATUS_KEY);

                                if (status != null && status.equals(SUCCESS) && !txnSuccessful) {
                                    txnSuccessful = true;
                                } else {
                                    txnSuccessful = false;
                                }
                            }
                        }
                    }

                    if (txnSuccessful) {
                        updateLease(mLeaseId, LeaseStatus.ACTIVE, "cashfree payment success" + DELIMITER + bundle.toString());
                    } else {
                        updateLease(mLeaseId, LeaseStatus.TERMINATED_FAILED_PAYMENT, "cashfree payment failed" + DELIMITER + bundle.toString());
                    }
                } else {
                    updateLease(mLeaseId, LeaseStatus.TERMINATED_FAILED_PAYMENT, "bundle null");
                }
            }
        }
    }

    private void showErrorAndFinish(String errorMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, errorMessage);
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    return;
                }
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(BookRentalVehicleActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.please_wait));
                if(!isFinishing()) {
                    mProgressDialog.show();
                }
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }
}