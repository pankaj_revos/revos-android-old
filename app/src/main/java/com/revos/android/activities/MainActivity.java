package com.revos.android.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.BuildConfig;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.fragments.ContactInfoFragment;
import com.revos.android.fragments.NotificationAccessMessageFragment;
import com.revos.android.jsonStructures.Contact;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.RentalVehicleCompany;
import com.revos.android.jsonStructures.RentalVehicleModel;
import com.revos.android.jsonStructures.User;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.GetUserLocationFromIpAPIClient;
import com.revos.android.retrofit.GetUserLocationFromIpApiInterface;
import com.revos.android.services.NordicBleService;
import com.revos.android.services.NetworkJobService;
import com.revos.android.utilities.adapters.DrawerListAdapter;
import com.revos.android.utilities.adapters.MainActivityViewPagerAdapter;
import com.revos.android.utilities.animations.ResizeAnimation;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.general.Version;
import com.revos.android.utilities.general.VersionUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.FetchLeaseForUserQuery;
import com.revos.scripts.type.DeviceType;
import com.revos.scripts.type.DeviceUpdateStatus;
import com.revos.scripts.type.LeaseStatus;
import com.revos.scripts.type.LeaseWhereInput;
import com.revos.scripts.type.ModelProtocol;
import com.revos.scripts.type.Role;
import com.revos.scripts.type.TxNodeType;
import com.revos.scripts.type.TxNodeWhereInput;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_CONNECTING;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;
import static com.revos.android.activities.ChangePinActivity.PHONE_ARG_KEY;
import static com.revos.android.activities.ChangePinActivity.RECONNECT_TO_DEVICE_ARG_KEY;
import static com.revos.android.activities.ChangePinActivity.VIN_ARG_KEY;
import static com.revos.android.activities.GenericMessageActivity.STATUS_INFO;
import static com.revos.android.constants.Constants.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY;
import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_SERVICE_LAST_ACTIVE_TIMESTAMP;
import static com.revos.android.constants.Constants.BT_EVENT_CHANGE_PIN_REQUIRED;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_START_ACTIVATION_PROCEDURE;
import static com.revos.android.constants.Constants.BT_EVENT_NO_PIN_FOUND;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_ICONCOX;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TBIT;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_DISCONNECT;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_CONNECTED;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_DISCONNECTED;
import static com.revos.android.constants.Constants.BT_EVENT_SWITCH_ON_BLUETOOTH;
import static com.revos.android.constants.Constants.BT_EVENT_WRONG_PIN;
import static com.revos.android.constants.Constants.CONTACT_IN_ACTION_KEY;
import static com.revos.android.constants.Constants.CRASH_TIMESTAMP_KEY;
import static com.revos.android.constants.Constants.DAY_NIGHT_AUTO_SWITCH_THEME_KEY;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_INFO_EVENT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_PERSISTENT_ERROR_EVENT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GENERAL_PREFS;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.GEN_EVENT_CHANGE_HAMBURGER_TO_BACK;
import static com.revos.android.constants.Constants.GEN_EVENT_NAV_BANNER_ANIM_UPDATED;
import static com.revos.android.constants.Constants.GEN_EVENT_TAB_CHANGE;
import static com.revos.android.constants.Constants.GOOGLE_MAPS_PACKAGE_NAME;
import static com.revos.android.constants.Constants.IS_APP_IN_GUEST_MODE;
import static com.revos.android.constants.Constants.IS_APP_IN_INDIA_KEY;
import static com.revos.android.constants.Constants.KILL_SWITCH;
import static com.revos.android.constants.Constants.KILL_SWITCH_FLAG_KEY;
import static com.revos.android.constants.Constants.LAST_GOOGLE_NOTIFICATION_TIMESTAMP;
import static com.revos.android.constants.Constants.LAST_LIVE_PACKET_REFRESHED_TIMESTAMP;
import static com.revos.android.constants.Constants.LIGHT_SENSOR_AVAILABLE;
import static com.revos.android.constants.Constants.LOG_OUT_FLAG_KEY;
import static com.revos.android.constants.Constants.NAV_EVENT_TRIP_ADVICE_DATA_UPDATED;
import static com.revos.android.constants.Constants.NAV_PREFS;
import static com.revos.android.constants.Constants.NETWORK_EVENT_DEVICE_DATA_REFRESHED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED;
import static com.revos.android.constants.Constants.NOTIFICATION_LISTENER_CONNECTED_KEY;
import static com.revos.android.constants.Constants.PHONE_EVENT_CONTACT_LIST_BUILDING;
import static com.revos.android.constants.Constants.PHONE_EVENT_CONTACT_LIST_BUILT;
import static com.revos.android.constants.Constants.PHONE_EVENT_MULTIPLE_MATCHED_RESULTS;
import static com.revos.android.constants.Constants.POS_ANALYTICS_TAB;
import static com.revos.android.constants.Constants.POS_NAV_TAB;
import static com.revos.android.constants.Constants.POS_OBD_TAB;
import static com.revos.android.constants.Constants.SHARED_PREFS_CLEARED_VERSION_CODE_KEY;
import static com.revos.android.constants.Constants.SPEECH_RECOGNITION_RESULT;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_PROFILE_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_PROFILE_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_SETTINGS_PREFS;
import static com.revos.android.constants.Constants.USE_DARK_THEME_KEY;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;
import static com.revos.android.constants.Constants.VER_1_0;
import static com.revos.android.constants.Constants.VER_1_1;
import static com.revos.android.services.NordicBleService.FASTEST_INTERVAL;
import static com.revos.android.services.NordicBleService.UPDATE_INTERVAL;

/**
 * Created by mohit on 26/8/17.
 */

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    /** views and layouts*/
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private DrawerLayout mDrawerLayout;
    private View mNavListHeader;
    private View mNavListFooter;
    private boolean mNavHeaderFooterFlag = false;

    private final static int MIN_SWITCH_PERIOD = 2000;

    private final int LIVE_PACKET_REFRESH_TIME_THRESHOLD = 60000;

    /**private local copy of user*/
    private User mUser;

    /**intent constants*/
    private final static String OVERRIDE_SETUP_THEME = "override_setup_theme";
    private final static String USE_DARK_THEME = "use_dark_theme";
    private final static String DEFAULT_TAB = "default_tab";

    /**timestamp values for marking light and dark conditions*/
    private long mLastSwitchToLightTimeStamp, mLastSwitchToDarkTimeStamp;

    private static final float LOW_LIGHT_THRESHOLD = 25;

    private boolean mAutoSwitchTheme;

    public static int mCurrentThemeID;

    /** titles for the tabs*/
    private CharSequence mTitles[] = {"Navigation", "OBD", "Analytics"};

    /**Timer for polling*/
    private Timer mOneSecondTimer;

    /** hamburger icon */
    private ImageView mHamBurgerMenuImageView, mVoiceCommandImageView;
    private static boolean isHamburgerSet = true;

    /** drawables for the tabs*/
    private int[] drawablesIds;

    /** animation related variables*/
    private final int TAB_LAYOUT_MAX_HEIGHT_IN_DP = 100;
    private final int TAB_LAYOUT_MIN_HEIGHT_IN_DP = 0;

    private Animation mResizeTabLayoutAnimation;

    public static boolean mPlugAndPlayEnterPinActivityLaunched = false;
    public static boolean mBluetoothEnterPinActivityLaunched = false;
    public static boolean mSetPinActivityLaunched = false;
    public static boolean mSOSActivityLaunched = false;
    public static boolean mChangePinActivityLaunched = false;

    /** intents for starting the service */
    private static Intent  mBLEServiceIntent;

    /**request codes*/
    //Speech Recognizer
    private final int REQ_CODE_SPEECH_INPUT = 102;
    private final int REQUEST_CHECK_LOCATION_SETTINGS = 201;
    private final int REQ_CODE_SET_PIN = 300;
    private final int REQ_CODE_ENTER_PIN = 400;


    /**flag for tracking state of location request setting*/
    private static boolean mIsLocationRequestInProgress = false;

    /**flag for showing poi search results on voice command*/
    public boolean mNavigateToOnVoiceResultReceived = false;

    public boolean mShowSetupVehicleFragment = false;

    /**List of autocomplete results*/
    public List<AutocompletePrediction> mAutoCompletePredictions;

    //local copy of contact List
    private ArrayList<Contact> mContactList;

    /**Progress dialog*/
    private ProgressDialog mProgressDialog;

    /**phone call permission string*/
    private static final int REQUEST_PHONE_CALL = 1;

    /**last known connection state when app was in focus*/
    private static int mAppInFocusLastKnownConnectionState;

    public static boolean mNotificationConnectedMessageShown = false;

    private ArrayList<Contact> mMatchedContactsList;

    private View mInternetConnectionPopupView, mActiveRentalBookingPopupView;

    private PopupWindow mInternetConnectionPopupWindow;

    private boolean mHasOTAMessageBeenShown = false;

    private boolean mIsAppInGuestMode = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mNavigateToOnVoiceResultReceived = false;
        mBluetoothEnterPinActivityLaunched = mSetPinActivityLaunched = mSOSActivityLaunched = mChangePinActivityLaunched = mPlugAndPlayEnterPinActivityLaunched = false;

        //check if app is in guest mode or not
        mIsAppInGuestMode = getSharedPreferences(APP_PREFS, MODE_PRIVATE).getBoolean(IS_APP_IN_GUEST_MODE, false);

        int currentVersionCode = BuildConfig.VERSION_CODE;

        //set the prefs once cleared flag
        if(!mIsAppInGuestMode) {
            getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putInt(SHARED_PREFS_CLEARED_VERSION_CODE_KEY, currentVersionCode).apply();
        }

        //clear the kill switch flag
        getSharedPreferences(GENERAL_PREFS, Context.MODE_PRIVATE).edit().putBoolean(KILL_SWITCH_FLAG_KEY, false).apply();

        boolean overrideSetupTheme = getIntent().getBooleanExtra(OVERRIDE_SETUP_THEME, false);

        if(overrideSetupTheme) {
            boolean useDarkTheme = getIntent().getBooleanExtra(USE_DARK_THEME, false);
            if(useDarkTheme) {
                mCurrentThemeID = R.style.AppFullScreenThemeDark;
                setTheme(mCurrentThemeID);
            } else {
                mCurrentThemeID = R.style.AppFullScreenTheme;
                setTheme(mCurrentThemeID);
            }
        } else {
            setupTheme();
        }

        int defaultTab = getIntent().getIntExtra(DEFAULT_TAB, 1);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);

        //initiate job
        startJob();
        //download vehicle model image if required
        downloadVehicleModelImage();

        //download images if required
        NetworkUtils.getInstance(getApplicationContext()).downloadDLPicIfRequired();
        NetworkUtils.getInstance(getApplicationContext()).downloadProfilePicIfRequired();

        //setup local user data
        if(!mIsAppInGuestMode) {
            setupLocalUserCopy();
        }




        //setup the view pager and the tab layout
        setupSlidingTabLayout(defaultTab);

        //check if app is being used in india
        isAppInIndia();

        //setup the drawer layout
        setupDrawerLayoutBasedOnAppMode();

        //setupViews()
        setupViews();

        //Create no internet popupWindow instance
        mInternetConnectionPopupWindow = new PopupWindow(mInternetConnectionPopupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        //setup speech listener
        setupSpeechRecognizer();

        //start the services and do other tasks if we're not in guest mode
        if(!mIsAppInGuestMode) {
            startServices();

            //setup the last known connection state
            mAppInFocusLastKnownConnectionState = NordicBleService.getConnectionState();

            //setup local contact list
            mContactList = PhoneUtils.getInstance(getApplicationContext()).getContactList();

            //fetch geo fence
            NetworkUtils.getInstance(getApplicationContext()).refreshGeoFenceData();

            //build the contact list again to update it, but only once per app launch
            //this flag is set inside splashscreen activity
            new BuildContactListTask().execute();
        }

        //start infinite polling for checking bluetooth connection state
        //check if theme needs to be switched
        if(mOneSecondTimer == null) {
            mOneSecondTimer = new Timer();
        }
        mOneSecondTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //start bluetooth service if not running
                            startBleServiceIfRequired();

                            //self destruct
                            if(decideAndSelfDestruct()) {
                                return;
                            }

                            //show progress dialog if required
                            toggleProgressDialog();


                            if (!hasWindowFocus()) {
                                return;
                            }

                            toggleNotificationWarningLayout();

                            switchThemeIfRequired();

                            refreshOBDFragmentIfRequired();

                            //check internet connectivity
                            toggleInternetPopupOnNoConnection();

                            //show notification connected dialog if required
                            showNotificationConnectedMessageIfRequired();

                            logOutIfRequired();

                            refreshBatteryADCVoltageIfRequired();

                            //check and ask user to switch on GPS
                            if (!checkForGPS()) {
                                checkAndShowLocationSettingsDialog();
                            }

                            //launchChangePinActivityIfRequired();

                            //open SOS activity if required
                            launchSOSActivityIfRequired();

                            //launch Enter Pin Activity If Required
                            launchBluetoothEnterPinActivityIfRequired();

                            //launch Plug N Play Enter Pin Activity if required
                            launchPlugAndPlayEnterPinActivityIfRequired();

                            //show ota message activity if required
                            showOTAAvailableMessageIfRequired();

                            //refresh vehicle location if required
                            refreshVehicleLocationIfRequired();
                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }

                    }
                });
            }
        }, 100, 1000);

    }

    private void isAppInIndia() {
        GetUserLocationFromIpApiInterface getUserLocationFromIpApiInterface = GetUserLocationFromIpAPIClient.getClient().create(GetUserLocationFromIpApiInterface.class);
        getUserLocationFromIpApiInterface.getUserLocation().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() != 200 || response.body() == null) {
                    getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(IS_APP_IN_INDIA_KEY, false).apply();

                } else {
                    try {
                        String bodyStr = response.body().string();

                        JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();

                        if(bodyJsonObject.has("countrycode") && bodyJsonObject.get("countrycode").getAsString().equals("IN")) {
                            getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(IS_APP_IN_INDIA_KEY, true).apply();
                        } else {
                            getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(IS_APP_IN_INDIA_KEY, false).apply();
                        }
                    } catch (IOException e) {
                        //do nothing
                    }
                }
                setupDrawerLayout();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(IS_APP_IN_INDIA_KEY, false).apply();
                setupDrawerLayout();
            }
        });
    }

    private void refreshVehicleLocationIfRequired() {
        if(!hasWindowFocus()) {
            return;
        }
        //refresh vehicle location every ten seconds, if window is in focus
        long timeInSeconds = Math.round(Math.floor(System.currentTimeMillis() / 1000));

        if(timeInSeconds % 10 == 0) {
            NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLastKnownLocation();
        }
    }

    private void launchChangePinActivityIfRequired() {
        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

        if(device != null) {

            if(device.isPinResetRequired()) {
                if(mBluetoothEnterPinActivityLaunched || mSetPinActivityLaunched || mSOSActivityLaunched || mChangePinActivityLaunched || mPlugAndPlayEnterPinActivityLaunched) {
                    return;
                }
                launchChangePinActivity(device.getVehicle().getVin(), device.getVehicle().getBuyerPhoneNo(),true);
            }
        }
    }

    private void launchBluetoothEnterPinActivityIfRequired() {
        if(NordicBleService.getConnectionState() != STATE_CONNECTED) {
            return;
        }
        //check if pin is present
        String devicePin = getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

        if(devicePin == null && device != null && !device.isPinResetRequired()) {
            if(NordicBleService.getDeviceFirmwareVersion() == null) {
                return;
            }
            if(mBluetoothEnterPinActivityLaunched || mSetPinActivityLaunched || mSOSActivityLaunched || mChangePinActivityLaunched || mPlugAndPlayEnterPinActivityLaunched) {
                return;
            } else {
                mBluetoothEnterPinActivityLaunched = true;
            }
            Intent intent = new Intent(getApplicationContext(), BluetoothEnterPinActivity.class);
            intent.putExtra(BluetoothEnterPinActivity.VERSION_NUMBER_KEY, NordicBleService.getDeviceFirmwareVersion());
            startActivityForResult(intent, REQ_CODE_ENTER_PIN);
        }
    }

    private void launchPlugAndPlayEnterPinActivityIfRequired() {
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle == null || vehicle.getModel().getProtocol() == null || vehicle.getModel().getProtocol() != ModelProtocol.PNP) {
            return;
        }

        String devicePin = getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(devicePin != null) {
            return;
        }

        if(mBluetoothEnterPinActivityLaunched || mSetPinActivityLaunched || mSOSActivityLaunched || mChangePinActivityLaunched || mPlugAndPlayEnterPinActivityLaunched) {
            return;
        } else {
            mPlugAndPlayEnterPinActivityLaunched = true;
        }

        Intent intent = new Intent(getApplicationContext(), PlugAndPlayEnterPinActivity.class);
        startActivity(intent);
    }

    private void launchSOSActivityIfRequired() {
        if(mSOSActivityLaunched) {
            return;
        }

        long crashStamp = getSharedPreferences(APP_PREFS, MODE_PRIVATE).getLong(CRASH_TIMESTAMP_KEY, 0);

        if(crashStamp == 0 || mSOSActivityLaunched) {
            return;
        }

        mSOSActivityLaunched = true;
        Intent intent = new Intent(getApplicationContext(), SOSActivity.class);
        startActivity(intent);
    }

    private void showNotificationConnectedMessageIfRequired() {
        boolean isNotificationConnected = getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).getBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, false);

        if(isNotificationConnected && !mNotificationConnectedMessageShown) {

            findViewById(R.id.notification_access_check_linear_layout).setVisibility(View.INVISIBLE);

            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.notification_connected));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            MainActivity.this.startActivity(intent);
            mNotificationConnectedMessageShown = true;
        }
    }

    private void refreshBatteryADCVoltageIfRequired() {
        if(NordicBleService.getConnectionState() == STATE_CONNECTED) {

            Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

            if(vehicle == null) {
                return;
            }
            if(vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {

                long batteryRefreshADCTimestamp = getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getLong(LAST_LIVE_PACKET_REFRESHED_TIMESTAMP, 0);

                //check if timestamp is greater than one minute
                long batteryRefreshTimeLapsed = System.currentTimeMillis() - batteryRefreshADCTimestamp;
                if(batteryRefreshTimeLapsed > LIVE_PACKET_REFRESH_TIME_THRESHOLD) {
                    NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLiveLogData();
                }
            }
        }
    }

    private void toggleInternetPopupOnNoConnection() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if(mInternetConnectionPopupWindow == null) {
            mInternetConnectionPopupWindow = new PopupWindow(mInternetConnectionPopupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        mInternetConnectionPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.RED));

        mInternetConnectionPopupWindow.setFocusable(false);

        mInternetConnectionPopupWindow.setOutsideTouchable(false);

        mInternetConnectionPopupWindow.update();

        if(connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()) {

            mInternetConnectionPopupWindow.dismiss();

        } else {
            mInternetConnectionPopupWindow.showAtLocation(findViewById(R.id.main_tab_holder_linear_layout), Gravity.BOTTOM, 0 , 0);
        }
    }

    private void fetchActiveBookingsForUser() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        LeaseWhereInput leaseWhereInput = LeaseWhereInput
                .builder()
                .asset(TxNodeWhereInput.builder().type(TxNodeType.VEHICLE).build())
                .status(LeaseStatus.ACTIVE)
                .build();

        FetchLeaseForUserQuery fetchLeaseForUserQuery = FetchLeaseForUserQuery.builder().leaseWhereInput(leaseWhereInput).build();

        apolloClient.query(fetchLeaseForUserQuery).enqueue(new ApolloCall.Callback<FetchLeaseForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull com.apollographql.apollo.api.Response<FetchLeaseForUserQuery.Data> response) {

                if(response.getData() != null && response.getData().lease() != null && response. getData().lease().getLeasesForUser() != null) {

                    List<FetchLeaseForUserQuery.GetLeasesForUser> fetchedLeaseForUserList = response.getData().lease().getLeasesForUser();

                    if(fetchedLeaseForUserList != null && !fetchedLeaseForUserList.isEmpty()) {
                        if(fetchedLeaseForUserList.get(0) != null) {

                            RentalVehicle rentalVehicle = new RentalVehicle();

                            /*if(fetchedLeaseForUserList.get(0).asset().type().equals(TxNodeType.VEHICLE)) {
                                rentalVehicle.setRentalAssetType(fetchedLeaseForUserList.get(0).asset().type());
                            }*/

                            if(fetchedLeaseForUserList.get(0).asset() != null && fetchedLeaseForUserList.get(0).asset().vehicle() != null) {

                                if(fetchedLeaseForUserList.get(0).asset().vehicle().vin() != null
                                        && !fetchedLeaseForUserList.get(0).asset().vehicle().vin().isEmpty()) {
                                    rentalVehicle.setVin(fetchedLeaseForUserList.get(0).asset().vehicle().vin());
                                }

                                if(fetchedLeaseForUserList.get(0).asset().vehicle().model() != null
                                        && fetchedLeaseForUserList.get(0).asset().vehicle().model().name() != null
                                        && !fetchedLeaseForUserList.get(0).asset().vehicle().model().name().isEmpty()) {

                                    RentalVehicleModel model = new RentalVehicleModel();
                                    model.setName(fetchedLeaseForUserList.get(0).asset().vehicle().model().name());

                                    rentalVehicle.setModel(model);
                                    //rentalVehicle.setModelName(fetchedLeaseForUserList.get(0).asset().vehicle().model().name());
                                }

                                if(fetchedLeaseForUserList.get(0).asset().vehicle().company() != null
                                        && fetchedLeaseForUserList.get(0).asset().vehicle().company().name() != null
                                        && !fetchedLeaseForUserList.get(0).asset().vehicle().company().name().isEmpty()) {

                                    RentalVehicleCompany company = new RentalVehicleCompany();
                                    company.setName(fetchedLeaseForUserList.get(0).asset().vehicle().company().name());

                                    rentalVehicle.setCompany(company);

                                    //rentalVehicle.setRentalProviderName(fetchedLeaseForUserList.get(0).asset().vehicle().company().name());
                                }
                            }

                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, new Gson().toJson(rentalVehicle)).apply();

                        } else {
                            getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
                        }
                    } else {
                        getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
                    }
                } else {
                    getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null).apply();
            }
        });
    }

    private void downloadVehicleModelImage() {
        //check if we have device details present
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(vehicle != null) {
            if(vehicle.getModel() != null) {
                NetworkUtils.getInstance(getApplicationContext()).downloadVehicleModelImageIfRequired(vehicle.getModel().getId());
            }
        }
    }

    private void toggleProgressDialog() {
        NetworkUtils networkUtils = NetworkUtils.getInstance(getApplicationContext());
        String completeFirmwareVersion = NordicBleService.getDeviceFirmwareVersion();
        String firmwareVersionToBeCompared = VersionUtils.removePatchVersion(completeFirmwareVersion);

        if(firmwareVersionToBeCompared == null && NordicBleService.getConnectionState() == STATE_CONNECTED) {
            hideProgressDialog();
            return;
        }


        if(networkUtils.isDeviceDataDownloadInProgress()) {
            showProgressDialog();
        } else if(NordicBleService.getConnectionState() == STATE_DISCONNECTED) {
            hideProgressDialog();
            toggleTabLayout(false);
        } else if(networkUtils.isReadDataDownloadInProgress()
                || networkUtils.isParamWriteDataDownloadInProgress()
                || networkUtils.isVehicleControlDataDownloadInProgress()
                || networkUtils.isLogDataDownloadInProgress()
                || networkUtils.isBatteryAdcLogDataDownloadInProgress()) {
            showProgressDialog();
        } else if(networkUtils.isReadDataDownloadComplete()
                    && networkUtils.isParamWriteDataDownloadComplete()
                    && networkUtils.isVehicleControlDataDownloadComplete()
                    && networkUtils.isLogDataDownloadComplete()) {
            if(firmwareVersionToBeCompared != null &&
                    !firmwareVersionToBeCompared.isEmpty() &&
                    new Version(firmwareVersionToBeCompared).equals(new Version(VER_1_0))) {
                hideProgressDialog();
            } else if(networkUtils.isBatteryAdcLogDataDownloadComplete()) {
                hideProgressDialog();
            }
        } else if(!networkUtils.isReadDataDownloadComplete()
                && !networkUtils.isParamWriteDataDownloadComplete()
                && !networkUtils.isVehicleControlDataDownloadComplete()
                && !networkUtils.isLogDataDownloadComplete()) {
            if(firmwareVersionToBeCompared != null &&
                    !firmwareVersionToBeCompared.isEmpty() &&
                    new Version(firmwareVersionToBeCompared).equals(new Version(VER_1_0))) {
                hideProgressDialog();
            } else if(!networkUtils.isBatteryAdcLogDataDownloadComplete()) {
                hideProgressDialog();
            }
        }
        //todo::handle failure scenario at granular level
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void refreshOBDFragmentIfRequired() {
        if(mAppInFocusLastKnownConnectionState == STATE_CONNECTED && NordicBleService.getConnectionState() != STATE_CONNECTED) {
            if(mViewPager.getAdapter() != null) {
                mViewPager.getAdapter().notifyDataSetChanged();
                mAppInFocusLastKnownConnectionState = NordicBleService.getConnectionState();
            }
        } else if((mAppInFocusLastKnownConnectionState == STATE_CONNECTING || mAppInFocusLastKnownConnectionState == STATE_DISCONNECTED)
                && NordicBleService.getConnectionState() == STATE_CONNECTED) {
            if(mViewPager.getAdapter() != null) {
                mViewPager.getAdapter().notifyDataSetChanged();
                mAppInFocusLastKnownConnectionState = NordicBleService.getConnectionState();
            }
        }
    }

    public void startGoogleMapsNavigation(String queryString) {
        String mapQueryString = "google.navigation:q=" + queryString + "&mode=d";

        Uri gmmIntentUri = Uri.parse(mapQueryString);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage(GOOGLE_MAPS_PACKAGE_NAME);
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mapIntent);
    }

    private void setupViews() {
        //set up notification access layout
        findViewById(R.id.main_toggle_notification_listener_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationAccessMessageFragment dialogFrag = NotificationAccessMessageFragment.newInstance();
                dialogFrag.setParentFab(findViewById(R.id.main_toggle_notification_listener_fab));
                dialogFrag.show(getSupportFragmentManager(), dialogFrag.getTag());
            }
        });

        mInternetConnectionPopupView = getLayoutInflater().inflate(R.layout.no_internet_connection_popup_layout, null);

        mActiveRentalBookingPopupView = getLayoutInflater().inflate(R.layout.rental_booking_in_progress_popup_layout, null);

        mActiveRentalBookingPopupView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActiveBookingActivity.class);
                startActivity(intent);
            }
        });
    }

    private void toggleNotificationWarningLayout() {
        boolean notificationListenerConnected = getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).getBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, false);
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

        if(notificationListenerConnected || vehicle == null || (vehicle != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) || mIsAppInGuestMode) {
            findViewById(R.id.main_toggle_notification_listener_fab).setVisibility(View.INVISIBLE);
            findViewById(R.id.notification_access_check_linear_layout).setVisibility(View.INVISIBLE);
        } else {

            if(findViewById(R.id.main_toggle_notification_listener_fab).getVisibility() == View.VISIBLE) {
                findViewById(R.id.notification_access_check_linear_layout).setVisibility(View.INVISIBLE);
            } else {
                findViewById(R.id.notification_access_check_linear_layout).setVisibility(View.VISIBLE);
            }

            //sometimes the notification listener takes some time to connect, wait for 10 seconds before we show the big ass layout
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (!hasWindowFocus()) {
                                    return;
                                }

                                if (!getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).getBoolean(NOTIFICATION_LISTENER_CONNECTED_KEY, false)) {
                                    findViewById(R.id.main_toggle_notification_listener_fab).setVisibility(View.VISIBLE);
                                    findViewById(R.id.notification_access_check_linear_layout).setVisibility(View.INVISIBLE);
                                }
                            }  catch (Exception e) {
                                FirebaseCrashlytics.getInstance().recordException(e);
                            }
                        }
                    });
                }
            }, 10000);
        }
    }

    private void logOutIfRequired() {
        boolean isLogOutFlagSet = getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).getBoolean(LOG_OUT_FLAG_KEY, false);

        if((hasWindowFocus() && isLogOutFlagSet)) {
            getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(LOG_OUT_FLAG_KEY, false).apply();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }
    }

    private boolean decideAndSelfDestruct() {
        boolean isKillSwitchSet = getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).getBoolean(KILL_SWITCH_FLAG_KEY, false);

        if(isKillSwitchSet) {
            finishAndRemoveTask();
        }

        return isKillSwitchSet;
    }

    private void startBleServiceIfRequired() {
        //if we're in guest mode then just return
        if(mIsAppInGuestMode) {
            return;
        }
        long lastActiveTSFromBleService = getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getLong(BLUETOOTH_SERVICE_LAST_ACTIVE_TIMESTAMP, 0);

        if(Math.abs(System.currentTimeMillis() - lastActiveTSFromBleService) > 3000) {
            //the timestamp is definitely stale, double check if the service is running just in case
            if(!isServiceRunning(NordicBleService.class)) {
                ContextCompat.startForegroundService(getApplicationContext(), mBLEServiceIntent);
            }
        }
    }

    private boolean checkForGPS() {
        //check if GPS is on
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(locationManager == null) {

            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.location_manager_null));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            MainActivity.this.startActivity(intent);

            finish();
            return false;
        }
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void checkAndShowLocationSettingsDialog() {
        if(mIsLocationRequestInProgress) {
            return;
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient
        builder.setNeedBle(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                mIsLocationRequestInProgress = true;
                                resolvable.startResolutionForResult(MainActivity.this, REQUEST_CHECK_LOCATION_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });

    }

    private void startJob() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if(jobScheduler == null) {
            return;
        }

        ComponentName serviceComponent = new ComponentName(getApplicationContext(), NetworkJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(14, serviceComponent);
        builder.setMinimumLatency(10 * 1000); // wait at least
        builder.setOverrideDeadline(15 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not

        jobScheduler.schedule(builder.build());
    }

    private void setupLocalUserCopy() {
        mUser = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();
        if(mUser == null) {
            //there has been an error, this situation should not occur
            finish();

            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_user_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            MainActivity.this.startActivity(intent);
        }
    }

    private void setupTheme() {
        SharedPreferences sharedPreferences = getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE);
        mAutoSwitchTheme = sharedPreferences.getBoolean(DAY_NIGHT_AUTO_SWITCH_THEME_KEY, true);
        if(mAutoSwitchTheme) {
            mCurrentThemeID = R.style.AppFullScreenTheme;
            setTheme(mCurrentThemeID);
            return;
        }

        boolean useDarkTheme = sharedPreferences.getBoolean(USE_DARK_THEME_KEY, false);
        if(useDarkTheme) {
            mCurrentThemeID = R.style.AppFullScreenThemeDark;
            setTheme(mCurrentThemeID);
            return;
        }

        mCurrentThemeID = R.style.AppFullScreenTheme;
        setTheme(mCurrentThemeID);
    }

    private void switchThemeIfRequired() {
        if(!hasWindowFocus()) {
            return;
        }

        if(!mAutoSwitchTheme) {
            SharedPreferences sharedPreferences = getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE);
            boolean useDarkTheme = sharedPreferences.getBoolean(USE_DARK_THEME_KEY, false);

            if(useDarkTheme && (mCurrentThemeID != R.style.AppFullScreenThemeDark)) {
                //ignore this warning by android studio, it's smart but not smart enough
                //the code works, there're still a couple of decades left before the machines take over
                restartActivityForThemeChange(useDarkTheme);
                return;
            }

            if(!useDarkTheme && (mCurrentThemeID != R.style.AppFullScreenTheme)) {
                //ignore this warning by android studio, it's smart but not smart enough
                //the code works, there're still a couple of decades left before the machines take over
                restartActivityForThemeChange(useDarkTheme);
            }
        } else {
            //check if light sensor is available or not
            if(!getSharedPreferences(APP_PREFS, MODE_PRIVATE).getBoolean(LIGHT_SENSOR_AVAILABLE, false)) {
                return;
            }

            long currentTimeStamp = System.currentTimeMillis();
            if(mCurrentThemeID != R.style.AppFullScreenTheme
                    && mLastSwitchToLightTimeStamp != 0
                    && mLastSwitchToLightTimeStamp >= mLastSwitchToDarkTimeStamp
                    && (currentTimeStamp - mLastSwitchToLightTimeStamp >= MIN_SWITCH_PERIOD)) {
                restartActivityForThemeChange(false);
            }

            if(mCurrentThemeID != R.style.AppFullScreenThemeDark
                    && mLastSwitchToDarkTimeStamp != 0
                    && mLastSwitchToDarkTimeStamp >= mLastSwitchToLightTimeStamp
                    && (currentTimeStamp - mLastSwitchToDarkTimeStamp >= MIN_SWITCH_PERIOD)) {
                restartActivityForThemeChange(true);
            }
        }
    }

    private void restartActivityForThemeChange(boolean useDarkTheme) {
        mOneSecondTimer.cancel();
        mOneSecondTimer.purge();


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra(OVERRIDE_SETUP_THEME, true);
        intent.putExtra(USE_DARK_THEME, useDarkTheme);
        intent.putExtra(DEFAULT_TAB, mTabLayout.getSelectedTabPosition());
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    public void toggleTabLayout(boolean hideTabLayout) {

        if(mResizeTabLayoutAnimation == null || !mResizeTabLayoutAnimation.hasStarted() || mResizeTabLayoutAnimation.hasEnded()) {

            if(hideTabLayout) {
                if(mTabLayout.getVisibility() == View.VISIBLE) {
                    resizeNavBannerWithAnimation(false);
                }
            } else {
                if(mTabLayout.getVisibility() == View.GONE || mTabLayout.getVisibility() == View.INVISIBLE) {
                    resizeNavBannerWithAnimation(true);
                }
            }
        }
    }

    private void resizeNavBannerWithAnimation(final boolean expandBanner) {
        //todo::decide on whether to block swipe or not
        float startHeight;
        float endHeight;

        startHeight = mTabLayout.getHeight();

        if(expandBanner) {
            endHeight = TAB_LAYOUT_MAX_HEIGHT_IN_DP * getScreenDensity();
        } else {
            endHeight = TAB_LAYOUT_MIN_HEIGHT_IN_DP;
        }

        mResizeTabLayoutAnimation = new ResizeAnimation(mTabLayout,
                mTabLayout.getWidth(), startHeight,
                mTabLayout.getWidth(), endHeight);

        mResizeTabLayoutAnimation.setInterpolator(new AccelerateInterpolator());
        mResizeTabLayoutAnimation.setDuration(500);
        mTabLayout.setAnimation(mResizeTabLayoutAnimation);
        mTabLayout.startAnimation(mResizeTabLayoutAnimation);

        mResizeTabLayoutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if(expandBanner) {
                    mTabLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(expandBanner) {

                    mTabLayout.setVisibility(View.VISIBLE);
                } else {
                    mTabLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private float getScreenDensity() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        return getResources().getDisplayMetrics().density;
    }

    private void setupSpeechRecognizer() {
        mVoiceCommandImageView = (ImageView) findViewById(R.id.main_speech_recognizer_image_view);
        mVoiceCommandImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startSpeechRecognizer();
            }
        });
    }

    private void startSpeechRecognizer() {

        mVoiceCommandImageView.setEnabled(false);

        Intent intent = new Intent(getApplicationContext(), GoogleSpeechRecognizerActivity.class);
        startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);

    }

    public void setupDrawerLayoutBasedOnAppMode() {
        if(mIsAppInGuestMode) {
            setupGuestDrawerLayout();
        } else {
            setupDrawerLayout();
        }
    }

    public void setupGuestDrawerLayout() {
        isHamburgerSet = true;

        mHamBurgerMenuImageView = (ImageView) findViewById(R.id.main_hamburger_menu_image_view);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);

        //drawer layout data
        ArrayList<String> drawerListStringData = new ArrayList<>();

        //setup the drawer layout
        DrawerListAdapter adapter = new DrawerListAdapter(MainActivity.this, drawerListStringData);

        final ListView navList = (ListView) findViewById(R.id.main_drawer_list_view);
        mNavListHeader = getLayoutInflater().inflate(R.layout.guest_nav_drawer_header, navList, false);
        mNavListFooter = getLayoutInflater().inflate(R.layout.nav_drawer_footer, navList, false);

        //populate app version number into drawer footer
        populateCurrentAppVersion(mNavListFooter.findViewById(R.id.version_number_value_text_view));

        if(mNavHeaderFooterFlag) {
            navList.setAdapter(adapter);
        } else {
            navList.addHeaderView(mNavListHeader);
            navList.setAdapter(adapter);
            navList.addFooterView(mNavListFooter);
            mNavHeaderFooterFlag = true;
        }


        mNavListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //slightly delay the hiding of drawer, else it gives an appearance of a lag
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mDrawerLayout.closeDrawer(navList);
                        mNavListHeader.setEnabled(true);
                    }
                }, 200);
                mNavListHeader.setEnabled(false);
                exitGuestMode();
            }
        });

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        mHamBurgerMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    if(isHamburgerSet) {
                        mDrawerLayout.openDrawer(navList);
                    } else {
                        setHamburgerIcon();
                    }
                }
            }
        });

    }

    private void exitGuestMode() {
        Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
        MainActivity.this.startActivity(myIntent);
        finish();
    }

    public void showRegisterAsUserActivity() {
        Intent myIntent = new Intent(MainActivity.this, RegisterAsUserActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void setupDrawerLayout() {
        isHamburgerSet = true;

        mHamBurgerMenuImageView = (ImageView) findViewById(R.id.main_hamburger_menu_image_view);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);

        //drawer layout data
        ArrayList<String> drawerListStringData = new ArrayList<>();
        drawerListStringData.add(getString(R.string.my_vehicle));
        drawerListStringData.add(getString(R.string.service_request));
        drawerListStringData.add(getString(R.string.settings));
        Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();
        Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        if(vehicle != null) {
            if(vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                drawerListStringData.add(getString(R.string.geo_fencing));
            } else if(device != null && device.getFirmware() != null && new Version(device.getFirmware()).compareTo(new Version(VER_1_1)) > 0) {
                Timber.e("Device firmware " + device.getFirmware());
                drawerListStringData.add(getString(R.string.geo_fencing));
            }
        }
        if(false && getSharedPreferences(APP_PREFS, MODE_PRIVATE).getBoolean(IS_APP_IN_INDIA_KEY, false)) {
            drawerListStringData.add(getString(R.string.bills_and_payments));
        }

        drawerListStringData.add(getString(R.string.long_term_rental));
        drawerListStringData.add(getString(R.string.help));

        if(mUser != null && mUser.getRole() == Role.DEV) {
            drawerListStringData.add(getString(R.string.diagnostics));
            drawerListStringData.add(getString(R.string.ota));
            drawerListStringData.add("HEX");
            drawerListStringData.add("Color OTA");
        }

        //setup the drawer layout
        DrawerListAdapter adapter = new DrawerListAdapter(MainActivity.this, drawerListStringData);

        final ListView navList = (ListView) findViewById(R.id.main_drawer_list_view);
        mNavListHeader = getLayoutInflater().inflate(R.layout.nav_drawer_header, navList, false);
        mNavListFooter = getLayoutInflater().inflate(R.layout.nav_drawer_footer, navList, false);

        //populate app version number into drawer footer
        populateCurrentAppVersion(mNavListFooter.findViewById(R.id.version_number_value_text_view));

        //load profile pic into navigation drawer header
        loadProfilePicIntoView((CircleImageView)(mNavListHeader.findViewById(R.id.drawer_circle_image_view)));

        if(mNavHeaderFooterFlag) {
            navList.setAdapter(adapter);
        } else {
            navList.addHeaderView(mNavListHeader);
            navList.setAdapter(adapter);
            navList.addFooterView(mNavListFooter);
            mNavHeaderFooterFlag = true;
        }

        navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int pos, long id) {
                mDrawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        super.onDrawerClosed(drawerView);

                    }
                });

                //slightly delay the hiding of drawer, else it gives an appearance of a lag
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mDrawerLayout.closeDrawer(navList);
                        navList.setEnabled(true);
                    }
                }, 200);

                navList.setEnabled(false);

                if(id < 0) {
                    navList.setEnabled(true);
                    return;
                }

                String menuString = drawerListStringData.get((int)id);

                if(menuString.equals(getString(R.string.my_vehicle))) {
                    launchVehicleDetailsActivity();
                } else if(menuString.equals(getString(R.string.diagnostics))) {
                    launchDiagnosticsActivity();
                } else if(menuString.equals(getString(R.string.service_request))) {
                    launchServiceRequestActivity();
                } else if(menuString.equals(getString(R.string.settings))) {
                    launchSettingsActivity();
                } else if(menuString.equals(getString(R.string.geo_fencing))) {
                    launchGeoFencingActivity();
                } else if(menuString.equals(getString(R.string.bills_and_payments))) {
                    launchInvoiceActivity();
                } else if(menuString.equals(getString(R.string.long_term_rental))) {
                    launchLongTermRentalActivity();
                } else if(menuString.equals(getString(R.string.ota))) {
                    launchOtaActivity();
                } else if(menuString.equals("HEX")) {
                    launchHexActivity();
                } else if(menuString.equals(getString(R.string.help))) {
                    openHelpUrl();
                } else if(menuString.equals("Color OTA")) {
                    launchColorOtaActivity();
                }
            }
        });

        mNavListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //slightly delay the hiding of drawer, else it gives an appearance of a lag
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mDrawerLayout.closeDrawer(navList);
                        mNavListHeader.setEnabled(true);
                    }
                }, 200);
                mNavListHeader.setEnabled(false);
                launchUserProfileActivity();
            }
        });

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        mHamBurgerMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    if(isHamburgerSet) {
                        mDrawerLayout.openDrawer(navList);
                    } else {
                        setHamburgerIcon();
                    }
                }
            }
        });
    }

    private void openHelpUrl() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.help_url)));
        startActivity(browserIntent);
    }

    private void launchColorOtaActivity() {
        Intent intent = new Intent(getApplicationContext(), StmOtaActivity.class);
        startActivity(intent);
    }

    private void launchHexActivity() {
        Intent myIntent = new Intent(MainActivity.this, HexFileGeneratorActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchOtaActivity() {
        Intent myIntent = new Intent(MainActivity.this, DevOtaActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchInvoiceActivity() {
        Intent myIntent = new Intent(MainActivity.this, InvoiceActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchLongTermRentalActivity() {
        Intent myIntent = new Intent(MainActivity.this, RentalActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void populateCurrentAppVersion(TextView versionNoTextView) {

        if(versionNoTextView == null) {
            return;
        }

        String versionName = BuildConfig.VERSION_NAME;

        versionNoTextView.setText(versionName);
    }

    private void loadProfilePicIntoView(CircleImageView view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (view == null) {
                        return;
                    }

                    if (getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, null) != null) {
                        //load user profile pic
                        String profilePicFileName = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_PROFILE_LOCAL_FILE_PATH_KEY, null);
                        if (profilePicFileName != null) {
                            File profilePicFile = new File(profilePicFileName);
                            if (profilePicFile.exists()) {
                                Picasso.with(getApplicationContext()).load(profilePicFile).fit().centerInside().into(view);
                            }
                        } else {
                            //request networking service to download pic
                            NetworkUtils.getInstance(getApplicationContext()).downloadProfilePicIfRequired();
                            //todo::change image to spinning icon
                        }
                    } else {
                        view.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_asset_avatar));
                    }
                } catch (Exception e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            }
        });
    }

    private void toggleHamburgerIconIfRequired() {
        if(mHamBurgerMenuImageView == null)
            return;

        //we are at our home tab, hide the hamburger icon if google maps notification is visible
        if(mTabLayout.getSelectedTabPosition() == 1) {
            long lastGoogleNotificationTimestamp = getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getLong(LAST_GOOGLE_NOTIFICATION_TIMESTAMP, 0);

            if(System.currentTimeMillis() - lastGoogleNotificationTimestamp <= 1500) {
                mHamBurgerMenuImageView.setVisibility(View.INVISIBLE);
            } else {
                mHamBurgerMenuImageView.setVisibility(View.VISIBLE);
            }
        } else {
            mHamBurgerMenuImageView.setVisibility(View.VISIBLE);
        }
    }

    private void toggleMicIfRequired() {
        if(mTabLayout == null) {
            return;
        }

        if(mTabLayout.getSelectedTabPosition() == POS_ANALYTICS_TAB) {
            findViewById(R.id.main_speech_recognizer_image_view).setVisibility(View.INVISIBLE);
        } else {
            findViewById(R.id.main_speech_recognizer_image_view).setVisibility(View.VISIBLE);
        }
    }

    private void setHamburgerIcon() {
        isHamburgerSet = true;

        TypedArray a = getApplicationContext().obtainStyledAttributes(mCurrentThemeID, new int[]{R.attr.drawableHamburgerIcon});
        int attributeResourceId = a.getResourceId(0, 0);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), attributeResourceId);
        a.recycle();
        if(mHamBurgerMenuImageView != null) {
            mHamBurgerMenuImageView.setImageDrawable(drawable);
        }
    }

    private class BuildContactListTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            EventBus.getDefault().post(new EventBusMessage(PHONE_EVENT_CONTACT_LIST_BUILDING));
        }

        @Override
        protected String doInBackground(String... params) {
            PhoneUtils.getInstance(getApplicationContext()).buildContactList();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            EventBus.getDefault().post(new EventBusMessage(PHONE_EVENT_CONTACT_LIST_BUILT));
        }
    }

    private void launchSettingsActivity() {
        Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchDiagnosticsActivity() {
        Intent myIntent = new Intent(MainActivity.this, DiagnosticsActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchUserProfileActivity() {
        Intent myIntent = new Intent(MainActivity.this, UserProfileActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchVehicleDetailsActivity() {
        Intent myIntent = new Intent(MainActivity.this, VehiclesAndDevicesListActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchServiceRequestActivity() {
        Intent myIntent = new Intent(MainActivity.this, ServiceRequestActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void launchGeoFencingActivity() {
        Intent intent = new Intent(MainActivity.this, GeoFencingActivity.class);
        MainActivity.this.startActivity(intent);
    }

    private void launchSetPinActivityIfRequired() {
        if(NordicBleService.getConnectionState() != STATE_CONNECTED) {
            return;
        }
        if(mBluetoothEnterPinActivityLaunched || mSetPinActivityLaunched || mSOSActivityLaunched || mChangePinActivityLaunched || mPlugAndPlayEnterPinActivityLaunched) {
            return;
        } else {
            mSetPinActivityLaunched = true;
        }
        Intent myIntent = new Intent(MainActivity.this, SetPinActivity.class);
        MainActivity.this.startActivityForResult(myIntent, REQ_CODE_SET_PIN);
    }

    private void launchChangePinActivity(String vin, String phone, boolean clearPrefs){

        if(mBluetoothEnterPinActivityLaunched || mSetPinActivityLaunched || mSOSActivityLaunched || mChangePinActivityLaunched || mPlugAndPlayEnterPinActivityLaunched) {
            return;
        } else {
            mChangePinActivityLaunched = true;
        }

        //clear prefs as the user is connecting to the app for the first time after it being sold
        if(clearPrefs) {
            clearDeviceAndVehiclePrefs();
        }

        Intent intent = new Intent(MainActivity.this, ChangePinActivity.class);
        intent.putExtra(VIN_ARG_KEY, vin);
        intent.putExtra(PHONE_ARG_KEY, phone);
        intent.putExtra(RECONNECT_TO_DEVICE_ARG_KEY, true);
        MainActivity.this.startActivity(intent);
    }

    private void clearDeviceAndVehiclePrefs() {
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
    }

    private void startServices() {
        mBLEServiceIntent = new Intent(getApplicationContext(), NordicBleService.class);

        //register the service after permissions


        //start ble service as foreground service
        if(!isServiceRunning(NordicBleService.class)) {
            ContextCompat.startForegroundService(getApplicationContext(), mBLEServiceIntent);
        }
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if(manager == null) {
            return false;
        }
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if(serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void setupSlidingTabLayout(int defaultTab) {
        //initialize the drawable id
        TypedArray a = getTheme().obtainStyledAttributes(mCurrentThemeID, new int[]{R.attr.drawableNavigationIcon,
                R.attr.drawableOBDIcon,
                R.attr.drawableAnalyticsIcon});
        drawablesIds = new int[]{a.getResourceId(0, 0), a.getResourceId(1, 0), a.getResourceId(2, 0)};

        //Initializing the tablayout
        mTabLayout = (TabLayout) findViewById(R.id.main_tab_layout);

        //Adding the tabs using addTab() method
        for (int tabIndex = 0; tabIndex < mTitles.length; ++tabIndex) {
            View customView = getLayoutInflater().inflate(R.layout.tab_custom_view, null);
            customView.findViewById(R.id.icon).setBackgroundResource(drawablesIds[tabIndex]);
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(customView));

            //mTabLayout.addTab(mTabLayout.newTab().setIcon(drawablesIds[tabIndex]));
        }

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        mViewPager = (ViewPager) findViewById(R.id.main_view_pager);
        mViewPager.setOffscreenPageLimit(3);

        //Creating our pager adapter
        MainActivityViewPagerAdapter adapter = new MainActivityViewPagerAdapter(getSupportFragmentManager(), mTitles, mTabLayout.getTabCount(), drawablesIds, MainActivity.this);

        //Adding adapter to pager
        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //Adding onTabSelectedListener to swipe views
        mTabLayout.addOnTabSelectedListener(this);

        //setting up the tab indicator color
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorSelectedTabIndicator, typedValue, true);
        @ColorInt int selectedTabIndicatorColor = typedValue.data;
        mTabLayout.setSelectedTabIndicatorColor(selectedTabIndicatorColor);


        //setting the obd tab as default
        mTabLayout.getTabAt(defaultTab).select();

        //set the vertical divider between the tabs
        View root = mTabLayout.getChildAt(0);
        if(root instanceof LinearLayout) {
            theme.resolveAttribute(R.attr.colorTabDivider, typedValue, true);
            @ColorInt int tabDividerColor = typedValue.data;

            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(tabDividerColor);
            drawable.setSize(2, 1);
            //((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showPermissionRequestExplanation(String title, String message, final String permission, final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, permissionRequestCode));
        builder.create().show();
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(PHONE_EVENT_CONTACT_LIST_BUILDING)) {
            Timber.d("building message received");
        } else if(event.message.equals(PHONE_EVENT_CONTACT_LIST_BUILT)) {
            Timber.d("built message received");
            mContactList = PhoneUtils.getInstance(getApplicationContext()).getContactList();
            getSharedPreferences(GENERAL_PREFS, MODE_PRIVATE).edit().putBoolean(CONTACT_IN_ACTION_KEY, true).apply();
        } else if(event.message.equals(GEN_EVENT_CHANGE_HAMBURGER_TO_BACK)) {
            mHamBurgerMenuImageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_back_white));
            isHamburgerSet = false;
        } else if(event.message.equals(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED)) {
            toggleHamburgerIconIfRequired();
        } else if(event.message.contains(GEN_EVENT_NAV_BANNER_ANIM_UPDATED)) {
            toggleHamburgerIconIfRequired();
        } else if(event.message.equals(NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED)) {
            loadProfilePicIntoView((mNavListHeader.findViewById(R.id.drawer_circle_image_view)));
        } else if(event.message.equals(BT_EVENT_STATE_CONNECTED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mHasOTAMessageBeenShown = false;
                        if (!hasWindowFocus()) {
                            return;
                        }
                        mAppInFocusLastKnownConnectionState = NordicBleService.getConnectionState();
                        if (mViewPager.getAdapter() != null) {
                            mViewPager.getAdapter().notifyDataSetChanged();
                        }
                        setupDrawerLayoutBasedOnAppMode();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });

        } else if(event.message.equals(BT_EVENT_STATE_DISCONNECTED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!hasWindowFocus()) {
                            return;
                        }
                        mAppInFocusLastKnownConnectionState = NordicBleService.getConnectionState();
                        if (mViewPager.getAdapter() != null) {
                            mViewPager.getAdapter().notifyDataSetChanged();
                        }
                        setupDrawerLayoutBasedOnAppMode();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(BT_EVENT_SWITCH_ON_BLUETOOTH)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!hasWindowFocus()) {
                            return;
                        }

                        Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
                        bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                        intent.putExtras(bundle);
                        MainActivity.this.startActivity(intent);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }

                }
            });
        } else if(event.message.equals(BT_EVENT_DEVICE_START_ACTIVATION_PROCEDURE)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!hasWindowFocus()) {
                            return;
                        }
                        launchSetPinActivityIfRequired();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(BT_EVENT_CHANGE_PIN_REQUIRED)) {
            String vin = event.message.split(GEN_DELIMITER)[1];
            String phone = event.message.split(GEN_DELIMITER)[2];

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try{
                        if(!hasWindowFocus()) {
                            return;
                        }
                        launchChangePinActivity(vin, phone, true);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(KILL_SWITCH)) {
            finishAndRemoveTask();
        } else if(event.message.contains(BT_EVENT_NO_PIN_FOUND) || event.message.contains(BT_EVENT_WRONG_PIN)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!hasWindowFocus()) {
                            return;
                        }
                        launchBluetoothEnterPinActivityIfRequired();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT)) {
            String message = event.message.split(GEN_DELIMITER)[1];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(GENERIC_MESSAGE_ACTIVITY_PERSISTENT_ERROR_EVENT)) {
            String message = event.message.split(GEN_DELIMITER)[1];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(GENERIC_MESSAGE_ACTIVITY_INFO_EVENT)) {
            String message = event.message.split(GEN_DELIMITER)[1];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT)) {
            String message = event.message.split(GEN_DELIMITER)[1];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                        bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(NETWORK_EVENT_DEVICE_DATA_REFRESHED)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showOTAAvailableMessageIfRequired();
                }
            });
        }
    }

    private void showOTAAvailableMessageIfRequired() {
        if(!hasWindowFocus()) {
            return;
        }

        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

        if(cloudDevice == null) {
            return;
        }

        if(cloudDevice.getDeviceUpdateStatus() == null) {
            return;
        }

        if(cloudDevice.getDeviceUpdateStatus() == DeviceUpdateStatus.UPDATE_REQUIRED && !mHasOTAMessageBeenShown && cloudDevice.getExpectedFirmware() != null) {
            launchOTAMessageActivity(cloudDevice.getExpectedFirmware(), cloudDevice.getDeviceId());
        }
    }

    private void launchOTAMessageActivity(String expectedFirmwareVersion, String deviceId) {
        mHasOTAMessageBeenShown = true;
        Intent intent = new Intent(MainActivity.this, OTAMessageActivity.class);
        intent.putExtra(OtaActivity.OTA_VERSION_KEY, expectedFirmwareVersion);
        intent.putExtra(OtaActivity.OTA_DEVICE_ID_KEY, deviceId);
        MainActivity.this.startActivity(intent);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition(), true);
        //broadcast tab change for fragments to listen
        EventBus.getDefault().post(new EventBusMessage(GEN_EVENT_TAB_CHANGE + GEN_DELIMITER + tab.getPosition()));

        setHamburgerIcon();

        toggleHamburgerIconIfRequired();

        toggleMicIfRequired();

        if(tab.getPosition() == POS_ANALYTICS_TAB) {
            NetworkUtils.getInstance(getApplicationContext()).refreshTripData();
            NetworkUtils.getInstance(getApplicationContext()).refreshVehicleHistoricalData();
        }

        if(tab.getPosition() == POS_OBD_TAB) {
            Vehicle vehicle = PrefUtils.getInstance(getApplicationContext()).getVehicleObjectFromPrefs();

            if(vehicle != null && vehicle.getModel() != null
                    && vehicle.getModel().getProtocol() != null
                    && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                //update live log data
                NetworkUtils.getInstance(getApplicationContext()).refreshVehicleLiveLogData();


                //update device data
                Device device = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();

                if(device != null && device.getDeviceId() != null) {
                    NetworkUtils.getInstance(getApplicationContext()).refreshDeviceData(device.getDeviceId());
                }

                //connect to tbit device if required
                if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT));
                } else if(device != null && device.getDeviceType() != null && device.getDeviceType().equals(DeviceType.ICONCOX)) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX));
                }
            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //broadcast tab change for fragments to listen
        EventBus.getDefault().post(new EventBusMessage(GEN_EVENT_TAB_CHANGE + GEN_DELIMITER + tab.getPosition()));
    }

    @Override
    public void onResume() {
        super.onResume();

        mSetPinActivityLaunched = mSOSActivityLaunched = mBluetoothEnterPinActivityLaunched = mChangePinActivityLaunched = mPlugAndPlayEnterPinActivityLaunched = false;

        //setup the light sensor
        setupLightSensor();

        SharedPreferences sharedPreferences = getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE);
        mAutoSwitchTheme = sharedPreferences.getBoolean(DAY_NIGHT_AUTO_SWITCH_THEME_KEY, true);

        switchThemeIfRequired();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //update the nav list header
        loadProfilePicIntoView((CircleImageView) (mNavListHeader.findViewById(R.id.drawer_circle_image_view)));
        if(mNavListHeader != null) {
            mNavListHeader.setEnabled(true);
        }

        if(findViewById(R.id.main_drawer_list_view) != null) {
            findViewById(R.id.main_drawer_list_view).setEnabled(true);
        }

        if(mVoiceCommandImageView != null) {
            mVoiceCommandImageView.setEnabled(true);
        }

        setupDrawerLayoutBasedOnAppMode();

        refreshFragments();

        refreshBatteryADCVoltageIfRequired();

        //check in any active Rental bookings
        fetchActiveBookingsForUser();

        //check
        String rentalActiveBookingStr = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);
        RentalVehicle rentalVehicle = new Gson().fromJson(rentalActiveBookingStr, RentalVehicle.class);
        if(rentalVehicle != null && rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {
            mViewPager.setCurrentItem(1, true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        performExitCleanup();
        super.onDestroy();
    }

    private void performExitCleanup() {
        if(mOneSecondTimer != null) {
            mOneSecondTimer.cancel();
            mOneSecondTimer.purge();
        }
    }

    private ArrayList<Contact> findClosestMatch(String queryContactName) {
        //final list that we return, restrict it's size to 4 elements max
        ArrayList<Contact> matchedContactList = new ArrayList<>();

        //fuzzy score list which helps us out with sorting
        ArrayList<Pair<Contact, Integer>> fuzzyScoreList = new ArrayList<>();

        boolean flag_partial_match = false;

        if(mContactList != null) {
            for (int contactIndex = 0; contactIndex < mContactList.size(); ++contactIndex) {
                Contact currentContact = mContactList.get(contactIndex);
                //convert this name to lower case
                String currentContactName = currentContact.getName().toLowerCase();

                //check if we have full match
                if(currentContactName.equals(queryContactName)) {
                    //We've found the chosen one!
                    //clear all results and return this one
                    matchedContactList.clear();
                    matchedContactList.add(currentContact);
                    return matchedContactList;
                }

                //get fuzzy score for this comparison
                int fuzzyScore = StringUtils.getFuzzyDistance(currentContactName, queryContactName, Locale.ENGLISH);

                //check for partial matches
                //first break down the name into separate words/tokens
                String[] currentNameTokens = currentContactName.split(" ");
                String[] queryNameTokens = queryContactName.split(" ");

                //check if any of the tokens match, we could use a hashmap here, but since our sample
                //size is so small, I'm going ahead with a double loop here
                boolean flag_token_found = false;
                for (int currentTokenIndex = 0; (currentTokenIndex < currentNameTokens.length) && !flag_token_found; ++currentTokenIndex) {
                    for (int queryTokenIndex = 0; (queryTokenIndex < queryNameTokens.length) && !flag_token_found; ++queryTokenIndex) {
                        if(currentNameTokens[currentTokenIndex].equals(queryNameTokens[queryTokenIndex])) {

                            //if there wasn't any partial match before then clear the list
                            if(!flag_partial_match) {
                                fuzzyScoreList.clear();
                            }
                            fuzzyScoreList.add(new Pair<>(currentContact, fuzzyScore));

                            //we have a token match
                            flag_token_found = true;
                            //we have a partial match
                            flag_partial_match = true;
                        }
                    }
                }

                //we don't have a partial match yet
                if(!flag_partial_match) {
                    fuzzyScoreList.add(new Pair<>(currentContact, fuzzyScore));
                }
            }
        }

        //we have the fuzzyScores for every name, sort the list in descending order of fuzzy score
        //higher the score, greater the similarity
        Collections.sort(fuzzyScoreList, new Comparator<Pair<Contact, Integer>>() {
            @Override
            public int compare(Pair<Contact, Integer> o1, Pair<Contact, Integer> o2) {
                return o2.second - o1.second;
            }
        });

        matchedContactList.clear();
        for (int fuzzyListIndex = 0; fuzzyListIndex < fuzzyScoreList.size(); ++fuzzyListIndex) {
            matchedContactList.add(fuzzyScoreList.get(fuzzyListIndex).first);
            if(matchedContactList.size() > 4) {
                break;
            }
        }

        return matchedContactList;
    }

    private void startCallSequence(ArrayList<Contact> matchedContactsResult) {

        if (matchedContactsResult.size() == 1 && matchedContactsResult.get(0).getPhoneNumbers().size() == 1) {
            //make the call right away
            PhoneUtils.getInstance(MainActivity.this).callNumber(matchedContactsResult.get(0).getPhoneNumbers().get(0));
        } else {
            //present user with a list view of the available options
            Intent intent = new Intent(MainActivity.this, PickContactToCallActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(PHONE_EVENT_MULTIPLE_MATCHED_RESULTS, new Gson().toJson(matchedContactsResult));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mBluetoothEnterPinActivityLaunched = mSetPinActivityLaunched = mSOSActivityLaunched = mChangePinActivityLaunched = mPlugAndPlayEnterPinActivityLaunched = false;

        if(requestCode == REQ_CODE_SPEECH_INPUT) {
            if(resultCode == Activity.RESULT_OK && data != null) {

                //if app is in guest mode then show register as user message and return
                if(mIsAppInGuestMode) {
                    showRegisterAsUserActivity();
                    return;
                }

                ArrayList<String> results = data.getStringArrayListExtra(SPEECH_RECOGNITION_RESULT);

                if(!results.isEmpty()) {

                    if (results.get(0).toLowerCase().contains(getString(R.string.call_number_hot_word))) {

                        String result = results.get(0);
                        String strippedResult = result.replace(getString(R.string.call_number_hot_word), "");
                        mMatchedContactsList = findClosestMatch(strippedResult.toLowerCase());

                        if (mMatchedContactsList.isEmpty()) {

                            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_match_found));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            MainActivity.this.startActivity(intent);

                        } else {

                            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                //request permission
                                showPermissionRequestExplanation(getString(R.string.make_call_confirmation_title), getString(R.string.make_call_confirmation_message), Manifest.permission.CALL_PHONE, REQUEST_PHONE_CALL);

                            } else {

                                //make a call right away or show matching contact result list
                                startCallSequence(mMatchedContactsList);
                            }

                        }

                    } else if (results.get(0).toLowerCase().contains(getString(R.string.navigate_to_hot_word))) {
                        String strippedResult = results.get(0).replace(getString(R.string.navigate_to_hot_word), "");

                        mViewPager.setCurrentItem(POS_NAV_TAB);

                        if (strippedResult.trim().isEmpty()) {

                            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.places_try_again));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            MainActivity.this.startActivity(intent);

                        } else {
                            if (mProgressDialog == null) {
                                mProgressDialog = new ProgressDialog(MainActivity.this);
                            }
                            mProgressDialog.setTitle(getString(R.string.please_wait_fetching_location));
                            mProgressDialog.show();

                            getPlaceAutoCompletePredictions(strippedResult);
                        }
                    } else {

                        Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_voice_search_command));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        MainActivity.this.startActivity(intent);

                    }
                }
            }
        } else if(requestCode == REQUEST_CHECK_LOCATION_SETTINGS) {
            mIsLocationRequestInProgress = false;
            switch (resultCode) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    break;
                case Activity.RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    checkAndShowLocationSettingsDialog();
                    break;
                default:
                    break;
            }
        } else if(requestCode == REQ_CODE_SET_PIN) {
            if(resultCode == RESULT_CANCELED) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));
            }
        }
    }

    public void getPlaceAutoCompletePredictions(String query) {
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setSessionToken(token)
                .setQuery(query)
                .build();

        PlacesClient placesClient = Places.createClient(this);

        placesClient.findAutocompletePredictions(request).addOnSuccessListener((response) -> {
            if(mProgressDialog != null)
                if(mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            processAutoCompleteApiResponse(response.getAutocompletePredictions());
        }).addOnFailureListener((exception) -> {
            if(mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.places_no_response_try_again));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            MainActivity.this.startActivity(intent);

        });

    }

    public void refreshFragments() {
        if(mViewPager.getAdapter() != null) {
            mViewPager.getAdapter().notifyDataSetChanged();
        }
    }

    private void processAutoCompleteApiResponse(List<AutocompletePrediction> autocompletePredictions){

        mAutoCompletePredictions = autocompletePredictions;

        if(mAutoCompletePredictions != null && !mAutoCompletePredictions.isEmpty()) {
            mNavigateToOnVoiceResultReceived = true;
            refreshFragments();
        } else {

            Intent intent = new Intent(MainActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_match_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            MainActivity.this.startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_PHONE_CALL) {
            //another check to be extra sure
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                showPermissionRequestExplanation(getString(R.string.make_call_confirmation_title), getString(R.string.make_call_confirmation_message), Manifest.permission.CALL_PHONE, REQUEST_PHONE_CALL);
                return;
            } else {

                //if permission granted, make call
                startCallSequence(mMatchedContactsList);
            }
        }
    }

    private void setupLightSensor() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if(sensorManager == null) {
            return;
        }

        Sensor LightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (LightSensor != null) {
            sensorManager.registerListener(
                    LightSensorListener,
                    LightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
            getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(LIGHT_SENSOR_AVAILABLE, true).apply();
        } else {
            getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putBoolean(LIGHT_SENSOR_AVAILABLE, false).apply();
        }
    }

    private final SensorEventListener LightSensorListener = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_LIGHT){
                float lightIntensity = event.values[0];
                long currentTimeStamp = System.currentTimeMillis();
                Timber.d("LIGHT INTENSITY : " + lightIntensity);

                if(lightIntensity > LOW_LIGHT_THRESHOLD && mLastSwitchToDarkTimeStamp >= mLastSwitchToLightTimeStamp) {
                    mLastSwitchToLightTimeStamp = currentTimeStamp;
                } else if(lightIntensity <= LOW_LIGHT_THRESHOLD && mLastSwitchToLightTimeStamp >= mLastSwitchToDarkTimeStamp){
                    mLastSwitchToDarkTimeStamp = currentTimeStamp;
                }
            }
        }
    };
}