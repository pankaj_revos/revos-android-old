package com.revos.android.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.TextView;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.revos.android.R;
import com.revos.android.utilities.general.ImageUtils;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import static com.revos.android.constants.Constants.FILE_PROVIDER;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.IMAGE_PREFS;
import static com.revos.android.constants.Constants.KYC_DL_SIDE_1_IMG_BUFFER_KEY;
import static com.revos.android.constants.Constants.KYC_DL_SIDE_2_IMG_BUFFER_KEY;
import static com.revos.android.constants.Constants.KYC_PROFILE_IMG_BUFFER_KEY;
import static com.revos.android.constants.Constants.TEMP_PIC_DIR_PATH;

public class CameraActivity extends AppCompatActivity {
    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private final int PERMISSION_CALLBACK_CONSTANT = 200;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private TextView mCameraHeadingTextView, mCameraDescriptionTextView;

    /**
     * temp bitmap object to operate
     */

    private Bitmap mWorkingBitmap;

    private File mCapturedFile, mCompressedFile, mCroppedFile;

    private Uri mCapturedFileUri, mCompressedFileUri ,mCroppedFileUri;

    private final String CAPTURED_FILE_NAME = "captured.jpg";
    private final String COMPRESSED_FILE_NAME = "compressed.jpg";
    private final String CROPPED_FILE_NAME = "cropped.jpg";

    public static String PIC_TYPE_ARG = "pic_type_arg";

    public static int PIC_TYPE_KYC_PROFILE = 0;
    public static int PIC_TYPE_KYC_DL_SIDE_1 = 1;
    public static int PIC_TYPE_KYC_DL_SIDE_2 = 2;

    private int mPicType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_activity);

        mPicType = getIntent().getIntExtra(PIC_TYPE_ARG, PIC_TYPE_KYC_PROFILE);

        setupViews();

        populateViews();

        setupFiles();

        setupAndStartCamera();
    }

    private void setupFiles() {
        try {
            mCapturedFile = new File(getExternalFilesDir(TEMP_PIC_DIR_PATH), CAPTURED_FILE_NAME);
            mCapturedFile.createNewFile();
            mCapturedFileUri = Uri.fromFile(mCapturedFile);

            mCompressedFile = new File(getExternalFilesDir(TEMP_PIC_DIR_PATH), COMPRESSED_FILE_NAME);
            mCompressedFile.createNewFile();
            mCompressedFileUri = Uri.fromFile(mCompressedFile);

            mCroppedFile = new File(getExternalFilesDir(TEMP_PIC_DIR_PATH), CROPPED_FILE_NAME);
            mCroppedFile.createNewFile();
            mCroppedFileUri = Uri.fromFile(mCroppedFile);


        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            finish();
        }
    }

    private void setupAndStartCamera() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, mPermissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            return;
        }


        Uri capturedPhotoURI = FileProvider.getUriForFile(getApplicationContext(), FILE_PROVIDER, mCapturedFile);
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, capturedPhotoURI);
        startActivityForResult(takePicture, mPicType);
    }

    private void setupViews() {

        mCameraHeadingTextView = findViewById(R.id.camera_heading_text_view);
        mCameraDescriptionTextView = findViewById(R.id.camera_description_text_view);

    }

    private void populateViews() {
        if(mPicType == PIC_TYPE_KYC_PROFILE) {
            mCameraHeadingTextView.setText(getString(R.string.selfie_without_colon));
            mCameraDescriptionTextView.setText(getString(R.string.select_selfie_message));
        }

        if(mPicType == PIC_TYPE_KYC_DL_SIDE_1) {
            mCameraHeadingTextView.setText(getString(R.string.dl_front));
            mCameraDescriptionTextView.setText(getString(R.string.select_dl_front_message));
        }

        if(mPicType == PIC_TYPE_KYC_DL_SIDE_2) {
            mCameraHeadingTextView.setText(getString(R.string.dl_back));
            mCameraDescriptionTextView.setText(getString(R.string.select_dl_back_message));
        }
    }

    private void compressCapturedFile() {
        try {
            mWorkingBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mCapturedFileUri);
            FileOutputStream compressedFileOutputStream = new FileOutputStream(mCompressedFile);
            mWorkingBitmap.compress(Bitmap.CompressFormat.JPEG, 75, compressedFileOutputStream);
            compressedFileOutputStream.flush();
            compressedFileOutputStream.close();
            mWorkingBitmap.recycle();
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(CameraActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void deleteTempFiles() {
        if(mCapturedFile != null && mCapturedFile.exists()) {
            mCapturedFile.delete();
        }
        if(mCompressedFile != null && mCompressedFile.exists()) {
            mCompressedFile.delete();
        }
        if(mCroppedFile != null && mCroppedFile.exists()) {
            mCroppedFile.delete();
        }
    }

    @Override
    public void
    onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            if (requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);
                storeImageBufferInPrefs(mPicType);
                deleteTempFiles();
                setResult(RESULT_OK);
                finish();
            } else if (requestCode == PIC_TYPE_KYC_PROFILE || requestCode == PIC_TYPE_KYC_DL_SIDE_1 || requestCode == PIC_TYPE_KYC_DL_SIDE_2) {
                compressAndCropFile();
            }
        } else if(resultCode == UCrop.RESULT_ERROR) {
            finish();
        } else if(resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    private void compressAndCropFile() {
        compressCapturedFile();

        try {
            ImageUtils.copyExif(new FileInputStream(mCapturedFile), mCompressedFile.getPath());
        } catch (IOException e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            hideProgressDialog();
            return;
        }

        hideProgressDialog();

        UCrop.of(mCompressedFileUri, mCroppedFileUri)
                .start(CameraActivity.this);
    }

    private void storeImageBufferInPrefs(int picType) {
        try {

            byte[] srFileBytesArray = new byte[(int) mCroppedFile.length()];
            FileInputStream fis = new FileInputStream(mCroppedFile);
            fis.read(srFileBytesArray); //read file into bytes[]
            fis.close();
            String srEncodedFileString = Base64.encodeToString(srFileBytesArray, Base64.DEFAULT);

            String encodedString = "data:image/jpg;base64," + srEncodedFileString;

            String sharedPrefImgBufferKey = KYC_PROFILE_IMG_BUFFER_KEY;
            if(picType == PIC_TYPE_KYC_DL_SIDE_1) {
                sharedPrefImgBufferKey = KYC_DL_SIDE_1_IMG_BUFFER_KEY;
            }
            if(picType == PIC_TYPE_KYC_DL_SIDE_2) {
                sharedPrefImgBufferKey = KYC_DL_SIDE_2_IMG_BUFFER_KEY;
            }

            getSharedPreferences(IMAGE_PREFS, MODE_PRIVATE).edit().putString(sharedPrefImgBufferKey, encodedString).apply();
        } catch (Exception e) {

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allGranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if (allGranted) {
                setupAndStartCamera();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, mPermissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, mPermissionsRequired[1])) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.need_following_permissions);

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, mPermissionsRequired[0])) {
                    builder.setMessage(R.string.camera_permission_required);
                } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, mPermissionsRequired[1])) {
                    builder.setMessage(R.string.write_external_storage_permission_required);
                }

                builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(CameraActivity.this, mPermissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {

                Intent intent = new Intent(this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_get_permission));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                intent.putExtras(bundle);
                this.startActivity(intent);

            }
        }
    }

}
