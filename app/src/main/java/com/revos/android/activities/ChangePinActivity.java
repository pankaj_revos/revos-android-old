package com.revos.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Device;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.SetPinMutation;
import com.revos.scripts.type.InputSetPin;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_DEVICE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_DISCONNECT;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class ChangePinActivity extends AppCompatActivity {

    private String mOldPin, mNewPin, mVin, mPhone;

    private boolean mIsReconnectRequired = false;

    private ProgressDialog mProgressDialog;

    public static String VIN_ARG_KEY = "vin_arg_key";
    public static String PHONE_ARG_KEY = "phone_arg_key";
    public static String RECONNECT_TO_DEVICE_ARG_KEY = "reconnect_to_device_arg_key";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(MainActivity.mCurrentThemeID);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.change_pin_activity);

        Intent intent = getIntent();

        if(intent == null) {
            return;
        }

        mVin = intent.getStringExtra(VIN_ARG_KEY);
        mPhone = intent.getStringExtra(PHONE_ARG_KEY);
        mIsReconnectRequired = intent.getBooleanExtra(RECONNECT_TO_DEVICE_ARG_KEY, false);

        if(mVin == null || mVin.isEmpty() || mPhone == null || mPhone.isEmpty()) {

            if(mVin == null || mVin.isEmpty()) {
                Intent errorMsgIntent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_change_pin_vin_not_found));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                errorMsgIntent.putExtras(bundle);
                ChangePinActivity.this.startActivity(errorMsgIntent);
            } else {
                Intent errorMsgIntent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_change_pin_phone_number_not_found));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                errorMsgIntent.putExtras(bundle);
                ChangePinActivity.this.startActivity(errorMsgIntent);
            }

            getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, true).apply();
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));

            finish();
            return;
        }

        setupViews();

        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, true).apply();
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));

    }

    private void setupViews() {

        OtpView mOldPinOtpView = findViewById(R.id.change_pin_enter_old_pin_otp_view);
        OtpView mNewPinOtpView = findViewById(R.id.change_pin_enter_new_pin_otp_view);
        Button mChangePinSubmitButton = findViewById(R.id.change_pin_submit_button);

        mOldPinOtpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                mOldPin = otp;
            }
        });

        mNewPinOtpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                mNewPin = otp;
            }
        });

        mChangePinSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mOldPin == null || mOldPin.isEmpty()) {

                    Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_old_pin));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    ChangePinActivity.this.startActivity(intent);


                } else if(mNewPin == null || mNewPin.isEmpty()) {

                    Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_new_pin));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    ChangePinActivity.this.startActivity(intent);

                } else {
                    makeChangePinCall(mOldPin, mNewPin);
                }
            }
        });
    }

    private void clearPrefs() {
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();

        getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

    }

    private void makeChangePinCall(String oldPin, String newPin) {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getApplicationContext()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_change_vehicle_pin));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            ChangePinActivity.this.startActivity(intent);

            return;
        }

        showProgressDialog();

        InputSetPin inputSetPin = InputSetPin
                                  .builder()
                                  .phone(mPhone)
                                  .vin(mVin)
                                  .pin(newPin)
                                  .previousPin(oldPin)
                                  .build();

        SetPinMutation setPinMutation = SetPinMutation
                                        .builder()
                                        .inputSetPin(inputSetPin)
                                        .build();

        apolloClient.mutate(setPinMutation).enqueue(new ApolloCall.Callback<SetPinMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SetPinMutation.Data> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            if(response.getErrors().get(0).getMessage().equals("400, Bad Request: Wrong PIN")) {      //if old pin entered is wrong

                                Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.old_pin_entered_is_wrong));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                ChangePinActivity.this.startActivity(intent);

                            } else if(response.getErrors().get(0).getMessage().equals("Bad input")) {     //if either of phone, pin, vin, buyer detail is not present

                                Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.insufficient_details));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                ChangePinActivity.this.startActivity(intent);

                            } else {
                                Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_change_vehicle_pin));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                ChangePinActivity.this.startActivity(intent);
                            }

                        } else {

                            if(response.getData() != null && response.getData().device() != null
                                && response.getData().device().setPin() != null && response.getData().device().setPin().device() != null) {

                                SetPinMutation.Device1 cloudDevice = response.getData().device().setPin().device();

                                //set/update device data
                                Device device = PrefUtils.getInstance(getApplicationContext())
                                                            .saveDeviceVariablesInPrefs(cloudDevice);

                                NordicBleService.setDeviceName(null);

                                //save device pin
                                getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, mNewPin).apply();

                                MetadataDecryptionHelper.getInstance(getApplicationContext()).clearLocalCache();

                                //clear previous metadata
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply();
                                getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply();

                                setResult(RESULT_OK);

                                Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_pin_changed_successfully));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                ChangePinActivity.this.startActivity(intent);

                                if(mIsReconnectRequired){
                                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.getMacId()));
                                }

                                finish();

                            } else {

                                Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_change_vehicle_pin));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                                intent.putExtras(bundle);
                                ChangePinActivity.this.startActivity(intent);

                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        clearPrefs();

                        Intent intent = new Intent(ChangePinActivity.this, GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        ChangePinActivity.this.startActivity(intent);

                        setResult(RESULT_CANCELED);
                        finish();
                    }
                });
            }
        });
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(ChangePinActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}
