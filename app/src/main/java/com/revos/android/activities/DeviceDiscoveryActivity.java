package com.revos.android.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.revos.android.R;
import com.revos.android.events.EventBusMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;

import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_DEVICE;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class DeviceDiscoveryActivity extends AppCompatActivity{
	/**bluetooth List View*/
	private ListView mDeviceListView = null;

    /**List of bluetooth devices, address, name*/
    private ArrayList<Pair<String, String>> mFoundBluetoothDevicesList;
    /**Bluetooth devices hash table, address, name*/
    private HashMap<String, Pair<String,String>> mFoundDeviceHashMap;

    //permissions required
    String[] locationPermission = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    //permission callback
    private final int REQUEST_PERMISSION_REQ_CODE = 200;

    /**Intent constants*/
    private final static String USE_DARK_THEME = "use_dark_theme";

	/**ListViewOf the dynamic array object (store the list of arrays for display)*/
	private ArrayList<HashMap<String, Object>> mListItem = null;
	/**SimpleAdapterObject (list display container object)*/
	private SimpleAdapter bleDeviceListSimpleAdapter = null;
	/**Scan status indicator*/
	private CircleProgressView mDiscoveryCircleProgressView;
    /**bluetooth adapter*/
    private BluetoothAdapter mBluetoothAdapter;
    private final Handler mHandler = new Handler();
    private boolean mIsScanning = false;


    private final static long SCAN_DURATION = 5000;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

        boolean useDarkTheme = getIntent().getBooleanExtra(USE_DARK_THEME, false);

        if(useDarkTheme) {
            setTheme(R.style.DialogThemeDark);
        } else {
            setTheme(R.style.DialogThemeLight);
        }

		setContentView(R.layout.device_discovery_activity);

        checkForGPS();

        //setup the bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        if(mBluetoothAdapter == null || mBluetoothAdapter.getState() != BluetoothAdapter.STATE_ON) {

            Intent intent = new Intent(DeviceDiscoveryActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            DeviceDiscoveryActivity.this.startActivity(intent);

            finish();
            return;
        }

        mDiscoveryCircleProgressView = (CircleProgressView) findViewById(R.id.discovery_circle_progress_view);

        mDiscoveryCircleProgressView.spin();
		
		this.mDeviceListView = (ListView)this.findViewById(R.id.discovery_list_view);
		
    	/* Select the item and return it to the calling page */
    	this.mDeviceListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER
                        + mFoundBluetoothDevicesList.get(position).first + GEN_DELIMITER
                        + mFoundBluetoothDevicesList.get(position).second));
                finish();
            }
        });

        checkForLocationPermission();
	}

	private void checkForGPS() {
        //check if GPS is on
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if(locationManager == null) {

            Intent intent = new Intent(DeviceDiscoveryActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.location_manager_null));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            DeviceDiscoveryActivity.this.startActivity(intent);

            finish();
            return;
        }
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            Intent intent = new Intent(DeviceDiscoveryActivity.this, GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_gps));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            DeviceDiscoveryActivity.this.startActivity(intent);

            finish();
        }
    }

	private void checkForLocationPermission() {
        if(ActivityCompat.checkSelfPermission(DeviceDiscoveryActivity.this , locationPermission[0]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DeviceDiscoveryActivity.this, locationPermission, REQUEST_PERMISSION_REQ_CODE);
        } else {
            startScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            boolean allGranted = false;
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if(allGranted){
                startScan();
            } else if(ActivityCompat.shouldShowRequestPermissionRationale(DeviceDiscoveryActivity.this,locationPermission[0])) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DeviceDiscoveryActivity.this);
                builder.setTitle("Need Location Permission");
                builder.setMessage("This app needs Location permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(DeviceDiscoveryActivity.this, locationPermission, REQUEST_PERMISSION_REQ_CODE);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {

                Intent intent = new Intent(DeviceDiscoveryActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_get_permission));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                DeviceDiscoveryActivity.this.startActivity(intent);

                finish();
            }
        }
    }


	/* Show devices list */
	protected void showDevices(){
		if(null == this.mListItem) //The array container does not exist when it is created
			this.mListItem = new ArrayList<>();
		
		//Created if the list adapter is not created
        if(null == this.bleDeviceListSimpleAdapter){
	        //Generates the adapter's Item and the corresponding elements of the dynamic array
	        this.bleDeviceListSimpleAdapter = new SimpleAdapter(this, mListItem,//data source
	            R.layout.ble_device_list_view_item,//The XML implementation of the ListItem
	            //Dynamic Arrays The children corresponding to an ImageItem
	            new String[] {"NAME"},
	            //ImageItem's XML file inside an ImageView, two TextView IDs
	            new int[] {R.id.device_item_ble_name}
	        );
	      	//Added and displayed
	    	this.mDeviceListView.setAdapter(this.bleDeviceListSimpleAdapter);
        }

        //reconstruct the found device list
        mFoundBluetoothDevicesList.clear();

        for(HashMap.Entry<String, Pair<String, String>> entry : mFoundDeviceHashMap.entrySet()) {
            mFoundBluetoothDevicesList.add(entry.getValue());
        }
        
		//Constructs the data for the adapter
        this.mListItem.clear();//Clears history entries
        /*Reconstruct the data*/
        for(int index = 0; index < mFoundBluetoothDevicesList.size(); ++index) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            String deviceName = mFoundBluetoothDevicesList.get(index).second;
            String deviceAddress = mFoundBluetoothDevicesList.get(index).first;
            if(deviceName == null || deviceName.trim().isEmpty()) {
                deviceName = deviceAddress;
            }
            map.put("NAME", deviceName);
            map.put("MAC", deviceAddress);
            this.mListItem.add(map);
        }

		this.bleDeviceListSimpleAdapter.notifyDataSetChanged(); //Informs the adapter that the content has changed automatically
	}

    /**
     * Scan for 5 seconds and then stop scanning when a BluetoothLE device is found then mLEScanCallback
     * is activated This will perform regular scan for custom BLE Service UUID and then filter out.
     * using class ScannerServiceParser
     */
    private void startScan() {
        // Since Android 6.0 we need to obtain either Manifest.permission.ACCESS_COARSE_LOCATION or Manifest.permission.ACCESS_FINE_LOCATION to be able to scan for
        // Bluetooth LE devices. This is related to beacons as proximity devices.
        // On API older than Marshmallow the following code does nothing.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // When user pressed Deny and still wants to use this functionality, show the rationale

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_REQ_CODE);
            return;
        }

        final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();

        if(mIsScanning) {
            return;
        }

        //Initialize the found device list
        if(mFoundBluetoothDevicesList == null) {
            mFoundBluetoothDevicesList = new ArrayList<>();
        } else {
            mFoundBluetoothDevicesList.clear();
        }

        //Initialize the found device hash map
        if(mFoundDeviceHashMap == null) {
            mFoundDeviceHashMap = new HashMap<>();
        } else {
            mFoundDeviceHashMap.clear();
        }

        scanner.startScan(scanCallback);

        mIsScanning = true;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mIsScanning) {
                    stopScan();
                    mIsScanning = false;
                }
            }
        }, SCAN_DURATION);
    }

    /**
     * Stop scan if user tap Cancel button
     */
    private void stopScan() {
        if(mIsScanning) {
            final BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
            scanner.stopScan(scanCallback);

            mIsScanning = false;

            if(mFoundDeviceHashMap.size() > 0) {
                mDiscoveryCircleProgressView.setVisibility(View.GONE);
                findViewById(R.id.discovery_scanning_text_view).setVisibility(View.GONE);
                findViewById(R.id.discovery_connect_to_vehicle_text_view).setVisibility(View.VISIBLE);
            } else {

                Intent intent = new Intent(DeviceDiscoveryActivity.this, GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_devices_found));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                DeviceDiscoveryActivity.this.startActivity(intent);

                finish();
            }
        }
    }

    private no.nordicsemi.android.support.v18.scanner.ScanCallback scanCallback = new no.nordicsemi.android.support.v18.scanner.ScanCallback() {
        @Override
        public void onScanResult(final int callbackType, final no.nordicsemi.android.support.v18.scanner.ScanResult result) {
            mFoundDeviceHashMap.put(result.getDevice().getAddress(), new Pair<>(result.getDevice().getAddress(), result.getDevice().getName()));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDevices();
                }
            });
        }

        @Override
        public void onBatchScanResults(final List<no.nordicsemi.android.support.v18.scanner.ScanResult> results) {

        }

        @Override
        public void onScanFailed(final int errorCode) {
        }
    };
}

