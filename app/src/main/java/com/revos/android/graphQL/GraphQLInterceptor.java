package com.revos.android.graphQL;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class GraphQLInterceptor implements Interceptor {

    private String mToken;
    private final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private final String AUTHORIZATION_HEADER_VALUE_PREFIX = "Bearer ";

    public GraphQLInterceptor(String token) {
        mToken = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;

        newRequest = request.newBuilder()
                .addHeader(AUTHORIZATION_HEADER_NAME, AUTHORIZATION_HEADER_VALUE_PREFIX + mToken)
                .build();
        return chain.proceed(newRequest);
    }
}

