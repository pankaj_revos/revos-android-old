package com.revos.android.jsonStructures;

import com.revos.scripts.type.PaymentType;
import com.revos.scripts.type.UnitType;

public class RentalPricingInfo {

    private String pricingName;
    private PaymentType paymentType;
    private UnitType unitType;
    private float baseAmount;
    private float costPerUnit;
    private String providerUpiAddress;
    private String paymentServiceType;

    public String getPricingName() {
        return pricingName;
    }

    public void setPricingName(String pricingName) {
        this.pricingName = pricingName;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }

    public float getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(float baseAmount) {
        this.baseAmount = baseAmount;
    }

    public float getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(float costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public String getProviderUpiAddress() {
        return providerUpiAddress;
    }

    public void setProviderUpiAddress(String providerUpiAddress) {
        this.providerUpiAddress = providerUpiAddress;
    }

    public String getPaymentServiceType() {
        return paymentServiceType;
    }

    public void setPaymentServiceType(String paymentServiceType) {
        this.paymentServiceType = paymentServiceType;
    }
}
