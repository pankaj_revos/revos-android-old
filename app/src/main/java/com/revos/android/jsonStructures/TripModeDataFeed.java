package com.revos.android.jsonStructures;

public class TripModeDataFeed {

    private double ECONOMY;
    private double RIDE;
    private double SPORT;

    public double getECONOMY() {
        return ECONOMY;
    }

    public void setECONOMY(double ECONOMY) {
        this.ECONOMY = ECONOMY;
    }

    public double getRIDE() {
        return RIDE;
    }

    public void setRIDE(double RIDE) {
        this.RIDE = RIDE;
    }

    public double getSPORT() {
        return SPORT;
    }

    public void setSPORT(double SPORT) {
        this.SPORT = SPORT;
    }
}
