package com.revos.android.jsonStructures;

public class DataFeedBatteryVoltageAdc {

    private double voltage;
    private String timestamp;

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}