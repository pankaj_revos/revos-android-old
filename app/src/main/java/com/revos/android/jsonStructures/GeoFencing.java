package com.revos.android.jsonStructures;

import com.google.android.gms.maps.model.LatLng;
import com.revos.scripts.type.FenceType;

import java.util.List;

public class GeoFencing {

    private String geoFenceId;
    private FenceType geoFenceType;
    private double circleRadius;
    private List<LatLng> polygonVerticesList;
    private LatLng circleGeoFenceCentre;

    public String getGeoFenceId() {
        return geoFenceId;
    }

    public void setGeoFenceId(String geoFenceId) {
        this.geoFenceId = geoFenceId;
    }

    public FenceType getGeoFenceType() {
        return geoFenceType;
    }

    public void setGeoFenceType(FenceType geoFenceType) {
        this.geoFenceType = geoFenceType;
    }

    public double getCircleRadius() {
        return circleRadius;
    }

    public void setCircleRadius(double circleRadius) {
        this.circleRadius = circleRadius;
    }

    public List<LatLng> getPolygonVerticesList() {
        return polygonVerticesList;
    }

    public void setPolygonVerticesList(List<LatLng> polygonVerticesList) {
        this.polygonVerticesList = polygonVerticesList;
    }

    public LatLng getCircleGeoFenceCentre() {
        return circleGeoFenceCentre;
    }

    public void setCircleGeoFenceCentre(LatLng circleGeoFenceCentre) {
        this.circleGeoFenceCentre = circleGeoFenceCentre;
    }
}
