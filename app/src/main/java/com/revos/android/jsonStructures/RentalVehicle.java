package com.revos.android.jsonStructures;

import com.revos.scripts.FetchRentalVehicleDetailsAndPricingInfoQuery;

import java.util.List;

public class RentalVehicle {

    private boolean isDeliverable;
    private String vin;
    private RentalVehicleModel model;
    private RentalVehicleCompany oem;
    private RentalVehicleCompany company;
    private RentalVehicleCompany distributor;
    private String rentalStatus;
    private String status;
    private double latitude;
    private double longitude;
    private double batteryVoltageAdc;
    private double batteryVoltage;
    private List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing> rentalPricingInfoList;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getRentalStatus() {
        return rentalStatus;
    }

    public void setRentalStatus(String rentalStatus) {
        this.rentalStatus = rentalStatus;
    }

    public List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing> getRentalPricingInfoList() {
        return rentalPricingInfoList;
    }

    public void setRentalPricingInfoList(List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing> rentalPricingInfoList) {
        this.rentalPricingInfoList = rentalPricingInfoList;
    }

    public boolean isDeliverable() {
        return isDeliverable;
    }

    public void setDeliverable(boolean deliverable) {
        isDeliverable = deliverable;
    }

    public RentalVehicleCompany getOem() {
        return oem;
    }

    public void setOem(RentalVehicleCompany oem) {
        this.oem = oem;
    }

    public RentalVehicleCompany getCompany() {
        return company;
    }

    public void setCompany(RentalVehicleCompany company) {
        this.company = company;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RentalVehicleCompany getDistributor() {
        return distributor;
    }

    public void setDistributor(RentalVehicleCompany distributor) {
        this.distributor = distributor;
    }

    public double getBatteryVoltageAdc() {
        return batteryVoltageAdc;
    }

    public void setBatteryVoltageAdc(double batteryVoltageAdc) {
        this.batteryVoltageAdc = batteryVoltageAdc;
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public RentalVehicleModel getModel() {
        return model;
    }

    public void setModel(RentalVehicleModel model) {
        this.model = model;
    }
}
