package com.revos.android.jsonStructures;

public class BatteryRegressionDataPoint {
    private double voltage;
    private double soc;
    private double dte;

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public double getSoc() {
        return soc;
    }

    public void setSoc(double soc) {
        this.soc = soc;
    }

    public double getDte() {
        return dte;
    }

    public void setDte(double dte) {
        this.dte = dte;
    }
}