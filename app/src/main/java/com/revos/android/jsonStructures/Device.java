package com.revos.android.jsonStructures;

import com.revos.scripts.type.DeviceStatus;
import com.revos.scripts.type.DeviceType;
import com.revos.scripts.type.DeviceUpdateStatus;

public class Device {
    private String id;
    private String macId;
    private String deviceId;
    private DeviceType deviceType;
    private DeviceStatus status;
    private String key;
    private boolean pinResetRequired;

    private Vehicle vehicle;

    private String firmware;
    private String expectedFirmware;
    private DeviceUpdateStatus deviceUpdateStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public boolean isPinResetRequired() {
        return pinResetRequired;
    }

    public void setPinResetRequired(boolean pinResetRequired) {
        this.pinResetRequired = pinResetRequired;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getExpectedFirmware() {
        return expectedFirmware;
    }

    public void setExpectedFirmware(String expectedFirmware) {
        this.expectedFirmware = expectedFirmware;
    }

    public DeviceUpdateStatus getDeviceUpdateStatus() {
        return deviceUpdateStatus;
    }

    public void setDeviceUpdateStatus(DeviceUpdateStatus deviceUpdateStatus) {
        this.deviceUpdateStatus = deviceUpdateStatus;
    }
}
