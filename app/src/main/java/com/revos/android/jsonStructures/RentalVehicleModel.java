package com.revos.android.jsonStructures;

public class RentalVehicleModel {

    private String name;
    private RentalVehicleCompany company;
    private RentalVehicleModelConfig config;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RentalVehicleCompany getCompany() {
        return company;
    }

    public void setCompany(RentalVehicleCompany company) {
        this.company = company;
    }

    public RentalVehicleModelConfig getConfig() {
        return config;
    }

    public void setConfig(RentalVehicleModelConfig config) {
        this.config = config;
    }
}
