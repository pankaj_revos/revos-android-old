package com.revos.android.jsonStructures;

public class DataFeedVehicleSnapshot {

    private String vin;
    private DataFeedIgnition ignition;
    private DataFeedBattery battery;
    private DataFeedUart uart;
    private DataFeedSnapshotLocation location;
    private DataFeedAlarm alarm;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public DataFeedIgnition getIgnition() {
        return ignition;
    }

    public void setIgnition(DataFeedIgnition ignition) {
        this.ignition = ignition;
    }

    public DataFeedBattery getBattery() {
        return battery;
    }

    public void setBattery(DataFeedBattery battery) {
        this.battery = battery;
    }

    public DataFeedUart getUart() {
        return uart;
    }

    public void setUart(DataFeedUart uart) {
        this.uart = uart;
    }

    public DataFeedSnapshotLocation getLocation() {
        return location;
    }

    public void setLocation(DataFeedSnapshotLocation location) {
        this.location = location;
    }

    public DataFeedAlarm getAlarm() {
        return alarm;
    }

    public void setAlarm(DataFeedAlarm alarm) {
        this.alarm = alarm;
    }
}