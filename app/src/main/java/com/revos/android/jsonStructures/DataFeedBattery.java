package com.revos.android.jsonStructures;

public class DataFeedBattery {

    private double batteryVoltageAdc;
    private double batteryCurrent;
    private double deviceBatteryVoltage;
    private double batterySOC;
    private double batterySOH;
    private String type;
    private String timestamp;
    private String terminalID;
    private String vin;

    public double getBatteryVoltageAdc() {
        return batteryVoltageAdc;
    }

    public void setBatteryVoltageAdc(double batteryVoltageAdc) {
        this.batteryVoltageAdc = batteryVoltageAdc;
    }

    public double getBatteryCurrent() {
        return batteryCurrent;
    }

    public void setBatteryCurrent(double batteryCurrent) {
        this.batteryCurrent = batteryCurrent;
    }

    public double getDeviceBatteryVoltage() {
        return deviceBatteryVoltage;
    }

    public void setDeviceBatteryVoltage(double deviceBatteryVoltage) {
        this.deviceBatteryVoltage = deviceBatteryVoltage;
    }

    public double getBatterySOC() {
        return batterySOC;
    }

    public void setBatterySOC(double batterySOC) {
        this.batterySOC = batterySOC;
    }

    public double getBatterySOH() {
        return batterySOH;
    }

    public void setBatterySOH(double batterySOH) {
        this.batterySOH = batterySOH;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}