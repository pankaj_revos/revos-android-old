package com.revos.android.jsonStructures;

public class RentalVehicleModelConfig {

    private double batteryMinVoltage;
    private double batteryMaxVoltage;

    public double getBatteryMinVoltage() {
        return batteryMinVoltage;
    }

    public void setBatteryMinVoltage(double batteryMinVoltage) {
        this.batteryMinVoltage = batteryMinVoltage;
    }

    public double getBatteryMaxVoltage() {
        return batteryMaxVoltage;
    }

    public void setBatteryMaxVoltage(double batteryMaxVoltage) {
        this.batteryMaxVoltage = batteryMaxVoltage;
    }
}
