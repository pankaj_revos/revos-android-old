package com.revos.android.jsonStructures;

/**
 * Created by mohit on 10/7/18.
 */

public class BluetoothCommand {
    private boolean isCommandBinary;
    private String commandValue;
    private int commandRetryCount;

    public boolean isCommandBinary() {
        return isCommandBinary;
    }

    public void setCommandBinary(boolean commandBinary) {
        isCommandBinary = commandBinary;
    }

    public String getCommandValue() {
        return commandValue;
    }

    public void setCommandValue(String commandValue) {
        this.commandValue = commandValue;
    }

    public int getCommandRetryCount() {
        return commandRetryCount;
    }

    public void setCommandRetryCount(int commandRetryCount) {
        this.commandRetryCount = commandRetryCount;
    }
}
