package com.revos.android.jsonStructures;

import com.revos.scripts.type.GenderType;
import com.revos.scripts.type.Role;
import com.revos.scripts.type.StatusType;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by mohit on 4/7/17.
 */

public class User {

    private String id;
    private Role role;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String altPhone1;
    private String altPhone2;
    private GenderType genderType;
    private String dob;
    private StatusType statusType;

    public StatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(StatusType statusType) {
        this.statusType = statusType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public GenderType getGenderType() {
        return genderType;
    }

    public void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAltPhone1() {
        return altPhone1;
    }

    public void setAltPhone1(String altPhone1) {
        this.altPhone1 = altPhone1;
    }

    public String getAltPhone2() {
        return altPhone2;
    }

    public void setAltPhone2(String altPhone2) {
        this.altPhone2 = altPhone2;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
