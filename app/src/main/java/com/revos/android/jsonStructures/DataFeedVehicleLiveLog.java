package com.revos.android.jsonStructures;

public class DataFeedVehicleLiveLog {

    private int towing;
    private int crashDetection;
    private int batteryUnplug;
    private int deviceBatteryLow;
    private int wheel;
    private int vibration;
    private String type;
    private String timestamp;
    private String terminalID;
    private String vin;
    private int ignition;
    private String movement;
    private double batteryVoltageAdc;
    private double batteryCurrent;
    private double deviceBatteryVoltage;
    private double batterySOC;
    private double batterySOH;
    private double latitude;
    private double longitude;
    private double altitude;
    private int satellites;
    private double xAcc;
    private double yAcc;
    private double zAcc;
    private double gpsSpeed;
    private String firmware;
    private double wheelRpm;
    private int throttle;
    private double batteryVoltage;
    private double odometer;
    private double controllerTemperature;
    private int mode;
    private boolean softLockStatus;
    private boolean overloadStatus;
    private boolean overCurrentStatus;
    private boolean reverseStatus;
    private boolean parkingStatus;
    private boolean antiTheftStatus;
    private boolean eabsStatus;
    private boolean chargingStatus;
    private boolean regenBrakingStatus;
    private boolean brakeStatus;
    private boolean throttleStatus;
    private boolean controllerStatus;
    private boolean motorStatus;
    private boolean hillAssistStatus;
    private boolean headlight;
    private boolean leftIndicator;
    private boolean rightIndicator;
    private int speedLimit;
    private double currentLimit;
    private double underVoltageLimit;
    private double overVoltageLimit;
    private int zeroThrottleRegenLimit;
    private int brakeRegenLimit;
    private int pickupControlLimit;
    private String riderId;
    private String tripId;


    public int getTowing() {
        return towing;
    }

    public void setTowing(int towing) {
        this.towing = towing;
    }

    public int getCrashDetection() {
        return crashDetection;
    }

    public void setCrashDetection(int crashDetection) {
        this.crashDetection = crashDetection;
    }

    public int getBatteryUnplug() {
        return batteryUnplug;
    }

    public void setBatteryUnplug(int batteryUnplug) {
        this.batteryUnplug = batteryUnplug;
    }

    public int getDeviceBatteryLow() {
        return deviceBatteryLow;
    }

    public void setDeviceBatteryLow(int deviceBatteryLow) {
        this.deviceBatteryLow = deviceBatteryLow;
    }

    public int getWheel() {
        return wheel;
    }

    public void setWheel(int wheel) {
        this.wheel = wheel;
    }

    public int getVibration() {
        return vibration;
    }

    public void setVibration(int vibration) {
        this.vibration = vibration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getIgnition() {
        return ignition;
    }

    public void setIgnition(int ignition) {
        this.ignition = ignition;
    }

    public String getMovement() {
        return movement;
    }

    public void setMovement(String movement) {
        this.movement = movement;
    }

    public double getBatteryVoltageAdc() {
        return batteryVoltageAdc;
    }

    public void setBatteryVoltageAdc(double batteryVoltageAdc) {
        this.batteryVoltageAdc = batteryVoltageAdc;
    }

    public double getBatteryCurrent() {
        return batteryCurrent;
    }

    public void setBatteryCurrent(double batteryCurrent) {
        this.batteryCurrent = batteryCurrent;
    }

    public double getDeviceBatteryVoltage() {
        return deviceBatteryVoltage;
    }

    public void setDeviceBatteryVoltage(double deviceBatteryVoltage) {
        this.deviceBatteryVoltage = deviceBatteryVoltage;
    }

    public double getBatterySOC() {
        return batterySOC;
    }

    public void setBatterySOC(double batterySOC) {
        this.batterySOC = batterySOC;
    }

    public double getBatterySOH() {
        return batterySOH;
    }

    public void setBatterySOH(double batterySOH) {
        this.batterySOH = batterySOH;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public int getSatellites() {
        return satellites;
    }

    public void setSatellites(int satellites) {
        this.satellites = satellites;
    }

    public double getxAcc() {
        return xAcc;
    }

    public void setxAcc(double xAcc) {
        this.xAcc = xAcc;
    }

    public double getyAcc() {
        return yAcc;
    }

    public void setyAcc(double yAcc) {
        this.yAcc = yAcc;
    }

    public double getzAcc() {
        return zAcc;
    }

    public void setzAcc(double zAcc) {
        this.zAcc = zAcc;
    }

    public double getGpsSpeed() {
        return gpsSpeed;
    }

    public void setGpsSpeed(double gpsSpeed) {
        this.gpsSpeed = gpsSpeed;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public double getWheelRpm() {
        return wheelRpm;
    }

    public void setWheelRpm(double wheelRpm) {
        this.wheelRpm = wheelRpm;
    }

    public int getThrottle() {
        return throttle;
    }

    public void setThrottle(int throttle) {
        this.throttle = throttle;
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public double getOdometer() {
        return odometer;
    }

    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

    public double getControllerTemperature() {
        return controllerTemperature;
    }

    public void setControllerTemperature(double controllerTemperature) {
        this.controllerTemperature = controllerTemperature;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public boolean isSoftLockStatus() {
        return softLockStatus;
    }

    public void setSoftLockStatus(boolean softLockStatus) {
        this.softLockStatus = softLockStatus;
    }

    public boolean isOverloadStatus() {
        return overloadStatus;
    }

    public void setOverloadStatus(boolean overloadStatus) {
        this.overloadStatus = overloadStatus;
    }

    public boolean isOverCurrentStatus() {
        return overCurrentStatus;
    }

    public void setOverCurrentStatus(boolean overCurrentStatus) {
        this.overCurrentStatus = overCurrentStatus;
    }

    public boolean isReverseStatus() {
        return reverseStatus;
    }

    public void setReverseStatus(boolean reverseStatus) {
        this.reverseStatus = reverseStatus;
    }

    public boolean isParkingStatus() {
        return parkingStatus;
    }

    public void setParkingStatus(boolean parkingStatus) {
        this.parkingStatus = parkingStatus;
    }

    public boolean isAntiTheftStatus() {
        return antiTheftStatus;
    }

    public void setAntiTheftStatus(boolean antiTheftStatus) {
        this.antiTheftStatus = antiTheftStatus;
    }

    public boolean isEabsStatus() {
        return eabsStatus;
    }

    public void setEabsStatus(boolean eabsStatus) {
        this.eabsStatus = eabsStatus;
    }

    public boolean isChargingStatus() {
        return chargingStatus;
    }

    public void setChargingStatus(boolean chargingStatus) {
        this.chargingStatus = chargingStatus;
    }

    public boolean isRegenBrakingStatus() {
        return regenBrakingStatus;
    }

    public void setRegenBrakingStatus(boolean regenBrakingStatus) {
        this.regenBrakingStatus = regenBrakingStatus;
    }

    public boolean isBrakeStatus() {
        return brakeStatus;
    }

    public void setBrakeStatus(boolean brakeStatus) {
        this.brakeStatus = brakeStatus;
    }

    public boolean isThrottleStatus() {
        return throttleStatus;
    }

    public void setThrottleStatus(boolean throttleStatus) {
        this.throttleStatus = throttleStatus;
    }

    public boolean isControllerStatus() {
        return controllerStatus;
    }

    public void setControllerStatus(boolean controllerStatus) {
        this.controllerStatus = controllerStatus;
    }

    public boolean isMotorStatus() {
        return motorStatus;
    }

    public void setMotorStatus(boolean motorStatus) {
        this.motorStatus = motorStatus;
    }

    public boolean isHillAssistStatus() {
        return hillAssistStatus;
    }

    public void setHillAssistStatus(boolean hillAssistStatus) {
        this.hillAssistStatus = hillAssistStatus;
    }

    public boolean isHeadlight() {
        return headlight;
    }

    public void setHeadlight(boolean headlight) {
        this.headlight = headlight;
    }

    public boolean isLeftIndicator() {
        return leftIndicator;
    }

    public void setLeftIndicator(boolean leftIndicator) {
        this.leftIndicator = leftIndicator;
    }

    public boolean isRightIndicator() {
        return rightIndicator;
    }

    public void setRightIndicator(boolean rightIndicator) {
        this.rightIndicator = rightIndicator;
    }

    public int getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    public double getCurrentLimit() {
        return currentLimit;
    }

    public void setCurrentLimit(double currentLimit) {
        this.currentLimit = currentLimit;
    }

    public double getUnderVoltageLimit() {
        return underVoltageLimit;
    }

    public void setUnderVoltageLimit(double underVoltageLimit) {
        this.underVoltageLimit = underVoltageLimit;
    }

    public double getOverVoltageLimit() {
        return overVoltageLimit;
    }

    public void setOverVoltageLimit(double overVoltageLimit) {
        this.overVoltageLimit = overVoltageLimit;
    }

    public int getZeroThrottleRegenLimit() {
        return zeroThrottleRegenLimit;
    }

    public void setZeroThrottleRegenLimit(int zeroThrottleRegenLimit) {
        this.zeroThrottleRegenLimit = zeroThrottleRegenLimit;
    }

    public int getBrakeRegenLimit() {
        return brakeRegenLimit;
    }

    public void setBrakeRegenLimit(int brakeRegenLimit) {
        this.brakeRegenLimit = brakeRegenLimit;
    }

    public int getPickupControlLimit() {
        return pickupControlLimit;
    }

    public void setPickupControlLimit(int pickupControlLimit) {
        this.pickupControlLimit = pickupControlLimit;
    }

    public String getRiderId() {
        return riderId;
    }

    public void setRiderId(String riderId) {
        this.riderId = riderId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
}
