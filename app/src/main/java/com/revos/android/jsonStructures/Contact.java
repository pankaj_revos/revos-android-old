package com.revos.android.jsonStructures;

import java.util.ArrayList;

/**
 * Created by moyadav on 5/22/2016.
 */
public class Contact {
    private String name;

    private boolean isImageSynced;
    private String imagePath;
    private int id;
    private ArrayList<String> phoneNumbers;

    public Contact(){
        isImageSynced = false;
        imagePath = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isImageSynced() {
        return isImageSynced;
    }

    public void setIsImageSynced(boolean isImageSynced) {
        this.isImageSynced = isImageSynced;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ArrayList<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(ArrayList<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
