package com.revos.android.jsonStructures;

public class DataFeedUart {

    private String firmware;
    private String timestamp;
    private float wheelRpm;
    private int throttle;
    private double batteryVoltage;
    private double batteryVoltageAdc;
    private double batteryCurrent;
    private int odometer;
    private double controllerTemperature;
    private int mode;
    private boolean softLockStatus;
    private boolean overloadStatus;
    private boolean overCurrentStatus;
    private boolean reverseStatus;
    private boolean parkingStatus;
    private boolean antiTheftStatus;
    private boolean eabsStatus;
    private boolean chargingStatus;
    private boolean regenBrakingStatus;
    private boolean brakeStatus;
    private boolean throttleStatus;
    private boolean controllerStatus;
    private boolean motorStatus;
    private boolean hillAssistStatus;
    private boolean headlight;
    private boolean leftIndicator;
    private boolean rightIndicator;
    private int speedLimit;
    private int currentLimit;
    private int underVoltageLimit;
    private int overVoltageLimit;
    private int zeroThrottleRegenLimit;
    private int brakeRegenLimit;
    private int pickupControlLimit;
    private String riderId;
    private boolean ignition;
    private String type;
    private String tripId;
    private String terminalID;
    private String vin;


    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public float getWheelRpm() {
        return wheelRpm;
    }

    public void setWheelRpm(float wheelRpm) {
        this.wheelRpm = wheelRpm;
    }

    public int getThrottle() {
        return throttle;
    }

    public void setThrottle(int throttle) {
        this.throttle = throttle;
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public double getBatteryVoltageAdc() {
        return batteryVoltageAdc;
    }

    public void setBatteryVoltageAdc(double batteryVoltageAdc) {
        this.batteryVoltageAdc = batteryVoltageAdc;
    }

    public double getBatteryCurrent() {
        return batteryCurrent;
    }

    public void setBatteryCurrent(double batteryCurrent) {
        this.batteryCurrent = batteryCurrent;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public double getControllerTemperature() {
        return controllerTemperature;
    }

    public void setControllerTemperature(double controllerTemperature) {
        this.controllerTemperature = controllerTemperature;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public boolean isSoftLockStatus() {
        return softLockStatus;
    }

    public void setSoftLockStatus(boolean softLockStatus) {
        this.softLockStatus = softLockStatus;
    }

    public boolean isOverloadStatus() {
        return overloadStatus;
    }

    public void setOverloadStatus(boolean overloadStatus) {
        this.overloadStatus = overloadStatus;
    }

    public boolean isOverCurrentStatus() {
        return overCurrentStatus;
    }

    public void setOverCurrentStatus(boolean overCurrentStatus) {
        this.overCurrentStatus = overCurrentStatus;
    }

    public boolean isReverseStatus() {
        return reverseStatus;
    }

    public void setReverseStatus(boolean reverseStatus) {
        this.reverseStatus = reverseStatus;
    }

    public boolean isParkingStatus() {
        return parkingStatus;
    }

    public void setParkingStatus(boolean parkingStatus) {
        this.parkingStatus = parkingStatus;
    }

    public boolean isAntiTheftStatus() {
        return antiTheftStatus;
    }

    public void setAntiTheftStatus(boolean antiTheftStatus) {
        this.antiTheftStatus = antiTheftStatus;
    }

    public boolean isEabsStatus() {
        return eabsStatus;
    }

    public void setEabsStatus(boolean eabsStatus) {
        this.eabsStatus = eabsStatus;
    }

    public boolean isChargingStatus() {
        return chargingStatus;
    }

    public void setChargingStatus(boolean chargingStatus) {
        this.chargingStatus = chargingStatus;
    }

    public boolean isRegenBrakingStatus() {
        return regenBrakingStatus;
    }

    public void setRegenBrakingStatus(boolean regenBrakingStatus) {
        this.regenBrakingStatus = regenBrakingStatus;
    }

    public boolean isBrakeStatus() {
        return brakeStatus;
    }

    public void setBrakeStatus(boolean brakeStatus) {
        this.brakeStatus = brakeStatus;
    }

    public boolean isThrottleStatus() {
        return throttleStatus;
    }

    public void setThrottleStatus(boolean throttleStatus) {
        this.throttleStatus = throttleStatus;
    }

    public boolean isControllerStatus() {
        return controllerStatus;
    }

    public void setControllerStatus(boolean controllerStatus) {
        this.controllerStatus = controllerStatus;
    }

    public boolean isMotorStatus() {
        return motorStatus;
    }

    public void setMotorStatus(boolean motorStatus) {
        this.motorStatus = motorStatus;
    }

    public boolean isHillAssistStatus() {
        return hillAssistStatus;
    }

    public void setHillAssistStatus(boolean hillAssistStatus) {
        this.hillAssistStatus = hillAssistStatus;
    }

    public boolean isHeadlight() {
        return headlight;
    }

    public void setHeadlight(boolean headlight) {
        this.headlight = headlight;
    }

    public boolean isLeftIndicator() {
        return leftIndicator;
    }

    public void setLeftIndicator(boolean leftIndicator) {
        this.leftIndicator = leftIndicator;
    }

    public boolean isRightIndicator() {
        return rightIndicator;
    }

    public void setRightIndicator(boolean rightIndicator) {
        this.rightIndicator = rightIndicator;
    }

    public int getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    public int getCurrentLimit() {
        return currentLimit;
    }

    public void setCurrentLimit(int currentLimit) {
        this.currentLimit = currentLimit;
    }

    public int getUnderVoltageLimit() {
        return underVoltageLimit;
    }

    public void setUnderVoltageLimit(int underVoltageLimit) {
        this.underVoltageLimit = underVoltageLimit;
    }

    public int getOverVoltageLimit() {
        return overVoltageLimit;
    }

    public void setOverVoltageLimit(int overVoltageLimit) {
        this.overVoltageLimit = overVoltageLimit;
    }

    public int getZeroThrottleRegenLimit() {
        return zeroThrottleRegenLimit;
    }

    public void setZeroThrottleRegenLimit(int zeroThrottleRegenLimit) {
        this.zeroThrottleRegenLimit = zeroThrottleRegenLimit;
    }

    public int getBrakeRegenLimit() {
        return brakeRegenLimit;
    }

    public void setBrakeRegenLimit(int brakeRegenLimit) {
        this.brakeRegenLimit = brakeRegenLimit;
    }

    public int getPickupControlLimit() {
        return pickupControlLimit;
    }

    public void setPickupControlLimit(int pickupControlLimit) {
        this.pickupControlLimit = pickupControlLimit;
    }

    public String getRiderId() {
        return riderId;
    }

    public void setRiderId(String riderId) {
        this.riderId = riderId;
    }

    public boolean isIgnition() {
        return ignition;
    }

    public void setIgnition(boolean ignition) {
        this.ignition = ignition;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}