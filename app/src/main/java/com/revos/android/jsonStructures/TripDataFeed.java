package com.revos.android.jsonStructures;

import java.util.List;

public class TripDataFeed {

    private String tripId;
    private String vin;
    private List<DataFeedAllTripsLocation> location;
    private double distance;
    private double energy;
    private float maxWheelRPM;
    private float minWheelRPM;
    private double minGpsSpeed;
    private double maxGpsSpeed;
    private double avgGpsSpeed;
    private List<DataFeedBatteryVoltageAdc> batteryVoltageAdc;
    private TripModeDataFeed mode;
    private String startTime;
    private String endTime;



    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public float getMaxWheelRPM() {
        return maxWheelRPM;
    }

    public void setMaxWheelRPM(float maxWheelRPM) {
        this.maxWheelRPM = maxWheelRPM;
    }

    public double getMaxGpsSpeed() {
        return maxGpsSpeed;
    }

    public void setMaxGpsSpeed(double maxGpsSpeed) {
        this.maxGpsSpeed = maxGpsSpeed;
    }

    public List<DataFeedBatteryVoltageAdc> getAdcVoltageArrayList() {
        return batteryVoltageAdc;
    }

    public void setAdcVoltageArrayList(List<DataFeedBatteryVoltageAdc> adcVoltageArrayList) {
        this.batteryVoltageAdc = adcVoltageArrayList;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<DataFeedAllTripsLocation> getLocation() {
        return location;
    }

    public void setLocation(List<DataFeedAllTripsLocation> location) {
        this.location = location;
    }

    public TripModeDataFeed getMode() {
        return mode;
    }

    public void setMode(TripModeDataFeed mode) {
        this.mode = mode;
    }

    public double getAvgGpsSpeed() {
        return avgGpsSpeed;
    }

    public void setAvgGpsSpeed(double avgGpsSpeed) {
        this.avgGpsSpeed = avgGpsSpeed;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public double getMinGpsSpeed() {
        return minGpsSpeed;
    }

    public void setMinGpsSpeed(double minGpsSpeed) {
        this.minGpsSpeed = minGpsSpeed;
    }

    public float getMinWheelRPM() {
        return minWheelRPM;
    }

    public void setMinWheelRPM(float minWheelRPM) {
        this.minWheelRPM = minWheelRPM;
    }
}
