package com.revos.android.jsonStructures;

import java.util.ArrayList;

public class BatteryRegressionData {
    private int version;
    private ArrayList<BatteryRegressionDataPoint> dataPoints;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public ArrayList<BatteryRegressionDataPoint> getDataPoints() {
        return dataPoints;
    }

    public void setDataPoints(ArrayList<BatteryRegressionDataPoint> dataPoints) {
        this.dataPoints = dataPoints;
    }
}
