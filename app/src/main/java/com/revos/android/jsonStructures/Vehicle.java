package com.revos.android.jsonStructures;

import com.revos.scripts.type.PassbookTxStatus;
import com.revos.scripts.type.VehicleStatus;
import com.revos.scripts.type.WarrantyStatus;

public class Vehicle {

    private String vin;
    private VehicleStatus status;
    private String ownerId;
    private String ownerFirstName;
    private String ownerLastName;
    private String ownerEmailId;
    private String ownerPhoneNo;
    private String buyerFirstName;
    private String buyerLastName;
    private String buyerPhoneNo;
    private String vehicleId;
    private Model model;
    private String distributorName;
    private String distributorPhoneNo1;
    private String distributorPhoneNo2;
    private String distributorAddress;
    private int followMeHeadlamp;
    private int childSpeedLock;
    private WarrantyStatus warrantyStatus;
    private String warrantyExpiryTime;
    private PassbookTxStatus invoiceStatus;
    private String invoiceCreationDate;
    private double odoValue;

    public String getVin() {

        return vin;
    }

    public void setVin(String vin) {

        this.vin = vin;
    }

    public VehicleStatus getStatus() {
        return status;
    }

    public void setStatus(VehicleStatus status) {
        this.status = status;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getBuyerFirstName() {

        return buyerFirstName;
    }

    public void setBuyerFirstName(String buyerFirstName) {

        this.buyerFirstName = buyerFirstName;
    }

    public String getBuyerLastName() {

        return buyerLastName;
    }

    public void setBuyerLastName(String buyerLastName) {

        this.buyerLastName = buyerLastName;
    }

    public String getBuyerPhoneNo() {

        return buyerPhoneNo;
    }

    public void setBuyerPhoneNo(String buyerPhoneNo) {

        this.buyerPhoneNo = buyerPhoneNo;
    }

    public String getVehicleId() {

        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {

        this.vehicleId = vehicleId;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public int getFollowMeHeadlamp() {
        return followMeHeadlamp;
    }

    public void setFollowMeHeadlamp(int followMeHeadlamp) {
        this.followMeHeadlamp = followMeHeadlamp;
    }

    public int getChildSpeedLock() {
        return childSpeedLock;
    }

    public void setChildSpeedLock(int childSpeedLock) {
        this.childSpeedLock = childSpeedLock;
    }

    public WarrantyStatus getWarrantyStatus() {
        return warrantyStatus;
    }

    public void setWarrantyStatus(WarrantyStatus warrantyStatus) {
        this.warrantyStatus = warrantyStatus;
    }

    public String getWarrantyExpiryTime() {
        return warrantyExpiryTime;
    }

    public void setWarrantyExpiryTime(String warrantyExpiryTime) {
        this.warrantyExpiryTime = warrantyExpiryTime;
    }

    public PassbookTxStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(PassbookTxStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getInvoiceCreationDate() {
        return invoiceCreationDate;
    }

    public void setInvoiceCreationDate(String invoiceCreationDate) {
        this.invoiceCreationDate = invoiceCreationDate;
    }

    public String getDistributorPhoneNo1() {
        return distributorPhoneNo1;
    }

    public void setDistributorPhoneNo1(String distributorPhoneNo1) {
        this.distributorPhoneNo1 = distributorPhoneNo1;
    }

    public String getDistributorPhoneNo2() {
        return distributorPhoneNo2;
    }

    public void setDistributorPhoneNo2(String distributorPhoneNo2) {
        this.distributorPhoneNo2 = distributorPhoneNo2;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getOwnerEmailId() {
        return ownerEmailId;
    }

    public void setOwnerEmailId(String ownerEmailId) {
        this.ownerEmailId = ownerEmailId;
    }

    public String getOwnerPhoneNo() {
        return ownerPhoneNo;
    }

    public void setOwnerPhoneNo(String ownerPhoneNo) {
        this.ownerPhoneNo = ownerPhoneNo;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public double getOdoValue() {
        return odoValue;
    }

    public void setOdoValue(double odoValue) {
        this.odoValue = odoValue;
    }
}