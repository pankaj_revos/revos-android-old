package com.revos.android.jsonStructures;

import com.revos.scripts.type.ModelAccessType;
import com.revos.scripts.type.ModelProtocol;

public class Model {
    private String id;
    private String name;
    private ModelProtocol protocol;

    private ModelAccessType modelAccessType;
    private int speedDivisor;
    private int odoDivisor;

    private int maxSpeed;

    private int speedLimit;
    private int pickupLimit;
    private int brakeRegenLimit;
    private int zeroThrottleRegenLimit;
    private int currentLimit;
    private int overVoltageLimit;
    private int underVoltageLimit;
    private int wheelDiameter;
    private double batteryMaxVoltageLimit;
    private double batteryMinVoltageLimit;


    private boolean hillAssist;
    private boolean parking;
    private boolean regenBraking;
    private boolean eAbs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(ModelProtocol protocol) {
        this.protocol = protocol;
    }

    public ModelAccessType getModelAccessType() {
        return modelAccessType;
    }

    public void setModelAccessType(ModelAccessType modelAccessType) {
        this.modelAccessType = modelAccessType;
    }

    public int getSpeedDivisor() {
        return speedDivisor;
    }

    public void setSpeedDivisor(int speedDivisor) {
        this.speedDivisor = speedDivisor;
    }

    public int getOdoDivisor() {
        return odoDivisor;
    }

    public void setOdoDivisor(int odoDivisor) {
        this.odoDivisor = odoDivisor;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    public int getPickupLimit() {
        return pickupLimit;
    }

    public void setPickupLimit(int pickupLimit) {
        this.pickupLimit = pickupLimit;
    }

    public int getBrakeRegenLimit() {
        return brakeRegenLimit;
    }

    public void setBrakeRegenLimit(int brakeRegenLimit) {
        this.brakeRegenLimit = brakeRegenLimit;
    }

    public int getZeroThrottleRegenLimit() {
        return zeroThrottleRegenLimit;
    }

    public void setZeroThrottleRegenLimit(int zeroThrottleRegenLimit) {
        this.zeroThrottleRegenLimit = zeroThrottleRegenLimit;
    }

    public int getCurrentLimit() {
        return currentLimit;
    }

    public void setCurrentLimit(int currentLimit) {
        this.currentLimit = currentLimit;
    }

    public int getOverVoltageLimit() {
        return overVoltageLimit;
    }

    public void setOverVoltageLimit(int overVoltageLimit) {
        this.overVoltageLimit = overVoltageLimit;
    }

    public int getUnderVoltageLimit() {
        return underVoltageLimit;
    }

    public void setUnderVoltageLimit(int underVoltageLimit) {
        this.underVoltageLimit = underVoltageLimit;
    }

    public int getWheelDiameter() {
        return wheelDiameter;
    }

    public void setWheelDiameter(int wheelDiameter) {
        this.wheelDiameter = wheelDiameter;
    }

    public boolean isHillAssist() {
        return hillAssist;
    }

    public void setHillAssist(boolean hillAssist) {
        this.hillAssist = hillAssist;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public boolean isRegenBraking() {
        return regenBraking;
    }

    public void setRegenBraking(boolean regenBraking) {
        this.regenBraking = regenBraking;
    }

    public boolean iseAbs() {
        return eAbs;
    }

    public void seteAbs(boolean eAbs) {
        this.eAbs = eAbs;
    }

    public double getBatteryMaxVoltageLimit() {
        return batteryMaxVoltageLimit;
    }

    public void setBatteryMaxVoltageLimit(double batteryMaxVoltageLimit) {
        this.batteryMaxVoltageLimit = batteryMaxVoltageLimit;
    }

    public double getBatteryMinVoltageLimit() {
        return batteryMinVoltageLimit;
    }

    public void setBatteryMinVoltageLimit(double batteryMinVoltageLimit) {
        this.batteryMinVoltageLimit = batteryMinVoltageLimit;
    }
}
