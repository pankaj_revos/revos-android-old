package com.revos.android.jsonStructures;

import com.revos.scripts.type.PassbookTxStatus;
import com.revos.scripts.type.PassbookTxType;

public class PassbookTxn {

    private String id;
    private String amount;
    private String dueDate;
    private String paymentDate;
    private String clearanceDate;
    private String remark;
    private PassbookTxStatus passbookTxStatus;
    private PassbookTxType passbookTxType;
    private String creationDate;
    private String payeeUpiId;
    private String vin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getClearanceDate() {
        return clearanceDate;
    }

    public void setClearanceDate(String clearanceDate) {
        this.clearanceDate = clearanceDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public PassbookTxStatus getPassbookTxStatus() {
        return passbookTxStatus;
    }

    public void setPassbookTxStatus(PassbookTxStatus passbookTxStatus) {
        this.passbookTxStatus = passbookTxStatus;
    }

    public PassbookTxType getPassbookTxType() {
        return passbookTxType;
    }

    public void setPassbookTxType(PassbookTxType passbookTxType) {
        this.passbookTxType = passbookTxType;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getPayeeUpiId() {
        return payeeUpiId;
    }

    public void setPayeeUpiId(String payeeUpiId) {
        this.payeeUpiId = payeeUpiId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}