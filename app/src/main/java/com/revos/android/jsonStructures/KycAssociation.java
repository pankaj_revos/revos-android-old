package com.revos.android.jsonStructures;

import com.revos.scripts.FetchAllKYCAssociationsQuery;
import com.revos.scripts.type.KYCStatus;

import java.util.List;

public class KycAssociation {

    private String rentalProviderName;
    private String rentalProviderId;
    private KYCStatus kycAssociationStatus;
    private String kycAssociationId;
    private List<FetchAllKYCAssociationsQuery.DocumentStatus> documentList;

    public String getRentalProviderName() {
        return rentalProviderName;
    }

    public void setRentalProviderName(String rentalProviderName) {
        this.rentalProviderName = rentalProviderName;
    }

    public String getRentalProviderId() {
        return rentalProviderId;
    }

    public void setRentalProviderId(String rentalProviderId) {
        this.rentalProviderId = rentalProviderId;
    }

    public KYCStatus getKycAssociationStatus() {
        return kycAssociationStatus;
    }

    public void setKycAssociationStatus(KYCStatus kycAssociationStatus) {
        this.kycAssociationStatus = kycAssociationStatus;
    }

    public String getKycAssociationId() {
        return kycAssociationId;
    }

    public void setKycAssociationId(String kycAssociationId) {
        this.kycAssociationId = kycAssociationId;
    }

    public List<FetchAllKYCAssociationsQuery.DocumentStatus> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<FetchAllKYCAssociationsQuery.DocumentStatus> documentList) {
        this.documentList = documentList;
    }
}
