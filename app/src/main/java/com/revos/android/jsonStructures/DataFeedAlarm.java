package com.revos.android.jsonStructures;

public class DataFeedAlarm {

    private int towing;
    private int crashDetection;
    private int batteryUnplug;
    private int deviceBatteryLow;
    private int wheel;
    private int vibration;
    private String type;
    private String timestamp;
    private String terminalID;
    private String vin;

    public int isTowing() {
        return towing;
    }

    public void setTowing(int towing) {
        this.towing = towing;
    }

    public int isCrashDetection() {
        return crashDetection;
    }

    public void setCrashDetection(int crashDetection) {
        this.crashDetection = crashDetection;
    }

    public int isBatteryUnplug() {
        return batteryUnplug;
    }

    public void setBatteryUnplug(int batteryUnplug) {
        this.batteryUnplug = batteryUnplug;
    }

    public int isDeviceBatteryLow() {
        return deviceBatteryLow;
    }

    public void setDeviceBatteryLow(int deviceBatteryLow) {
        this.deviceBatteryLow = deviceBatteryLow;
    }

    public int isWheel() {
        return wheel;
    }

    public void setWheel(int wheel) {
        this.wheel = wheel;
    }

    public int isVibration() {
        return vibration;
    }

    public void setVibration(int vibration) {
        this.vibration = vibration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}