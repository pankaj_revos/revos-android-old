package com.revos.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.activities.DiagnosticsActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Device;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.ble.BleUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.vehicleDataTransfer.ControllerWriteCommandHelper;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameters;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;

import static com.revos.android.constants.Constants.BT_EVENT_IMAGE_CHECK_COMPLETE;
import static com.revos.android.constants.Constants.BT_EVENT_IMG_CHAR_WRITE_FAIL;
import static com.revos.android.constants.Constants.BT_EVENT_IMG_CHAR_WRITE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_OBD_CHAR_WRITE_FAIL;
import static com.revos.android.constants.Constants.BT_EVENT_OBD_CHAR_WRITE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CLEAR_IMAGE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_LOCK_HANDLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_LOCK_VEHICLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_ROTATE_TEST_IMAGE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_SEND_FILLER_BYTES_IMG;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_SEND_FILLER_BYTES_OBD;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_SEND_UBER_SCENARIO_1_IMAGE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_SEND_UBER_SCENARIO_2_IMAGE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_UNLOCK_HANDLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_UNLOCK_SEAT;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_UNLOCK_VEHICLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;

/**
 * Created by mohit on 14/9/17.
 */

public class DiagnosticsTextFragment extends Fragment {

    private View mRootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.diagnostics_text_fragment, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        mRootView.findViewById(R.id.diag_send_filler_bytes_obd_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String byteString = ((EditText)mRootView.findViewById(R.id.diag_filler_byte_edit_text)).getText().toString().trim();
                String byteCount = ((EditText)mRootView.findViewById(R.id.diag_filler_byte_count_edit_text)).getText().toString().trim();

                if(!byteString.isEmpty() && !byteCount.isEmpty()) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SEND_FILLER_BYTES_OBD + GEN_DELIMITER + byteString + GEN_DELIMITER + byteCount));
                }
            }
        });

        mRootView.findViewById(R.id.diag_send_filler_bytes_img_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String byteString = ((EditText)mRootView.findViewById(R.id.diag_filler_byte_edit_text)).getText().toString().trim();
                String byteCount = ((EditText)mRootView.findViewById(R.id.diag_filler_byte_count_edit_text)).getText().toString().trim();

                if(!byteString.isEmpty() && !byteCount.isEmpty()) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SEND_FILLER_BYTES_IMG + GEN_DELIMITER + byteString + GEN_DELIMITER + byteCount));
                }
            }
        });

        mRootView.findViewById(R.id.diag_text_write_controller_params_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null){
                    return;
                }

                String bitControl1EditTextStr = ((TextView)mRootView.findViewById(R.id.diag_bit_control_1_edit_text)).getText().toString().trim();
                String speedLimitEditTextStr = ((TextView)mRootView.findViewById(R.id.diag_speed_limit_edit_text)).getText().toString().trim();
                String currentLimitEditTextStr = ((TextView)mRootView.findViewById(R.id.diag_current_limit_edit_text)).getText().toString().trim();
                String underVoltageLimitEditTextStr = ((TextView)mRootView.findViewById(R.id.diag_under_voltage_limit_edit_text)).getText().toString().trim();
                String overVoltageLimitEditTextStr = ((TextView)mRootView.findViewById(R.id.diag_over_voltage_limit_edit_text)).getText().toString().trim();
                String regenZeroThrottleEditTextStr = ((TextView)mRootView.findViewById(R.id.diag_regen_zero_throttle_edit_text)).getText().toString().trim();
                String regenBrakeEditTextStr = ((TextView)mRootView.findViewById(R.id.diag_regen_braking_edit_text)).getText().toString().trim();

                VehicleParameters vehicleParameters = NordicBleService.mVehicleLiveParameters;

                if(vehicleParameters != null) {

                    ControllerWriteCommandHelper controllerWriteCommandHelper = new ControllerWriteCommandHelper(vehicleParameters, getActivity());

                    if(!bitControl1EditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setBitControl1(Integer.parseInt(bitControl1EditTextStr));
                    }

                    if(!speedLimitEditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setSpeedPercent(Integer.parseInt(speedLimitEditTextStr));
                    }

                    if(!currentLimitEditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setOverCurrentLimit(Integer.parseInt(currentLimitEditTextStr));
                    }

                    if(!underVoltageLimitEditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setUnderVoltageLimit(Integer.parseInt(underVoltageLimitEditTextStr));
                    }

                    if(!overVoltageLimitEditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setOverVoltageLimit(Integer.parseInt(overVoltageLimitEditTextStr));
                    }

                    if(!regenBrakeEditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setThrottleZeroRegenLimit(Integer.parseInt(regenZeroThrottleEditTextStr));
                    }

                    if(!regenBrakeEditTextStr.trim().isEmpty()) {
                        controllerWriteCommandHelper.setBrakeRegenLimit(Integer.parseInt(regenBrakeEditTextStr));
                    }

                    byte[] commandArray = controllerWriteCommandHelper.constructWriteCommandByteArray();

                    String constructedString = "";

                    for (byte currentByte : commandArray) {
                        constructedString += BleUtils.singleByteToHexString(currentByte) + " ";
                    }
                    constructedString = constructedString.trim();

                    ((TextView)mRootView.findViewById(R.id.diag_last_written_command_text_view)).setText("LC : " + "(" + System.currentTimeMillis() + ")" + constructedString);

                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS + GEN_DELIMITER + constructedString));
                } else {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_parameters_not_ready));
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                    return;
                }

            }
        });

        mRootView.findViewById(R.id.diag_write_image_complete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_ROTATE_TEST_IMAGE));
            }
        });

        mRootView.findViewById(R.id.diag_clear_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CLEAR_IMAGE));
            }
        });

        mRootView.findViewById(R.id.diag_text_write_vehicle_control_params_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS + GEN_DELIMITER + ((EditText)mRootView.findViewById(R.id.diag_vehicle_params_edit_text)).getText().toString().trim()));
            }
        });

        mRootView.findViewById(R.id.diag_show_uber_scenario_1_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SEND_UBER_SCENARIO_1_IMAGE));
            }
        });

        mRootView.findViewById(R.id.diag_show_uber_scenario_2_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SEND_UBER_SCENARIO_2_IMAGE));
            }
        });

        mRootView.findViewById(R.id.diag_lock_vehicle_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_LOCK_VEHICLE));
            }
        });

        mRootView.findViewById(R.id.diag_unlock_vehicle_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_UNLOCK_VEHICLE));
            }
        });

        mRootView.findViewById(R.id.diag_lock_handle_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_LOCK_HANDLE));
            }
        });

        mRootView.findViewById(R.id.diag_unlock_handle_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_UNLOCK_HANDLE));
            }
        });

        mRootView.findViewById(R.id.diag_unlock_seat_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_UNLOCK_SEAT));
            }
        });

        //test data
        //parseAndUpdateOBDData("57 01 26 54 2B 56 05 56 43 00 34 4D 00 49 00 55 4F 08 CF 54 1D 52 F0 0F");

    }

    @Subscribe
    public void onEventBusMessage(final EventBusMessage event) {
        if(event.message.contains(OBD_EVENT_LIVE_DATA)) {
            final String dataString = event.message.split(GEN_DELIMITER)[1];
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    parseAndUpdateOBDData(dataString);
                }
            });
        } else if(event.message.contains(BT_EVENT_OBD_CHAR_WRITE_SUCCESS)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.parameters_written));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                    ((TextView) mRootView.findViewById(R.id.diag_ble_callback_text_view)).setText("obd char write success : " + new java.util.Date());
                }
            });
        } else if(event.message.contains(BT_EVENT_IMAGE_CHECK_COMPLETE)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
            //todo::log this properly
        } else if(event.message.equals(BT_EVENT_IMG_CHAR_WRITE_SUCCESS)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) mRootView.findViewById(R.id.diag_ble_callback_text_view)).setText("img char write success : " + new java.util.Date());
                }
            });
        } else if(event.message.equals(BT_EVENT_IMG_CHAR_WRITE_FAIL)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) mRootView.findViewById(R.id.diag_ble_callback_text_view)).setText("img char write fail : " + new java.util.Date());
                }
            });
        }  else if(event.message.equals(BT_EVENT_OBD_CHAR_WRITE_FAIL)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) mRootView.findViewById(R.id.diag_ble_callback_text_view)).setText(String.format("obd char write fail : %s", new java.util.Date()));
                }
            });
        }
    }


    private void parseAndUpdateOBDData(String dataString) {
        if(getActivity() == null) {
            return;
        }

        ((TextView) mRootView.findViewById(R.id.diag_debug_string)).setText("time_stamp : " + new java.util.Date() + "\n" + dataString + "(" + dataString.split(" ").length + ")");

        String[] dataArray = dataString.split(" ");

        int speedDivisor = 1;

        Device cloudDevice = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

        if (cloudDevice != null) {
            speedDivisor = cloudDevice.getVehicle().getModel().getSpeedDivisor();
        }

        VehicleParameters vehicleParameters = NordicBleService.mVehicleLiveParameters;

        if(vehicleParameters != null) {
            ((TextView) mRootView.findViewById(R.id.diag_metadata_value_text_view)).setText(NordicBleService.getDeviceMetadataString());

            ((TextView) mRootView.findViewById(R.id.diag_rpm_value_text_view)).setText(String.format("%s", new DecimalFormat("#.####").format(vehicleParameters.getRpm())));

            ((TextView) mRootView.findViewById(R.id.diag_speed_value_text_view)).setText(String.format("%s km/h", new DecimalFormat("#.####").format((int) Math.floor(vehicleParameters.getRpm() / speedDivisor))));

            ((TextView) mRootView.findViewById(R.id.diag_throttle_value_text_view)).setText(String.format("%s %%", vehicleParameters.getThrottle()));

            ((TextView) mRootView.findViewById(R.id.diag_voltage_value_text_view)).setText(String.format("%s V", vehicleParameters.getVoltage()));

            ((TextView) mRootView.findViewById(R.id.diag_current_value_text_view)).setText(String.format("%s A", vehicleParameters.getCurrent()));

            ((TextView) mRootView.findViewById(R.id.diag_odo_value_text_view)).setText((long)vehicleParameters.getOdo() + "");

            ((TextView) mRootView.findViewById(R.id.diag_temperature_value_text_view)).setText(String.format("%s ℃", vehicleParameters.getTemperature()));
            ((TextView) mRootView.findViewById(R.id.diag_lock_value_text_view)).setText(vehicleParameters.isLocked() ? "LOCKED" + "(" + vehicleParameters.isLocked() + ")" : "UNLOCKED" + "(" + vehicleParameters.isLocked() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_speed_limit_value_text_view)).setText(vehicleParameters.getSpeedCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_over_voltage_limit_value_text_view)).setText(vehicleParameters.getOverVoltageCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_under_voltage_limit_value_text_view)).setText(vehicleParameters.getUnderVoltageCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_current_limit_value_text_view)).setText(vehicleParameters.getCurrentCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_zero_throttle_regen_value_text_view)).setText(vehicleParameters.getThrottleZeroRegenCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_brake_regen_value_text_view)).setText(vehicleParameters.getBrakeRegenCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_pickup_control_value_text_view)).setText(vehicleParameters.getPickupPercentageCheckByte() + "");

            ((TextView) mRootView.findViewById(R.id.diag_mode_value_text_view)).setText(String.valueOf(vehicleParameters.getMode()));

            ((TextView) mRootView.findViewById(R.id.diag_overload_value_text_view)).setText(vehicleParameters.isOverLoad() ? "ERROR" + "(" + vehicleParameters.isOverLoad() + ")" : "OK" + "(" + vehicleParameters.isOverLoad() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_overcurrent_value_text_view)).setText(vehicleParameters.isOverCurrent() ? "ERROR" + "(" + vehicleParameters.isOverCurrent() + ")" : "OK" + "(" + vehicleParameters.isOverCurrent() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_reverse_value_text_view)).setText(vehicleParameters.isReverse() ? "ON" + "(" + vehicleParameters.isReverse() + ")" : "OFF" + "(" + vehicleParameters.isReverse() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_motor_value_text_view)).setText(vehicleParameters.isMotorError() ? "ERROR" + "(" + vehicleParameters.isMotorError() + ")" : "OK" + "(" + vehicleParameters.isMotorError() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_controller_value_text_view)).setText(vehicleParameters.isControllerError() ? "ERROR" + "(" + vehicleParameters.getControlCheckByte() + ")" : "OK" + "(" + vehicleParameters.getControlCheckByte() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_throttle_status_value_text_view)).setText(vehicleParameters.isThrottleError() ? "ERROR" + "(" + vehicleParameters.isThrottleError() + ")" : "OK" + "(" + vehicleParameters.isThrottleError() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_brake_value_text_view)).setText(vehicleParameters.isBraking() ? "ON" + "(" + vehicleParameters.isBraking() + ")" : "OFF" + "(" + vehicleParameters.isBraking() + ")");

            ((TextView)mRootView.findViewById(R.id.diag_regen_braking_value_text_view)).setText(vehicleParameters.isRegenBraking() ? "ON" + "(" + vehicleParameters.isRegenBraking() + ")": "OFF" + "(" + vehicleParameters.isRegenBraking() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_eabs_value_text_view)).setText(vehicleParameters.isEABSEngaged() ? "ON" + "(" + vehicleParameters.isEABSEngaged() + ")" : "OFF" + "(" + vehicleParameters.isEABSEngaged() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_x_acc_value_text_view)).setText(String.format("%s", vehicleParameters.getxAcc()));
            ((TextView) mRootView.findViewById(R.id.diag_y_acc_value_text_view)).setText(String.format("%s", vehicleParameters.getyAcc()));
            ((TextView) mRootView.findViewById(R.id.diag_z_acc_value_text_view)).setText(String.format("%s", vehicleParameters.getzAcc()));
            ((TextView) mRootView.findViewById(R.id.diag_anti_theft_mode_value_text_view)).setText("MODE : " + vehicleParameters.getAntiTheftMode() + ", SENSITIVITY : " + vehicleParameters.getAntiTheftSensitivity());

            ((TextView) mRootView.findViewById(R.id.diag_left_indicator_value_text_view)).setText(vehicleParameters.isLeftIndicatorOn() ? "ON" + "(" + vehicleParameters.isLeftIndicatorOn() + ")" : "OFF" + "(" + vehicleParameters.isLeftIndicatorOn() + ")");
            ((TextView) mRootView.findViewById(R.id.diag_right_indicator_value_text_view)).setText(vehicleParameters.isRightIndicatorOn() ? "ON" + "(" + vehicleParameters.isRightIndicatorOn() + ")" : "OFF" + "(" + vehicleParameters.isRightIndicatorOn() + ")");
            ((TextView) mRootView.findViewById(R.id.diag_headlamp_value_text_view)).setText(vehicleParameters.isHeadLampOn() ? "ON" + "(" + vehicleParameters.isHeadLampOn() + ")" : "OFF" + "(" + vehicleParameters.isHeadLampOn() + ")");

            ((TextView) mRootView.findViewById(R.id.diag_battery_adc_value_text_view)).setText(String.valueOf(vehicleParameters.getBatteryAdcVoltage()));
            ((TextView) mRootView.findViewById(R.id.diag_follow_me_home_value_text_view)).setText(String.valueOf(vehicleParameters.getFollowMeHomeHeadlampOnTime()));

            ((TextView) mRootView.findViewById(R.id.diag_generic_wheel_lock_value_text_view)).setText("ACTIVE : " + vehicleParameters.isGenericWheelLockEngaged() + ", SET : " + vehicleParameters.isGenericWheelLockSet());
            ((TextView) mRootView.findViewById(R.id.diag_geo_fencing_wheel_lock_value_text_view)).setText("SET : " + vehicleParameters.isGeoFencingLockSet());
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if(getActivity() != null) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}