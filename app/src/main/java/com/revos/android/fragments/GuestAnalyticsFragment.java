package com.revos.android.fragments;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.revos.android.R;
import com.revos.android.activities.MainActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.TripDataFeed;
import com.revos.android.utilities.general.DateTimeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTime;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import static com.revos.android.activities.MainActivity.mCurrentThemeID;

/**
 * Created by mohit on 11/9/17.
 */

public class GuestAnalyticsFragment extends Fragment{
    /**views and layouts*/
    private View mRootView;

    private LottieAnimationView mLogStatusLottieImageView;

    private TextView mLogStatusTextView, mTripsHistoryTextView, mMaxDistanceTextView,
                        mTotalDistanceTextView, mAvgDistanceTextView, mMaxMileageTextView,
                        mAvgMileageTextView,  mMaxBatteryUsageTextView, mAvgBatteryUsageTextView;

    private BarChart mDistanceBarChart, mMileageBarChart, mBatteryUsageBarChart;
    private LineChart mBatteryStatusLineChart;

    private LinearLayout mVehicleSetupNotCompleteLinearLayout, mLogsSyncStatusLinearLayout, mRefreshingTripDataLinearLayout,
                         mTripsHistoryLinearLayout, mNoTripInfoAvailableLinearLayout;

    private Button mVehicleSetupNotCompleteOkButton;

    private CardView mTripsAndSyncLogsCardView, mMileageGraphCardView, mBatteryStatusGraphCardView, mDistanceGraphCardView, mBatteryUsageGraphCardView;

    private ScrollView mAnalyticsScrollView;

    private final int ANIMATION_DURATION = 500;

    private final int NO_OF_TRIPS_TO_BE_SHOWN = 10;

    private ArrayList<TripDataFeed> mProcessedTripList = null;

    public static final int MINIMUM_TRIP_DISTANCE = 100;  //in meters

    private final int BATTERY_GRAPH_XAXIS_LABEL_COUNT = 6;
    private final int BATTERY_GRAPH_YAXIS_LABEL_COUNT = 5;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.analytics_fragment, container, false);

        setupViews();

        refreshAnalyticsViews();

        return mRootView;
    }

    private void setupViews() {

        if(getActivity() == null) {
            return;
        }

        mVehicleSetupNotCompleteLinearLayout = mRootView.findViewById(R.id.analytics_vehicle_setup_not_complete_linear_layout);
        mVehicleSetupNotCompleteOkButton     = mRootView.findViewById(R.id.analytics_vehicle_setup_not_complete_ok_button);
        mNoTripInfoAvailableLinearLayout     = mRootView.findViewById(R.id.analytics_no_trip_info_available_linear_layout);
        mTripsAndSyncLogsCardView            = mRootView.findViewById(R.id.analytics_trips_and_sync_logs_card_view);
        mLogsSyncStatusLinearLayout          = mRootView.findViewById(R.id.analytics_logs_sync_status_linear_layout);
        mAnalyticsScrollView                 = mRootView.findViewById(R.id.analytics_scroll_view);
        mDistanceGraphCardView               = mRootView.findViewById(R.id.analytics_distance_graph_card_view);
        mMileageGraphCardView                = mRootView.findViewById(R.id.analytics_mileage_card_view);
        mBatteryStatusGraphCardView          = mRootView.findViewById(R.id.analytics_battery_status_card_view);
        mBatteryUsageGraphCardView           = mRootView.findViewById(R.id.analytics_battery_usage_card_view);

        mTripsAndSyncLogsCardView.setVisibility(View.VISIBLE);
        mAnalyticsScrollView.setVisibility(View.VISIBLE);

        setupTripAndLogRelatedViews();
        setupDistanceAnalyticsViews();
        setupBatteryStatusAnalyticsViews();

        //show logs sync status linear layout
        toggleSyncLogsLayout(true);

        //hide battery usage views
        mBatteryUsageGraphCardView.setVisibility(View.GONE);
        mBatteryStatusGraphCardView.setVisibility(View.VISIBLE);
        mMileageGraphCardView.setVisibility(View.VISIBLE);
        setupMileageAnalyticsViews();
    }

    private void toggleSyncLogsLayout(boolean showLayout) {
        if(getActivity() == null) {
            return;
        }

        int visibility = View.VISIBLE;
        int[] attrs = new int[]{R.attr.colorViewTripHistoryBackground};

        if(!showLayout) {
            visibility = View.INVISIBLE;
            attrs = new int[]{R.attr.colorBackgroundFill};
        }

        mLogStatusLottieImageView.setVisibility(visibility);
        mLogStatusTextView.setVisibility(visibility);
        TypedArray a = getActivity().obtainStyledAttributes(mCurrentThemeID, attrs);
        int attributeResourceId = a.getResourceId(0, 0);
        Drawable syncLogsBackground = ContextCompat.getDrawable(getActivity(), attributeResourceId);
        mLogsSyncStatusLinearLayout.setBackground(syncLogsBackground);

    }

    private void setupTripAndLogRelatedViews() {
        mLogStatusLottieImageView = mRootView.findViewById(R.id.analytics_log_status_lottie_image_view);
        mLogStatusTextView = (TextView)mRootView.findViewById(R.id.analytics_log_status_text_view);
        mRefreshingTripDataLinearLayout = mRootView.findViewById(R.id.refreshing_trip_data_linear_layout);

        mTripsHistoryTextView = mRootView.findViewById(R.id.analytics_trip_history_text_view);
        mTripsHistoryLinearLayout = mRootView.findViewById(R.id.analytics_trips_history_linear_view);

        mTripsHistoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }
                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });

        mTripsHistoryLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }
                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });

    }

    private void setupDistanceAnalyticsViews() {
        mDistanceBarChart = (BarChart)mRootView.findViewById(R.id.analytics_distance_chart);
        mMaxDistanceTextView = (TextView)mRootView.findViewById(R.id.analytics_max_distance_text_view);
        mTotalDistanceTextView = (TextView)mRootView.findViewById(R.id.analytics_total_distance_text_view);
        mAvgDistanceTextView = (TextView)mRootView.findViewById(R.id.analytics_avg_distance_text_view);
    }

    private void setupMileageAnalyticsViews() {
        mMileageBarChart = (BarChart)mRootView.findViewById(R.id.analytics_mileage_chart);
        mMaxMileageTextView = (TextView)mRootView.findViewById(R.id.analytics_max_mileage_text_view);
        mAvgMileageTextView = (TextView)mRootView.findViewById(R.id.analytics_avg_mileage_text_view);
    }

    private void setupBatteryUsageAnalyticsViews() {
        mBatteryUsageBarChart = mRootView.findViewById(R.id.analytics_battery_usage_chart);
        mMaxBatteryUsageTextView = mRootView.findViewById(R.id.analytics_max_battery_used_text_view);
        mAvgBatteryUsageTextView = mRootView.findViewById(R.id.analytics_avg_battery_used_text_view);
    }

    private void setupBatteryStatusAnalyticsViews() {
        mBatteryStatusLineChart = mRootView.findViewById(R.id.analytics_battery_status_chart);
    }



    private void refreshAnalyticsViews() {
        if(getActivity() == null) {
            return;
        }

        refreshBatteryStatusLineGraph();
        refreshDistanceGraph();
        refreshMileageGraph();

    }

    private void refreshBatteryStatusLineGraph() {
        if(getActivity() == null || mBatteryStatusLineChart == null) {
            return;
        }

        ArrayList<Entry> xyEntry = new ArrayList<>();
        float lastEntry = 90;
        for(int index = 0 ; index < 8 ; index++) {
            float currentEntry = lastEntry - new Random().nextInt(8);
            xyEntry.add(new Entry(index, currentEntry));
            lastEntry = currentEntry;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        //line data set
        LineDataSet lineDataSet = new LineDataSet(xyEntry, getString(R.string.percent_used_per_trip));
        lineDataSet.setLineWidth(2);
        lineDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        lineDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillColor(ContextCompat.getColor(getActivity(), R.color.revos_green));
        lineDataSet.setDrawHighlightIndicators(false);

        //axis settings
        mBatteryStatusLineChart.getXAxis().setDrawLabels(true);
        mBatteryStatusLineChart.getXAxis().setDrawGridLines(false);
        mBatteryStatusLineChart.getAxisRight().setEnabled(false);
        YAxis y = mBatteryStatusLineChart.getAxisLeft();
        y.setLabelCount(BATTERY_GRAPH_YAXIS_LABEL_COUNT, true);
        y.setTextColor(textColor);
        y.setAxisMinValue(0);
        y.setAxisMaxValue(100);

        mBatteryStatusLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mBatteryStatusLineChart.getXAxis().setLabelRotationAngle(-20f);
        mBatteryStatusLineChart.getXAxis().setTextSize(8f);
        mBatteryStatusLineChart.getXAxis().setLabelCount(BATTERY_GRAPH_XAXIS_LABEL_COUNT, true);
        mBatteryStatusLineChart.getXAxis().setGranularity(1.0f);
        mBatteryStatusLineChart.getXAxis().setTextColor(textColor);

        mBatteryStatusLineChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                DateTime dateTime = new DateTime();
                dateTime = dateTime.minusHours(NO_OF_TRIPS_TO_BE_SHOWN - Math.round(value));
                String timeElapsed = DateTimeUtils.getInstance(getActivity()).generateTimeAgoString(dateTime);

                return timeElapsed;
            }
        });


        //legend settings
        mBatteryStatusLineChart.getLegend().setEnabled(true);
        mBatteryStatusLineChart.getLegend().setTextColor(textColor);
        mBatteryStatusLineChart.setExtraOffsets(0, 0, convertDipToPixels(12), convertDipToPixels(4));

        mBatteryStatusLineChart.animateY(ANIMATION_DURATION);
        mBatteryStatusLineChart.getDescription().setText("");
        mBatteryStatusLineChart.getDescription().setTextSize(12);

        LineData lineData = new LineData(lineDataSet);
        mBatteryStatusLineChart.setData(lineData);
    }

    private void refreshDistanceGraph() {
        if(getActivity() == null || mDistanceBarChart == null || mMaxDistanceTextView == null ||mTotalDistanceTextView == null || mAvgDistanceTextView == null) {
            return;
        }

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        int index = 0;
        double maxDistance = 0;
        double totalDistance = 0;
        for(int i = 0; i < NO_OF_TRIPS_TO_BE_SHOWN; ++i) {
            double randomEntryDistance = new Random().nextInt(100) + 2;
            randomEntryDistance = randomEntryDistance / 10;
            xyEntry.add(new BarEntry(index++, (float)randomEntryDistance));
            if(randomEntryDistance > maxDistance) {
                maxDistance = randomEntryDistance;
            }
            totalDistance += randomEntryDistance;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;


        BarDataSet barDataSet = new BarDataSet(xyEntry, getString(R.string.kilometers));
        barDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mDistanceBarChart.getXAxis().setDrawLabels(true);
        mDistanceBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mDistanceBarChart.getXAxis().setGranularityEnabled(true);
        mDistanceBarChart.getXAxis().setLabelCount(NO_OF_TRIPS_TO_BE_SHOWN);
        mDistanceBarChart.getXAxis().setLabelRotationAngle(-20f);
        mDistanceBarChart.getXAxis().setGranularity(1f);
        mDistanceBarChart.getXAxis().setTextColor(textColor);
        mDistanceBarChart.getXAxis().setTextSize(8f);
        mDistanceBarChart.getXAxis().setDrawGridLines(false);
        mDistanceBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime();
                dateTime = dateTime.minusDays(Math.round(NO_OF_TRIPS_TO_BE_SHOWN - value));
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MMM";

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return getString(R.string.not_applicable);
                }
            }
        });

        mDistanceBarChart.getDescription().setTextColor(textColor);
        mDistanceBarChart.getDescription().setEnabled(false);

        mDistanceBarChart.getLegend().setEnabled(true);
        mDistanceBarChart.getLegend().setTextColor(textColor);

        mDistanceBarChart.getAxisLeft().setTextColor(textColor);
        mDistanceBarChart.getAxisRight().setDrawLabels(false);
        mDistanceBarChart.getAxisRight().setAxisMinimum(0);
        mDistanceBarChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mDistanceBarChart.setData(barData);
        mDistanceBarChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxDistanceTextView.setText(String.format("%s %s", df.format(maxDistance), getString(R.string.kilometers)));
        mTotalDistanceTextView.setText(String.format("%s %s", df.format(totalDistance), getString(R.string.kilometers)));
        mAvgDistanceTextView.setText(String.format("%s %s", df.format(totalDistance / NO_OF_TRIPS_TO_BE_SHOWN), getString(R.string.kilometers)));
    }

    private void refreshMileageGraph() {
        if(getActivity() == null || mMileageBarChart == null || mMaxMileageTextView == null || mAvgMileageTextView == null) {
            return;
        }

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        int index = 0;
        double maxMileage = 0;
        double totalMileage = 0;
        for(int i = 0; i < NO_OF_TRIPS_TO_BE_SHOWN; ++i) {
            double randomMileageEntry = new Random().nextInt(150) + 300;
            randomMileageEntry = randomMileageEntry / 10;
            xyEntry.add(new BarEntry(index++, (float)randomMileageEntry));
            if(randomMileageEntry > maxMileage) {
                maxMileage = randomMileageEntry;
            }
            totalMileage += randomMileageEntry;
        }


        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry, getString(R.string.km_per_kwh));
        barDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mMileageBarChart.getXAxis().setDrawLabels(true);
        mMileageBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mMileageBarChart.getXAxis().setGranularityEnabled(true);
        mMileageBarChart.getXAxis().setLabelCount(NO_OF_TRIPS_TO_BE_SHOWN);
        mMileageBarChart.getXAxis().setLabelRotationAngle(-20f);
        mMileageBarChart.getXAxis().setGranularity(1f);
        mMileageBarChart.getXAxis().setTextColor(textColor);
        mMileageBarChart.getXAxis().setTextSize(8f);
        mMileageBarChart.getXAxis().setDrawGridLines(false);
        mMileageBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime();
                dateTime = dateTime.minusDays(Math.round(NO_OF_TRIPS_TO_BE_SHOWN - value));
                Date dob = dateTime.toDate();
                String dateFormat = "dd-MMM";

                String tripDate = new SimpleDateFormat(dateFormat).format(dob);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return getString(R.string.not_applicable);
                }
            }
        });

        mMileageBarChart.getDescription().setTextColor(textColor);
        mMileageBarChart.getDescription().setEnabled(false);

        mMileageBarChart.getLegend().setEnabled(true);
        mMileageBarChart.getLegend().setTextColor(textColor);

        mMileageBarChart.getAxisLeft().setTextColor(textColor);
        mMileageBarChart.getAxisRight().setDrawLabels(false);
        mMileageBarChart.getAxisRight().setAxisMinimum(0);
        mMileageBarChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mMileageBarChart.setData(barData);
        mMileageBarChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxMileageTextView.setText(String.format("%s %s", df.format(maxMileage), getString(R.string.km_per_kwh)));
        mAvgMileageTextView.setText(String.format("%s %s", df.format(totalMileage / NO_OF_TRIPS_TO_BE_SHOWN), getString(R.string.km_per_kwh)));
    }


    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }


    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAnalyticsViews();
    }
}
