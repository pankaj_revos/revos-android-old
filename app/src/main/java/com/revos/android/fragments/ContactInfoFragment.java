package com.revos.android.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import timber.log.Timber;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;
import com.mukesh.OtpView;
import com.revos.android.R;
import com.revos.android.activities.AppIntroActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.utilities.general.PhoneUtils;

import java.util.concurrent.TimeUnit;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.IS_USER_PHONE_NUMBER_VERIFIED;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_VERIFIED_PHONE_NUMBER;

/**
 * Created by mohit on 6/9/17.
 */

public class ContactInfoFragment extends Fragment {
    /**views and layouts*/
    private View mRootView;

    private EditText mPhoneNumberEditText;

    private CountryCodePicker mEnterPhoneNumCcp;

    private Button mSendOtpVerificationButton, mSubmitOtpButton;

    private RelativeLayout mContactVerificationRelativeLayout;

    private OtpView mContactInfoOtpView;

    private LinearLayout mResendOtpLinearLayout;

    private TextView mOtpCountDownTextView, mResendOtpTextView;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    private boolean mVerificationInProgress = false;
    private final int TIMEOUT_DURATION = 120; //timeout duration in seconds

    private ProgressDialog mProgressDialog;

    private String mPhoneNumberWithCCP;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification withoutse
            //     user action.
            Timber.d("onVerificationCompleted:%s", credential);

            mVerificationInProgress = false;

            if(getActivity() == null) {
                return;
            }

            hideProgressDialog();

            //save verified phone number to prefs
            getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                    .edit()
                    .putString(USER_VERIFIED_PHONE_NUMBER, mPhoneNumberWithCCP)
                    .apply();

            getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit().putBoolean(IS_USER_PHONE_NUMBER_VERIFIED, true).apply();

            //hide otp views
            mSendOtpVerificationButton.setVisibility(View.INVISIBLE);
            mContactVerificationRelativeLayout.setVisibility(View.INVISIBLE);
            mOtpCountDownTextView.setVisibility(View.INVISIBLE);
            mResendOtpTextView.setVisibility(View.GONE);

            linkUserWithCredentials(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Timber.w(e, "onVerificationFailed");
            mVerificationInProgress = false;

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
            }

            // Show a message and update the UI
            // ...
            if (getActivity() != null) {
                hideProgressDialog();

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_verify_phone_number));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

            }
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Timber.d("onCodeSent:%s", verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            showOTPVerificationLayout();
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);
            Timber.d("Auto retrieval time out");
            mVerificationInProgress = false;
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.contact_info_fragment, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        mEnterPhoneNumCcp = mRootView.findViewById(R.id.contact_info_country_code_picker);
        mPhoneNumberEditText = mRootView.findViewById(R.id.contact_info_phone_number_edit_text);
        mEnterPhoneNumCcp.registerCarrierNumberEditText(mPhoneNumberEditText);

        mSendOtpVerificationButton = mRootView.findViewById(R.id.contact_info_send_otp_to_verify_button);

        mContactVerificationRelativeLayout = mRootView.findViewById(R.id.contact_info__verification_relative_layout);

        mContactInfoOtpView = mRootView.findViewById(R.id.contact_info_otp_view);

        mResendOtpLinearLayout = mRootView.findViewById(R.id.contact_info_resend_otp_linear_layout);
        mOtpCountDownTextView = mRootView.findViewById(R.id.contact_info_countdown_text_view);
        mResendOtpTextView = mRootView.findViewById(R.id.contact_info_resend_text_view);

        mSubmitOtpButton = mRootView.findViewById(R.id.contact_info_submit_otp_button);


        mSendOtpVerificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOTP(false);
            }
        });

        mSubmitOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mContactInfoOtpView.getText() == null || mContactInfoOtpView.getText().toString().trim().isEmpty()) {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_entire_OTP));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    verifyPhoneNumberWithCode(mVerificationId, mContactInfoOtpView.getText().toString().trim());
                }
            }
        });
    }

    public CountryCodePicker getCountryCodePicker() {
        return mEnterPhoneNumCcp;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        showProgressDialog();
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        Timber.d(credential.getSmsCode());
        // [END verify_with_code]

        linkUserWithCredentials(credential);
    }

    private void linkUserWithCredentials(PhoneAuthCredential credential) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if(firebaseUser == null) {
            return;
        }

        showProgressDialog();

        firebaseUser.linkWithCredential(credential).addOnCompleteListener(getActivity(), task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = task.getResult().getUser();

                getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                        .edit()
                        .putString(USER_VERIFIED_PHONE_NUMBER, mPhoneNumberWithCCP)
                        .apply();

                getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).edit().putBoolean(IS_USER_PHONE_NUMBER_VERIFIED, true).apply();

                //hide otp views
                mSendOtpVerificationButton.setVisibility(View.INVISIBLE);
                mContactVerificationRelativeLayout.setVisibility(View.INVISIBLE);
                mOtpCountDownTextView.setVisibility(View.INVISIBLE);
                mResendOtpTextView.setVisibility(View.GONE);

                ((AppIntroActivity)getActivity()).handleDonePressed();
            } else {
                hideProgressDialog();

                String message = getString(R.string.incorrect_otp);

                if(task.getException() != null) {
                    message = task.getException().getLocalizedMessage();
                }

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                intent.putExtras(bundle);
                startActivity(intent);

                showOTPVerificationLayout();
            }
        });
    }

    private void sendOTP(boolean isResend) {
        if(mVerificationInProgress) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_wait_for_otp_sms));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String phoneNumber = mPhoneNumberEditText.getText().toString();
        String sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(phoneNumber);

        String countryCode = getCountryCodePicker().getSelectedCountryCode();

        mPhoneNumberWithCCP = String.format("+%s%s", countryCode, sanitizedPhoneNumber);
        if(!isResend) {
            startPhoneNumberVerification(mPhoneNumberWithCCP);
        } else {
            resendVerificationCode(mPhoneNumberWithCCP, mResendToken);
        }
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        if(getActivity() != null) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    TIMEOUT_DURATION,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks,         // OnVerificationStateChangedCallbacks
                    token);             // ForceResendingToken from callbacks

            showProgressDialog();
        }
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        if(getActivity() != null) {
            // [START start_phone_auth]
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    TIMEOUT_DURATION,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks);     // OnVerificationStateChangedCallbacks
            // [END start_phone_auth]

            mVerificationInProgress = true;

            showProgressDialog();
        }
    }


    private void showOTPVerificationLayout() {
        hideProgressDialog();

        mSendOtpVerificationButton.setVisibility(View.INVISIBLE);
        mContactVerificationRelativeLayout.setVisibility(View.VISIBLE);

        mOtpCountDownTextView.setVisibility(View.VISIBLE);
        mResendOtpTextView.setVisibility(View.GONE);

        new CountDownTimer(TIMEOUT_DURATION * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                mOtpCountDownTextView.setText(String.format("%s s", String.valueOf(millisUntilFinished / 1000)));
            }

            public void onFinish() {
                mOtpCountDownTextView.setVisibility(View.GONE);
                mResendOtpLinearLayout.setVisibility(View.VISIBLE);
                mResendOtpTextView.setVisibility(View.VISIBLE);
                mResendOtpTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendOTP(true);
                    }
                });
            }
        }.start();
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }

        if(getActivity() == null) {
            return;
        }

        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.please_wait));
        if(!getActivity().isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
