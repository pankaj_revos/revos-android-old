package com.revos.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.jsonStructures.PassbookTxn;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.adapters.PassbookTransactionListAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.FetchInvoicesForVehicleQuery;
import com.revos.scripts.FetchRentalInvoicesQuery;
import com.revos.scripts.type.PassbookTxStatus;
import com.revos.scripts.type.PassbookTxWhereInput;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PendingPaymentsFragment extends Fragment {

    private View mRootView;
    private LinearLayout mRefreshingPendingPaymentsLinearLayout;
    private RecyclerView mPendingPaymentsRecyclerView;

    private String mPayeeName = null;

    private HashMap<String, PassbookTxn> mPassbookTxnHashMap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.passbook_transactions_list_layout, container, false);

        setupViews();

        mPassbookTxnHashMap = new HashMap<>();

        return mRootView;
    }

    private void setupViews() {
        mRootView.findViewById(R.id.passbook_transactions_heading_card_view).setVisibility(View.GONE);
        mRefreshingPendingPaymentsLinearLayout = mRootView.findViewById(R.id.passbooks_transactions_list_refreshing_linear_layout);
        mPendingPaymentsRecyclerView = mRootView.findViewById(R.id.passbooks_transactions_list_list_recycler_view);
    }

    private void fetchRentalInvoiceDataForAccount() {
        if(getActivity() == null) {
            fetchRentalInvoiceDataForVehicle();
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            fetchRentalInvoiceDataForVehicle();
            return;
        }

        PassbookTxWhereInput passbookTxWhereInput = PassbookTxWhereInput.builder().status(PassbookTxStatus.PENDING).build();

        FetchRentalInvoicesQuery fetchRentalInvoicesQuery = FetchRentalInvoicesQuery.builder().passbookTxnWhereInput(passbookTxWhereInput).build();

        if(mRefreshingPendingPaymentsLinearLayout.getVisibility() != View.VISIBLE) {
            mRefreshingPendingPaymentsLinearLayout.setVisibility(View.VISIBLE);
        }

        apolloClient.query(fetchRentalInvoicesQuery).enqueue(new ApolloCall.Callback<FetchRentalInvoicesQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchRentalInvoicesQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            fetchRentalInvoiceDataForVehicle();

                        } else if(response.getData() != null && response.getData().account() != null && response.getData().account().invoices() != null) {
                            List<FetchRentalInvoicesQuery.Invoice> pendingPaymentsInvoiceListForAccount;

                            pendingPaymentsInvoiceListForAccount = response.getData().account().invoices();
                            if(pendingPaymentsInvoiceListForAccount == null || pendingPaymentsInvoiceListForAccount.isEmpty()) {
                                fetchRentalInvoiceDataForVehicle();
                                return;
                            }

                            List<PassbookTxn> passbookTxnList = new ArrayList<>();

                            for(int index = 0 ; index < pendingPaymentsInvoiceListForAccount.size() ; index++) {
                                List<FetchRentalInvoicesQuery.Passbook> passbookList = pendingPaymentsInvoiceListForAccount.get(index).passbook();

                                FetchRentalInvoicesQuery.Payee payeeDetails= null;
                                if(pendingPaymentsInvoiceListForAccount.get(index) != null && pendingPaymentsInvoiceListForAccount.get(index).payee() != null && pendingPaymentsInvoiceListForAccount.get(index).payee().company() != null) {
                                    payeeDetails = pendingPaymentsInvoiceListForAccount.get(index).payee();
                                }

                                String payeeUpiId = null;
                                if(payeeDetails != null && payeeDetails.company() != null) {
                                    mPayeeName = payeeDetails.company().name();
                                    List<FetchRentalInvoicesQuery.App> appsList = payeeDetails.company().apps();
                                    if(appsList != null && !appsList.isEmpty()) {
                                        for(int appsListindex = 0 ; appsListindex < appsList.size() ; appsListindex++) {
                                            if(appsList.get(appsListindex).paymentDetails() != null) {
                                                if(appsList.get(appsListindex).paymentDetails().address() != null && !appsList.get(appsListindex).paymentDetails().address().isEmpty()) {
                                                    payeeUpiId = appsList.get(appsListindex).paymentDetails().address();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                String vin = null;
                                FetchRentalInvoicesQuery.Asset asset = pendingPaymentsInvoiceListForAccount.get(index).asset();
                                if(asset != null) {
                                    if(asset.vehicle() != null) {
                                        vin = asset.vehicle().vin();
                                    }
                                }

                                if(passbookList != null && !passbookList.isEmpty()) {
                                    for(int passbookListIndex = 0 ; passbookListIndex < passbookList.size() ; passbookListIndex++) {
                                        PassbookTxn passbookTxn = new PassbookTxn();

                                        passbookTxn.setId(passbookList.get(passbookListIndex).id());
                                        if(passbookList.get(passbookListIndex).amount() != null) {
                                            passbookTxn.setAmount(passbookList.get(passbookListIndex).amount().toString());
                                        }
                                        passbookTxn.setDueDate(passbookList.get(passbookListIndex).dueDate());
                                        passbookTxn.setPaymentDate(passbookList.get(passbookListIndex).paymentDate());
                                        passbookTxn.setClearanceDate(passbookList.get(passbookListIndex).clearanceDate());
                                        passbookTxn.setRemark(passbookList.get(passbookListIndex).remark());
                                        passbookTxn.setPassbookTxStatus(passbookList.get(passbookListIndex).status());
                                        passbookTxn.setPassbookTxType(passbookList.get(passbookListIndex).type());
                                        passbookTxn.setCreationDate(passbookList.get(passbookListIndex).createdAt());
                                        passbookTxn.setPayeeUpiId(payeeUpiId);
                                        passbookTxn.setVin(vin);

                                        passbookTxnList.add(passbookTxn);
                                    }
                                }
                            }

                            refreshPassbookList(passbookTxnList);
                            fetchRentalInvoiceDataForVehicle();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fetchRentalInvoiceDataForVehicle();
                    }
                });
            }
        });
    }

    private void fetchRentalInvoiceDataForVehicle() {
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
        if(vehicle == null) {
            populatePendingTxnsList(mPassbookTxnHashMap);
            return;
        }

        String vin = vehicle.getVin();

        if(vin == null || vin.isEmpty()) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            populatePendingTxnsList(mPassbookTxnHashMap);
            return;
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vin).build();
        PassbookTxWhereInput passbookTxWhereInput = PassbookTxWhereInput.builder().status(PassbookTxStatus.PENDING).build();

        FetchInvoicesForVehicleQuery fetchInvoicesForVehicleQuery = FetchInvoicesForVehicleQuery.builder().vehicleWhereUniqueInput(vehicleWhereUniqueInput).passbookTxnWhereInput(passbookTxWhereInput).build();

        if(mRefreshingPendingPaymentsLinearLayout.getVisibility() != View.VISIBLE) {
            mRefreshingPendingPaymentsLinearLayout.setVisibility(View.VISIBLE);
        }

        apolloClient.query(fetchInvoicesForVehicleQuery).enqueue(new ApolloCall.Callback<FetchInvoicesForVehicleQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchInvoicesForVehicleQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            populatePendingTxnsList(mPassbookTxnHashMap);
                        } else if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().invoices() != null) {
                            List<FetchInvoicesForVehicleQuery.Invoice> pendingPaymentsInvoicesListForVehicle;

                            pendingPaymentsInvoicesListForVehicle = response.getData().vehicles().invoices();
                            if(pendingPaymentsInvoicesListForVehicle == null || pendingPaymentsInvoicesListForVehicle.isEmpty()) {
                                return;
                            }

                            List<PassbookTxn> passbookTxnList = new ArrayList<>();

                            for(int index = 0 ; index < pendingPaymentsInvoicesListForVehicle.size() ; index++) {
                                List<FetchInvoicesForVehicleQuery.Passbook> passbookList = pendingPaymentsInvoicesListForVehicle.get(index).passbook();

                                FetchInvoicesForVehicleQuery.Payee payeeDetails= null;
                                if(pendingPaymentsInvoicesListForVehicle.get(index) != null && pendingPaymentsInvoicesListForVehicle.get(index).payee() != null && pendingPaymentsInvoicesListForVehicle.get(index).payee().company() != null) {
                                    payeeDetails = pendingPaymentsInvoicesListForVehicle.get(index).payee();
                                }

                                String payeeUpiId = null;
                                if(payeeDetails != null && payeeDetails.company() != null) {
                                    mPayeeName = payeeDetails.company().name();
                                    List<FetchInvoicesForVehicleQuery.App> appsList = payeeDetails.company().apps();
                                    if(appsList != null && !appsList.isEmpty()) {
                                        for(int appsListindex = 0 ; appsListindex < appsList.size() ; appsListindex++) {
                                            if(appsList.get(appsListindex).paymentDetails() != null) {
                                                if(appsList.get(appsListindex).paymentDetails().address() != null && !appsList.get(appsListindex).paymentDetails().address().isEmpty()) {
                                                    payeeUpiId = appsList.get(appsListindex).paymentDetails().address();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                String vin = null;
                                FetchInvoicesForVehicleQuery.Asset asset = pendingPaymentsInvoicesListForVehicle.get(index).asset();
                                if(asset != null) {
                                    if(asset.vehicle() != null || !asset.vehicle().vin().isEmpty()) {
                                        vin = asset.vehicle().vin();
                                    }
                                }

                                for(int passbookListIndex = 0 ; passbookListIndex < passbookList.size() ; passbookListIndex++) {
                                    PassbookTxn passbookTxn = new PassbookTxn();

                                    passbookTxn.setId(passbookList.get(passbookListIndex).id());
                                    passbookTxn.setAmount(passbookList.get(passbookListIndex).amount().toString());
                                    passbookTxn.setDueDate(passbookList.get(passbookListIndex).dueDate());
                                    passbookTxn.setPaymentDate(passbookList.get(passbookListIndex).paymentDate());
                                    passbookTxn.setClearanceDate(passbookList.get(passbookListIndex).clearanceDate());
                                    passbookTxn.setRemark(passbookList.get(passbookListIndex).remark());
                                    passbookTxn.setPassbookTxStatus(passbookList.get(passbookListIndex).status());
                                    passbookTxn.setPassbookTxType(passbookList.get(passbookListIndex).type());
                                    passbookTxn.setCreationDate(passbookList.get(passbookListIndex).createdAt());
                                    passbookTxn.setPayeeUpiId(payeeUpiId);
                                    passbookTxn.setVin(vin);

                                    passbookTxnList.add(passbookTxn);
                                }
                            }

                            refreshPassbookList(passbookTxnList);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        populatePendingTxnsList(mPassbookTxnHashMap);
                    }
                });
            }
        });
    }

    private void refreshPassbookList(List<PassbookTxn> passbookTxnList) {
        if(passbookTxnList == null || passbookTxnList.isEmpty()) {
            populatePendingTxnsList(mPassbookTxnHashMap);
            return;
        }

        for(int index = 0 ; index < passbookTxnList.size() ; index++) {
            mPassbookTxnHashMap.put(passbookTxnList.get(index).getId(), passbookTxnList.get(index));
        }

        populatePendingTxnsList(mPassbookTxnHashMap);
    }

    private void populatePendingTxnsList(HashMap<String, PassbookTxn> pendingPassbookHashMap) {
        mRefreshingPendingPaymentsLinearLayout.setVisibility(View.GONE);
        List<PassbookTxn> passbookTxnList = new ArrayList<>();

        for (HashMap.Entry<String, PassbookTxn> entry : pendingPassbookHashMap.entrySet()) {
            passbookTxnList.add(entry.getValue());
        }

        //sort invoices by latest date first
        Collections.sort(passbookTxnList, new Comparator<PassbookTxn>() {
            @Override
            public int compare(PassbookTxn t1, PassbookTxn t2) {
                DateTime dateTime1 = new DateTime(t1.getCreationDate());
                DateTime dateTime2 = new DateTime(t2.getCreationDate());

                return dateTime2.compareTo(dateTime1);
            }
        });

        PassbookTransactionListAdapter mPendingTxnPassbookInfoAdapter = new PassbookTransactionListAdapter(mPayeeName, passbookTxnList, getActivity());
        mPendingPaymentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPendingPaymentsRecyclerView.setAdapter(mPendingTxnPassbookInfoAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchRentalInvoiceDataForAccount();
    }
}