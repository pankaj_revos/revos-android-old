package com.revos.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.activities.DiagnosticsActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.ble.BleUtils;
import com.revos.android.utilities.vehicleDataTransfer.ControllerWriteCommandHelper;
import com.revos.android.utilities.vehicleDataTransfer.VehicleControlHelper;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameters;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;

public class DiagnosticsControlFragment extends Fragment {
    private boolean mViewsPopulated = false;

    View mRootView;

    private SeekBar mModeSeekBar, mSpeedLimitSeekBar, mCurrentLimitSeekBar, mVoltageUnderSeekBar, mVoltageOverSeekBar,
                        mRegenZeroThrottleSeekBar, mRegenBrakingLimitSeekBar, mPickUpControlSeekBar;

    private TextView mBitControlValueTextView, mSpeedLimitValueTextView, mCurrentLimitValueTextView, mVoltageUnderValueTextView,
                        mVoltageOverValueTextView, mRegenZeroThrottleValueTextView, mRegenBrakingLimitValueTextView, mPickUpControlValueTextView;

    private com.suke.widget.SwitchButton mSoftLockSwitch, mWheelLockSwitch, mEABSSwitch, mRegenSwitch, mHillAssistSwitch, mParkModeSwitch,
                                            mHandleLockSwitch, mUnlockSeatSwitch, mHeadlampSwitch, mRightIndicatorSwitch, mLeftIndicatorSwitch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.diagnostics_control_fragment, container, false);

        setupViews();

        return mRootView;

    }

    private void setupViews() {

        //switch buttons
        mSoftLockSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_soft_lock_switch);
        mWheelLockSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_wheel_lock_switch);
        mEABSSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_eabs_switch);
        mRegenSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_regen_switch);
        mHillAssistSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_hill_assist_switch);
        mParkModeSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_park_mode_switch);
        mHandleLockSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_handle_lock_switch);
        mUnlockSeatSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_seat_lock_switch);
        mHeadlampSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_headlamp_switch);
        mRightIndicatorSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_right_indicator_switch);
        mLeftIndicatorSwitch = (com.suke.widget.SwitchButton) mRootView.findViewById(R.id.diag_left_indicator_switch);


        //seek bars
        mModeSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_mode_seek_bar);
        mSpeedLimitSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_speed_limit_seek_bar);
        mCurrentLimitSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_current_limit_seek_bar);
        mVoltageUnderSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_voltage_under_seek_bar);
        mVoltageOverSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_voltage_over_seek_bar);
        mRegenZeroThrottleSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_regen_zero_throttle_seek_bar);
        mRegenBrakingLimitSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_regen_braking_limit_seek_bar);
        mPickUpControlSeekBar = (SeekBar) mRootView.findViewById(R.id.diag_pick_up_control_limit_seek_bar);


        //seek bar values
        mBitControlValueTextView = (TextView) mRootView.findViewById(R.id.diag_mode_value_text_view);
        mBitControlValueTextView.setText("0");

        mSpeedLimitValueTextView = (TextView) mRootView.findViewById(R.id.diag_speed_limit_value_text_view);
        mSpeedLimitValueTextView.setText("0");

        mCurrentLimitValueTextView = (TextView) mRootView.findViewById(R.id.diag_current_limit_value_text_view);
        mCurrentLimitValueTextView.setText("0");

        mVoltageUnderValueTextView = (TextView) mRootView.findViewById(R.id.diag_voltage_under_value_text_view);
        mVoltageUnderValueTextView.setText("0");

        mVoltageOverValueTextView = (TextView) mRootView.findViewById(R.id.diag_voltage_over_value_text_view);
        mVoltageOverValueTextView.setText("0");

        mRegenZeroThrottleValueTextView = (TextView) mRootView.findViewById(R.id.diag_regen_zero_throttle_value_text_view);
        mRegenZeroThrottleValueTextView.setText("0");

        mRegenBrakingLimitValueTextView = (TextView) mRootView.findViewById(R.id.diag_regen_braking_limit_value_text_view);
        mRegenBrakingLimitValueTextView.setText("0");

        mPickUpControlValueTextView = (TextView) mRootView.findViewById(R.id.diag_pick_up_control_limit_value_text_view);
        mPickUpControlValueTextView.setText("0");

        mModeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBitControlValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mSpeedLimitSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSpeedLimitValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mCurrentLimitSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCurrentLimitValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mVoltageUnderSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mVoltageUnderValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mVoltageOverSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mVoltageOverValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mRegenZeroThrottleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mRegenZeroThrottleValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mRegenBrakingLimitSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mRegenBrakingLimitValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mPickUpControlSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mPickUpControlValueTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mRootView.findViewById(R.id.diag_control_write_controller_params_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constructAndSendControllerParams();
            }
        });

        mRootView.findViewById(R.id.diag_control_write_vehicle_params_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                constructAndSendVehicleControlParams();
            }
        });

    }

    private void constructAndSendControllerParams() {
        if(getActivity() == null) {
            return;
        }

        VehicleParameters vehicleParameters = NordicBleService.mVehicleLiveParameters;

        if(vehicleParameters != null) {

            ControllerWriteCommandHelper controllerWriteCommandHelper = new ControllerWriteCommandHelper(vehicleParameters, getActivity());

            controllerWriteCommandHelper.setSoftLock(mSoftLockSwitch.isChecked());
            controllerWriteCommandHelper.setWheelLock(mWheelLockSwitch.isChecked());
            controllerWriteCommandHelper.setEAbs(mEABSSwitch.isChecked());
            controllerWriteCommandHelper.setHillAssist(mHillAssistSwitch.isChecked());
            controllerWriteCommandHelper.setParking(mParkModeSwitch.isChecked());
            controllerWriteCommandHelper.setRegenBraking(mRegenSwitch.isChecked());

            controllerWriteCommandHelper.setMode(mModeSeekBar.getProgress());
            controllerWriteCommandHelper.setSpeedPercent(mSpeedLimitSeekBar.getProgress());
            controllerWriteCommandHelper.setOverCurrentLimit(mCurrentLimitSeekBar.getProgress());
            controllerWriteCommandHelper.setUnderVoltageLimit(mVoltageUnderSeekBar.getProgress());
            controllerWriteCommandHelper.setOverVoltageLimit(mVoltageOverSeekBar.getProgress());
            controllerWriteCommandHelper.setThrottleZeroRegenLimit(mRegenZeroThrottleSeekBar.getProgress());
            controllerWriteCommandHelper.setBrakeRegenLimit(mRegenBrakingLimitSeekBar.getProgress());
            controllerWriteCommandHelper.setPickupControlLimit(mPickUpControlSeekBar.getProgress());

            byte[] commandArray = controllerWriteCommandHelper.constructWriteCommandByteArray();

            String constructedString = "";

            for (byte currentByte : commandArray) {
                constructedString += BleUtils.singleByteToHexString(currentByte) + " ";
            }
            constructedString = constructedString.trim();

            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS + GEN_DELIMITER + constructedString));
        } else {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_parameters_not_ready));
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }
    }

    private void constructAndSendVehicleControlParams() {
        if(getActivity() == null) {
            return;
        }


        int size = 4;
        if(mUnlockSeatSwitch.isChecked()) {
            size = 5;
        }

        byte[] vehicleControlByteArray = new byte[size];

        VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(getActivity());

        vehicleControlByteArray[0] = vehicleControlHelper.getHandleControlByte(mHandleLockSwitch.isChecked());
        vehicleControlByteArray[1] = vehicleControlHelper.getHeadLampControlByte(mHeadlampSwitch.isChecked());
        vehicleControlByteArray[2] = vehicleControlHelper.getRightIndicatorControlByte(mRightIndicatorSwitch.isChecked());
        vehicleControlByteArray[3] = vehicleControlHelper.getLeftIndicatorControlByte(mLeftIndicatorSwitch.isChecked());

        if(size == 5) {
            vehicleControlByteArray[4] = vehicleControlHelper.getSeatUnlockByte();
        }


        String constructedString = "";

        for (byte currentByte : vehicleControlByteArray) {
            constructedString += BleUtils.singleByteToHexString(currentByte) + " ";
        }
        constructedString = constructedString.trim();


        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS + GEN_DELIMITER + constructedString));
    }

    private void populateViews() {
        if(getActivity() == null){
            return;
        }

        VehicleParameters vehicleParameters = NordicBleService.mVehicleLiveParameters;
        try {

            if (vehicleParameters != null) {
                //set this variable to true before actually populating the views
                //so that in case of an exception, the seekbars aren't constantly updated
                mViewsPopulated = true;

                boolean regenSettingStatus = false;
                long regenControlCheckByte = vehicleParameters.getControlCheckByte();
                //shift 4 position for detecting regen braking status
                regenControlCheckByte  = regenControlCheckByte >> 4;
                regenControlCheckByte = regenControlCheckByte & 1;
                regenSettingStatus = (regenControlCheckByte == 1);

                boolean eABSSettingStatus = false;
                long eABSControlCheckByte = vehicleParameters.getControlCheckByte();
                //shift 4 position for detecting eABS braking status
                eABSControlCheckByte  = eABSControlCheckByte >> 3;
                eABSControlCheckByte = eABSControlCheckByte & 1;
                eABSSettingStatus = (eABSControlCheckByte == 1);

                //todo::this bit is flipped from what has been mentioned in the protocol document
                boolean parkingSettingStatus = false;
                long parkingControlCheckByte = vehicleParameters.getControlCheckByte();
                //shift 4 position for detecting parking braking status
                parkingControlCheckByte  = parkingControlCheckByte >> 2;
                parkingControlCheckByte = parkingControlCheckByte & 1;
                parkingSettingStatus = (parkingControlCheckByte == 0);

                boolean wheelLockSettingStatus = false;
                long wheelLockCheckByte = vehicleParameters.getControlCheckByte();
                //shift 4 position for detecting parking braking status
                wheelLockCheckByte  = wheelLockCheckByte >> 7;
                wheelLockCheckByte = wheelLockCheckByte & 1;
                wheelLockSettingStatus = (wheelLockCheckByte == 1);

                boolean hillAssistSettingStatus = false;
                long hillAssistCheckByte = vehicleParameters.getControlCheckByte();
                //shift 4 position for detecting parking braking status
                hillAssistCheckByte  = hillAssistCheckByte >> 5;
                hillAssistCheckByte = hillAssistCheckByte & 1;
                hillAssistSettingStatus = (hillAssistCheckByte == 1);

                boolean controllerLockSettingStatus = false;
                long controllerLockCheckByte = vehicleParameters.getControlCheckByte();
                //shift 4 position for detecting parking braking status
                controllerLockCheckByte  = controllerLockCheckByte >> 7;
                controllerLockCheckByte = controllerLockCheckByte & 1;
                controllerLockSettingStatus = (controllerLockCheckByte == 1);

                mSoftLockSwitch.setChecked(controllerLockSettingStatus);
                mWheelLockSwitch.setChecked(wheelLockSettingStatus);
                mEABSSwitch.setChecked(eABSSettingStatus);
                mHillAssistSwitch.setChecked(hillAssistSettingStatus);
                mParkModeSwitch.setChecked(parkingSettingStatus);
                mRegenSwitch.setChecked(regenSettingStatus);

                mModeSeekBar.setProgress((int) vehicleParameters.getMode());
                mSpeedLimitSeekBar.setProgress((int) vehicleParameters.getSpeedCheckByte());
                mCurrentLimitSeekBar.setProgress((int) vehicleParameters.getCurrentCheckByte());
                mVoltageOverSeekBar.setProgress((int) vehicleParameters.getOverVoltageCheckByte());
                mVoltageUnderSeekBar.setProgress((int) vehicleParameters.getUnderVoltageCheckByte());
                mRegenZeroThrottleSeekBar.setProgress((int) vehicleParameters.getThrottleZeroRegenCheckByte());
                mRegenBrakingLimitSeekBar.setProgress((int) vehicleParameters.getBrakeRegenCheckByte());
                mPickUpControlSeekBar.setProgress((int) vehicleParameters.getPickupPercentageCheckByte());

            }
        } catch (Exception e) {
            //todo::may be alert the user, we'll see
        }
    }

    @Subscribe
    public void onEventBusMessage(final EventBusMessage event) {
        if (event.message.contains(OBD_EVENT_LIVE_DATA)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!mViewsPopulated) {
                        populateViews();
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getActivity() != null) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}