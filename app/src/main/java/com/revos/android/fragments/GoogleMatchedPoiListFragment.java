package com.revos.android.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.revos.android.R;
import com.revos.android.activities.MainActivity;
import com.revos.android.utilities.adapters.GoogleMatchedPoiAdapter;

import java.util.List;

public class GoogleMatchedPoiListFragment extends Fragment {

    private View rootView;
    private TextView mPlaceListTextView;
    private GoogleMatchedPoiAdapter mGoogleMatchedPoiAdapter;
    private RecyclerView mRecyclerView;
    private TextView mTimerTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        List<AutocompletePrediction> autocompletePredictionList = null;

        rootView = inflater.inflate(R.layout.matched_poi_fragment, container, false);

        mPlaceListTextView = (TextView) rootView.findViewById(R.id.places_list_header_text_view);

        if(getActivity() != null) {
             autocompletePredictionList = ((MainActivity)getActivity()).mAutoCompletePredictions;
        }

        mTimerTextView = (TextView) rootView.findViewById(R.id.countdown_timer_text_view);
        setupViews(autocompletePredictionList);

        return rootView;
    }

    private void setupViews(List<AutocompletePrediction> autocompletePredictions) {

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.poi_recycler_view);

        if(autocompletePredictions != null && !autocompletePredictions.isEmpty()) {
            mGoogleMatchedPoiAdapter = new GoogleMatchedPoiAdapter(autocompletePredictions, getActivity());

            //use a grid layout manager
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            mRecyclerView.setAdapter(mGoogleMatchedPoiAdapter);
            startTimer(autocompletePredictions);
        }
    }

    private void startTimer(List<AutocompletePrediction> autocompletePredictions) {

        new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimerTextView.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                if(getActivity() == null) {
                    return;
                }
                ((MainActivity)getActivity()).mNavigateToOnVoiceResultReceived = false;

                //start the navigation first address
                ((MainActivity)getActivity()).startGoogleMapsNavigation(autocompletePredictions.get(0).getPrimaryText(null).toString() + "," + autocompletePredictions.get(0).getSecondaryText(null).toString());

                ((MainActivity)getActivity()).refreshFragments();

            }
        }.start();
    }

}
