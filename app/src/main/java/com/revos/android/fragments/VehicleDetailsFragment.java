package com.revos.android.fragments;

import android.app.AlertDialog;
import android.bluetooth.BluetoothProfile;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.activities.ChangePinActivity;
import com.revos.android.activities.ConnectUsingVinActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.VehicleSettingsActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.User;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.DateTimeUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.general.RevosFileUtils;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameters;
import com.revos.scripts.type.InvoiceStatus;
import com.revos.scripts.type.ModelProtocol;
import com.revos.scripts.type.VehicleStatus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;
import static com.revos.android.activities.ChangePinActivity.PHONE_ARG_KEY;
import static com.revos.android.activities.ChangePinActivity.RECONNECT_TO_DEVICE_ARG_KEY;
import static com.revos.android.activities.ChangePinActivity.VIN_ARG_KEY;
import static com.revos.android.activities.ConnectUsingVinActivity.CONNECT_USING_VIN_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_LOCKED;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.LOG_TYPE_LIVE;
import static com.revos.android.constants.Constants.LOG_TYPE_OFFLINE;
import static com.revos.android.constants.Constants.LOG_TYPE_OFFLINE_BATTERY_ADC;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.constants.Constants.OVER_TEMPERATURE_LIMIT;

public class VehicleDetailsFragment extends Fragment {

    private TextView mVehicleVinTextView, mVehicleModelNameTextView, mVehiclePinTextView,
                     mThrottleDescTextView, mControllerDescTextView, mMotorDescTextView, mTemperatureDescTextView, mOverloadDescTextView,
                     mTapToViewVehicleInfoTextView, mTapToViewVehicleHealthTextView,
                     mBuyerLabelTextView, mBuyerNameTextView, mBuyerPhoneTextView, mBuyerDateOfPurchaseTextView,
                     mOwnerNameTextView, mOwnerPhoneTextView, mOwnerEmailTextView,
                     mWarrantyLabelTextView, mWarrantyValidTillTextView, mWarrantyExpiresInTextView,
                     mDistributorNameTextView, mDistributorPhoneTextView, mDistributorAddressTextView, mFirmwareVersionNumberTextView;

    private CardView mConnectToViewVehicleHealthCardView, mVehicleHealthCardView,mBuyerInfoCardView,
                     mOwnerInfoCardView, mWarrantyInfoCardView;

    private LinearLayout mVehiclePinLinearLayout, mVehicleInfoDetailsLinearLayout, mVehicleInfoHealthButtonsLinearLayout,
                         mOwnerEmailLinearLayout, mOwnerLabelLinearLayout;

    private ImageView mVehicleDetailsBackButtonImageView, mVehicleSettingsIconImageView, mChangePinImageView;

    private CircleImageView mVehicleModelCircleImageView;

    private Button mVehicleInfoButton, mVehicleHealthInfoButton, mConnectToVehicleButton;

    private LottieAnimationView mThrottleLottieImageView, mControllerLottieImageView, mMotorLottieImageView,
                                mTemperatureLottieImageView, mOverloadLottieImageView;

    private View mRootView, mOwnerInfoEmailSeparatorView;

    private Vehicle mVehicle;

    private final String DEVICE_DISCONNECTED = "100";
    private final String DEVICE_LOCKED = "200";

    private boolean mIsThrottleError = false, mIsControllerError = false, mIsMotorError = false,
                    mIsTemperatureError = false, mIsOverload = false;

    private int mVehicleConnectionState;

    public static String VEHICLE_DETAILS_OBJECT = "vehicle_details_object";
    public static String VEHICLE_PIN_KEY = "vehicle_pin_key";
    public static String CURRENT_VEHICLE_MODEL_IMAGE_URL = "current_vehicle_model_image_url";

    private String mVehicleObjectStr = null;
    private String mVehiclePin = null;
    private String mCurrentVehicleModelImageUri = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getActivity() == null) {
            return;
        }

        Bundle bundle = getArguments();
        if(bundle == null) {
            return;
        }

        if(bundle.containsKey(VEHICLE_DETAILS_OBJECT)) {
            mVehicleObjectStr = bundle.getString(VEHICLE_DETAILS_OBJECT);
        }

        if(bundle.containsKey(VEHICLE_PIN_KEY)) {
            mVehiclePin = bundle.getString(VEHICLE_PIN_KEY);
        }

        if(bundle.containsKey(CURRENT_VEHICLE_MODEL_IMAGE_URL)) {
            mCurrentVehicleModelImageUri = bundle.getString(CURRENT_VEHICLE_MODEL_IMAGE_URL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.vehicle_details_fragment, container, false);

        mVehicleConnectionState = NordicBleService.getConnectionState();

        setupVehicleVariable();

        setupViews();

        populateVehicleDetailsViews();

        return mRootView;
    }

    private void setupVehicleVariable() {
        if(getActivity() == null) {
            return;
        }

        mVehicle = new Gson().fromJson(mVehicleObjectStr, Vehicle.class);

    }

    private boolean checkIfCurrentUserIsOwner() {

        if(getActivity() == null) {
            return false;
        }

        String deviceOwnerId = null, currentUserId = null;
        User loggedInUserDetails;


        loggedInUserDetails = PrefUtils.getInstance(getActivity()).getUserObjectFromPrefs();

        if(loggedInUserDetails == null) {
            return false;
        }

        deviceOwnerId = mVehicle.getOwnerId();
        currentUserId = loggedInUserDetails.getId();

        if(currentUserId != null && deviceOwnerId != null) {
            if (currentUserId.equals(deviceOwnerId)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private void setupViews() {

        mVehicleDetailsBackButtonImageView = mRootView.findViewById(R.id.vehicle_details_back_button_image_view);
        mVehicleSettingsIconImageView = mRootView.findViewById(R.id.my_vehicle_settings_icon_image_view);
        mConnectToVehicleButton = mRootView.findViewById(R.id.vehicle_details_connect_to_vehicle_button);
        mVehicleModelCircleImageView = mRootView.findViewById(R.id.vehicle_details_model_circle_image_view);
        mChangePinImageView = mRootView.findViewById(R.id.vehicle_details_change_pin_image_view);


        mVehicleVinTextView = mRootView.findViewById(R.id.vehicle_details_vin_value_text_view);
        mVehicleModelNameTextView = mRootView.findViewById(R.id.vehicle_details_model_name_value_text_view);
        mVehiclePinTextView = mRootView.findViewById(R.id.vehicle_details_pin_value_text_view);


        mTapToViewVehicleInfoTextView = mRootView.findViewById(R.id.tap_to_view_vehicle_info_text_view);
        mTapToViewVehicleHealthTextView = mRootView.findViewById(R.id.tap_to_view_vehicle_health_text_view);


        mThrottleDescTextView = mRootView.findViewById(R.id.vehicle_health_throttle_desc_text_view);
        mControllerDescTextView = mRootView.findViewById(R.id.vehicle_health_controller_desc_text_view);
        mMotorDescTextView = mRootView.findViewById(R.id.vehicle_health_motor_desc_text_view);
        mTemperatureDescTextView = mRootView.findViewById(R.id.vehicle_health_temperature_desc_text_view);
        mOverloadDescTextView = mRootView.findViewById(R.id.vehicle_health_overload_desc_text_view);

        mFirmwareVersionNumberTextView = mRootView.findViewById(R.id.vehicle_details_firmware_version_text_view);


        mBuyerLabelTextView = mRootView.findViewById(R.id.buyer_info_label_text_view);
        mBuyerNameTextView = mRootView.findViewById(R.id.buyer_info_buyer_name_text_view);
        mBuyerPhoneTextView = mRootView.findViewById(R.id.buyer_info_phone_text_view);
        mBuyerDateOfPurchaseTextView = mRootView.findViewById(R.id.buyer_info_date_of_purchase_text_view);

        mOwnerLabelLinearLayout = mRootView.findViewById(R.id.owner_info_label_linear_layout);
        mOwnerNameTextView = mRootView.findViewById(R.id.owner_info_name_text_view);
        mOwnerPhoneTextView = mRootView.findViewById(R.id.owner_info_phone_text_view);
        mOwnerEmailTextView = mRootView.findViewById(R.id.owner_info_email_text_view);

        mWarrantyLabelTextView = mRootView.findViewById(R.id.warranty_info_label_text_view);
        mWarrantyValidTillTextView = mRootView.findViewById(R.id.warranty_info_valid_till_text_view);
        mWarrantyExpiresInTextView = mRootView.findViewById(R.id.warranty_info_expires_in_text_view);

        mDistributorNameTextView = mRootView.findViewById(R.id.distributor_name_text_view);
        mDistributorPhoneTextView = mRootView.findViewById(R.id.distributor_phone_text_view);
        mDistributorAddressTextView = mRootView.findViewById(R.id.distributor_address_text_view);


        mOwnerInfoEmailSeparatorView = mRootView.findViewById(R.id.owner_info_email_separator_view);


        mVehicleInfoButton = mRootView.findViewById(R.id.vehicle_details_vehicle_info_button);
        mVehicleHealthInfoButton = mRootView.findViewById(R.id.vehicle_details_vehicle_health_button);
        mVehicleInfoHealthButtonsLinearLayout = mRootView.findViewById(R.id.vehicle_details_info_health_buttons_linear_layout);
        mConnectToViewVehicleHealthCardView = mRootView.findViewById(R.id.vehicle_disconnected_for_health_card_view);


        mVehiclePinLinearLayout = mRootView.findViewById(R.id.vehicle_details_pin_linear_layout);
        mVehicleInfoDetailsLinearLayout = mRootView.findViewById(R.id.vehicle_info_details_linear_layout);
        mOwnerEmailLinearLayout = mRootView.findViewById(R.id.owner_info_email_linear_layout);


        mVehicleHealthCardView = mRootView.findViewById(R.id.vehicle_health_card_view);
        mBuyerInfoCardView = mRootView.findViewById(R.id.buyer_info_card_view);
        mOwnerInfoCardView = mRootView.findViewById(R.id.owner_info_card_view);
        mWarrantyInfoCardView = mRootView.findViewById(R.id.warranty_info_card_view);


        mThrottleLottieImageView = mRootView.findViewById(R.id.vehicle_health_throttle_image_view);
        mControllerLottieImageView = mRootView.findViewById(R.id.vehicle_health_controller_image_view);
        mMotorLottieImageView = mRootView.findViewById(R.id.vehicle_health_motor_image_view);
        mTemperatureLottieImageView = mRootView.findViewById(R.id.vehicle_health_temperature_image_view);
        mOverloadLottieImageView = mRootView.findViewById(R.id.vehicle_health_overload_image_view);


        //setting visibility
        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(mVehicle == null) {
            return;
        }

        //if connected to vehicle currently being viewed
        if(vehicle != null && mVehicle.getVin().equals(vehicle.getVin())) {

            mConnectToVehicleButton.setVisibility(View.GONE);
            mVehicleSettingsIconImageView.setVisibility(View.VISIBLE);
        } else {

            mConnectToVehicleButton.setVisibility(View.VISIBLE);
            mVehicleSettingsIconImageView.setVisibility(View.GONE);
        }

        //pin layout
        if(mVehicle.getBuyerPhoneNo() != null && !mVehicle.getBuyerPhoneNo().isEmpty()) {
            mVehiclePinLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mVehiclePinLinearLayout.setVisibility(View.GONE);
        }

        //vehicle health layout
        if(mVehicle != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mVehicleSettingsIconImageView.setVisibility(View.INVISIBLE);
            mVehicleInfoHealthButtonsLinearLayout.setVisibility(View.INVISIBLE);
            mVehicleHealthCardView.setVisibility(View.GONE);

        }

        //owner email to be shown if user is owner
        if(checkIfCurrentUserIsOwner()) {
            mOwnerInfoEmailSeparatorView.setVisibility(View.VISIBLE);
            mOwnerEmailLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mOwnerInfoEmailSeparatorView.setVisibility(View.GONE);
            mOwnerEmailLinearLayout.setVisibility(View.GONE);
        }

        //hide buyer, owner, warranty details if vehicle not SOLD
        if(mVehicle != null && mVehicle.getStatus() != null && mVehicle.getStatus() == VehicleStatus.SOLD) {

            mBuyerLabelTextView.setVisibility(View.VISIBLE);
            mBuyerInfoCardView.setVisibility(View.VISIBLE);

            mOwnerLabelLinearLayout.setVisibility(View.VISIBLE);
            mOwnerInfoCardView.setVisibility(View.VISIBLE);

            mWarrantyLabelTextView.setVisibility(View.VISIBLE);
            mWarrantyInfoCardView.setVisibility(View.VISIBLE);

        } else {

            mBuyerLabelTextView.setVisibility(View.GONE);
            mBuyerInfoCardView.setVisibility(View.GONE);

            mOwnerLabelLinearLayout.setVisibility(View.GONE);
            mOwnerInfoCardView.setVisibility(View.GONE);

            mWarrantyLabelTextView.setVisibility(View.GONE);
            mWarrantyInfoCardView.setVisibility(View.GONE);

        }

        //show hide vehicle info and health button description textViews
        if(mVehicleInfoHealthButtonsLinearLayout.getVisibility() == View.VISIBLE) {
            if (mVehicleInfoDetailsLinearLayout.getVisibility() == View.VISIBLE) {
                mTapToViewVehicleHealthTextView.setVisibility(View.VISIBLE);
                mTapToViewVehicleInfoTextView.setVisibility(View.INVISIBLE);

            } else if (mVehicleHealthCardView.getVisibility() == View.VISIBLE) {
                mTapToViewVehicleInfoTextView.setVisibility(View.VISIBLE);
                mTapToViewVehicleHealthTextView.setVisibility(View.INVISIBLE);

            }
        }

        //setup onClick Listeners
        mVehicleDetailsBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        mVehicleSettingsIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null) {
                    return;
                }

                if(NordicBleService.getConnectionState() == STATE_CONNECTED) {
                    mVehicleSettingsIconImageView.setEnabled(false);
                    Intent myIntent = new Intent(getActivity(), VehicleSettingsActivity.class);
                    startActivity(myIntent);
                } else if(NordicBleService.getConnectionState() == STATE_DISCONNECTED){

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_connect_to_vehicle_to_access_settings));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

       mChangePinImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                mChangePinImageView.setEnabled(false);

                startChangePinActivity();
            }
        });

        mVehiclePinTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                mChangePinImageView.setEnabled(false);

                startChangePinActivity();
            }
        });

        mVehicleInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null || mVehicleInfoHealthButtonsLinearLayout.getVisibility() != View.VISIBLE) {
                    return;
                }

                mTapToViewVehicleHealthTextView.setVisibility(View.VISIBLE);
                mTapToViewVehicleInfoTextView.setVisibility(View.INVISIBLE);

                mConnectToViewVehicleHealthCardView.setVisibility(View.GONE);
                mVehicleHealthCardView.setVisibility(View.GONE);

                mVehicleInfoDetailsLinearLayout.setVisibility(View.VISIBLE);

                //set button style
                mVehicleInfoButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.capsule_button_green_filled_background));
                mVehicleInfoButton.setTextAppearance(getActivity(), R.style.StyleButtonFilledText16sp);
                mVehicleHealthInfoButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.capsule_button_stroke_background));
                mVehicleHealthInfoButton.setTextAppearance(getActivity(), R.style.StyleButtonStrokeText16sp);

            }
        });

        mVehicleHealthInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null || mVehicleInfoHealthButtonsLinearLayout.getVisibility() != View.VISIBLE) {
                    return;
                }

                mTapToViewVehicleHealthTextView.setVisibility(View.INVISIBLE);
                mTapToViewVehicleInfoTextView.setVisibility(View.VISIBLE);
                mVehicleInfoDetailsLinearLayout.setVisibility(View.GONE);

                if(NordicBleService.getConnectionState() == STATE_DISCONNECTED) {
                    mConnectToViewVehicleHealthCardView.setVisibility(View.VISIBLE);
                    mVehicleHealthCardView.setVisibility(View.GONE);
                } else {
                    mConnectToViewVehicleHealthCardView.setVisibility(View.GONE);
                    mVehicleHealthCardView.setVisibility(View.VISIBLE);
                }

                //set button style
                mVehicleInfoButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.capsule_button_stroke_background));
                mVehicleInfoButton.setTextAppearance(getActivity(), R.style.StyleButtonStrokeText16sp);
                mVehicleHealthInfoButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.capsule_button_green_filled_background));
                mVehicleHealthInfoButton.setTextAppearance(getActivity(), R.style.StyleButtonFilledText16sp);
            }
        });

        mConnectToVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                Intent myIntent = new Intent(getActivity(), ConnectUsingVinActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString(CONNECT_USING_VIN_KEY, mVehicle.getVin());
                myIntent.putExtras(bundle);
                startActivity(myIntent);

                getActivity().finish();
            }
        });
    }

    private void populateVehicleDetailsViews() {
        if(getActivity() == null) {
            return;
        }

        if(mVehicle == null) {
            //there has been an error, this situation should not occur
            assert getFragmentManager() != null;
            getFragmentManager().popBackStack();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            getActivity().finish();
            return;
        }

        //vehicle VIN
        if(mVehicle.getVin() != null && !mVehicle.getVin().isEmpty()) {
            mVehicleVinTextView.setText(mVehicle.getVin());
        } else {
            mVehicleVinTextView.setText(getString(R.string. not_available));
        }

        //vehicle model name
        if(mVehicle.getModel() != null && mVehicle.getModel().getName() != null && !mVehicle.getModel().getName().isEmpty()) {
            mVehicleModelNameTextView.setText(mVehicle.getModel().getName());
        } else {
            mVehicleModelNameTextView.setText(getString(R.string.not_available));
        }

        //vehicle PIN
        if (mVehiclePin != null && !mVehiclePin.isEmpty() && checkIfCurrentUserIsOwner()) {
            mVehiclePinTextView.setText(mVehiclePin);
        } else {
            mVehiclePinTextView.setText(getString(R.string.not_available));
        }

        if(!checkIfCurrentUserIsOwner()) {
            mChangePinImageView.setVisibility(View.INVISIBLE);
        }

        //vehicle buyer's name (purchased by), phone and purchase date
        String buyerFirstName = "", buyerLastName = "";
        if(mVehicle.getBuyerFirstName() != null) {
            buyerFirstName = mVehicle.getBuyerFirstName();
        }
        if(mVehicle.getBuyerLastName() != null) {
            buyerLastName = mVehicle.getBuyerLastName();
        }
        if(buyerFirstName.isEmpty() && buyerLastName.isEmpty()) {
            mBuyerNameTextView.setText(getString(R.string.not_available));
        } else {
            mBuyerNameTextView.setText(String.format("%s %s", buyerFirstName, buyerLastName));
        }

        if(mVehicle.getBuyerPhoneNo() != null && !mVehicle.getBuyerPhoneNo().isEmpty()) {
            mBuyerPhoneTextView.setText(mVehicle.getBuyerPhoneNo());
        } else {
            mBuyerPhoneTextView.setText(getString(R.string.not_available));
        }

        if(mVehicle.getInvoiceCreationDate() != null) {
            DateTime dateTime = new DateTime(mVehicle.getInvoiceCreationDate());
            Date purchasedOnDate = dateTime.toDate();
            String dateFormat = "dd-MMM-yy";
            String purchasedOnStr = new SimpleDateFormat(dateFormat).format(purchasedOnDate);

            if(mVehicle.getInvoiceStatus() != null && mVehicle.getInvoiceStatus().equals(InvoiceStatus.PENDING)) {
                mBuyerDateOfPurchaseTextView.setText(String.format("%s %s", purchasedOnStr, getString(R.string.invoice_payment)));
            } else {
                mBuyerDateOfPurchaseTextView.setText(purchasedOnStr);
            }
        } else {
            mBuyerDateOfPurchaseTextView.setText(getString(R.string.not_available));
        }

        //vehicle owner's name, phone and email
        String ownersFirstName = "", ownersLastName = "";
        if(mVehicle.getOwnerFirstName() != null) {
            ownersFirstName = mVehicle.getOwnerFirstName();
        }
        if(mVehicle.getOwnerLastName() != null) {
            ownersLastName = mVehicle.getOwnerLastName();
        }
        if(ownersFirstName.isEmpty() && ownersLastName.isEmpty()) {
            mOwnerNameTextView.setText(getString(R.string.not_available));
        } else {
            mOwnerNameTextView.setText(String.format("%s %s", ownersFirstName, ownersLastName));
        }

        if(mVehicle.getOwnerPhoneNo() != null && !mVehicle.getOwnerPhoneNo().isEmpty()) {
            mOwnerPhoneTextView.setText(mVehicle.getOwnerPhoneNo());
        } else {
            mOwnerPhoneTextView.setText(getString(R.string.not_available));
        }

        if(mVehicle.getOwnerEmailId() != null && !mVehicle.getOwnerEmailId().isEmpty() && mOwnerEmailLinearLayout.getVisibility() == View.VISIBLE) {
            mOwnerEmailTextView.setText(mVehicle.getOwnerEmailId());
        } else {
            mOwnerEmailTextView.setText(getString(R.string.not_available));
        }

        //vehicle warranty details
        if(mVehicle.getWarrantyExpiryTime() != null && !mVehicle.getWarrantyExpiryTime().isEmpty()) {
            //set trip date and time
            String dateFormat = "dd-MMM-yy";

            DateTime warrantyExpiryDateTime = new DateTime(mVehicle.getWarrantyExpiryTime());
            Date warrantyExpiryDate = warrantyExpiryDateTime.toDate();

            String expiryDate = new SimpleDateFormat(dateFormat).format(warrantyExpiryDate);
            String expiryTime = DateTimeUtils.getInstance(getActivity()).generateTimeRemainingFromNowString(warrantyExpiryDateTime);

            if(expiryTime.equals("")) {
                mWarrantyExpiresInTextView.setText(getString(R.string.warranty_expired));
                mWarrantyExpiresInTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_ic_battery_low_light));
            } else {
                if(!expiryTime.contains(getString(R.string.years_remaining)) && !expiryTime.contains(getString(R.string.months_remaining))) {
                    mWarrantyExpiresInTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_ic_battery_low_light));
                }
                mWarrantyExpiresInTextView.setText(String.format("%s %s", expiryTime, getString(R.string.time_remaining)));
            }

            if(!expiryDate.isEmpty()) {
                mWarrantyValidTillTextView.setText(String.format("%s %s", getString(R.string.warranty_valid_till), expiryDate));
            } else {
                mWarrantyValidTillTextView.setText(getString(R.string.not_available));
            }
        } else {
            mWarrantyValidTillTextView.setText(getString(R.string.not_available));
            mWarrantyExpiresInTextView.setText(getString(R.string.not_available));
        }

        //vehicle distributor details
        if(mVehicle.getDistributorName() != null && !mVehicle.getDistributorName().isEmpty()) {
            mDistributorNameTextView.setText(mVehicle.getDistributorName());
        } else {
            mDistributorNameTextView.setText(getString(R.string.not_available));
        }

        if(mVehicle.getDistributorPhoneNo1() != null && !mVehicle.getDistributorPhoneNo1().isEmpty()) {

            if(mVehicle.getDistributorPhoneNo2() != null && !mVehicle.getDistributorPhoneNo2().isEmpty()) {
                mDistributorPhoneTextView.setText(String.format("%s, %s", mVehicle.getDistributorPhoneNo1(), mVehicle.getDistributorPhoneNo2()));
            } else {
                mDistributorPhoneTextView.setText(mVehicle.getDistributorPhoneNo1());
            }
        } else if(mVehicle.getDistributorPhoneNo2() != null && !mVehicle.getDistributorPhoneNo2().isEmpty()) {
            mDistributorPhoneTextView.setText(mVehicle.getDistributorPhoneNo2());
        } else {
            mDistributorPhoneTextView.setText(getString(R.string.not_available));
        }

        if(mVehicle.getDistributorAddress() != null && !mVehicle.getDistributorAddress().isEmpty()) {
            mDistributorAddressTextView.setText(mVehicle.getDistributorAddress());
        } else {
            mDistributorAddressTextView.setText(getString(R.string.not_available));
        }

        loadModelImage();
    }

    private void startChangePinActivity() {

        if(NordicBleService.getConnectionState() == STATE_CONNECTED) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_disconnect_from_vehicle_to_change_pin));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            //get log packets count
            int unsyncedLiveLogFilesCount = RevosFileUtils.getInstance(getActivity()).getStoredLogFileCount(LOG_TYPE_LIVE);
            int unsyncedOfflineLogFilesCount = RevosFileUtils.getInstance(getActivity()).getStoredLogFileCount(LOG_TYPE_OFFLINE);
            int unsyncedOfflineBatteryLogFilesCount = RevosFileUtils.getInstance(getActivity()).getStoredLogFileCount(LOG_TYPE_OFFLINE_BATTERY_ADC);

            if(unsyncedLiveLogFilesCount > 0 || unsyncedOfflineLogFilesCount > 0 || unsyncedOfflineBatteryLogFilesCount > 0) {

                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.log_sync_pending_title))
                        .setMessage(R.string.log_sync_pending_message)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //Start change vehicle PIN activity
                                Intent myIntent = new Intent(getActivity(), ChangePinActivity.class);
                                myIntent.putExtra(VIN_ARG_KEY, mVehicle.getVin());
                                myIntent.putExtra(PHONE_ARG_KEY, mVehicle.getBuyerPhoneNo());
                                myIntent.putExtra(RECONNECT_TO_DEVICE_ARG_KEY, false);
                                startActivity(myIntent);
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .show();
            } else {
                Intent myIntent = new Intent(getActivity(), ChangePinActivity.class);
                myIntent.putExtra(VIN_ARG_KEY, mVehicle.getVin());
                myIntent.putExtra(PHONE_ARG_KEY, mVehicle.getBuyerPhoneNo());
                myIntent.putExtra(RECONNECT_TO_DEVICE_ARG_KEY, false);
                startActivity(myIntent);
            }
        }
    }

    private void loadModelImage() {
        if(getActivity() == null) {
            return;
        }

        if(mCurrentVehicleModelImageUri == null || mCurrentVehicleModelImageUri.isEmpty()) {

            HashMap<String, String> modelsImagesHashMap = PrefUtils.getInstance(getActivity()).getModelImageHashMapObjectFromPrefs();

            if(modelsImagesHashMap == null) {
                return;
            }

            String modelImageUri = modelsImagesHashMap.get(mVehicle.getModel().getId());
            if(modelImageUri != null && !modelImageUri.isEmpty()) {
                mVehicleModelCircleImageView.setPadding(convertDipToPixels(0), convertDipToPixels(0), convertDipToPixels(0), convertDipToPixels(0));

                Glide.with(getActivity())
                        .load(modelImageUri)
                        .fitCenter()
                        .into(mVehicleModelCircleImageView);
            }

        } else {
            mVehicleModelCircleImageView.setPadding(convertDipToPixels(0), convertDipToPixels(0), convertDipToPixels(0), convertDipToPixels(0));

            Glide.with(getActivity())
                    .load(mCurrentVehicleModelImageUri)
                    .fitCenter()
                    .into(mVehicleModelCircleImageView);
        }
    }

    private void populateVehicleHealthViews(String dataString) {
        if(getActivity() == null) {
            return;
        }

        //vehicle firmware version
        String deviceFirmwareVersion = NordicBleService.getDeviceFirmwareVersion();

        if(deviceFirmwareVersion != null && !deviceFirmwareVersion.isEmpty()) {
            mFirmwareVersionNumberTextView.setText(NordicBleService.getDeviceFirmwareVersion());
        } else {
            mFirmwareVersionNumberTextView.setText(getString(R.string.not_available));
        }

        if(mVehicleConnectionState == BluetoothProfile.STATE_DISCONNECTED) {
            updateViewsOnVehicleDisconnected(DEVICE_DISCONNECTED);

        } else if(mVehicleConnectionState == STATE_CONNECTED) {

            if (NordicBleService.mVehicleLiveParameters != null) {

                setVehicleHealthTextValues(NordicBleService.mVehicleLiveParameters);
                setVehicleHealthImageValue(NordicBleService.mVehicleLiveParameters);
            }
        }
    }

    private void setVehicleHealthTextValues(VehicleParameters vehicleParameters) {

        mThrottleDescTextView.setText(vehicleParameters.isThrottleError() ? "ERROR" : "OK");
        mControllerDescTextView.setText(vehicleParameters.isControllerError() ? "ERROR" : "OK");
        mMotorDescTextView.setText(vehicleParameters.isMotorError() ? "ERROR" : "OK");
        mTemperatureDescTextView.setText(String.format("%s ℃", vehicleParameters.getTemperature()));
        mOverloadDescTextView.setText(vehicleParameters.isOverLoad() ? "Overload" : "OK");

    }

    private void setVehicleHealthImageValue(VehicleParameters vehicleParameters) {

        mThrottleLottieImageView.setVisibility(View.VISIBLE);
        mControllerLottieImageView.setVisibility(View.VISIBLE);
        mMotorLottieImageView.setVisibility(View.VISIBLE);
        mTemperatureLottieImageView.setVisibility(View.VISIBLE);
        mOverloadLottieImageView.setVisibility(View.VISIBLE);

        if(mIsThrottleError != vehicleParameters.isThrottleError()) {
            if(!vehicleParameters.isThrottleError()) {
                mThrottleLottieImageView.loop(false);
                mThrottleLottieImageView.setAnimation(R.raw.ic_generic_message_success);
            } else {
                mThrottleLottieImageView.loop(true);
                mThrottleLottieImageView.setAnimation(R.raw.ic_generic_message_error);
            }
            mThrottleLottieImageView.setProgress(0);
            mThrottleLottieImageView.playAnimation();
        }

        if(mIsControllerError != vehicleParameters.isControllerError()) {
            if(!vehicleParameters.isControllerError()) {
                mControllerLottieImageView.loop(false);
                mControllerLottieImageView.setAnimation(R.raw.ic_generic_message_success);
            } else {
                mControllerLottieImageView.loop(true);
                mControllerLottieImageView.setAnimation(R.raw.ic_generic_message_error);
            }
            mControllerLottieImageView.setProgress(0);
            mControllerLottieImageView.playAnimation();
        }

        if(mIsMotorError != vehicleParameters.isMotorError()) {
            if(!vehicleParameters.isMotorError()) {
                mMotorLottieImageView.loop(false);
                mMotorLottieImageView.setAnimation(R.raw.ic_generic_message_success);
            } else {
                mMotorLottieImageView.loop(true);
                mMotorLottieImageView.setAnimation(R.raw.ic_generic_message_error);
            }
            mMotorLottieImageView.setProgress(0);
            mMotorLottieImageView.playAnimation();
        }

        if(mIsTemperatureError != (vehicleParameters.getTemperature() < OVER_TEMPERATURE_LIMIT)) {
            if(vehicleParameters.getTemperature() < OVER_TEMPERATURE_LIMIT) {
                mTemperatureLottieImageView.loop(false);
                mTemperatureLottieImageView.setAnimation(R.raw.ic_generic_message_success);
            } else {
                mTemperatureLottieImageView.loop(true);
                mTemperatureLottieImageView.setAnimation(R.raw.ic_generic_message_error);
            }
            mTemperatureLottieImageView.setProgress(0);
            mTemperatureLottieImageView.playAnimation();
        }

        if(mIsOverload != vehicleParameters.isOverLoad()) {
            if(!vehicleParameters.isOverLoad()) {
                mOverloadLottieImageView.loop(false);
                mOverloadLottieImageView.setAnimation(R.raw.ic_generic_message_success);
            } else {
                mOverloadLottieImageView.loop(true);
                mOverloadLottieImageView.setAnimation(R.raw.ic_generic_message_error);
            }
            mOverloadLottieImageView.setProgress(0);
            mOverloadLottieImageView.playAnimation();
        }

        mIsThrottleError = vehicleParameters.isThrottleError();
        mIsControllerError = vehicleParameters.isControllerError();
        mIsMotorError = vehicleParameters.isMotorError();
        mIsTemperatureError = vehicleParameters.getTemperature() < OVER_TEMPERATURE_LIMIT;
        mIsOverload = vehicleParameters.isOverLoad();

    }

    private void updateViewsOnVehicleDisconnected(String vehicleState) {
        mThrottleDescTextView.setText(vehicleState.equals(DEVICE_LOCKED) ? R.string.vehicle_locked : R.string.vehicle_not_connected);
        mThrottleLottieImageView.setVisibility(View.INVISIBLE);

        mControllerDescTextView.setText(vehicleState.equals(DEVICE_LOCKED) ? R.string.vehicle_locked : R.string.vehicle_not_connected);
        mControllerLottieImageView.setVisibility(View.INVISIBLE);

        mMotorDescTextView.setText(vehicleState.equals(DEVICE_LOCKED) ? R.string.vehicle_locked : R.string.vehicle_not_connected);
        mMotorLottieImageView.setVisibility(View.INVISIBLE);

        mTemperatureDescTextView.setText(vehicleState.equals(DEVICE_LOCKED) ? R.string.vehicle_locked : R.string.vehicle_not_connected);
        mTemperatureLottieImageView.setVisibility(View.INVISIBLE);

        mOverloadDescTextView.setText(vehicleState.equals(DEVICE_LOCKED) ? R.string.vehicle_locked : R.string.vehicle_not_connected);
        mOverloadLottieImageView.setVisibility(View.INVISIBLE);
    }

    @Subscribe
    public void onEventBusMessage(final EventBusMessage event) {
        if(event.message.contains(OBD_EVENT_LIVE_DATA)) {
            final String dataString = event.message.split(GEN_DELIMITER)[1];

            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    populateVehicleHealthViews(dataString);
                }
            });
        } else if(event.message.contains(BT_EVENT_DEVICE_LOCKED)) {

            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateViewsOnVehicleDisconnected(DEVICE_LOCKED);
                }
            });
        } else if(event.message.equals(NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateVehicleDetailsViews();
                }
            });
        }
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mVehicleSettingsIconImageView != null) {
            mVehicleSettingsIconImageView.setEnabled(true);
        }

        if(mChangePinImageView != null) {
            mChangePinImageView.setEnabled(true);
        }

        if(mVehiclePinTextView != null) {
            mVehiclePinTextView.setEnabled(true);
        }

        populateVehicleDetailsViews();
    }
}