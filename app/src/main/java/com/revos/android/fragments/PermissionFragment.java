package com.revos.android.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.revos.android.R;

/**
 * Created by mohit on 6/9/17.
 */

public class PermissionFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**views and layouts*/
        View rootView = inflater.inflate(R.layout.permission_fragment, container, false);

        return rootView;
    }
}
