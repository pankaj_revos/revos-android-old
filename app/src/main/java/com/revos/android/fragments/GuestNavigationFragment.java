package com.revos.android.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.SphericalUtil;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.LiveTrackingActivity;
import com.revos.android.activities.MainActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.SavedPlace;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.services.NordicBleService;
import com.revos.android.services.NotificationReaderService;
import com.revos.android.utilities.adapters.SavedPlacesAdapter;
import com.revos.android.utilities.animations.ResizeAnimation;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.type.ModelProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GOOGLE_MAPS_PACKAGE_NAME;
import static com.revos.android.constants.Constants.LAST_GOOGLE_NOTIFICATION_TIMESTAMP;
import static com.revos.android.constants.Constants.NAV_EVENT_SAVED_PLACE_LIST_UPDATED;
import static com.revos.android.constants.Constants.NAV_EVENT_TRIP_ADVICE_DATA_UPDATED;
import static com.revos.android.constants.Constants.NAV_PREFS;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.constants.Constants.SAVED_PLACES_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DESTINATION_NAME_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DISTANCE_FROM_TURN_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_ETA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_IS_REROUTING_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_NEXT_ROAD_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_REMAINING_DISTANCE_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_REMAINING_TIME_KEY;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LATITUDE;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

/**
 * Created by mohit on 11/9/17.
 */

public class GuestNavigationFragment extends Fragment{
    /**views and layouts*/
    private View rootView;

    private final int LOCATION_BIAS_RADIUS_IN_METERS = 70000;

    private Animation mResizeCurrentDestinationCardAnimation;

    /**Timer for polling*/
    private Timer mTimer;

    private CardView mCurrentDestinationCardView, mAddDestinationCardView, mEnterDestinationCardView, mVehicleLastKnownLocationCardView;

    private ImageView mTurnIconImageView;

    private TextView mCurrentDestinationTextView, mDistanceFromTurnTextView, mNextRoadTextView, mETATextView;

    private LinearLayout mSavedPlacesLinearLayout;

    /**saved places list view*/
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SavedPlacesAdapter mSavedPlacesAdapter;

    private ArrayList<SavedPlace> mSavedPlacesList;

    private int PLACE_PICKER_SET_DESTINATION_REQUEST = 100;
    private int PLACE_PICKER_ADD_DESTINATION_REQUEST = 200;

    private final int CUR_DEST_CARD_MAX_HEIGHT_IN_DP = 160;
    private final int CUR_DEST_CARD_MIN_HEIGHT_IN_DP = 0;

    private String mVehicleLatitude, mVehicleLongitude;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.navigation_fragment, container, false);

        setupViews();

        setupTimer();

        return rootView;
    }

    private void setupViews() {
        //setup the enter destination card view
        mEnterDestinationCardView = (CardView) rootView.findViewById(R.id.nav_enter_destination_card_view);
        mEnterDestinationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEnterDestinationCardView.setEnabled(false);
                startPlacePickerActivity(PLACE_PICKER_SET_DESTINATION_REQUEST);
            }
        });

        setupCurrentDestinationRelatedViews();

        initializeSavedPlacesList();

        setupAddDestinationCard();

        setupVehicleLastKnownLocation();

    }

    private void setupAddDestinationCard() {

        mAddDestinationCardView = (CardView)rootView.findViewById(R.id.nav_add_destination_card_view);
        mAddDestinationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAddDestinationCardView.setEnabled(false);
                startPlacePickerActivity(PLACE_PICKER_ADD_DESTINATION_REQUEST);
            }
        });
    }

    private void setupVehicleLastKnownLocation() {

        if(getActivity() == null) {
            return;
        }

        mVehicleLastKnownLocationCardView = rootView.findViewById(R.id.nav_vehicle_last_location_card_view);
        mVehicleLastKnownLocationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });
    }

    private void startPlacePickerActivity(int requestCode) {
        if(getActivity() == null) {
            return;
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
        Intent intent;
        if(NordicBleService.mCurrentLocation == null) {

            // Start the autocomplete intent.
             intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields)
                    .build(getActivity());
        } else {

            RectangularBounds rectangularBounds = RectangularBounds.newInstance(getRectangularBounds(new LatLng(NordicBleService.mCurrentLocation.getLatitude(), NordicBleService.mCurrentLocation.getLongitude()), LOCATION_BIAS_RADIUS_IN_METERS));
            // Start the autocomplete intent.
            intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields)
                    .setLocationBias(rectangularBounds)
                    .build(getActivity());
        }
        startActivityForResult(intent, requestCode);
    }

    private LatLngBounds getRectangularBounds(LatLng center, double radiusInMeters) {
        double distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0);
        return new LatLngBounds(southwestCorner, northeastCorner);
    }

    private void setupCurrentDestinationRelatedViews() {
        mCurrentDestinationCardView = (CardView)rootView.findViewById(R.id.nav_current_destination_card_view);
        mCurrentDestinationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentDestinationCardView.setEnabled(false);
                startGoogleMaps();
            }
        });

        mCurrentDestinationTextView = (TextView)rootView.findViewById(R.id.nav_current_destination_text_view);
        mDistanceFromTurnTextView = (TextView)rootView.findViewById(R.id.nav_distance_from_turn_text_view);
        mNextRoadTextView = (TextView)rootView.findViewById(R.id.nav_next_road_text_view);
        mETATextView = (TextView)rootView.findViewById(R.id.nav_eta_view);

        mTurnIconImageView = (ImageView)rootView.findViewById(R.id.nav_turn_icon_image_view);
    }

    private void startGoogleMaps() {
        Uri gmmIntentUri = Uri.parse("geo:0,0");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage(GOOGLE_MAPS_PACKAGE_NAME);
        startActivity(mapIntent);
    }

    private void startNavigation(String name, String address) {
        String queryString = name + ", " + address;
        String mapQueryString = "google.navigation:q=" + queryString + "&mode=d";

        Uri gmmIntentUri = Uri.parse(mapQueryString);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage(GOOGLE_MAPS_PACKAGE_NAME);
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mapIntent);
    }

    private void initializeSavedPlacesList() {
        if(getActivity() == null){
            return;
        }

        //setup the linear layout that holds the saved places list
        mSavedPlacesLinearLayout = (LinearLayout)rootView.findViewById(R.id.saved_place_linear_layout);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.nav_recycler_view);

        String savedPlacesListStr = null;
        savedPlacesListStr = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(SAVED_PLACES_DATA_KEY, null);
        
        if(savedPlacesListStr == null) {
            mSavedPlacesLinearLayout.setVisibility(View.GONE);
        } else {
            mSavedPlacesList = new Gson().fromJson(savedPlacesListStr, new TypeToken<ArrayList<SavedPlace>>() {}.getType());
            if(mSavedPlacesList.isEmpty())
                mSavedPlacesLinearLayout.setVisibility(View.GONE);
        }

        //use a grid layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //set the adapter
        mSavedPlacesAdapter = new SavedPlacesAdapter(mSavedPlacesList, getActivity());
        mRecyclerView.setAdapter(mSavedPlacesAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PLACE_PICKER_SET_DESTINATION_REQUEST) {
            if(resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                startNavigation(place.getName(), place.getAddress());
            }
        } else if(requestCode == PLACE_PICKER_ADD_DESTINATION_REQUEST) {
            if(resultCode == RESULT_OK) {
                if(getActivity() == null){
                    return;
                }

                Place place = Autocomplete.getPlaceFromIntent(data);

                SavedPlace savedPlace = new SavedPlace();

                if(place != null && place.getLatLng() != null) {
                    savedPlace.setLatitude(place.getLatLng().latitude);
                    savedPlace.setLongitude(place.getLatLng().longitude);
                }

                savedPlace.setName(place.getName());
                savedPlace.setAddress(place.getAddress());

                mSavedPlacesAdapter.add(0, savedPlace);

                String savedPlacesListStr;
                savedPlacesListStr = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(SAVED_PLACES_DATA_KEY, null);

                if(savedPlacesListStr != null) {
                    mSavedPlacesList = new Gson().fromJson(savedPlacesListStr, new TypeToken<ArrayList<SavedPlace>>(){}.getType());
                    if(!mSavedPlacesList.isEmpty()) {
                        mSavedPlacesLinearLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

    }

    private void setupTimer() {
        //start infinite polling for checking bluetooth connection state
        //check if notification advice has to be displayed or not
        if(mTimer == null) {
            mTimer = new Timer();
        }
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if(getActivity() == null || !getActivity().hasWindowFocus()) {
                                return;
                            }
                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }
                    }
                });
            }
        }, 100, 2000);
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED)) {
            if(getActivity() == null) {
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(NAV_EVENT_SAVED_PLACE_LIST_UPDATED)) {
            if(getActivity() == null) {
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }

                        String savedPlacesListStr;
                        savedPlacesListStr = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(SAVED_PLACES_DATA_KEY, null);

                        if(savedPlacesListStr == null) {
                            mSavedPlacesLinearLayout.setVisibility(View.GONE);
                        } else {
                            mSavedPlacesList = new Gson().fromJson(savedPlacesListStr, new TypeToken<ArrayList<SavedPlace>>() {}.getType());
                        }

                        if(mSavedPlacesList == null || mSavedPlacesList.isEmpty()) {
                            mSavedPlacesLinearLayout.setVisibility(View.GONE);
                        } else if(mSavedPlacesList != null && !mSavedPlacesList.isEmpty()) {
                            mSavedPlacesLinearLayout.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if (event.message.contains(OBD_EVENT_LIVE_DATA)) {
            if(getActivity() == null) {
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mAddDestinationCardView != null){
            mAddDestinationCardView.setEnabled(true);
        }

        if(mEnterDestinationCardView != null) {
            mEnterDestinationCardView.setEnabled(true);
        }

        if(mCurrentDestinationCardView != null) {
            mCurrentDestinationCardView.setEnabled(true);
        }

        if(mVehicleLastKnownLocationCardView != null) {
            mVehicleLastKnownLocationCardView.setEnabled(true);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }

        super.onDestroy();
    }

}
