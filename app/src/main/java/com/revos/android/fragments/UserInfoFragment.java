package com.revos.android.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.PhotoViewActivity;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.type.GenderType;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.GENERAL_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.FILE_PROVIDER;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GENERIC_TEMP_IMAGE_KEY;
import static com.revos.android.constants.Constants.LOG_OUT_FLAG_KEY;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED;
import static com.revos.android.constants.Constants.TEMP_IMAGE_PREFS;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_PROFILE_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_PROFILE_FRAG_EVENT_LOAD_EDIT_USER_FRAGMENT;
import static com.revos.android.constants.Constants.USER_PROFILE_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DETAILS_DOWNLOAD_SUCCESS;

/**
 * Created by mohit on 15/9/17.
 */

public class UserInfoFragment extends Fragment{
    private View mRootView;

    /**user profile screen layouts and views*/
    private CircleImageView mUserProfilePicCircleImageView, mUserEmergencyContact1CircleImageView, mUserEmergencyContact2CircleImageView;

    /**private local copy of user*/
    private User mUser;

    private ImageView mUserDriverLicenseSide1ImageView, mUserDriverLicenseSide2ImageView, mUserRefreshDetailsImageView, mUserBackButtonImageView;

    private TextView mUserFullNameTextView, mUserAgeGenderTextView, mUserContactTextView, mUserEmailTextView, mUserEmergencyContact1NumberTextView,
                     mUserEmergencyContact1NameTextView, mUserEmergencyContact2NumberTextView, mUserEmergencyContact2NameTextView;

    private Uri mProfilePicUri, mDlPic1Uri, mDlPic2Uri;

    private SharedPreferences mSharedPreferences;

    private Animation mAnimation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.user_info_fragment, container, false);

        setupAndPopulateUserProfilePage();

        return mRootView;
    }

    private void setupAndPopulateUserProfilePage() {
        if(getActivity() == null) {
            return;
        }

        mSharedPreferences = getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);

        mUser = PrefUtils.getInstance(getActivity()).getUserObjectFromPrefs();
        if(mUser == null) {
            //there has been an error, this situation should not occur
            getActivity().finish();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        //setup the user profile pic view
        mUserProfilePicCircleImageView = (CircleImageView)mRootView.findViewById(R.id.user_profile_pic_circle_image_view);
        loadProfilePic();

        //populate the name
        mUserFullNameTextView =(TextView) mRootView.findViewById(R.id.user_name_text_view);
        mUserFullNameTextView.setText(String.format("%s %s", mUser.getFirstName(), mUser.getLastName()));

        //populate user age and gender
        DateTime currentDateTime = new DateTime();
        DateTime dobDateTime = new DateTime(mUser.getDob());

        int ageInYears = new Period(dobDateTime, currentDateTime).getYears();

        String genderString = "";

        if(mUser.getGenderType() == GenderType.MALE) {
            genderString = getString(R.string.male);
        } else if(mUser.getGenderType() == GenderType.FEMALE) {
            genderString = getString(R.string.female);
        } else {
            genderString = getString(R.string.other);
        }

        mUserAgeGenderTextView = (TextView) mRootView.findViewById(R.id.user_age_gender_text_view);
        mUserAgeGenderTextView.setText(ageInYears + ", " + genderString);

        //populate the phone number and email
        mUserContactTextView = (TextView) mRootView.findViewById(R.id.user_contact_number_text_view);
        mUserContactTextView.setText(mUser.getPhone());

        mUserEmailTextView = (TextView) mRootView.findViewById(R.id.user_email_text_view);
        mUserEmailTextView.setText(mUser.getEmail());

        //populate emergency contacts
        mUserEmergencyContact1NumberTextView = (TextView) mRootView.findViewById(R.id.user_first_emergency_contact_number_text_view);
        mUserEmergencyContact1NumberTextView.setText(mUser.getAltPhone1());
        String firstEmergencyContactName = PhoneUtils.getInstance(getActivity()).findContactName(mUser.getAltPhone1());
        if(firstEmergencyContactName == null) {
            firstEmergencyContactName = getString(R.string.unknown);
        }
        mUserEmergencyContact1NameTextView = (TextView) mRootView.findViewById(R.id.user_first_emergency_contact_name_text_view);
        mUserEmergencyContact1NameTextView.setText(firstEmergencyContactName);

        //set emergency contact 1 photo
        mUserEmergencyContact1CircleImageView = (CircleImageView) mRootView.findViewById(R.id.user_first_emergency_contact_circle_image_view);
        Uri ec1PhotoUri = PhoneUtils.getInstance(getActivity()).retrieveContactThumbPhotoUri(mUser.getAltPhone1());
        Picasso.with(getActivity())
                .load(ec1PhotoUri).fit().centerInside()
                .placeholder(ContextCompat.getDrawable(getActivity(), R.drawable.ic_asset_avatar))
                .into(mUserEmergencyContact1CircleImageView);

        if(mUser.getAltPhone2() == null || mUser.getAltPhone2().isEmpty()) {
            (mRootView.findViewById(R.id.user_second_emergency_contact_card_view)).setVisibility(View.GONE);
        } else {
            mUserEmergencyContact2NumberTextView = (TextView) mRootView.findViewById(R.id.user_second_emergency_contact_number_text_view);
            mUserEmergencyContact2NumberTextView.setText(mUser.getAltPhone2());
            String secondEmergencyContactName = PhoneUtils.getInstance(getActivity()).findContactName(mUser.getAltPhone2());
            if(secondEmergencyContactName == null) {
                secondEmergencyContactName = getString(R.string.unknown);
            }

            mUserEmergencyContact2NameTextView = (TextView) mRootView.findViewById(R.id.user_second_emergency_contact_name_text_view);
            mUserEmergencyContact2NameTextView.setText(secondEmergencyContactName);

            //set emergency contact 2 photo
            mUserEmergencyContact2CircleImageView = (CircleImageView) mRootView.findViewById(R.id.user_second_emergency_contact_circle_image_view);
            Uri ec2PhotoUri = PhoneUtils.getInstance(getActivity()).retrieveContactThumbPhotoUri(mUser.getAltPhone2());
            Picasso.with(getActivity())
                    .load(ec2PhotoUri).fit().centerInside()
                    .placeholder(ContextCompat.getDrawable(getActivity(), R.drawable.ic_asset_avatar))
                    .into(mUserEmergencyContact2CircleImageView);
        }

        //setup driving license
        //setup the dl showing views
        mUserDriverLicenseSide1ImageView = (ImageView)mRootView.findViewById(R.id.user_driver_license_side_1_image_view);
        mUserDriverLicenseSide2ImageView = (ImageView)mRootView.findViewById(R.id.user_driver_license_side_2_image_view);
        loadBothDLPic();

        mUserProfilePicCircleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mProfilePicUri != null) {
                    Intent intent = new Intent(getActivity(), PhotoViewActivity.class);
                    if(getActivity() == null) {
                        return;
                    }
                    getActivity().getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).edit().putString(GENERIC_TEMP_IMAGE_KEY, mProfilePicUri.toString()).apply();
                    startActivity(intent);
                }
            }
        });

        mUserDriverLicenseSide1ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDlPic1Uri != null) {
                    Intent intent = new Intent(getActivity(), PhotoViewActivity.class);
                    if(getActivity() == null) {
                        return;
                    }
                    getActivity().getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).edit().putString(GENERIC_TEMP_IMAGE_KEY, mDlPic1Uri.toString()).apply();
                    startActivity(intent);
                }
            }
        });

        mUserDriverLicenseSide2ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDlPic2Uri != null) {
                    Intent intent = new Intent(getActivity(), PhotoViewActivity.class);
                    if(getActivity() == null) {
                        return;
                    }
                    getActivity().getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).edit().putString(GENERIC_TEMP_IMAGE_KEY, mDlPic1Uri.toString()).apply();
                    startActivity(intent);
                }
            }
        });

        //setup the back button
        mUserBackButtonImageView = (ImageView) mRootView.findViewById(R.id.user_back_button_image_view);
        mUserBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity() == null){
                    return;
                }

                getActivity().finish();

            }
        });

        //setup the edit button
        mRootView.findViewById(R.id.user_edit_button_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_LOAD_EDIT_USER_FRAGMENT));
            }
        });

        mUserRefreshDetailsImageView = (ImageView) mRootView.findViewById(R.id.user_refresh_details_image_view);
        mUserRefreshDetailsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity() == null) {
                    return;
                }

                Timber.i("on click refresh fragment");
                mAnimation = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF,
                        0.5f, Animation.RELATIVE_TO_SELF,0.5f);
                mAnimation.setInterpolator(new LinearInterpolator());
                mAnimation.setRepeatCount(Animation.INFINITE);
                mAnimation.setDuration(1000);

                mUserRefreshDetailsImageView.startAnimation(mAnimation);

                mUserRefreshDetailsImageView.setEnabled(false);

                NetworkUtils.getInstance(getActivity()).refreshUserDetails();
                Timber.i("after populating views");

            }
        });

        mRootView.findViewById(R.id.user_logout_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() != null) {
                    getActivity().getSharedPreferences(GENERAL_PREFS, Context.MODE_PRIVATE).edit().putBoolean(LOG_OUT_FLAG_KEY, true).apply();
                    getActivity().finish();
                }
            }
        });
    }

    private void loadProfilePic() {
        if(getActivity() == null) {
            return;
        }
        if(getActivity()
            .getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
            .getString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, null) != null) {
            //load user profile pic
            String profilePicFileName = mSharedPreferences.getString(USER_PROFILE_LOCAL_FILE_PATH_KEY, null);
            if(profilePicFileName != null) {
                File profilePicFile = new File(profilePicFileName);
                if(profilePicFile.exists()) {
                    Picasso.with(getActivity()).load(profilePicFile).fit().centerInside().into(mUserProfilePicCircleImageView);
                    mProfilePicUri = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, profilePicFile);
                }
            } else {
                //request networking service to download pic
                NetworkUtils.getInstance(getActivity()).downloadProfilePicIfRequired();
                //todo::change image to spinning icon
            }
        }
    }

    private void loadBothDLPic() {
        if(getActivity() == null) {
            return;
        }
        if(getActivity()
            .getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
            .getString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, null) != null) {
            loadDLPic1();
        }

        if(getActivity()
                .getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                .getString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, null) != null) {
            loadDLPic2();
        }
    }

    private void loadDLPic1() {

        if(getActivity() == null){
            return;
        }

        String dlPic1FileName = mSharedPreferences.getString(USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY, null);
        if(dlPic1FileName != null) {
            //hide the text view
            (mRootView.findViewById(R.id.user_scan_driver_license_side_1_text_view)).setVisibility(View.INVISIBLE);
            File dlPic1File = new File(dlPic1FileName);
            if(dlPic1File.exists()) {
                Picasso.with(getActivity()).load(dlPic1File).fit().centerInside().into(mUserDriverLicenseSide1ImageView);
                mDlPic1Uri = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, dlPic1File);
            }
        } else {
            //request networking service to download pic
            NetworkUtils.getInstance(getActivity()).downloadDLPicIfRequired();
            //todo::change image to spinning icon
        }
    }

    private void loadDLPic2() {
        if(getActivity() == null){
            return;
        }

        String dlPic2FileName = mSharedPreferences.getString(USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY, null);
        if(dlPic2FileName != null) {
            //hide the text view
            (mRootView.findViewById(R.id.user_scan_driver_license_side_2_text_view)).setVisibility(View.INVISIBLE);
            File dlPic2File = new File(dlPic2FileName);
            if(dlPic2File.exists()) {
                Picasso.with(getActivity()).load(dlPic2File).fit().centerInside().into(mUserDriverLicenseSide2ImageView);
                mDlPic2Uri = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, dlPic2File);
            }
        } else {
            //request networking service to download pic
            NetworkUtils.getInstance(getActivity()).downloadDLPicIfRequired();
            //todo::change image to spinning icon
        }
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadProfilePic();
                }
            });

        } else if(event.message.equals(NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED) || event.message.equals(NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadBothDLPic();
                }
            });

        } else if(event.message.equals(NETWORK_EVENT_USER_DETAILS_DOWNLOAD_SUCCESS)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mUserRefreshDetailsImageView.clearAnimation();
                    mUserRefreshDetailsImageView.setEnabled(true);
                    setupAndPopulateUserProfilePage();

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.profile_refresh_success));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        } else if(event.message.equals(NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mUserRefreshDetailsImageView.clearAnimation();
                    mUserRefreshDetailsImageView.setEnabled(true);
                    setupAndPopulateUserProfilePage();

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.profile_refresh_fail));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mUserRefreshDetailsImageView != null) {
            mUserRefreshDetailsImageView.setEnabled(true);
        }

        loadProfilePic();
        loadBothDLPic();
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
