package com.revos.android.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.ImageUtils;
import com.revos.android.utilities.general.PhoneUtils;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;
import com.revos.android.R;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.UpdateUserMutation;
import com.revos.scripts.UploadFileMutation;
import com.revos.scripts.type.FileType;
import com.revos.scripts.type.GenderType;
import com.revos.scripts.type.UploadInput;
import com.revos.scripts.type.UserUpdateInput;
import com.squareup.picasso.Picasso;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.DL_SIDE_1_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_1_TEMP_COMPRESSED_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_1_TEMP_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_2_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_2_TEMP_COMPRESSED_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_2_TEMP_PREFIX;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.FILE_PROVIDER;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED;
import static com.revos.android.constants.Constants.PIC_DIR_PATH;
import static com.revos.android.constants.Constants.PROFILE_PIC_PREFIX;
import static com.revos.android.constants.Constants.PROFILE_PIC_TEMP_COMPRESSED_PREFIX;
import static com.revos.android.constants.Constants.PROFILE_PIC_TEMP_PREFIX;
import static com.revos.android.constants.Constants.TEMP_PIC_DIR_PATH;
import static com.revos.android.constants.Constants.UPLOAD_TYPE_DL_SIDE_1;
import static com.revos.android.constants.Constants.UPLOAD_TYPE_DL_SIDE_2;
import static com.revos.android.constants.Constants.UPLOAD_TYPE_PROFILE;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_PROFILE_EVENT_BACK_PRESSED;
import static com.revos.android.constants.Constants.USER_PROFILE_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_PROFILE_FRAG_EVENT_USER_UPDATED;
import static com.revos.android.constants.Constants.USER_PROFILE_UPLOAD_FILE_NAME_KEY;

/**
 * Created by mohit on 15/9/17.
 */

public class EditUserInfoFragment extends Fragment{
    private View mRootView;

    /**views and layouts*/
    private EditText mUserNameEditText, mUserPhoneNumberEditText, mUserEmergencyContact1EditText,
            mUserEmergencyContact2EditText;

    private CircleImageView mUserProfilePicCircleImageView;

    private View mDoneButtonView, mUserEmergencyContact1ImageView, mUserEmergencyContact2ImageView;

    private ImageView mUserDrivingLicenseSide1ImageView, mUserDrivingLicenseSide2ImageView, mBackButtonView;

    private TextView mDrivingLicenseScanFirstSideTextView, mDrivingLicenseScanSecondSideTextView, mUserDobTextView;

    /**file size restriction*/
    private int IMAGE_SIZE_CAP = 5242880; //5MB //todo::discuss with ravi if this should be reduced

    private final int CAMERA_PERMISSION_REQUEST_CODE = 2000;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    /**private local copy of user*/
    private User mUser;

    private CountryCodePicker mEditUserPhoneNumberCcp;

    /**change tracking flags*/
    private boolean mTextualDataChanged, mProfilePicChanged, mDLSide1Changed, mDLSide2Changed;

    /**request codes*/
    private final int PICK_FIRST_CONTACT_REQ_CODE = 1001;
    private final int PICK_SECOND_CONTACT_REQ_CODE = 1002;

    private final int PROFILE_PIC_OPEN_CAMERA_REQUEST_CODE = 100;
    private final int PROFILE_PIC_OPEN_GALLERY_REQUEST_CODE = 101;

    private final int DL_PIC_1_OPEN_CAMERA_REQUEST_CODE = 200;
    private final int DL_PIC_1_OPEN_GALLERY_REQUEST_CODE = 201;

    private final int DL_PIC_2_OPEN_CAMERA_REQUEST_CODE = 300;
    private final int DL_PIC_2_OPEN_GALLERY_REQUEST_CODE = 301;

    String mSelectedDOB = "";

    /**
     * temp bitmap object to operate
     */

    private Bitmap mWorkingBitmap;

    /**
     * shared preferences
     */

    private SharedPreferences mSharedPreferences;

    /**
     * External storage variables
     */

    private File mProfilePicTempFile, mProfilePicTempCompressedFile, mProfilePicActualFile,
                 mDLPic1TempFile, mDLPic1TempCompressedFile, mDLPic1ActualFile,
                 mDLPic2TempFile, mDLPic2TempCompressedFile, mDLPic2ActualFile;
    private Uri  mProfilePicTempURI, mDLPic1TempURI, mDLPic2TempURI;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.edit_user_info_fragment, container, false);

        //reset change status flags
        mTextualDataChanged = mProfilePicChanged = mDLSide1Changed = mDLSide2Changed = false;

        setupAndPopulateEditUserProfilePage();

        return mRootView;
    }

    private void setupAndPopulateEditUserProfilePage() {
        if(getActivity() == null){
            return;
        }

        mSharedPreferences = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE);

        mUser = PrefUtils.getInstance(getActivity()).getUserObjectFromPrefs();
        if(mUser == null) {
            //there has been an error, this situation should not occur
            getActivity().finish();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unknown_error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);
        }

        mUserNameEditText = (EditText)mRootView.findViewById(R.id.edit_user_name_edit_text);
        mUserNameEditText.setText(String.format("%s %s", mUser.getFirstName(), mUser.getLastName()));

        mUserDobTextView = (TextView) mRootView.findViewById(R.id.edit_user_dob_edit_text);
        if(mUser.getDob() != null) {
            DateTime dateTime = new DateTime(mUser.getDob());
            Date dob = dateTime.toDate();
            String dateFormat = "dd/MMM/yyyy"; //In which you need put here
            mSelectedDOB = dateTime.toString();
            ((TextView)mRootView.findViewById(R.id.edit_user_dob_edit_text)).setText(new SimpleDateFormat(dateFormat).format(dob));
        }

        mUserPhoneNumberEditText = (EditText)mRootView.findViewById(R.id.edit_user_phone_number_edit_text);
        mEditUserPhoneNumberCcp = (CountryCodePicker)mRootView.findViewById(R.id.edit_user_phone_number_ccp);
        mEditUserPhoneNumberCcp.registerCarrierNumberEditText(mUserPhoneNumberEditText);
        String userPhoneNum = mUser.getPhone().replace(mEditUserPhoneNumberCcp.getSelectedCountryCodeWithPlus(), "");
        mUserPhoneNumberEditText.setText(userPhoneNum);

        mUserEmergencyContact1EditText = (EditText)mRootView.findViewById(R.id.edit_user_emergency_contact_1_edit_text);
        mUserEmergencyContact1EditText.setText(mUser.getAltPhone1());
        mUserEmergencyContact1EditText.setFocusable(false);

        mUserEmergencyContact2EditText = (EditText)mRootView.findViewById(R.id.edit_user_emergency_contact_2_edit_text);
        if(mUser.getAltPhone2() != null) {
            mUserEmergencyContact2EditText.setText(mUser.getAltPhone2());
        }
        mUserEmergencyContact2EditText.setFocusable(false);

        mUserEmergencyContact1ImageView = mRootView.findViewById(R.id.edit_user_emergency_contact_1_image_view);
        mUserEmergencyContact2ImageView = mRootView.findViewById(R.id.edit_user_emergency_contact_2_image_view);

        mBackButtonView = (ImageView) mRootView.findViewById(R.id.edit_user_back_button_image_view);
        mDoneButtonView = mRootView.findViewById(R.id.edit_user_done_text_view);

        mUserProfilePicCircleImageView = (CircleImageView)mRootView.findViewById(R.id.edit_user_profile_pic_circle_image_view);

        mUserDrivingLicenseSide1ImageView = (ImageView)mRootView.findViewById(R.id.edit_user_driver_license_side_1_image_view);
        mUserDrivingLicenseSide2ImageView = (ImageView)mRootView.findViewById(R.id.edit_user_driver_license_side_2_image_view);
        mDrivingLicenseScanFirstSideTextView = (TextView) mRootView.findViewById(R.id.edit_user_scan_driver_license_side_1_text_view);
        mDrivingLicenseScanSecondSideTextView = (TextView) mRootView.findViewById(R.id.edit_user_scan_driver_license_side_2_text_view);

        loadProfilePic();

        loadBothDLPic();

        mDoneButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateChangeFlags();
                if (mTextualDataChanged || mProfilePicChanged || mDLSide1Changed || mDLSide2Changed) {
                    saveChanges();
                } else {
                    //let user info fragment load
                    EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_USER_UPDATED));
                }
            }
        });

        mBackButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBackButton();
            }
        });

        mUserEmergencyContact1EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact(PICK_FIRST_CONTACT_REQ_CODE);
            }
        });

        mUserEmergencyContact1ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact(PICK_FIRST_CONTACT_REQ_CODE);
            }
        });

        mUserEmergencyContact2EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact(PICK_SECOND_CONTACT_REQ_CODE);
            }
        });

        mUserEmergencyContact2ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact(PICK_SECOND_CONTACT_REQ_CODE);
            }
        });

        mUserDobTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateTime dateTime = new DateTime(mUser.getDob());
                int year = dateTime.getYear();
                int month = dateTime.getMonthOfYear();
                int day = dateTime.getDayOfMonth();

                new SpinnerDatePickerDialogBuilder()
                        .context(getActivity())
                        .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);

                                Date date = calendar.getTime();
                                String dateFormat = "dd/MMM/yyyy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                                ((TextView)mRootView.findViewById(R.id.edit_user_dob_edit_text)).setText(sdf.format(calendar.getTime()));
                                mSelectedDOB = new DateTime(date).toString();
                            }
                        })
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(year, month - 1, day)
                        .build()
                        .show();
            }
        });

        if(mUser.getGenderType() !=  null) {
            if(mUser.getGenderType().equals(GenderType.MALE)) {
                ((RadioButton)mRootView.findViewById(R.id.edit_user_radio_button_male)).setChecked(true);
            } else if(mUser.getGenderType().equals(GenderType.FEMALE)) {
                ((RadioButton)mRootView.findViewById(R.id.edit_user_radio_button_female)).setChecked(true);
            } else {
                ((RadioButton)mRootView.findViewById(R.id.edit_user_radio_button_other)).setChecked(true);
            }
        }

        mUserProfilePicCircleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null){
                    return;
                }

                final Dialog mediaPickerDialog = new Dialog(getActivity());
                mediaPickerDialog.setContentView(R.layout.media_picker_dialog);

                mediaPickerDialog.findViewById(R.id.camera_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // do we have a camera?
                        if(!getActivity().getPackageManager()
                                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_camera_on_device));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            startActivity(intent);

                            mediaPickerDialog.dismiss();
                            return;
                        }

                        //check for runtime permission
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                            return;
                        }

                        //create temp file
                        String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
                        String profilePicTempFileName = PROFILE_PIC_TEMP_PREFIX + timeStamp + ".jpg";
                        mProfilePicTempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), profilePicTempFileName);

                        Uri tempPhotoURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mProfilePicTempFile);
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPhotoURI);
                        startActivityForResult(takePicture, PROFILE_PIC_OPEN_CAMERA_REQUEST_CODE);

                        mediaPickerDialog.dismiss();
                    }
                });

                mediaPickerDialog.findViewById(R.id.gallery_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto , PROFILE_PIC_OPEN_GALLERY_REQUEST_CODE);
                        mediaPickerDialog.dismiss();
                    }
                });

                mediaPickerDialog.show();

            }
        });

        mUserDrivingLicenseSide1ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null){
                    return;
                }

                final Dialog mediaPickerDialog = new Dialog(getActivity());
                mediaPickerDialog.setContentView(R.layout.media_picker_dialog);

                mediaPickerDialog.findViewById(R.id.camera_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // do we have a camera?
                        if(!getActivity().getPackageManager()
                                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_camera_on_device));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            startActivity(intent);

                            mediaPickerDialog.dismiss();
                            return;
                        }

                        //check for runtime permission
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                            return;
                        }

                        //create temp file
                        String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
                        String profilePicTempFileName = DL_SIDE_1_TEMP_PREFIX + timeStamp + ".jpg";
                        mDLPic1TempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), profilePicTempFileName);

                        Uri tempPhotoURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mDLPic1TempFile);
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPhotoURI);
                        startActivityForResult(takePicture, DL_PIC_1_OPEN_CAMERA_REQUEST_CODE);

                        mediaPickerDialog.dismiss();
                    }
                });

                mediaPickerDialog.findViewById(R.id.gallery_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto , DL_PIC_1_OPEN_GALLERY_REQUEST_CODE);
                        mediaPickerDialog.dismiss();
                    }
                });

                mediaPickerDialog.show();
            }
        });

        mUserDrivingLicenseSide2ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null){
                    return;
                }

                final Dialog mediaPickerDialog = new Dialog(getActivity());
                mediaPickerDialog.setContentView(R.layout.media_picker_dialog);

                mediaPickerDialog.findViewById(R.id.camera_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // do we have a camera?
                        if(!getActivity().getPackageManager()
                                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_camera_on_device));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            startActivity(intent);

                            mediaPickerDialog.dismiss();
                            return;
                        }

                        //check for runtime permission
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                            return;
                        }

                        //create temp file
                        String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
                        String profilePicTempFileName = DL_SIDE_2_TEMP_PREFIX + timeStamp + ".jpg";
                        mDLPic2TempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), profilePicTempFileName);

                        Uri tempPhotoURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mDLPic2TempFile);
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPhotoURI);
                        startActivityForResult(takePicture, DL_PIC_2_OPEN_CAMERA_REQUEST_CODE);

                        mediaPickerDialog.dismiss();
                    }
                });

                mediaPickerDialog.findViewById(R.id.gallery_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto , DL_PIC_2_OPEN_GALLERY_REQUEST_CODE);
                        mediaPickerDialog.dismiss();
                    }
                });

                mediaPickerDialog.show();
            }
        });
    }

    private void handleBackButton() {
        updateChangeFlags();
        if(mTextualDataChanged || mProfilePicChanged || mDLSide1Changed || mDLSide2Changed) {
            showExitDialog();
        } else {
            //let user info fragment load
            EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_USER_UPDATED));
        }
    }

    private void loadProfilePic() {
        if(getActivity() == null){
            return;
        }

        String uploadFileNameKey = getActivity()
                .getSharedPreferences(USER_PREFS, MODE_PRIVATE)
                .getString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, null);
        if(uploadFileNameKey != null) {

            //load user profile pic
            String profilePicFileName = mSharedPreferences.getString(USER_PROFILE_LOCAL_FILE_PATH_KEY, null);
            if(profilePicFileName != null) {
                File profilePicFile = new File(profilePicFileName);
                if(profilePicFile.exists()) {
                    Picasso.with(getActivity()).load(profilePicFile).fit().centerInside().into(mUserProfilePicCircleImageView);
                }
            } else {
                //request networking service to download pic
                NetworkUtils.getInstance(getActivity()).downloadProfilePicIfRequired();
                //todo::change image to spinning icon
            }
        }
    }

    private void loadBothDLPic() {
        if(getActivity() == null){
            return;
        }

        if(getActivity()
                .getSharedPreferences(USER_PREFS, MODE_PRIVATE)
                .getString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, null) != null) {
            loadDLPic1();
        }

        if(getActivity()
                .getSharedPreferences(USER_PREFS, MODE_PRIVATE)
                .getString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, null) != null) {
            loadDLPic2();
        }
    }

    private void loadDLPic1() {
        String dlPic1FileName = mSharedPreferences.getString(USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY, null);
        if(dlPic1FileName != null) {
            //hide the text view
            (mRootView.findViewById(R.id.edit_user_scan_driver_license_side_1_text_view)).setVisibility(View.INVISIBLE);
            File dlPic1File = new File(dlPic1FileName);
            if(dlPic1File.exists()) {
                Picasso.with(getActivity()).load(dlPic1File).fit().centerInside().into(mUserDrivingLicenseSide1ImageView);
            }
        }
    }

    private void loadDLPic2() {
        String dlPic2FileName = mSharedPreferences.getString(USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY, null);
        if(dlPic2FileName != null) {
            //hide the text view
            (mRootView.findViewById(R.id.edit_user_scan_driver_license_side_2_text_view)).setVisibility(View.INVISIBLE);
            File dlPic2File = new File(dlPic2FileName);
            if(dlPic2File.exists()) {
                Picasso.with(getActivity()).load(dlPic2File).fit().centerInside().into(mUserDrivingLicenseSide2ImageView);
            }
        }
    }

    private void pickContact(int requestCode) {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, requestCode);
    }

    private void contactPicked(Intent data, int requestCode) {
        Cursor cursor = null;
        String contactPickedPreSanitized = null ;
        String contactNumberPicked = null;
        try {
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                // column index of the phone number
                int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                contactPickedPreSanitized = cursor.getString(phoneIndex);
            }

            contactNumberPicked = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(contactPickedPreSanitized);

        } catch (Exception e) {
            //todo::handle exception
        }
        // Set the value to the edittext
        if(requestCode == PICK_FIRST_CONTACT_REQ_CODE) {
            mUserEmergencyContact1EditText.setText(contactNumberPicked);
        }

        if(requestCode == PICK_SECOND_CONTACT_REQ_CODE) {
            mUserEmergencyContact2EditText.setText(contactNumberPicked);
        }
    }

    private void updateChangeFlags() {
        String firstName = "";
        String lastName = "";
        if(mUserNameEditText.getText().toString().trim().split(" ").length >= 2) {
            firstName = mUserNameEditText.getText().toString().trim().split(" ")[0];
            lastName = mUserNameEditText.getText().toString().split(" ")[1];
        }

        String fullPhoneNumberWithPlus =mEditUserPhoneNumberCcp.getFullNumberWithPlus().trim();
        String sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(fullPhoneNumberWithPlus);

        String emergencyContact1EditTextStr = mUserEmergencyContact1EditText.getText().toString().trim();
        String sanitizedEmergencyContact1 = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(emergencyContact1EditTextStr);

        String emergencyContact2EditTextStr = mUserEmergencyContact2EditText.getText().toString().trim();
        String sanitizedEmergencyContact2 = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(emergencyContact2EditTextStr);

        //selected DOB
        String selectedDateOfBirth = mUserDobTextView.getText().toString().trim();

        //user's DOB
        DateTime userDOBDateTime = new DateTime(mUser.getDob());
        Date userDobDate = userDOBDateTime.toDate();
        String dateFormatUserDOB = "dd/MMM/yyyy";
        String userDOB = new SimpleDateFormat(dateFormatUserDOB).format(userDobDate);

        int checkedRadioButtonId = ((RadioGroup)mRootView.findViewById(R.id.edit_user_gender_radio_group)).getCheckedRadioButtonId();

        GenderType genderType = GenderType.OTHERS;
        if(checkedRadioButtonId == R.id.edit_user_radio_button_male) {
            genderType = GenderType.MALE;
        } else if(checkedRadioButtonId == R.id.edit_user_radio_button_female) {
            genderType = GenderType.FEMALE;
        } else {
            genderType = GenderType.OTHERS;
        }

        mTextualDataChanged = !(firstName.equals(mUser.getFirstName()) && lastName.equals(mUser.getLastName()) &&
                sanitizedPhoneNumber.equals(PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(mUser.getPhone())) &&
                sanitizedEmergencyContact1.equals(PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(mUser.getAltPhone1())) &&
                sanitizedEmergencyContact2.equals(PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(mUser.getAltPhone2())) &&
                selectedDateOfBirth.equals(userDOB) && genderType == mUser.getGenderType());
    }

    private void showExitDialog() {
        if (getActivity() == null){
            return;
        }

        final Dialog discardCancelDialog = new Dialog(getActivity());
        discardCancelDialog.setContentView(R.layout.discard_cancel_dialog);

        discardCancelDialog.findViewById(R.id.discard_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //let user info fragment load
                EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_USER_UPDATED));
                discardCancelDialog.dismiss();
            }
        });

        discardCancelDialog.findViewById(R.id.cancel_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discardCancelDialog.dismiss();
            }
        });

        discardCancelDialog.show();
    }

    private void saveChanges() {

        //avoid accidental reuploads
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }

        if(mTextualDataChanged) {
            saveTextualChanges();
            return;
        }

        if(mProfilePicChanged) {
            saveProfilePicChanges();
            return;
        }

        if(mDLSide1Changed) {
            saveDLSide1Changes();
            return;
        }

        if(mDLSide2Changed) {
            saveDLSide2Changes();
        }
    }

    private void saveTextualChanges() {
        //check data
        String firstName = "";
        String lastName = "";
        if(mUserNameEditText.getText().toString().trim().split(" ").length >= 2) {
            firstName = mUserNameEditText.getText().toString().trim().split(" ")[0];
            lastName  = mUserNameEditText.getText().toString().split(" ")[1];
        }

        String fullPhoneNumberWithPlus = "";
        String sanitizedPhoneNumber ="";
        if (mEditUserPhoneNumberCcp.isValidFullNumber()) {
                fullPhoneNumberWithPlus = mEditUserPhoneNumberCcp.getFullNumberWithPlus().trim();
                sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(fullPhoneNumberWithPlus);

        } else {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.enter_valid_phone));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String fullEmergencyContact1WithPlus = mUserEmergencyContact1EditText.getText().toString().trim();
        String sanitizedEmergencyContact1 = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(fullEmergencyContact1WithPlus);

        String fullEmergencyContact2WithPlus = mUserEmergencyContact2EditText.getText().toString().trim();
        String sanitizedEmergencyContact2 = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(fullEmergencyContact2WithPlus);

        String dateOfBirth = null;
        if(mSelectedDOB != null && !mSelectedDOB.isEmpty()) {
            //calculate user age
            DateTime currentDateTime = new DateTime();
            DateTime dobDateTime = new DateTime(mSelectedDOB);

            int ageInYears = new Period(dobDateTime, currentDateTime).getYears();
            if(ageInYears >= 16){
                dateOfBirth = mSelectedDOB;
            } else {

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.user_must_be_16_years_or_above));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

                return;
            }
        }

        int checkedRadioButtonId = ((RadioGroup)mRootView.findViewById(R.id.edit_user_gender_radio_group)).getCheckedRadioButtonId();
        GenderType genderType = GenderType.OTHERS;
        if(checkedRadioButtonId == R.id.edit_user_radio_button_male) {
            genderType = GenderType.MALE;
        } else if(checkedRadioButtonId == R.id.edit_user_radio_button_female) {
            genderType = GenderType.FEMALE;
        } else {
            genderType = GenderType.OTHERS;
        }

        if(!firstName.isEmpty() && !lastName.isEmpty()) {
            mUser.setFirstName(firstName);
            mUser.setLastName(lastName);
        } else {
            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.enter_full_name));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);
            return;
        }

        if(!sanitizedPhoneNumber.isEmpty()) {
            mUser.setPhone(sanitizedPhoneNumber);
        } else {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_your_phone_number));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(dateOfBirth != null && !dateOfBirth.isEmpty()) {
            mUser.setDob(dateOfBirth);
        }

        mUser.setGenderType(genderType);

        if(!sanitizedEmergencyContact1.isEmpty()) {
            mUser.setAltPhone1(sanitizedEmergencyContact1);
        } else {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_one_emergency_contact_number));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(!sanitizedEmergencyContact2.isEmpty()) {
            if(!sanitizedEmergencyContact2.equals(mUser.getAltPhone1())) {
                mUser.setAltPhone2(sanitizedEmergencyContact2);
            } else {

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.contact_already_set_as_emergency_contact));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

                return;
            }
        }

        makeUpdateUserCall();

    }

    private void makeUpdateUserCall() {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.profile_update_fail));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        UserUpdateInput userUpdateInput = UserUpdateInput
                .builder()
                .firstName(mUser.getFirstName())
                .lastName(mUser.getLastName())
                .email(mUser.getEmail())
                .phone(mUser.getPhone())
                .altPhone1(mUser.getAltPhone1())
                .altPhone2(mUser.getAltPhone2())
                .dob(mUser.getDob())
                .gender(mUser.getGenderType())
                .role(mUser.getRole())
                .build();

        UpdateUserMutation updateUserMutation = UpdateUserMutation.builder().data(userUpdateInput).build();

        apolloClient.mutate(updateUserMutation).enqueue(new ApolloCall.Callback<UpdateUserMutation.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<UpdateUserMutation.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                if(response.getErrors() != null && response.getErrors().isEmpty()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            hideProgressDialog();

                            String message = getString(R.string.profile_update_fail);
                            if(response.getErrors().get(0) != null) {
                                BigDecimal statusCode = (BigDecimal)response.getErrors().get(0).getCustomAttributes().get("statusCode");
                                if(statusCode != null && statusCode.intValue() == 409) {
                                    message = response.getErrors().get(0).getMessage();
                                }
                            }

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                            EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_USER_UPDATED));
                        }
                    });
                } else {

                    if(getActivity() == null) {
                        return;
                    }

                    //store user object in prefs
                    getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_JSON_DATA_KEY, new Gson().toJson(mUser)).apply();

                    if (mProfilePicChanged) {
                        saveProfilePicChanges();
                        return;
                    }

                    if (mDLSide1Changed) {
                        saveDLSide1Changes();
                        return;
                    }

                    if (mDLSide2Changed) {
                        saveDLSide2Changes();
                        return;
                    }

                    //hide progress dialog and let user profile fragment load
                    hideProgressDialog();

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.profile_updated));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_USER_UPDATED));
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        //todo::handle server error scenarios at a granular level

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });
    }

    private void saveProfilePicChanges() {
        showProgressDialog();
        if(mProgressDialog.isShowing()) {
            new CompressPicTask().execute(UPLOAD_TYPE_PROFILE);

        }
    }

    private void saveDLSide1Changes() {
        showProgressDialog();
        if(mProgressDialog.isShowing()) {
            new CompressPicTask().execute(UPLOAD_TYPE_DL_SIDE_1);
        }
    }


    private void saveDLSide2Changes() {
        showProgressDialog();
        if(mProgressDialog.isShowing()) {
            new CompressPicTask().execute(UPLOAD_TYPE_DL_SIDE_2);
        }
    }

    //TODO::clear temp file directory while exiting

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**file handling variables*/
        File tempDir;
        File[] tempFiles;
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_FIRST_CONTACT_REQ_CODE:
                case PICK_SECOND_CONTACT_REQ_CODE: contactPicked(data, requestCode);
                                                    break;

                case PROFILE_PIC_OPEN_CAMERA_REQUEST_CODE:
                    if(getActivity() == null) {
                        return;
                    }

                    mProfilePicTempURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mProfilePicTempFile);
                    //clear previous files
                    tempDir = mProfilePicTempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(PROFILE_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mProfilePicTempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }
                    Glide.with(getActivity()).
                            load(mProfilePicTempURI).
                            into(mUserProfilePicCircleImageView);
                    mProfilePicChanged = true;
                    break;

                case PROFILE_PIC_OPEN_GALLERY_REQUEST_CODE:
                    if(getActivity() == null){
                        return;
                    }

                    mProfilePicTempURI = data.getData();
                    //clear previous files
                    mProfilePicTempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), "place_holder_file_name");
                    tempDir = mProfilePicTempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(PROFILE_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mProfilePicTempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }
                    Glide.with(getActivity()).
                            load(mProfilePicTempURI).
                            into(mUserProfilePicCircleImageView);
                    mProfilePicChanged = true;
                    break;

                case DL_PIC_1_OPEN_CAMERA_REQUEST_CODE:
                    if(getActivity() == null){
                        return;
                    }

                    mDLPic1TempURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mDLPic1TempFile);
                    //clear previous files
                    tempDir = mDLPic1TempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(DL_SIDE_1_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mDLPic1TempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }
                    //hide the text view
                    mRootView.findViewById(R.id.edit_user_scan_driver_license_side_1_text_view).setVisibility(View.INVISIBLE);
                    Glide.with(getActivity()).
                            load(mDLPic1TempURI).
                            into(mUserDrivingLicenseSide1ImageView);
                    mDLSide1Changed = true;
                    break;

                case DL_PIC_1_OPEN_GALLERY_REQUEST_CODE:
                    if(getActivity() == null){
                        return;
                    }

                    mDLPic1TempURI = data.getData();
                    //clear previous files
                    mDLPic1TempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), "place_holder_file_name");
                    tempDir = mDLPic1TempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(DL_SIDE_1_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mDLPic1TempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    //hide the text view
                    mRootView.findViewById(R.id.edit_user_scan_driver_license_side_1_text_view).setVisibility(View.INVISIBLE);
                    Glide.with(getActivity()).
                            load(mDLPic1TempURI).
                            into(mUserDrivingLicenseSide1ImageView);
                    mDLSide1Changed = true;
                    break;

                case DL_PIC_2_OPEN_CAMERA_REQUEST_CODE:
                    if(getActivity() == null){
                        return;
                    }

                    mDLPic2TempURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mDLPic2TempFile);
                    //clear previous files
                    tempDir = mDLPic2TempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(DL_SIDE_2_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mDLPic2TempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }
                    //hide the text view
                    mRootView.findViewById(R.id.edit_user_scan_driver_license_side_2_text_view).setVisibility(View.INVISIBLE);
                    Glide.with(getActivity()).
                            load(mDLPic2TempURI).
                            into(mUserDrivingLicenseSide2ImageView);
                    mDLSide2Changed = true;
                    break;

                case DL_PIC_2_OPEN_GALLERY_REQUEST_CODE:
                    if(getActivity() == null){
                        return;
                    }

                    mDLPic2TempURI = data.getData();
                    //clear previous files
                    mDLPic2TempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), "place_holder_file_name");
                    tempDir = mDLPic2TempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(DL_SIDE_2_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mDLPic2TempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }
                    //hide the text view
                    mRootView.findViewById(R.id.edit_user_scan_driver_license_side_2_text_view).setVisibility(View.INVISIBLE);
                    Glide.with(getActivity()).
                            load(mDLPic2TempURI).
                            into(mUserDrivingLicenseSide2ImageView);

                    mDLSide2Changed = true;
                    break;

                default: break;
            }
        }
    }

    public void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.updating_profile));
        if(getActivity() != null && !getActivity().isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    private class CompressPicTask extends AsyncTask<String, String, File> {

        String mFileType;

        @Override
        protected File doInBackground(String... params) {
            if(getActivity() == null){
                return null;
            }

            String fileType = params[0];
            mFileType = fileType;

            String tempCompressedFileName;
            File tempCompressedFile;
            Uri picTempUri;
            String picTempPrefix;

            //compress the profile pic before uploading
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());

            switch (fileType) {
                case UPLOAD_TYPE_PROFILE:
                    picTempPrefix = PROFILE_PIC_TEMP_COMPRESSED_PREFIX;
                    picTempUri = mProfilePicTempURI;
                    break;
                case UPLOAD_TYPE_DL_SIDE_1:
                    picTempPrefix = DL_SIDE_1_TEMP_COMPRESSED_PREFIX;
                    picTempUri = mDLPic1TempURI;
                    break;
                default:
                    picTempPrefix = DL_SIDE_2_TEMP_COMPRESSED_PREFIX;
                    picTempUri = mDLPic2TempURI;
                    break;
            }

            tempCompressedFileName = picTempPrefix + timeStamp + ".jpg";
            tempCompressedFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), tempCompressedFileName);
            try {
                mWorkingBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picTempUri);
                FileOutputStream compressedFileOutputStream = new FileOutputStream(tempCompressedFile);
                mWorkingBitmap.compress(Bitmap.CompressFormat.JPEG, 75, compressedFileOutputStream);
                compressedFileOutputStream.flush();
                compressedFileOutputStream.close();
                mWorkingBitmap.recycle();
                ImageUtils.copyExif(getActivity().getContentResolver().openInputStream(picTempUri), tempCompressedFile.getPath());
            } catch (Exception e) {
                //todo:: handle this
                hideProgressDialog();
            }

            switch (fileType) {
                case UPLOAD_TYPE_PROFILE:
                    mProfilePicTempCompressedFile = tempCompressedFile;
                    break;
                case UPLOAD_TYPE_DL_SIDE_1:
                    mDLPic1TempCompressedFile = tempCompressedFile;
                    break;
                default:
                    mDLPic2TempCompressedFile = tempCompressedFile;
                    break;
            }

            return tempCompressedFile;
        }

        @Override
        protected void onPostExecute(File tempCompressedFile) {
            super.onPostExecute(tempCompressedFile);
            //check image size
            if(tempCompressedFile.length() > IMAGE_SIZE_CAP) {

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.image_too_large));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

                hideProgressDialog();
                return;
            }
            //upload the pic
            uploadPic(mFileType);
        }
    }

    private void uploadPic(final String fileType) {
        final File tempCompressedFile;
        final String picPrefix;
        final File tempFileFromCamera;

        switch (fileType) {
            case UPLOAD_TYPE_PROFILE:
                tempCompressedFile = mProfilePicTempCompressedFile;
                tempFileFromCamera = mProfilePicTempFile;
                picPrefix = PROFILE_PIC_PREFIX;
                break;
            case UPLOAD_TYPE_DL_SIDE_1:
                tempCompressedFile = mDLPic1TempCompressedFile;
                tempFileFromCamera = mDLPic1TempFile;
                picPrefix = DL_SIDE_1_PREFIX;
                break;
            default:
                tempCompressedFile = mDLPic2TempCompressedFile;
                tempFileFromCamera = mDLPic2TempFile;
                picPrefix = DL_SIDE_2_PREFIX;
                break;
        }

        try {
            byte[] fileBytesArray = new byte[(int) tempCompressedFile.length()];
            FileInputStream fis = new FileInputStream(tempCompressedFile);
            fis.read(fileBytesArray); //read file into bytes[]
            fis.close();
            String encodedFileString = Base64.encodeToString(fileBytesArray, Base64.NO_WRAP);

            FileType uploadFileType = FileType.PROFILE;

            if(fileType.equals(UPLOAD_TYPE_PROFILE)) {
                uploadFileType = FileType.PROFILE;
            } else if(fileType.equals(UPLOAD_TYPE_DL_SIDE_1)) {
                uploadFileType = FileType.DL_SIDE_1;
            } else if(fileType.equals(UPLOAD_TYPE_DL_SIDE_2)) {
                uploadFileType = FileType.DL_SIDE_2;
            }

            if(getActivity() == null) {
                return;
            }

            ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

            if(apolloClient == null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_upload_file));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
                return;
            }

            showProgressDialog();

            UploadInput uploadInput = UploadInput
                    .builder()
                    .uploadType(uploadFileType)
                    .fileData(encodedFileString)
                    .build();

            UploadFileMutation uploadFileMutation = UploadFileMutation
                    .builder()
                    .uploadInputVar(uploadInput)
                    .build();

            apolloClient.mutate(uploadFileMutation).enqueue(new ApolloCall.Callback<UploadFileMutation.Data>() {
                @Override
                public void onResponse(com.apollographql.apollo.api.@NotNull Response<UploadFileMutation.Data> response) {
                    if(getActivity() == null) {
                        return;
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                                hideProgressDialog();

                                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_upload_file));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                intent.putExtras(bundle);
                                startActivity(intent);

                            } else {

                                try {
                                    //write this data to prefs
                                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();

                                    String fileName = response.getData().files().upload().name();
                                    if(fileType.equals(UPLOAD_TYPE_PROFILE)) {
                                        editor.putString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, fileName);
                                    }
                                    if(fileType.equals(UPLOAD_TYPE_DL_SIDE_1)) {
                                        editor.putString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, fileName);
                                    }
                                    if(fileType.equals(UPLOAD_TYPE_DL_SIDE_2)) {
                                        editor.putString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, fileName);
                                    }
                                    editor.apply();

                                    fileName = picPrefix + fileName + ".jpg";
                                    File actualFile = new File(getActivity().getExternalFilesDir(PIC_DIR_PATH), fileName);
                                    if(tempCompressedFile.exists()) {
                                        try {
                                            if(!actualFile.exists()) {
                                                actualFile.createNewFile();
                                            }
                                            copyFile(tempCompressedFile, actualFile);
                                        } catch (IOException e) {
                                            //todo::handle exception
                                            hideProgressDialog();

                                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_write_to_storage));
                                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                        }
                                    }

                                    //update shared preferences for file path key
                                    if(actualFile.exists()) {
                                        String fileKey;
                                        switch (fileType) {
                                            case UPLOAD_TYPE_PROFILE:
                                                fileKey = USER_PROFILE_LOCAL_FILE_PATH_KEY;
                                                break;
                                            case UPLOAD_TYPE_DL_SIDE_1:
                                                fileKey = USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY;
                                                break;
                                            default :
                                                fileKey = USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY;
                                                break;
                                        }
                                        editor.putString(fileKey, actualFile.getAbsolutePath());
                                        editor.apply();

                                        //clear previous profile pic
                                        File actualPicDir = actualFile.getParentFile();
                                        File[] actualPicFiles = actualPicDir.listFiles();
                                        for (int fileIndex = 0; fileIndex < actualPicFiles.length; ++fileIndex) {
                                            String currentFileName = actualPicFiles[fileIndex].getName();
                                            if(currentFileName.contains(picPrefix) && !currentFileName.equals(actualFile.getName())) {
                                                actualPicFiles[fileIndex].delete();
                                            }
                                        }

                                        //delete the temp files
                                        if(tempFileFromCamera.exists()) {
                                            tempFileFromCamera.delete();
                                        }
                                        if(tempCompressedFile.exists()) {
                                            tempCompressedFile.delete();
                                        }
                                    }


                                    if(fileType.equals(UPLOAD_TYPE_PROFILE) && mDLSide1Changed) {
                                        saveDLSide1Changes();
                                        return;
                                    }

                                    if(fileType.equals(UPLOAD_TYPE_DL_SIDE_1) && mDLSide2Changed) {
                                        saveDLSide2Changes();
                                        return;
                                    }

                                    if(getActivity() == null) {
                                        return;
                                    }

                                    //hide progress dialog and let user profile fragment load
                                    hideProgressDialog();

                                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.profile_updated));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);

                                    EventBus.getDefault().post(new EventBusMessage(USER_PROFILE_FRAG_EVENT_USER_UPDATED));

                                } catch (Exception e) {
                                    hideProgressDialog();

                                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_upload_file));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            }
                        }
                    });
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {
                    if(getActivity() == null) {
                        return;
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Log error here since request failed
                            Timber.e(e.toString());
                            hideProgressDialog();

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                            //delete the temp files
                            if(tempFileFromCamera.exists()) {
                                tempFileFromCamera.delete();
                            }
                            if(tempCompressedFile.exists()) {
                                tempCompressedFile.delete();
                            }
                        }
                    });
                }
            });

        } catch (Exception e) {
            //todo::handle file write exception
            //delete the temp files
            if(tempFileFromCamera.exists()) {
                tempFileFromCamera.delete();}
            if(tempCompressedFile.exists()) {
                tempCompressedFile.delete();}
        }
    }

    public void copyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(USER_PROFILE_EVENT_BACK_PRESSED)) {
            handleBackButton();
        } else if(event.message.equals(NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadProfilePic();
                }
            });
        } else if(event.message.equals(NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED) || event.message.equals(NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadBothDLPic();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadProfilePic();
        loadBothDLPic();
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
