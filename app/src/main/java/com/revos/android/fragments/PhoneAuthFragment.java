package com.revos.android.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OtpView;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.LoginActivity;
import com.revos.android.constants.Constants;
import com.revos.android.utilities.general.PhoneUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class PhoneAuthFragment extends Fragment {

    private View mRootView;
    private boolean mVerificationInProgress = false;
    private final int TIMEOUT_DURATION = 120; //timeout duration in seconds

    private ImageView mLogoImageView;
    private Button mSendOTPButton, mSubmitOTPButton;
    private EditText mPhoneNumberEditText, mPhoneCountryCodeEditText;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    private String mPhoneNumber, mCountryCode;

    private RelativeLayout mOTPVerifyRelativeLayout;

    private OtpView mOTPView;

    public static final String LOGIN_SCREEN_PHONE_NUMBER = "login_screen_phone_number";
    public static final String LOGIN_SCREEN_COUNTRY_CODE = "login_screen_country_code";

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification withoutse
            //     user action.
            Timber.d("onVerificationCompleted:%s", credential);
            mVerificationInProgress = false;


            if(getActivity() != null) {
                ((LoginActivity) getActivity()).saveVerifiedPhoneNumberToPrefs(mCountryCode + mPhoneNumber);
            }
            signInWithPhoneAuthCredential(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Timber.w(e, "onVerificationFailed");
            mVerificationInProgress = false;

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
            }

            // Show a message and update the UI
            // ...
            if (getActivity() != null) {
                hideProgressDialog();

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_verify_phone_number));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

                ((LoginActivity) getActivity()).clearVerifiedPhoneNumberFromPrefs();
            }
        }

        @Override
        public void onCodeSent(String verificationId,
                PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Timber.d("onCodeSent:%s", verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            // ...

            showOTPVerificationLayout();
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);
            Timber.d("Auto retrieval time out");
            mVerificationInProgress = false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle == null) {
            return;
        }

        mPhoneNumber = bundle.getString(LOGIN_SCREEN_PHONE_NUMBER, null);
        mCountryCode = bundle.getString(LOGIN_SCREEN_COUNTRY_CODE, null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.login_phone_auth_fragment, container, false);

        setupViews();

        updateLogoIfRequired();

        return mRootView;
    }

    private void setupViews() {
        //setup logo
        mLogoImageView = (ImageView) mRootView.findViewById(R.id.login_phone_auth_logo_image_view);

        mPhoneCountryCodeEditText = mRootView.findViewById(R.id.login_phone_auth_country_code_edit_text);
        mPhoneNumberEditText = (EditText) mRootView.findViewById(R.id.login_main_phone_number_edit_text);

        mSendOTPButton = (Button) mRootView.findViewById(R.id.login_send_otp_button);
        mSubmitOTPButton = (Button) mRootView.findViewById(R.id.login_submit_otp_button);

        mOTPVerifyRelativeLayout = (RelativeLayout) mRootView.findViewById(R.id.login_otp_verification_relative_layout);
        mOTPView = (OtpView) mRootView.findViewById(R.id.login_otp_view);

        //populate phone number (if exists), and country code
        if(mPhoneNumber != null) {
            mPhoneNumberEditText.setText(mPhoneNumber);
        }

        if(mCountryCode != null && !mCountryCode.isEmpty()) {
            mPhoneCountryCodeEditText.setText(mCountryCode);
        }

        mSendOTPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOTP(true);
            }
        });

        mSubmitOTPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() != null) {
                    if (mOTPView.getText() == null || mOTPView.getText().toString().trim().isEmpty()) {

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_enter_entire_OTP));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                        bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } else {
                        verifyPhoneNumberWithCode(mVerificationId, mOTPView.getText().toString().trim());
                    }
                }
            }
        });
    }

    private void updateLogoIfRequired() {
        if(getActivity() == null){
            return;
        }

        //see if bike logo is available
        //read info from shared prefs
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.GENERAL_PREFS, Context.MODE_PRIVATE);

        String oemLogoFilePath = sharedPreferences.getString(Constants.COMPANY_LOGO_FILE_PATH_KEY, null);

        if(oemLogoFilePath != null) {
            File file = new File(oemLogoFilePath);
            if(file.exists()) {
                //TODO::uncomment this
                /*Bitmap logoBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                logoView.setImageBitmap(logoBitmap);*/
                //TODO::comment this
                mLogoImageView.setBackgroundColor(Color.parseColor("#00FF00"));
                return;
            }
        }

        mLogoImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_center));
        mLogoImageView.setVisibility(View.VISIBLE);
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        showProgressDialog();
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        Timber.d(credential.getSmsCode());
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        if(getActivity() != null) {
            FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Timber.d("signInWithCredential:success");
                                if(getActivity() != null) {
                                    ((LoginActivity) getActivity()).saveVerifiedPhoneNumberToPrefs(mCountryCode + mPhoneNumber);
                                }
                                FirebaseUser user = task.getResult().getUser();
                            } else {
                                // Sign in failed, display a message and update the UI
                                Timber.w(task.getException(), "signInWithCredential:failure");
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    // The verification code entered was invalid
                                    // [START_EXCLUDE silent]
                                    if (getActivity() != null) {
                                        hideProgressDialog();

                                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.incorrect_otp));
                                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                        intent.putExtras(bundle);
                                        startActivity(intent);

                                        ((LoginActivity) getActivity()).clearVerifiedPhoneNumberFromPrefs();
                                    }
                                    // [END_EXCLUDE]
                                }
                            }
                        }
                    });
        }
    }

    private void showOTPVerificationLayout() {
        hideProgressDialog();
        mSendOTPButton.setVisibility(View.INVISIBLE);
        mOTPVerifyRelativeLayout.setVisibility(View.VISIBLE);

        mRootView.findViewById(R.id.login_countdown_text_view).setVisibility(View.VISIBLE);
        mRootView.findViewById(R.id.login_resend_text_view).setVisibility(View.GONE);

        new CountDownTimer(TIMEOUT_DURATION * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                ((TextView)mRootView.findViewById(R.id.login_countdown_text_view)).setText(String.format("%s s", String.valueOf(millisUntilFinished / 1000)));
            }

            public void onFinish() {
                mRootView.findViewById(R.id.login_countdown_text_view).setVisibility(View.GONE);
                mRootView.findViewById(R.id.login_resend_text_view).setVisibility(View.VISIBLE);
                mRootView.findViewById(R.id.login_resend_text_view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendOTP(true);
                    }
                });
            }
        }.start();
    }

    private void showProgressDialog() {
        if(getActivity() != null) {
            ((LoginActivity)getActivity()).showProgressDialog();
        }
    }

    private void hideProgressDialog() {
        if(getActivity() != null) {
            ((LoginActivity)getActivity()).hideProgressDialog();
        }
    }

    @Override
    public void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        if(getActivity() != null) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    TIMEOUT_DURATION,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks,         // OnVerificationStateChangedCallbacks
                    token);             // ForceResendingToken from callbacks

            showProgressDialog();
        }
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        if(getActivity() != null) {
            // [START start_phone_auth]
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    TIMEOUT_DURATION,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks);     // OnVerificationStateChangedCallbacks
            // [END start_phone_auth]

            mVerificationInProgress = true;

            showProgressDialog();
        }
    }

    private void sendOTP(boolean isResend) {
        if(mVerificationInProgress) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_wait_for_otp_sms));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String phoneNumber = mPhoneNumberEditText.getText().toString();
        String sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(phoneNumber);
        String phoneNumberWithCCP = String.format("%s%s", mCountryCode, sanitizedPhoneNumber);
        if(!isResend) {
            startPhoneNumberVerification(phoneNumberWithCCP);
        } else {
            resendVerificationCode(phoneNumberWithCCP, mResendToken);
        }
    }
}
