package com.revos.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.jsonStructures.Invoice;
import com.revos.android.jsonStructures.PassbookTxn;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.adapters.RentalInvoiceInfoAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.FetchInvoicesForVehicleQuery;
import com.revos.scripts.FetchRentalInvoicesQuery;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class RentalInvoiceFragment extends Fragment {

    private View mRootView;
    private LinearLayout mRefreshingRentalInvoiceLinearLayout;
    private RecyclerView mRentalInvoiceRecyclerView;

    private HashMap<String, Invoice> mInvoiceHashMap;

    private List<FetchRentalInvoicesQuery.Invoice> mRentalInvoicesListForAccount = null;
    private List<FetchInvoicesForVehicleQuery.Invoice> mRentalInvoicesListForVehicle = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.rental_invoices_fragment, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        mRefreshingRentalInvoiceLinearLayout = mRootView.findViewById(R.id.rental_invoice_refreshing_linear_layout);
        mRentalInvoiceRecyclerView = mRootView.findViewById(R.id.rental_invoice_list_recycler_view);
    }

    private void fetchRentalInvoiceDataForAccount() {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            fetchRentalInvoiceDataForVehicle();
            return;
        }

        FetchRentalInvoicesQuery fetchRentalInvoicesQuery = FetchRentalInvoicesQuery.builder().build();

        if(mRefreshingRentalInvoiceLinearLayout.getVisibility() != View.VISIBLE) {
            mRefreshingRentalInvoiceLinearLayout.setVisibility(View.VISIBLE);
        }

        apolloClient.query(fetchRentalInvoicesQuery).enqueue(new ApolloCall.Callback<FetchRentalInvoicesQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchRentalInvoicesQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            fetchRentalInvoiceDataForVehicle();

                        } else if(response.getData() != null && response.getData().account() != null && response.getData().account().invoices() != null) {

                            mRentalInvoicesListForAccount = response.getData().account().invoices();

                            if(mRentalInvoicesListForAccount == null || mRentalInvoicesListForAccount.isEmpty()) {
                                fetchRentalInvoiceDataForVehicle();
                                return;
                            }

                            List<Invoice> invoiceList = new ArrayList<>();

                            for(int index = 0 ; index < mRentalInvoicesListForAccount.size() ; index++) {
                                Invoice invoice = new Invoice();
                                List<PassbookTxn> passbookTxnList = new ArrayList<>();

                                invoice.setInvoiceId(mRentalInvoicesListForAccount.get(index).id());
                                invoice.setInvoiceNumber(mRentalInvoicesListForAccount.get(index).number());
                                invoice.setInvoiceStatus(mRentalInvoicesListForAccount.get(index).status());
                                if(mRentalInvoicesListForAccount.get(index).payee() != null && mRentalInvoicesListForAccount.get(index).payee().company() != null) {
                                    invoice.setPayeeName(mRentalInvoicesListForAccount.get(index).payee().company().name());
                                }
                                if(mRentalInvoicesListForAccount.get(index).payee() != null && mRentalInvoicesListForAccount.get(index).payee().company() != null) {
                                    invoice.setPayeePhone(mRentalInvoicesListForAccount.get(index).payee().company().phone());
                                }
                                if(mRentalInvoicesListForAccount.get(index).payer() != null && mRentalInvoicesListForAccount.get(index).payer().user() != null) {
                                    invoice.setPayerFirstName(mRentalInvoicesListForAccount.get(index).payer().user().firstName());
                                }
                                if(mRentalInvoicesListForAccount.get(index).payer() != null && mRentalInvoicesListForAccount.get(index).payer().user() != null) {
                                    invoice.setPayerLastName(mRentalInvoicesListForAccount.get(index).payer().user().lastName());
                                }
                                if(mRentalInvoicesListForAccount.get(index).payer() != null && mRentalInvoicesListForAccount.get(index).payer().user() != null) {
                                    invoice.setPayerPhone(mRentalInvoicesListForAccount.get(index).payer().user().phone());
                                }
                                invoice.setInvoiceCreationDate(mRentalInvoicesListForAccount.get(index).createdAt());

                                FetchRentalInvoicesQuery.Payee payeeDetails= null;
                                if(mRentalInvoicesListForAccount.get(index) != null && mRentalInvoicesListForAccount.get(index).payee() != null && mRentalInvoicesListForAccount.get(index).payee().company() != null) {
                                    payeeDetails = mRentalInvoicesListForAccount.get(index).payee();
                                }

                                String payeeUpiId = null;
                                if(payeeDetails != null && payeeDetails.company() != null && payeeDetails.company().apps() != null) {
                                    List<FetchRentalInvoicesQuery.App> appsList = payeeDetails.company().apps();
                                    if(appsList != null && !appsList.isEmpty()) {
                                        for(int appsListindex = 0 ; appsListindex < appsList.size() ; appsListindex++) {
                                            if(appsList.get(appsListindex).paymentDetails() != null) {
                                                if(appsList.get(appsListindex).paymentDetails().address() != null && !appsList.get(appsListindex).paymentDetails().address().isEmpty()) {
                                                    payeeUpiId = appsList.get(appsListindex).paymentDetails().address();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                String vin = null;
                                FetchRentalInvoicesQuery.Asset asset = mRentalInvoicesListForAccount.get(index).asset();
                                if(asset != null) {
                                    if(asset.vehicle() != null) {
                                        vin = asset.vehicle().vin();
                                    }
                                }

                                List<FetchRentalInvoicesQuery.Passbook> passbookList = mRentalInvoicesListForAccount.get(index).passbook();

                                if(passbookList != null && !passbookList.isEmpty()) {
                                    for(int passbookListIndex = 0 ; passbookListIndex < passbookList.size() ; passbookListIndex++) {
                                        PassbookTxn passbookTxn = new PassbookTxn();

                                        passbookTxn.setId(passbookList.get(passbookListIndex).id());
                                        if(passbookList.get(passbookListIndex).amount() != null) {
                                            passbookTxn.setAmount(passbookList.get(passbookListIndex).amount().toString());
                                        }
                                        passbookTxn.setDueDate(passbookList.get(passbookListIndex).dueDate());
                                        passbookTxn.setPaymentDate(passbookList.get(passbookListIndex).paymentDate());
                                        passbookTxn.setClearanceDate(passbookList.get(passbookListIndex).clearanceDate());
                                        passbookTxn.setRemark(passbookList.get(passbookListIndex).remark());
                                        passbookTxn.setPassbookTxStatus(passbookList.get(passbookListIndex).status());
                                        passbookTxn.setPassbookTxType(passbookList.get(passbookListIndex).type());
                                        passbookTxn.setCreationDate(passbookList.get(passbookListIndex).createdAt());
                                        passbookTxn.setPayeeUpiId(payeeUpiId);
                                        passbookTxn.setVin(vin);

                                        passbookTxnList.add(passbookTxn);
                                    }
                                }

                                invoice.setPassbooksList(passbookTxnList);
                                invoiceList.add(invoice);
                            }

                            refreshInvoiceList(invoiceList);
                            fetchRentalInvoiceDataForVehicle();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fetchRentalInvoiceDataForVehicle();
                    }
                });
            }
        });
    }

    private void fetchRentalInvoiceDataForVehicle() {
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
        if(vehicle == null) {
            populateRentalInvoiceList(mInvoiceHashMap);
            return;
        }

        String vin = vehicle.getVin();

        if(vin == null || vin.isEmpty()) {
            populateRentalInvoiceList(mInvoiceHashMap);
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            populateRentalInvoiceList(mInvoiceHashMap);
            return;
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vin).build();
        FetchInvoicesForVehicleQuery fetchInvoicesForVehicleQuery = FetchInvoicesForVehicleQuery.builder().vehicleWhereUniqueInput(vehicleWhereUniqueInput).build();

        if(mRefreshingRentalInvoiceLinearLayout.getVisibility() != View.VISIBLE) {
            mRefreshingRentalInvoiceLinearLayout.setVisibility(View.VISIBLE);
        }

        apolloClient.query(fetchInvoicesForVehicleQuery).enqueue(new ApolloCall.Callback<FetchInvoicesForVehicleQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchInvoicesForVehicleQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            populateRentalInvoiceList(mInvoiceHashMap);
                        } else if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().invoices() != null) {

                            mRentalInvoicesListForVehicle = response.getData().vehicles().invoices();
                            if(mRentalInvoicesListForVehicle == null || mRentalInvoicesListForVehicle.isEmpty()) {
                                return;
                            }

                            List<Invoice> invoiceList = new ArrayList<>();

                            for(int index = 0 ; index < mRentalInvoicesListForVehicle.size() ; index++) {
                                Invoice invoice = new Invoice();
                                List<PassbookTxn> passbookTxnList = new ArrayList<>();

                                invoice.setInvoiceId(mRentalInvoicesListForVehicle.get(index).id());
                                invoice.setInvoiceNumber(mRentalInvoicesListForVehicle.get(index).number());
                                invoice.setInvoiceStatus(mRentalInvoicesListForVehicle.get(index).status());
                                if(mRentalInvoicesListForVehicle.get(index).payee() != null && mRentalInvoicesListForVehicle.get(index).payee().company() != null) {
                                    invoice.setPayeeName(mRentalInvoicesListForVehicle.get(index).payee().company().name());
                                }
                                if(mRentalInvoicesListForVehicle.get(index).payee() != null && mRentalInvoicesListForVehicle.get(index).payee().company() != null) {
                                    invoice.setPayeePhone(mRentalInvoicesListForVehicle.get(index).payee().company().phone());
                                }
                                if(mRentalInvoicesListForVehicle.get(index).payer() != null && mRentalInvoicesListForVehicle.get(index).payer().user() != null) {
                                    invoice.setPayerFirstName(mRentalInvoicesListForVehicle.get(index).payer().user().firstName());
                                }
                                if(mRentalInvoicesListForVehicle.get(index).payer() != null && mRentalInvoicesListForVehicle.get(index).payer().user() != null) {
                                    invoice.setPayerLastName(mRentalInvoicesListForVehicle.get(index).payer().user().lastName());
                                }
                                if(mRentalInvoicesListForVehicle.get(index).payer() != null && mRentalInvoicesListForVehicle.get(index).payer().user() != null) {
                                    invoice.setPayerPhone(mRentalInvoicesListForVehicle.get(index).payer().user().phone());
                                }
                                invoice.setInvoiceCreationDate(mRentalInvoicesListForVehicle.get(index).createdAt());

                                //Fetch and store payee upi id
                                List<FetchInvoicesForVehicleQuery.App> appsList = null;
                                String payeeUpiId = null;
                                if(mRentalInvoicesListForVehicle.get(index).payee() != null && mRentalInvoicesListForVehicle.get(index).payee().company() != null) {
                                    appsList = mRentalInvoicesListForVehicle.get(index).payee().company().apps();
                                }

                                if(appsList != null && !appsList.isEmpty()) {
                                    for(int appsListindex = 0 ; appsListindex < appsList.size() ; appsListindex++) {
                                        if(appsList.get(appsListindex).paymentDetails() != null) {
                                            if(appsList.get(appsListindex).paymentDetails().address() != null && !appsList.get(appsListindex).paymentDetails().address().isEmpty()) {
                                                payeeUpiId = appsList.get(appsListindex).paymentDetails().address();
                                                break;
                                            }
                                        }
                                    }
                                }

                                String vin = null;
                                FetchInvoicesForVehicleQuery.Asset asset = mRentalInvoicesListForVehicle.get(index).asset();
                                if(asset != null) {
                                    if(asset.vehicle() != null) {
                                        vin = asset.vehicle().vin();
                                    }
                                }

                                //store passbook List
                                List<FetchInvoicesForVehicleQuery.Passbook> passbookList = mRentalInvoicesListForVehicle.get(index).passbook();

                                if(passbookList != null && !passbookList.isEmpty()) {
                                    for(int passbookListIndex = 0 ; passbookListIndex < passbookList.size() ; passbookListIndex++) {
                                        PassbookTxn passbookTxn = new PassbookTxn();

                                        passbookTxn.setId(passbookList.get(passbookListIndex).id());
                                        passbookTxn.setAmount(passbookList.get(passbookListIndex).amount().toString());
                                        passbookTxn.setDueDate(passbookList.get(passbookListIndex).dueDate());
                                        passbookTxn.setPaymentDate(passbookList.get(passbookListIndex).paymentDate());
                                        passbookTxn.setClearanceDate(passbookList.get(passbookListIndex).clearanceDate());
                                        passbookTxn.setRemark(passbookList.get(passbookListIndex).remark());
                                        passbookTxn.setPassbookTxStatus(passbookList.get(passbookListIndex).status());
                                        passbookTxn.setPassbookTxType(passbookList.get(passbookListIndex).type());
                                        passbookTxn.setCreationDate(passbookList.get(passbookListIndex).createdAt());
                                        passbookTxn.setPayeeUpiId(payeeUpiId);
                                        passbookTxn.setVin(vin);

                                        passbookTxnList.add(passbookTxn);
                                    }
                                }
                                invoice.setPassbooksList(passbookTxnList);
                                invoiceList.add(invoice);
                            }
                            refreshInvoiceList(invoiceList);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        populateRentalInvoiceList(mInvoiceHashMap);
                    }
                });
            }
        });
    }

    private void refreshInvoiceList(List<Invoice> invoiceList) {
        if(invoiceList == null || invoiceList.isEmpty()) {
            return;
        }

        for(int index = 0 ; index < invoiceList.size() ; index++) {
            mInvoiceHashMap.put(invoiceList.get(index).getInvoiceId(), invoiceList.get(index));
        }

        populateRentalInvoiceList(mInvoiceHashMap);
    }

    private void populateRentalInvoiceList(HashMap<String, Invoice> invoiceHashMap) {
        mRefreshingRentalInvoiceLinearLayout.setVisibility(View.GONE);

        if(invoiceHashMap == null || invoiceHashMap.isEmpty()) {
            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_invoice_found));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        List<Invoice> invoiceList = new ArrayList<>();

        for (HashMap.Entry<String, Invoice> entry : invoiceHashMap.entrySet()) {
            invoiceList.add(entry.getValue());
        }

        //sort invoices by latest date first
        Collections.sort(invoiceList, new Comparator<Invoice>() {
            @Override
            public int compare(Invoice t1, Invoice t2) {
                DateTime dateTime1 = new DateTime(t1.getInvoiceCreationDate());
                DateTime dateTime2 = new DateTime(t2.getInvoiceCreationDate());

                return dateTime2.compareTo(dateTime1);
            }
        });

        RentalInvoiceInfoAdapter mRentalInvoiceInfoAdapter = new RentalInvoiceInfoAdapter(invoiceList, getActivity());
        mRentalInvoiceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRentalInvoiceRecyclerView.setAdapter(mRentalInvoiceInfoAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();

        mInvoiceHashMap = new HashMap<>();

        fetchRentalInvoiceDataForAccount();
    }
}