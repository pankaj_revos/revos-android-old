package com.revos.android.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.boltCore.android.activities.BoltMainActivity;
import com.boltCore.android.sdk.BoltSDK;
import com.boltCore.android.sdk.StatusCallBack;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.SphericalUtil;
import com.revos.android.R;
import com.revos.android.activities.ActiveBookingActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.LiveTrackingActivity;
import com.revos.android.activities.LoginActivity;
import com.revos.android.activities.RentalActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.SavedPlace;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.services.NordicBleService;
import com.revos.android.services.NotificationReaderService;
import com.revos.android.utilities.adapters.SavedPlacesAdapter;
import com.revos.android.utilities.animations.ResizeAnimation;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.type.ModelProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GOOGLE_MAPS_PACKAGE_NAME;
import static com.revos.android.constants.Constants.LAST_GOOGLE_NOTIFICATION_TIMESTAMP;
import static com.revos.android.constants.Constants.NAV_EVENT_SAVED_PLACE_LIST_UPDATED;
import static com.revos.android.constants.Constants.NAV_EVENT_TRIP_ADVICE_DATA_UPDATED;
import static com.revos.android.constants.Constants.NAV_PREFS;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.constants.Constants.SAVED_PLACES_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DESTINATION_NAME_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DISTANCE_FROM_TURN_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_ETA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_IS_REROUTING_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_NEXT_ROAD_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_REMAINING_DISTANCE_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_REMAINING_TIME_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LATITUDE;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

/**
 * Created by mohit on 11/9/17.
 */

public class NavigationFragment extends Fragment{
    /**views and layouts*/
    private View rootView;

    private final int LOCATION_BIAS_RADIUS_IN_METERS = 70000;

    private Animation mResizeCurrentDestinationCardAnimation;

    /**Timer for polling*/
    private Timer mTimer;

    private CardView mCurrentDestinationCardView, mAddDestinationCardView, mEnterDestinationCardView,
            mVehicleLastKnownLocationCardView, mVehicleRentalCardView, mSavedListsCardView;

    private ImageView mTurnIconImageView;

    private TextView mCurrentDestinationTextView, mDistanceFromTurnTextView, mNextRoadTextView,
            mETATextView;

    private LottieAnimationView mVehicleRentalLottieAnimationView;

    /**saved places list view*/
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SavedPlacesAdapter mSavedPlacesAdapter;

    private ArrayList<SavedPlace> mSavedPlacesList;

    private int PLACE_PICKER_SET_DESTINATION_REQUEST = 100;
    private int PLACE_PICKER_ADD_DESTINATION_REQUEST = 200;

    private final int CUR_DEST_CARD_MAX_HEIGHT_IN_DP = 160;
    private final int CUR_DEST_CARD_MIN_HEIGHT_IN_DP = 0;

    private String mVehicleLatitude, mVehicleLongitude;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.navigation_fragment, container, false);

        setupViews();

        setupTimer();

        return rootView;
    }

    private void setupViews() {
        //setup the enter destination card view
        mEnterDestinationCardView = (CardView) rootView.findViewById(R.id.nav_enter_destination_card_view);
        mEnterDestinationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEnterDestinationCardView.setEnabled(false);
                startPlacePickerActivity(PLACE_PICKER_SET_DESTINATION_REQUEST);
            }
        });

        setupCurrentDestinationRelatedViews();

        initializeSavedPlacesList();

        setupVehicleRentalCard();

        setupAddDestinationCard();

        setupVehicleLastKnownLocation();

        setupChargingCard();

    }

    private void setupChargingCard() {
        rootView.findViewById(R.id.nav_show_charging_card_view).setOnClickListener(view -> {
            if(getActivity() == null) {
                return;
            }

            // no need to pass user details like uid, name, phone etc here as USER_JSON_DATA already exists in prefs in case of REVOS main app
            BoltSDK.getInstance().initialize(getActivity(), "1234", null, new StatusCallBack() {
                @Override
                public void onStatusChange(String message) {
                    Timber.d("BOLT SDK status message: " + message);
                }

                @Override
                public void onError(String message) {
                    Timber.d("BOLT SDK error message: " + message);
                }

            }, null, null, null, null,  null, "https://amzn.to/3jlNyZF");


            BoltSDK.getInstance().launchBolt();
        });
    }

    private void setupVehicleRentalCard() {
        mVehicleRentalCardView = rootView.findViewById(R.id.nav_vehicle_rental_card_view);
        mVehicleRentalLottieAnimationView = rootView.findViewById(R.id.nav_vehicle_rental_pulse_lottie_view);

        mVehicleRentalCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                String rentalActiveVehicleString = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);

                RentalVehicle rentalVehicle = new Gson().fromJson(rentalActiveVehicleString, RentalVehicle.class);

                if(rentalVehicle != null && rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {
                    Intent intent = new Intent(getActivity(), ActiveBookingActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), RentalActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    private void setupAddDestinationCard() {

        mAddDestinationCardView = (CardView)rootView.findViewById(R.id.nav_add_destination_card_view);
        mAddDestinationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAddDestinationCardView.setEnabled(false);
                startPlacePickerActivity(PLACE_PICKER_ADD_DESTINATION_REQUEST);
            }
        });
    }

    private void setupVehicleLastKnownLocation() {

        if(getActivity() == null) {
            return;
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE);

        mVehicleLatitude = sharedPreferences.getString(VEHICLE_LAST_KNOWN_LATITUDE, null);
        mVehicleLongitude = sharedPreferences.getString(VEHICLE_LAST_KNOWN_LONGITUDE, null);

        mVehicleLastKnownLocationCardView = rootView.findViewById(R.id.nav_vehicle_last_location_card_view);

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            //change text and click behavior of the cardview
            ((TextView) rootView.findViewById(R.id.nav_last_parked_location_text_view)).setText(getString(R.string.tap_to_locate_vehicle));
        }

        mVehicleLastKnownLocationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                if(vehicle == null) {
                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.connect_or_setup_vehicle_to_view_location));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    return;
                }

                Intent intent = new Intent(getActivity(), LiveTrackingActivity.class);
                startActivity(intent);
            }
        });
    }

    private void startPlacePickerActivity(int requestCode) {
        if(getActivity() == null) {
            return;
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
        Intent intent;
        if(NordicBleService.mCurrentLocation == null) {

            // Start the autocomplete intent.
             intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields)
                    .build(getActivity());
        } else {

            RectangularBounds rectangularBounds = RectangularBounds.newInstance(getRectangularBounds(new LatLng(NordicBleService.mCurrentLocation.getLatitude(), NordicBleService.mCurrentLocation.getLongitude()), LOCATION_BIAS_RADIUS_IN_METERS));
            // Start the autocomplete intent.
            intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields)
                    .setLocationBias(rectangularBounds)
                    .build(getActivity());
        }
        startActivityForResult(intent, requestCode);
    }

    private LatLngBounds getRectangularBounds(LatLng center, double radiusInMeters) {
        double distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0);
        return new LatLngBounds(southwestCorner, northeastCorner);
    }

    private void setupCurrentDestinationRelatedViews() {
        mCurrentDestinationCardView = (CardView)rootView.findViewById(R.id.nav_current_destination_card_view);
        mCurrentDestinationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentDestinationCardView.setEnabled(false);
                startGoogleMaps();
            }
        });

        mCurrentDestinationTextView = (TextView)rootView.findViewById(R.id.nav_current_destination_text_view);
        mDistanceFromTurnTextView = (TextView)rootView.findViewById(R.id.nav_distance_from_turn_text_view);
        mNextRoadTextView = (TextView)rootView.findViewById(R.id.nav_next_road_text_view);
        mETATextView = (TextView)rootView.findViewById(R.id.nav_eta_view);

        mTurnIconImageView = (ImageView)rootView.findViewById(R.id.nav_turn_icon_image_view);
    }

    private void startGoogleMaps() {
        Uri gmmIntentUri = Uri.parse("geo:0,0");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage(GOOGLE_MAPS_PACKAGE_NAME);
        startActivity(mapIntent);
    }

    private void startNavigation(String name, String address) {
        String queryString = name + ", " + address;
        String mapQueryString = "google.navigation:q=" + queryString + "&mode=d";

        Uri gmmIntentUri = Uri.parse(mapQueryString);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage(GOOGLE_MAPS_PACKAGE_NAME);
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mapIntent);
    }

    private void updateNavAdviceViews() {
        if(getActivity() == null){
            return;
        }

        HashMap<String, Object> tripAdviceDataHashMap = null;
        String tripDataStr = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(TRIP_ADVICE_DATA_KEY, null);
        if(tripDataStr != null) {
            Type hashMapType = new TypeToken<HashMap<String, Object>>(){}.getType();
            tripAdviceDataHashMap = new Gson().fromJson(tripDataStr, hashMapType);
        } else {
            return;
        }

        //updating nav advice if rerouting
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_IS_REROUTING_KEY)) {
            String isReroutingKeyValue = (String)tripAdviceDataHashMap.get(TRIP_ADVICE_IS_REROUTING_KEY);
            if(isReroutingKeyValue != null && isReroutingKeyValue.equals("yes")) {

                mCurrentDestinationTextView.setText(getString(R.string.rerouting_to_destination_nav_advice));
                mDistanceFromTurnTextView.setText("");
                mNextRoadTextView.setText("");
                mETATextView.setText("");
                mTurnIconImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_reroute_svg));
                return;
            }
        }

        //update the destination name
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_DESTINATION_NAME_KEY)) {
            mCurrentDestinationTextView.setText((String)tripAdviceDataHashMap.get(TRIP_ADVICE_DESTINATION_NAME_KEY));
        }

        if(NotificationReaderService.mCachedNavigationBitmap != null) {
            //update the turn icon
            Glide.with(this).load(NotificationReaderService.mCachedNavigationBitmap).into(mTurnIconImageView);
        }

        //update the distance from turn
        //todo::handle localization
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_DISTANCE_FROM_TURN_KEY)) {
            double distanceFromTurnInMeters = (double) tripAdviceDataHashMap.get(TRIP_ADVICE_DISTANCE_FROM_TURN_KEY);
            String distanceFromTurnStr;
            String unitStr;
            if(distanceFromTurnInMeters >= 1000) {
                double distanceFromTurnInKilos = distanceFromTurnInMeters / 1000;
                distanceFromTurnStr = String.format("%.1f", distanceFromTurnInKilos);
                unitStr = "km";
            } else {
                distanceFromTurnStr = String.valueOf(Math.round(distanceFromTurnInMeters));
                unitStr = "m";
            }

            mDistanceFromTurnTextView.setText(String.format("%s%s", distanceFromTurnStr, unitStr));
        } else {
            mDistanceFromTurnTextView.setText("");
        }

        //update the next road text view
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_NEXT_ROAD_KEY)) {
            mNextRoadTextView.setText((String)tripAdviceDataHashMap.get(TRIP_ADVICE_NEXT_ROAD_KEY));
        }

        String etaStr, remainingDistanceStr, remainingTimeStr;
        etaStr = remainingDistanceStr = remainingTimeStr = "";
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_ETA_KEY)) {
            etaStr = (String)tripAdviceDataHashMap.get(TRIP_ADVICE_ETA_KEY);
        }

        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_REMAINING_DISTANCE_KEY)) {
            double remainingDistanceInMeters = (double) tripAdviceDataHashMap.get(TRIP_ADVICE_REMAINING_DISTANCE_KEY);
            String remainingDistanceValueStr;
            String unitStr;
            if(remainingDistanceInMeters >= 1000) {
                double distanceFromTurnInKilos = remainingDistanceInMeters / 1000;
                remainingDistanceValueStr = String.format("%.1f", distanceFromTurnInKilos);
                unitStr = getString(R.string.kilometers);
            } else {
                remainingDistanceValueStr = String.valueOf(Math.round(remainingDistanceInMeters));
                unitStr = getString(R.string.meters);
            }

            remainingDistanceStr = remainingDistanceValueStr + unitStr;
        }

        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_REMAINING_TIME_KEY)) {
            int remainingTimeInMinutes = Math.round(Float.parseFloat(tripAdviceDataHashMap.get(TRIP_ADVICE_REMAINING_TIME_KEY).toString()));
            int hours = remainingTimeInMinutes / 60;
            int minutes = remainingTimeInMinutes % 60;

            String hoursText = hours > 0 ? "" + hours + " " + getString(R.string.hours) + " " : "";
            String minuteText = minutes + " " + getString(R.string.minutes);

            remainingTimeStr = hoursText + minuteText;
        }

        //update the eta summary text
        String etaSummary = String.format(getResources().getString(R.string.eta_summary), remainingTimeStr, remainingDistanceStr, etaStr);
        mETATextView.setText(etaSummary);
    }

    private void toggleCurrentDestinationCardIfRequired() {
        if(getActivity() == null) {
            return;
        }

        long lastGoogleNotificationTimestamp = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getLong(LAST_GOOGLE_NOTIFICATION_TIMESTAMP, 0);


        if(mResizeCurrentDestinationCardAnimation == null || !mResizeCurrentDestinationCardAnimation.hasStarted() || mResizeCurrentDestinationCardAnimation.hasEnded()) {

            if(System.currentTimeMillis() - lastGoogleNotificationTimestamp <= 1500) {
                if(mCurrentDestinationCardView.getVisibility() == View.GONE || mCurrentDestinationCardView.getVisibility() == View.INVISIBLE) {
                    resizeCurrentDestinationCardWithAnimation(true);
                }
            } else {
                if(mCurrentDestinationCardView.getVisibility() == View.VISIBLE) {
                    resizeCurrentDestinationCardWithAnimation(false);
                }
            }
        }
    }

    private void resizeCurrentDestinationCardWithAnimation(final boolean expandBanner) {
        float startHeight;
        float endHeight;

        startHeight = mCurrentDestinationCardView.getHeight();

        if(expandBanner) {
            endHeight = CUR_DEST_CARD_MAX_HEIGHT_IN_DP * getScreenDensity();
        } else {
            endHeight = CUR_DEST_CARD_MIN_HEIGHT_IN_DP;
        }

        mResizeCurrentDestinationCardAnimation = new ResizeAnimation(mCurrentDestinationCardView,
                mCurrentDestinationCardView.getWidth(), startHeight,
                mCurrentDestinationCardView.getWidth(), endHeight);

        mResizeCurrentDestinationCardAnimation.setInterpolator(new AccelerateInterpolator());
        mResizeCurrentDestinationCardAnimation.setDuration(500);
        mCurrentDestinationCardView.setAnimation(mResizeCurrentDestinationCardAnimation);
        mCurrentDestinationCardView.startAnimation(mResizeCurrentDestinationCardAnimation);

        mResizeCurrentDestinationCardAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if(expandBanner) {
                    updateNavAdviceViews();
                    mCurrentDestinationCardView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(expandBanner) {
                    mCurrentDestinationCardView.setVisibility(View.VISIBLE);
                } else {

                    mCurrentDestinationCardView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void initializeSavedPlacesList() {
        if(getActivity() == null){
            return;
        }

        //setup the card view that holds the saved places list
        mSavedListsCardView = rootView.findViewById(R.id.nav_saved_destinations_card_view);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.nav_recycler_view);

        updateSavedPlacesListAndToggleCardView();

        //use a grid layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //set the adapter
        mSavedPlacesAdapter = new SavedPlacesAdapter(mSavedPlacesList, getActivity());
        mRecyclerView.setAdapter(mSavedPlacesAdapter);
    }

    private void toggleRentalCardViewDetailsIfRequired() {
        if(getActivity() == null) {
            return;
        }

        String activeRentalBookingString = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);

        RentalVehicle rentalVehicle = new Gson().fromJson(activeRentalBookingString, RentalVehicle.class);

        if(rentalVehicle != null && rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {
            mVehicleRentalLottieAnimationView.setVisibility(View.VISIBLE);
        } else {
            mVehicleRentalLottieAnimationView.setVisibility(View.INVISIBLE);
        }
    }

    private void updateSavedPlacesListAndToggleCardView() {
        String savedPlacesListStr = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(SAVED_PLACES_DATA_KEY, null);

        if(savedPlacesListStr != null && !savedPlacesListStr.isEmpty()) {

            mSavedPlacesList = new Gson().fromJson(savedPlacesListStr, new TypeToken<ArrayList<SavedPlace>>(){}.getType());

            if(mSavedPlacesList != null && !mSavedPlacesList.isEmpty()) {
                mSavedListsCardView.setVisibility(View.VISIBLE);
            } else {
                mSavedListsCardView.setVisibility(View.GONE);
            }
        } else {
            mSavedListsCardView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PLACE_PICKER_SET_DESTINATION_REQUEST) {
            if(resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                startNavigation(place.getName(), place.getAddress());
            }
        } else if(requestCode == PLACE_PICKER_ADD_DESTINATION_REQUEST) {
            if(resultCode == RESULT_OK) {
                if(getActivity() == null){
                    return;
                }

                Place place = Autocomplete.getPlaceFromIntent(data);

                SavedPlace savedPlace = new SavedPlace();

                if(place != null && place.getLatLng() != null) {
                    savedPlace.setLatitude(place.getLatLng().latitude);
                    savedPlace.setLongitude(place.getLatLng().longitude);
                }

                savedPlace.setName(place.getName());
                savedPlace.setAddress(place.getAddress());

                mSavedPlacesAdapter.add(0, savedPlace);

                updateSavedPlacesListAndToggleCardView();
            }
        }
    }

    private float getScreenDensity() {

        if(getActivity() == null) {
            return 0.0f;
        }

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        return getResources().getDisplayMetrics().density;
    }

    private void setupTimer() {
        //start infinite polling for checking bluetooth connection state
        //check if notification advice has to be displayed or not
        if(mTimer == null) {
            mTimer = new Timer();
        }
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if(getActivity() == null || !getActivity().hasWindowFocus()) {
                                return;
                            }
                            toggleCurrentDestinationCardIfRequired();
                            toggleRentalCardViewDetailsIfRequired();
                            updateSavedPlacesListAndToggleCardView();
                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }
                    }
                });
            }
        }, 100, 2000);
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED)) {
            if(getActivity() == null) {
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        updateNavAdviceViews();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(NAV_EVENT_SAVED_PLACE_LIST_UPDATED)) {
            if(getActivity() == null) {
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }

                        updateSavedPlacesListAndToggleCardView();

                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if (event.message.contains(OBD_EVENT_LIVE_DATA)) {
            if(getActivity() == null) {
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mAddDestinationCardView != null){
            mAddDestinationCardView.setEnabled(true);
        }

        if(mEnterDestinationCardView != null) {
            mEnterDestinationCardView.setEnabled(true);
        }

        if(mCurrentDestinationCardView != null) {
            mCurrentDestinationCardView.setEnabled(true);
        }

        if(mVehicleLastKnownLocationCardView != null) {
            mVehicleLastKnownLocationCardView.setEnabled(true);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }

        super.onDestroy();
    }

}
