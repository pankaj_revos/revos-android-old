package com.revos.android.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.revos.android.R;

public class NotificationAccessMessageFragment extends AAH_FabulousFragment {

    private final int REQUEST_NOTIFICATION_ACCESS_SETTING = 202;

    public static NotificationAccessMessageFragment newInstance() {
        NotificationAccessMessageFragment f = new NotificationAccessMessageFragment();
        return f;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.notification_access_message_fab_fragment, null);
        RelativeLayout fabFilterRelativeLayout = contentView.findViewById(R.id.fab_filter_relative_layout);
        LottieAnimationView fabFilterLottieWarningSign = contentView.findViewById(R.id.fab_filter_lottie_image_view);
        TextView notificationAccessMessageTextView = contentView.findViewById(R.id.fab_filter_notification_accessmsg_text_view);

        contentView.findViewById(R.id.fab_filter_button_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFilter("closed");
            }
        });

        fabFilterRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), REQUEST_NOTIFICATION_ACCESS_SETTING);
                closeFilter("closed");
            }
        });

        fabFilterLottieWarningSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), REQUEST_NOTIFICATION_ACCESS_SETTING);
                closeFilter("closed");
            }
        });

        notificationAccessMessageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), REQUEST_NOTIFICATION_ACCESS_SETTING);
                closeFilter("closed");
            }
        });

        //params to set
        setAnimationDuration(300); //optional; default 500ms
        setPeekHeight(300); // optional; default 400dp
        setViewMain(fabFilterRelativeLayout); //necessary; main bottomsheet view
        setMainContentView(contentView); // necessary; call at end before super
        super.setupDialog(dialog, style); //call super at last
    }
}