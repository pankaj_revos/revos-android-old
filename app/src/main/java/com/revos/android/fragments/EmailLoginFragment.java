package com.revos.android.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.LoginActivity;
import com.revos.android.constants.Constants;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import timber.log.Timber;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;


public class EmailLoginFragment extends Fragment {

    private View mRootView;

    private EditText mEmailEditTextView, mPasswordEditTextView, mForgotPasswordEnterEmailIdEditText;

    private Button mLoginViaEmailButton, mForgotPasswordSubmitButton;

    private ImageView mLogoImageView;

    private LinearLayout mLoginViaEmailLinearLayout, mForgotPasswordLinearLayout, mForgotPasswordEnterEmailIdLinearLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.login_via_email_fragment, container, false);

        setupViews();
        updateLogoIfRequired();

        return mRootView;
    }

    private void setupViews() {
         mLoginViaEmailLinearLayout = mRootView.findViewById(R.id.login_via_email_linear_layout);
         mEmailEditTextView = mRootView.findViewById(R.id.login_email_id_edit_text);
         mPasswordEditTextView = mRootView.findViewById(R.id.login_email_password_edit_text);
         mLoginViaEmailButton = mRootView.findViewById(R.id.login_via_email_button);

         mLogoImageView = mRootView.findViewById(R.id.login_via_email_logo_image_view);

         mForgotPasswordLinearLayout = mRootView.findViewById(R.id.login_email_forgot_password_linear_layout);

         mForgotPasswordEnterEmailIdLinearLayout = mRootView.findViewById(R.id.forgot_password_enter_email_id_linear_layout);
         mForgotPasswordEnterEmailIdEditText = mRootView.findViewById(R.id.forgot_password_email_id_edit_text);
         mForgotPasswordSubmitButton = mRootView.findViewById(R.id.forgot_password_submit_email_id_button);

         mLoginViaEmailButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 showProgressDialog();

                 String emailId = mEmailEditTextView.getText().toString().trim();
                 String password = mPasswordEditTextView.getText().toString().trim();

                 if(emailId.isEmpty() && password.isEmpty()) {
                     hideProgressDialog();

                     Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                     Bundle bundle = new Bundle();
                     bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.enter_email_id_and_password_to_sign_in));
                     bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                     bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                     intent.putExtras(bundle);
                     startActivity(intent);

                     return;
                 } else if (emailId.isEmpty()) {
                     hideProgressDialog();

                     Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                     Bundle bundle = new Bundle();
                     bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.enter_email_id));
                     bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                     bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                     intent.putExtras(bundle);
                     startActivity(intent);

                     return;
                 } else if (password.isEmpty()) {
                     hideProgressDialog();

                     Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                     Bundle bundle = new Bundle();
                     bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.enter_password));
                     bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                     bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                     intent.putExtras(bundle);
                     startActivity(intent);

                     return;
                 }

                 signInWithEmailCredentials(emailId, password);
             }
         });

         mForgotPasswordLinearLayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 toggleEmailLoginAndForgotPasswordUi();
             }
         });

         mForgotPasswordSubmitButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 showProgressDialog();

                 String email = mForgotPasswordEnterEmailIdEditText.getText().toString().trim();

                 if(email.isEmpty()) {
                     Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                     Bundle bundle = new Bundle();
                     bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.enter_email_id));
                     bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
                     bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                     intent.putExtras(bundle);
                     startActivity(intent);

                 } else {
                     FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                             .addOnCompleteListener(new OnCompleteListener<Void>() {
                                 @Override
                                 public void onComplete(@NonNull Task<Void> task) {
                                     hideProgressDialog();

                                     if (task.isSuccessful()) {
                                         Timber.d("Password reset email sent.");

                                         Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                         Bundle bundle = new Bundle();
                                         bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.password_reset_email_sent));
                                         bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                         bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2500);
                                         intent.putExtras(bundle);
                                         startActivity(intent);

                                         toggleEmailLoginAndForgotPasswordUi();
                                     } else {
                                         Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                         Bundle bundle = new Bundle();
                                         bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_process_request));
                                         bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                         bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2500);
                                         intent.putExtras(bundle);
                                         startActivity(intent);
                                     }
                                 }
                             });
                 }
             }
         });


    }

    private void toggleEmailLoginAndForgotPasswordUi() {

        if(mLoginViaEmailLinearLayout.getVisibility() == View.VISIBLE) {

            mLoginViaEmailLinearLayout.setVisibility(View.GONE);
            mForgotPasswordEnterEmailIdLinearLayout.setVisibility(View.VISIBLE);
            mForgotPasswordEnterEmailIdEditText.setText("");

        } else if(mForgotPasswordEnterEmailIdLinearLayout.getVisibility() == View.VISIBLE) {

            mForgotPasswordEnterEmailIdLinearLayout.setVisibility(View.GONE);
            mLoginViaEmailLinearLayout.setVisibility(View.VISIBLE);

        }

    }

    private void updateLogoIfRequired() {
        if(getActivity() == null){
            return;
        }

        //see if bike logo is available
        //read info from shared prefs
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.GENERAL_PREFS, Context.MODE_PRIVATE);

        String oemLogoFilePath = sharedPreferences.getString(Constants.COMPANY_LOGO_FILE_PATH_KEY, null);

        if(oemLogoFilePath != null) {
            File file = new File(oemLogoFilePath);
            if(file.exists()) {
                //TODO::uncomment this
                /*Bitmap logoBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                logoView.setImageBitmap(logoBitmap);*/
                //TODO::comment this
                mLogoImageView.setBackgroundColor(Color.parseColor("#00FF00"));
                return;
            }
        }

        mLogoImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_center));
        mLogoImageView.setVisibility(View.VISIBLE);
    }

    private void signInWithEmailCredentials(String emailId, String password) {
        if(getActivity() == null) {
            hideProgressDialog();
            return;
        }

        FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(emailId, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                hideProgressDialog();
                if(task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Timber.d("signInWithEmailCredential:success");

                } else {
                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, task.getException().getMessage());
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2500);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

    }

    private void showProgressDialog() {
        if(getActivity() != null) {
            ((LoginActivity)getActivity()).showProgressDialog();
        }
    }

    private void hideProgressDialog() {
        if(getActivity() != null) {
            ((LoginActivity)getActivity()).hideProgressDialog();
        }
    }
}
