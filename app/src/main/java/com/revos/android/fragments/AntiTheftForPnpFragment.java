package com.revos.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.revos.android.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AntiTheftForPnpFragment extends Fragment {

    private View mRootView;

    private com.suke.widget.SwitchButton mAntiTheftSwitch;

    private LinearLayout mAntiTheftLinearLayout, mAntiTheftModeLinearLayout, mAntiTheftSensitivityLinearLayout;

    private RadioGroup mAntiTheftModeRadioGroup, mAntiTheftSensitivityRadioGroup;

    private RadioButton mAntiTheftModeNormalRadioButton, mAntiTheftModeSilentRadioButton, mAntiTheftSensitivityLowRadioButton,
                        mAntiTheftSensitivityMediumRadioButton, mAntiTheftSensitivityHighRadioButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.anti_theft_for_pnp, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        mAntiTheftSwitch = mRootView.findViewById(R.id.pnp_anti_theft_switch);
        mAntiTheftLinearLayout = mRootView.findViewById(R.id.pnp_anti_theft_linear_layout);
        mAntiTheftModeLinearLayout = mRootView.findViewById(R.id.pnp_anti_theft_mode_linear_layout);
        mAntiTheftModeRadioGroup = mRootView.findViewById(R.id.pnp_anti_theft_mode_radio_group);
        mAntiTheftModeNormalRadioButton = mRootView.findViewById(R.id.pnp_anti_theft_mode_normal);
        mAntiTheftModeSilentRadioButton = mRootView.findViewById(R.id.pnp_anti_theft_mode_silent);
        mAntiTheftSensitivityLowRadioButton = mRootView.findViewById(R.id.pnp_anti_theft_sensitivity_low);
        mAntiTheftSensitivityMediumRadioButton = mRootView.findViewById(R.id.pnp_anti_theft_sensitivity_medium);
        mAntiTheftSensitivityHighRadioButton = mRootView.findViewById(R.id.pnp_anti_theft_sensitivity_high);
        mAntiTheftSensitivityLinearLayout = mRootView.findViewById(R.id.pnp_anti_theft_sensitivity_linear_layout);
        mAntiTheftSensitivityRadioGroup = mRootView.findViewById(R.id.pnp_anti_theft_sensitivity_radio_group);
    }
}
