package com.revos.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hbb20.CountryCodePicker;
import com.revos.android.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by mohit on 6/9/17.
 */

public class RegisterHelpFragment extends Fragment {
    /**views and layouts*/
    private View rootView;

    private CountryCodePicker mEnterPhoneNumCcp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.register_help_fragment, container, false);

        return rootView;
    }

}
