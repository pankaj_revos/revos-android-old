package com.revos.android.fragments;

import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.MainActivity;
import com.revos.android.activities.TripHistoryInfoActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.DataFeedBatteryVoltageAdc;
import com.revos.android.jsonStructures.DataFeedVehicleLiveLog;
import com.revos.android.jsonStructures.Model;
import com.revos.android.jsonStructures.TripDataFeed;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.NewsRainApiClient;
import com.revos.android.retrofit.NewsRainApiInterface;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.general.DateTimeUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.general.RevosFileUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.type.ModelProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTime;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.activities.MainActivity.mCurrentThemeID;
import static com.revos.android.constants.Constants.AVERAGE_PETROL_PRICE_KEY;
import static com.revos.android.constants.Constants.HISTORICAL_DATA_IS_REFRESHING_KEY;
import static com.revos.android.constants.Constants.HISTORICAL_DATA_LIST_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.LOG_TYPE_OFFLINE;
import static com.revos.android.constants.Constants.LOG_TYPE_LIVE;
import static com.revos.android.constants.Constants.LOG_TYPE_OFFLINE_BATTERY_ADC;
import static com.revos.android.constants.Constants.NETWORK_EVENT_TRIP_DATA_REFRESHED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_DATA_IS_REFRESHING_KEY;
import static com.revos.android.constants.Constants.TRIP_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_DATA_NEEDS_REFRESH_KEY;
import static com.revos.android.constants.Constants.TRIP_PREFS;

/**
 * Created by mohit on 11/9/17.
 */

public class AnalyticsFragment extends Fragment{
    /**views and layouts*/
    private View mRootView;

    private LottieAnimationView mLogStatusLottieImageView;

    private TextView mLogStatusTextView, mTripsHistoryTextView, mMaxDistanceTextView,
                        mTotalFuelMoneySavedTextView, mTotalCarbonEmissionSavedTextView,
                        mTotalDistanceTextView, mAvgDistanceTextView, mMaxMileageTextView,
                        mAvgMileageTextView,  mMaxBatteryUsageTextView, mAvgBatteryUsageTextView;

    private BarChart mDistanceBarChart, mMileageBarChart, mBatteryUsageBarChart;
    private LineChart mBatteryStatusLineChart;

    private Timer mTimer;

    private LinearLayout mVehicleSetupNotCompleteLinearLayout, mLogsSyncStatusLinearLayout,
                         mRefreshingTripDataLinearLayout, mFuelAndCarbonSavedLinearLayout,
                         mTripsHistoryLinearLayout, mNoTripInfoAvailableLinearLayout,
                         mFuelAndCarbonEmissionStatsNotFoundLinearLayout;

    private Button mVehicleSetupNotCompleteOkButton;

    private CardView mTripsAndSyncLogsCardView, mGreenSavingsCardView, mMileageGraphCardView,
                     mBatteryStatusGraphCardView, mDistanceGraphCardView, mBatteryUsageGraphCardView;

    private ScrollView mAnalyticsScrollView;

    private final int ANIMATION_DURATION = 500;

    private final int NO_OF_TRIPS_TO_BE_SHOWN = 10;

    private double mVehicleOdoMeterValue = 0;

    private Vehicle mVehicle = null;

    private ArrayList<TripDataFeed> mProcessedTripList = null;

    public static final int MINIMUM_TRIP_DISTANCE = 100;  //in meters

    private final int BATTERY_GRAPH_XAXIS_LABEL_COUNT = 6;
    private final int BATTERY_GRAPH_YAXIS_LABEL_COUNT = 5;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.analytics_fragment, container, false);

        setupViews();

        refreshAnalyticsViews();

        initializeTimer();

        return mRootView;
    }

    private void setupViews() {

        if(getActivity() == null) {
            return;
        }

        mVehicleSetupNotCompleteLinearLayout = mRootView.findViewById(R.id.analytics_vehicle_setup_not_complete_linear_layout);
        mVehicleSetupNotCompleteOkButton     = mRootView.findViewById(R.id.analytics_vehicle_setup_not_complete_ok_button);
        mNoTripInfoAvailableLinearLayout     = mRootView.findViewById(R.id.analytics_no_trip_info_available_linear_layout);
        mTripsAndSyncLogsCardView            = mRootView.findViewById(R.id.analytics_trips_and_sync_logs_card_view);
        mLogsSyncStatusLinearLayout          = mRootView.findViewById(R.id.analytics_logs_sync_status_linear_layout);
        mAnalyticsScrollView                 = mRootView.findViewById(R.id.analytics_scroll_view);
        mDistanceGraphCardView               = mRootView.findViewById(R.id.analytics_distance_graph_card_view);
        mMileageGraphCardView                = mRootView.findViewById(R.id.analytics_mileage_card_view);
        mBatteryStatusGraphCardView          = mRootView.findViewById(R.id.analytics_battery_status_card_view);
        mBatteryUsageGraphCardView           = mRootView.findViewById(R.id.analytics_battery_usage_card_view);
        mGreenSavingsCardView                = mRootView.findViewById(R.id.analytics_green_savings_card_view);
        mFuelAndCarbonSavedLinearLayout      = mRootView.findViewById(R.id.analytics_money_and_carbon_emission_saved_linear_view);
        mFuelAndCarbonEmissionStatsNotFoundLinearLayout = mRootView.findViewById(R.id.analytics_ride_ev_to_view_green_savings_stats_linear_layout);


        mVehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        //check if vehicle setup is complete or not
        if(mVehicle == null) {
            mVehicleSetupNotCompleteLinearLayout.setVisibility(View.VISIBLE);
            mVehicleSetupNotCompleteOkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getActivity() == null) { return; }

                    ((MainActivity)getActivity()).mShowSetupVehicleFragment = true;
                    ((MainActivity)getActivity()).refreshFragments();
                }
            });
        } else {
            mTripsAndSyncLogsCardView.setVisibility(View.VISIBLE);
            mAnalyticsScrollView.setVisibility(View.VISIBLE);

            setupTripAndLogRelatedViews();
            setupGreenSavingsRelatedViews();
            setupDistanceAnalyticsViews();
            setupBatteryStatusAnalyticsViews();

            if(mVehicle != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                //hide logs sync status linear layout
                toggleSyncLogsLayout(false);

                //hide mileage views
                mMileageGraphCardView.setVisibility(View.GONE);
                mBatteryUsageGraphCardView.setVisibility(View.VISIBLE);
                setupBatteryUsageAnalyticsViews();
            } else {
                //show logs sync status linear layout
                toggleSyncLogsLayout(true);

                //hide battery usage views
                mBatteryUsageGraphCardView.setVisibility(View.GONE);
                mBatteryStatusGraphCardView.setVisibility(View.GONE);
                mMileageGraphCardView.setVisibility(View.VISIBLE);
                setupMileageAnalyticsViews();
            }
        }
    }

    private void toggleSyncLogsLayout(boolean showLayout) {
        if(getActivity() == null) {
            return;
        }

        int visibility = View.VISIBLE;
        int[] attrs = new int[]{R.attr.colorViewTripHistoryBackground};

        if(!showLayout) {
            visibility = View.INVISIBLE;
            attrs = new int[]{R.attr.colorBackgroundFill};
        }

        mLogStatusLottieImageView.setVisibility(visibility);
        mLogStatusTextView.setVisibility(visibility);
        TypedArray a = getActivity().obtainStyledAttributes(mCurrentThemeID, attrs);
        int attributeResourceId = a.getResourceId(0, 0);
        Drawable syncLogsBackground = ContextCompat.getDrawable(getActivity(), attributeResourceId);
        mLogsSyncStatusLinearLayout.setBackground(syncLogsBackground);

    }

    private void setupTripAndLogRelatedViews() {
        mLogStatusLottieImageView = mRootView.findViewById(R.id.analytics_log_status_lottie_image_view);
        mLogStatusTextView = mRootView.findViewById(R.id.analytics_log_status_text_view);
        mRefreshingTripDataLinearLayout = mRootView.findViewById(R.id.refreshing_trip_data_linear_layout);

        mTripsHistoryTextView = mRootView.findViewById(R.id.analytics_trip_history_text_view);
        mTripsHistoryLinearLayout = mRootView.findViewById(R.id.analytics_trips_history_linear_view);

        mTripsHistoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decideAndOpenTripHistoryActivity();
            }
        });

        mTripsHistoryLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decideAndOpenTripHistoryActivity();
            }
        });

    }

    private void setupGreenSavingsRelatedViews() {
        mTotalFuelMoneySavedTextView = mRootView.findViewById(R.id.analytics_money_saved_text_view);
        mTotalCarbonEmissionSavedTextView = mRootView.findViewById(R.id.analytics_carbon_emission_saved_text_view);
    }

    private void setupDistanceAnalyticsViews() {
        mDistanceBarChart = (BarChart)mRootView.findViewById(R.id.analytics_distance_chart);
        mMaxDistanceTextView = (TextView)mRootView.findViewById(R.id.analytics_max_distance_text_view);
        mTotalDistanceTextView = (TextView)mRootView.findViewById(R.id.analytics_total_distance_text_view);
        mAvgDistanceTextView = (TextView)mRootView.findViewById(R.id.analytics_avg_distance_text_view);
    }

    private void setupMileageAnalyticsViews() {
        mMileageBarChart = (BarChart)mRootView.findViewById(R.id.analytics_mileage_chart);
        mMaxMileageTextView = (TextView)mRootView.findViewById(R.id.analytics_max_mileage_text_view);
        mAvgMileageTextView = (TextView)mRootView.findViewById(R.id.analytics_avg_mileage_text_view);
    }

    private void setupBatteryUsageAnalyticsViews() {
        mBatteryUsageBarChart = mRootView.findViewById(R.id.analytics_battery_usage_chart);
        mMaxBatteryUsageTextView = mRootView.findViewById(R.id.analytics_max_battery_used_text_view);
        mAvgBatteryUsageTextView = mRootView.findViewById(R.id.analytics_avg_battery_used_text_view);
    }

    private void setupBatteryStatusAnalyticsViews() {
        mBatteryStatusLineChart = mRootView.findViewById(R.id.analytics_battery_status_chart);
    }


    private void decideAndOpenTripHistoryActivity() {

        if(NetworkUtils.getInstance(getActivity()).mTripDataDownloadInProgress && (mProcessedTripList == null || mProcessedTripList.isEmpty())) {
            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.downloading_trip_data));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        if(mProcessedTripList == null || mProcessedTripList.isEmpty()) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_trips_made_yet));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        Intent intent = new Intent(getActivity(), TripHistoryInfoActivity.class);
        startActivity(intent);
    }

    private void initializeTimer() {
        if(mTimer == null) {
            mTimer = new Timer();
        }

        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getActivity() == null || !getActivity().hasWindowFocus()) {
                                return;
                            }
                            setLogStatusImageViewIcon();
                            refreshTripDataIfRequired();
                            boolean isTripDataRefreshing = getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).getBoolean(TRIP_DATA_IS_REFRESHING_KEY, false);
                            boolean isHistoricalDataRefreshing = getActivity().getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getBoolean(HISTORICAL_DATA_IS_REFRESHING_KEY, false);
                            if((isHistoricalDataRefreshing || isTripDataRefreshing) && mVehicle != null) {
                                mRefreshingTripDataLinearLayout.setVisibility(View.VISIBLE);
                            } else{
                                mRefreshingTripDataLinearLayout.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }
                    }
                });
            }
        }, 0,1000);
    }

    private void refreshAnalyticsViews() {
        if(getActivity() == null || mVehicle == null) {
            return;
        }

        refreshPetrolPriceAndCarbonEmissionSavedViews();

        String tripDataStr = getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).getString(TRIP_DATA_KEY, null);

        if(tripDataStr == null) {
            return;
        }

        ArrayList<TripDataFeed> tripList = new Gson().fromJson(tripDataStr, new TypeToken<ArrayList<TripDataFeed>>(){}.getType());

        mProcessedTripList = new ArrayList<>();

        //logic for what counts as a trip taken
        for(TripDataFeed trip : tripList) {
            if(mVehicle != null && mVehicle.getModel() != null
                    && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                if(trip.getDistance() > MINIMUM_TRIP_DISTANCE && trip.getTripId() != null) {
                    mProcessedTripList.add(trip);
                }
            } else {
                if(trip.getDistance() > MINIMUM_TRIP_DISTANCE && trip.getEnergy() > 0 && trip.getTripId() != null) {
                    mProcessedTripList.add(trip);
                }
            }
        }

        //sort trips by latest date first
        Collections.sort(mProcessedTripList, new Comparator<TripDataFeed>() {
            @Override
            public int compare(TripDataFeed t1, TripDataFeed t2) {
                DateTime dateTime1 = new DateTime(t1.getStartTime());
                DateTime dateTime2 = new DateTime(t2.getStartTime());

                return dateTime2.compareTo(dateTime1);
            }
        });

        //save latest 10 trips in the array list if the tripList's size is more than 10
        if(mProcessedTripList.size() > NO_OF_TRIPS_TO_BE_SHOWN) {
            mProcessedTripList = new ArrayList<>(mProcessedTripList.subList(0, NO_OF_TRIPS_TO_BE_SHOWN));
        }
        Collections.reverse(mProcessedTripList);

        if(mProcessedTripList == null || mProcessedTripList.isEmpty()) {

            if(mVehicle.getModel() != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                mNoTripInfoAvailableLinearLayout.setVisibility(View.GONE);
                mAnalyticsScrollView.setVisibility(View.VISIBLE);
                mDistanceGraphCardView.setVisibility(View.GONE);
                mBatteryUsageGraphCardView.setVisibility(View.GONE);
            } else {
                mNoTripInfoAvailableLinearLayout.setVisibility(View.VISIBLE);
                mAnalyticsScrollView.setVisibility(View.GONE);
            }
        } else {
            mNoTripInfoAvailableLinearLayout.setVisibility(View.GONE);
            mAnalyticsScrollView.setVisibility(View.VISIBLE);
        }

        if(mVehicle != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            refreshBatteryStatusLineGraph();
        }

        if(mProcessedTripList == null || mProcessedTripList.isEmpty()) {
            return;
        }

        refreshDistanceGraph(mProcessedTripList);
        if(mVehicle != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            refreshBatteryUsageGraph(mProcessedTripList);
        } else {
            refreshMileageGraph(mProcessedTripList);
        }
    }

    private void refreshPetrolPriceAndCarbonEmissionSavedViews() {
        if(getActivity() == null) {
            return;
        }

        String batteryADCStr = getActivity().getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getString(HISTORICAL_DATA_LIST_KEY, null);
        if(batteryADCStr == null || batteryADCStr.isEmpty()) {
            NetworkUtils.getInstance(getActivity()).refreshVehicleLiveLogData();
            return;
        }

        ArrayList<DataFeedVehicleLiveLog> vehicleHistoricalLogList = new Gson().fromJson(batteryADCStr, new TypeToken<ArrayList<DataFeedVehicleLiveLog>>(){}.getType());
        if(vehicleHistoricalLogList == null || vehicleHistoricalLogList.isEmpty()) {
            NetworkUtils.getInstance(getActivity()).refreshVehicleLiveLogData();
            return;
        }

        Collections.reverse(vehicleHistoricalLogList);

        for(int index = 0 ; index < vehicleHistoricalLogList.size() ; index++) {
            if(vehicleHistoricalLogList.get(index).getOdometer() != 0) {
                mVehicleOdoMeterValue = vehicleHistoricalLogList.get(index).getOdometer();
                break;
            }
        }

        getAveragePetrolPrice();

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle == null) {

            mFuelAndCarbonEmissionStatsNotFoundLinearLayout.setVisibility(View.VISIBLE);
            mFuelAndCarbonSavedLinearLayout.setVisibility(View.GONE);

            return;
        }

        double odoActual = 0;
        if(mVehicle != null && mVehicle.getModel().getProtocol() != null && mVehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            odoActual = vehicle.getOdoValue();
        } else {
            odoActual = mVehicleOdoMeterValue;
        }

        if(odoActual < 1000) {
            mFuelAndCarbonEmissionStatsNotFoundLinearLayout.setVisibility(View.VISIBLE);
            mFuelAndCarbonSavedLinearLayout.setVisibility(View.GONE);
        } else {

            mFuelAndCarbonEmissionStatsNotFoundLinearLayout.setVisibility(View.GONE);
            mFuelAndCarbonSavedLinearLayout.setVisibility(View.VISIBLE);

            double odoValueInKm = odoActual / 1000;

            //money saved
            float averageFuelPrice = getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).getFloat(AVERAGE_PETROL_PRICE_KEY, 85.0f);
            double averageMileageByFuelBasedScooter = 55;    // *honda activa gives a mileage of 55km/litre

            double fuelConsumptionForDistanceTravelled = odoValueInKm / averageMileageByFuelBasedScooter;

            double fuelMoneySaved = Math.abs(Math.round(Math.floor(fuelConsumptionForDistanceTravelled * averageFuelPrice)));

            mTotalFuelMoneySavedTextView.setText(String.format("%s%s", getString(R.string.rupee), String.valueOf(fuelMoneySaved)));

            double averageCarbonEmissionPerLitre = 2.3;     //kg per litre of petrol
            double carbonEmissionSaved = Math.abs(Math.round(Math.floor(fuelConsumptionForDistanceTravelled * averageCarbonEmissionPerLitre)));

            mTotalCarbonEmissionSavedTextView.setText(String.format("%s %s", String.valueOf(carbonEmissionSaved), getString(R.string.kilograms)));
        }
    }

    private void getAveragePetrolPrice() {
        NewsRainApiInterface newsRainApiInterface = NewsRainApiClient.getClient().create(NewsRainApiInterface.class);

        newsRainApiInterface.getPetrolPrice().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String responseStr = response.body().string();

                            extractPetrolPrice(responseStr);

                        } catch (Exception e) {
                            //nothing to do here
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mFuelAndCarbonEmissionStatsNotFoundLinearLayout.setVisibility(View.VISIBLE);
                        mFuelAndCarbonSavedLinearLayout.setVisibility(View.GONE);

                    }
                });
            }
        });
    }

    private void extractPetrolPrice(String responseStr) {
        if(getActivity() == null) {
            return;
        }

        if(responseStr == null || responseStr.isEmpty()) {
            return;
        }

        String petrolPriceRegex = "Petrol price in India today is";

        if(responseStr.contains(petrolPriceRegex)) {

            String[] splitStr1 = responseStr.split(petrolPriceRegex);
            String petrolPriceString = splitStr1[1];

            float averagePetrolPrice = Float.parseFloat(petrolPriceString.trim().split(" ")[0]);

            getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putFloat(AVERAGE_PETROL_PRICE_KEY, averagePetrolPrice).apply();
        }
    }

    private void refreshBatteryStatusLineGraph() {
        if(getActivity() == null || mBatteryStatusLineChart == null) {
            return;
        }

        String batteryADCStr = getActivity().getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).getString(HISTORICAL_DATA_LIST_KEY, null);
        if(batteryADCStr == null || batteryADCStr.isEmpty()) {
            return;
        }

        ArrayList<DataFeedVehicleLiveLog> batteryADCList = new Gson().fromJson(batteryADCStr, new TypeToken<ArrayList<DataFeedVehicleLiveLog>>(){}.getType());
        if(batteryADCList == null || batteryADCList.isEmpty() || batteryADCList.size() < BATTERY_GRAPH_XAXIS_LABEL_COUNT) {
            return;
        }
        Collections.reverse(batteryADCList);

        Model model = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs().getVehicle().getModel();

        ArrayList<Entry> xyEntry = new ArrayList<>();
        for(int index = 0 ; index < batteryADCList.size() ; index++) {
            long socValue = 0;
            if(model != null && model.getBatteryMaxVoltageLimit() != 0 && model.getBatteryMinVoltageLimit() != 0) {
                double batteryPercentage = ((batteryADCList.get(index).getBatteryVoltageAdc() - model.getBatteryMinVoltageLimit()) / (model.getBatteryMaxVoltageLimit() - model.getBatteryMinVoltageLimit()))*100;
                socValue = Math.round(Math.floor(batteryPercentage));
            }

            xyEntry.add(new Entry(index, (float) socValue));
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        //line data set
        LineDataSet lineDataSet = new LineDataSet(xyEntry, getString(R.string.percent_used_per_trip));
        lineDataSet.setLineWidth(2);
        lineDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        lineDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillColor(ContextCompat.getColor(getActivity(), R.color.revos_green));
        lineDataSet.setDrawHighlightIndicators(false);

        //axis settings
        mBatteryStatusLineChart.getXAxis().setDrawLabels(true);
        mBatteryStatusLineChart.getXAxis().setDrawGridLines(false);
        mBatteryStatusLineChart.getAxisRight().setEnabled(false);
        YAxis y = mBatteryStatusLineChart.getAxisLeft();
        y.setLabelCount(BATTERY_GRAPH_YAXIS_LABEL_COUNT, true);
        y.setTextColor(textColor);
        y.setAxisMinValue(0);
        y.setAxisMaxValue(100);

        mBatteryStatusLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mBatteryStatusLineChart.getXAxis().setLabelRotationAngle(-20f);
        mBatteryStatusLineChart.getXAxis().setTextSize(8f);
        mBatteryStatusLineChart.getXAxis().setLabelCount(BATTERY_GRAPH_XAXIS_LABEL_COUNT, true);
        mBatteryStatusLineChart.getXAxis().setGranularity(1.0f);
        mBatteryStatusLineChart.getXAxis().setTextColor(textColor);

        mBatteryStatusLineChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                DateTime dateTime = new DateTime(batteryADCList.get(Math.round(value)).getTimestamp());
                String timeElapsed = DateTimeUtils.getInstance(getActivity()).generateTimeAgoString(dateTime);

                if(timeElapsed != null && value != 0.0) {
                    return timeElapsed;
                } else {
                    return "";
                }
            }
        });


        //legend settings
        mBatteryStatusLineChart.getLegend().setEnabled(true);
        mBatteryStatusLineChart.getLegend().setTextColor(textColor);
        mBatteryStatusLineChart.setExtraOffsets(0, 0, convertDipToPixels(12), convertDipToPixels(4));

        mBatteryStatusLineChart.animateY(ANIMATION_DURATION);
        mBatteryStatusLineChart.getDescription().setText("");
        mBatteryStatusLineChart.getDescription().setTextSize(12);

        LineData lineData = new LineData(lineDataSet);
        mBatteryStatusLineChart.setData(lineData);
    }

    private void refreshDistanceGraph(ArrayList<TripDataFeed> tripList) {
        if(getActivity() == null || mDistanceBarChart == null || mMaxDistanceTextView == null ||mTotalDistanceTextView == null || mAvgDistanceTextView == null) {
            return;
        }

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        int index = 0;
        double maxDistance = 0;
        double totalDistance = 0;
        for(TripDataFeed trip : tripList) {
            xyEntry.add(new BarEntry(index++, (float) trip.getDistance()/1000));
            if(trip.getDistance() > maxDistance) {
                maxDistance = trip.getDistance()/1000;
            }
            totalDistance += trip.getDistance()/1000;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;


        BarDataSet barDataSet = new BarDataSet(xyEntry, getString(R.string.kilometers));
        barDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mDistanceBarChart.getXAxis().setDrawLabels(true);
        mDistanceBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mDistanceBarChart.getXAxis().setGranularityEnabled(true);
        mDistanceBarChart.getXAxis().setLabelCount(tripList.size());
        mDistanceBarChart.getXAxis().setLabelRotationAngle(-20f);
        mDistanceBarChart.getXAxis().setGranularity(1f);
        mDistanceBarChart.getXAxis().setTextColor(textColor);
        mDistanceBarChart.getXAxis().setTextSize(8f);
        mDistanceBarChart.getXAxis().setDrawGridLines(false);
        mDistanceBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime(tripList.get(Math.round(value)).getStartTime());
                Date tripStartDate = dateTime.toDate();
                String dateFormat = "dd-MMM";

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return getString(R.string.not_applicable);
                }
            }
        });

        mDistanceBarChart.getDescription().setTextColor(textColor);
        mDistanceBarChart.getDescription().setEnabled(false);

        mDistanceBarChart.getLegend().setEnabled(true);
        mDistanceBarChart.getLegend().setTextColor(textColor);

        mDistanceBarChart.getAxisLeft().setTextColor(textColor);
        mDistanceBarChart.getAxisRight().setDrawLabels(false);
        mDistanceBarChart.getAxisRight().setAxisMinimum(0);
        mDistanceBarChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mDistanceBarChart.setData(barData);
        mDistanceBarChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxDistanceTextView.setText(String.format("%s %s", df.format(maxDistance), getString(R.string.kilometers)));
        mTotalDistanceTextView.setText(String.format("%s %s", df.format(totalDistance), getString(R.string.kilometers)));
        mAvgDistanceTextView.setText(String.format("%s %s", df.format(totalDistance / tripList.size()), getString(R.string.kilometers)));
    }

    private void refreshMileageGraph(ArrayList<TripDataFeed> tripList) {
        if(getActivity() == null || mMileageBarChart == null || mMaxMileageTextView == null || mAvgMileageTextView == null) {
            return;
        }

        ArrayList<BarEntry> xyEntry = new ArrayList<>();

        int index = 0;
        double maxMileage = 0;
        double totalMileage = 0;
        for(TripDataFeed trip : tripList) {
            float mileage = (float) ((trip.getDistance()/1000) / trip.getEnergy());
            xyEntry.add(new BarEntry(index++, mileage));
            if(mileage > maxMileage) {
                maxMileage = mileage;
            }
            totalMileage += mileage;
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;

        BarDataSet barDataSet = new BarDataSet(xyEntry, getString(R.string.km_per_kwh));
        barDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mMileageBarChart.getXAxis().setDrawLabels(true);
        mMileageBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mMileageBarChart.getXAxis().setGranularityEnabled(true);
        mMileageBarChart.getXAxis().setLabelCount(tripList.size());
        mMileageBarChart.getXAxis().setLabelRotationAngle(-20f);
        mMileageBarChart.getXAxis().setGranularity(1f);
        mMileageBarChart.getXAxis().setTextColor(textColor);
        mMileageBarChart.getXAxis().setTextSize(8f);
        mMileageBarChart.getXAxis().setDrawGridLines(false);
        mMileageBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                DateTime dateTime = new DateTime(tripList.get(Math.round(value)).getStartTime());
                Date dob = dateTime.toDate();
                String dateFormat = "dd-MMM";

                String tripDate = new SimpleDateFormat(dateFormat).format(dob);

                if(tripDate != null) {
                    return tripDate;
                } else {
                    return getString(R.string.not_applicable);
                }
            }
        });

        mMileageBarChart.getDescription().setTextColor(textColor);
        mMileageBarChart.getDescription().setEnabled(false);

        mMileageBarChart.getLegend().setEnabled(true);
        mMileageBarChart.getLegend().setTextColor(textColor);

        mMileageBarChart.getAxisLeft().setTextColor(textColor);
        mMileageBarChart.getAxisRight().setDrawLabels(false);
        mMileageBarChart.getAxisRight().setAxisMinimum(0);
        mMileageBarChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mMileageBarChart.setData(barData);
        mMileageBarChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        mMaxMileageTextView.setText(String.format("%s %s", df.format(maxMileage), getString(R.string.km_per_kwh)));
        mAvgMileageTextView.setText(String.format("%s %s", df.format(totalMileage / tripList.size()), getString(R.string.km_per_kwh)));
    }


    private void refreshBatteryUsageGraph(ArrayList<TripDataFeed> tripArrayList) {
        if (getActivity() == null || mBatteryUsageBarChart == null || mMaxBatteryUsageTextView == null || mAvgBatteryUsageTextView == null) {
            return;
        }

        ArrayList<BarEntry> xyEntry = new ArrayList<>();
        List<Double> batteryPercentageList = new ArrayList<>();
        int xyEntryIndex = 0;

        for (int index = 0; index < tripArrayList.size(); index++) {
            List<DataFeedBatteryVoltageAdc> batteryVoltageADCList = tripArrayList.get(index).getAdcVoltageArrayList();
            if(batteryVoltageADCList != null && batteryVoltageADCList.size() > 1) {
                double batteryTripEnd = batteryVoltageADCList.get(0).getVoltage();
                double batteryTripStart  = batteryVoltageADCList.get(batteryVoltageADCList.size() - 1).getVoltage();
                Model model = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs().getVehicle().getModel();
                if (model != null && model.getBatteryMaxVoltageLimit() > 0 && model.getBatteryMinVoltageLimit() > 0) {
                    double batteryPercentage = ((batteryTripStart - batteryTripEnd) / (model.getBatteryMaxVoltageLimit() - model.getBatteryMinVoltageLimit())) * 100;
                    xyEntry.add(new BarEntry(xyEntryIndex++, (float) batteryPercentage));
                    batteryPercentageList.add(batteryPercentage);
                }
            }
        }

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
        @ColorInt int textColor = typedValue.data;
        BarDataSet barDataSet = new BarDataSet(xyEntry, getString(R.string.percent_used_per_trip));
        barDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green_dark));
        barDataSet.setValueTextColor(textColor);

        mBatteryUsageBarChart.getXAxis().setDrawLabels(true);
        mBatteryUsageBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mBatteryUsageBarChart.getXAxis().setGranularityEnabled(true);
        mBatteryUsageBarChart.getXAxis().setLabelCount(tripArrayList.size());
        mBatteryUsageBarChart.getXAxis().setLabelRotationAngle(-20f);
        mBatteryUsageBarChart.getXAxis().setGranularity(1f);
        mBatteryUsageBarChart.getXAxis().setTextColor(textColor);
        mBatteryUsageBarChart.getXAxis().setTextSize(8f);
        mBatteryUsageBarChart.getXAxis().setDrawGridLines(false);
        mBatteryUsageBarChart.getXAxis().setValueFormatter(new ValueFormatter() {

            @Override
            public String getFormattedValue(float value) {
                DateTime dateTime = new DateTime(tripArrayList.get(Math.round(value)).getStartTime());
                Date dob = dateTime.toDate();
                String dateFormat = "dd-MMM";
                String tripDate = new SimpleDateFormat(dateFormat).format(dob);

                if (tripDate != null) {
                    return tripDate;
                } else {
                    return getString(R.string.not_applicable);
                }
            }

        });

        mBatteryUsageBarChart.getDescription().setTextColor(textColor);
        mBatteryUsageBarChart.getDescription().setEnabled(false);
        mBatteryUsageBarChart.getLegend().setEnabled(true);
        mBatteryUsageBarChart.getLegend().setTextColor(textColor);
        mBatteryUsageBarChart.getAxisLeft().setTextColor(textColor);
        mBatteryUsageBarChart.getAxisRight().setDrawLabels(false);
        mBatteryUsageBarChart.getAxisRight().setAxisMinimum(0);
        mBatteryUsageBarChart.getAxisLeft().setAxisMinimum(0);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.5f);

        mBatteryUsageBarChart.setData(barData);
        mBatteryUsageBarChart.animateY(ANIMATION_DURATION);

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        if(batteryPercentageList == null || batteryPercentageList.isEmpty()) {
            return;
        }

        mMaxBatteryUsageTextView.setText(String.format("%s %s", df.format(Collections.max(batteryPercentageList)), "%"));
        int totalBatteryUsage = 0;
        for (int index = 0; index < batteryPercentageList.size(); index++) {
            totalBatteryUsage += batteryPercentageList.get(index);
        }

        mAvgBatteryUsageTextView.setText(String.format("%s %s", df.format(totalBatteryUsage / batteryPercentageList.size()), "%"));
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    private void setLogStatusImageViewIcon() {
        if(getActivity() == null || mLogStatusTextView == null || mLogStatusLottieImageView == null || mLogsSyncStatusLinearLayout.getVisibility() != View.VISIBLE) {
            return;
        }
        int offlineLogCount = RevosFileUtils.getInstance(getActivity()).getStoredLogFileCount(LOG_TYPE_OFFLINE);
        int liveLogCount = RevosFileUtils.getInstance(getActivity()).getStoredLogFileCount(LOG_TYPE_LIVE);
        int offlineBatteryAdcLogCount = RevosFileUtils.getInstance(getActivity()).getStoredLogFileCount(LOG_TYPE_OFFLINE_BATTERY_ADC);

        if(offlineLogCount + liveLogCount + offlineBatteryAdcLogCount > 0) {

            String logsLeftStr = String.format(getResources().getString(R.string.syncing_logs_dynamic), String.valueOf(offlineLogCount + liveLogCount + offlineBatteryAdcLogCount));
            mLogStatusTextView.setText(logsLeftStr);

        } else {

            mLogStatusLottieImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_revos_check));
            mLogStatusTextView.setText(getString(R.string.sync_complete));
        }
    }

    private void refreshTripDataIfRequired() {
        if(getActivity() == null || NordicBleService.getConnectionState() == BluetoothProfile.STATE_CONNECTED) {
            return;
        }

        boolean tripDataNeedsRefresh = getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).getBoolean(TRIP_DATA_NEEDS_REFRESH_KEY, false);

        if(tripDataNeedsRefresh) {
            NetworkUtils.getInstance(getActivity()).refreshTripData();
        }
    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(NETWORK_EVENT_TRIP_DATA_REFRESHED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        refreshAnalyticsViews();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshBatteryStatusLineGraph();
                    refreshPetrolPriceAndCarbonEmissionSavedViews();
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mTimer.cancel();
        mTimer.purge();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAnalyticsViews();
    }
}
