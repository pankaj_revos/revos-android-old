package com.revos.android.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.MainActivity;
import com.revos.android.activities.ServiceRequestActivity;
import com.revos.android.activities.VehiclesAndDevicesListActivity;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.scripts.FetchVehicleQuery;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.jetbrains.annotations.NotNull;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.activities.ServiceRequestActivity.SERVICE_REQUEST_ACTIVITY;
import static com.revos.android.activities.VehiclesAndDevicesListActivity.VEHICLE_DETAILS_ACTIVITY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;

public class SetupVehicleFragment extends Fragment {

    private View mRootView;

    private FetchVehicleQuery.Get vehicleDetailsObject;

    private ImageView mSetupVehicleBackButtonImageView;
    private Button mSetupVehicleSubmitButton;

    private EditText mVinEditText, mPhoneNumberEditText;

    private String launchingActivity;

    private ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.setup_vehicle_fragment, container, false);

        launchingActivity = getTag();

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        mSetupVehicleBackButtonImageView = (ImageView) mRootView.findViewById(R.id.setup_vehicle_back_button_image_view);
        mSetupVehicleSubmitButton = (Button) mRootView.findViewById(R.id.setup_vehicle_submit_button);

        mVinEditText = (EditText) mRootView.findViewById(R.id.setup_vehicle_vin_number_edit_text);
        mPhoneNumberEditText = (EditText) mRootView.findViewById(R.id.setup_vehicle_phone_number_edit_text);

        if(launchingActivity == null) {             //Main activity is the launching activity since vehicle setup is done from analytics fragment

            mSetupVehicleBackButtonImageView.setVisibility(View.INVISIBLE);
        }

        mSetupVehicleBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity() == null) {
                    return;
                }

                if(launchingActivity.equals(SERVICE_REQUEST_ACTIVITY)) {
                    getActivity().finish();

                } else if(launchingActivity.equals(VEHICLE_DETAILS_ACTIVITY)) {
                    getActivity().finish();
                }
            }
        });

        mSetupVehicleSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String vin = mVinEditText.getText().toString();
                String phone = mPhoneNumberEditText.getText().toString();

                if(vin.trim().isEmpty() || phone.trim().isEmpty()) {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vin_or_phone_number_cannot_be_empty));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    fetchVehicleObject();
                }
            }
        });
    }

    private void fetchVehicleObject() {
        if(getActivity() == null){
            return;
        }

        String vinEntered = mVinEditText.getText().toString();

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            hideProgressDialog();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog();

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder()
                                                            .vin(vinEntered)
                                                            .build();

        FetchVehicleQuery fetchVehicleQuery = FetchVehicleQuery.builder()
                                                .vehicleWhereInputVar(vehicleWhereUniqueInput)
                                                .build();

        apolloClient.query(fetchVehicleQuery).enqueue(new ApolloCall.Callback<FetchVehicleQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchVehicleQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.incorrect_vin));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else {

                            if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().get() != null) {

                                vehicleDetailsObject = response.getData().vehicles().get();
                                verifyVinAndMobileNumber(vehicleDetailsObject);

                            } else {

                                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_vehicle_found));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }

        });
    }


    private void verifyVinAndMobileNumber(FetchVehicleQuery.Get fetchedVehicle){
        if(getActivity() == null) {
            return;
        }

        String vin = mVinEditText.getText().toString();
        String phoneNo = mPhoneNumberEditText.getText().toString();
        String sanitizedPhoneNo = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(phoneNo);

        String fetchedBuyersNo = "";
        String sanitizedFetchedBuyersNo = "";
        if(fetchedVehicle != null && fetchedVehicle.buyer() != null && fetchedVehicle.buyer().phone() != null) {
            fetchedBuyersNo = fetchedVehicle.buyer().phone();
            sanitizedFetchedBuyersNo = PhoneUtils.getInstance(getActivity()).sanitizePhoneNumber(fetchedBuyersNo);
        }

        if(!(vin.isEmpty() && phoneNo.isEmpty())) {

            if (fetchedVehicle != null) {
                if (vin.equals(fetchedVehicle.vin()) && sanitizedPhoneNo.equals(sanitizedFetchedBuyersNo)) {
                    //clear prefs
                    clearPrefs();
                    //everything seems okay, store these vehicle details
                    setVehiclePrefs(fetchedVehicle);
                    //refresh fence settings
                    NetworkUtils.getInstance(getActivity()).refreshGeoFenceData();
                    //make a network call to refresh trip data
                    NetworkUtils.getInstance(getActivity()).refreshTripData();
                    //make a network call to download vehicle model image
                    NetworkUtils.getInstance(getActivity()).downloadVehicleModelImageIfRequired(fetchedVehicle.model().id());
                    //load the next fragment
                    decideNextStep();
                } else {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_vin_or_phone_number));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                }
            }
        } else {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.fields_cannot_be_empty));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_WARNING);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }

    private void clearPrefs() {
        if(getActivity() == null) {
            return;
        }
        getActivity().getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        getActivity().getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();

        getActivity().getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

    }

    private void setVehiclePrefs(FetchVehicleQuery.Get vehicleQueryObject){

        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).saveVehicleVariablesInPrefs(vehicleQueryObject);
        if(vehicle == null) {
            return;
        }

        //check if there's any device associated with this vehicle
        Device device = PrefUtils.getInstance(getActivity())
                                 .saveDeviceVariablesInPrefs(vehicleQueryObject);

        if(device != null) {
            //save device pin
            if(vehicleQueryObject.device().pin() != null) {
                getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, vehicleQueryObject.device().pin()).apply();
            } else {
                getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, null).apply();
            }
        }

        hideProgressDialog();
    }

    private void decideNextStep() {
        if(getActivity() == null) {
            return;
        }

        MainActivity.hideKeyboard(getActivity());

        if(launchingActivity == null) {         //Main activity is the launching activity since vehicle setup is done from analytics fragment
            ((MainActivity)getActivity()).mShowSetupVehicleFragment = false;
            //refresh main menu drawer layout
            ((MainActivity)getActivity()).setupDrawerLayout();
            ((MainActivity)getActivity()).refreshFragments();

        } else if(launchingActivity.equals(SERVICE_REQUEST_ACTIVITY)) {
            ((ServiceRequestActivity)getActivity()).loadAppropriateServiceRequestFragment();

        } else if(launchingActivity.equals(VEHICLE_DETAILS_ACTIVITY)) {
            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.updating_vehicle_prefs_message));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_INFO);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            ((VehiclesAndDevicesListActivity)getActivity()).loadAppropriateVehicleFragment();

        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.setting_up_vehicle_details));
        if(getActivity() != null && !getActivity().isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}