package com.revos.android.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.revos.android.R;
import com.revos.android.activities.ActiveBookingActivity;
import com.revos.android.activities.BookRentalVehicleActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.RentalActivity;
import com.revos.android.activities.RentalKycActivity;
import com.revos.android.constants.Constants;
import com.revos.android.jsonStructures.RentalPricingInfo;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.RentalVehicleCompany;
import com.revos.android.jsonStructures.RentalVehicleModel;
import com.revos.android.jsonStructures.RentalVehicleModelConfig;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.utilities.adapters.RentalVehiclesListAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.scripts.FetchKYCAssociationForUserQuery;
import com.revos.scripts.FetchRentalVehicleDetailsAndPricingInfoQuery;
import com.revos.scripts.type.CompanyWhereUniqueInput;
import com.revos.scripts.type.InputGetRentalVehicles;
import com.revos.scripts.type.KYCStatus;
import com.revos.scripts.type.RentalStatus;
import com.revos.scripts.type.VehicleWhereInput;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static com.revos.android.activities.BookRentalVehicleActivity.BOOK_RENTAL_VEHICLE_OBJECT_KEY;
import static com.revos.android.activities.RentalKycActivity.RENTAL_KYC_VEHICLE_OBJECT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_TOKEN_KEY;

public class RentalMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener{

    private View mRootView;

    private MapView mMapView;
    private GoogleMap mGoogleMap = null;

    private boolean mLocationZoomHappenedOnce = false;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private int REQUEST_LOCATION_PERMISSION_REQ_CODE = 200;

    private Geocoder mGeoCoder;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private LatLngBounds mCachedLatLngBounds;

    private Bitmap mGreenMarkerBitmap, mRedMarkerBitmap;

    private TextView mRentalVehicleModelNameTextView, mRentalVehicleVinTextView, mRentalVehicleStatusTextView,
                     mRentalVehicleBatteryStatusTextView, mRentalVehicleProviderNameTextView, mRentalVehicleProviderPhoneTextView;

    private Button mRentalVehicleBookOrNavigateButton;

    private ImageButton mRentalMapListImageButton;

    private LinearLayout mRentalVehicleIsDeliverableLinearLayout;

    private ImageView mCloseRentalVehicleInfoCardImageView, mRentalVehicleProviderImageView;

    private CardView mRentalVehicleInfoCardView;

    private boolean mIsRentalVehicleInfoVisible = false;

    private HashMap<String, RentalVehicle> mRentalVehicleHashMap;

    private RecyclerView mRentalVehiclesListRecyclerView;

    private boolean mIsVehiclesListViewSelected = false;

    private final int BOOKING_ACTIVITY_REQUEST_FROM_MAP_INFO_CARD_CODE = 1002;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.rental_map_fragment, container, false);

        setupMarkerBitmap();

        setupVehicleInfoCardViews();

        setupMapViews(savedInstanceState);

        setupMapListFloatingActionButton();

        return mRootView;
    }

    private void setupMapListFloatingActionButton() {

        if(getActivity() == null) {
            return;
        }

        mRentalMapListImageButton = mRootView.findViewById(R.id.rental_map_list_image_button);

        mRentalMapListImageButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_list_view));

        //initialising recycler view for list here
        mRentalVehiclesListRecyclerView = mRootView.findViewById(R.id.rental_map_vehicles_list_recycler_view);

        mRentalMapListImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(getActivity() == null) {
                    return;
                }

                if(!mIsVehiclesListViewSelected) {

                    if(mRentalVehicleHashMap == null || mRentalVehicleHashMap.isEmpty()) {

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_rental_vehicles_available));
                        bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                        return;
                    }

                    List<RentalVehicle> rentalVehiclesList = new ArrayList<>();

                    for (Map.Entry<String, RentalVehicle> entry : mRentalVehicleHashMap.entrySet()) {
                        RentalVehicle rentalVehicle = entry.getValue();

                        //add to list if vehicle is deliverable
                        if(rentalVehicle.isDeliverable()) {
                            rentalVehiclesList.add(rentalVehicle);
                        }
                    }

                    if(rentalVehiclesList == null || rentalVehiclesList.isEmpty()) {

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_deliverable_rental_vehicles_available));
                        bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                        return;
                    }

                    mRentalMapListImageButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_location_on_map));

                    mMapView.setVisibility(View.GONE);
                    if(mIsRentalVehicleInfoVisible) {
                        mRentalVehicleInfoCardView.setVisibility(View.GONE);
                    }

                    mRentalVehiclesListRecyclerView.setVisibility(View.VISIBLE);
                    mIsVehiclesListViewSelected = true;

                    RentalVehiclesListAdapter mRentalVehicleListAdapter = new RentalVehiclesListAdapter(rentalVehiclesList, getActivity());
                    mRentalVehiclesListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    mRentalVehiclesListRecyclerView.setAdapter(mRentalVehicleListAdapter);

                } else {
                    mIsVehiclesListViewSelected = false;
                    mRentalMapListImageButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_list_view));

                    mMapView.setVisibility(View.VISIBLE);
                    if(mIsRentalVehicleInfoVisible) {
                        mRentalVehicleInfoCardView.setVisibility(View.VISIBLE);
                    }
                    mRentalVehiclesListRecyclerView.setVisibility(View.GONE);

                }
            }
        });
    }

    private void setupMarkerBitmap() {
        mGreenMarkerBitmap = resizeMapIcons(R.drawable.ic_bike_unselected, convertDipToPixels(48), convertDipToPixels(48));
        mRedMarkerBitmap = resizeMapIcons(R.drawable.ic_charging_png_red, convertDipToPixels(32), convertDipToPixels(32));
    }

    private void setupVehicleInfoCardViews() {

        mRentalVehicleModelNameTextView = mRootView.findViewById(R.id.rental_map_vehicle_model_name_text_view);
        mRentalVehicleVinTextView = mRootView.findViewById(R.id.rental_map_vehicle_vin_text_view);
        mRentalVehicleStatusTextView = mRootView.findViewById(R.id.rental_map_vehicle_status_text_view);
        mRentalVehicleBatteryStatusTextView = mRootView.findViewById(R.id.rental_map_vehicle_battery_status_text_view);
        mRentalVehicleProviderNameTextView = mRootView.findViewById(R.id.rental_map_provider_text_view);
        mRentalVehicleProviderPhoneTextView = mRootView.findViewById(R.id.rental_map_provider_phone_text_view);

        mRentalVehicleInfoCardView = mRootView.findViewById(R.id.rental_map_vehicle_card_view);
        mRentalVehicleBookOrNavigateButton = mRootView.findViewById(R.id.rental_map_navigate_button);
        mCloseRentalVehicleInfoCardImageView = mRootView.findViewById(R.id.rental_map_close_image_view);
        mRentalVehicleProviderImageView = mRootView.findViewById(R.id.rental_map_vehicle_provider_image_view);

        mRentalVehicleIsDeliverableLinearLayout = mRootView.findViewById(R.id.rental_vehicle_is_deliverable_linear_layout);

        mCloseRentalVehicleInfoCardImageView.setOnClickListener(v -> {
            mRentalVehicleInfoCardView.setVisibility(View.INVISIBLE);
            mIsRentalVehicleInfoVisible = false;

            if(mCachedLatLngBounds != null) {
                zoomMapToBounds(mCachedLatLngBounds);
            }
        });
    }

    private void setupMapViews(Bundle savedInstanceState) {
        mMapView = mRootView.findViewById(R.id.long_term_rental_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        mGoogleMap.clear();
        mGoogleMap.setOnMarkerClickListener(this);

        if(getActivity() == null) {
            return;
        }

        setupGoogleMapsAndFetchCurrentLocationDetails();
    }

    @Override
    public void onCameraIdle() {
        Timber.d("Camera Idle");
        if(mGoogleMap == null) {
            return;
        }

        LatLngBounds cameraLatLngBounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

        if(mCachedLatLngBounds == null) {
            mCachedLatLngBounds = cameraLatLngBounds;
            fetchRentalVehicles();
            return;
        }

        if(mCachedLatLngBounds.contains(cameraLatLngBounds.northeast) && mCachedLatLngBounds.contains(cameraLatLngBounds.southwest)) {
            Timber.d("inside");
        } else {
            Timber.d("outside");
            mCachedLatLngBounds = cameraLatLngBounds;
            //fetch more vehicles
            fetchRentalVehicles();
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        RentalVehicle rentalVehicle = (RentalVehicle) marker.getTag();

        if(rentalVehicle != null) {

            //reset battery voltage and soc value
            mRentalVehicleBatteryStatusTextView.setText(getString(R.string.battery)
                    .concat(" : ").concat(("--")
                            .concat("\n").concat(getString(R.string.soc))
                            .concat(" : ").concat("--")));

            if(rentalVehicle.getModel() != null) {

                if(rentalVehicle.getModel().getName() != null
                        && !rentalVehicle.getModel().getName().isEmpty()) {
                    mRentalVehicleModelNameTextView.setText(rentalVehicle.getModel().getName());
                } else {
                    mRentalVehicleModelNameTextView.setText(getString(R.string.not_applicable));
                }

                //fetchVehicleLiveLogForBatteryDetails(rentalVehicle.getVin(), rentalVehicle);

                if(rentalVehicle.getModel().getConfig() != null) {
                    String batteryStatus = getBatteryVoltageDetails(rentalVehicle);

                    if(batteryStatus != null) {
                        mRentalVehicleBatteryStatusTextView.setText(batteryStatus);
                    }
                }
            } else {
                mRentalVehicleModelNameTextView.setText(getString(R.string.not_applicable));
            }

            if(rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {
                mRentalVehicleVinTextView.setText(rentalVehicle.getVin());
            } else {
                mRentalVehicleVinTextView.setText(getString(R.string.vin_not_found_try_again));
            }

            if(rentalVehicle.getRentalStatus() != null) {
                mRentalVehicleStatusTextView.setText(rentalVehicle.getRentalStatus());
            } else {
                mRentalVehicleStatusTextView.setText(getString(R.string.not_available));
            }

            if(rentalVehicle.getCompany() != null) {
                if (rentalVehicle.getCompany().getName() != null
                        && !rentalVehicle.getCompany().getName().isEmpty()) {
                    mRentalVehicleProviderNameTextView.setText(rentalVehicle.getCompany().getName());
                } else {
                    mRentalVehicleProviderNameTextView.setText(getString(R.string.not_available));
                }

                if(rentalVehicle.getCompany().getPhone() != null && !rentalVehicle.getCompany().getPhone().isEmpty() ) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity())
                            .sanitizePhoneNumber(rentalVehicle.getCompany().getPhone().trim());
                    mRentalVehicleProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    mRentalVehicleProviderPhoneTextView.setText(getString(R.string.not_available));
                }
            } else if(rentalVehicle.getOem() != null) {
                if (rentalVehicle.getOem().getName() != null
                        && !rentalVehicle.getOem().getName().isEmpty()) {
                    mRentalVehicleProviderNameTextView.setText(rentalVehicle.getOem().getName());
                } else {
                    mRentalVehicleProviderNameTextView.setText(getString(R.string.not_available));
                }

                if(rentalVehicle.getOem().getPhone() != null && !rentalVehicle.getOem().getPhone().isEmpty() ) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity())
                            .sanitizePhoneNumber(rentalVehicle.getOem().getPhone().trim());
                    mRentalVehicleProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    mRentalVehicleProviderPhoneTextView.setText(getString(R.string.not_available));
                }
            } else if(rentalVehicle.getDistributor() != null) {
                if (rentalVehicle.getDistributor().getName() != null
                        && !rentalVehicle.getDistributor().getName().isEmpty()) {
                    mRentalVehicleProviderNameTextView.setText(rentalVehicle.getDistributor().getName());
                } else {
                    mRentalVehicleProviderNameTextView.setText(getString(R.string.not_available));
                }

                if(rentalVehicle.getDistributor().getPhone() != null && !rentalVehicle.getDistributor().getPhone().isEmpty() ) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(getActivity())
                            .sanitizePhoneNumber(rentalVehicle.getDistributor().getPhone().trim());
                    mRentalVehicleProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    mRentalVehicleProviderPhoneTextView.setText(getString(R.string.not_available));
                }
            } else {
                mRentalVehicleProviderNameTextView.setText(getString(R.string.not_available));
                mRentalVehicleProviderPhoneTextView.setText(getString(R.string.not_available));

            }

            if(rentalVehicle.isDeliverable()) {
                mRentalVehicleBookOrNavigateButton.setText(getString(R.string.rental_vehicle_book));
                mRentalVehicleIsDeliverableLinearLayout.setVisibility(View.VISIBLE);
            } else {
                mRentalVehicleBookOrNavigateButton.setText(getString(R.string.navigate));
                mRentalVehicleIsDeliverableLinearLayout.setVisibility(View.GONE);
            }
        }

        if(mRentalVehicleBookOrNavigateButton.getText().equals(getString(R.string.navigate))) {
            mRentalVehicleBookOrNavigateButton.setOnClickListener(v -> {
                String url = "https://www.google.com/maps/dir/?api=1&destination=" + String.valueOf(marker.getPosition().latitude) + "," + String.valueOf(marker.getPosition().longitude);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,  Uri.parse(url));
                startActivity(intent);
            });
        } else if(mRentalVehicleBookOrNavigateButton.getText().equals(getString(R.string.rental_vehicle_book))) {
            mRentalVehicleBookOrNavigateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(rentalVehicle != null && rentalVehicle.getVin() != null) {
                        fetchVehicleDetailsAndPricingInfo(rentalVehicle);
                    }
                }
            });
        }

        //make the card visible
        mRentalVehicleInfoCardView.setVisibility(View.VISIBLE);
        mIsRentalVehicleInfoVisible = true;

        if(mGoogleMap != null) {
            //zoom to the marker
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(marker.getPosition())
                    .zoom(18)
                    .build();

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        return false;
    }

    private void setupGoogleMapsAndFetchCurrentLocationDetails() {
        if(getActivity() == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }

        setupGoogleMaps();

        setupGeoCoderAndLocationClient();

        fetchCurrentLocationLocalityDetails();
    }

    private void setupGoogleMaps() {

        if (getActivity() == null) {
            return;
        }

        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
                return;
            }

            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);

            mGoogleMap.setOnCameraIdleListener(RentalMapFragment.this::onCameraIdle);
        }
    }

    private void fetchRentalVehicles() {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        double latitudeBottom, latitudeTop, longitudeLeft, longitudeRight;

        LatLng northEast = mCachedLatLngBounds.northeast;
        LatLng southWest = mCachedLatLngBounds.southwest;

        latitudeTop = northEast.latitude;
        longitudeRight = northEast.longitude;
        latitudeBottom = southWest.latitude;
        longitudeLeft = southWest.longitude;


        JsonObject locationBoundJsonObject = new JsonObject();

        locationBoundJsonObject.addProperty("latitudeBottom", latitudeBottom);
        locationBoundJsonObject.addProperty("latitudeTop", latitudeTop);
        locationBoundJsonObject.addProperty("longitudeLeft", longitudeLeft);
        locationBoundJsonObject.addProperty("longitudeRight", longitudeRight);

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.add("locationBounds", locationBoundJsonObject);

        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        //if charging is in progress then show bolt control activity
        String bearerToken = "Bearer " + userToken;

        String packageName = getActivity().getPackageName();

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getAllRentalVehicles(packageName, bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() != 200 || response.body() == null) {
                    showServerError();
                    return;
                }

                try {

                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    JsonArray jsonArray = jsonObject.getAsJsonArray("data");

                    if(jsonArray != null && jsonArray.size() > 0) {

                        String dataStr = jsonArray.toString();

                        ArrayList<RentalVehicle> rentalVehicleArrayList = new Gson().fromJson(dataStr, new TypeToken<ArrayList<RentalVehicle>>(){}.getType());

                        if(mRentalVehicleHashMap == null) {
                            mRentalVehicleHashMap = new HashMap<>();
                        }

                        for(RentalVehicle rentalVehicle : rentalVehicleArrayList) {
                            if(rentalVehicle.getStatus().equals("RENTAL")) {
                                mRentalVehicleHashMap.put(rentalVehicle.getVin(), rentalVehicle);
                            }
                        }

                        updateMarkers();
                    } else {
                        hideProgressDialog();
                    }

                } catch (Exception e) {
                    hideProgressDialog();
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                hideProgressDialog();
                showServerError();
            }
        });
    }

    /*private void fetchVehicleLiveLogForBatteryDetails(String vin, RentalVehicle rentalVehicle) {
        if(getActivity() == null || rentalVehicle == null || rentalVehicle.getVin() == null || rentalVehicle.getVin().isEmpty()) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(com.boltCore.android.constants.Constants.USER_PREFS, MODE_PRIVATE).getString(com.boltCore.android.constants.Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getVehicleLocationSnapshot(rentalVehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }

                    String dataStr = jsonObject.get("data").toString();

                    DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                    if(vehicleSnapshot != null && vehicleSnapshot.getBattery() != null) {

                        if(getActivity() == null) {
                            return;
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getBatteryVoltageDetails(vehicleSnapshot.getBattery().getBatteryVoltageAdc(), rentalVehicle);
                            }
                        });
                    }

                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {}
        });
    }*/

    private String getBatteryVoltageDetails(RentalVehicle rentalVehicle) {

        if(rentalVehicle == null) {
            return null;
        }

        double batteryADCVoltage = rentalVehicle.getBatteryVoltageAdc();
        double maxVoltage = rentalVehicle.getModel().getConfig().getBatteryMaxVoltage();
        double minVoltage = rentalVehicle.getModel().getConfig().getBatteryMinVoltage();
        long socValue = 0;
        double batteryPercentage = 0;

        if (batteryADCVoltage != 0 && maxVoltage != 0 && minVoltage != 0) {
             batteryPercentage = ((batteryADCVoltage - minVoltage) / (maxVoltage - minVoltage)) * 100;
             socValue = Math.round(Math.floor(batteryPercentage));

             if(socValue <= 0) {
                 socValue = 0;
             } else if(socValue > 100) {
                 socValue = 100;
             }
        }

        if(batteryADCVoltage <= 0) {
            batteryADCVoltage = 0;
        } else if(batteryADCVoltage > 100) {
            batteryADCVoltage = 100;
        }

        String batteryVoltageString = String.valueOf(Math.round(Math.floor(batteryADCVoltage))).concat(" V");
        String batterySocString = String.valueOf(socValue).concat("%");

        return getString(R.string.battery).concat(" : ").concat(batteryVoltageString).concat("\n")
                .concat(getString(R.string.soc)).concat(" : ").concat(batterySocString);
    }

    private void updateMarkers() {
        hideProgressDialog();

        if(mGoogleMap == null || mRentalVehicleHashMap == null || mRentalVehicleHashMap.isEmpty() || getActivity() == null) {
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGoogleMap.clear();

                for (Map.Entry<String, RentalVehicle> entry : mRentalVehicleHashMap.entrySet()) {
                    RentalVehicle rentalVehicle = entry.getValue();

                    Bitmap bitmap = mGreenMarkerBitmap;

                    if(!rentalVehicle.getStatus().equals("RENTAL")) {
                        bitmap = mRedMarkerBitmap;
                    }

                    if(rentalVehicle.getLatitude() != 0 && rentalVehicle.getLongitude() != 0) {

                        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(rentalVehicle.getLatitude(), rentalVehicle.getLongitude()))
                                .anchor(0.5f, 1)
                                .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));

                        marker.setTag(rentalVehicle);
                    }
                }
            }
        });
    }

    private void fetchVehicleDetailsAndPricingInfo(RentalVehicle rentalVehicle) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            showGenericMessage(false, getString(R.string.unable_to_fetch_vehicle_details), GenericMessageActivity.STATUS_ERROR);

            return;
        }

        showProgressDialog();

        InputGetRentalVehicles inputGetRentalVehicles = InputGetRentalVehicles
                .builder()
                .package_(getActivity().getPackageName())
                .vehicle(VehicleWhereInput.builder().vin(rentalVehicle.getVin()).build())
                .build();

        FetchRentalVehicleDetailsAndPricingInfoQuery fetchRentalVehicleDetailsAndPricingInfoQuery = FetchRentalVehicleDetailsAndPricingInfoQuery
                .builder()
                .inputGetRentalVehicles(inputGetRentalVehicles)
                .build();


        apolloClient.query(fetchRentalVehicleDetailsAndPricingInfoQuery).enqueue(new ApolloCall.Callback<FetchRentalVehicleDetailsAndPricingInfoQuery.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<FetchRentalVehicleDetailsAndPricingInfoQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            showGenericMessage(false, getString(R.string.unable_to_fetch_vehicle_details), GenericMessageActivity.STATUS_ERROR);

                        } else if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getRentalVehicles() !=null ) {

                            FetchRentalVehicleDetailsAndPricingInfoQuery.GetRentalVehicle fetchedRentalVehicleDetails;

                            if(response.getData().vehicles().getRentalVehicles() != null && !response.getData().vehicles().getRentalVehicles().isEmpty()
                                    && response.getData().vehicles().getRentalVehicles().get(0) != null) {

                                fetchedRentalVehicleDetails = response.getData().vehicles().getRentalVehicles().get(0);

                                if(fetchedRentalVehicleDetails != null && fetchedRentalVehicleDetails.rentalStatus() == RentalStatus.AVAILABLE) {

                                    if(fetchedRentalVehicleDetails.txInfo() != null && fetchedRentalVehicleDetails.txInfo().pricing() != null) {

                                        List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing> pricingList = fetchedRentalVehicleDetails.txInfo().pricing();

                                        if(pricingList != null && !pricingList.isEmpty()) {
                                            for(int index = 0 ; index < pricingList.size() ; index++) {

                                                RentalPricingInfo rentalPricingInfo = new RentalPricingInfo();

                                                if(pricingList.get(index).name() != null && !pricingList.get(index).name().isEmpty()) {
                                                    rentalPricingInfo.setPricingName(pricingList.get(index).name());
                                                }
                                                if(pricingList.get(index).type() != null) {
                                                    rentalPricingInfo.setPaymentType(pricingList.get(index).type());
                                                }
                                                if(pricingList.get(index).baseAmount() != null) {
                                                    rentalPricingInfo.setBaseAmount(pricingList.get(index).baseAmount().floatValue());
                                                }
                                                if(pricingList.get(index).costPerUnit() != null) {
                                                    rentalPricingInfo.setCostPerUnit(pricingList.get(index).costPerUnit().floatValue());
                                                }
                                                if(pricingList.get(index).unit() != null) {
                                                    rentalPricingInfo.setUnitType(pricingList.get(index).unit());
                                                }
                                                if(pricingList.get(index).paymentMeta() != null && pricingList.get(index).paymentMeta().address() != null
                                                        && !pricingList.get(index).paymentMeta().address().isEmpty()) {
                                                    rentalPricingInfo.setProviderUpiAddress(pricingList.get(0).paymentMeta().address());
                                                }
                                            }
                                        }
                                        rentalVehicle.setRentalPricingInfoList(pricingList);
                                    }

                                    checkKycForProvider(rentalVehicle);

                                } else {
                                    showVehicleNotAvailable();
                                }
                            } else {
                                unableToFetchVehicleDetails();
                            }
                        } else {
                            unableToFetchVehicleDetails();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();

                            showGenericMessage(false, getString(R.string.server_error), GenericMessageActivity.STATUS_ERROR);
                        }
                    });
                }
            }
        });
    }

    private String getRentalProviderId(RentalVehicle rentalVehicle) {
        if(rentalVehicle != null) {

            String rentalProviderId = null;

            if (rentalVehicle.getCompany() != null && rentalVehicle.getCompany().getId() != null
                    && !rentalVehicle.getCompany().getId().isEmpty()) {
                rentalProviderId = rentalVehicle.getCompany().getId();
            } else if (rentalVehicle.getOem() != null && rentalVehicle.getOem().getId() != null
                    && !rentalVehicle.getOem().getId().isEmpty()) {
                rentalProviderId = rentalVehicle.getOem().getId();
            } else if (rentalVehicle.getDistributor() != null && rentalVehicle.getDistributor().getId() != null
                    && !rentalVehicle.getDistributor().getId().isEmpty()) {
                rentalProviderId = rentalVehicle.getDistributor().getId();
            }

            return rentalProviderId;
        } else {
            return null;
        }
    }

    private void checkKycForProvider(RentalVehicle rentalVehicle) {
        //if Provider's id is available, check KYC association
        String providerId = getRentalProviderId(rentalVehicle);

        if(providerId != null && !providerId.isEmpty()) {

            checkKycAssociationForSelectedCompany(rentalVehicle);

        } else {
            hideProgressDialog();

            launchKycActivity(rentalVehicle);
        }
    }

    private void launchKycActivity(RentalVehicle rentalVehicle) {
        if(getActivity() == null) {
            return;
        }

        //start KYC activity
        Intent myIntent = new Intent(getActivity(), RentalKycActivity.class);
        myIntent.putExtra(RENTAL_KYC_VEHICLE_OBJECT_KEY, new Gson().toJson(rentalVehicle));
        startActivity(myIntent);
    }

    private void checkKycAssociationForSelectedCompany(RentalVehicle rentalVehicle) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            hideProgressDialog();

            showGenericMessage(false, getString(R.string.error), GenericMessageActivity.STATUS_ERROR);
            return;
        }

        CompanyWhereUniqueInput companyWhereUniqueInput = CompanyWhereUniqueInput
                .builder()
                .id(rentalVehicle.getCompany().getId())
                .build();

        FetchKYCAssociationForUserQuery getKycAssociationForRentalQuery = FetchKYCAssociationForUserQuery
                .builder()
                .companyWhereUniqueInput(companyWhereUniqueInput)
                .build();


        apolloClient.query(getKycAssociationForRentalQuery).enqueue(new ApolloCall.Callback<FetchKYCAssociationForUserQuery.Data>() {
            @Override
            public void onResponse(com.apollographql.apollo.api.@NotNull Response<FetchKYCAssociationForUserQuery.Data> response) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getData() != null && response.getData().account() != null && response.getData().account().getKYC() != null) {
                            if(response.getData().account().getKYC().status().equals(KYCStatus.APPROVED)) {

                                launchBookVehicleActivity(rentalVehicle);
                            } else {
                                launchKycActivity(rentalVehicle);
                            }
                        } else {
                            launchKycActivity(rentalVehicle);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        launchKycActivity(rentalVehicle);
                    }
                });
            }
        });
    }

    private void launchBookVehicleActivity(RentalVehicle rentalVehicle) {
        if(getActivity() == null) {
            return;
        }

        Intent intent = new Intent(getActivity(), BookRentalVehicleActivity.class);
        intent.putExtra(BOOK_RENTAL_VEHICLE_OBJECT_KEY, new Gson().toJson(rentalVehicle));
        startActivityForResult(intent, BOOKING_ACTIVITY_REQUEST_FROM_MAP_INFO_CARD_CODE);
    }

    private void setupGeoCoderAndLocationClient() {
        if(getActivity() == null) {
            return;
        }

        mGeoCoder = new Geocoder(getActivity());
        mFusedLocationProviderClient = getFusedLocationProviderClient(getActivity());
    }

    private void fetchCurrentLocationLocalityDetails() {

        if (getActivity() == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), mPermissionsRequired, REQUEST_LOCATION_PERMISSION_REQ_CODE);
            return;
        }

        showProgressDialog();

        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
            if(location == null || mLocationZoomHappenedOnce) {
                return;
            }

            //center map around current location
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(12)
                    .build();

            mGoogleMap.setOnCameraIdleListener(RentalMapFragment.this);

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            mLocationZoomHappenedOnce = true;

        });
    }

    private void zoomMapToBounds(LatLngBounds latLngBounds) {
        if(getActivity() == null || mGoogleMap == null) {
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));

                //zoom by one level
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .zoom(mGoogleMap.getCameraPosition().zoom + 1)
                        .target(mGoogleMap.getCameraPosition().target)
                        .build()));


            }
        });
    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    private void showProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((RentalActivity)getActivity()).showProgressDialog();
    }

    private void hideProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((RentalActivity)getActivity()).hideProgressDialog();
    }

    private void showServerError() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                    bundle.putInt(Constants.GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(Constants.GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }
    }

    private void unableToFetchVehicleDetails() {
        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vehicle_details));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showVehicleNotAvailable() {
        hideProgressDialog();

        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_not_available_for_renting));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showGenericMessage(boolean makeMsgPersistent, String message, int messageType) {
        if(getActivity() == null) {
            return;
        }

        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, messageType);

        if(makeMsgPersistent) {
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));

        } else {
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        }

        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        mGreenMarkerBitmap.recycle();
        mRedMarkerBitmap.recycle();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == BOOKING_ACTIVITY_REQUEST_FROM_MAP_INFO_CARD_CODE) {
            if(resultCode == RESULT_OK) {
                if(getActivity() != null) {
                    Intent intent = new Intent(getActivity(), ActiveBookingActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        }
    }
}