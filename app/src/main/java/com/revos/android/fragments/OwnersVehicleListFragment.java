package com.revos.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.PhotoViewActivity;
import com.revos.android.jsonStructures.Model;
import com.revos.android.jsonStructures.User;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.adapters.OwnersVehicleListAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.FetchModelImageQuery;
import com.revos.scripts.FetchOwnersVehiclesQuery;
import com.revos.scripts.type.ModelWhereUniqueInput;
import com.revos.scripts.type.UserWhereInput;
import com.revos.scripts.type.VehicleWhereInput;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GENERIC_TEMP_IMAGE_KEY;
import static com.revos.android.constants.Constants.OWNERS_VEHICLES_LIST;
import static com.revos.android.constants.Constants.TEMP_IMAGE_PREFS;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_MODEL_IMAGE_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;
import static com.revos.android.fragments.VehicleDetailsFragment.CURRENT_VEHICLE_MODEL_IMAGE_URL;
import static com.revos.android.fragments.VehicleDetailsFragment.VEHICLE_DETAILS_OBJECT;
import static com.revos.android.fragments.VehicleDetailsFragment.VEHICLE_PIN_KEY;

public class OwnersVehicleListFragment extends Fragment {

    private View mRootView;

    private CardView mCurrentVehicleCardView;
    private CircleImageView mCurrentVehicleCircleImageView;
    private TextView mCurrentVehicleHeadingTextView, mCurrentVehicleModelNameTextView, mCurrentVehicleVinTextView,
            mRefreshingViewsTextView;
    private RecyclerView mOwnersVehiclesListRecyclerView;

    private ProgressBar mRefreshingViewsProgressBar;

    private OwnersVehicleListAdapter mOwnersVehicleListAdapter;

    private HashMap<String, Pair<Vehicle, String>> mVehiclesListHashMap;
    private HashMap<String, Boolean> mIsModelImageDownloadedHashMap;
    private HashMap<String, String> mModelImagesHashMap;

    private String mModelId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.owners_vehicle_list_fragment, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        ImageView backButtonImageView = mRootView.findViewById(R.id.owners_vehicle_list_back_button_image_view);

        mCurrentVehicleHeadingTextView = mRootView.findViewById(R.id.owners_vehicle_list_current_vehicle_text_view);
        mCurrentVehicleCardView = mRootView.findViewById(R.id.owners_vehicle_list_current_vehicle_card_view);
        mCurrentVehicleCircleImageView = mRootView.findViewById(R.id.owners_vehicle_list_vehicle_circle_image_view);
        mCurrentVehicleModelNameTextView = mRootView.findViewById(R.id.owners_vehicles_list_vehicle_model_text_view);
        mCurrentVehicleVinTextView = mRootView.findViewById(R.id.owners_vehicle_list_vehicle_vin_text_view);
        Button mCurrentVehicleViewVehicleDetailsButton = mRootView.findViewById(R.id.owners_vehicles_view_details_button);

        mRefreshingViewsTextView = mRootView.findViewById(R.id.owners_vehicle_list_other_vehicles_progress_bar_text_view);
        mRefreshingViewsProgressBar = mRootView.findViewById(R.id.owners_vehicle_list_other_vehicles_progress_bar);

        mOwnersVehiclesListRecyclerView = mRootView.findViewById(R.id.owners_vehicle_list_other_vehicles_recycler_view);

        backButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().finish();
            }
        });

        mCurrentVehicleViewVehicleDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                VehicleDetailsFragment vehicleDetailsFragment = new VehicleDetailsFragment();

                String modelImageUrl = getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, null);
                String devicePin = getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

                Bundle bundle = new Bundle();
                bundle.putString(CURRENT_VEHICLE_MODEL_IMAGE_URL, modelImageUrl);
                bundle.putString(VEHICLE_PIN_KEY, devicePin);
                bundle.putString(VEHICLE_DETAILS_OBJECT, new Gson().toJson(PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs()));
                vehicleDetailsFragment.setArguments(bundle);


                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(android.R.id.content, vehicleDetailsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private void decideAndPopulateCurrentVehicleViews() {
        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        //populate current vehicle views
        if(vehicle == null) {

            mCurrentVehicleHeadingTextView.setVisibility(View.GONE);
            mCurrentVehicleCardView.setVisibility(View.GONE);
        } else {

            populateCurrentVehicleViews(vehicle);
        }
    }


    private void fetchOwnersVehiclesList() {
        if(getActivity() == null) {
            return;
        }

        if(mRefreshingViewsTextView.getVisibility() != View.VISIBLE || mRefreshingViewsProgressBar.getVisibility() != View.VISIBLE) {
            mRefreshingViewsTextView.setVisibility(View.VISIBLE);
            mRefreshingViewsProgressBar.setVisibility(View.VISIBLE);
        }

        User user = PrefUtils.getInstance(getActivity()).getUserObjectFromPrefs();

        if(user == null) {
            return;
        }

        String loggedInUserId = user.getId();

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vehicles_list_for_owners));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        UserWhereInput userWhereInput = UserWhereInput.builder().id(loggedInUserId).build();
        VehicleWhereInput vehicleWhereInput = VehicleWhereInput.builder().owner(userWhereInput).build();
        FetchOwnersVehiclesQuery fetchOwnersVehiclesQuery = FetchOwnersVehiclesQuery.builder().vehicleWhereInput(vehicleWhereInput).build();

        apolloClient.query(fetchOwnersVehiclesQuery).enqueue(new ApolloCall.Callback<FetchOwnersVehiclesQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchOwnersVehiclesQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vehicles_list_for_owners));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else {

                            if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getAll() != null) {
                                List<FetchOwnersVehiclesQuery.GetAll> vehiclesList = response.getData().vehicles().getAll();

                                if(vehiclesList == null) {
                                    return;
                                }

                                List<Pair<Vehicle, String>> vehiclesAndPinPairList = new ArrayList<>();

                                for(int index = 0 ; index < vehiclesList.size() ; index++) {
                                    FetchOwnersVehiclesQuery.GetAll fetchedVehicle = vehiclesList.get(index);

                                    Pair<Vehicle, String> vehiclePinPair;
                                    Vehicle vehicle = new Vehicle();
                                    Model model = new Model();
                                    String pin = null;

                                    if (fetchedVehicle != null) {

                                        if (fetchedVehicle.vin() != null) {
                                            vehicle.setVin(fetchedVehicle.vin());
                                        }
                                        if (fetchedVehicle.status() != null) {
                                            vehicle.setStatus(fetchedVehicle.status());
                                        }
                                        if(fetchedVehicle.metrics() != null && fetchedVehicle.metrics().odometer() != null) {
                                            vehicle.setOdoValue(fetchedVehicle.metrics().odometer());
                                        }
                                        if (fetchedVehicle.owner() != null && fetchedVehicle.owner().id() != null) {
                                            vehicle.setOwnerId(fetchedVehicle.owner().id());
                                        }
                                        if(fetchedVehicle.owner() != null && fetchedVehicle.owner().firstName() != null) {
                                            vehicle.setOwnerFirstName(fetchedVehicle.owner().firstName());
                                        }
                                        if(fetchedVehicle.owner() != null && fetchedVehicle.owner().lastName() != null) {
                                            vehicle.setOwnerLastName(fetchedVehicle.owner().lastName());
                                        }
                                        if(fetchedVehicle.owner() != null && fetchedVehicle.owner().email() != null) {
                                            vehicle.setOwnerEmailId(fetchedVehicle.owner().email());
                                        }
                                        if(fetchedVehicle.owner() != null && fetchedVehicle.owner().phone() != null) {
                                            vehicle.setOwnerPhoneNo(fetchedVehicle.owner().phone());
                                        }
                                        if (fetchedVehicle.buyer() != null && fetchedVehicle.buyer().firstName() != null) {
                                            vehicle.setBuyerFirstName(fetchedVehicle.buyer().firstName());
                                        }
                                        if (fetchedVehicle.buyer() != null && fetchedVehicle.buyer().lastName() != null) {
                                            vehicle.setBuyerLastName(fetchedVehicle.buyer().lastName());
                                        }
                                        if (fetchedVehicle.buyer() != null && fetchedVehicle.buyer().phone() != null) {
                                            vehicle.setBuyerPhoneNo(fetchedVehicle.buyer().phone());
                                        }
                                        if (fetchedVehicle.id() != null) {
                                            vehicle.setVehicleId(fetchedVehicle.id());
                                        }
                                        if (fetchedVehicle.distributor() != null) {
                                            if (fetchedVehicle.distributor().name() != null) {
                                                vehicle.setDistributorName(fetchedVehicle.distributor().name());
                                            }
                                            if (fetchedVehicle.distributor().phone() != null) {
                                                vehicle.setDistributorPhoneNo1(fetchedVehicle.distributor().phone());
                                            }
                                            if (fetchedVehicle.distributor().phone1() != null) {
                                                vehicle.setDistributorPhoneNo2(fetchedVehicle.distributor().phone1());
                                            }
                                            if(fetchedVehicle.distributor().address() != null) {
                                                vehicle.setDistributorAddress(fetchedVehicle.distributor().address());
                                            }
                                        }

                                        //set vehicle config
                                        if(fetchedVehicle.config() != null && fetchedVehicle.config().childSpeedLock() != null) {
                                            vehicle.setChildSpeedLock(fetchedVehicle.config().childSpeedLock());
                                        }
                                        if(fetchedVehicle.config() != null && fetchedVehicle.config().followMeHomeHeadLamp() != null) {
                                            vehicle.setFollowMeHeadlamp(fetchedVehicle.config().followMeHomeHeadLamp());
                                        }

                                        //set vehicle warranty
                                        if(fetchedVehicle.warranty() != null && fetchedVehicle.warranty().expiry() != null) {
                                            vehicle.setWarrantyExpiryTime(fetchedVehicle.warranty().expiry());
                                        }
                                        if(fetchedVehicle.warranty() != null && fetchedVehicle.warranty().status() != null) {
                                            vehicle.setWarrantyStatus(fetchedVehicle.warranty().status());
                                        }

                                        //set vehicle invoice details
                                        if(fetchedVehicle.invoice() != null && fetchedVehicle.invoice().createdAt() != null) {
                                            vehicle.setInvoiceCreationDate(fetchedVehicle.invoice().createdAt());
                                        }
                                        if(fetchedVehicle.invoice() != null && fetchedVehicle.invoice().passbook() != null
                                                &&  !fetchedVehicle.invoice().passbook().isEmpty() && fetchedVehicle.invoice().passbook().get(0).status() != null) {
                                            vehicle.setInvoiceStatus(fetchedVehicle.invoice().passbook().get(0).status());
                                        }

                                        //set model specific data
                                        if (fetchedVehicle.model() != null && fetchedVehicle.model().id() != null) {
                                            model.setId(fetchedVehicle.model().id());
                                        }
                                        if (fetchedVehicle.model() != null && fetchedVehicle.model().name() != null) {
                                            model.setName(fetchedVehicle.model().name());
                                        }
                                        if (fetchedVehicle.model() != null && fetchedVehicle.model().protocol() != null) {
                                            model.setProtocol(fetchedVehicle.model().protocol());
                                        }

                                        if(fetchedVehicle.device() != null && fetchedVehicle.device().pin() != null && !fetchedVehicle.device().pin().isEmpty()) {
                                            pin = fetchedVehicle.device().pin();
                                        }
                                    }

                                    vehicle.setModel(model);

                                    vehiclePinPair = new Pair<>(vehicle, pin);

                                    //add vehicle object to list
                                    vehiclesAndPinPairList.add(vehiclePinPair);
                                }

                                updateVehicleListHashMap(vehiclesAndPinPairList);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mRefreshingViewsTextView.setVisibility(View.GONE);
                        mRefreshingViewsProgressBar.setVisibility(View.GONE);

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });
    }

    private void updateVehicleListHashMap(List<Pair<Vehicle, String>> ownersVehiclesAndPinList) {
        if(getActivity() == null) {
            return;
        }

        if(ownersVehiclesAndPinList == null) {
            return;
        }

        //populate hash maps for owners vehicles list and models download required flag
        for(int index = 0 ; index < ownersVehiclesAndPinList.size() ; index++) {

            Pair<Vehicle, String> vehiclePinPair = ownersVehiclesAndPinList.get(index);

            mVehiclesListHashMap.put(vehiclePinPair.first.getVin(), vehiclePinPair);
            mIsModelImageDownloadedHashMap.put(vehiclePinPair.first.getModel().getId(), false);
        }

        String vehiclesListHashMapStr = new Gson().toJson(mVehiclesListHashMap);
        getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(OWNERS_VEHICLES_LIST, vehiclesListHashMapStr).apply();

        if(mIsModelImageDownloadedHashMap.containsValue(false)) {
            downLoadModelImages();
        }

        populateVehiclesList();

    }

    private void downLoadModelImages() {
        if(getActivity() == null) {
            return;
        }

        mModelId = null;
        for (HashMap.Entry<String, Boolean> entry : mIsModelImageDownloadedHashMap.entrySet()) {
            if(!entry.getValue()) {
                mModelId = entry.getKey();
                break;
            }
        }

        if(mModelId == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            return;
        }

        ModelWhereUniqueInput modelWhereUniqueInput = ModelWhereUniqueInput.builder().id(mModelId).build();
        FetchModelImageQuery fetchModelImageQuery = FetchModelImageQuery.builder().modelWhereUniqueInput(modelWhereUniqueInput).build();

        apolloClient.query(fetchModelImageQuery).enqueue(new ApolloCall.Callback<FetchModelImageQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchModelImageQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //mark image download as true
                        mIsModelImageDownloadedHashMap.put(mModelId, true);

                        if(response.getData() != null && response.getData().models() != null && response.getData().models().get() != null
                                && response.getData().models().get().image() != null) {

                            PrefUtils.getInstance(getActivity()).updateModelImageHashMap(mModelId, response.getData().models().get().image());
                            populateVehiclesList();
                        }

                        downLoadModelImages();
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mRefreshingViewsTextView.setVisibility(View.GONE);
                        mRefreshingViewsProgressBar.setVisibility(View.GONE);

                        //mark image download as true
                        mIsModelImageDownloadedHashMap.put(mModelId, true);
                        downLoadModelImages();
                    }
                });
            }
        });
    }

    private void populateCurrentVehicleViews(Vehicle vehicle) {
        if(getActivity() == null || vehicle == null) {
            return;
        }

        Model model = vehicle.getModel();
        if(model == null) {
            mCurrentVehicleModelNameTextView.setText(getString(R.string.not_available));
        } else {
            if(model.getName() == null || model.getName().isEmpty()) {
                mCurrentVehicleModelNameTextView.setText(getString(R.string.not_available));
            } else {
                mCurrentVehicleModelNameTextView.setText(model.getName());
            }
        }

        String vin = vehicle.getVin();

        if(vin == null || vin.isEmpty()) {
            mCurrentVehicleVinTextView.setText(getString(R.string.not_available));
        } else {
            mCurrentVehicleVinTextView.setText(vin);
        }

        //populate current vehicle model image
        String fileAbsolutePath = getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, null);
        if(fileAbsolutePath == null) {
            return;
        }

        File imageFile = new File(fileAbsolutePath);
        if(imageFile.exists()) {
            mCurrentVehicleCircleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getActivity() == null) {
                        return;
                    }

                    //set click listener for showing full model image
                    Intent intent = new Intent(getActivity(), PhotoViewActivity.class);

                    getActivity().getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).edit().putString(GENERIC_TEMP_IMAGE_KEY, imageFile.toURI().toString()).apply();
                    startActivity(intent);
                }
            });

            Picasso.with(getActivity()).load(imageFile).fit().centerInside().into(mCurrentVehicleCircleImageView);
        }
    }

    private void populateVehiclesList() {
        if(getActivity() == null) {
            return;
        }

        mRefreshingViewsTextView.setVisibility(View.GONE);
        mRefreshingViewsProgressBar.setVisibility(View.GONE);

        if(mVehiclesListHashMap == null || mVehiclesListHashMap.isEmpty()) {
            return;
        }

        List<Pair<Vehicle, String>> vehiclesDetailsAndPinList = new ArrayList<>();
        for (HashMap.Entry<String, Pair<Vehicle, String>> entry : mVehiclesListHashMap.entrySet()) {
            vehiclesDetailsAndPinList.add(entry.getValue());
        }

        HashMap<String, String> modelsImagesHashMap = PrefUtils.getInstance(getActivity()).getModelImageHashMapObjectFromPrefs();

        if(mOwnersVehicleListAdapter == null) {
            OwnersVehicleListAdapter ownersVehicleListAdapter = new OwnersVehicleListAdapter(vehiclesDetailsAndPinList, modelsImagesHashMap, getActivity());
            mOwnersVehicleListAdapter = ownersVehicleListAdapter;

            mOwnersVehiclesListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mOwnersVehiclesListRecyclerView.setAdapter(ownersVehicleListAdapter);

        } else {
            mOwnersVehicleListAdapter.updateImageHashMap(modelsImagesHashMap);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getActivity() == null) {
            return;
        }

        mVehiclesListHashMap = new HashMap<>();
        mIsModelImageDownloadedHashMap = new HashMap<>();
        mModelImagesHashMap = new HashMap<>();
        mOwnersVehicleListAdapter = null;

        decideAndPopulateCurrentVehicleViews();

        String vehiclesListStr = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(OWNERS_VEHICLES_LIST, null);
        if(vehiclesListStr != null && !vehiclesListStr.isEmpty()) {
            mVehiclesListHashMap = new Gson().fromJson(vehiclesListStr, new TypeToken<HashMap<String, Pair<Vehicle, String>>>(){}.getType());
            populateVehiclesList();
        }

        fetchOwnersVehiclesList();
    }
}