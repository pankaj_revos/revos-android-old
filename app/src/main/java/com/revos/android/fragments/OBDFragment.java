package com.revos.android.fragments;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.boltCore.android.constants.Constants;
import com.c.NativeController;
import com.csy.bl.ble.BleManager;
import com.csy.bl.ble.common.utils.TypeConvert;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.revos.android.R;
import com.revos.android.activities.ActiveBookingActivity;
import com.revos.android.activities.ConnectUsingVinActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.GeoFencingActivity;
import com.revos.android.activities.MainActivity;
import com.revos.android.activities.VehiclesAndDevicesListActivity;
import com.revos.android.activities.ZXingActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.DataFeedVehicleSnapshot;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.GeoFencing;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.retrofit.ApiClient;
import com.revos.android.retrofit.ApiInterface;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.ble.BleUtils;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.general.Version;
import com.revos.android.utilities.geofence.GeoFenceUtil;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.android.utilities.progressBars.DirectionProgressBar;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameters;
import com.revos.scripts.FetchDeviceQuery;
import com.revos.scripts.type.DeviceStatus;
import com.revos.scripts.type.DeviceType;
import com.revos.scripts.type.DeviceWhereUniqueInput;
import com.revos.scripts.type.FenceType;
import com.revos.scripts.type.ModelAccessType;
import com.revos.scripts.type.ModelProtocol;
import com.revos.scripts.type.VehicleStatus;
import com.tbit.tbitblesdk.Bike.TbitBle;
import com.tbit.tbitblesdk.Bike.model.BikeState;
import com.tbit.tbitblesdk.Bike.services.command.callback.SimpleCommonCallback;
import com.tbit.tbitblesdk.bluetooth.BleClient;
import com.tbit.tbitblesdk.protocol.callback.ResultCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import at.grabner.circleprogress.CircleProgressView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_CONNECTING;
import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.revos.android.activities.GenericMessageActivity.STATUS_INFO;
import static com.revos.android.constants.Constants.ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY;
import static com.revos.android.constants.Constants.ARROW_LEFT;
import static com.revos.android.constants.Constants.ARROW_RIGHT;
import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_LAST_READ_VEHICLE_CONTROL_PARAMS_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_LOCKED;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_NOT_FOUND;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CANCEL_PAIRING;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_ICONCOX;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TBIT;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_DEVICE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_DISCONNECT;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_LOCK_VEHICLE;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND;
import static com.revos.android.constants.Constants.BT_EVENT_REQUEST_UNLOCK_VEHICLE;
import static com.revos.android.constants.Constants.BT_EVENT_SCAN_STARTED;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_CONNECTING;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_DISCONNECTED;
import static com.revos.android.constants.Constants.BT_GPS_ICONCOX_CMD_RESPONSE;
import static com.revos.android.constants.Constants.CMD_SEND_FIND_VEHICLE;
import static com.revos.android.constants.Constants.CMD_SEND_REMOTE_LOCK;
import static com.revos.android.constants.Constants.CMD_SEND_REMOTE_IGNITION;
import static com.revos.android.constants.Constants.DEVICE_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.DISTRACTION_FREE_MODE_KEY;
import static com.revos.android.constants.Constants.ENABLE_CONNECT_USING_VIN_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_INFO_EVENT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.GEO_FENCING_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.GEO_FENCING_PREFS;
import static com.revos.android.constants.Constants.IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.NAV_EVENT_TRIP_ADVICE_DATA_UPDATED;
import static com.revos.android.constants.Constants.NAV_PREFS;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT;
import static com.revos.android.constants.Constants.NETWORKING_PREFS;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.constants.Constants.OVER_TEMPERATURE_LIMIT;
import static com.revos.android.constants.Constants.REVOS_DEVICE_ID_LENGTH;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_ADVICE_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_ADVICE_ETA_KEY;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_SETTINGS_PREFS;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LATITUDE;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;
import static com.revos.android.constants.Constants.VER_1_1;
import static com.revos.android.services.NordicBleService.mCurrentLocation;
import static com.revos.android.services.NordicBleService.mVehicleLiveParameters;

/**
 * Created by mohit on 11/9/17.
 */

public class OBDFragment extends Fragment{
    private Context mActivityContext;
    /**views and layouts*/
    private View rootView;

    private DirectionProgressBar mLeftTurnDirectionProgressBar, mRightTurnDirectionProgressBar;

    private ImageView mDisconnectImageView, mPowerButtonImageView, mOBDVehicleLockedTrueImageView,
                      mOBDBatteryImageView, mOBDHeadlightImageView, mOBDBrakeImageView, mOBDFindMyVehicleImageView,
                      mOBDPnPUnlockButtonImageView, mOBDPnPLockButtonImageView, mOBDSafeParkImageView;

    private LottieAnimationView mOBDFindMyVehicleLottieView, mOBDPnPUnlockButtonLottieView, mOBDPnPLockButtonLottieView;

    private LottieAnimationView mOBDErrorCodeImageView;

    private TextView mETALabelTextView, mETAValueTextView, mSpeedValueTextView, mOBDModeValueTextView,
                     mOBDOdoValueTextView, mOBDOdoLabelTextView, mOBDOdoUnitTextView, mOBDErrorCodeTextView,
                     mOBDBatteryLabelTextView, mOBDRangeTextView, mOBDRangeUnitTextView, mOBDConnectingToVehicleTextView,
                     mOBDVehicleRentalOngoingTextView, mOBDVehicleLockedTextView, mOBDVehicleUnlockedTextView;

    private TextClock mTextClock;

    private CircleProgressView mSpeedCircleProgressView, mConnectingToDeviceCircleProgressView, mOBDErrorCodeCircleProgressView;

    private Button mConnectToPreviousVehicleButton, mScanQRCodeButton, mOBDConnectUsingVINButton, mCancelButton, mOBDVehicleRentalOngoingButton;

    private LinearLayout mOBDVehicleRentalOngoingLinearLayout;

    private int mSpeedDivisor = 1;

    private int mMaxSpeed = 100;

    private final float ODO_MAX_MULTIPLIER = 1.5f;

    private final int NAV_BANNER_MAX_HEIGHT_IN_DP = 100;
    private final int NAV_BANNER_MIN_HEIGHT_IN_DP = 0;

    private Animation mResizeNavBannerAnimation;

    private boolean mIsVehicleLocked = true;

    private final byte KEYLESS_SWITCH_DEFAULT = 0;
    private final byte KEYLESS_SWITCH_DISABLED = 1;
    private final byte KEYLESS_SWITCH_ENABLED = 2;

    private byte mKeylessSwitchStatus = KEYLESS_SWITCH_DEFAULT;

    /**Timer for polling*/
    private Timer mTimer;

    /**Bound for demo mode*/
    Integer[] setBoundArray;

    private String mZxingResultStr;

    /**request code for zxing activity*/
    private final int ZXING_REQUEST_CODE = 1000;
    private final int CAMERA_PERMISSION_REQUEST_CODE = 2000;
    private final int DEVICE_ID_QR_CODE_LENGTH = 37;

    private final String TAG = "tag";
    private final String COMMAND = "command";
    private final String VIN = "tag";
    private final String EVENTS = "events";
    private final String FIND = "FIND";
    private final String IGNITION_ON = "IGNITION_ON";
    private final String IGNITION_OFF = "IGNITION_OFF";

    private final int ANTI_THEFT_OFF = 1;
    private final int ANTI_THEFT_SILENT = 2;
    private final int ANTI_THEFT_NORMAL = 3;

    private boolean isLockRequested = false;
    private boolean isUnlockRequested = false;
    private boolean isFindVehicleRequested = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        //load the appropriate layout according to the state of bluetooth connection
        if(getActivity() != null && vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            rootView = inflater.inflate(R.layout.obd_fragment, container, false);
        } else if(getActivity() != null && NordicBleService.getConnectionState() == STATE_CONNECTED) {
            rootView = inflater.inflate(R.layout.obd_fragment, container, false);
        } else {
            rootView = inflater.inflate(R.layout.obd_fragment_pair_vehicle, container, false);
        }

        mActivityContext = getActivity();

        setupViews();

        setupOngoingRentalPopUpViews();

        showHideScanManuallyView();

        setBound();

        clearTimer();

        setupTimer();

        return rootView;
    }

    private void setupOngoingRentalPopUpViews() {
        if(rootView == null) {
            return;
        }

        mOBDVehicleRentalOngoingTextView = rootView.findViewById(R.id.obd_vehicle_rental_ongoing_text_view);
        mOBDVehicleRentalOngoingLinearLayout = rootView.findViewById(R.id.obd_vehicle_rental_ongoing_linear_layout);
        mOBDVehicleRentalOngoingButton = rootView.findViewById(R.id.obd_vehicle_rental_ongoing_button);

        mOBDVehicleRentalOngoingTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openActiveBookingsActivity();
            }
        });

        mOBDVehicleRentalOngoingLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openActiveBookingsActivity();
            }
        });

        mOBDVehicleRentalOngoingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openActiveBookingsActivity();
            }
        });
    }

    private void openActiveBookingsActivity() {
        Intent intent = new Intent(getActivity(), ActiveBookingActivity.class);
        startActivity(intent);
    }

    private void toggleOngoingRentalViews() {
        if(getActivity() == null) {
            return;
        }

        String rentalActiveBookingStr = getActivity().getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);

        RentalVehicle rentalVehicle = new Gson().fromJson(rentalActiveBookingStr, RentalVehicle.class);

        if(rentalVehicle != null && rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {
            mOBDVehicleRentalOngoingLinearLayout.setVisibility(VISIBLE);
        } else {
            mOBDVehicleRentalOngoingLinearLayout.setVisibility(GONE);
        }
    }

    private void setupTimer() {
        //start infinite polling for checking bluetooth connection state
        //check if notification advice has to be displayed or not
        if(mTimer == null) {
            mTimer = new Timer();
        }

        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getActivity() == null || !getActivity().hasWindowFocus()) {
                                return;
                            }
                            updateConnectToVehicleLayout();
                            animateViewsForGpsTrackerMode();
                            updateBatteryViewsIfRequired();
                            toggleBatteryViews();
                            toggleSafeParkStatus();

                            if(NordicBleService.getConnectionState() != STATE_CONNECTED) {
                                //this might be a plug n play device, update the views
                                toggleBrakeAndHeadLightView(null);
                            }

                            updateDisconnectImageView();
                            showGeoFenceErrorIfRequired();

                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }
                    }
                });
            }
        }, 100, 500);
    }

    private void toggleSafeParkStatus() {
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        if(vehicle.getModel() != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {

            fetchVehicleLockStatus(vehicle);

        } else if(NordicBleService.getConnectionState() == STATE_CONNECTED && mVehicleLiveParameters != null) {

            long antiTheftMode = mVehicleLiveParameters.getAntiTheftMode();

            mOBDSafeParkImageView.setVisibility(VISIBLE);

            if(antiTheftMode == ANTI_THEFT_OFF) {
                mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_red));
            } else if(antiTheftMode == ANTI_THEFT_SILENT || antiTheftMode == ANTI_THEFT_NORMAL) {
                mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_green));
            } else {
                mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_orange));
            }
        } else {
            mOBDSafeParkImageView.setVisibility(GONE);
        }
    }

    private void fetchVehicleLockStatus(Vehicle vehicle) {
        if(getActivity() == null || vehicle == null) {
            return;
        }

        String vin = vehicle.getVin();

        if(vin == null || vin.isEmpty()) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String responseBody = response.body().string();

                            JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                            int status = jsonObject.get("status").getAsInt();

                            if(status != 200) {
                                return;
                            }

                            String dataStr = jsonObject.get("data").toString();

                            DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                            toggleVehicleLockUnlockStatus(vehicleSnapshot.getIgnition().getIgnition());

                            if(vehicleSnapshot != null && vehicleSnapshot.getUart() != null) {

                                if(!vehicleSnapshot.getUart().isSoftLockStatus()) {
                                    mOBDSafeParkImageView.setVisibility(VISIBLE);
                                    mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_green));
                                } else {
                                    mOBDSafeParkImageView.setVisibility(GONE);
                                }
                                //TODO:: when to show orange shield
                            }
                        } catch (Exception e) {}
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mOBDSafeParkImageView.setVisibility(VISIBLE);
                        mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_orange));
                    }
                });
            }
        });
    }

    private void toggleVehicleLockUnlockStatus(int ignitionStatus) {

        if(getActivity() == null) {
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(ignitionStatus == 0) {
                    mOBDVehicleLockedTextView.setVisibility(VISIBLE);
                    mOBDVehicleUnlockedTextView.setVisibility(GONE);
                } else if(ignitionStatus == 1) {
                    mOBDVehicleUnlockedTextView.setVisibility(VISIBLE);
                    mOBDVehicleLockedTextView.setVisibility(GONE);
                } else {
                    mOBDVehicleUnlockedTextView.setVisibility(GONE);
                    mOBDVehicleLockedTextView.setVisibility(GONE);
                }
            }
        });
    }

    private boolean showGeoFenceErrorIfRequired() {
        if(getActivity() == null) {
            return false;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle == null || vehicle.getModel() == null) {
            return false;
        }

        if(vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {

            if(!isVehicleWithinGeoFenceBounds(vehicle)) {
                showOBDErrorLayout(getString(R.string.obd_error_vehicle_outside_bounds));
                return true;
            }
        }

        if(NordicBleService.getConnectionState() == STATE_CONNECTED && new Version(NordicBleService.getDeviceFirmwareVersion()).compareTo(new Version(VER_1_1)) > 0) {

            if(!isVehicleWithinGeoFenceBounds(vehicle) && NordicBleService.mVehicleLiveParameters != null) {
                showOBDErrorLayout(getString(R.string.obd_error_vehicle_outside_bounds));
                return true;
            }
        }

        return false;
    }

    private boolean isVehicleWithinGeoFenceBounds(Vehicle vehicle) {
        if(getActivity() == null || vehicle == null) {
            return true;
        }

        //check for geo fence crossing
        String geoFencingDataStr = getActivity().getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).getString(GEO_FENCING_JSON_DATA_KEY, null);
        if(geoFencingDataStr == null) {
            return true;
        }
        GeoFencing geoFencing = new Gson().fromJson(geoFencingDataStr, GeoFencing.class);
        boolean isInside = false;

        double lastKnownVehicleLat = 0;
        double lastKnownVehicleLng = 0;

        try {

            if(vehicle.getModel() != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                String lastKnownLatStr = getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_LAST_KNOWN_LATITUDE, null);
                String lastKnownLngStr = getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).getString(VEHICLE_LAST_KNOWN_LONGITUDE, null);

                //if either is null then it's a corrupted packet
                if(lastKnownLatStr == null || lastKnownLngStr == null) {
                    return true;
                }

                lastKnownVehicleLat = Double.parseDouble(lastKnownLatStr);
                lastKnownVehicleLng = Double.parseDouble(lastKnownLngStr);
            } else {
                if(mCurrentLocation == null) {
                    return true;
                }
                lastKnownVehicleLat = mCurrentLocation.getLatitude();
                lastKnownVehicleLng = mCurrentLocation.getLongitude();
            }

            //if both are null then tracker could not acquire a GPS lock
            if(lastKnownVehicleLat == 0 && lastKnownVehicleLng == 0) {
                return true;
            }

            LatLng currentLatLng = new LatLng(lastKnownVehicleLat, lastKnownVehicleLng);
            if(geoFencing.getGeoFenceType() == FenceType.RADIAL) {
                isInside = GeoFenceUtil.isInsideCircle(currentLatLng, geoFencing.getCircleGeoFenceCentre(), geoFencing.getCircleRadius());
            } else if(geoFencing.getGeoFenceType() == FenceType.POLYGON) {
                isInside = GeoFenceUtil.isInsidePolygon(currentLatLng, geoFencing.getPolygonVerticesList());
            }

            return isInside;
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            return true;
        }
    }

    private void clearTimer() {
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
    }

    private void updateDisconnectImageView() {
        if(mDisconnectImageView == null || getActivity() == null) {
            return;
        }

        Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
        if(device != null && device.getDeviceType() != null) {

            if(device.getDeviceType() == DeviceType.TBIT) {
                if (TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {
                    mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_bluetooth_disconnect));
                } else {
                    mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_plug_disconnect));
                }
            } else if(device.getDeviceType().equals(DeviceType.ICONCOX)) {
                if(NordicBleService.mIsIconcoxBleConnected) {
                    mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_bluetooth_disconnect));
                } else {
                    mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_plug_disconnect));
                }
            }
        }
    }

    private void showHideScanManuallyView() {

        if(getActivity() == null) {
            return;
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE);
        boolean connectToVehicleUsingVIN = sharedPreferences.getBoolean(ENABLE_CONNECT_USING_VIN_KEY, false);

        if(!connectToVehicleUsingVIN) {

            if(mOBDConnectUsingVINButton != null) {

                mOBDConnectUsingVINButton.setVisibility(GONE);
            }

        } else {

            if(mOBDConnectUsingVINButton != null) {

                mOBDConnectUsingVINButton.setVisibility(VISIBLE);
            }
        }

    }

    private void setupViews() {
        setupOBDRelatedViews();

        setupBluetoothPairingRelatedViews();

        if(getActivity() == null) {
            return;
        }

        Device cloudDevice = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        //if app is in connected state then set the odo, speed divisor and max speed
        if(NordicBleService.getConnectionState() == STATE_CONNECTED) {
            if (cloudDevice != null) {
                mMaxSpeed = cloudDevice.getVehicle().getModel().getMaxSpeed();
                mSpeedDivisor = cloudDevice.getVehicle().getModel().getSpeedDivisor();
                mSpeedCircleProgressView.setMaxValue((float)Math.ceil(mMaxSpeed * ODO_MAX_MULTIPLIER));
            }
        } else if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            if(cloudDevice != null) {
                mMaxSpeed = cloudDevice.getVehicle().getModel().getMaxSpeed();
                mSpeedCircleProgressView.setMaxValue((float)Math.ceil(mMaxSpeed * ODO_MAX_MULTIPLIER));
            }
        }
    }

    private void updateBatteryViewsIfRequired() {
        // do this also only every ten seconds, else TBIT BLE interface jams
        long timeInSeconds = Math.round(Math.floor(System.currentTimeMillis() / 1000));
        if (timeInSeconds % 10 != 0) {
            return;
        }

        updateBatteryViews();
    }

    private void updateBatteryViews() {
        if (getActivity() == null) {
            return;
        }

        Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
        if (device != null && device.getVehicle() != null && device.getVehicle().getModel().getProtocol() != null
                && device.getVehicle().getModel().getProtocol() == ModelProtocol.PNP) {

            DataFeedVehicleSnapshot livePacket = PrefUtils.getInstance(getActivity()).getLivePacketObjectFromPrefs();
            if (livePacket == null && device.getDeviceType() != null && device.getDeviceType() != DeviceType.TBIT) {
                //live packet is null, also device type is not equal to TBIT, then return
                return;
            }

            double batteryADCVoltage = 0;

            if(livePacket != null) {
                batteryADCVoltage = livePacket.getBattery().getBatteryVoltageAdc();
            }

            //if we're connected via tbit ble then get the bike state using that and update the battery ADC voltage
            if (TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {

                BikeState bikeState = TbitBle.getState();
                //Print bike state
                Timber.d("Bike state : " + bikeState.toString());

                int[] systemState = bikeState.getSystemState();
                int[] bikeStateLocked = {1, 0, 1, 0, 0, 0, 1, 0};
                int[] bikeStateUnlocked = {0, 0, 0, 1, 0, 0, 1, 0};

                if(Arrays.equals(systemState, bikeStateLocked) || Arrays.equals(systemState, bikeStateUnlocked)) {

                    if(Arrays.equals(systemState, bikeStateLocked)) {
                        mOBDSafeParkImageView.setVisibility(VISIBLE);
                        mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_green));
                    } else {
                        mOBDSafeParkImageView.setVisibility(GONE);
                    }
                } else if(livePacket != null) {
                    if(livePacket.getIgnition().getIgnition() == 0) {
                        mOBDSafeParkImageView.setVisibility(VISIBLE);
                        mOBDSafeParkImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_shield_green));
                    } else if(livePacket.getIgnition().getIgnition() == 1) {
                        mOBDSafeParkImageView.setVisibility(GONE);
                    }
                }

                batteryADCVoltage = bikeState.getBattery() / 1000;
                //if bluetooth gives voltage value as zero then use livePacket data
                if(batteryADCVoltage == 0) {
                    //update battery voltage using live packet
                    if (livePacket != null) {
                        batteryADCVoltage = livePacket.getBattery().getBatteryVoltageAdc();
                    }
                }
            }

            double maxVoltage = device.getVehicle().getModel().getBatteryMaxVoltageLimit();
            double minVoltage = device.getVehicle().getModel().getBatteryMinVoltageLimit();

            if(batteryADCVoltage <= (minVoltage * 0.5)) {
                showOBDErrorLayout(getString(R.string.battery_removed_error));
            } else {
                if(mOBDErrorCodeTextView != null && mOBDErrorCodeTextView.getText().equals(getString(R.string.battery_removed_error))) {
                    hideOBDErrorLayout();
                }
            }

            if (batteryADCVoltage != 0 && maxVoltage != 0 && minVoltage != 0) {
                double batteryPercentage = ((batteryADCVoltage - minVoltage) / (maxVoltage - minVoltage)) * 100;
                long socValue = Math.round(Math.floor(batteryPercentage));
                updateBatteryLayout(socValue, batteryADCVoltage, true);
            }
        }
    }

    private void toggleBatteryViews() {
        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
        if((vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP)
                || NordicBleService.getConnectionState() == STATE_CONNECTED) {

            if(vehicle != null && vehicle.getModel().getProtocol() !=  null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                mOBDBatteryImageView.setVisibility(VISIBLE);
                mOBDBatteryLabelTextView.setVisibility(VISIBLE);
            }

            if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                mOBDRangeUnitTextView.setVisibility(INVISIBLE);
                mOBDRangeTextView.setVisibility(INVISIBLE);
            } else {
                mOBDRangeUnitTextView.setVisibility(INVISIBLE);
                mOBDRangeTextView.setVisibility(INVISIBLE);
            }

        } else {
            mOBDBatteryImageView.setVisibility(INVISIBLE);
            mOBDBatteryLabelTextView.setVisibility(INVISIBLE);
            mOBDRangeUnitTextView.setVisibility(INVISIBLE);
            mOBDRangeTextView.setVisibility(INVISIBLE);
        }
    }

    /**Set array speed, battery and range bounds*/
    private void setBound(){

        setBoundArray = new Integer[12];

        for(int i = 0; i <= 10; i++)
        {
            setBoundArray[i] = i * 10;
        }

    }

    private void animateViewsForGpsTrackerMode() {
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            int speed = -1;
            if(mCurrentLocation != null && mCurrentLocation.hasSpeed()) {
                speed = Math.round(mCurrentLocation.getSpeed() * 18 / 5);
            }
            updateSpeedRelatedViews(speed, false, -1, false);
        }
    }

    private void setupOBDRelatedViews() {
        if(getActivity() == null)
            return;

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        //we are not connected to bluetooth, nor is the vehicle type plug n play
        if(NordicBleService.getConnectionState() != STATE_CONNECTED
                && (vehicle == null || vehicle.getModel().getProtocol() == null || (vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() != ModelProtocol.PNP)))
            return;

        mDisconnectImageView = (ImageView) rootView.findViewById(R.id.obd_disconnect_image_view);
        //update icon according to vehicle status
        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            if(mDisconnectImageView == null) {
                return;
            }
            mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_plug_disconnect));
        } else {
            if(mDisconnectImageView == null) {
                return;
            }
            mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_bluetooth_disconnect));
        }

        mPowerButtonImageView = (ImageView) rootView.findViewById(R.id.obd_power_button_image_view);

        mOBDPnPLockButtonImageView = rootView.findViewById(R.id.obd_pnp_lock_button_image_view);
        mOBDPnPLockButtonLottieView = rootView.findViewById(R.id.obd_pnp_lock_button_lottie_view);
        mOBDPnPUnlockButtonImageView = rootView.findViewById(R.id.obd_pnp_unlock_button_image_view);
        mOBDPnPUnlockButtonLottieView = rootView.findViewById(R.id.obd_pnp_unlock_button_lottie_view);
        mOBDSafeParkImageView = rootView.findViewById(R.id.obd_safe_park_image_view);
        mOBDVehicleLockedTextView = rootView.findViewById(R.id.obd_vehicle_locked_text_view);
        mOBDVehicleUnlockedTextView = rootView.findViewById(R.id.obd_vehicle_unlocked_text_view);

        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mOBDPnPLockButtonImageView.setVisibility(VISIBLE);
            mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);
        }

        /*mOBDSafeParkImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {

                    Fragment antiTheftForPnp = new AntiTheftForPnpFragment();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(android.R.id.content, antiTheftForPnp)
                            .addToBackStack(null)
                            .commit();
                } else {
                    Intent intent = new Intent(getActivity(), VehicleSettingsActivity.class);
                    startActivity(intent);
                }
            }
        });*/

        mOBDPnPUnlockButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                //check for device type
                Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
                if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {
                    if(TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {
                        sendUnlockVehicleCommandForTbitBle();
                        return;
                    }
                } else if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.ICONCOX) {
                    isUnlockRequested = true;
                    sendUnlockCommandForIConcoxBle();
                    return;
                }

                makeVehicleIgnitionOnApiCall();
            }
        });

        mOBDPnPLockButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                //check for device type
                Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
                if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {
                    if(TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {
                        sendLockVehicleCommandForTbitBle();
                        return;
                    }
                } else if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.ICONCOX) {
                    isLockRequested = true;
                    sendLockCommandForIConcoxBle();
                    return;
                }

                makeLockVehicleApiCall();
            }
        });

        mOBDModeValueTextView = (TextView) rootView.findViewById(R.id.obd_mode_value_text_view);
        mOBDOdoValueTextView = (TextView) rootView.findViewById(R.id.obd_odo_value_text_view);
        mOBDOdoLabelTextView = (TextView) rootView.findViewById(R.id.obd_odo_label_text_view);
        mOBDOdoUnitTextView = (TextView) rootView.findViewById(R.id.obd_odo_unit_text_view);

        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mOBDModeValueTextView.setVisibility(INVISIBLE);
            mOBDOdoValueTextView.setVisibility(INVISIBLE);
            mOBDOdoLabelTextView.setVisibility(INVISIBLE);
            mOBDOdoUnitTextView.setVisibility(INVISIBLE);
        }

        mLeftTurnDirectionProgressBar = (DirectionProgressBar)rootView.findViewById(R.id.obd_left_turn_direction_progress_bar);
        mRightTurnDirectionProgressBar = (DirectionProgressBar)rootView.findViewById(R.id.obd_right_turn_direction_progress_bar);
        mTextClock = (TextClock)rootView.findViewById(R.id.obd_text_clock);
        mSpeedValueTextView = (TextView)rootView.findViewById(R.id.obd_speed_value_text_view);
        mSpeedCircleProgressView = (CircleProgressView)rootView.findViewById(R.id.obd_speed_circle_progress_view);
        mOBDErrorCodeCircleProgressView = (CircleProgressView) rootView.findViewById(R.id.obd_error_code_circle_progress_view);
        mOBDErrorCodeImageView = (LottieAnimationView) rootView.findViewById(R.id.obd_error_code_image_view);
        mOBDErrorCodeTextView = (TextView) rootView.findViewById(R.id.obd_error_code_text_view);
        mOBDVehicleLockedTrueImageView = (ImageView) rootView.findViewById(R.id.obd_vehicle_locked_true);

        mOBDBatteryLabelTextView = (TextView) rootView.findViewById(R.id.obd_battery_label_text_view);
        mOBDBatteryImageView = (ImageView) rootView.findViewById(R.id.obd_battery_image_view);
        mOBDRangeTextView = (TextView) rootView.findViewById(R.id.obd_range_text_view);
        mOBDRangeUnitTextView = (TextView) rootView.findViewById(R.id.obd_range_unit_text_view);

        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mOBDRangeTextView.setVisibility(INVISIBLE);
            mOBDRangeUnitTextView.setVisibility(INVISIBLE);
        }

        mOBDHeadlightImageView = (ImageView) rootView.findViewById(R.id.obd_headlight_image_view);
        mOBDBrakeImageView = (ImageView) rootView.findViewById(R.id.obd_brake_image_view);
        mOBDFindMyVehicleImageView = (ImageView) rootView.findViewById(R.id.obd_find_my_vehicle_image_view);
        mOBDFindMyVehicleLottieView = (LottieAnimationView) rootView.findViewById(R.id.obd_find_my_vehicle_lottie_view);

        //make find my vehicle button visible only if device is tbit
        if(vehicle != null && vehicle.getModel() != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

            if(device != null && device.getDeviceType() != null &&
                    (device.getDeviceType().equals(DeviceType.GPS_IOT) || device.getDeviceType().equals(DeviceType.TELTONIKA))) {
                mOBDFindMyVehicleImageView.setVisibility(INVISIBLE);
                mOBDFindMyVehicleLottieView.setVisibility(INVISIBLE);

            } else {
                mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
            }
        } else {
            mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
        }

        mPowerButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }
                if(mKeylessSwitchStatus == KEYLESS_SWITCH_DISABLED) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_UNLOCK_VEHICLE));
                } else if(mKeylessSwitchStatus == KEYLESS_SWITCH_ENABLED) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_LOCK_VEHICLE));
                }
            }
        });

        mDisconnectImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null) {
                    return;
                }

                Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
                if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                    //check if device type is TBIT and we're connected or not
                    Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
                    if(device != null && device.getDeviceType() != null
                            && device.getDeviceType() == DeviceType.TBIT) {

                        if((TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED
                                || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED))) {
                            TbitBle.disConnect();
                        }
                    } else if(device !=null && device.getDeviceType() != null && device.getDeviceType().equals(DeviceType.ICONCOX)) {
                        if(NordicBleService.mIsIconcoxBleConnected) {
                            BleManager.getInstance(getActivity()).disconnect();
                        }
                    }


                    //clear device, vehicle and trip data and refresh fragments
                    getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().clear().apply();
                    getActivity().getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
                    getActivity().getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();
                    //refresh the drawer layout
                    ((MainActivity)getActivity()).setupDrawerLayout();
                    //clear geo fence prefs
                    getActivity().getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().clear().apply();
                    //refresh the fragments at last
                    ((MainActivity)getActivity()).refreshFragments();
                } else {
                    //set user manually disconnected flag
                    getActivity().getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putBoolean(BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY, true).apply();
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_DISCONNECT));
                }
            }
        });

        mOBDFindMyVehicleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null) {
                    return;
                }

                Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

                if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                    //check for device type
                    Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
                    if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {
                        if(TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {
                            sendFindVehicleCommandForTbitBle();
                            return;
                        }
                    } else if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.ICONCOX) {
                        isFindVehicleRequested = true;
                        sendFindMyVehicleCommandForIConcoxBle();
                        return;
                    }

                    makeFindVehicleApiCall();
                } else {
                    findVehicleIfConnected();
                }

            }
        });

        mOBDVehicleLockedTrueImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null){
                    return;
                }

                Device lockedCloudDevice = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

                if(lockedCloudDevice != null) {

                   if(lockedCloudDevice.getVehicle().getModel().getModelAccessType().equals(ModelAccessType.KEY)) {

                       Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                       Bundle bundle = new Bundle();
                       bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.insert_key_to_unlock_vehicle));
                       bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
                       bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                       intent.putExtras(bundle);
                       startActivity(intent);

                   } else {
                       if(NordicBleService.getConnectionState() == STATE_CONNECTED) {

                           Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                           Bundle bundle = new Bundle();
                           bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.press_start_button_to_unlock_vehicle));
                           bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
                           bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                           intent.putExtras(bundle);
                           startActivity(intent);

                       } else {

                           Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                           Bundle bundle = new Bundle();
                           bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_not_connected));
                           bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_NONE);
                           bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                           intent.putExtras(bundle);
                           startActivity(intent);

                       }
                   }
                }
            }
        });

        mOBDErrorCodeCircleProgressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAppropriateActivityOnErrorClick();
            }
        });

        mOBDErrorCodeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAppropriateActivityOnErrorClick();
            }
        });

        mOBDErrorCodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAppropriateActivityOnErrorClick();
            }
        });

        setupNavigationAdviceRelatedViews();

    }

    private void openAppropriateActivityOnErrorClick() {
        if(mOBDErrorCodeTextView == null) {
            return;
        }

        String displayMessage = mOBDErrorCodeTextView.getText().toString();
        if(displayMessage == null || displayMessage.equals("")) {
            return;
        }

        if(displayMessage.equals(getString(R.string.obd_error_vehicle_outside_bounds))) {
            Intent intent = new Intent(getActivity(), GeoFencingActivity.class);
            startActivity(intent);
        }

        if(displayMessage.equals(getString(R.string.over_current)) || displayMessage.equals(getString(R.string.over_temperature))
                || displayMessage.equals(getString(R.string.throttle_error)) || displayMessage.equals(getString(R.string.controller_error))
                || displayMessage.equals(getString(R.string.motor_error))) {
            Intent intent = new Intent(getActivity(), VehiclesAndDevicesListActivity.class);
            startActivity(intent);
        }

    }

    private void sendLockVehicleCommandForTbitBle() {
        //device type is TBIT, this means we can send commands over BLE if we're connected
        if(TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {

            mOBDPnPLockButtonImageView.setVisibility(INVISIBLE);

            mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
            mOBDPnPLockButtonLottieView.setAnimation(R.raw.ic_wait);
            mOBDPnPLockButtonLottieView.setProgress(0);
            mOBDPnPLockButtonLottieView.loop(true);
            mOBDPnPLockButtonLottieView.playAnimation();

            //try sending command directly via ble and return
            TbitBle.lock(new ResultCallback() {
                @Override
                public void onResult(int resultCode) {
                    int resId = R.raw.ic_generic_message_success;
                    if(resultCode != 0) {
                        resId = R.raw.ic_generic_message_error;
                    }

                    mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
                    mOBDPnPLockButtonLottieView.setAnimation(resId);
                    mOBDPnPLockButtonLottieView.setProgress(0);
                    mOBDPnPLockButtonLottieView.loop(false);
                    mOBDPnPLockButtonLottieView.playAnimation();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mOBDPnPLockButtonLottieView.setVisibility(INVISIBLE);
                            mOBDPnPLockButtonImageView.setVisibility(VISIBLE);
                        }
                    }, 2000);
                }
            });
            return;
        }
    }

    private void sendUnlockVehicleCommandForTbitBle() {
        //device type is TBIT, this means we can send commands over BLE if we're connected
        if(TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {

            mOBDPnPUnlockButtonImageView.setVisibility(INVISIBLE);

            mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
            mOBDPnPUnlockButtonLottieView.setAnimation(R.raw.ic_wait);
            mOBDPnPUnlockButtonLottieView.setProgress(0);
            mOBDPnPUnlockButtonLottieView.loop(true);
            mOBDPnPUnlockButtonLottieView.playAnimation();

            //try sending command directly via ble and return
            TbitBle.unlock(new ResultCallback() {
                @Override
                public void onResult(int resultCode) {
                    int resId = R.raw.ic_generic_message_success;
                    if(resultCode != 0) {
                        resId = R.raw.ic_generic_message_error;
                    }

                    mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
                    mOBDPnPUnlockButtonLottieView.setAnimation(resId);
                    mOBDPnPUnlockButtonLottieView.setProgress(0);
                    mOBDPnPUnlockButtonLottieView.loop(false);
                    mOBDPnPUnlockButtonLottieView.playAnimation();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mOBDPnPUnlockButtonLottieView.setVisibility(INVISIBLE);
                            mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);
                        }
                    }, 2000);
                }
            });
            return;
        }
    }

    private void sendFindVehicleCommandForTbitBle() {
        //device type is TBIT, this means we can send commands over BLE if we're connected
        if(TbitBle.hasInitialized() && (TbitBle.getBleConnectionState() == BleClient.STATE_CONNECTED || TbitBle.getBleConnectionState() == BleClient.STATE_SERVICES_DISCOVERED)) {

            mOBDFindMyVehicleImageView.setVisibility(INVISIBLE);

            mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
            mOBDFindMyVehicleLottieView.setAnimation(R.raw.ic_wait);
            mOBDFindMyVehicleLottieView.setProgress(0);
            mOBDFindMyVehicleLottieView.loop(true);
            mOBDFindMyVehicleLottieView.playAnimation();


            //try sending command directly via ble and return
            TbitBle.commonCommand((byte)0x03, (byte)0x04, new Byte[]{0x01}, new SimpleCommonCallback(new ResultCallback() {
                @Override
                public void onResult(int resultCode) {
                    int resId = R.raw.ic_generic_message_success;
                    if(resultCode != 0) {
                        resId = R.raw.ic_generic_message_error;
                    }

                    mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
                    mOBDFindMyVehicleLottieView.setAnimation(resId);
                    mOBDFindMyVehicleLottieView.setProgress(0);
                    mOBDFindMyVehicleLottieView.loop(false);
                    mOBDFindMyVehicleLottieView.playAnimation();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mOBDFindMyVehicleLottieView.setVisibility(INVISIBLE);
                            mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
                        }
                    }, 2000);
                }
            }));
            return;
        }
    }

    private void sendLockCommandForIConcoxBle() {

        mOBDPnPLockButtonImageView.setVisibility(INVISIBLE);

        mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
        mOBDPnPLockButtonLottieView.setAnimation(R.raw.ic_wait);
        mOBDPnPLockButtonLottieView.setProgress(0);
        mOBDPnPLockButtonLottieView.loop(true);
        mOBDPnPLockButtonLottieView.playAnimation();

        if(NordicBleService.mIsIconcoxBleConnected) {
            String cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(CMD_SEND_REMOTE_LOCK));
            BleManager.getInstance(getActivity()).sendHexStringCmd(cmd);
        } else {
            makeLockVehicleApiCall();
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isLockRequested) {
                    makeLockVehicleApiCall();
                }
            }
        }, 5000);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mOBDPnPLockButtonLottieView.setVisibility(INVISIBLE);
                mOBDPnPLockButtonImageView.setVisibility(VISIBLE);
            }
        }, 3000);

        return;
    }

    private void sendUnlockCommandForIConcoxBle() {

        mOBDPnPUnlockButtonImageView.setVisibility(INVISIBLE);

        mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
        mOBDPnPUnlockButtonLottieView.setAnimation(R.raw.ic_wait);
        mOBDPnPUnlockButtonLottieView.setProgress(0);
        mOBDPnPUnlockButtonLottieView.loop(true);
        mOBDPnPUnlockButtonLottieView.playAnimation();

        if(NordicBleService.mIsIconcoxBleConnected) {
            String cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(CMD_SEND_REMOTE_IGNITION));
            BleManager.getInstance(getActivity()).sendHexStringCmd(cmd);
        } else {
            makeVehicleIgnitionOnApiCall();
        }

        //if command not executed then, send command via api
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isUnlockRequested) {
                    makeVehicleIgnitionOnApiCall();
                }
            }
        }, 5000);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mOBDPnPUnlockButtonLottieView.setVisibility(INVISIBLE);
                mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);
            }
        }, 3000);

        return;

    }

    private void sendFindMyVehicleCommandForIConcoxBle() {

        mOBDFindMyVehicleImageView.setVisibility(INVISIBLE);

        mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
        mOBDFindMyVehicleLottieView.setAnimation(R.raw.ic_wait);
        mOBDFindMyVehicleLottieView.setProgress(0);
        mOBDFindMyVehicleLottieView.loop(true);
        mOBDFindMyVehicleLottieView.playAnimation();

        if(NordicBleService.mIsIconcoxBleConnected) {
            String cmd = NativeController.encode8String(TypeConvert.hexStringToBytes(CMD_SEND_FIND_VEHICLE));
            BleManager.getInstance(getActivity()).sendHexStringCmd(cmd);
        } else {
            makeFindVehicleApiCall();
        }

        //if command not executed then, send command via api
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isFindVehicleRequested) {
                    makeFindVehicleApiCall();
                }
            }
        }, 5000);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mOBDFindMyVehicleLottieView.setVisibility(INVISIBLE);
                mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
            }
        }, 3000);

        return;
    }

    private void updateFindVehicleStatusForIconcox(boolean isSuccessful) {
        int resId = R.raw.ic_generic_message_success;
         if(!isSuccessful) {
            resId = R.raw.ic_generic_message_error;
        }

        mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
        mOBDFindMyVehicleLottieView.setAnimation(resId);
        mOBDFindMyVehicleLottieView.setProgress(0);
        mOBDFindMyVehicleLottieView.loop(false);
        mOBDFindMyVehicleLottieView.playAnimation();
    }

    private void updateLockStatusForIconcox(boolean isSuccessul) {
        int resId = R.raw.ic_generic_message_success;
        if(!isSuccessul) {
            resId = R.raw.ic_generic_message_error;
        }

        mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
        mOBDPnPLockButtonLottieView.setAnimation(resId);
        mOBDPnPLockButtonLottieView.setProgress(0);
        mOBDPnPLockButtonLottieView.loop(false);
        mOBDPnPLockButtonLottieView.playAnimation();
    }

    private void updateUnlockStatusForIconcox(boolean isSuccessful) {
        int resId = R.raw.ic_generic_message_success;
         if(!isSuccessful) {
            resId = R.raw.ic_generic_message_error;
        }

        mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
        mOBDPnPUnlockButtonLottieView.setAnimation(resId);
        mOBDPnPUnlockButtonLottieView.setProgress(0);
        mOBDPnPUnlockButtonLottieView.loop(false);
        mOBDPnPUnlockButtonLottieView.playAnimation();
    }

    private void makeLockVehicleApiCall() {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle == null || vehicle.getVin() == null || vehicle.getVin().isEmpty()) {
            return;
        }

        String pin = getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(pin == null || pin.isEmpty()) {
            return;
        }

        mOBDPnPLockButtonImageView.setVisibility(INVISIBLE);

        mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
        mOBDPnPLockButtonLottieView.setAnimation(R.raw.ic_wait);
        mOBDPnPLockButtonLottieView.setProgress(0);
        mOBDPnPLockButtonLottieView.loop(true);
        mOBDPnPLockButtonLottieView.playAnimation();

        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("command", IGNITION_OFF);
        bodyJsonObject.addProperty("pin", pin);
        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        Timber.d("iConcox lock api call");

        apiInterface.vehicleExecuteCommand(vehicle.getVin(), "1234", bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isLockRequested = false;

                        if(response.code() != 200 || response.body() == null) {
                            mOBDPnPLockButtonLottieView.setVisibility(INVISIBLE);
                            mOBDPnPLockButtonImageView.setVisibility(VISIBLE);

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            return;
                        }

                        try {

                            String bodyStr = response.body().string();
                            JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();

                            if(bodyJsonObject.has("data")) {

                                String dataStr = bodyJsonObject.get("data").getAsString();

                                if(dataStr == null || dataStr.isEmpty()) {
                                    mOBDPnPLockButtonLottieView.setVisibility(INVISIBLE);
                                    mOBDPnPLockButtonImageView.setVisibility(VISIBLE);

                                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);

                                } else {
                                    int resId = R.raw.ic_generic_message_success;

                                    if(!dataStr.equals("SUCCESS.")) {
                                        resId = R.raw.ic_generic_message_error;
                                    }

                                    mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
                                    mOBDPnPLockButtonLottieView.setAnimation(resId);
                                    mOBDPnPLockButtonLottieView.setProgress(0);
                                    mOBDPnPLockButtonLottieView.loop(false);
                                    mOBDPnPLockButtonLottieView.playAnimation();

                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mOBDPnPLockButtonLottieView.setVisibility(INVISIBLE);
                                            mOBDPnPLockButtonImageView.setVisibility(VISIBLE);
                                        }
                                    }, 2000);
                                }
                            }
                            //Update the vehicle live log packet
                            NetworkUtils.getInstance(getActivity()).refreshVehicleLiveLogData();
                        } catch (Exception e) {

                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isLockRequested = false;

                        int resId = R.raw.ic_generic_message_error;

                        mOBDPnPLockButtonLottieView.setVisibility(VISIBLE);
                        mOBDPnPLockButtonLottieView.setAnimation(resId);
                        mOBDPnPLockButtonLottieView.setProgress(0);
                        mOBDPnPLockButtonLottieView.loop(false);
                        mOBDPnPLockButtonLottieView.playAnimation();

                        //Update the vehicle live log packet
                        NetworkUtils.getInstance(getActivity()).refreshVehicleLiveLogData();

                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mOBDPnPLockButtonLottieView.setVisibility(INVISIBLE);
                                mOBDPnPLockButtonImageView.setVisibility(VISIBLE);
                            }
                        }, 2000);
                    }
                });
            }
        });
    }

    private void makeVehicleIgnitionOnApiCall() {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle == null || vehicle.getVin() == null || vehicle.getVin().isEmpty()) {
            return;
        }

        String pin = getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(pin == null || pin.isEmpty()) {
            return;
        }

        mOBDPnPUnlockButtonImageView.setVisibility(INVISIBLE);

        mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
        mOBDPnPUnlockButtonLottieView.setAnimation(R.raw.ic_wait);
        mOBDPnPUnlockButtonLottieView.setProgress(0);
        mOBDPnPUnlockButtonLottieView.loop(true);
        mOBDPnPUnlockButtonLottieView.playAnimation();


        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("command", IGNITION_ON);
        bodyJsonObject.addProperty("pin", pin);
        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        Timber.d("iConcox turn ignition on api call");

        apiInterface.vehicleExecuteCommand(vehicle.getVin(), "1234", bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isUnlockRequested = false;

                        if(response.code() != 200 || response.body() == null) {

                            mOBDPnPUnlockButtonLottieView.setVisibility(INVISIBLE);
                            mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            return;
                        }

                        try {

                            String bodyStr = response.body().string();

                            JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();

                            if(bodyJsonObject.has("data")) {

                                String dataStr = bodyJsonObject.get("data").getAsString();

                                if(dataStr == null || dataStr.isEmpty()) {
                                    mOBDPnPUnlockButtonLottieView.setVisibility(INVISIBLE);
                                    mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);

                                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else {

                                    int resId = R.raw.ic_generic_message_success;

                                    if(!dataStr.equals("SUCCESS.")) {
                                        resId = R.raw.ic_generic_message_error;
                                    }

                                    mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
                                    mOBDPnPUnlockButtonLottieView.setAnimation(resId);
                                    mOBDPnPUnlockButtonLottieView.setProgress(0);
                                    mOBDPnPUnlockButtonLottieView.loop(false);
                                    mOBDPnPUnlockButtonLottieView.playAnimation();

                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mOBDPnPUnlockButtonLottieView.setVisibility(INVISIBLE);
                                            mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);
                                        }
                                    }, 2000);
                                }
                            }

                            //Update the vehicle live log packet
                            NetworkUtils.getInstance(getActivity()).refreshVehicleLiveLogData();

                        } catch (Exception e) {

                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isUnlockRequested = false;

                        int resId = R.raw.ic_generic_message_error;

                        mOBDPnPUnlockButtonLottieView.setVisibility(VISIBLE);
                        mOBDPnPUnlockButtonLottieView.setAnimation(resId);
                        mOBDPnPUnlockButtonLottieView.setProgress(0);
                        mOBDPnPUnlockButtonLottieView.loop(false);
                        mOBDPnPUnlockButtonLottieView.playAnimation();

                        //Update the vehicle live log packet
                        NetworkUtils.getInstance(getActivity()).refreshVehicleLiveLogData();

                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mOBDPnPUnlockButtonLottieView.setVisibility(INVISIBLE);
                                mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);
                            }
                        }, 2000);
                    }
                });
            }
        });
    }

    private void makeFindVehicleApiCall() {
        if(getActivity() == null) {
            return;
        }

        String userToken = getActivity().getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String bearerToken = "Bearer " + userToken;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle == null || vehicle.getVin() == null || vehicle.getVin().isEmpty()) {
            return;
        }

        String pin = getActivity().getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(pin == null || pin.isEmpty()) {
            return;
        }

        mOBDFindMyVehicleImageView.setVisibility(INVISIBLE);

        mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
        mOBDFindMyVehicleLottieView.setAnimation(R.raw.ic_wait);
        mOBDFindMyVehicleLottieView.setProgress(0);
        mOBDFindMyVehicleLottieView.loop(true);
        mOBDFindMyVehicleLottieView.playAnimation();


        JsonObject bodyJsonObject = new JsonObject();
        bodyJsonObject.addProperty("command", FIND);
        bodyJsonObject.addProperty("pin", pin);
        String bodyStr = new Gson().toJson(bodyJsonObject);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

        Timber.d("iconcox find api call");

        apiInterface.vehicleExecuteCommand(vehicle.getVin(), "1234", bearerToken, requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isFindVehicleRequested = false;

                        if(response.code() != 200 || response.body() == null) {

                            mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
                            mOBDFindMyVehicleLottieView.setAnimation(R.raw.ic_generic_message_error);
                            mOBDFindMyVehicleLottieView.setProgress(0);
                            mOBDFindMyVehicleLottieView.loop(false);
                            mOBDFindMyVehicleLottieView.playAnimation();

                            return;
                        }

                        try {

                            String bodyStr = response.body().string();
                            JsonObject bodyJsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();

                            if(bodyJsonObject.has("data")) {

                                String dataStr = bodyJsonObject.get("data").getAsString();

                                int resId = R.raw.ic_generic_message_success;

                                if(!dataStr.equals("SUCCESS.")) {
                                    resId = R.raw.ic_generic_message_error;
                                }

                                mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
                                mOBDFindMyVehicleLottieView.setAnimation(resId);
                                mOBDFindMyVehicleLottieView.setProgress(0);
                                mOBDFindMyVehicleLottieView.loop(false);
                                mOBDFindMyVehicleLottieView.playAnimation();

                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        mOBDFindMyVehicleLottieView.setVisibility(INVISIBLE);
                                        mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
                                    }
                                }, 2000);
                            }
                        } catch (Exception e) {

                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isFindVehicleRequested = false;

                        int resId = R.raw.ic_generic_message_error;

                        mOBDFindMyVehicleLottieView.setVisibility(VISIBLE);
                        mOBDFindMyVehicleLottieView.setAnimation(resId);
                        mOBDFindMyVehicleLottieView.setProgress(0);
                        mOBDFindMyVehicleLottieView.loop(false);
                        mOBDFindMyVehicleLottieView.playAnimation();

                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mOBDFindMyVehicleLottieView.setVisibility(INVISIBLE);
                                mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
                            }
                        }, 2000);
                    }
                });
            }
        });
    }

    private void findVehicleIfConnected() {
        if(getActivity() == null) {
            return;
        }

        if(NordicBleService.getConnectionState() == STATE_CONNECTED) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND));
        } else {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_not_connected));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_NONE);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

        }

    }

    private String constructLockControlString(boolean lockVehicle) {
        if(getActivity() == null) {
            return null;
        }

        String commandArrayListStr = getActivity().getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_LAST_READ_VEHICLE_CONTROL_PARAMS_KEY, null);
        if(commandArrayListStr == null) {
            return null;
        }

        ArrayList<Byte> commandArrayList = new Gson().fromJson(commandArrayListStr, new TypeToken<ArrayList<Byte>>(){}.getType());

        commandArrayList.set(2, lockVehicle ? (byte)8 : (byte)0);

        String commandString = "";

        byte[] commandByteArray = new byte[commandArrayList.size()];

        for(int byteIndex = 0; byteIndex < commandArrayList.size(); ++byteIndex) {
            commandByteArray[byteIndex] = commandArrayList.get(byteIndex);
        }

        commandString = BleUtils.ByteArraytoHex(commandByteArray);

        /*for(int byteIndex = 0; byteIndex < commandArrayList.size(); ++byteIndex) {
            //todo::optimize this very little bit later
            commandString += Integer.toHexString(commandArrayList.get(byteIndex));
            if(byteIndex != (commandArrayList.size() - 1)) {
                commandString += " ";
            }

        }*/
        return commandString;
    }

    private void setupBluetoothPairingRelatedViews() {
        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        //we are connected to bluetooth
        if(getActivity() != null && NordicBleService.getConnectionState() == STATE_CONNECTED
                || (vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP)) {
            return;
        }

        mConnectToPreviousVehicleButton = (Button)rootView.findViewById(R.id.obd_connect_to_previous_vehicle_button);
        mConnectingToDeviceCircleProgressView = (CircleProgressView) rootView.findViewById(R.id.obd_connecting_to_device_circle_progress_view);
        mScanQRCodeButton = (Button) rootView.findViewById(R.id.scan_qr_code_button);
        mOBDConnectingToVehicleTextView = (TextView) rootView.findViewById(R.id.obd_connecting_to_vehicle_text_view);

        mOBDConnectUsingVINButton = rootView.findViewById(R.id.obd_connect_using_vin_button);

        mCancelButton = (Button) rootView.findViewById(R.id.obd_cancel_button);

        //check if any last saved vehicle found
        if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {
            showSwitchOnBluetoothMessage();
        } else {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
            String lastConnectedDeviceMacAddress = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null);
            String lastConnectedDeviceName = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null);

            Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

            if (lastConnectedDeviceMacAddress == null || lastConnectedDeviceName == null || device == null) {
                mConnectToPreviousVehicleButton.setVisibility(GONE);
            } else {
                mScanQRCodeButton.setBackground(getActivity().getDrawable(R.drawable.capsule_button_background_grey));
                mScanQRCodeButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.revos_pitch_black));
                mConnectToPreviousVehicleButton.setVisibility(VISIBLE);
            }
        }

        mConnectToPreviousVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }
                if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {
                    showSwitchOnBluetoothMessage();
                } else {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE));
                }
            }
        });

        mScanQRCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BluetoothAdapter.getDefaultAdapter() == null || BluetoothAdapter.getDefaultAdapter().getState() != BluetoothAdapter.STATE_ON) {
                    showSwitchOnBluetoothMessage();
                } else {

                    mScanQRCodeButton.setEnabled(false);
                    startZXingActivity();
                }
            }
        });

        mOBDConnectUsingVINButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), ConnectUsingVinActivity.class);
                startActivity(myIntent);
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CANCEL_PAIRING));
            }
        });

    }

    private void showSwitchOnBluetoothMessage() {
        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.please_switch_on_bluetooth));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, STATUS_INFO);
        bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void startZXingActivity() {
        if(getActivity() == null) {
            return;
        }
        //check for runtime permission
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
            return;
        }

        Intent ZXingActivityIntent = new Intent(getActivity(), ZXingActivity.class);
        startActivityForResult(ZXingActivityIntent, ZXING_REQUEST_CODE);
    }

    private void clearVehicleAndDeviceRelatedPrefs() {
        if(mActivityContext == null) {
            return;
        }
        mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
        mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

        mActivityContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY,  null).apply();
        mActivityContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_JSON_DATA_KEY,  null).apply();

        mActivityContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().clear().apply();
        mActivityContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().clear().apply();

        mActivityContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

    }

    private void fetchDeviceDetailsAndConnect(String deviceId) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if (apolloClient == null) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.error));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        DeviceWhereUniqueInput deviceWhereUniqueInput = DeviceWhereUniqueInput
                                                        .builder()
                                                        .deviceId(deviceId)
                                                        .build();

        FetchDeviceQuery fetchDeviceQuery = FetchDeviceQuery
                                            .builder()
                                            .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
                                            .build();

        if(mActivityContext != null) {
            mActivityContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().putBoolean(IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY, true).apply();
        }

        if (mActivityContext != null) {
            ((MainActivity)mActivityContext).showProgressDialog();
        }

        apolloClient.query(fetchDeviceQuery).enqueue(new ApolloCall.Callback<FetchDeviceQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchDeviceQuery.Data> response) {
                ((Activity) mActivityContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ((MainActivity)mActivityContext).hideProgressDialog();

                        mActivityContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().putBoolean(IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false).apply();

                        if(response.getErrors() != null && !response. getErrors().isEmpty()) {
                            EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.unable_to_fetch_device_details)));
                        } else if(response.getData() != null && response.getData().device() != null && response.getData().device().get() !=null ) {

                            Device device = PrefUtils.getInstance(mActivityContext).saveDeviceVariablesInPrefs(response.getData().device().get());

                            if(device == null) {
                                return;
                            }

                            if(device.getStatus() == DeviceStatus.INITIALIZED && device.getVehicle() != null && device.getVehicle().getStatus() != VehicleStatus.SOLD) {
                                new Timer().schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.vehicle_not_marked_sold)));
                                    }
                                },1000);

                                clearVehicleAndDeviceRelatedPrefs();
                                return;
                            }

                            if(device.getStatus() == DeviceStatus.ACTIVATED && device.getVehicle() != null && device.getVehicle().getStatus() != VehicleStatus.SOLD) {

                                if(device.getVehicle().getStatus().equals(VehicleStatus.RENTAL)) {

                                    String activeBookingPrefString = mActivityContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY, null);

                                    if(activeBookingPrefString == null) {

                                        new Timer().schedule(new TimerTask() {
                                            @Override
                                            public void run() {
                                                EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.vehicle_marked_for_rental_go_to_rental)));
                                            }
                                        },1000);

                                        clearVehicleAndDeviceRelatedPrefs();
                                        return;
                                    }

                                    RentalVehicle rentalVehicle = new Gson().fromJson(activeBookingPrefString, RentalVehicle.class);

                                    if(rentalVehicle != null && rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {

                                        if(!rentalVehicle.getVin().equals(device.getVehicle().getVin())) {

                                            clearVehicleAndDeviceRelatedPrefs();

                                            new Timer().schedule(new TimerTask() {
                                                @Override
                                                public void run() {
                                                    EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.vehicle_marked_for_rental_go_to_rental)));
                                                }
                                            },1000);

                                            return;
                                        }
                                    } else {
                                        clearVehicleAndDeviceRelatedPrefs();
                                        //invalid data return;
                                        return;
                                    }
                                } else {

                                    new Timer().schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_INFO_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.vehicle_not_marked_sold)));
                                        }
                                    },1000);
                                }
                            }

                            //clear previous PIN if any
                            mActivityContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, null).apply();

                            //save device pin if it's not null
                            if(response.getData().device().get().pin() != null) {
                                mActivityContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, response.getData().device().get().pin()).apply();
                            } else {
                                mActivityContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, null).apply();
                            }

                            NordicBleService.setDeviceName(null);

                            MetadataDecryptionHelper.getInstance(mActivityContext).clearLocalCache();

                            //clear previous metadata
                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null).apply();
                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null).apply();
                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null).apply();
                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null).apply();
                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null).apply();


                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null).apply();
                            mActivityContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null).apply();

                            //clear previous tracking data
                            mActivityContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().clear().apply();

                            //refresh live log and geo fence data
                            NetworkUtils.getInstance(mActivityContext).refreshVehicleLiveLogData();
                            NetworkUtils.getInstance(mActivityContext).refreshGeoFenceData();

                            if(device.getVehicle() != null && device.getVehicle().getModel() != null && device.getVehicle().getModel().getProtocol() != null
                                    && device.getVehicle().getModel().getProtocol() == ModelProtocol.PNP) {

                                //send request to connect to tbit, if device is tbit
                                if(device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.TBIT) {
                                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TBIT));
                                } else if( device != null && device.getDeviceType() != null && device.getDeviceType() == DeviceType.ICONCOX) {
                                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_ICONCOX));
                                }

                                ((MainActivity)mActivityContext).refreshFragments();
                            } else {
                                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_REQUEST_CONNECT_TO_DEVICE + GEN_DELIMITER + device.getMacId()));
                            }
                        } else {
                            EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.device_details_null)));
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(mActivityContext != null) {
                    mActivityContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().putBoolean(IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false).apply();
                }

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity)mActivityContext).hideProgressDialog();
                    }
                });

                EventBus.getDefault().post(new EventBusMessage(GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT + GEN_DELIMITER + mActivityContext.getString(R.string.server_error)));
            }
        });
    }

    private void setupNavigationAdviceRelatedViews() {

        mETALabelTextView = (TextView)rootView.findViewById(R.id.obd_eta_label_text_view);
        mETAValueTextView = (TextView)rootView.findViewById(R.id.obd_eta_value_text_view);
    }

    private void updateSpeedRelatedViews(int speed, boolean isParking, int mode, boolean isReverse) {
        if(getActivity() == null) {
            return;
        }
        int speedInKMPH = speed;
        Timber.d("%s", speedInKMPH);
        if(mSpeedValueTextView != null && mSpeedCircleProgressView != null) {
            if(!isParking) {
                if(speedInKMPH == -1) {
                    mSpeedValueTextView.setText("--");
                } else {
                    mSpeedValueTextView.setText(String.valueOf(speedInKMPH));
                }
                mOBDModeValueTextView.setVisibility(VISIBLE);
            } else {
                mSpeedValueTextView.setText("P");
                mOBDModeValueTextView.setVisibility(INVISIBLE);
            }
            mSpeedCircleProgressView.setValueAnimated(speedInKMPH, 100);
        }

        if(mOBDModeValueTextView != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getActivity().getTheme();
            theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
            @ColorInt int textColor = typedValue.data;
            if(isReverse) {
                theme.resolveAttribute(R.attr.colorBatteryLow, typedValue, true);
                textColor = typedValue.data;
                mOBDModeValueTextView.setTextColor(textColor);
                mOBDModeValueTextView.setText("R");
            } else {
                theme.resolveAttribute(R.attr.colorTextOBD, typedValue, true);
                textColor = typedValue.data;
                mOBDModeValueTextView.setTextColor(textColor);
                switch (mode) {
                    case 0 : if(Locale.getDefault().getLanguage().equals("en")) {
                                mOBDModeValueTextView.setText(getString(R.string.vehicle_mode_economy));
                             } else {
                                mOBDModeValueTextView.setText(String.format("MODE %s", String.valueOf(mode+1)));
                             }
                             break;

                    case 1 : if(Locale.getDefault().getLanguage().equals("en")) {
                                mOBDModeValueTextView.setText(getString(R.string.vehicle_mode_ride));
                             } else {
                                mOBDModeValueTextView.setText(String.format("MODE %s", String.valueOf(mode+1)));
                             }
                                break;

                    case 2 : if(Locale.getDefault().getLanguage().equals("en")) {
                                mOBDModeValueTextView.setText(getString(R.string.vehicle_mode_sport));
                             } else {
                                mOBDModeValueTextView.setText(String.format("MODE %s", String.valueOf(mode+1)));
                             }
                                break;
                }
            }
        }
    }

    private void updateNavAdviceViews() {
        if(getActivity() == null) {
            return;
        }

        //we are not connected to bluetooth
        if(NordicBleService.getConnectionState() != STATE_CONNECTED
            || mETALabelTextView == null
            || mETAValueTextView == null
            || mLeftTurnDirectionProgressBar == null) {
            return;
        }

        HashMap<String, Object> tripAdviceDataHashMap = null;
        String tripDataStr = getActivity().getSharedPreferences(NAV_PREFS, MODE_PRIVATE).getString(TRIP_ADVICE_DATA_KEY, null);
        if(tripDataStr != null) {
            Type hashMapType = new TypeToken<HashMap<String, Object>>(){}.getType();
            tripAdviceDataHashMap = new Gson().fromJson(tripDataStr, hashMapType);
        } else {
            return;
        }

        //update the eta
        if(tripAdviceDataHashMap.containsKey(TRIP_ADVICE_ETA_KEY)) {
            mETAValueTextView.setText((String)tripAdviceDataHashMap.get(TRIP_ADVICE_ETA_KEY));
        }

    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if(event.message.equals(NAV_EVENT_TRIP_ADVICE_DATA_UPDATED)) {

            Timber.d("OBD: event NAV_EVENT_TRIP_ADVICE_DATA_UPDATED");

            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateNavAdviceViews();
                }
            });
        } else if(event.message.contains(OBD_EVENT_LIVE_DATA) && NordicBleService.getConnectionState() == STATE_CONNECTED) {

            Timber.d("OBD: event OBD_EVENT_LIVE_DATA && STATE_CONNECTED");

            mIsVehicleLocked = false;
            final String dataString = event.message.split(GEN_DELIMITER)[1];
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        togglePowerButtonVisibility();
                        togglePowerButtonBackground(dataString);
                        parseLiveDataAndUpdateOBDFragment(dataString);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });

        } else if(event.message.equals(BT_EVENT_STATE_DISCONNECTED)) {
            if(getActivity() == null) {
                return;
            }
            Timber.d("OBD: event BT_EVENT_STATE_DISCONNECTED");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        updateConnectToVehicleLayout();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(BT_EVENT_STATE_CONNECTING)) {
            if(getActivity() == null) {
                return;
            }

            Timber.d("OBD: event BT_EVENT_STATE_CONNECTING");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        updateConnectToVehicleLayout();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(BT_EVENT_SCAN_STARTED)) {
            if(getActivity() == null) {
                return;
            }

            Timber.d("OBD: event BT_EVENT_SCAN_STARTED");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        updateConnectToVehicleLayout();
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.equals(BT_EVENT_DEVICE_NOT_FOUND)) {
            if(getActivity() == null) {
                return;
            }

            Timber.d("OBD: event BT_EVENT_DEVICE_NOT_FOUND");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_not_found));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }

                }
            });
        } else if(event.message.contains(BT_EVENT_DEVICE_LOCKED)) {

            Timber.d("OBD: event BT_EVENT_DEVICE_LOCKED");

            mIsVehicleLocked = true;
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getActivity() == null || !getActivity().hasWindowFocus()) {
                            return;
                        }
                        toggleTabLayoutIfRequired(null);
                        updateViewsOnLockedState();
                        hideOBDErrorLayout();
                        togglePowerButtonVisibility();
                        togglePowerButtonBackground(event.message.split(GEN_DELIMITER)[1]);
                    } catch (Exception e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                }
            });
        } else if(event.message.contains(BT_GPS_ICONCOX_CMD_RESPONSE)) {

            String command = event.message.split(GEN_DELIMITER)[1];
            String responseStatus = event.message.split(GEN_DELIMITER)[2];
            boolean isSuccess = Boolean.parseBoolean(responseStatus);

            if(command.equals(CMD_SEND_REMOTE_LOCK)) {
                if(isSuccess) {
                    isLockRequested = false;
                    Timber.d("iConcox BLE lock success");
                    updateLockStatusForIconcox(isSuccess);
                } else {
                    makeLockVehicleApiCall();
                }
            } else if(command.equals(CMD_SEND_REMOTE_IGNITION)) {
                if(isSuccess) {
                    isUnlockRequested = false;
                    Timber.d("iConcox BLE ignition success");
                    updateUnlockStatusForIconcox(isSuccess);
                } else {
                    makeVehicleIgnitionOnApiCall();
                }
            } else if(command.equals(CMD_SEND_FIND_VEHICLE)) {
                if(isSuccess) {
                    isFindVehicleRequested = false;
                    Timber.d("iConcox BLE find success");
                    updateFindVehicleStatusForIconcox(isSuccess);
                } else {
                    makeFindVehicleApiCall();
                }
            }
        }
    }

    private void togglePowerButtonBackground(String dataString) {
        if(getActivity() == null || mPowerButtonImageView == null) {
            return;
        }
        mKeylessSwitchStatus = KEYLESS_SWITCH_DEFAULT;

        if(dataString.length() == 8 && dataString.contains("LOCKED")) {
            String keylessSwitchStatusString = dataString.split(",")[1];
            mKeylessSwitchStatus = (byte) keylessSwitchStatusString.charAt(0);

            if (mKeylessSwitchStatus == KEYLESS_SWITCH_DEFAULT) {
                mPowerButtonImageView.setVisibility(INVISIBLE);
            } else if (mKeylessSwitchStatus == KEYLESS_SWITCH_DISABLED) {
                mPowerButtonImageView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.power_on_background));
            } else if (mKeylessSwitchStatus == KEYLESS_SWITCH_ENABLED) {
                mPowerButtonImageView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.power_off_background));
            }
        } else {
            mKeylessSwitchStatus = KEYLESS_SWITCH_ENABLED;
            mPowerButtonImageView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.power_off_background));
        }
    }

    private void togglePowerButtonVisibility() {
        if(getActivity() == null || mPowerButtonImageView == null) {
            return;
        }

        Device cloudDevice = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();
        if(cloudDevice != null) {
            if(cloudDevice.getVehicle().getModel().getModelAccessType().equals(ModelAccessType.KEY)) {
                mPowerButtonImageView.setVisibility(INVISIBLE);
            } else {
                mPowerButtonImageView.setVisibility(VISIBLE);
            }
        }
    }

    private void updateConnectToVehicleLayout() {
        if(NordicBleService.getConnectionState() == STATE_CONNECTED ||
                mConnectingToDeviceCircleProgressView == null ||
                mScanQRCodeButton == null ||
                mConnectToPreviousVehicleButton == null ||
                getActivity() == null) {
            return;
        }

        int spinnerVisibility = NordicBleService.getConnectionState() == STATE_CONNECTING ? VISIBLE : GONE;

        String generatedText = "";
        if(NordicBleService.getDeviceName() != null && !NordicBleService.getDeviceName().isEmpty()) {
            generatedText = String.format(getResources().getString(R.string.connecting_to_vehicle_dynamic), NordicBleService.getDeviceName());
        } else {
            generatedText = getString(R.string.connecting_to_vehicle);
        }

        //set spinner visibility as true in case we are searching for device
        if(NordicBleService.mIsScanning && NordicBleService.getConnectionState() != STATE_CONNECTING) {
            spinnerVisibility = VISIBLE;
            generatedText = getResources().getString(R.string.searching_for_vehicle);
        }

        //set the vehicle name text
        mOBDConnectingToVehicleTextView.setText(generatedText);
        //set the visibility for the trying to connect to vehicle text view
        mOBDConnectingToVehicleTextView.setVisibility(spinnerVisibility);

        //set the visibility for the spinner
        mConnectingToDeviceCircleProgressView.setVisibility(spinnerVisibility);
        mCancelButton.setVisibility(spinnerVisibility);

        if(spinnerVisibility == VISIBLE) {
            mConnectToPreviousVehicleButton.setVisibility(GONE);
            mScanQRCodeButton.setVisibility(GONE);
            mOBDConnectUsingVINButton.setVisibility(GONE);
            mConnectingToDeviceCircleProgressView.spin();

        } else {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE);
            String lastConnectedDeviceMacAddress = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY, null);
            String lastConnectedDeviceName = sharedPreferences.getString(BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY, null);

            Device device = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

            if (lastConnectedDeviceMacAddress == null || lastConnectedDeviceName == null || device == null) {
                mConnectToPreviousVehicleButton.setVisibility(GONE);
            } else {
                mScanQRCodeButton.setBackground(getActivity().getDrawable(R.drawable.capsule_button_background_grey));
                mScanQRCodeButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.revos_pitch_black));
                mConnectToPreviousVehicleButton.setVisibility(VISIBLE);
            }

            showHideScanManuallyView();
            mScanQRCodeButton.setVisibility(VISIBLE);
            mConnectingToDeviceCircleProgressView.stopSpinning();
        }
    }


    private void showUnlockedStateViews() {
        if(
        mDisconnectImageView == null ||
        mSpeedCircleProgressView == null ||
        mOBDModeValueTextView == null ||
        mSpeedValueTextView == null ||
        mOBDOdoValueTextView == null ||
        mOBDOdoLabelTextView == null ||
        mOBDOdoUnitTextView == null ||
        mOBDBatteryLabelTextView == null ||
        mOBDBatteryImageView == null ||
        mOBDRangeTextView == null ||
        mOBDRangeUnitTextView == null ||
        mOBDHeadlightImageView == null ||
        mOBDBrakeImageView == null ||
        mOBDFindMyVehicleImageView == null ||
        mOBDVehicleLockedTrueImageView == null) {
            return;
        }

        mDisconnectImageView.setVisibility(VISIBLE);
        mSpeedCircleProgressView.setVisibility(VISIBLE);
        mOBDModeValueTextView.setVisibility(VISIBLE);
        mSpeedValueTextView.setVisibility(VISIBLE);
        mOBDOdoValueTextView.setVisibility(VISIBLE);
        mOBDOdoLabelTextView.setVisibility(VISIBLE);
        mOBDOdoUnitTextView.setVisibility(VISIBLE);
        mOBDBatteryLabelTextView.setVisibility(VISIBLE);
        mOBDBatteryImageView.setVisibility(VISIBLE);
        mOBDRangeTextView.setVisibility(VISIBLE);
        mOBDRangeUnitTextView.setVisibility(VISIBLE);
        mOBDHeadlightImageView.setVisibility(VISIBLE);
        mOBDBrakeImageView.setVisibility(VISIBLE);
        mOBDFindMyVehicleImageView.setVisibility(VISIBLE);

        mOBDVehicleLockedTrueImageView.setVisibility(INVISIBLE);
    }

    private void parseLiveDataAndUpdateOBDFragment(String dataString) {
        if(NordicBleService.getConnectionState() != STATE_CONNECTED) {
            return;
        }
        if(showGeoFenceErrorIfRequired()) {
            return;
        }

        //todo::the actual order of the bytes is MSB followed LSB, hence the variable values have been switched while calculation

        String[] dataArray = dataString.split(" ");

        boolean allZero = true;
        //skip the last byte
        for(int index = 0; index < dataArray.length - 2 && allZero; ++index) {
            if(!dataArray[index].equals("00"))
            {
                allZero = false;
                break;
            }
        }

        if(dataArray[0].equals("4C") && dataArray[1].equals("4F") && dataArray[2].equals("43") && dataArray[3].equals("4B")) {
            //vehicle is locked, return
            return;
        }

        if(allZero) {
            return;
        }

        //make obd layouts visible
        showUnlockedStateViews();

        VehicleParameters vehicleParameters = NordicBleService.mVehicleLiveParameters;

        if(vehicleParameters == null) {
            return;
        }

        int vehicleSpeed = (int) Math.floor(vehicleParameters.getRpm() / mSpeedDivisor);

        toggleSpeedSensitiveViews(vehicleParameters, vehicleSpeed);

        updateSpeedRelatedViews(vehicleSpeed, vehicleParameters.isParking(), vehicleParameters.getMode(), vehicleParameters.isReverse());

        toggleBrakeAndHeadLightView(vehicleParameters);

        updateOdoRelatedViews(vehicleParameters);

        toggleIndicatorViews(vehicleParameters);


        /*if (mVehicleLockStatus != vehicleParameters.isLocked()) {
            mVehicleLockStatus = vehicleParameters.isLocked();
            updateLockIcon();
        }*/

        if(mOBDBatteryImageView != null && mOBDBatteryLabelTextView != null && mOBDRangeTextView != null && mOBDRangeUnitTextView != null) {

            mOBDBatteryImageView.setVisibility(INVISIBLE);
            mOBDBatteryLabelTextView.setVisibility(INVISIBLE);
            mOBDRangeTextView.setVisibility(INVISIBLE);
            mOBDRangeUnitTextView.setVisibility(INVISIBLE);


            Device cloudDevice = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

            if(cloudDevice != null) {

                double batteryMinVoltage = cloudDevice.getVehicle().getModel().getBatteryMinVoltageLimit();
                double batteryMaxVoltage = cloudDevice.getVehicle().getModel().getBatteryMaxVoltageLimit();

                if(batteryMinVoltage > 0 && batteryMaxVoltage > 0) {
                    mOBDBatteryImageView.setVisibility(VISIBLE);
                    mOBDBatteryLabelTextView.setVisibility(VISIBLE);

                    double batteryPercentage = ((vehicleParameters.getVoltage() - batteryMinVoltage) / (batteryMaxVoltage - batteryMinVoltage)) * 100;

                    if(batteryPercentage < 0) {
                        batteryPercentage = 0;
                    }

                    if(batteryPercentage > 100) {
                        batteryPercentage = 100;
                    }

                    if(vehicleParameters.getVoltage() <= (batteryMinVoltage * 0.5)) {
                        showOBDErrorLayout(getString(R.string.battery_removed_error));
                    }

                    if(vehicleParameters.getSoc() == -1) {
                        updateBatteryLayout(Math.round(batteryPercentage), vehicleParameters.getVoltage(), false);
                    } else {
                        updateBatteryLayout(vehicleParameters.getSoc(), vehicleParameters.getVoltage(), true);
                    }
                }
            }
            //int socValue = Integer.parseInt(dataArray[SOC], 16);
        }

        float temperature = vehicleParameters.getTemperature();

            String errorString = "";
            boolean showErrorLayout = false;
            if (temperature > OVER_TEMPERATURE_LIMIT) {
                showErrorLayout = true;
                errorString = getString(R.string.over_temperature);
            }

        if (vehicleParameters.isOverCurrent()) {
            showErrorLayout = true;
            errorString = getString(R.string.over_current);
        }

        if (vehicleParameters.isThrottleError()) {
            showErrorLayout = true;
            errorString = getString(R.string.throttle_error);
        }

        if (vehicleParameters.isMotorError()) {
            showErrorLayout = true;
            errorString = getString(R.string.motor_error);
        }

        if (vehicleParameters.isControllerError()) {
            showErrorLayout = true;
            errorString = getString(R.string.controller_error);
        }

        if (showErrorLayout) {
            showOBDErrorLayout(errorString);
        } else {
            hideOBDErrorLayout();
        }

        //int rangeValue = Integer.parseInt(dataArray[DTE], 16);
        //updateRange(rangeValue);

        /*if(isHeadlightOn) {
            mOBDHeadlightImageView.setVisibility(View.VISIBLE);
        } else {
            mOBDHeadlightImageView.setVisibility(View.INVISIBLE);
        }*/
    }

    private void toggleIndicatorViews(VehicleParameters vehicleParameters) {
        if(mLeftTurnDirectionProgressBar == null || mRightTurnDirectionProgressBar == null) {
            return;
        }

        if(vehicleParameters.isLeftIndicatorOn()) {
            mLeftTurnDirectionProgressBar.drawDirectionalArc(ARROW_LEFT);
        } else {
            mLeftTurnDirectionProgressBar.setProgress(0);
        }

        if(vehicleParameters.isRightIndicatorOn()) {
            mRightTurnDirectionProgressBar.drawDirectionalArc(ARROW_RIGHT);
        } else {
            mRightTurnDirectionProgressBar.setProgress(0);
        }
    }

    private void toggleSpeedSensitiveViews(VehicleParameters vehicleParameters, int vehicleSpeed) {
        if(getActivity() == null) {
            return;
        }

        toggleTabLayoutIfRequired(vehicleParameters);

        if(mOBDFindMyVehicleImageView != null) {
            if(vehicleSpeed > 0) {
                mOBDFindMyVehicleImageView.setVisibility(INVISIBLE);
            } else {
                mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
            }
        }

        if(mDisconnectImageView != null) {
            if(vehicleSpeed > 0) {
                mDisconnectImageView.setVisibility(INVISIBLE);
            } else {
                mDisconnectImageView.setVisibility(VISIBLE);
            }
        }

        Device cloudDevice = PrefUtils.getInstance(getActivity()).getDeviceObjectFromPrefs();

        if(cloudDevice != null) {

            if(mPowerButtonImageView != null) {
                if(vehicleSpeed > 0 || cloudDevice.getVehicle().getModel().getModelAccessType() == ModelAccessType.KEY) {
                    mPowerButtonImageView.setVisibility(INVISIBLE);
                } else {
                    mPowerButtonImageView.setVisibility(VISIBLE);
                }
            }
        }

    }

    private void toggleTabLayoutIfRequired(@Nullable VehicleParameters vehicleParameters) {
        try {
            if (getActivity() == null || !getActivity().hasWindowFocus()) {
                return;
            }

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE);
            boolean isSettingTurnedOn = sharedPreferences.getBoolean(DISTRACTION_FREE_MODE_KEY, true);

            float SPEED_LIMIT = 9;

            int speed = (vehicleParameters == null) ? 0 : (int) Math.floor(vehicleParameters.getRpm() / mSpeedDivisor);

            boolean isSpeedAboveLimit = (speed > SPEED_LIMIT);

            ((MainActivity) getActivity()).toggleTabLayout(isSettingTurnedOn && isSpeedAboveLimit && NordicBleService.getConnectionState() == STATE_CONNECTED);
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void updateOdoRelatedViews(VehicleParameters vehicleParameters) {
        if(mOBDOdoValueTextView == null || mOBDOdoUnitTextView == null) {
            return;
        }
        double odoActual = vehicleParameters.getOdo();

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);

        String odoString = odoActual < 1000 ? String.valueOf(odoActual) : df.format(odoActual / 1000);
        String odoUnit = odoActual < 1000 ? getString(R.string.meters) : getString(R.string.kilometers);

        mOBDOdoValueTextView.setText(odoString);
        mOBDOdoUnitTextView.setText(odoUnit);
    }

    private void toggleBrakeAndHeadLightView(VehicleParameters vehicleParameters) {
        if(mOBDBrakeImageView == null || mOBDHeadlightImageView == null) {
            return;
        }
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            mOBDBrakeImageView.setVisibility(INVISIBLE);
            mOBDHeadlightImageView.setVisibility(INVISIBLE);
            return;
        }

        if(vehicleParameters == null) {
            mOBDBrakeImageView.setVisibility(INVISIBLE);
            mOBDHeadlightImageView.setVisibility(INVISIBLE);
            return;
        }
        if (vehicleParameters.isEABSEngaged() || vehicleParameters.isBraking()) {
            mOBDBrakeImageView.setVisibility(VISIBLE);
        } else {
            mOBDBrakeImageView.setVisibility(INVISIBLE);
        }

        if (vehicleParameters.isHeadLampOn()) {
            mOBDHeadlightImageView.setVisibility(VISIBLE);
        } else {
            mOBDHeadlightImageView.setVisibility(INVISIBLE);
        }
    }

    private void showOBDErrorLayout(String obdErrorString) {

        if(mOBDErrorCodeTextView == null || mOBDErrorCodeImageView == null || mOBDErrorCodeCircleProgressView == null) {
            return;
        }

        mOBDErrorCodeTextView.setText(obdErrorString);

        mOBDErrorCodeTextView.setVisibility(VISIBLE);
        mOBDErrorCodeCircleProgressView.setVisibility(VISIBLE);
        mOBDErrorCodeImageView.setVisibility(VISIBLE);
    }

    private void hideOBDErrorLayout() {
        if(mOBDErrorCodeTextView == null || mOBDErrorCodeImageView == null || mOBDErrorCodeCircleProgressView == null) {
            return;
        }
        mOBDErrorCodeTextView.setVisibility(INVISIBLE);
        mOBDErrorCodeCircleProgressView.setVisibility(INVISIBLE);
        mOBDErrorCodeImageView.setVisibility(INVISIBLE);
    }

    private void updateViewsOnLockedState() {

        if(getActivity() == null) {
            return;
        }

        mSpeedCircleProgressView.setVisibility(INVISIBLE);
        mLeftTurnDirectionProgressBar.setProgress(0);
        mRightTurnDirectionProgressBar.setProgress(0);
        mOBDModeValueTextView.setVisibility(INVISIBLE);
        mSpeedValueTextView.setVisibility(INVISIBLE);
        mOBDOdoValueTextView.setVisibility(INVISIBLE);
        mOBDOdoLabelTextView.setVisibility(INVISIBLE);
        mOBDOdoUnitTextView.setVisibility(INVISIBLE);
        mOBDBatteryLabelTextView.setVisibility(INVISIBLE);
        mOBDBatteryImageView.setVisibility(INVISIBLE);
        mOBDRangeTextView.setVisibility(INVISIBLE);
        mOBDRangeUnitTextView.setVisibility(INVISIBLE);
        mOBDHeadlightImageView.setVisibility(INVISIBLE);
        mOBDBrakeImageView.setVisibility(INVISIBLE);
        mOBDFindMyVehicleImageView.setVisibility(VISIBLE);
        mDisconnectImageView.setVisibility(VISIBLE);
        mOBDVehicleLockedTrueImageView.setVisibility(VISIBLE);
    }

    private void updateRange(int value) {
        mOBDRangeTextView.setText(String.valueOf(value));
    }

    private void updateBatteryLayout(double socValue, double batteryVoltage, boolean showSOCPercentage) {
        if(getActivity() == null) {
            return;
        }

        String socValueString = "", batteryVoltageString = "";

        if(socValue < 0) {
            socValueString = String.valueOf(0);
        } else if(socValue > 100) {
            socValueString = String.valueOf(100);
        } else {
            socValueString = String.valueOf(Math.round(Math.floor(socValue)));
        }

        if(batteryVoltage < 0) {
            batteryVoltageString = String.valueOf(0);
        } else if(batteryVoltage > 100) {
            batteryVoltageString = String.valueOf(100);
        } else {
            batteryVoltageString = String.valueOf(Math.round(Math.floor(batteryVoltage)));
        }

        if(showSOCPercentage) {
            mOBDBatteryLabelTextView.setText(getString(R.string.battery).concat(" : ").concat(batteryVoltageString).concat(" V")
                    .concat("\t").concat(getString(R.string.soc)).concat(" : ").concat(socValueString).concat("%"));
        } else {
            mOBDBatteryLabelTextView.setText(getString(R.string.battery).concat(" : ").concat(batteryVoltageString).concat(" V"));
        }

        if(socValue < 5) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_warning_0));
        } else if(socValue >= 5 && socValue <= 10) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_warning_10));
        } else if(socValue > 10 && socValue <= 20) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_warning_20));
        } else if(socValue > 20 && socValue <= 40) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_30));
        } else if(socValue > 40 && socValue <= 50) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_40));
        } else if(socValue > 50 && socValue <= 60) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_50));
        } else if(socValue > 60 && socValue <= 70) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_60));
        } else if(socValue > 70 && socValue <= 80) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_70));
        } else if(socValue > 80 && socValue <= 90) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_80));
        } else if(socValue > 90 && socValue <= 95) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_90));
        } else if(socValue > 95) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_100));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        showHideScanManuallyView();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getActivity() != null && mScanQRCodeButton != null) {
            mScanQRCodeButton.setEnabled(true);
        }

        showHideScanManuallyView();

        toggleOngoingRentalViews();

        updateBatteryViews();
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        clearTimer();

        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            //check if all permissions are granted
            boolean permissionGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                } else {
                    permissionGranted = false;
                    break;
                }
            }
            if (permissionGranted) {
                startZXingActivity();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ZXING_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                mZxingResultStr = data.getStringExtra(ZXingActivity.ZXING_RESULT_KEY);

                //check type of string
                if(mZxingResultStr != null && !mZxingResultStr.isEmpty()) {
                    if(getActivity() == null) {
                        return;
                    }

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.data_read_successfully));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 1000);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    //check it's first character, if it's 2 then it's device id
                    if(mZxingResultStr.length() == REVOS_DEVICE_ID_LENGTH && mZxingResultStr.charAt(0) == '2') {

                        //it's a device id, it's validity will be checked by a call to the server
                        String deviceId = mZxingResultStr.substring(1);

                        fetchDeviceDetailsAndConnect(deviceId);

                    } else {
                        //it might be a generic device id, send it as a whole
                        fetchDeviceDetailsAndConnect(mZxingResultStr);
                    }
                } else {
                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_qr_code));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        }
    }
}
