package com.revos.android.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.ServiceRequestActivity;
import com.revos.android.utilities.adapters.ServiceRequestsListAdapter;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.ListServiceRequestQuery;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class ServiceRequestFragment extends Fragment {

    private View mRootView;

    private RecyclerView mServiceRequestListRecyclerView;

    private Fragment mNewServiceRequest;

    private ProgressBar mServiceRequestProgressBar;

    private TextView mServiceRequestProgressBarTextView;

    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.service_request_fragment, container, false);

        setupViews();

        if(getActivity() != null) {
            mContext = getActivity();
        }

        if(mContext != null && ((ServiceRequestActivity)mContext).mCachedServiceRequestList != null) {
            populateServiceRequestList(((ServiceRequestActivity)mContext).mCachedServiceRequestList);
        }

        fetchServiceTickets();

        return mRootView;

    }

    private void setupViews() {

        mRootView.findViewById(R.id.service_request_back_button_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().finish();
            }
        });

        ImageView mCreateNewServiceRequestImageView = mRootView.findViewById(R.id.service_request_create_new_request_image_view);
        mCreateNewServiceRequestImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity() == null) {
                    return;
                }

                mNewServiceRequest = new NewServiceRequestFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(android.R.id.content, mNewServiceRequest)
                        .addToBackStack(null)
                        .commit();

            }
        });

        mServiceRequestListRecyclerView = mRootView.findViewById(R.id.service_request_list_recycler_view);
        mServiceRequestProgressBar = mRootView.findViewById(R.id.service_request_frag_progressBar);
        mServiceRequestProgressBarTextView = mRootView.findViewById(R.id.service_request_frag_progressBar_text_view);

    }

    private void fetchServiceTickets() {
        if(getActivity() == null) {
            mServiceRequestProgressBar.setIndeterminate(false);
            mServiceRequestProgressBar.setVisibility(View.GONE);
            mServiceRequestProgressBarTextView.setVisibility(View.GONE);
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            mServiceRequestProgressBar.setIndeterminate(false);
            mServiceRequestProgressBar.setVisibility(View.GONE);
            mServiceRequestProgressBarTextView.setVisibility(View.GONE);

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_service_requests));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        ListServiceRequestQuery listServiceRequestQuery = ListServiceRequestQuery.builder().build();

        apolloClient.query(listServiceRequestQuery).enqueue(new ApolloCall.Callback<ListServiceRequestQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<ListServiceRequestQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mServiceRequestProgressBar.setIndeterminate(false);
                        mServiceRequestProgressBar.setVisibility(View.GONE);
                        mServiceRequestProgressBarTextView.setVisibility(View.GONE);

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_service_requests));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else {

                            if(response.getData() != null && response.getData().support() != null && response.getData().support().getAll() != null && response.getData().support().getAll().size() >0) {
                                List<ListServiceRequestQuery.GetAll> serviceTicketList =  response.getData().support().getAll();
                                populateServiceRequestList(serviceTicketList);

                            } else {

                                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_service_requests_found));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                intent.putExtras(bundle);
                                startActivity(intent);

                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mServiceRequestProgressBar.setIndeterminate(false);
                        mServiceRequestProgressBar.setVisibility(View.GONE);
                        mServiceRequestProgressBarTextView.setVisibility(View.GONE);

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });
    }

    private void populateServiceRequestList(List<ListServiceRequestQuery.GetAll> serviceRequestList) {
        if(getActivity() == null || serviceRequestList == null) {
            return;
        }

        List<ListServiceRequestQuery.GetAll> modifiableServiceRequestList = new ArrayList<ListServiceRequestQuery.GetAll>(serviceRequestList);

        Collections.sort(modifiableServiceRequestList, new Comparator<ListServiceRequestQuery.GetAll>() {
            @Override
            public int compare(ListServiceRequestQuery.GetAll o1, ListServiceRequestQuery.GetAll o2) {
                DateTime o1DateTime = new DateTime(o1.createdAt());
                DateTime o2DateTime = new DateTime(o2.createdAt());

                if(o1DateTime.isBefore(o2DateTime)) {
                    return 1;
                } else if(o1DateTime.isAfter(o2DateTime)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        ((ServiceRequestActivity)mContext).mCachedServiceRequestList = modifiableServiceRequestList;

        ServiceRequestsListAdapter mServiceRequestListAdapter = new ServiceRequestsListAdapter(modifiableServiceRequestList, getActivity());
        mServiceRequestListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mServiceRequestListRecyclerView.setAdapter(mServiceRequestListAdapter);
    }

    @Override
    public void onDestroy() {
        mServiceRequestProgressBar.setIndeterminate(false);
        mServiceRequestProgressBar.setVisibility(View.GONE);
        mServiceRequestProgressBarTextView.setVisibility(View.GONE);

        super.onDestroy();
    }
}