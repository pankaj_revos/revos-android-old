package com.revos.android.fragments;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.revos.android.R;
import com.revos.android.activities.DiagnosticsActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.services.NordicBleService;
import com.revos.android.utilities.vehicleDataTransfer.VehicleParameters;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;

public class DiagnosticsGraphFragment extends Fragment {

    private View mRootView;

    private long mStartTimeStamp;

    //charts with non-binary values
    private LineChart mCurrentChart, mVoltageChart, mRpmChart, mThrottleChart, mTempChart, mModeChart;

    //charts with binary values
    private LineChart mBrakeStatusChart, mEAbsStatusChart, mRegenBrakingStatusChart, mOverloadStatusChart, mOverCurrentStatusChart;

    private ArrayList<Entry> mCurrentEntryList, mVoltageEntryList, mRpmEntryList, mThrottleEntryList, mTempEntryList, mModeEntryList;

    private ArrayList<Entry> mBrakeStatusEntryList, mEAbsStatusEntryList, mRegenBrakingStatusList, mOverloadStatusEntryList, mOverCurrentStatusEntryList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.diagnostics_graph_fragment, container, false);

        setupCharts();

        return mRootView;
    }

    private void setupCharts() {

        mStartTimeStamp = System.currentTimeMillis();

        setupVoltageChart();
        setupCurrentChart();
        setupThrottleChart();
        setupRpmChart();
        setupTempChart();
        setupModeChart();
        setupBrakeStatusChart();
        setupRegenStatusChart();
        setupEAbsStatusChart();
    }

    private void setupVoltageChart() {
        mVoltageChart = (LineChart) mRootView.findViewById(R.id.diag_chart_voltage_line_chart);
        setupLineChart(mVoltageChart, 20, "Volts");
        mVoltageEntryList = new ArrayList<>();
    }

    private void setupCurrentChart() {
        mCurrentChart = (LineChart) mRootView.findViewById(R.id.diag_chart_current_line_chart);
        setupLineChart(mCurrentChart, 0, "Ampere");
        mCurrentEntryList = new ArrayList<>();
    }

    private void setupThrottleChart() {
        mThrottleChart = (LineChart) mRootView.findViewById(R.id.diag_chart_throttle_line_chart);
        setupLineChart(mThrottleChart, 0, "%");
        mThrottleEntryList = new ArrayList<>();
    }

    private void setupRpmChart() {
        mRpmChart = (LineChart) mRootView.findViewById(R.id.diag_chart_rpm_line_chart);
        setupLineChart(mRpmChart, 0, "RPM");
        mRpmEntryList = new ArrayList<>();
    }

    private void setupTempChart() {
        mTempChart = (LineChart) mRootView.findViewById(R.id.diag_chart_temp_line_chart);
        setupLineChart(mTempChart, 0, "°C");
        mTempEntryList = new ArrayList<>();
    }

    private void setupModeChart() {
        mModeChart = (LineChart) mRootView.findViewById(R.id.diag_chart_mode_line_chart);
        setupLineChart(mModeChart, 0, "Mode");
        mModeEntryList = new ArrayList<>();
    }

    private void setupBrakeStatusChart() {
        mBrakeStatusChart = (LineChart) mRootView.findViewById(R.id.diag_chart_brake_status_line_chart);
        setupLineChart(mBrakeStatusChart, 0, "BrakeStatus");
        mBrakeStatusEntryList = new ArrayList<>();
    }

    private void setupRegenStatusChart() {
        mRegenBrakingStatusChart = (LineChart) mRootView.findViewById(R.id.diag_chart_regen_line_chart);
        setupLineChart(mRegenBrakingStatusChart, 0, "Regen");
        mRegenBrakingStatusList = new ArrayList<>();
    }

    private void setupEAbsStatusChart() {
        mEAbsStatusChart = (LineChart) mRootView.findViewById(R.id.diag_chart_eabs_line_chart);
        setupLineChart(mEAbsStatusChart, 0, "E ABS");
        mEAbsStatusEntryList = new ArrayList<>();
    }

    private void setupLineChart(LineChart lineChart, int minimumValue, String descriptionText) {
        lineChart.setPinchZoom(false);
        lineChart.setScaleEnabled(false);
        lineChart.getXAxis().setDrawLabels(true);
        lineChart.getXAxis().setDrawGridLines(true);
        lineChart.getDescription().setText(descriptionText);
        lineChart.getDescription().setTextColor(Color.parseColor("#000000"));
        lineChart.getLegend().setEnabled(false);
        lineChart.getAxisLeft().setTextColor(Color.parseColor("#000000"));
        lineChart.getAxisRight().setDrawLabels(false);
        lineChart.getAxisRight().setAxisMinimum(minimumValue);
        lineChart.getAxisLeft().setAxisMinimum(minimumValue);
    }

    private void updateCharts() {
        if(getActivity() == null) {
            return;
        }

        VehicleParameters vehicleParameters = NordicBleService.mVehicleLiveParameters;

        if(vehicleParameters == null) {
            return;
        }

        updateVoltageChart(vehicleParameters);
        updateCurrentChart(vehicleParameters);
        updateThrottleChart(vehicleParameters);
        updateRpmChart(vehicleParameters);
        updateTempChart(vehicleParameters);
        updateModeChart(vehicleParameters);
        updateBrakeStatusChart(vehicleParameters);
        updateRegenStatusChart(vehicleParameters);
        updateEAbsStatusChart(vehicleParameters);
    }

    private void updateVoltageChart(VehicleParameters vehicleParameters) {
        addEntry(mVoltageChart, mVoltageEntryList, vehicleParameters.getVoltage(), LineDataSet.Mode.CUBIC_BEZIER);
        ((TextView)mRootView.findViewById(R.id.diag_chart_voltage_value_text_view)).setText(String.format("(%s)", vehicleParameters.getVoltage()));
    }

    private void updateCurrentChart(VehicleParameters vehicleParameters) {
        addEntry(mCurrentChart, mCurrentEntryList, vehicleParameters.getCurrent(), LineDataSet.Mode.CUBIC_BEZIER);
        ((TextView)mRootView.findViewById(R.id.diag_chart_current_value_text_view)).setText(String.format("(%s)", vehicleParameters.getCurrent()));
    }

    private void updateThrottleChart(VehicleParameters vehicleParameters) {
        addEntry(mThrottleChart, mThrottleEntryList, vehicleParameters.getThrottle(), LineDataSet.Mode.CUBIC_BEZIER);
        ((TextView)mRootView.findViewById(R.id.diag_chart_throttle_value_text_view)).setText(String.format("(%s)", vehicleParameters.getThrottle()));
    }

    private void updateRpmChart(VehicleParameters vehicleParameters) {
        addEntry(mRpmChart, mRpmEntryList, vehicleParameters.getRpm(), LineDataSet.Mode.CUBIC_BEZIER);
        ((TextView)mRootView.findViewById(R.id.diag_chart_rpm_value_text_view)).setText(String.format("(%s)", vehicleParameters.getRpm()));
    }

    private void updateTempChart(VehicleParameters vehicleParameters) {
        addEntry(mTempChart, mTempEntryList, vehicleParameters.getTemperature(), LineDataSet.Mode.CUBIC_BEZIER);
        ((TextView)mRootView.findViewById(R.id.diag_chart_temp_value_text_view)).setText(String.format("(%s)", vehicleParameters.getTemperature()));
    }

    private void updateModeChart(VehicleParameters vehicleParameters) {
        addEntry(mModeChart, mModeEntryList, vehicleParameters.getMode(), LineDataSet.Mode.LINEAR);
        ((TextView)mRootView.findViewById(R.id.diag_chart_mode_value_text_view)).setText("(" + vehicleParameters.getMode() + ")");
    }

    private void updateBrakeStatusChart(VehicleParameters vehicleParameters) {
        int nonBooleanValue = vehicleParameters.isBraking() ? 1 : 0;
        addEntry(mBrakeStatusChart, mBrakeStatusEntryList, nonBooleanValue, LineDataSet.Mode.STEPPED);
        ((TextView)mRootView.findViewById(R.id.diag_chart_brake_status_value_text_view)).setText("(" + nonBooleanValue + ")");
    }

    private void updateRegenStatusChart(VehicleParameters vehicleParameters) {
        int nonBooleanValue = vehicleParameters.isRegenBraking() ? 1 : 0;
        addEntry(mRegenBrakingStatusChart, mRegenBrakingStatusList, nonBooleanValue, LineDataSet.Mode.STEPPED);
        ((TextView)mRootView.findViewById(R.id.diag_chart_regen_value_text_view)).setText("(" + nonBooleanValue + ")");
    }

    private void updateEAbsStatusChart(VehicleParameters vehicleParameters) {
        int nonBooleanValue = vehicleParameters.isEABSEngaged() ? 1 : 0;
        addEntry(mEAbsStatusChart, mEAbsStatusEntryList, nonBooleanValue, LineDataSet.Mode.STEPPED);
        ((TextView)mRootView.findViewById(R.id.diag_chart_eabs_value_text_view)).setText("(" + nonBooleanValue + ")");
    }


    private void addEntry(LineChart chart, ArrayList<Entry> entryList,float value, LineDataSet.Mode lineMode) {

        if(getActivity() == null) {
            return;
        }

        entryList.add(new Entry((System.currentTimeMillis() - mStartTimeStamp) / 1000.0f, value));

        if(entryList.size() > 40) {
            entryList.remove(0);
        }

        LineDataSet lineDataSet = new LineDataSet(entryList, "label");

        lineDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.revos_green));

        lineDataSet.setMode(lineMode);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.area_chart_background));

        LineData lineData = new LineData(lineDataSet);

        chart.setData(lineData);

        chart.invalidate();
    }

    @Subscribe
    public void onEventBusMessage(final EventBusMessage event) {
        if (event.message.contains(OBD_EVENT_LIVE_DATA)) {
            if(getActivity() == null) {
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCharts();
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with eventbus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //deregister with the eventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}