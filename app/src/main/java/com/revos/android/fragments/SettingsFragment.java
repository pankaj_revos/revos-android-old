package com.revos.android.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.revos.android.R;
import com.suke.widget.SwitchButton;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.DAY_NIGHT_AUTO_SWITCH_THEME_KEY;
import static com.revos.android.constants.Constants.DISTRACTION_FREE_MODE_KEY;
import static com.revos.android.constants.Constants.ENABLE_CONNECT_USING_VIN_KEY;
import static com.revos.android.constants.Constants.USER_SETTINGS_PREFS;
import static com.revos.android.constants.Constants.USE_DARK_THEME_KEY;

/**
 * Created by priya on 28/11/19.
 */

public class SettingsFragment extends Fragment {

    private View mRootView;

    private com.suke.widget.SwitchButton mDayNightThemeSwitch, mDistractionFreeModeSwitch,
            mUseDarkModeOnlySwitch, mEnableConnectUsingVinSwitch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.settings_fragment, container, false);

        setupViews();

        populateSwitchButtons();

        return mRootView;
    }

    private void populateSwitchButtons() {
        if(getActivity() == null) {
            return;
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE);
        boolean autoThemeSwitch = sharedPreferences.getBoolean(DAY_NIGHT_AUTO_SWITCH_THEME_KEY, true);
        boolean distractionFreeMode = sharedPreferences.getBoolean(DISTRACTION_FREE_MODE_KEY, true);
        boolean useDarkTheme = sharedPreferences.getBoolean(USE_DARK_THEME_KEY, false);
        boolean enableConnectUsingVin = sharedPreferences.getBoolean(ENABLE_CONNECT_USING_VIN_KEY, false);

        if(autoThemeSwitch) {
            mDayNightThemeSwitch.setChecked(true);
        } else {
            mDayNightThemeSwitch.setChecked(false);
        }

        if(distractionFreeMode) {
            mDistractionFreeModeSwitch.setChecked(true);
        } else {
            mDistractionFreeModeSwitch.setChecked(false);
        }

        if(useDarkTheme) {
            mUseDarkModeOnlySwitch.setChecked(true);
        } else {
            mUseDarkModeOnlySwitch.setChecked(false);
        }

        if(enableConnectUsingVin) {
            mEnableConnectUsingVinSwitch.setChecked(true);
        } else {
            mEnableConnectUsingVinSwitch.setChecked(false);
        }

    }
    private void setupViews() {

        mDayNightThemeSwitch = mRootView.findViewById(R.id.auto_day_night_theme_switch);
        mDistractionFreeModeSwitch = mRootView.findViewById(R.id.distraction_free_mode_switch);
        mUseDarkModeOnlySwitch = mRootView.findViewById(R.id.use_dark_theme_switch);
        mEnableConnectUsingVinSwitch = mRootView.findViewById(R.id.enable_connect_using_vin_switch);

        mDayNightThemeSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(getActivity() == null){
                    return;
                }

                if(isChecked) {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(DAY_NIGHT_AUTO_SWITCH_THEME_KEY, true).apply();
                } else {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(DAY_NIGHT_AUTO_SWITCH_THEME_KEY, false).apply();
                }
            }
        });

        mDistractionFreeModeSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(getActivity() == null) {
                    return;
                }

                if(isChecked) {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(DISTRACTION_FREE_MODE_KEY, true).apply();
                } else {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(DISTRACTION_FREE_MODE_KEY, false).apply();
                }
            }
        });

        mUseDarkModeOnlySwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(getActivity() == null) {
                    return;
                }

                if(isChecked) {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(USE_DARK_THEME_KEY, true).apply();
                } else {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(USE_DARK_THEME_KEY, false).apply();
                }
            }
        });

        mEnableConnectUsingVinSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(getActivity() == null) {
                    return;
                }

                if(isChecked) {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(ENABLE_CONNECT_USING_VIN_KEY, true).apply();
                } else {
                    getActivity().getSharedPreferences(USER_SETTINGS_PREFS, MODE_PRIVATE).edit().putBoolean(ENABLE_CONNECT_USING_VIN_KEY, false).apply();
                }
            }
        });
    }

}
