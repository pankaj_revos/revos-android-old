package com.revos.android.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.revos.android.R;
import com.revos.android.activities.ActiveBookingActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.RentalActivity;
import com.revos.android.activities.BookRentalVehicleActivity;
import com.revos.android.activities.RentalKycActivity;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.RentalPricingInfo;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.RentalVehicleCompany;
import com.revos.android.jsonStructures.RentalVehicleModel;
import com.revos.android.jsonStructures.RentalVehicleModelConfig;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.scripts.FetchDeviceQuery;
import com.revos.scripts.FetchKYCAssociationForUserQuery;
import com.revos.scripts.FetchRentalVehicleDetailsAndPricingInfoQuery;
import com.revos.scripts.type.CompanyWhereUniqueInput;
import com.revos.scripts.type.DeviceWhereUniqueInput;
import com.revos.scripts.type.InputGetRentalVehicles;
import com.revos.scripts.type.KYCStatus;
import com.revos.scripts.type.RentalStatus;
import com.revos.scripts.type.VehicleStatus;
import com.revos.scripts.type.VehicleWhereInput;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.revos.android.activities.BookRentalVehicleActivity.BOOK_RENTAL_VEHICLE_OBJECT_KEY;
import static com.revos.android.activities.RentalKycActivity.RENTAL_KYC_VEHICLE_OBJECT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.REVOS_DEVICE_ID_LENGTH;

public class RentalScanQRCodeFragment extends Fragment implements ZXingScannerView.ResultHandler {

    private View mRootView;

    private ZXingScannerView mScannerView;

    private int mTabPosition;

    private boolean mIsFlashOn;

    private String[] mPermissionsRequired = new String[]{Manifest.permission.CAMERA};

    private int REQUEST_CAMERA_PERMISSION_REQ_CODE = 300;

    private final int BOOKING_ACTIVITY_REQUEST_CODE = 1002;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.rental_scan_vehicle_fragment, container, false);

        mScannerView = mRootView.findViewById(R.id.rental_scan_qr_code_to_book_scanner_view);

        ArrayList<BarcodeFormat> barcodeFormats = new ArrayList<>();
        barcodeFormats.add(BarcodeFormat.AZTEC);
        barcodeFormats.add(BarcodeFormat.CODABAR);
        barcodeFormats.add(BarcodeFormat.CODE_39);
        barcodeFormats.add(BarcodeFormat.CODE_93);
        barcodeFormats.add(BarcodeFormat.CODE_128);
        barcodeFormats.add(BarcodeFormat.DATA_MATRIX);
        barcodeFormats.add(BarcodeFormat.EAN_8);
        barcodeFormats.add(BarcodeFormat.EAN_13);
        barcodeFormats.add(BarcodeFormat.ITF);
        barcodeFormats.add(BarcodeFormat.MAXICODE);
        barcodeFormats.add(BarcodeFormat.PDF_417);
        barcodeFormats.add(BarcodeFormat.QR_CODE);
        barcodeFormats.add(BarcodeFormat.RSS_14);
        barcodeFormats.add(BarcodeFormat.RSS_EXPANDED);
        barcodeFormats.add(BarcodeFormat.UPC_A);
        barcodeFormats.add(BarcodeFormat.UPC_E);
        barcodeFormats.add(BarcodeFormat.UPC_EAN_EXTENSION);

        // this parameter will make your HUAWEI phone works great!
        mScannerView.setAspectTolerance(0.5f);
        mScannerView.setFormats(barcodeFormats);
        mScannerView.setAutoFocus(true);

        return mRootView;
    }

    private void checkPermissionAndStartScannerView() {
        if(getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }
                if (ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(mPermissionsRequired, REQUEST_CAMERA_PERMISSION_REQ_CODE);
                    return;
                }

                mScannerView.setResultHandler(RentalScanQRCodeFragment.this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        //this method does not launch launch permission dialog unlike checkPermissionAndStartScannerView
        //calling checkPermissionAndStartScannerView here was leading to an infinite loop
        //this is used to start camera when we return to main activity
        startScannerViewIfPossible();

    }

    private void startScannerViewIfPossible() {
        if(getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), mPermissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mScannerView.setResultHandler(RentalScanQRCodeFragment.this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Timber.v(rawResult.getText()); // Prints scan results
        Timber.v(rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        /*// If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);*/

        if(mTabPosition == 1) {
            Toast.makeText(getActivity(), getString(R.string.data_read_successfully), Toast.LENGTH_LONG).show();
            showProgressDialog();
            processScannedText(rawResult.getText());
        } else {
            checkPermissionAndStartScannerView();
        }
    }

    private void processScannedText(String scannedText) {
        if(getActivity() == null) {
            return;
        }

        if(scannedText != null && !scannedText.isEmpty()) {
            if(getActivity() == null) {
                return;
            }

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.data_read_successfully));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 1000);
            intent.putExtras(bundle);
            startActivity(intent);

            //check it's first character, if it's 2 then it's device id and string length = revos_device_id_length
            if(scannedText.length() == REVOS_DEVICE_ID_LENGTH && scannedText.charAt(0) == '2') {

                //it's a device id, it's validity will be checked by a call to the server
                String deviceId = scannedText.substring(1);

                fetchDevice(deviceId);

            } else {
                //it might be a generic device id, send it as a whole
                fetchDevice(scannedText);
            }
        } else {
            hideProgressDialog();
            showInvalidQrCode();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == BOOKING_ACTIVITY_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                if(getActivity() != null) {
                    Intent intent = new Intent(getActivity(), ActiveBookingActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        }
    }

    private void toggleFlash(boolean on) {
        mScannerView.setFlash(on);
    }

    private void updateCameraFlashImageView() {
        if(getActivity() == null) {
            return;
        }

        mIsFlashOn = mScannerView.getFlash();
        int backGroundId = R.drawable.turn_flash_on_background;
        if(mIsFlashOn) {
            backGroundId = R.drawable.turn_flash_off_background;
        }

        mRootView.findViewById(R.id.rental_scan_qr_code_to_book_toggle_flash_image_view).setBackground(ContextCompat.getDrawable(getActivity(), backGroundId));

        mRootView.findViewById(R.id.rental_scan_qr_code_to_book_toggle_flash_image_view).setOnClickListener(v -> {
            toggleFlash(!mIsFlashOn);
            updateCameraFlashImageView();
        });
    }

    private void fetchDevice(String deviceId) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            showGenericMessage(false, getString(R.string.unable_to_fetch_device_details), GenericMessageActivity.STATUS_ERROR);
            hideProgressDialog();
            return;
        }

        DeviceWhereUniqueInput deviceWhereUniqueInput = DeviceWhereUniqueInput
                .builder()
                .deviceId(deviceId)
                .build();

        FetchDeviceQuery fetchDeviceQuery = FetchDeviceQuery
                .builder()
                .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
                .build();

        apolloClient.query(fetchDeviceQuery).enqueue(new ApolloCall.Callback<FetchDeviceQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchDeviceQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            showGenericMessage(false, getString(R.string.unable_to_fetch_device_details), GenericMessageActivity.STATUS_ERROR);

                        } else if(response.getData() != null && response.getData().device() != null && response.getData().device().get() !=null ) {

                            FetchDeviceQuery.Get fetchedDevice = response.getData().device().get();

                            if(fetchedDevice != null && fetchedDevice.vehicle() != null && fetchedDevice.vehicle().status() == VehicleStatus.RENTAL) {
                                if(fetchedDevice.vehicle().rentalStatus() != null && fetchedDevice.vehicle().rentalStatus() == RentalStatus.AVAILABLE) {

                                    String vin = fetchedDevice.vehicle().vin();

                                    fetchVehicleDetailsAndPricingInfo(vin);
                                } else {
                                    showVehicleNotAvailable();
                                }
                            } else {
                                showVehicleNotAvailable();
                            }
                        } else {
                            showVehicleNotAvailable();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            showGenericMessage(false, getString(R.string.server_error), GenericMessageActivity.STATUS_ERROR);
                        }
                    });
                }
            }
        });
    }

    private void fetchVehicleDetailsAndPricingInfo(String vin) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            showGenericMessage(false, getString(R.string.unable_to_fetch_vehicle_details), GenericMessageActivity.STATUS_ERROR);

            return;
        }

        showProgressDialog();

        InputGetRentalVehicles inputGetRentalVehicles = InputGetRentalVehicles
                .builder()
                .package_(getActivity().getPackageName())
                .vehicle(VehicleWhereInput.builder().vin(vin).build())
                .build();

        FetchRentalVehicleDetailsAndPricingInfoQuery fetchRentalVehicleDetailsAndPricingInfoQuery = FetchRentalVehicleDetailsAndPricingInfoQuery
                .builder()
                .inputGetRentalVehicles(inputGetRentalVehicles)
                .build();


        apolloClient.query(fetchRentalVehicleDetailsAndPricingInfoQuery).enqueue(new ApolloCall.Callback<FetchRentalVehicleDetailsAndPricingInfoQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchRentalVehicleDetailsAndPricingInfoQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            showGenericMessage(false, getString(R.string.unable_to_fetch_vehicle_details), GenericMessageActivity.STATUS_ERROR);

                        } else if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getRentalVehicles() !=null ) {

                            FetchRentalVehicleDetailsAndPricingInfoQuery.GetRentalVehicle fetchedRentalVehicleDetails;

                            if(response.getData().vehicles().getRentalVehicles() != null && !response.getData().vehicles().getRentalVehicles().isEmpty()
                                    && response.getData().vehicles().getRentalVehicles().get(0) != null) {

                                fetchedRentalVehicleDetails = response.getData().vehicles().getRentalVehicles().get(0);

                                if(fetchedRentalVehicleDetails != null && fetchedRentalVehicleDetails.rentalStatus() == RentalStatus.AVAILABLE) {

                                    RentalVehicle rentalVehicle = new RentalVehicle();

                                    if(fetchedRentalVehicleDetails.isDeliverable() != null) {
                                        rentalVehicle.setDeliverable(fetchedRentalVehicleDetails.isDeliverable());
                                    }

                                    if(fetchedRentalVehicleDetails.vin() != null && !fetchedRentalVehicleDetails.vin().isEmpty()) {
                                        rentalVehicle.setVin(fetchedRentalVehicleDetails.vin());
                                    }

                                    if(fetchedRentalVehicleDetails.rentalStatus() != null) {
                                        rentalVehicle.setRentalStatus(fetchedRentalVehicleDetails.rentalStatus().toString());
                                    }

                                    /*if(fetchedRentalVehicleDetails.lastMarkedLatitude() != null && !fetchedRentalVehicleDetails.lastMarkedLatitude().isEmpty()) {
                                        double lat;
                                        lat = Double.parseDouble(fetchedRentalVehicleDetails.lastMarkedLatitude());
                                        rentalVehicle.setLatitude(lat);
                                    }

                                    if(fetchedRentalVehicleDetails.lastMarkedLongitude() != null && !fetchedRentalVehicleDetails.lastMarkedLongitude().isEmpty()) {
                                        double lng;
                                        lng = Double.parseDouble(fetchedRentalVehicleDetails.lastMarkedLongitude());
                                        rentalVehicle.setLongitude(lng);
                                    }*/

                                    if(fetchedRentalVehicleDetails.model() != null) {

                                        RentalVehicleModel model = new RentalVehicleModel();

                                        if(fetchedRentalVehicleDetails.model().name() != null && ! fetchedRentalVehicleDetails.model().name().isEmpty()) {
                                            model.setName(fetchedRentalVehicleDetails.model().name());
                                        }

                                        if(fetchedRentalVehicleDetails.model().config() != null) {

                                            RentalVehicleModelConfig modelConfig = new RentalVehicleModelConfig();

                                            if(fetchedRentalVehicleDetails.model().config().batteryMaxVoltage() != null) {
                                                modelConfig.setBatteryMaxVoltage(fetchedRentalVehicleDetails.model().config().batteryMaxVoltage());
                                            }

                                            if(fetchedRentalVehicleDetails.model().config().batteryMinVoltage() != null) {
                                                modelConfig.setBatteryMinVoltage(fetchedRentalVehicleDetails.model().config().batteryMinVoltage());
                                            }

                                            model.setConfig(modelConfig);
                                        }

                                        if(fetchedRentalVehicleDetails.model().company() != null) {
                                            RentalVehicleCompany rentalVehicleCompany = new RentalVehicleCompany();

                                            if(fetchedRentalVehicleDetails.model().company().id() != null
                                                    && !fetchedRentalVehicleDetails.model().company().id().isEmpty()) {
                                                rentalVehicleCompany.setId(fetchedRentalVehicleDetails.model().company().id());
                                            }

                                            if(fetchedRentalVehicleDetails.model().company().name() != null
                                                    && !fetchedRentalVehicleDetails.model().company().name().isEmpty()) {
                                                rentalVehicleCompany.setName(fetchedRentalVehicleDetails.model().company().name());
                                            }

                                            if(fetchedRentalVehicleDetails.model().company().phone() != null
                                                    && !fetchedRentalVehicleDetails.model().company().phone().isEmpty()) {
                                                rentalVehicleCompany.setPhone(fetchedRentalVehicleDetails.model().company().phone());
                                            }

                                            model.setCompany(rentalVehicleCompany);
                                        }

                                        rentalVehicle.setModel(model);
                                    }


                                    if(fetchedRentalVehicleDetails.company() != null) {

                                        RentalVehicleCompany company = new RentalVehicleCompany();

                                        if(fetchedRentalVehicleDetails.company().id() != null && !fetchedRentalVehicleDetails.company().id().isEmpty()) {
                                            company.setId(fetchedRentalVehicleDetails.company().id());
                                        }

                                        if(fetchedRentalVehicleDetails.company().name() != null && !fetchedRentalVehicleDetails.company().name().isEmpty()) {
                                            company.setName(fetchedRentalVehicleDetails.company().name());
                                        }

                                        if(fetchedRentalVehicleDetails.company().phone() != null && !fetchedRentalVehicleDetails.company().phone().isEmpty()) {
                                            company.setPhone(fetchedRentalVehicleDetails.company().phone());
                                        }

                                        rentalVehicle.setCompany(company);
                                    }

                                    if(fetchedRentalVehicleDetails.oem() != null) {

                                        RentalVehicleCompany oem = new RentalVehicleCompany();

                                        if(fetchedRentalVehicleDetails.oem().id() != null && !fetchedRentalVehicleDetails.oem().id().isEmpty()) {
                                            oem.setId(fetchedRentalVehicleDetails.oem().id());
                                        }

                                        if(fetchedRentalVehicleDetails.oem().name() != null && !fetchedRentalVehicleDetails.oem().name().isEmpty()) {
                                            oem.setName(fetchedRentalVehicleDetails.oem().name());
                                        }

                                        if(fetchedRentalVehicleDetails.oem().phone() != null && !fetchedRentalVehicleDetails.oem().phone().isEmpty()) {
                                            oem.setPhone(fetchedRentalVehicleDetails.oem().phone());
                                        }

                                        rentalVehicle.setOem(oem);
                                    }

                                    if(fetchedRentalVehicleDetails.distributor() != null) {

                                        RentalVehicleCompany distributor = new RentalVehicleCompany();

                                        if(fetchedRentalVehicleDetails.distributor().id() != null && !fetchedRentalVehicleDetails.distributor().id().isEmpty()) {
                                            distributor.setId(fetchedRentalVehicleDetails.distributor().id());
                                        }

                                        if(fetchedRentalVehicleDetails.distributor().name() != null && !fetchedRentalVehicleDetails.distributor().name().isEmpty()) {
                                            distributor.setName(fetchedRentalVehicleDetails.distributor().name());
                                        }

                                        if(fetchedRentalVehicleDetails.distributor().phone() != null && !fetchedRentalVehicleDetails.distributor().phone().isEmpty()) {
                                            distributor.setPhone(fetchedRentalVehicleDetails.distributor().phone());
                                        }

                                        rentalVehicle.setDistributor(distributor);
                                    }

                                    //vehicle status, lat lng, batteryVoltageAdc & batteryVoltage not populated

                                    if(fetchedRentalVehicleDetails.txInfo() != null && fetchedRentalVehicleDetails.txInfo().pricing() != null) {
                                        List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing> pricingList = fetchedRentalVehicleDetails.txInfo().pricing();

                                        if(pricingList != null && !pricingList.isEmpty()) {
                                            for(int index = 0 ; index < pricingList.size() ; index++) {

                                                RentalPricingInfo rentalPricingInfo = new RentalPricingInfo();

                                                if(pricingList.get(index).name() != null && !pricingList.get(index).name().isEmpty()) {
                                                    rentalPricingInfo.setPricingName(pricingList.get(index).name());
                                                }
                                                if(pricingList.get(index).type() != null) {
                                                    rentalPricingInfo.setPaymentType(pricingList.get(index).type());
                                                }
                                                if(pricingList.get(index).baseAmount() != null) {
                                                    rentalPricingInfo.setBaseAmount(pricingList.get(index).baseAmount().floatValue());
                                                }
                                                if(pricingList.get(index).costPerUnit() != null) {
                                                    rentalPricingInfo.setCostPerUnit(pricingList.get(index).costPerUnit().floatValue());
                                                }
                                                if(pricingList.get(index).unit() != null) {
                                                    rentalPricingInfo.setUnitType(pricingList.get(index).unit());
                                                }
                                                if(pricingList.get(index).paymentMeta() != null && pricingList.get(index).paymentMeta().address() != null
                                                        && !pricingList.get(index).paymentMeta().address().isEmpty()) {
                                                    rentalPricingInfo.setProviderUpiAddress(pricingList.get(0).paymentMeta().address());
                                                }
                                            }
                                        }
                                        rentalVehicle.setRentalPricingInfoList(pricingList);
                                    }

                                    checkKycForProvider(rentalVehicle);

                                } else {
                                    showVehicleNotAvailable();
                                }
                            } else {
                                unableToFetchVehicleDetails();
                            }
                        } else {
                            unableToFetchVehicleDetails();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();

                            showGenericMessage(false, getString(R.string.server_error), GenericMessageActivity.STATUS_ERROR);
                        }
                    });
                }
            }
        });
    }

    private void checkKycForProvider(RentalVehicle rentalVehicle) {
        //if Provider's id is available, check KYC association
        if(rentalVehicle != null) {

            if(getRentalProviderId(rentalVehicle) != null && !getRentalProviderId(rentalVehicle).isEmpty()) {
                checkKycAssociationForSelectedCompany(rentalVehicle);
            } else {

                hideProgressDialog();
                launchKycActivity(rentalVehicle);
            }
        } else {
            hideProgressDialog();

            launchKycActivity(rentalVehicle);
        }
    }

    private void launchKycActivity(RentalVehicle rentalVehicle) {
        //start KYC activity
        Intent myIntent = new Intent(getActivity(), RentalKycActivity.class);
        myIntent.putExtra(RENTAL_KYC_VEHICLE_OBJECT_KEY, new Gson().toJson(rentalVehicle));
        startActivity(myIntent);
    }

    private void launchBookVehicleActivity(RentalVehicle rentalVehicle) {
        Intent intent = new Intent(getActivity(), BookRentalVehicleActivity.class);

        intent.putExtra(BOOK_RENTAL_VEHICLE_OBJECT_KEY, new Gson().toJson(rentalVehicle));

        startActivityForResult(intent, BOOKING_ACTIVITY_REQUEST_CODE);
    }

    private String getRentalProviderId(RentalVehicle rentalVehicle) {
        if(rentalVehicle != null) {

            String rentalProviderId = null;

            if (rentalVehicle.getCompany() != null && rentalVehicle.getCompany().getId() != null
                    && !rentalVehicle.getCompany().getId().isEmpty()) {
                rentalProviderId = rentalVehicle.getCompany().getId();
            } else if (rentalVehicle.getOem() != null && rentalVehicle.getOem().getId() != null
                    && !rentalVehicle.getOem().getId().isEmpty()) {
                rentalProviderId = rentalVehicle.getOem().getId();
            } else if (rentalVehicle.getDistributor() != null && rentalVehicle.getDistributor().getId() != null
                    && !rentalVehicle.getDistributor().getId().isEmpty()) {
                rentalProviderId = rentalVehicle.getDistributor().getId();
            }

            return rentalProviderId;
        } else {
            return null;
        }
    }

    private void checkKycAssociationForSelectedCompany(RentalVehicle rentalVehicle) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            hideProgressDialog();

            showGenericMessage(false, getString(R.string.error), GenericMessageActivity.STATUS_ERROR);
            return;
        }

        CompanyWhereUniqueInput companyWhereUniqueInput = CompanyWhereUniqueInput
                .builder()
                .id(getRentalProviderId(rentalVehicle))
                .build();

        FetchKYCAssociationForUserQuery getKycAssociationForRentalQuery = FetchKYCAssociationForUserQuery
                .builder()
                .companyWhereUniqueInput(companyWhereUniqueInput)
                .build();

        apolloClient.query(getKycAssociationForRentalQuery).enqueue(new ApolloCall.Callback<FetchKYCAssociationForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchKYCAssociationForUserQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getData() != null && response.getData().account() != null && response.getData().account().getKYC() != null) {
                            if(response.getData().account().getKYC().status().equals(KYCStatus.APPROVED)) {

                                launchBookVehicleActivity(rentalVehicle);
                            } else {
                                launchKycActivity(rentalVehicle);
                            }
                        } else {
                            launchKycActivity(rentalVehicle);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        launchKycActivity(rentalVehicle);
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_CAMERA_PERMISSION_REQ_CODE){
            //check if all permissions are granted
            checkPermissionAndStartScannerView();
        }

    }

    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {
        if (event.message.contains(Constants.GEN_EVENT_VEHICLE_RENTAL_TAB_CHANGE)) {
            mTabPosition = Integer.parseInt(event.message.split(GEN_DELIMITER)[1]);

            if(mTabPosition == 1) {
                updateCameraFlashImageView();
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(getActivity() == null) {
                            return;
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                checkPermissionAndStartScannerView();
                            }
                        });
                    }
                }, 500);
            } else {
                toggleFlash(false);
            }
        }
    }

    private void showInvalidQrCode() {
        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.invalid_qr_code));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void unableToFetchVehicleDetails() {
        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vehicle_details));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showVehicleNotAvailable() {
        hideProgressDialog();

        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.vehicle_not_available_for_renting));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        startActivity(intent);

        checkPermissionAndStartScannerView();
    }

    private void showGenericMessage(boolean makeMsgPersistent, String message, int messageType) {
        if(getActivity() == null) {
            return;
        }

        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, messageType);

        if(makeMsgPersistent) {
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));

        } else {
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        }

        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void showProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((RentalActivity)getActivity()).showProgressDialog();
    }

    private void hideProgressDialog() {
        if(getActivity() == null) {
            return;
        }
        ((RentalActivity)getActivity()).hideProgressDialog();
    }
}
