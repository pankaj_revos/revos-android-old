package com.revos.android.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.FileProvider;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.bumptech.glide.Glide;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.SimpleImageViewActivity;
import com.revos.android.jsonStructures.User;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.FetchServiceRequestDetailsQuery;
import com.revos.scripts.UpdateServiceRequestMutation;
import com.revos.scripts.type.FileType;
import com.revos.scripts.type.ServiceTicketCommentsInput;
import com.revos.scripts.type.ServiceTicketWhereUniqueInput;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;
import static com.revos.android.constants.Constants.FILE_PROVIDER;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.GENERIC_TEMP_IMAGE_KEY;
import static com.revos.android.constants.Constants.SERVICE_TICKET_PIC_COMPRESSED_PREFIX;
import static com.revos.android.constants.Constants.SERVICE_TICKET_PIC_PREFIX;
import static com.revos.android.constants.Constants.SERVICE_TICKET_PIC_TEMP_PREFIX;
import static com.revos.android.constants.Constants.TEMP_IMAGE_PREFS;
import static com.revos.android.constants.Constants.TEMP_PIC_DIR_PATH;
import static com.revos.android.constants.Constants.UPLOAD_TYPE_SERVICE_TICKET;


public class ServiceRequestDetailsFragment extends Fragment {

    private View mRootView;

    private TextView mTicketIDNumber, mTicketStatus, mTicketTitle, mTicketDescription, mTicketAssigneeName,
                     mTicketAssigneeAddress, mTicketAssigneePhone, mTicketCreationDate, mTicketActivitiesHeading,
                     mAddTicketImageTextView;

    private EditText mAddCommentEditText;

    private Button mAddCommentButton, mUploadTicketImageButton;

    private ImageView mAddTicketImagesImageView;

    private LinearLayout mServiceRequestActivitiesLinearLayout, mImagesUploadedLinearLayout;

    private CardView mTicketActivitiesCardView;

    public static final String SERVICE_REQUEST_ID = "service_request_id";

    private String mTicketID = null;

    private String mSrTempCompressedFileName;

    private File mSrTempCompressedFile;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private final int SR_PIC_OPEN_CAMERA_REQUEST_CODE = 100;
    private final int SR_PIC_OPEN_GALLERY_REQUEST_CODE = 101;

    private final int CAMERA_PERMISSION_REQUEST_CODE = 2000;

    private Uri mServiceTicketPicTempURI;

    private File mServiceTicketPicTempFile;

    private Bitmap mSRWorkingBitmap;

    private File mSRPicCompressedTempFile;

    private int SR_IMAGE_SIZE_CAP = 5242880;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(SERVICE_REQUEST_ID)) {

            mTicketID = getArguments().getString(SERVICE_REQUEST_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.service_request_details_fragment, container, false);

        setupViews();

        fetchServiceRequestDetails();

        return mRootView;
    }

    private void setupViews() {

        mRootView.findViewById(R.id.srd_back_button_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        mTicketIDNumber = mRootView.findViewById(R.id.srd_ticket_number_value_text_view);
        mTicketStatus = mRootView.findViewById(R.id.srd_status_value_text_view);
        mTicketTitle = mRootView.findViewById(R.id.srd_title_value_text_view);
        mTicketDescription = mRootView.findViewById(R.id.srd_description_value_text_view);
        mTicketAssigneeName = mRootView.findViewById(R.id.srd_assignee_name_value_text_view);
        mTicketAssigneeAddress = mRootView.findViewById(R.id.srd_assignee_address_value_text_view);
        mTicketAssigneePhone = mRootView.findViewById(R.id.srd_assignee_phone_value_text_view);
        mTicketCreationDate = mRootView.findViewById(R.id.srd_creation_date_value_text_view);
        mAddCommentEditText = mRootView.findViewById(R.id.service_request_details_comment_edit_text);
        mAddCommentButton = mRootView.findViewById(R.id.service_request_details_comment_button_view);
        mImagesUploadedLinearLayout = mRootView.findViewById(R.id.srd_uploaded_images_linear_layout);
        mAddTicketImagesImageView = mRootView.findViewById(R.id.srd_add_image_image_view);
        mAddTicketImageTextView = mRootView.findViewById(R.id.srd_add_image_text_view);
        mUploadTicketImageButton = mRootView.findViewById(R.id.srd_upload_image_button_view);
        mServiceRequestActivitiesLinearLayout = mRootView.findViewById(R.id.service_request_activities_linear_layout);
        mTicketActivitiesHeading = mRootView.findViewById(R.id.service_request_activities_heading_text_view);
        mTicketActivitiesCardView = mRootView.findViewById(R.id.service_request_activities_card_view);

        //Enable Add comment button only if text is typed in Edit Text
        mAddCommentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                toggleAddCommentButton();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        mAddTicketImagesImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addTicketImages();
            }
        });

        mAddTicketImageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addTicketImages();
            }
        });

        mUploadTicketImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //clear views before populating
                mImagesUploadedLinearLayout.removeAllViews();
                mServiceRequestActivitiesLinearLayout.removeAllViews();

                new SRCompressPicTask().execute(UPLOAD_TYPE_SERVICE_TICKET);
            }
        });

    }

    private void toggleAddCommentButton() {

        boolean isReady = mAddCommentEditText.getText().toString().length() > 1;
        mAddCommentButton.setEnabled(isReady);

        mAddCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newCommentAdded = mAddCommentEditText.getText().toString();

                //clear add comment edit text view
                mAddCommentEditText.getText().clear();

                //clear views before populating
                mServiceRequestActivitiesLinearLayout.removeAllViews();
                mImagesUploadedLinearLayout.removeAllViews();

                makeUpdateServiceTicketCall(newCommentAdded);

            }
        });
    }

    private void addTicketImages() {
        if(getActivity() == null) {
            return;
        }

        final Dialog mediaPickerDialog = new Dialog(getActivity());
        mediaPickerDialog.setContentView(R.layout.media_picker_dialog);

        mediaPickerDialog.findViewById(R.id.camera_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null) {
                    return;
                }

                // do we have a camera?
                if(!getActivity().getPackageManager()
                        .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_camera_on_device));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                    mediaPickerDialog.dismiss();
                    return;
                }

                //check for runtime permission
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                    return;
                }

                //create temp file
                String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
                String serviceTicketPicTempFileName = SERVICE_TICKET_PIC_TEMP_PREFIX +timeStamp + ".jpg";
                mServiceTicketPicTempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), serviceTicketPicTempFileName);

                Uri tempPhotoURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mServiceTicketPicTempFile);
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPhotoURI);
                startActivityForResult(takePicture, SR_PIC_OPEN_CAMERA_REQUEST_CODE);
                mediaPickerDialog.dismiss();
            }
        });

        mediaPickerDialog.findViewById(R.id.gallery_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , SR_PIC_OPEN_GALLERY_REQUEST_CODE);
                mediaPickerDialog.dismiss();
            }
        });

        mediaPickerDialog.show();
    }

    private void fetchServiceRequestDetails() {
        if(getActivity() == null || mTicketID == null){
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            hideProgressDialog();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_service_ticket_details));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog(getString(R.string.fetching_service_ticket_details));


        FetchServiceRequestDetailsQuery fetchServiceRequestDetails = FetchServiceRequestDetailsQuery.builder()
                .serviceTicketWhereUniqueInputVar(mTicketID)
                .build();

        apolloClient.query(fetchServiceRequestDetails).enqueue(new ApolloCall.Callback<FetchServiceRequestDetailsQuery.Data>() {

            @Override
            public void onResponse(@NotNull Response<FetchServiceRequestDetailsQuery.Data> response) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_service_ticket_details));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else {

                            if(response.getData() != null && response.getData().support() != null && response.getData().support().get() != null) {
                                FetchServiceRequestDetailsQuery.Get serviceTicketDetailsObject =  response.getData().support().get();

                                if (serviceTicketDetailsObject != null) {
                                    populateServiceRequestDetails(serviceTicketDetailsObject);
                                    populateServiceRequestActivities(serviceTicketDetailsObject);

                                }
                            } else {

                                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_service_requests_found));
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            }
        });

    }

    private void makeUpdateServiceTicketCall(String commentAdded) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            hideProgressDialog();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_add_comment));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog(getString(R.string.adding_comment));

        ServiceTicketCommentsInput  serviceTicketCommentsInput = ServiceTicketCommentsInput
                                                                 .builder()
                                                                 .comment(commentAdded)
                                                                 .build();

        ServiceTicketWhereUniqueInput serviceTicketWhereUniqueInput = ServiceTicketWhereUniqueInput
                                                                      .builder()
                                                                      .id(mTicketID)
                                                                      .build();

        UpdateServiceRequestMutation updateServiceRequestMutation = UpdateServiceRequestMutation
                                                                    .builder()
                                                                    .data(serviceTicketCommentsInput)
                                                                    .where(serviceTicketWhereUniqueInput)
                                                                    .build();

        apolloClient.mutate(updateServiceRequestMutation).enqueue(new ApolloCall.Callback<UpdateServiceRequestMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateServiceRequestMutation.Data> response) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_add_comment));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else if(response.getData() != null && response.getData().support() != null
                                && response.getData().support().update() != null && response.getData().support().update().activities() != null ) {

                            fetchServiceRequestDetails();

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.comment_added_successfully));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }

                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void makeAddTicketImagesCall(FileType fileType, List<String> encodedFileString, String ticketImagePrefix) {
        if(getActivity() == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();

        if(apolloClient == null) {
            hideProgressDialog();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_add_image));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        showProgressDialog(getString(R.string.uploading_image));

        ServiceTicketCommentsInput  serviceTicketImagesInput = ServiceTicketCommentsInput
                .builder()
                .logoBuffer(encodedFileString)
                .build();

        ServiceTicketWhereUniqueInput serviceTicketWhereUniqueInput = ServiceTicketWhereUniqueInput
                .builder()
                .id(mTicketID)
                .build();

        UpdateServiceRequestMutation updateServiceRequestMutation = UpdateServiceRequestMutation
                .builder()
                .data(serviceTicketImagesInput)
                .where(serviceTicketWhereUniqueInput)
                .build();

        apolloClient.mutate(updateServiceRequestMutation).enqueue(new ApolloCall.Callback<UpdateServiceRequestMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateServiceRequestMutation.Data> response) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_add_image));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else if(response.getData() != null && response.getData().support() != null
                                && response.getData().support().update() != null) {

                            fetchServiceRequestDetails();

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.image_added_successfully));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private class SRCompressPicTask extends AsyncTask<String, String, File> {

        @Override
        protected File doInBackground(String... params) {
            if(getActivity() == null){
                return null;
            }

            Uri srPicTempUri;
            String srPicTempPrefix;

            //compress the profile pic before uploading
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());

            srPicTempPrefix = SERVICE_TICKET_PIC_COMPRESSED_PREFIX;
            srPicTempUri = mServiceTicketPicTempURI;

            mSrTempCompressedFileName = srPicTempPrefix + timeStamp + ".jpg";
            mSrTempCompressedFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), mSrTempCompressedFileName);
            try {
                mSRWorkingBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), srPicTempUri);
                FileOutputStream compressedFileOutputStream = new FileOutputStream(mSrTempCompressedFile);
                mSRWorkingBitmap.compress(Bitmap.CompressFormat.JPEG, 75, compressedFileOutputStream);
                compressedFileOutputStream.flush();
                compressedFileOutputStream.close();
                mSRWorkingBitmap.recycle();

            } catch (Exception e) {
                //todo:: handle this
                hideProgressDialog();
            }

            mSRPicCompressedTempFile = mSrTempCompressedFile;

            return mSrTempCompressedFile;
        }

        @Override
        protected void onPostExecute(File mSrTempCompressedFile) {
            super.onPostExecute(mSrTempCompressedFile);
            //check image size
            if(mSrTempCompressedFile.length() > SR_IMAGE_SIZE_CAP) {

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.image_too_large));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

                hideProgressDialog();
                return;
            }

            //upload the pic
            uploadSRPic();
        }
    }

    private void uploadSRPic() {

        if(getActivity() == null) {
            return;
        }

        final File srTempCompressedFile;
        final String srPicPrefix;
        final File srTempFileFromCamera;

        srTempCompressedFile = mSRPicCompressedTempFile;
        srTempFileFromCamera = mServiceTicketPicTempFile;
        srPicPrefix = SERVICE_TICKET_PIC_PREFIX;

        try {

            byte[] srFileBytesArray = new byte[(int) srTempCompressedFile.length()];
            FileInputStream fis = new FileInputStream(srTempCompressedFile);
            fis.read(srFileBytesArray); //read file into bytes[]
            fis.close();
            String srEncodedFileString = Base64.encodeToString(srFileBytesArray, Base64.DEFAULT);

            String encodedString = "data:image/jpg;base64," + srEncodedFileString;

            FileType srUploadFileType = FileType.SERVICE_TICKET_IMG;

            List<String> srEncodedFileStringArray = new ArrayList<>();
            srEncodedFileStringArray.add(encodedString);

            //refresh add image view
            mServiceTicketPicTempURI = null;
            mAddTicketImagesImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_add_image));
            mUploadTicketImageButton.setVisibility(GONE);

            clearTempFiles();

            makeAddTicketImagesCall(srUploadFileType, srEncodedFileStringArray, srPicPrefix);

        } catch (Exception e) {

            //todo::handle file write exception
            //delete the temp files
            if(srTempFileFromCamera.exists()) { srTempFileFromCamera.delete(); }
            if(srTempCompressedFile.exists()) { srTempCompressedFile.delete(); }
        }

    }

    private void clearTempFiles(){

        if(mSRPicCompressedTempFile.exists()) {
            mSRPicCompressedTempFile.delete();
        }

        if(mServiceTicketPicTempFile.exists()) {
            mServiceTicketPicTempFile.delete();
        }
    }

    private void populateServiceRequestDetails(FetchServiceRequestDetailsQuery.Get serviceTicketDetails) {
        if(getActivity() == null) {
            return;
        }

        if(serviceTicketDetails.number() != null) {
            mTicketIDNumber.setText(serviceTicketDetails.number());
        }

        String srdStatus = serviceTicketDetails.status().toString();

        switch (srdStatus) {
            case "RAISED" : mTicketStatus.setText(getString(R.string.service_request_status_raised));
                            mTicketStatus.setBackground(getActivity().getDrawable(R.drawable.capsule_background_status_raised));
                            break;

            case "PENDING" :    mTicketStatus.setText(getString(R.string.service_request_status_pending));
                                mTicketStatus.setBackground(getActivity().getDrawable(R.drawable.capsule_background_status_pending));
                                break;

            case "INPROGRESS" : mTicketStatus.setText(getString(R.string.service_request_status_in_progress));
                                mTicketStatus.setBackground(getActivity().getDrawable(R.drawable.capsule_background_status_inprogress));
                                break;

            case "COMPLETED" :  mTicketStatus.setText(getString(R.string.service_request_status_completed));
                                mTicketStatus.setBackground(getActivity().getDrawable(R.drawable.capsule_background_status_completed));
                                break;

            case "CLOSED" : mTicketStatus.setText(getString(R.string.service_request_status_closed));
                            mTicketStatus.setBackground(getActivity().getDrawable(R.drawable.capsule_background_status_closed));
                            mRootView.findViewById(R.id.srd_add_image_linear_layout).setVisibility(GONE);
                            mRootView.findViewById(R.id.service_request_details_add_comment_linear_layout).setVisibility(GONE);
                            break;
        }

        mTicketStatus.setVisibility(View.VISIBLE);

        if(serviceTicketDetails.title() != null) {
            mTicketTitle.setText(serviceTicketDetails.title());
        }

        if(serviceTicketDetails.description() != null) {
            mTicketDescription.setText(serviceTicketDetails.description());
        }

        String assigneeName = null, assigneeAddress = null, assigneePhone = null, assigneePhone1 = null;

        if(serviceTicketDetails.assignee() != null) {

            if(serviceTicketDetails.assignee().name() != null) {
                assigneeName = serviceTicketDetails.assignee().name();
            }

            if(serviceTicketDetails.assignee().address() != null) {
                assigneeAddress  = serviceTicketDetails.assignee().address();
            }

            if(serviceTicketDetails.assignee().phone() != null) {
                assigneePhone = serviceTicketDetails.assignee().phone();
            }

            if(serviceTicketDetails.assignee().phone1() != null) {
                assigneePhone1 = serviceTicketDetails.assignee().phone1();
            }

            if(assigneeName == null) {
                assigneeName = getString(R.string.name) + " " + getString(R.string.not_available);
            }
            mTicketAssigneeName.setText(assigneeName);

            if(assigneeAddress == null) {
                assigneeAddress = getString(R.string.address) + " " + getString(R.string.not_available);
            }
            mTicketAssigneeAddress.setText(assigneeAddress);

            if(assigneePhone == null) {
                if(assigneePhone1 == null) {
                    assigneePhone = getString(R.string.phone) + " " + getString(R.string.not_available);
                    mTicketAssigneePhone.setText(assigneePhone);
                } else {
                    mTicketAssigneePhone.setText(assigneePhone1);
                }
            } else {
                mTicketAssigneePhone.setText(assigneePhone);
                if(assigneePhone1 != null) {
                    mTicketAssigneePhone.append("," + assigneePhone1);
                }
            }
        } else {

            assigneeName = getString(R.string.name) + " " + getString(R.string.not_available);
            assigneeAddress = getString(R.string.address) + " " + getString(R.string.not_available);
            assigneePhone = getString(R.string.phone) + " " + getString(R.string.not_available);

            mTicketAssigneeName.setText(assigneeName);
            mTicketAssigneeAddress.setText(assigneeAddress);
            mTicketAssigneePhone.setText(assigneePhone);
        }

        String srdCreationDate = serviceTicketDetails.createdAt();

        //convert Service Request date to dd/MM/yy format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(srdCreationDate);

        } catch (ParseException e) {
            //todo::handle exception
        }

        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        String srdDate = formatter.format(date);

        mTicketCreationDate.setText(srdDate);

        int imageBufferSize = serviceTicketDetails.logoBuffer().size();

        //check if any previously uploaded images exist
        if(imageBufferSize > 0) {
            if(getActivity() == null) {
                return;
            }

            mImagesUploadedLinearLayout.setVisibility(View.VISIBLE);

            for(int index = 0 ; index < imageBufferSize ; index++) {

                String srImageUriStr = serviceTicketDetails.logoBuffer().get(index);

                ImageView srTicketImageView = new ImageView(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                                            convertDipToPixels(100),
                                                            convertDipToPixels(100));

                layoutParams.setMargins(1, 1, 8, 16);
                srTicketImageView.setLayoutParams(layoutParams);

                //set image drawable
                Glide.with(getActivity()).load(srImageUriStr).into(srTicketImageView);

                mImagesUploadedLinearLayout.addView(srTicketImageView);

                srTicketImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(srImageUriStr != null) {
                            if(getActivity() == null) {
                                return;
                            }
                            Intent intent = new Intent(getActivity(), SimpleImageViewActivity.class);
                            getActivity().getSharedPreferences(TEMP_IMAGE_PREFS, MODE_PRIVATE).edit().putString(GENERIC_TEMP_IMAGE_KEY, srImageUriStr).apply();
                            startActivity(intent);
                        }
                    }
                });
            }

        } else {
            mImagesUploadedLinearLayout.setVisibility(GONE);
        }
    }

    private int convertDipToPixels(float dips) {

        int px = 0;
        if(getActivity() != null) {
            px = (int) (dips * getActivity().getResources().getDisplayMetrics().density + 0.5f);
        }

        return px;
    }

    private void populateServiceRequestActivities(FetchServiceRequestDetailsQuery.Get serviceTicketActivities) {

        if(getActivity() == null) {
            return;
        }

        int activitiesSize = 0;

        if(serviceTicketActivities != null && serviceTicketActivities.activities() != null) {
            activitiesSize = serviceTicketActivities.activities().size();
        }

        if(activitiesSize == 0) {
            mTicketActivitiesCardView.setVisibility(GONE);
            mServiceRequestActivitiesLinearLayout.setVisibility(GONE);
            mTicketActivitiesHeading.setVisibility(GONE);
        } else {

            mTicketActivitiesCardView.setVisibility(View.VISIBLE);
            mServiceRequestActivitiesLinearLayout.setVisibility(View.VISIBLE);
            mTicketActivitiesHeading.setVisibility(View.VISIBLE);

            for(int index = activitiesSize-1 ; index >=  0 ; index--) {

                //get service request activity
                FetchServiceRequestDetailsQuery.Activity activity = serviceTicketActivities.activities().get(index);

                //get user prefs to check and compare the user who created the activity
                User userLoggedIn = PrefUtils.getInstance(getActivity()).getUserObjectFromPrefs();

                if(userLoggedIn == null) {
                    return;
                }

                if(activity.type().toString().equals("COMMENT")) {

                    TextView srActivityCommentTextView = new TextView(getActivity());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);

                    if(activity.user().id().equals(userLoggedIn.getId())) {

                        layoutParams.gravity = RelativeLayout.ALIGN_PARENT_END;

                    } else {
                        layoutParams.gravity = RelativeLayout.ALIGN_PARENT_START;
                    }

                    layoutParams.setMargins(16, 16, 16, 16);
                    srActivityCommentTextView.setLayoutParams(layoutParams);
                    srActivityCommentTextView.setPadding(24,0,24,8);

                    //set comment text view background as per user
                    if(activity.user().id().equals(userLoggedIn.getId())) {

                        srActivityCommentTextView.setBackgroundResource(R.drawable.comment_shape_user_customer);

                    } else {
                        srActivityCommentTextView.setBackgroundResource(R.drawable.comment_shape_user_customer_service_team);
                    }

                    if(activity.user().id().equals(userLoggedIn.getId())) {

                        layoutParams.gravity = RelativeLayout.ALIGN_PARENT_END;
                        srActivityCommentTextView.setGravity(Gravity.END);

                    } else {
                        layoutParams.gravity = RelativeLayout.ALIGN_PARENT_START;
                        srActivityCommentTextView.setGravity(Gravity.START);
                    }


                    String userName = activity.user().firstName() + " " + activity.user().lastName();
                    String userComment = activity.comment();

                    //Parse createdAt datetime to required format
                    DateTime dateTime = new DateTime(activity.createdAt());
                    Date creationDate = dateTime.toDate();
                    String dateFormat = "dd/MM/yyyy hh:mm:ss";
                    String commentDateTime = new SimpleDateFormat(dateFormat).format(creationDate);

                    String userCommentBeforeSpanning = userName + "\n" + userComment + "\n" + commentDateTime;

                    int userNameSize = userName.length();
                    int startIndexOfCreationDateTime = 0;
                    int lastIndexOfCreationDateTime = 0;
                    if (userComment != null) {
                        startIndexOfCreationDateTime = userName.length() + userComment.length() + 1; //plus 1 for for a new line character
                        lastIndexOfCreationDateTime = userCommentBeforeSpanning.length();
                    }

                    //set a different size for userName and creation date
                    SpannableString userCommentDetailsAfterSpanning =  new SpannableString(userCommentBeforeSpanning);
                    userCommentDetailsAfterSpanning.setSpan(new RelativeSizeSpan(0.65f), 0,userNameSize, 0); // set size
                    userCommentDetailsAfterSpanning.setSpan(new RelativeSizeSpan(0.65f), startIndexOfCreationDateTime,lastIndexOfCreationDateTime, 0); // set size

                    srActivityCommentTextView.setText(userCommentDetailsAfterSpanning);
                    srActivityCommentTextView.setTextAppearance(getActivity(), R.style.StyleTextViewRegular16sp);
                    mServiceRequestActivitiesLinearLayout.addView(srActivityCommentTextView);

                } else if(activity.type().toString().equals("STATUS_CHANGE")) {

                    TextView srActivityStatusChangeTextView = new TextView(getActivity());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);

                    layoutParams.gravity = Gravity.CENTER;
                    layoutParams.setMargins(16, 16, 16, 16);

                    srActivityStatusChangeTextView.setLayoutParams(layoutParams);
                    srActivityStatusChangeTextView.setPadding(24,16,24,16);

                    srActivityStatusChangeTextView.setBackgroundResource(R.drawable.activities_status_change_box_shape);
                    srActivityStatusChangeTextView.setText(activity.comment());
                    srActivityStatusChangeTextView.setTextAppearance(getActivity(), R.style.StyleTextViewRegular14sp);
                    mServiceRequestActivitiesLinearLayout.addView(srActivityStatusChangeTextView);

                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File tempDir;
        File[] tempFiles;

        if (resultCode == RESULT_OK) {

            switch(requestCode) {

                case SR_PIC_OPEN_CAMERA_REQUEST_CODE :
                    if(getActivity() == null) {
                        return;
                    }

                    mServiceTicketPicTempURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mServiceTicketPicTempFile);
                    //clear previous files
                    tempDir = mServiceTicketPicTempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(SERVICE_TICKET_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mServiceTicketPicTempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    Glide.with(getActivity())
                            .load(mServiceTicketPicTempURI)
                            .into(mAddTicketImagesImageView);

                    mUploadTicketImageButton.setVisibility(View.VISIBLE);
                    break;

                case SR_PIC_OPEN_GALLERY_REQUEST_CODE :
                    if(getActivity() == null) {
                        return;
                    }

                    mServiceTicketPicTempURI = data.getData();

                    //clear previous files
                    mServiceTicketPicTempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), "place_holder_file_name");
                    tempDir = mServiceTicketPicTempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(SERVICE_TICKET_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mServiceTicketPicTempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    Glide.with(getActivity())
                            .load(mServiceTicketPicTempURI)
                            .into(mAddTicketImagesImageView);

                    mUploadTicketImageButton.setVisibility(View.VISIBLE);
                    break;
            }
        }

    }

    private void showProgressDialog(String message) {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(message);
        if(getActivity() != null && !getActivity().isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}