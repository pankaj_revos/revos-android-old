package com.revos.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.revos.android.R;
import com.revos.android.activities.ActiveBookingActivity;
import com.revos.android.activities.BookingHistoryActivity;
import com.revos.android.activities.RentalKycActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

public class RentalBookingsInfoFragment extends Fragment {

    private View mRootView;

    private CardView mActiveBookingsCardView, mBookingHistoryCardView, mKycCardView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.rental_booking_info_fragment, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {
        mActiveBookingsCardView = mRootView.findViewById(R.id.rental_active_booking_card_view);
        mBookingHistoryCardView = mRootView.findViewById(R.id.rental_booking_history_card_view);
        mKycCardView = mRootView.findViewById(R.id.rental_kyc_card_view);

        mActiveBookingsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                Intent intent = new Intent(getActivity(), ActiveBookingActivity.class);
                startActivity(intent);
            }
        });

        mBookingHistoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                Intent intent = new Intent(getActivity(), BookingHistoryActivity.class);
                startActivity(intent);
            }
        });

        mKycCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                Intent intent = new Intent(getActivity(), RentalKycActivity.class);
                startActivity(intent);
            }
        });
    }
}
