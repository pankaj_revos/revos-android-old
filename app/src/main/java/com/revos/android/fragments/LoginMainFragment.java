package com.revos.android.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.revos.android.R;
import com.revos.android.activities.LoginActivity;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_EMAIL;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_FB;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_GOOGLE;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_GUEST;
import static com.revos.android.constants.Constants.LOGIN_EVENT_SIGN_IN_PHONE;

public class LoginMainFragment extends Fragment implements View.OnClickListener {
    private View mRootView;

    /**views and layouts*/
    private LinearLayout mPrimarySignInLinearLayout, mContinuePhoneLoginLinearLayout;
    private RelativeLayout mGoogleSignInRelativeLayout, mFacebookSignInRelativeLayout,
            mEmailSignUpRelativeLayout, mGuestLoginRelativeLayout;
    private ImageView mLogoImageView;
    private EditText mPhoneLoginCountryCodeEditText, mPhoneNumberEditText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.login_main_fragment, container, false);

        setupViews();
        updateLogoIfRequired();

        return mRootView;
    }

    private void setupViews() {
        if(getActivity() == null) {
            return;
        }
        //setup logo
        mLogoImageView = (ImageView) mRootView.findViewById(R.id.login_main_logo_image_view);

        //setup layout
        mPrimarySignInLinearLayout = (LinearLayout) mRootView.findViewById(R.id.login_main_sign_in_linear_layout);
        mGoogleSignInRelativeLayout = (RelativeLayout) mRootView.findViewById(R.id.login_google_sign_in_relative_layout);
        mFacebookSignInRelativeLayout = (RelativeLayout) mRootView.findViewById(R.id.login_fb_sign_in_relative_layout);
        mContinuePhoneLoginLinearLayout = (LinearLayout) mRootView.findViewById(R.id.continue_with_phone_login_linear_layout);
        mEmailSignUpRelativeLayout = (RelativeLayout) mRootView.findViewById(R.id.login_email_sign_in_relative_layout);
        mGuestLoginRelativeLayout = (RelativeLayout) mRootView.findViewById(R.id.login_guest_sign_in_relative_layout);

        mPhoneLoginCountryCodeEditText = mRootView.findViewById(R.id.login_main_phone_country_code_edit_text);
        mPhoneNumberEditText = mRootView.findViewById(R.id.login_main_phone_number_edit_text);
        mPhoneLoginCountryCodeEditText.setText(((LoginActivity)getActivity()).mCountryPhoneCode);

        //setup click listener on layout
        mGoogleSignInRelativeLayout.setOnClickListener(this);
        mFacebookSignInRelativeLayout.setOnClickListener(this);
        mContinuePhoneLoginLinearLayout.setOnClickListener(this);
        mEmailSignUpRelativeLayout.setOnClickListener(this);
        mGuestLoginRelativeLayout.setOnClickListener(this);

    }

    private void updateLogoIfRequired() {
        if(getActivity() == null){
            return;
        }

        //see if bike logo is available
        //read info from shared prefs
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.GENERAL_PREFS, Context.MODE_PRIVATE);

        String oemLogoFilePath = sharedPreferences.getString(Constants.COMPANY_LOGO_FILE_PATH_KEY, null);

        if(oemLogoFilePath != null) {
            File file = new File(oemLogoFilePath);
            if(file.exists()) {
                //TODO::uncomment this
                /*Bitmap logoBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                logoView.setImageBitmap(logoBitmap);*/
                //TODO::comment this
                mLogoImageView.setBackgroundColor(Color.parseColor("#00FF00"));
                return;
            }
        }

        mLogoImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_center));
        mLogoImageView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.continue_with_phone_login_linear_layout:
                String phoneNumber = mPhoneNumberEditText.getText().toString();
                if(!phoneNumber.isEmpty()) {
                    EventBus.getDefault().post(new EventBusMessage(LOGIN_EVENT_SIGN_IN_PHONE + GEN_DELIMITER + phoneNumber));
                } else {
                    EventBus.getDefault().post(new EventBusMessage(LOGIN_EVENT_SIGN_IN_PHONE));
                }
                break;


            case R.id.login_google_sign_in_circle_image_view:
            case R.id.login_google_sign_in_relative_layout: EventBus.getDefault().post(new EventBusMessage(LOGIN_EVENT_SIGN_IN_GOOGLE));
                break;

            case R.id.login_fb_sign_in_circle_image_view:
            case R.id.login_fb_sign_in_relative_layout:     EventBus.getDefault().post(new EventBusMessage(LOGIN_EVENT_SIGN_IN_FB));
                break;

            case R.id.login_email_sign_in_circle_image_view:
            case R.id.login_email_sign_in_relative_layout : EventBus.getDefault().post(new EventBusMessage(LOGIN_EVENT_SIGN_IN_EMAIL));
                break;


            case R.id.login_guest_sign_in_image_view:
            case R.id.login_guest_sign_in_text_view:
            case R.id.login_guest_sign_in_relative_layout: EventBus.getDefault().post(new EventBusMessage(LOGIN_EVENT_SIGN_IN_GUEST));
            default: break;
        }
    }
}
