package com.revos.android.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.bumptech.glide.Glide;
import com.revos.android.R;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.CreateServiceTicketMutation;
import com.revos.scripts.type.ServiceTicketInput;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.revos.android.constants.Constants.FILE_PROVIDER;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;
import static com.revos.android.constants.Constants.SERVICE_TICKET_PIC_COMPRESSED_PREFIX;
import static com.revos.android.constants.Constants.SERVICE_TICKET_PIC_TEMP_PREFIX;
import static com.revos.android.constants.Constants.TEMP_PIC_DIR_PATH;
import static com.revos.android.constants.Constants.UPLOAD_TYPE_SERVICE_TICKET;

public class NewServiceRequestFragment extends Fragment {

    private View mRootView;

    /** progress dialog*/
    private ProgressDialog mProgressDialog;

    private ImageView mNSRUploadPicImageView, mNSRBackButtonImageView;

    private TextView mNSRUploadPicTextView;

    private EditText mNSRTitleEditText, mNSRDescEditText;

    private Button mCreateServiceRequestButton;

    private File mNewServiceTicketPicTempFile, mNSRTempCompressedFile;

    private Uri mNSRTicketPicTempURI;

    private String mNSRTempCompressedFileName;

    private Bitmap mNSRWorkingBitmap;

    private File mNSRPicCompressedTempFile;

    private List<String> mNSREncodedFileStringArray;

    private final int NSR_PIC_OPEN_CAMERA_REQUEST_CODE = 100;
    private final int NSR_PIC_OPEN_GALLERY_REQUEST_CODE = 101;
    private int NSR_IMAGE_SIZE_CAP = 5242880;
    private final int CAMERA_PERMISSION_REQUEST_CODE = 2000;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.new_service_request_fragment, container, false);

        setupViews();

        return mRootView;
    }

    private void setupViews() {

        mNSRTitleEditText = mRootView.findViewById(R.id.nsr_title_edit_text);
        mNSRDescEditText = mRootView.findViewById(R.id.nsr_description_edit_text);
        mNSRUploadPicImageView = mRootView.findViewById(R.id.nsr_add_image_image_view);
        mNSRUploadPicTextView = mRootView.findViewById(R.id.nsr_add_image_text_view);
        mNSRBackButtonImageView = mRootView.findViewById(R.id.nsr_back_button_image_view);
        mCreateServiceRequestButton = mRootView.findViewById(R.id.nsr_create_button);

        mNSRUploadPicImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMediaPicker();
            }
        });

        mNSRUploadPicTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMediaPicker();
            }
        });

        mNSRBackButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        mCreateServiceRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                createServiceRequest();
            }
        });
    }

    private void createServiceRequest() {

        String titleStr = mNSRTitleEditText.getText().toString();
        String descStr = mNSRDescEditText.getText().toString();

        if(titleStr.trim().isEmpty() || descStr.isEmpty()) {

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.title_desc_cannot_be_empty));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
            intent.putExtras(bundle);
            startActivity(intent);

            hideProgressDialog();

        } else {

            if(mNSREncodedFileStringArray != null && mNSREncodedFileStringArray.size() > 0) {
                if(getActivity() == null) {
                    return;
                }
                //refresh add image view
                mNSRTicketPicTempURI = null;
                mNSRUploadPicImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_add_image));

                //create new Service Request call
                makeCreateServiceRequestCall(titleStr, descStr, mNSREncodedFileStringArray);
            } else {
                makeCreateServiceRequestCall(titleStr, descStr, null);
            }
        }
    }

    private void makeCreateServiceRequestCall(String title, String description, List<String> imageLogoBuffer) {
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();
        if(vehicle == null) {
            hideProgressDialog();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_fetch_vehicle_prefs));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        String vehicleId = vehicle.getVehicleId();

        //get apollo client
        ApolloClient apolloClient = ApolloClientUtils.getInstance(getActivity()).getApolloClient();
        if(apolloClient == null) {
            hideProgressDialog();

            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_create_new_service_ticket));
            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
            intent.putExtras(bundle);
            startActivity(intent);

            return;
        }

        ServiceTicketInput serviceTicketInput = null;
        if(imageLogoBuffer != null && imageLogoBuffer.size() > 0) {

            serviceTicketInput = ServiceTicketInput
                                    .builder()
                                    .title(title)
                                    .description(description)
                                    .logoBuffer(imageLogoBuffer)
                                    .vehicle(vehicleId)
                                    .build();
        } else {

            serviceTicketInput = ServiceTicketInput
                                    .builder()
                                    .title(title)
                                    .description(description)
                                    .vehicle(vehicleId)
                                    .build();
        }

        CreateServiceTicketMutation createServiceTicketMutation = CreateServiceTicketMutation
                                                                    .builder()
                                                                    .serviceTicketInputVar(serviceTicketInput)
                                                                    .build();

        apolloClient.mutate(createServiceTicketMutation).enqueue(new ApolloCall.Callback<CreateServiceTicketMutation.Data>() {

            @Override
            public void onResponse(@NotNull Response<CreateServiceTicketMutation.Data> response) {
                if(getActivity() == null) {
                    return;
                }

                if(response.getErrors() != null && !response.getErrors().isEmpty()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();

                            Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.unable_to_create_new_service_ticket));
                            bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });

                } else {

                    if (response.getData() != null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();

                                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.ticket_created_successfully) + ": " + response.getData().support().create().id());
                                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_SUCCESS);
                                bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                                intent.putExtras(bundle);
                                startActivity(intent);

                                assert getFragmentManager() != null;
                                getFragmentManager().popBackStack();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.server_error));
                        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void openMediaPicker() {
        if(getActivity() == null) {
            return;
        }

        final Dialog mediaPickerDialog = new Dialog(getActivity());
        mediaPickerDialog.setContentView(R.layout.media_picker_dialog);

        mediaPickerDialog.findViewById(R.id.camera_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity() == null) {
                    return;
                }
                // do we have a camera?
                if(!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                    Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.no_camera_on_device));
                    bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                    bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                    intent.putExtras(bundle);
                    startActivity(intent);

                    mediaPickerDialog.dismiss();
                    return;
                }

                //check for runtime permission
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                    return;
                }

                //create temp file
                String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
                String serviceTicketPicTempFileName = SERVICE_TICKET_PIC_TEMP_PREFIX +timeStamp + ".jpg";
                mNewServiceTicketPicTempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), serviceTicketPicTempFileName);

                Uri tempPhotoURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mNewServiceTicketPicTempFile);
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPhotoURI);
                startActivityForResult(takePicture, NSR_PIC_OPEN_CAMERA_REQUEST_CODE);
                mediaPickerDialog.dismiss();
            }
        });

        mediaPickerDialog.findViewById(R.id.gallery_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , NSR_PIC_OPEN_GALLERY_REQUEST_CODE);
                mediaPickerDialog.dismiss();
            }
        });

        mediaPickerDialog.show();
    }


    private class NewSRCompressPicTask extends AsyncTask<String, String, File> {

        @Override
        protected File doInBackground(String... params) {
            if(getActivity() == null){
                return null;
            }

            Uri newSrPicTempUri;
            String newSrPicTempPrefix;

            //compress the profile pic before uploading
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());

            newSrPicTempPrefix = SERVICE_TICKET_PIC_COMPRESSED_PREFIX;
            newSrPicTempUri = mNSRTicketPicTempURI;

            mNSRTempCompressedFileName = newSrPicTempPrefix + timeStamp + ".jpg";
            mNSRTempCompressedFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), mNSRTempCompressedFileName);
            try {
                mNSRWorkingBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), newSrPicTempUri);
                FileOutputStream compressedFileOutputStream = new FileOutputStream(mNSRTempCompressedFile);
                mNSRWorkingBitmap.compress(Bitmap.CompressFormat.JPEG, 75, compressedFileOutputStream);
                compressedFileOutputStream.flush();
                compressedFileOutputStream.close();
                mNSRWorkingBitmap.recycle();

            } catch (Exception e) {
                //todo:: handle this
                hideProgressDialog();
            }

            mNSRPicCompressedTempFile = mNSRTempCompressedFile;

            return mNSRTempCompressedFile;
        }

        @Override
        protected void onPostExecute(File mNewSrTempCompressedFile) {
            super.onPostExecute(mNewSrTempCompressedFile);
            //check image size
            if(mNewSrTempCompressedFile.length() > NSR_IMAGE_SIZE_CAP) {

                Intent intent = new Intent(getActivity(), GenericMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(GENERIC_MESSAGE_TEXT_KEY, getString(R.string.image_too_large));
                bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
                bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, getString(R.string.okay));
                intent.putExtras(bundle);
                startActivity(intent);

                hideProgressDialog();
                return;
            }

            //populate the image buffer
            populateImageBuffer();
        }
    }

    private void populateImageBuffer() {

        final File srTempCompressedFile;
        final File srTempFileFromCamera;

        srTempCompressedFile = mNSRPicCompressedTempFile;
        srTempFileFromCamera = mNewServiceTicketPicTempFile;

        try {

            byte[] srFileBytesArray = new byte[(int) srTempCompressedFile.length()];
            FileInputStream fis = new FileInputStream(srTempCompressedFile);
            fis.read(srFileBytesArray); //read file into bytes[]
            fis.close();
            String srEncodedFileString = Base64.encodeToString(srFileBytesArray, Base64.DEFAULT);

            String encodedString = "data:image/jpg;base64," + srEncodedFileString;

            mNSREncodedFileStringArray = new ArrayList<>();
            mNSREncodedFileStringArray.add(encodedString);

            clearTempFiles();

        } catch (Exception e) {

            //todo::handle file write exception
            //delete the temp files
            if(srTempFileFromCamera.exists()) { srTempFileFromCamera.delete(); }
            if(srTempCompressedFile.exists()) { srTempCompressedFile.delete(); }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File tempDir;
        File[] tempFiles;

        if (resultCode == RESULT_OK) {

            switch(requestCode) {

                case NSR_PIC_OPEN_CAMERA_REQUEST_CODE :
                    if(getActivity() == null) {
                        return;
                    }

                    mNSRTicketPicTempURI = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, mNewServiceTicketPicTempFile);
                    //clear previous files
                    tempDir = mNewServiceTicketPicTempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(SERVICE_TICKET_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mNewServiceTicketPicTempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    Glide.with(getActivity())
                            .load(mNSRTicketPicTempURI)
                            .into(mNSRUploadPicImageView);

                    new NewSRCompressPicTask().execute(UPLOAD_TYPE_SERVICE_TICKET);

                    break;

                case NSR_PIC_OPEN_GALLERY_REQUEST_CODE :
                    if(getActivity() == null) {
                        return;
                    }

                    mNSRTicketPicTempURI = data.getData();

                    //clear previous files
                    mNewServiceTicketPicTempFile = new File(getActivity().getExternalFilesDir(TEMP_PIC_DIR_PATH), "place_holder_file_name");
                    tempDir = mNewServiceTicketPicTempFile.getParentFile();
                    tempFiles = tempDir.listFiles();
                    for(int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(SERVICE_TICKET_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(mNewServiceTicketPicTempFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    Glide.with(getActivity())
                            .load(mNSRTicketPicTempURI)
                            .into(mNSRUploadPicImageView);

                    new NewSRCompressPicTask().execute(UPLOAD_TYPE_SERVICE_TICKET);

                    break;
            }
        }
    }

    private void clearTempFiles() {

        if(mNSRPicCompressedTempFile.exists()) {
            mNSRPicCompressedTempFile.delete();
        }

        if(mNewServiceTicketPicTempFile.exists()) {
            mNewServiceTicketPicTempFile.delete();
        }
    }

    private void showProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = null;
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.creating_service_request));
        if(getActivity() != null && !getActivity().isFinishing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        hideProgressDialog();
        super.onDestroy();
    }
}