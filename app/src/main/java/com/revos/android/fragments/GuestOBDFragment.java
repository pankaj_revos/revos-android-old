package com.revos.android.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.revos.android.R;
import com.revos.android.activities.MainActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.progressBars.DirectionProgressBar;
import com.revos.scripts.type.ModelProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import at.grabner.circleprogress.CircleProgressView;
import timber.log.Timber;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.revos.android.services.NordicBleService.mCurrentLocation;

/**
 * Created by mohit on 11/9/17.
 */

public class GuestOBDFragment extends Fragment{
    private Context mActivityContext;
    /**views and layouts*/
    private View rootView;

    private DirectionProgressBar mLeftTurnDirectionProgressBar, mRightTurnDirectionProgressBar;

    private ImageView mDisconnectImageView, mPowerButtonImageView, mTurnIconImageView, mOBDVehicleLockedTrueImageView,
                      mOBDBatteryImageView, mOBDHeadlightImageView, mOBDBrakeImageView, mOBDFindMyVehicleImageView, mOBDPnPUnlockButtonImageView, mOBDPnPLockButtonImageView;

    private LottieAnimationView mOBDFindMyVehicleLottieView, mOBDPnPUnlockButtonLottieView, mOBDPnPLockButtonLottieView;

    private LottieAnimationView mOBDErrorCodeImageView;


    private TextView mDistanceFromTurnTextView, mNextRoadTextView, mETALabelTextView, mETAValueTextView, mSpeedValueTextView,
                     mOBDModeValueTextView, mOBDOdoValueTextView, mOBDOdoLabelTextView, mOBDOdoUnitTextView, mOBDErrorCodeTextView,
                     mOBDBatteryLabelTextView, mOBDRangeTextView, mOBDRangeUnitTextView, mOBDConnectingToVehicleTextView, mOBDConnectUsingVINTextView;

    private TextClock mTextClock;

    private CircleProgressView mSpeedCircleProgressView, mOBDErrorCodeCircleProgressView;

    /**Timer for polling*/
    private Timer mTimer;

    /**Bound for demo mode*/
    Integer[] setBoundArray;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.obd_fragment, container, false);

        mActivityContext = getActivity();

        setupViews();

        setBound();

        clearTimer();

        setupTimer();

        return rootView;
    }

    private void setupTimer() {
        //start infinite polling for checking bluetooth connection state
        //check if notification advice has to be displayed or not
        if(mTimer == null) {
            mTimer = new Timer();
        }

        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getActivity() == null || !getActivity().hasWindowFocus()) {
                                return;
                            }
                            animateViewsForDemoMode();

                        } catch (Exception e) {
                            FirebaseCrashlytics.getInstance().recordException(e);
                        }
                    }
                });
            }
        }, 100, 500);
    }

    private void clearTimer() {
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
    }

    private void setupViews() {
        setupOBDRelatedViews();

        if(getActivity() == null) {
            return;
        }
    }

    /**Set array speed, battery and range bounds*/
    private void setBound(){

        setBoundArray = new Integer[12];

        for(int i = 0; i <= 10; i++)
        {
            setBoundArray[i] = i * 10;
        }

    }

    private void animateViewsForGpsTrackerMode() {
        if(getActivity() == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(getActivity()).getVehicleObjectFromPrefs();

        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            int speed = -1;
            if(mCurrentLocation != null && mCurrentLocation.hasSpeed()) {
                speed = Math.round(mCurrentLocation.getSpeed() * 18 / 5);
            }
            updateSpeedRelatedViews(speed, false, -1, false);
        }
    }

    private void animateViewsForDemoMode() {
        if(getActivity() == null) {
            return;
        }

        long currentTime = System.currentTimeMillis() / 1000;
        int index = (int) (currentTime % 10);

        int setBound = setBoundArray[index];

        int modeNumber = new Random().nextInt(3) + 1;
        updateSpeedRelatedViews(setBound, false, modeNumber, new Random().nextBoolean());

        int lightRandomNumber = new Random().nextInt(10);
        int brakeRandomNumber = new Random().nextInt(10);

        int lightVisibility = lightRandomNumber % 2 == 0 ? VISIBLE : INVISIBLE;
        int brakeVisibility = brakeRandomNumber % 3 == 0 ? VISIBLE : INVISIBLE;

        mOBDHeadlightImageView.setVisibility(lightVisibility);
        mOBDBrakeImageView.setVisibility(brakeVisibility);

        updateBatteryLayout(setBound, setBound);

        mOBDRangeTextView.setVisibility(VISIBLE);
        mOBDRangeUnitTextView.setVisibility(VISIBLE);
        updateRange(setBound);

    }

    private void setupOBDRelatedViews() {
        if(getActivity() == null)
            return;


        mDisconnectImageView = (ImageView) rootView.findViewById(R.id.obd_disconnect_image_view);
        mDisconnectImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_bluetooth_disconnect));

        mPowerButtonImageView = (ImageView) rootView.findViewById(R.id.obd_power_button_image_view);

        mOBDPnPLockButtonImageView = (ImageView) rootView.findViewById(R.id.obd_pnp_lock_button_image_view);
        mOBDPnPLockButtonLottieView = (LottieAnimationView) rootView.findViewById(R.id.obd_pnp_lock_button_lottie_view);
        mOBDPnPUnlockButtonImageView = (ImageView) rootView.findViewById(R.id.obd_pnp_unlock_button_image_view);
        mOBDPnPUnlockButtonLottieView = (LottieAnimationView) rootView.findViewById(R.id.obd_pnp_unlock_button_lottie_view);

        mOBDPnPLockButtonImageView.setVisibility(VISIBLE);
        mOBDPnPUnlockButtonImageView.setVisibility(VISIBLE);

        mOBDPnPUnlockButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });

        mOBDPnPLockButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();

            }
        });

        mOBDModeValueTextView = (TextView) rootView.findViewById(R.id.obd_mode_value_text_view);
        mOBDOdoValueTextView = (TextView) rootView.findViewById(R.id.obd_odo_value_text_view);
        mOBDOdoLabelTextView = (TextView) rootView.findViewById(R.id.obd_odo_label_text_view);
        mOBDOdoUnitTextView = (TextView) rootView.findViewById(R.id.obd_odo_unit_text_view);

        mLeftTurnDirectionProgressBar = (DirectionProgressBar)rootView.findViewById(R.id.obd_left_turn_direction_progress_bar);
        mRightTurnDirectionProgressBar = (DirectionProgressBar)rootView.findViewById(R.id.obd_right_turn_direction_progress_bar);
        mTextClock = (TextClock)rootView.findViewById(R.id.obd_text_clock);
        mSpeedValueTextView = (TextView)rootView.findViewById(R.id.obd_speed_value_text_view);
        mSpeedCircleProgressView = (CircleProgressView)rootView.findViewById(R.id.obd_speed_circle_progress_view);
        mOBDErrorCodeCircleProgressView = (CircleProgressView) rootView.findViewById(R.id.obd_error_code_circle_progress_view);
        mOBDErrorCodeImageView = (LottieAnimationView) rootView.findViewById(R.id.obd_error_code_image_view);
        mOBDErrorCodeTextView = (TextView) rootView.findViewById(R.id.obd_error_code_text_view);
        mOBDVehicleLockedTrueImageView = (ImageView) rootView.findViewById(R.id.obd_vehicle_locked_true);

        mOBDBatteryLabelTextView = (TextView) rootView.findViewById(R.id.obd_battery_label_text_view);
        mOBDBatteryImageView = (ImageView) rootView.findViewById(R.id.obd_battery_image_view);
        mOBDRangeTextView = (TextView) rootView.findViewById(R.id.obd_range_text_view);
        mOBDRangeUnitTextView = (TextView) rootView.findViewById(R.id.obd_range_unit_text_view);

        mOBDHeadlightImageView = (ImageView) rootView.findViewById(R.id.obd_headlight_image_view);
        mOBDBrakeImageView = (ImageView) rootView.findViewById(R.id.obd_brake_image_view);
        mOBDFindMyVehicleImageView = (ImageView) rootView.findViewById(R.id.obd_find_my_vehicle_image_view);
        mOBDFindMyVehicleLottieView = (LottieAnimationView) rootView.findViewById(R.id.obd_find_my_vehicle_lottie_view);

        mPowerButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();

            }
        });

        mDisconnectImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();

            }
        });

        mOBDFindMyVehicleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();

            }
        });

        mOBDVehicleLockedTrueImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() == null){
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();

            }
        });

        mOBDErrorCodeCircleProgressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });

        mOBDErrorCodeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });

        mOBDErrorCodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getActivity() == null) {
                    return;
                }

                ((MainActivity)getActivity()).showRegisterAsUserActivity();
            }
        });

        setupNavigationAdviceRelatedViews();

    }


    private void setupNavigationAdviceRelatedViews() {

        mETALabelTextView = (TextView)rootView.findViewById(R.id.obd_eta_label_text_view);
        mETAValueTextView = (TextView)rootView.findViewById(R.id.obd_eta_value_text_view);

    }

    private void updateSpeedRelatedViews(int speed, boolean isParking, int mode, boolean isReverse) {
        if(getActivity() == null) {
            return;
        }
        int speedInKMPH = speed;
        Timber.d("%s", speedInKMPH);
        if(mSpeedValueTextView != null && mSpeedCircleProgressView != null) {
            if(!isParking) {
                if(speedInKMPH == -1) {
                    mSpeedValueTextView.setText("--");
                } else {
                    mSpeedValueTextView.setText(String.valueOf(speedInKMPH));
                }
                mOBDModeValueTextView.setVisibility(VISIBLE);
            } else {
                mSpeedValueTextView.setText("P");
                mOBDModeValueTextView.setVisibility(INVISIBLE);
            }
            mSpeedCircleProgressView.setValueAnimated(speedInKMPH, 100);
        }

        if(mOBDModeValueTextView != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getActivity().getTheme();
            theme.resolveAttribute(R.attr.colorRegularText, typedValue, true);
            @ColorInt int textColor = typedValue.data;
            if(isReverse) {
                theme.resolveAttribute(R.attr.colorBatteryLow, typedValue, true);
                textColor = typedValue.data;
                mOBDModeValueTextView.setTextColor(textColor);
                mOBDModeValueTextView.setText("R");
            } else {
                theme.resolveAttribute(R.attr.colorTextOBD, typedValue, true);
                textColor = typedValue.data;
                mOBDModeValueTextView.setTextColor(textColor);
                switch (mode) {
                    case 0 : if(Locale.getDefault().getLanguage().equals("en")) {
                                mOBDModeValueTextView.setText(getString(R.string.vehicle_mode_economy));
                             } else {
                                mOBDModeValueTextView.setText(String.format("MODE %s", String.valueOf(mode+1)));
                             }
                             break;

                    case 1 : if(Locale.getDefault().getLanguage().equals("en")) {
                                mOBDModeValueTextView.setText(getString(R.string.vehicle_mode_ride));
                             } else {
                                mOBDModeValueTextView.setText(String.format("MODE %s", String.valueOf(mode+1)));
                             }
                                break;

                    case 2 : if(Locale.getDefault().getLanguage().equals("en")) {
                                mOBDModeValueTextView.setText(getString(R.string.vehicle_mode_sport));
                             } else {
                                mOBDModeValueTextView.setText(String.format("MODE %s", String.valueOf(mode+1)));
                             }
                                break;
                }
            }
        }
    }


    @Subscribe
    public void onEventBusMessage(EventBusMessage event) {

    }

    private void updateRange(int value) {
        mOBDRangeTextView.setText(String.valueOf(value));
    }

    private void updateBatteryLayout(long socValue, double batteryVoltage) {
        if(getActivity() == null) {
            return;
        }
        mOBDBatteryLabelTextView.setText(getString(R.string.battery).concat(" : ").concat(String.valueOf(Math.round(Math.floor(batteryVoltage)))).concat(" V"));

        if(socValue < 5) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_warning_0));
        } else if(socValue >= 5 && socValue <= 10) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_warning_10));
        } else if(socValue > 10 && socValue <= 20) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_warning_20));
        } else if(socValue > 20 && socValue <= 40) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_30));
        } else if(socValue > 40 && socValue <= 50) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_40));
        } else if(socValue > 50 && socValue <= 60) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_50));
        } else if(socValue > 60 && socValue <= 70) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_60));
        } else if(socValue > 70 && socValue <= 80) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_70));
        } else if(socValue > 80 && socValue <= 90) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_80));
        } else if(socValue > 90 && socValue <= 95) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_90));
        } else if(socValue > 95) {
            mOBDBatteryImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_graphic_battery_100));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //register with the EventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        //unregister with the EventBus
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        clearTimer();

        super.onDestroy();
    }

}
