package com.revos.android.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface NewsRainApiInterface {

    @GET("petrol-diesel-prices")
    Call<ResponseBody> getPetrolPrice();
}
