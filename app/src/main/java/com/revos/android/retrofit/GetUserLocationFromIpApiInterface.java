package com.revos.android.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetUserLocationFromIpApiInterface {

    @GET("api")
    Call<ResponseBody> getUserLocation();
}
