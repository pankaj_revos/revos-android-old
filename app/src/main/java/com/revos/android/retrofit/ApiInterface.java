package com.revos.android.retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @Headers("Content-Type: application/json")
    @POST("vehicles/{VIN}/exec")
    Call<ResponseBody> vehicleExecuteCommand(@Path("VIN") String VIN,
                                             @Header("token") String token,
                                             @Header("Authorization") String bearerToken,
                                             @Body RequestBody requestBody);


    @Headers("Content-Type: application/json")
    @GET("charger")
    Call<ResponseBody> getAvailableChargers(@Query("lat_top") double latTop,
                                            @Query("long_left") double longLeft,
                                            @Query("lat_bottom") double latBottom,
                                            @Query("long_right") double longRight,
                                            @Header("token") String token,
                                            @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("charger/{UID}")
    Call<ResponseBody> getChargerById(@Path("UID") String UID,
                                      @Header("token") String token,
                                      @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("user/bookings")
    Call<ResponseBody> getUserBookings(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @POST("charger/{UID}/book")
    Call<ResponseBody> bookCharger(@Path("UID") String UID,
                                   @Header("token") String token,
                                   @Header("Authorization") String bearerToken,
                                   @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("charger/{bookingId}/end")
    Call<ResponseBody> endBooking(@Path("bookingId") String bookingId,
                                  @Header("token") String token,
                                  @Header("Authorization") String bearerToken,
                                  @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("charger/{bookingId}/update")
    Call<ResponseBody> updateBooking(@Path("bookingId") String bookingId,
                                     @Header("token") String token,
                                     @Header("Authorization") String bearerToken,
                                     @Body RequestBody requestBody);


    @Headers("Content-Type: application/json")
    @GET("charger/products")
    Call<ResponseBody> getChargerModels(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @POST("charger/register")
    Call<ResponseBody> registerCharger(@Header("token") String token,
                                       @Header("Authorization") String bearerToken,
                                       @Body RequestBody requestBody);


    @Headers("Content-Type: application/json")
    @GET("user/chargers")
    Call<ResponseBody> getMyChargers(@Header("token") String token, @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("user/earnings")
    Call<ResponseBody> getEarnings(@Header("token") String token, @Header("Authorization") String bearerToken);

}
