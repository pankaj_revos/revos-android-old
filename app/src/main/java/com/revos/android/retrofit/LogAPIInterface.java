package com.revos.android.retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LogAPIInterface {

    @POST("mobile")
    Call<ResponseBody> uploadLog(@Query ("authToken") String authToken, @Body RequestBody requestBody);

}