package com.revos.android.retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface DataFeedApiInterface {

    @Headers("Content-Type: application/json")
    @POST("v2/getAllRentalVehicles")
    Call<ResponseBody> getAllRentalVehicles(@Query("package") String packageName,
                                            @Header("Authorization") String bearerToken,
                                            @Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @GET("/v2/trips")
    Call<ResponseBody> getAllTripsForVehicle(@Query("vin") String vin,
                                             @Query("first") int first,
                                             @Query("skip") int skip,
                                             @Header("Authorization") String bearerToken);


    @Headers("Content-Type: application/json")
    @GET("tripV2")
    Call<ResponseBody> getTripForVehicle(@Query("tripid") String tripId,
                                         @Header("Authorization") String bearerToken);


    @Headers("Content-Type: application/json")
    @GET("v2/vehicleSnapshot")
    Call<ResponseBody> getVehicleLocationSnapshot(@Query("vin") String vin,
                                                  @Header("Authorization") String bearerToken);


    @Headers("Content-Type: application/json")
    @GET("v2/vehicleLogsV2")
    Call<ResponseBody> getVehicleLiveLogForCount(@Query("vin") String vin,
                                                 @Query("startTime") String startTime,
                                                 @Query("endTime") String endTime,
                                                 @Query("tripId") String tripId,
                                                 @Query("count") int count,
                                                 @Query("mobile") boolean mobile,
                                                 @Header("Authorization") String bearerToken);

    @Headers("Content-Type: application/json")
    @GET("v2/vehicleLogsV2")
    Call<ResponseBody> getVehicleLiveLogWithoutCount(@Query("vin") String vin,
                                                     @Query("startTime") String startTime,
                                                     @Query("endTime") String endTime,
                                                     @Query("tripId") String tripId,
                                                     @Query("mobile") boolean mobile,
                                                     @Header("Authorization") String bearerToken);

}
