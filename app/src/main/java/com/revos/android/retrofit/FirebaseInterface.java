package com.revos.android.retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface FirebaseInterface {

    @Headers("Content-Type: application/json")
    @POST("usertoken")
    Call<ResponseBody> sendFirebaseToken(@Header("Authorization") String bearerToken,@Body RequestBody requestBody);
}
