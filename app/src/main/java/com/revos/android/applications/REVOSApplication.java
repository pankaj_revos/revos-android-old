package com.revos.android.applications;

import android.app.Application;
import com.google.android.libraries.places.api.Places;
import com.revos.android.BuildConfig;
import timber.log.Timber;

public class REVOSApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //initialize places
        Places.initialize(getApplicationContext(), "AIzaSyCIE9einCxr6rgfx1vC9IVGRpsshW6_bFM");

        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }
    }
}
