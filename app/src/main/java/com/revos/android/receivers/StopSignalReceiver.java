package com.revos.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.revos.android.events.EventBusMessage;

import org.greenrobot.eventbus.EventBus;

import static com.revos.android.constants.Constants.GENERAL_PREFS;
import static com.revos.android.constants.Constants.KILL_SWITCH;
import static com.revos.android.constants.Constants.KILL_SWITCH_FLAG_KEY;

public class StopSignalReceiver extends BroadcastReceiver {
    public StopSignalReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(new EventBusMessage(KILL_SWITCH));
        context.getSharedPreferences(GENERAL_PREFS, Context.MODE_PRIVATE).edit().putBoolean(KILL_SWITCH_FLAG_KEY, true).apply();
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }
}
