package com.revos.android.services;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.User;
import com.revos.android.retrofit.LogAPIClient;
import com.revos.android.retrofit.LogAPIInterface;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.security.EncryptionHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.revos.android.constants.Constants.*;

/**
 * Created by mohit on 4/9/18.
 */

public class NetworkJobService extends JobService {
    private static boolean mIsLiveLogUploadInProgress = false;
    private static boolean mIsOfflineLogUploadInProgress = false;
    private static boolean mIsOfflineBatteryAdcLogUploadInProgress = false;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Timber.d("Job executed at => %s", System.currentTimeMillis());

        decideAndUploadNextLogFile();
        rescheduleJob();
        return true;
    }

    private void decideAndUploadNextLogFile() {

        if(mIsLiveLogUploadInProgress && mIsOfflineLogUploadInProgress && mIsOfflineBatteryAdcLogUploadInProgress) {
            return;
        }

        File latestLiveLogFileToBeUploaded = getLatestLogFileToBeUploaded(LOG_TYPE_LIVE);
        if(latestLiveLogFileToBeUploaded != null && !mIsLiveLogUploadInProgress) {
            uploadLogFile(latestLiveLogFileToBeUploaded, LOG_TYPE_LIVE);
        }

        File latestOfflineLogFileToBeUploaded = getLatestLogFileToBeUploaded(LOG_TYPE_OFFLINE);
        if (latestOfflineLogFileToBeUploaded != null && !mIsOfflineLogUploadInProgress) {
            uploadLogFile(latestOfflineLogFileToBeUploaded, LOG_TYPE_OFFLINE);
        }

        File latestOfflineBatteryAdcLogFileToBeUploaded = getLatestLogFileToBeUploaded(LOG_TYPE_OFFLINE_BATTERY_ADC);
        if (latestOfflineBatteryAdcLogFileToBeUploaded != null && !mIsOfflineBatteryAdcLogUploadInProgress) {
            uploadLogFile(latestOfflineBatteryAdcLogFileToBeUploaded, LOG_TYPE_OFFLINE_BATTERY_ADC);
        }

    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }

    private void rescheduleJob() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if(jobScheduler == null) {
            return;
        }

        ComponentName serviceComponent = new ComponentName(getApplicationContext(),  NetworkJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(14, serviceComponent);
        builder.setMinimumLatency(5 * 1000); // wait at least
        builder.setOverrideDeadline(10 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not

        jobScheduler.schedule(builder.build());
    }

    private File getLatestLogFileToBeUploaded(String vehicleLogType) {
        SharedPreferences sharedPreferences = getSharedPreferences(LOG_PREFS, MODE_PRIVATE);

        //check if there are any local files to sync, if not then abort
        String logDirKey = VEHICLE_OFFLINE_LOG_DIR_KEY;

        if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
            logDirKey = VEHICLE_LIVE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
            logDirKey = VEHICLE_OFFLINE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)){
            logDirKey = VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY;
        }

        String logDirStr = sharedPreferences.getString(logDirKey, null);
        if(logDirStr == null) {
            return null;
        }

        File logDir = new File(logDirStr);
        if(!logDir.exists()) {
            return null;
        }

        File[] logFiles = logDir.listFiles();
        if(logFiles == null || logFiles.length == 0) {
            return null;
        }

        ArrayList<String> logFileList = new ArrayList<>();

        //sort file with latest at position zero
        for (File logFile: logFiles) {
            logFileList.add(logFile.getName());
        }

        Collections.sort(logFileList);

        Collections.reverse(logFileList);

        File latestLogFile = new File(logDirStr, logFileList.get(0));

        //check if this file is not the one being written into
        String workingLogFileKey = WORKING_OFFLINE_LOG_FILE_NAME_KEY;
        if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
            workingLogFileKey = WORKING_LIVE_LOG_FILE_NAME_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
            workingLogFileKey = WORKING_OFFLINE_LOG_FILE_NAME_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)){
            workingLogFileKey = WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY;
        }
        String workingLogFile = getSharedPreferences(LOG_PREFS, MODE_PRIVATE).getString(workingLogFileKey, null);

        //if yes then take the next file to sync
        if(workingLogFile != null && workingLogFile.equals(latestLogFile.getName()) && logFileList.size() > 1) {
            latestLogFile = new File(logDirStr, logFileList.get(1));
        }

        if(!latestLogFile.getName().equals(workingLogFile)) {
            return latestLogFile;
        } else {
            return null;
        }
    }

    private void uploadLogFile(File logFile, String vehicleLogType) {
        String userToken = getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_TOKEN_KEY, null);

        if(userToken == null) {
            if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
                mIsLiveLogUploadInProgress = false;
            } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
                mIsOfflineLogUploadInProgress = false;
            } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)){
                mIsOfflineBatteryAdcLogUploadInProgress = false;
            }

            return;
        }

        if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
            mIsLiveLogUploadInProgress = true;
        }
        if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
            mIsOfflineLogUploadInProgress = true;
        }
        if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)) {
            mIsOfflineBatteryAdcLogUploadInProgress = true;
        }
        try {

            JsonArray vehicleLogJsonArray = createLogJSONArray(logFile);
            if(vehicleLogJsonArray == null) {
                //delete this inconsistent file
                if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
                    mIsLiveLogUploadInProgress = false;
                }
                if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
                    mIsOfflineLogUploadInProgress = false;
                }
                if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)) {
                    mIsOfflineBatteryAdcLogUploadInProgress = false;
                }

                deleteLogFile(logFile.getName(), vehicleLogType);

                return;
            }

            JsonObject bodyJsonObject = new JsonObject();
            bodyJsonObject.addProperty("tag", logFile.getName());
            bodyJsonObject.add("data", vehicleLogJsonArray);

            LogAPIInterface logAPIInterface = LogAPIClient.getClient().create(LogAPIInterface.class);

            String bodyStr = new Gson().toJson(bodyJsonObject);

            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), bodyStr);

            logAPIInterface.uploadLog(userToken, requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
                        mIsLiveLogUploadInProgress = false;
                    }
                    if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
                        mIsOfflineLogUploadInProgress = false;
                    }
                    if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)) {
                        mIsOfflineBatteryAdcLogUploadInProgress = false;
                    }
                    if(response.code() == 200) {
                        try {
                            if(response.body() == null) {
                                decideAndUploadNextLogFile();
                                return;
                            }
                            String bodyStr = response.body().string();
                            JsonObject jsonObject = new JsonParser().parse(bodyStr).getAsJsonObject();
                            getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putBoolean(TRIP_DATA_NEEDS_REFRESH_KEY, true).apply();
                            if(jsonObject.has("tag")) {
                                deleteLogFile(jsonObject.get("tag").getAsString(), vehicleLogType);
                            }
                            decideAndUploadNextLogFile();
                        } catch (Exception e) {
                        }

                    } else {
                        decideAndUploadNextLogFile();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
                        mIsLiveLogUploadInProgress = false;
                    }
                    if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
                        mIsOfflineLogUploadInProgress = false;
                    }
                    if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)) {
                        mIsOfflineBatteryAdcLogUploadInProgress = false;
                    }
                    decideAndUploadNextLogFile();
                }
            });

        } catch (Exception e) {
            //nothing to do here
            if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
                mIsLiveLogUploadInProgress = false;
            }
            if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
                mIsOfflineLogUploadInProgress = false;
            }
            if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)) {
                mIsOfflineBatteryAdcLogUploadInProgress = false;
            }
        }
    }

    private JsonArray createLogJSONArray(File logPartFile) {
        JsonArray vehicleLogJSONArray = new JsonArray();

        try (BufferedReader br = new BufferedReader(new FileReader(logPartFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.isEmpty()) {
                    byte[] encryptedBytes = Base64.decode(line, Base64.NO_WRAP | Base64.URL_SAFE);
                    String decryptedString = null;
                    try {
                        byte[] decryptedBytes = EncryptionHelper.decryptBytes(encryptedBytes, constructEncryptionKey());
                        decryptedString = new String(decryptedBytes);
                    } catch (Exception e) {
                        continue;
                    }

                    JsonObject logJsonObject = new JsonParser().parse(decryptedString).getAsJsonObject();
                    User user = PrefUtils.getInstance(getApplicationContext()).getUserObjectFromPrefs();

                    if (!logJsonObject.has(LOG_VIN) || user == null) {
                        continue;
                    } else {
                        logJsonObject.addProperty(LOG_RIDER_ID, user.getId());
                        vehicleLogJSONArray.add(logJsonObject);
                    }

                }
            }
            return vehicleLogJSONArray;
        } catch (Exception e) {
            return  null;
        }
    }

    private void deleteLogFile(String fileName, String vehicleLogType) {
        if(fileName == null || fileName.isEmpty()) {
            return;
        }

        String logDirKey = VEHICLE_OFFLINE_LOG_DIR_KEY;

        if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
            logDirKey = VEHICLE_LIVE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
            logDirKey = VEHICLE_OFFLINE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)){
            logDirKey = VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY;
        }

        String logDir = getSharedPreferences(LOG_PREFS, MODE_PRIVATE).getString(logDirKey, null);
        if(logDir != null) {
            File uploadedFile = new File(logDir, fileName);
            if(uploadedFile.exists()) {
                uploadedFile.delete();
            }
        }
    }

    private String constructEncryptionKey() {

        Device cloudDevice = PrefUtils.getInstance(getApplicationContext()).getDeviceObjectFromPrefs();
        String devicePin = getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        if(cloudDevice == null || devicePin == null) {
            return null;
        }

        byte[] aesKey = Base64.decode(cloudDevice.getKey(), Base64.NO_WRAP | Base64.URL_SAFE);

        for(int byteIndex = 0; byteIndex < devicePin.length(); ++byteIndex) {
            aesKey[aesKey.length - devicePin.length() + byteIndex] = (byte)devicePin.charAt(byteIndex);
        }

        return Base64.encodeToString(aesKey, Base64.NO_WRAP|Base64.URL_SAFE);
    }

}
