package com.revos.android.constants;

import android.Manifest;

import com.revos.android.R;

import java.util.HashMap;

/**
 * Created by mohit on 29/8/17.
 */

public interface Constants {

    int SERVICE_KILL_THRESHOLD = 3000;
    int SOS_COOL_OFF_IN_MILLIS = 60000;
    int SOS_COOL_OFF = 60;
    float OVER_TEMPERATURE_LIMIT = 90.0f;

    int REVOS_DEVICE_ID_LENGTH = 37;


    //tab positions
    int POS_NAV_TAB = 0;
    int POS_OBD_TAB = 1;
    int POS_ANALYTICS_TAB = 2;

    //general prefs
    String GENERAL_PREFS = "generalPrefs";
    String FIREBASE_INSTANCE_ID_KEY = "firebase_instance_id_key";
    String COMPANY_LOGO_FILE_PATH_KEY = "company_logo_file_path_key";
    String KILL_SWITCH_FLAG_KEY = "kill_switch_flag_key";
    String LOG_OUT_FLAG_KEY = "log_out_flag_key";
    String CONTACT_LIST_BUILT_FLAG_KEY = "contact_list_built_flag_key";
    String NOTIFICATION_LISTENER_CONNECTED_KEY = "notification_listener_connected_key";
    String SHARED_PREFS_CLEARED_VERSION_CODE_KEY = "app_prefs_cleared_version_code_key";

    //app prefs
    String APP_PREFS = "appPrefs";
    String IS_APP_IN_GUEST_MODE = "is_app_in_guest_mode_key";
    String NOTIFICATION_SERVICE_BINDED_KEY = "notification_service_binded";
    String LIGHT_SENSOR_AVAILABLE = "light_sensor_available_key";
    String CRASH_TIMESTAMP_KEY = "crash_timestamp_key";
    String IS_APP_IN_INDIA_KEY = "is_app_in_india_key";

    //phone prefs
    String PHONE_PREFS = "phonePrefs";
    String CONTACT_LIST_KEY = "contact_list_key";
    String NUMBER_IN_ACTION_KEY = "number_in_action_key";
    String CONTACT_IN_ACTION_KEY = "contact_in_action_key";
    String PHONE_STATE_KEY = "phone_state_key";
    String CALL_LOG_HASHMAP_KEY = "call_log_hashmap_key";

    //user prefs
    String USER_PREFS = "userPrefs";
    String USER_VERIFIED_PHONE_NUMBER = "user_verified_phone_number";
    String IS_USER_PHONE_NUMBER_VERIFIED = "is_user_phone_number_verified";
    String USER_JSON_DATA_KEY = "user_json_data_key";
    String USER_TOKEN_KEY = "user_token_key";
    String USER_PROFILE_LOCAL_FILE_PATH_KEY = "user_profile_file_path_key";
    String USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY = "user_dl_side_1_file_path_key";
    String USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY = "user_dl_side_2_file_path_key";
    String USER_PROFILE_UPLOAD_FILE_NAME_KEY = "user_profile_upload_file_name_key";
    String USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY = "user_dl_side_1_upload_file_name_key";
    String USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY = "user_dl_side_2_upload_file_name_key";
    String ACTIVE_RENTAL_VEHICLE_DATA_FOR_USER_KEY = "active_rental_booking_for_user_key";
    String USER_KYC_STATUS_DATA_KEY = "user_kyc_status_data_key";
    String KYC_PROFILE_UPLOAD_FILE_NAME_KEY = "kyc_profile_upload_file_name_key";
    String KYC_DL_SIDE_1_UPLOAD_FILE_NAME_KEY = "kyc_dl_side_1_upload_file_name_key";
    String KYC_DL_SIDE_2_UPLOAD_FILE_NAME_KEY = "kyc_dl_side_2_upload_file_name_key";

    //image prefs
    String IMAGE_PREFS = "imagePrefs";
    String KYC_PROFILE_IMG_BUFFER_KEY = "kyc_profile_img_buffer_key";
    String KYC_DL_SIDE_1_IMG_BUFFER_KEY = "kyc_dl_side_1_img_buffer_key";
    String KYC_DL_SIDE_2_IMG_BUFFER_KEY = "kyc_dl_side_2_img_buffer_key";

    //user settings prefs
    String USER_SETTINGS_PREFS = "userSettingsPrefs";
    String DAY_NIGHT_AUTO_SWITCH_THEME_KEY = "day_night_auto_switch_theme_key";
    String DISTRACTION_FREE_MODE_KEY = "distraction_free_mode_key";
    String USE_DARK_THEME_KEY = "use_dark_theme_key";
    String ENABLE_CONNECT_USING_VIN_KEY = "enable_connect_using_vin_key";

    //bluetooth prefs
    String BLUETOOTH_PREFS = "bluetoothPrefs";
    String BLUETOOTH_LAST_CONNECTED_DEVICE_MAC_KEY = "bluetooth_last_connected_device_mac_key";
    String BLUETOOTH_LAST_CONNECTED_DEVICE_NAME_KEY = "bluetooth_last_connected_device_name_key";
    String BLUETOOTH_CONNECTION_STATUS_KEY = "bluetooth_connection_status_key";
    String BLUETOOTH_SERVICES_DISCOVERED_FLAG_KEY = "bluetooth_services_discovered_flag_key";
    String BLUETOOTH_IMAGE_SEQUENCE_KEY = "bluetooth_image_sequence_key";
    String BLUETOOTH_EXPECTED_IMAGE_CODE_KEY = "bluetooth_expected_image_code_key";
    String BLUETOOTH_LAST_IMAGE_TX_ATTEMPT_TIMESTAMP = "bluetooth_last_image_tx_attempt_timestamp";
    String BLUETOOTH_LAST_READ_VEHICLE_CONTROL_PARAMS_KEY = "bluetooth_last_read_vehicle_control_params_key"; //byte array list
    String BLUETOOTH_EXPECTED_CONTROLLER_PARAMS_KEY = "bluetooth_expected_vehicle_control_params_key"; //byte array list
    String BLUETOOTH_CONTROL_PARAMS_WRITE_LAST_ATTEMPT_TIMESTAMP = "bluetooth_control_params_write_last_attempt_timestamp";
    String BLUETOOTH_SPEEDOMETER_MESSAGE_EXPIRY_TIME = "bluetooth_speedometer_message_expiry_time";
    String BLUETOOTH_SPEEDOMETER_MESSAGE_ERASED = "bluetooth_speedometer_message_erased";
    String BLUETOOTH_SERVICE_LAST_ACTIVE_TIMESTAMP = "bluetooth_service_last_active_time_stamp";
    String BLUETOOTH_USER_MANUALLY_DISCONNECTED_FLAG_KEY = "bluetooth_user_manually_disconnected_flag_key";

    String BLUETOOTH_CONTROLLER_READ_FORMAT_KEY = "bluetooth_controller_read_format_key";
    String BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY = "bluetooth_controller_log_format_key";
    String BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY = "bluetooth_controller_write_format_key";
    String BLUETOOTH_PARAMID_TO_LOGID_KEY = "bluetooth_paramid_to_logid_key";
    String BLUETOOTH_VEHICLE_CONTROL_JSON_KEY = "bluetooth_vehicle_control_json_key";
    String BLUETOOTH_MODEL_CONFIG_JSON_KEY = "bluetooth_model_config_json_key";
    String BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY = "bluetooth_battery_adc_log_json_key";

    //iconcox
    String BT_GPS_ICONCOX_CMD_RESPONSE = "bt_gps_iconcox_cmd_response";


    //iconcox commands strings
    //String CMD_SEND_MAC = "";
    String CMD_SEND_REMOTE_LOCK = "23240104" + "0000" + "0D0A";
    String CMD_SEND_REMOTE_IGNITION = "23240103" + "0000" + "0D0A";
    String CMD_SEND_FIND_VEHICLE = "23240105" + "0000" + "0D0A";
    String CMD_SEND_LOCK = "23240101" + "0000" + "0D0A";
    String CMD_SEND_UNLOCK = "23240102" + "0000" + "0D0A";

    //trip prefs
    String TRIP_PREFS = "tripPrefs";
    String TRIP_DATA_KEY = "trip_data_key";
    String TRIP_DATA_NEEDS_REFRESH_KEY = "trip_data_needs_refresh";
    String TRIP_DATA_IS_REFRESHING_KEY = "trip_data_is_refreshing";
    String AVERAGE_PETROL_PRICE_KEY = "average_petrol_price_key";

    String GEO_FENCING_PREFS = "geo_fencing_prefs";
    String GEO_FENCING_JSON_DATA_KEY = "geo_fencing_data_key";

    //log prefs
    String LOG_PREFS = "logPrefs";

    String WORKING_LIVE_LOG_FILE_NAME_KEY = "working_live_log_file_name_key";
    String VEHICLE_LIVE_LOG_DIR_KEY = "vehicle_live_log_dir_key";
    String VEHICLE_DATA_LIVE_LOG_DIR_PATH = "vehicle_data_live";


    String WORKING_OFFLINE_LOG_FILE_NAME_KEY = "working_offline_log_file_name_key";
    String VEHICLE_OFFLINE_LOG_DIR_KEY = "vehicle_offline_log_dir_key";
    String VEHICLE_DATA_OFFLINE_LOG_DIR_PATH = "vehicle_data_offline";

    String WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY = "working_offline_battery_adc_log_file_name_key";
    String VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY = "vehicle_offline_battery_adc_log_dir_key";
    String VEHICLE_DATA_OFFLINE_BATTERY_ADC_LOG_DIR_PATH = "vehicle_data_offline_battery_adc";

    //directory constants
    String TRIP_DATA_DIR_PATH = "trip_data";


    //File extension constants
    String VEHICLE_DATA_LIVE_LOG_EXTENSION = ".rvdl";
    String VEHICLE_DATA_OFFLINE_LOG_EXTENSION = ".rvdo";
    String VEHICLE_DATA_OFFLINE_BATTERY_ADC_LOG_EXTENSION = ".rvdba";

    //firmware version strings
    String VER_1_0 = "1.0";
    String VER_1_1 = "1.1";
    String VER_5_0 = "5.0";

    String VER_1_0_0 = "1.0.0";
    String VER_1_1_0 = "1.1.0";
    String VER_5_0_0 = "5.0.0";
    String VER_5_1_0 = "5.1.0";


    String LOG_TYPE_OFFLINE = "0";
    String LOG_TYPE_LIVE = "1";
    String LOG_TYPE_OFFLINE_BATTERY_ADC = "2";

    //battery adc log constants
    String LOG_BATTERY_VOLTAGE_ADC = "batteryVoltageAdc";

    //vehicle log entry constants
    String LOG_RIDER_ID = "riderId";
    String LOG_IS_TIMESTAMP_VERIFIED = "timestampVerified";
    String LOG_TYPE = "type";
    String LOG_VIN = "vin";
    String LOG_TRIP_ID = "tripId";
    String LOG_MANUFACTURER_ID = "manufacturerId";
    String LOG_FIRMWARE_VERSION = "firmware";

    String LOG_GPS_SPEED = "gpsSpeed";
    String LOG_LAT = "latitude";
    String LOG_LNG = "longitude";
    String LOG_GPS_ACCURACY = "gpsAccuracy";
    String LOG_ELEVATION = "elevation";
    String LOG_HEADING = "heading";

    String LOG_TIMESTAMP = "timestamp";
    String LOG_WHEEL_RPM = "wheelRpm";
    String LOG_BATTERY_VOLTAGE = "batteryVoltage";
    String LOG_BATTERY_CURRENT = "batteryCurrent";
    String LOG_BATTERY_SOC = "batterySoc";
    String LOG_CONTROLLER_TEMP = "controllerTemperature";
    String LOG_THROTTLE = "throttle";
    String LOG_ODO = "odometer";
    String LOG_MODE = "mode";
    String LOG_HILL_ASSIST_STATUS = "hillAssistStatus";
    String LOG_ANTI_THEFT_STATUS = "antiTheftStatus";
    String LOG_CHARGING_STATUS  = "chargingStatus";
    String LOG_PARKING_STATUS = "parkingStatus";
    String LOG_OVERLOAD_STATUS = "overloadStatus";
    String LOG_OVERCURRENT_STATUS = "overCurrentStatus";
    String LOG_THROTTLE_STATUS = "throttleStatus";
    String LOG_BRAKE_STATUS = "brakeStatus";
    String LOG_REVERSE_STATUS = "reverseStatus";
    String LOG_SOFT_LOCK_STATUS = "softLockStatus";
    String LOG_MOTOR_STATUS = "motorStatus";
    String LOG_CONTROLLER_STATUS = "controllerStatus";
    String LOG_EABS_STATUS = "eabsStatus";
    String LOG_REGEN_BRAKING_STATUS = "regenBrakingStatus";

    String LOG_BRAKE_REGEN_LIMIT = "brakeRegenLimit";
    String LOG_ZERO_THROTTLE_REGEN_LIMIT = "zeroThrottleRegenLimit";
    String LOG_CURRENT_LIMIT = "currentLimit";
    String LOG_OVER_VOLTAGE_LIMIT = "overVoltageLimit";
    String LOG_UNDER_VOLTAGE_LIMIT = "underVoltageLimit";
    String LOG_SPEED_LIMIT = "speedLimit";
    String LOG_PICKUP_CONTROL_LIMIT = "pickupControlLimit";

    String LOG_LEFT_INDICATOR = "leftIndicator";
    String LOG_RIGHT_INDICATOR = "rightIndicator";
    String LOG_HEADLIGHT = "headlight";
    String LOG_BATTERY_DTE = "batteryDte";
    String LOG_BMS_SPEED = "bmsSpeed";

    String LOG_X_ACC = "xAcc";
    String LOG_Y_ACC = "yAcc";
    String LOG_Z_ACC = "zAcc";

    //bluetooth low energy service uuid
    String BLUETOOTH_OBD_DATA_SERVICE_UUID = "699aa9da-fe76-4b13-bda5-07f38795776b";
    String BLUETOOTH_IMAGE_TRANSFER_SERVICE_UUID = "653d1e34-885a-4324-8d35-a6eb1266e008";

    //a hash map of the commands and the number of retries attempted for that command
    String BLUETOOTH_COMMAND_STATUS_HASH_MAP_KEY = "bluetooth_command_status_hash_map_key";

    //a count of the number of retries made for a connection attempt
    String BLUETOOTH_RETRY_ATTEMPT_COUNT_KEY = "bluetooth_retry_attempt_count_key";

    //max retries allowed in case of attempting a connection
    int MAX_RETRY_ATTEMPT_COUNT = 2;

    //networking prefs
    String NETWORKING_PREFS = "networkingPrefs";
    String IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isReadDataDownloadInProgressKey";
    String IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isLogDataDownloadInProgressKey";
    String IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isParamWriteDataDownloadInProgressKey";
    String IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isVehicleControlDataDownloadInProgressKey";
    String IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isBatteryAdcLogDataDownloadInProgressKey";
    String IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY = "isDeviceDataDownloadInProgressKey";

    String IS_SOS_SMS_IN_PROGRESS_KEY = "isSosSmsInProgressKey";
    String IS_SOS_SMS_SENT_KEY = "isSosSmsSentKey";
    String SMS_SENT_TIMESTAMP_KEY = "smsSentTimestampKey";

    //navigation prefs
    String NAV_PREFS = "navPrefs";
    String LAST_GOOGLE_NOTIFICATION_TIMESTAMP = "last_google_notification_timestamp";
    String TRIP_ADVICE_DATA_KEY = "trip_advice_data_key";
    String SAVED_PLACES_DATA_KEY = "saved_places_key";
    String IS_GOOGLE_NAV_ACTIVE_KEY = "is_google_nav_active_key";

    //device prefs
    String DEVICE_PREFS = "devicePrefs";
    //this is pertaining to the cloud status
    String DEVICE_JSON_DATA_KEY  = "device_json_data_key";
    //this is just to store device PIN
    String DEVICE_PIN_KEY = "device_pin_key";
    String DEVICE_IS_ODO_RESET_REQUIRED_KEY = "device_is_od_reset_required_key";
    String DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS = "device_firmware_version_update_in_progress";

    //vehicle prefs
    String VEHICLE_PREFS = "vehiclePrefs";
    String VEHICLE_STATUS_FUEL_KEY = "vehicle_status_fuel_key";
    String VEHICLE_STATUS_BATTERY_KEY = "vehicle_status_battery_key";
    String VEHICLE_JSON_KEY = "vehicle_json_key";
    String VEHICLE_MODEL_IMAGE_FILE_PATH_KEY = "vehicle_model_image_file_path_key";
    String VEHICLE_MODEL_IMAGE_DIR_PATH = "vehicle_model_image";
    String VEHICLE_LAST_KNOWN_LATITUDE = "vehicle_last_known_latitude";
    String VEHICLE_LAST_KNOWN_LONGITUDE = "vehicle_last_known_longitude";

    //onwer's vehicles prefs
    String OWNERS_VEHICLES_LIST = "owners_vehicles_list";
    String OWNERS_VEHICLES_PIN_LIST = "owners_vehicles_pin_list";
    String MODEL_IMAGE_HASHMAP_KEY = "owners_vehicles_models_image_list";
    //battery prefs
    String BATTERY_PREFS = "batteryPrefs";
    String BATTERY_REGRESSION_DATA_KEY = "battery_regression_data_key";

    //tracking prefs
    String TRACKING_PREFS = "trackingPrefs";
    String LIVE_PACKET_JSON_KEY = "live_packet_json_key";
    String LAST_LIVE_PACKET_REFRESHED_TIMESTAMP = "last_live_packet_refreshed_timestamp";
    String HISTORICAL_DATA_LIST_KEY = "historical_data_list_key";
    String HISTORICAL_DATA_IS_REFRESHING_KEY = "historical_data_refreshing_key";

    //ota prefs
    String OTA_DIR_PATH = "ota";
    String OTA_FILE_PATH_KEY = "ota_file_path_key";

    //temp image prefs
    String TEMP_IMAGE_PREFS = "temp_image_prefs";
    String GENERIC_TEMP_IMAGE_KEY = "generic_temp_image_key";

    //permissions required
    String[] permissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    //String delimiter for messages
    String GEN_DELIMITER = "-1del-1m-1ter";

    //General Events
    String GEN_EVENT_TAB_CHANGE = "gen_event_tab_change";
    String GEN_EVENT_CHANGE_HAMBURGER_TO_BACK = "gen_event_change_hamburger_to_back";
    String GEN_EVENT_NAV_BANNER_ANIM_UPDATED = "gen_event_navigation_banner_updated";
    String GEN_EVENT_PHONE_LOCATION_UPDATED = "gen_event_location_updated";
    String GEN_EVENT_VEHICLE_RENTAL_TAB_CHANGE = "gen_event_vehicle_rental_tab_change";


    //Phone constants
    String ENTRY_TYPE_CALL_INCOMING = "call_incoming";
    String ENTRY_TYPE_CALL_OUTGOING = "call_outgoing";
    String ENTRY_TYPE_CALL_MISSED = "call_missed";
    String ENTRY_TYPE_CONTACT_SEARCH_RESULT = "contact_search_result";
    String PHONE_NUMBER_REGEX = "[0-9 ()+-]*";

    //Kill switch event
    String KILL_SWITCH = "kill_switch";

    //CAN constants
    int CAN_SPEED_ID = 71;
    int CAN_ODO_ID = 72;
    int CAN_RANGE_ID = 73;
    int CAN_SOC_ID = 74;

    //vehicle constants
    int WHEEL_RPM_ID = 0x57;
    int THROTTLE_ID = 0x54;
    int VOLTAGE_ID = 0x56;
    int CURRENT_ID = 0x43;
    int MODE_AND_LOCK_ID = 0x4D;
    int ODO_ID = 0x4F;
    int TEMP_ID = 0x54;
    int RESISTANCE_ID = 0x52;
    int OTHER_INFO_ID = 0x49;


    //OBD Constants
    String OBD_EVENT_LIVE_DATA = "obd_event_live_data";
    String OBD_EVENT_OFFLINE_DATA = "obd_event_offline_data";
    String OBD_EVENT_BATTERY_ADC_OFFLINE_DATA = "obd_event_battery_adc_offline_data";

    //Vehicle control command strings
    String HEADLIGHT_ON = "16";
    String HEADLIGHT_OFF = "17";

    String LEFT_INDICATOR_ON = "32";
    String LEFT_INDICATOR_OFF = "33";

    String RIGHT_INDICATOR_ON = "48";
    String RIGHT_INDICATOR_OFF = "49";

    String VEHICLE_LOCK = "64";
    String VEHICLE_UNLOCK = "65";

    String COMMAND_STRING_HEADER = "DA";
    String SOFT_LOCK_HEADER = "A5";//0xA5
    String SPEED_LIMIT_HEADER = "53";//0x53, 'S'
    String CURRENT_LIMIT_HEADER = "43";//0x43, 'C'
    String VOLTAGE_LIMIT_HEADER = "56";//0x56, 'V'
    String CONTROL_PARAMS_DELIMITER = "55";//0x55

    //Dynamic Image type
    int IMAGE_TYPE_BLANK = 0;
    int IMAGE_TYPE_INTERNAL = 1;
    int IMAGE_TYPE_CUSTOM = 2;

    //Login events
    String LOGIN_EVENT_SIGN_IN_GOOGLE = "login_event_sign_in_google";
    String LOGIN_EVENT_SIGN_IN_FB = "login_event_sign_in_fb";
    String LOGIN_EVENT_SIGN_IN_PHONE = "login_event_sign_in_phone";
    String LOGIN_EVENT_SIGN_IN_EMAIL = "login_event_sign_in_email";
    String LOGIN_EVENT_SIGN_IN_GUEST = "login_event_sign_in_guest";

    //Navigation events
    String NAV_EVENT_TRIP_ADVICE_DATA_UPDATED = "nav_event_trip_advice_data_updated";
    String NAV_EVENT_SAVED_PLACE_LIST_UPDATED = "nav_event_saved_place_list_updated";

    //Bluetooth events
    String BT_EVENT_REQUEST_CONNECT_TO_DEVICE = "bt_event_request_connect_to_device";
    String BT_EVENT_REQUEST_CONNECT_TO_LAST_SAVED_DEVICE = "bt_event_request_connect_to_last_device";
    String BT_EVENT_REQUEST_WRITE_CONTROLLER_PARAMS = "bt_event_request_write_controller_params";
    String BT_EVENT_REQUEST_WRITE_VEHICLE_PARAMS = "bt_event_request_write_vehicle_params";
    String BT_EVENT_REQUEST_SEND_FIND_MY_VEHICLE_COMMAND = "bt_event_request_send_find_my_vehicle_command";
    String BT_EVENT_REQUEST_RECONNECT = "bt_event_request_reconnect";
    String BT_EVENT_REQUEST_DISCONNECT = "bt_event_request_disconnect";
    String BT_EVENT_REQUEST_CANCEL_PAIRING = "bt_event_request_cancel_pairing";

    String BT_EVENT_REQUEST_ROTATE_TEST_IMAGE = "bt_event_rotate_test_image";
    String BT_EVENT_IMAGE_CHECK_COMPLETE = "bt_event_image_check_complete";
    String BT_EVENT_REQUEST_CLEAR_IMAGE = "bt_event_clear_image";
    String BT_EVENT_DECREASE_CUSTOM_IMAGE_PRIORITY = "bt_event_decrease_custom_image_priority";
    String BT_EVENT_REQUEST_SEND_UBER_SCENARIO_1_IMAGE = "bt_send_uber_scenario_1_image";
    String BT_EVENT_REQUEST_SEND_UBER_SCENARIO_2_IMAGE = "bt_send_uber_scenario_2_image";
    String BT_EVENT_REQUEST_SEND_CALL_BITMAP = "bt_send_dynamic_bitmap";
    String BT_EVENT_REQUEST_SEND_FILLER_BYTES_OBD = "bt_request_send_filler_bytes_obd";
    String BT_EVENT_REQUEST_SEND_FILLER_BYTES_IMG = "bt_request_send_filler_bytes_img";
    String BT_EVENT_REQUEST_SEND_LARGE_BITMAP = "bt_request_send_large_bitmap";

    String BT_EVENT_REQUEST_LOCK_VEHICLE = "bt_request_lock_vehicle";
    String BT_EVENT_REQUEST_UNLOCK_VEHICLE = "bt_request_unlock_vehicle";
    String BT_EVENT_REQUEST_LOCK_HANDLE = "bt_request_lock_handle";
    String BT_EVENT_REQUEST_UNLOCK_HANDLE = "bt_request_unlock_handle";
    String BT_EVENT_REQUEST_UNLOCK_SEAT = "bt_request_seat_unlock";
    String BT_EVENT_REQUEST_SHOW_SETTINGS_CHANGED_ICON = "bt_request_show_settings_changed_icon";

    String BT_EVENT_REQUEST_DEVICE_SYNC = "bt_event_request_device_sync";
    String BT_EVENT_REQUEST_CONNECT_TBIT = "bt_event_request_connect_tbit";
    String BT_EVENT_REQUEST_CONNECT_ICONCOX = "bt_event_request_connect_iconcox";

    String BT_EVENT_OBD_CHAR_WRITE_SUCCESS = "bt_event_obd_char_write_success";
    String BT_EVENT_OBD_CHAR_WRITE_FAIL = "bt_event_obd_char_write_fail";
    String BT_EVENT_IMG_CHAR_WRITE_SUCCESS = "bt_event_img_char_write_success";
    String BT_EVENT_IMG_CHAR_WRITE_FAIL = "bt_event_img_char_write_fail";


    String BT_EVENT_SERVICES_DISCOVERED = "bt_event_state_services_discovered";
    String BT_EVENT_MTU_CHANGED = "bt_event_mtu_changed";
    String BT_EVENT_STATE_CONNECTED = "bt_event_state_connected";
    String BT_EVENT_STATE_AUTHENTICATING = "bt_event_state_authenticating";
    String BT_EVENT_STATE_AUTHENTICATION_SUCCESS = "bt_event_state_authentication_success";
    String BT_EVENT_STATE_AUTHENTICATION_FAIL = "bt_event_state_authentication_fail";
    String BT_EVENT_STATE_CONNECTING = "bt_event_state_connecting";
    String BT_EVENT_STATE_DISCONNECTED = "bt_event_state_disconnected";
    String BT_EVENT_SWITCH_ON_BLUETOOTH = "bt_event_switch_on_bluetooth";
    String BT_EVENT_SCAN_STARTED = "bt_event_scan_started";
    String BT_EVENT_DEVICE_NOT_FOUND = "bt_event_device_not_found";
    String BT_EVENT_DEVICE_START_ACTIVATION_PROCEDURE = "bt_event_device_start_activation_procedure";
    String BT_EVENT_VEHICLE_IDS_CHANGE_SUCCESS = "bt_event_vehicle_ids_change_success";
    String BT_EVENT_VEHICLE_CONSTANTS_CHANGE_SUCCESS = "bt_event_vehicle_constants_change_success";
    String BT_EVENT_VEHICLE_BATTERY_VOLTAGE_CHANGE_SUCCESS = "bt_event_vehicle_battery_voltage_change_success";
    String BT_EVENT_DEVICE_STATE_CHANGE_SUCCESS = "bt_event_device_state_change_success";
    String BT_EVENT_DEVICE_SYNC_FAIL = "bt_event_device_sync_fail";
    String BT_EVENT_DEVICE_SYNC_SUCCESS = "bt_event_device_sync_success";
    String BT_EVENT_BATTERY_REGRESSION_DATA_SYNC_SUCCESS = "bt_event_battery_regression_data_sync_success";
    String BT_EVENT_UNABLE_TO_READ_FIRMWARE_METADATA = "bt_event_unable_to_read_firmware_metadata";
    String BT_EVENT_DEVICE_LOCKED = "bt_event_device_locked";
    String BT_EVENT_NO_PIN_FOUND = "bt_event_no_pin_found";
    String BT_EVENT_WRONG_PIN = "bt_event_wrong_pin";
    String BT_EVENT_CHANGE_PIN_REQUIRED = "bt_event_change_pin_required";

    //Phone event constants
    String PHONE_EVENT_CONTACT_LIST_BUILDING = "phone_event_contact_list_building";
    String PHONE_EVENT_CONTACT_LIST_BUILT = "phone_event_contact_list_built";
    String PHONE_EVENT_REQUEST_BUILD_CONTACT_LIST = "phone_event_request_build_contact_list";
    String PHONE_EVENT_REQUEST_ACCEPT_CALL_VIA_NOTIFICATION = "phone_event_request_accept_call_via_notification";
    String PHONE_EVENT_CREATE_CALL_LOG = "phone_event_create_call_log";
    String PHONE_EVENT_MULTIPLE_MATCHED_RESULTS = "phone_event_multiple_results";

    //Networking events
    String NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED = "network_event_user_profile_pic_downloaded";
    String NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED = "network_event_user_dl_pic_1_downloaded";
    String NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED = "network_event_user_dl_pic_2_downloaded";
    String NETWORK_EVENT_CONTROLLER_METADATA_FILE_DOWNLOADED = "network_event_controller_metadata_file_downloaded";
    String NETWORK_EVENT_USER_DETAILS_DOWNLOAD_SUCCESS = "network_event_user_details_download_success";
    String NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL = "network_event_user_details_download_fail";
    String NETWORK_EVENT_TRIP_DATA_REFRESHED = "network_event_trip_data_refreshed";
    String NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED = "network_event_vehicle_model_image_downloaded";
    String NETWORK_EVENT_DEVICE_DATA_REFRESHED = "network_event_device_data_refreshed";
    String NETWORK_EVENT_BATTERY_REGRESSION_DATA_REFRESHED = "network_event_battery_regression_data_refreshed";
    String NETWORK_EVENT_VEHICLE_LAST_KNOWN_LOCATION_UPDATED = "network_event_vehicle_last_known_location_updated";
    String NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED = "network_event_vehicle_historical_data_updated";
    String NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED = "network_event_vehicle_live_log_data_updated";

    //error event
    String GENERIC_MESSAGE_ACTIVITY_ERROR_EVENT = "generic_message_activity_error_event";
    String GENERIC_MESSAGE_ACTIVITY_PERSISTENT_ERROR_EVENT = "generic_message_activity_persistent_error_event";
    String GENERIC_MESSAGE_ACTIVITY_INFO_EVENT = "generic_message_activity_info_event";
    String GENERIC_MESSAGE_ACTIVITY_PERSISTENT_INFO_EVENT = "generic_message_activity_persistent_info_event";

    //User profile activity constants
    String USER_PROFILE_FRAG_EVENT_USER_UPDATED = "user_profile_frag_event_user_updated";
    String USER_PROFILE_FRAG_EVENT_LOAD_EDIT_USER_FRAGMENT = "user_profile_frag_event_load_edit_user_fragment";
    String USER_PROFILE_EVENT_BACK_PRESSED = "user_profile_event_back_pressed";

    //Directory names constant
    String TEMP_PIC_DIR_PATH = "temp_pic_dir";
    String PIC_DIR_PATH = "pics";

    //File prefix constants
    String PROFILE_PIC_TEMP_PREFIX = "tempProfilePic_";
    String DL_SIDE_1_TEMP_PREFIX = "tempDLPic1_";
    String DL_SIDE_2_TEMP_PREFIX = "tempDLPic2_";
    String SERVICE_TICKET_PIC_TEMP_PREFIX = "tempSRPic_";
    String KYC_PROFILE_PIC_TEMP_PREFIX = "tempKycProfilePic_";
    String KYC_DL_SIDE_1_PIC_TEMP_PREFIX = "tempKycDLSide1Pic_";
    String KYC_DL_SIDE_2_PIC_TEMP_PREFIX = "tempKycDLSide2Pic_";

    //File provider string
    String FILE_PROVIDER = "com.revos.android.fileprovider";

    //User profile pic and dl pic constants
    String PROFILE_PIC_TEMP_COMPRESSED_PREFIX = "tempCompressedProfilePic_";
    String DL_SIDE_1_TEMP_COMPRESSED_PREFIX = "tempCompressedDLPic1_";
    String DL_SIDE_2_TEMP_COMPRESSED_PREFIX = "tempCompressedDLPic2_";
    String SERVICE_TICKET_PIC_COMPRESSED_PREFIX = "tempCompressedSRPic_";
    String KYC_PROFILE_PIC_TEMP_COMPRESSED_PREFIX = "tempCompressedKycProfilePic_";
    String KYC_DL_SIDE_1_TEMP_COMPRESSED_PREFIX = "tempCompressedKycDLPic1_";
    String KYC_DL_SIDE_2_TEMP_COMPRESSED_PREFIX = "tempCompressedKycDLPic2_";

    String PROFILE_PIC_PREFIX = "ProfilePic_";
    String DL_SIDE_1_PREFIX = "DLSide1_";
    String DL_SIDE_2_PREFIX = "DLSide2_";
    String SERVICE_TICKET_PIC_PREFIX = "SRPic_";
    String KYC_PROFILE_PIC_PREFIX = "KYCProfilePic_";
    String KYC_DL_SIDE_1_PREFIX = "KYCDLSide1_";
    String KYC_DL_SIDE_2_PREFIX = "KYCDLSide2_";

    String UPLOAD_TYPE_PROFILE = "PROFILE";
    String UPLOAD_TYPE_DL_SIDE_1 = "DL_SIDE_1";
    String UPLOAD_TYPE_DL_SIDE_2 = "DL_SIDE_2";
    String UPLOAD_TYPE_SERVICE_TICKET = "SERVICE_TICKET";
    String UPLOAD_TYPE_KYC_PROFILE = "KYC_PROFILE";
    String UPLOAD_TYPE_KYC_DL_SIDE_1 = "KYC_DL_SIDE_1";
    String UPLOAD_TYPE_KYC_DL_SIDE_2 = "KYC_DL_SIDE_2";

    //BITMAP TYPE
    String BITMAP_TYPE_NAVIGATION = "navigation";
    String BITMAP_TYPE_PHONE = "phone";

    String GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    String PHONE_PAY_PACKAGE_NAME = "com.phonepe.app";


    String START_STM_INTERNAL_OTA = "start_stm_internal_ota";
    String START_STM_EXTERNAL_OTA = "start_stm_external_ota";
    String STM_OTA_STATUS_MESSAGE = "stm_ota_status_message";

    //TODO::tall right and tall straight, later
    int[] turnIconIds = {
            R.drawable.da_turn_depart,
            R.drawable.da_turn_straight,
            R.drawable.da_turn_left, R.drawable.da_turn_right,
            R.drawable.da_turn_slight_left, R.drawable.da_turn_slight_right,
            R.drawable.da_turn_fork_left, R.drawable.da_turn_fork_right,
            R.drawable.da_turn_sharp_left, R.drawable.da_turn_sharp_right,
            R.drawable.da_turn_ramp_left, R.drawable.da_turn_ramp_right,
            R.drawable.da_lane_uturn_left, R.drawable.da_lane_uturn_right,
            R.drawable.da_lane_uturn_short_left, R.drawable.da_lane_uturn_short_right,
            R.drawable.da_turn_uturn_left, R.drawable.da_turn_uturn_right,
            R.drawable.da_turn_arrive, R.drawable.da_turn_arrive_left, R.drawable.da_turn_arrive_right,
            R.drawable.da_turn_generic_roundabout_left, R.drawable.da_turn_generic_roundabout_right,
            R.drawable.da_turn_roundabout_1_left, R.drawable.da_turn_roundabout_1_right,
            R.drawable.da_turn_roundabout_2_left, R.drawable.da_turn_roundabout_2_right,
            R.drawable.da_turn_roundabout_3_left, R.drawable.da_turn_roundabout_3_right,
            R.drawable.da_turn_roundabout_4_left, R.drawable.da_turn_roundabout_4_right,
            R.drawable.da_turn_roundabout_5_left, R.drawable.da_turn_roundabout_5_right,
            R.drawable.da_turn_roundabout_6_left, R.drawable.da_turn_roundabout_6_right,
            R.drawable.da_turn_roundabout_7_left, R.drawable.da_turn_roundabout_7_right,
            R.drawable.da_turn_roundabout_8_left, R.drawable.da_turn_roundabout_8_right,
            R.drawable.da_turn_roundabout_exit_left, R.drawable.da_turn_roundabout_exit_right,
            R.drawable.da_turn_unknown,
            R.drawable.da_turn_generic_merge,
            R.drawable.da_turn_ferry,
            R.drawable.da_turn_mylocation
    };

    //notification service constants
    String TRIP_ADVICE_DISTANCE_FROM_TURN_KEY = "tripAdvice_distanceFromTurn";
    String TRIP_ADVICE_ETA_KEY = "tripAdvice_eta";
    String TRIP_ADVICE_REMAINING_DISTANCE_KEY = "tripAdvice_remainingDistance";
    String TRIP_ADVICE_DESTINATION_NAME_KEY = "tripAdvice_destinationName";
    String TRIP_ADVICE_REMAINING_TIME_KEY = "tripAdvice_remainingTime";
    String TRIP_ADVICE_NEXT_ROAD_KEY = "tripAdvice_nextRoad";
    String TRIP_ADVICE_IS_REROUTING_KEY = "tripAdvice_isRerouting";
    String TRIP_ADVICE_TITLE_KEY = "tripAdvice_title";
    String TRIP_ADVICE_SUBTEXT_KEY = "tripAdvice_subtext";
    String TRIP_ADVICE_TEXT_KEY = "tripAdvice_text";

    //google maps package name
    String GOOGLE_MAPS_PACKAGE_NAME = "com.google.android.apps.maps";
    String YOUTUBE_MUSIC_PACKAGE_NAME = "com.google.android.apps.youtube.music";
    String SPOTIFY_PACKAGE_NAME = "com.spotify.music";

    //various arrow positions for center console
    int ARROW_STRAIGHT = 6;
    int ARROW_SLIGHT_LEFT = 5;
    int ARROW_LEFT = 4;
    int ARROW_SHARP_LEFT = 3;
    int ARROW_SLIGHT_RIGHT = 7;
    int ARROW_RIGHT = 0;
    int ARROW_SHARP_RIGHT = 1;

    //these two values are supposed to be used as they are but are to be used as a reference
    int ARROW_LEFT_UTURN = 10;
    int ARROW_RIGHT_UTURN = 20;

    int ARROW_ARRIVE = 30;

    int ARROW_OFF = -1;

    //Speech Recognition
    String SPEECH_RECOGNITION_RESULT = "speech_recog_result";

    //Generic error message activity arguments constants
    String GENERIC_MESSAGE_TEXT_KEY = "error_text_message";
    String GENERIC_MESSAGE_STATUS_KEY = "error_status";
    String GENERIC_MESSAGE_TIMER_KEY = "error_message_show_time";
    String GENERIC_MESSAGE_BUTTON_TEXT_KEY = "error_message_button_text_key";

    HashMap<String, String> countryCodes = new HashMap<String, String>() {{
        put("ad", "376");
        put("ae", "971");
        put("af", "93");
        put("ag", "1");
        put("ai", "1");
        put("al", "355");
        put("am", "374");
        put("ao", "244");
        put("aq", "672");
        put("ar", "54");
        put("as", "1");
        put("at", "43");
        put("au", "61");
        put("aw", "297");
        put("ax", "358");
        put("az", "994");
        put("ba", "387");
        put("bb", "1");
        put("bd", "880");
        put("be", "32");
        put("bf", "226");
        put("bg", "359");
        put("bh", "973");
        put("bi", "257");
        put("bj", "229");
        put("bl", "590");
        put("bm", "1");
        put("bn", "673");
        put("bo", "591");
        put("br", "55");
        put("bs", "1");
        put("bt", "975");
        put("bw", "267");
        put("by", "375");
        put("bz", "501");
        put("ca", "1");
        put("cc", "61");
        put("cd", "243");
        put("cf", "236");
        put("cg", "242");
        put("ch", "41");
        put("ci", "225");
        put("ck", "682");
        put("cl", "56");
        put("cm", "237");
        put("cn", "86");
        put("co", "57");
        put("cr", "506");
        put("cu", "53");
        put("cv", "238");
        put("cw", "599");
        put("cx", "61");
        put("cy", "357");
        put("cz", "420");
        put("de", "49");
        put("dj", "253");
        put("dk", "45");
        put("dm", "1");
        put("do", "1");
        put("dz", "213");
        put("ec", "593");
        put("ee", "372");
        put("eg", "20");
        put("er", "291");
        put("es", "34");
        put("et", "251");
        put("fi", "358");
        put("fj", "679");
        put("fk", "500");
        put("fm", "691");
        put("fo", "298");
        put("fr", "33");
        put("ga", "241");
        put("gb", "44");
        put("gd", "1");
        put("ge", "995");
        put("gf", "594");
        put("gh", "233");
        put("gi", "350");
        put("gl", "299");
        put("gm", "220");
        put("gn", "224");
        put("gp", "450");
        put("gq", "240");
        put("gr", "30");
        put("gt", "502");
        put("gu", "1");
        put("gw", "245");
        put("gy", "592");
        put("hk", "852");
        put("hn", "504");
        put("hr", "385");
        put("ht", "509");
        put("hu", "36");
        put("id", "62");
        put("ie", "353");
        put("il", "972");
        put("im", "44");
        put("is", "354");
        put("in", "91");
        put("io", "246");
        put("iq", "964");
        put("ir", "98");
        put("it", "39");
        put("je", "44");
        put("jm", "1");
        put("jo", "962");
        put("jp", "81");
        put("ke", "254");
        put("kg", "996");
        put("kh", "855");
        put("ki", "686");
        put("km", "269");
        put("kn", "1");
        put("kp", "850");
        put("kr", "82");
        put("kw", "965");
        put("ky", "1");
        put("kz", "7");
        put("la", "856");
        put("lb", "961");
        put("lc", "1");
        put("li", "423");
        put("lk", "94");
        put("lr", "231");
        put("ls", "266");
        put("lt", "370");
        put("lu", "352");
        put("lv", "371");
        put("ly", "218");
        put("ma", "212");
        put("mc", "377");
        put("md", "373");
        put("me", "382");
        put("mf", "590");
        put("mg", "261");
        put("mh", "692");
        put("mk", "389");
        put("ml", "223");
        put("mm", "95");
        put("mn", "976");
        put("mo", "853");
        put("mp", "1");
        put("mq", "596");
        put("mr", "222");
        put("ms", "1");
        put("mt", "356");
        put("mu", "230");
        put("mv", "960");
        put("mw", "265");
        put("mx", "52");
        put("my", "60");
        put("mz", "258");
        put("na", "264");
        put("nc", "687");
        put("ne", "227");
        put("nf", "672");
        put("ng", "234");
        put("ni", "505");
        put("nl", "31");
        put("no", "47");
        put("np", "977");
        put("nr", "674");
        put("nu", "683");
        put("nz", "64");
        put("om", "968");
        put("pa", "507");
        put("pe", "51");
        put("pf", "689");
        put("pg", "675");
        put("ph", "63");
        put("pk", "92");
        put("pl", "48");
        put("pm", "508");
        put("pn", "870");
        put("pr", "1");
        put("ps", "970");
        put("pt", "351");
        put("pw", "680");
        put("py", "595");
        put("qa", "974");
        put("re", "262");
        put("ro", "40");
        put("rs", "381");
        put("ru", "7");
        put("rw", "250");
        put("sa", "966");
        put("sb", "677");
        put("sc", "248");
        put("sd", "249");
        put("se", "46");
        put("sg", "65");
        put("sh", "290");
        put("si", "386");
        put("sk", "421");
        put("sl", "232");
        put("sm", "378");
        put("sn", "221");
        put("so", "252");
        put("sr", "597");
        put("ss", "211");
        put("st", "239");
        put("sv", "503");
        put("sx", "1");
        put("sy", "963");
        put("sz", "268");
        put("tc", "1");
        put("td", "235");
        put("tg", "228");
        put("th", "66");
        put("tj", "992");
        put("tk", "690");
        put("tl", "670");
        put("tm", "993");
        put("tn", "216");
        put("to", "676");
        put("tr", "90");
        put("tt", "1");
        put("tv", "688");
        put("tw", "886");
        put("tz", "255");
        put("ua", "380");
        put("ug", "256");
        put("us", "1");
        put("uy", "598");
        put("uz", "998");
        put("va", "379");
        put("vc", "1");
        put("ve", "58");
        put("vg", "1");
        put("vi", "1");
        put("vn", "84");
        put("vu", "678");
        put("wf", "681");
        put("ws", "685");
        put("xk", "383");
        put("ye", "967");
        put("yt", "262");
        put("za", "27");
        put("zm", "260");
        put("zw", "263");
    }};
}
