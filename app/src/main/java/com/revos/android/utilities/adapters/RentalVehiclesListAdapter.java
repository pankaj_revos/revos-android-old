package com.revos.android.utilities.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.revos.android.R;
import com.revos.android.activities.BookRentalVehicleActivity;
import com.revos.android.activities.GenericMessageActivity;
import com.revos.android.activities.RentalActivity;
import com.revos.android.activities.RentalKycActivity;
import com.revos.android.jsonStructures.DataFeedVehicleSnapshot;
import com.revos.android.jsonStructures.RentalPricingInfo;
import com.revos.android.jsonStructures.RentalVehicle;
import com.revos.android.jsonStructures.RentalVehicleCompany;
import com.revos.android.jsonStructures.RentalVehicleModel;
import com.revos.android.jsonStructures.RentalVehicleModelConfig;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.scripts.FetchKYCAssociationForUserQuery;
import com.revos.scripts.FetchRentalVehicleDetailsAndPricingInfoQuery;
import com.revos.scripts.type.CompanyWhereUniqueInput;
import com.revos.scripts.type.InputGetRentalVehicles;
import com.revos.scripts.type.KYCStatus;
import com.revos.scripts.type.RentalStatus;
import com.revos.scripts.type.VehicleWhereInput;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.activities.BookRentalVehicleActivity.BOOK_RENTAL_VEHICLE_OBJECT_KEY;
import static com.revos.android.activities.RentalKycActivity.RENTAL_KYC_VEHICLE_OBJECT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_BUTTON_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_STATUS_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TEXT_KEY;
import static com.revos.android.constants.Constants.GENERIC_MESSAGE_TIMER_KEY;

public class RentalVehiclesListAdapter extends RecyclerView.Adapter<RentalVehiclesListAdapter.ViewHolder> {


    private List<RentalVehicle> mRentalVehiclesList;
    private Context mContext;
    private String batteryVoltageString = "", batterySocString = "";
    private final int BOOKING_ACTIVITY_REQUEST_CODE = 1002;

    public RentalVehiclesListAdapter(List<RentalVehicle> serviceRequestList, Context context) {
        mRentalVehiclesList = serviceRequestList;
        mContext = context;
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{

        LinearLayout listItemLinearLayout;
        TextView listItemModelNameTextView, vehicleStatusTextView, listItemVehicleVinTextView,
                vehicleProviderNameTextView, vehicleProviderPhoneTextView, vehicleBatteryStatusTextView;
        ImageView providerImageView;
        Button bookRentalVehicleButton;

        public ViewHolder(@NonNull View view) {

            super(view);

            listItemLinearLayout = view.findViewById(R.id.rental_vehicle_list_item_linear_layout);

            listItemModelNameTextView = view.findViewById(R.id.rental_vehicle_list_model_name_text_view);

            vehicleStatusTextView = view.findViewById(R.id.rental_vehicle_list_status_text_view);

            listItemVehicleVinTextView = view.findViewById(R.id.rental_vehicle_list_vin_text_view);

            vehicleProviderNameTextView = view.findViewById(R.id.rental_vehicle_list_provider_text_view);

            vehicleProviderPhoneTextView = view.findViewById(R.id.rental_vehicle_list_provider_phone_text_view);

            providerImageView = view.findViewById(R.id.rental_vehicle_list_provider_image_view);

            vehicleBatteryStatusTextView = view.findViewById(R.id.rental_vehicle_list_battery_status_text_view);

            bookRentalVehicleButton = view.findViewById(R.id.rental_vehicle_list_book_now_button);
        }
    }

    @NonNull
    @Override
    public RentalVehiclesListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rental_vehicles_list_item, viewGroup, false);

        return new RentalVehiclesListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        RentalVehicle rentalVehicle = mRentalVehiclesList.get(position);

        //clear views before populating
        viewHolder.listItemModelNameTextView.clearComposingText();
        viewHolder.listItemVehicleVinTextView.clearComposingText();
        viewHolder.vehicleBatteryStatusTextView.setText(mContext.getString(R.string.battery).concat(" : ")
                .concat(("--").concat("\n").concat(mContext.getString(R.string.soc))
                        .concat(" : ").concat("--")));
        viewHolder.vehicleProviderNameTextView.clearComposingText();
        viewHolder.vehicleProviderPhoneTextView.clearComposingText();


        if(rentalVehicle.getModel() != null && rentalVehicle.getModel().getName() != null && !rentalVehicle.getModel().getName().isEmpty()) {
            viewHolder.listItemModelNameTextView.setText(rentalVehicle.getModel().getName());
        } else {
            viewHolder.listItemModelNameTextView.setText(mContext.getString(R.string.not_available));
        }

        if(rentalVehicle != null) {
            if(rentalVehicle.getModel() != null && rentalVehicle.getModel().getName() != null && !rentalVehicle.getModel().getName().isEmpty()) {
                viewHolder.listItemModelNameTextView.setText(rentalVehicle.getModel().getName());
            } else {
                viewHolder.listItemModelNameTextView.setText(mContext.getString(R.string.not_available));
            }

            if(rentalVehicle.getVin() != null && !rentalVehicle.getVin().isEmpty()) {
                viewHolder.listItemVehicleVinTextView.setText(rentalVehicle.getVin());

                //reset battery voltage and soc value
                viewHolder.vehicleBatteryStatusTextView.setText(mContext.getString(R.string.battery).concat(" : ")
                        .concat(("--").concat("\n").concat(mContext.getString(R.string.soc))
                                .concat(" : ").concat("--")));

            } else {
                viewHolder.listItemVehicleVinTextView.setText(mContext.getString(R.string.vin_not_found_try_again));
            }

            if(rentalVehicle.getRentalStatus() != null) {
                viewHolder.vehicleStatusTextView.setText(rentalVehicle.getRentalStatus().toString());
            } else {
                viewHolder.vehicleStatusTextView.setText(mContext.getString(R.string.not_available));
            }

            if(rentalVehicle.getCompany() != null) {

                if(rentalVehicle.getCompany().getName() != null && !rentalVehicle.getCompany().getName().isEmpty()) {
                    viewHolder.vehicleProviderNameTextView.setText(rentalVehicle.getCompany().getName());
                } else {
                    viewHolder.vehicleProviderNameTextView.setText(mContext.getString(R.string.not_available));
                }

                if(rentalVehicle.getCompany().getPhone() != null && !rentalVehicle.getCompany().getPhone().isEmpty() ) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(mContext).sanitizePhoneNumber(rentalVehicle.getCompany().getPhone().trim());
                    viewHolder.vehicleProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    viewHolder.vehicleProviderPhoneTextView.setText(mContext.getString(R.string.not_available));
                }

            } else if(rentalVehicle.getOem() != null) {

                if(rentalVehicle.getOem().getName() != null && !rentalVehicle.getOem().getName().isEmpty()) {
                    viewHolder.vehicleProviderNameTextView.setText(rentalVehicle.getOem().getName());
                } else {
                    viewHolder.vehicleProviderNameTextView.setText(mContext.getString(R.string.not_available));
                }

                if(rentalVehicle.getOem().getPhone() != null && !rentalVehicle.getOem().getPhone().isEmpty() ) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(mContext).sanitizePhoneNumber(rentalVehicle.getOem().getPhone().trim());
                    viewHolder.vehicleProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    viewHolder.vehicleProviderPhoneTextView.setText(mContext.getString(R.string.not_available));
                }

            } else if(rentalVehicle.getDistributor() != null) {

                if(rentalVehicle.getDistributor().getName() != null && !rentalVehicle.getDistributor().getName().isEmpty()) {
                    viewHolder.vehicleProviderNameTextView.setText(rentalVehicle.getDistributor().getName());
                } else {
                    viewHolder.vehicleProviderNameTextView.setText(mContext.getString(R.string.not_available));
                }

                if(rentalVehicle.getDistributor().getPhone() != null && !rentalVehicle.getDistributor().getPhone().isEmpty() ) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(mContext).sanitizePhoneNumber(rentalVehicle.getDistributor().getPhone().trim());
                    viewHolder.vehicleProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    viewHolder.vehicleProviderPhoneTextView.setText(mContext.getString(R.string.not_available));
                }

            }

        }

        fetchAndPopulateBatteryDetails(viewHolder, rentalVehicle.getVin(), rentalVehicle);

        viewHolder.bookRentalVehicleButton.setOnClickListener(v -> {
            if(rentalVehicle != null && rentalVehicle.getVin() != null) {
                fetchVehicleDetailsAndPricingInfo(rentalVehicle);
            }
        });
    }

    private void fetchAndPopulateBatteryDetails(ViewHolder viewHolder, String vin, RentalVehicle rentalVehicle) {
        if(mContext == null) {
            return;
        }

        String userToken = mContext.getSharedPreferences(com.boltCore.android.constants.Constants.USER_PREFS, MODE_PRIVATE).getString(com.boltCore.android.constants.Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getVehicleLocationSnapshot(rentalVehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }

                    String dataStr = jsonObject.get("data").toString();

                    DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                    if(vehicleSnapshot != null && vehicleSnapshot.getBattery() != null) {

                        double batteryVoltageAdc = vehicleSnapshot.getBattery().getBatteryVoltageAdc();

                        if(rentalVehicle == null) {
                            return;
                        }

                        double maxVoltage = rentalVehicle.getModel().getConfig().getBatteryMaxVoltage();
                        double minVoltage = rentalVehicle.getModel().getConfig().getBatteryMinVoltage();
                        long socValue = 0;
                        double batteryPercentage = 0;

                        if (batteryVoltageAdc != 0 && maxVoltage != 0 && minVoltage != 0) {
                            batteryPercentage = ((batteryVoltageAdc - minVoltage) / (maxVoltage - minVoltage)) * 100;
                            socValue = Math.round(Math.floor(batteryPercentage));

                            if(socValue <= 0) {
                                socValue = 0;
                            } else if(socValue > 100) {
                                socValue = 100;
                            }
                        }

                        if(batteryVoltageAdc <= 0) {
                            batteryVoltageAdc = 0;
                        } else if(batteryVoltageAdc > 100) {
                            batteryVoltageAdc = 100;
                        }

                        batteryVoltageString = String.valueOf(Math.round(Math.floor(batteryVoltageAdc))).concat(" V");
                        batterySocString = String.valueOf(socValue).concat("%");


                        ((RentalActivity)mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                viewHolder.vehicleBatteryStatusTextView.setText(mContext.getString(R.string.battery)
                                        .concat(" : ")
                                        .concat(batteryVoltageString).concat("\n")
                                        .concat(mContext.getString(R.string.soc))
                                        .concat(" : ").concat(batterySocString));

                            }
                        });

                    }

                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) { }
        });
    }

    private void fetchVehicleDetailsAndPricingInfo(RentalVehicle rentalVehicle) {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();
        if(apolloClient == null) {
            showGenericMessage(false, mContext.getString(R.string.unable_to_fetch_vehicle_details), GenericMessageActivity.STATUS_ERROR);

            return;
        }

        showProgressDialog();

        InputGetRentalVehicles inputGetRentalVehicles = InputGetRentalVehicles
                .builder()
                .package_(mContext.getPackageName())
                .vehicle(VehicleWhereInput.builder().vin(rentalVehicle.getVin()).build())
                .build();

        FetchRentalVehicleDetailsAndPricingInfoQuery fetchRentalVehicleDetailsAndPricingInfoQuery = FetchRentalVehicleDetailsAndPricingInfoQuery
                .builder()
                .inputGetRentalVehicles(inputGetRentalVehicles)
                .build();


        apolloClient.query(fetchRentalVehicleDetailsAndPricingInfoQuery).enqueue(new ApolloCall.Callback<FetchRentalVehicleDetailsAndPricingInfoQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchRentalVehicleDetailsAndPricingInfoQuery.Data> response) {

                ((RentalActivity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                            showGenericMessage(false, mContext.getString(R.string.unable_to_fetch_vehicle_details), GenericMessageActivity.STATUS_ERROR);

                        } else if(response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getRentalVehicles() !=null ) {

                            FetchRentalVehicleDetailsAndPricingInfoQuery.GetRentalVehicle fetchedRentalVehicleDetails;

                            if(response.getData().vehicles().getRentalVehicles() != null && !response.getData().vehicles().getRentalVehicles().isEmpty()
                                    && response.getData().vehicles().getRentalVehicles().get(0) != null) {

                                fetchedRentalVehicleDetails = response.getData().vehicles().getRentalVehicles().get(0);

                                if(fetchedRentalVehicleDetails != null && fetchedRentalVehicleDetails.rentalStatus() == RentalStatus.AVAILABLE) {

                                    if(fetchedRentalVehicleDetails.txInfo() != null && fetchedRentalVehicleDetails.txInfo().pricing() != null) {
                                        List<FetchRentalVehicleDetailsAndPricingInfoQuery.Pricing> pricingList = fetchedRentalVehicleDetails.txInfo().pricing();

                                        if(pricingList != null && !pricingList.isEmpty()) {
                                            for(int index = 0 ; index < pricingList.size() ; index++) {

                                                RentalPricingInfo rentalPricingInfo = new RentalPricingInfo();

                                                if(pricingList.get(index).name() != null && !pricingList.get(index).name().isEmpty()) {
                                                    rentalPricingInfo.setPricingName(pricingList.get(index).name());
                                                }
                                                if(pricingList.get(index).type() != null) {
                                                    rentalPricingInfo.setPaymentType(pricingList.get(index).type());
                                                }
                                                if(pricingList.get(index).baseAmount() != null) {
                                                    rentalPricingInfo.setBaseAmount(pricingList.get(index).baseAmount().floatValue());
                                                }
                                                if(pricingList.get(index).costPerUnit() != null) {
                                                    rentalPricingInfo.setCostPerUnit(pricingList.get(index).costPerUnit().floatValue());
                                                }
                                                if(pricingList.get(index).unit() != null) {
                                                    rentalPricingInfo.setUnitType(pricingList.get(index).unit());
                                                }
                                                if(pricingList.get(index).paymentMeta() != null && pricingList.get(index).paymentMeta().address() != null
                                                        && !pricingList.get(index).paymentMeta().address().isEmpty()) {
                                                    rentalPricingInfo.setProviderUpiAddress(pricingList.get(0).paymentMeta().address());
                                                }
                                            }
                                        }
                                        rentalVehicle.setRentalPricingInfoList(pricingList);
                                    }

                                    checkKycForProvider(rentalVehicle);

                                } else {
                                    showVehicleNotAvailable();
                                }
                            } else {
                                unableToFetchVehicleDetails();
                            }
                        } else {
                            unableToFetchVehicleDetails();
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                ((RentalActivity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        showGenericMessage(false, mContext.getString(R.string.server_error), GenericMessageActivity.STATUS_ERROR);
                    }
                });
            }
        });
    }

    private void checkKycForProvider(RentalVehicle rentalVehicle) {

        String providerId = null;

        if(rentalVehicle.getCompany() != null && rentalVehicle.getCompany().getId() != null
                && !rentalVehicle.getCompany().getId().isEmpty()) {

            providerId = rentalVehicle.getCompany().getId();

        } else if(rentalVehicle.getOem() != null && rentalVehicle.getOem().getId() != null
                && !rentalVehicle.getOem().getId().isEmpty()) {

            providerId = rentalVehicle.getOem().getId();

        } else if(rentalVehicle.getDistributor() != null && rentalVehicle.getDistributor().getId() != null
                && !rentalVehicle.getDistributor().getId().isEmpty()) {

            providerId = rentalVehicle.getDistributor().getId();
        }
        //if Provider's id is available, check KYC association
        if(providerId != null && !providerId.isEmpty()) {

            checkKycAssociationForSelectedCompany(rentalVehicle);

        } else {
            hideProgressDialog();

            launchKycActivity(rentalVehicle);
        }
    }

    private void launchKycActivity(RentalVehicle rentalVehicle) {

        //start KYC activity
        Intent myIntent = new Intent(mContext, RentalKycActivity.class);
        myIntent.putExtra(RENTAL_KYC_VEHICLE_OBJECT_KEY, new Gson().toJson(rentalVehicle));
        mContext.startActivity(myIntent);
    }

    private void checkKycAssociationForSelectedCompany(RentalVehicle rentalVehicle) {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();
        if(apolloClient == null) {
            hideProgressDialog();

            showGenericMessage(false, mContext.getString(R.string.error), GenericMessageActivity.STATUS_ERROR);
            return;
        }

        String providerId = null;

        if(rentalVehicle.getCompany() != null && rentalVehicle.getCompany().getId() != null
                && !rentalVehicle.getCompany().getId().isEmpty()) {

            providerId = rentalVehicle.getCompany().getId();

        } else if(rentalVehicle.getOem() != null && rentalVehicle.getOem().getId() != null
                && !rentalVehicle.getOem().getId().isEmpty()) {

            providerId = rentalVehicle.getOem().getId();

        } else if(rentalVehicle.getDistributor() != null && rentalVehicle.getDistributor().getId() != null
                && !rentalVehicle.getDistributor().getId().isEmpty()) {

            providerId = rentalVehicle.getDistributor().getId();
        }

        CompanyWhereUniqueInput companyWhereUniqueInput = CompanyWhereUniqueInput
                .builder()
                .id(providerId)
                .build();

        FetchKYCAssociationForUserQuery getKycAssociationForRentalQuery = FetchKYCAssociationForUserQuery
                .builder()
                .companyWhereUniqueInput(companyWhereUniqueInput)
                .build();

        apolloClient.query(getKycAssociationForRentalQuery).enqueue(new ApolloCall.Callback<FetchKYCAssociationForUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchKYCAssociationForUserQuery.Data> response) {

                ((RentalActivity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        if(response.getData() != null && response.getData().account() != null && response.getData().account().getKYC() != null) {
                            if(response.getData().account().getKYC().status().equals(KYCStatus.APPROVED)) {

                                launchBookVehicleActivity(rentalVehicle);
                            } else {
                                launchKycActivity(rentalVehicle);
                            }
                        } else {
                            launchKycActivity(rentalVehicle);
                        }
                    }
                });
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                ((RentalActivity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        launchKycActivity(rentalVehicle);
                    }
                });
            }
        });
    }

    private void launchBookVehicleActivity(RentalVehicle rentalVehicle) {

        Intent intent = new Intent(mContext, BookRentalVehicleActivity.class);
        intent.putExtra(BOOK_RENTAL_VEHICLE_OBJECT_KEY, new Gson().toJson(rentalVehicle));
        ((RentalActivity)mContext).startActivityForResult(intent, BOOKING_ACTIVITY_REQUEST_CODE);
    }

    private void unableToFetchVehicleDetails() {
        Intent intent = new Intent(mContext, GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, mContext.getString(R.string.unable_to_fetch_vehicle_details));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    private void showVehicleNotAvailable() {
        hideProgressDialog();

        Intent intent = new Intent(mContext, GenericMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, mContext.getString(R.string.vehicle_not_available_for_renting));
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, GenericMessageActivity.STATUS_ERROR);
        bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 2000);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    private void showGenericMessage(boolean makeMsgPersistent, String message, int messageType) {

        Intent intent = new Intent(mContext, GenericMessageActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(GENERIC_MESSAGE_TEXT_KEY, message);
        bundle.putInt(GENERIC_MESSAGE_STATUS_KEY, messageType);

        if(makeMsgPersistent) {
            bundle.putString(GENERIC_MESSAGE_BUTTON_TEXT_KEY, mContext.getString(R.string.okay));

        } else {
            bundle.putInt(GENERIC_MESSAGE_TIMER_KEY, 3000);
        }

        intent.putExtras(bundle);
        mContext.startActivity(intent);

    }

    @Override
    public int getItemCount() {
        return mRentalVehiclesList.size();
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder viewHolder) {
        super.onViewRecycled(viewHolder);
    }

    private void showProgressDialog() {
        ((RentalActivity)mContext).showProgressDialog();
    }

    private void hideProgressDialog() {
        ((RentalActivity)mContext).hideProgressDialog();
    }
}