package com.revos.android.utilities.vehicleDataTransfer;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import androidx.core.util.Pair;

import static com.revos.android.utilities.vehicleDataTransfer.SpeedometerDataExchanger.PACKET_TYPE_BATTERY_ADC;
import static com.revos.android.utilities.vehicleDataTransfer.SpeedometerDataExchanger.PACKET_TYPE_OFFLINE;

public class BatteryAdcLogPacketParser {
    private Context mContext;
    private final String TIMESTAMP_KEY = "timeStamp";
    private final String TIMESTAMP_TRUE_KEY = "timeStampTrueFlag";
    private final String BATTERY_VOLTAGE_ADC = "batteryVoltageADC";

    public BatteryAdcLogPacketParser(Context context) {
        mContext = context;
    }

    public ArrayList<BatteryAdcParameters> parseLogPacket(String dataString) {
        String[] dataArray = dataString.split(" ");

        //check if this data is valid or not
        //first check if the length is valid
        SpeedometerDataExchanger speedometerDataExchanger = SpeedometerDataExchanger.getInstance(mContext);
        if(speedometerDataExchanger == null) {
            return null;
        }

        if(speedometerDataExchanger.getMetadataFirmwareVersion() != null) {
            return parseOfflineAdcLogPackets(dataArray, speedometerDataExchanger);
        } else {
            return null;
        }
    }

    private ArrayList<BatteryAdcParameters> parseOfflineAdcLogPackets(String[] dataArray, SpeedometerDataExchanger speedometerDataExchanger) {
        int logPacketHeaderLength = speedometerDataExchanger.getOfflineBatteryAdcLogPacketHeaderLength();
        int parameterFormatLength = speedometerDataExchanger.getOfflineBatteryAdcLogParameterLength();

        if((dataArray.length - logPacketHeaderLength) % parameterFormatLength != 0) {
            return null;
        }

        //data length is valid
        int noOfPackets = (dataArray.length - logPacketHeaderLength) / parameterFormatLength;

        ArrayList<BatteryAdcParameters> batteryAdcParametersArrayList = new ArrayList<>();

        for(int packetIndex = 0; packetIndex < noOfPackets; ++packetIndex) {

            int startIndex = logPacketHeaderLength + packetIndex * parameterFormatLength;
            String[] partDataArray = new String[parameterFormatLength];

            for(int byteIndex = startIndex; byteIndex < startIndex + parameterFormatLength; ++byteIndex) {
                partDataArray[byteIndex - startIndex] = (dataArray[byteIndex]);
            }

            BatteryAdcParameters batteryAdcParameters = new BatteryAdcParameters();

            batteryAdcParameters.setTimestamp((long)speedometerDataExchanger.decodeParamValue(TIMESTAMP_KEY, partDataArray, PACKET_TYPE_BATTERY_ADC));
            batteryAdcParameters.setTimeStampTrue((long)speedometerDataExchanger.decodeParamValue(TIMESTAMP_TRUE_KEY, partDataArray, PACKET_TYPE_BATTERY_ADC));
            batteryAdcParameters.setBatteryAdcVoltage((float)speedometerDataExchanger.decodeParamValue(BATTERY_VOLTAGE_ADC, partDataArray, PACKET_TYPE_BATTERY_ADC));

            batteryAdcParametersArrayList.add(batteryAdcParameters);
        }


        return batteryAdcParametersArrayList;
    }
}
