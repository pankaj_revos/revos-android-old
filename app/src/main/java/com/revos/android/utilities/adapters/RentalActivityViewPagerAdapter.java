/*************************************************************************
 *
 * RevOS CONFIDENTIAL
 * __________________
 *
 *  [2016] RevOS Pvt Ltd
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of RevOS Pvt Ltd and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to RevOS Pvt Ltd
 * and its suppliers and may be covered by India and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from RevOS Pvt Ltd.
 */

package com.revos.android.utilities.adapters;

/**
 * Created by moyadav on 23/09/2020.
 */

import android.content.Context;

import com.revos.android.fragments.RentalBookingsInfoFragment;
import com.revos.android.fragments.RentalMapFragment;
import com.revos.android.fragments.RentalScanQRCodeFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


/**
 * Created by hp1 on 21-01-2015.
 */
public class RentalActivityViewPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence mTitles[]; // This will Store the Titles of the Tabs which are Going to be passed when MainActivityViewPagerAdapter is created
    private int mNumberOfTabs; // Store the number of tabs, this will also be passed when the MainActivityViewPagerAdapter is created
    private Context mContext;

    private int[] mDrawableIds;

    public int getDrawableId(int position){
        //Here is only example for getting tab drawables
        return mDrawableIds[position];
    }

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public RentalActivityViewPagerAdapter(FragmentManager fm, CharSequence titles[], int numberOfTabs, int[] drawablesIds, Context context) {
        super(fm);

        this.mTitles = titles;
        this.mNumberOfTabs = numberOfTabs;
        this.mDrawableIds = drawablesIds;
        this.mContext = context;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        Fragment tab = null ;
        if(position == 0) {
            tab = new RentalMapFragment();
        } else if(position == 1) {
            tab = new RentalScanQRCodeFragment();
        } else if(position == 2) {
            tab = new RentalBookingsInfoFragment();
        }
        return tab;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
        //return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return mNumberOfTabs;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
