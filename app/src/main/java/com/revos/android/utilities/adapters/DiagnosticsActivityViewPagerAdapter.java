package com.revos.android.utilities.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.revos.android.fragments.DiagnosticsControlFragment;
import com.revos.android.fragments.DiagnosticsGraphFragment;
import com.revos.android.fragments.DiagnosticsTextFragment;

public class DiagnosticsActivityViewPagerAdapter extends FragmentPagerAdapter {

    private int totalTabs;

    public DiagnosticsActivityViewPagerAdapter(Context context, FragmentManager fragmentManager, int totalTabs) {
        super(fragmentManager);

        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DiagnosticsGraphFragment();

            case 1:
                return new DiagnosticsTextFragment();

            case 2:
                return new DiagnosticsControlFragment();

            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}
