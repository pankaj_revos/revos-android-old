/*************************************************************************
 *
 * RevOS CONFIDENTIAL
 * __________________
 *
 *  [2016] RevOS Pvt Ltd
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of RevOS Pvt Ltd and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to RevOS Pvt Ltd
 * and its suppliers and may be covered by India and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from RevOS Pvt Ltd.
 */

package com.revos.android.utilities.adapters;

/**
 * Created by moyadav on 5/12/2016.
 */

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.revos.android.activities.MainActivity;
import com.revos.android.fragments.AnalyticsFragment;
import com.revos.android.fragments.GoogleMatchedPoiListFragment;
import com.revos.android.fragments.GuestAnalyticsFragment;
import com.revos.android.fragments.GuestNavigationFragment;
import com.revos.android.fragments.GuestOBDFragment;
import com.revos.android.fragments.NavigationFragment;
import com.revos.android.fragments.OBDFragment;
import com.revos.android.fragments.SetupVehicleFragment;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.PrefUtils;

import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.IS_APP_IN_GUEST_MODE;

/**
 * Created by hp1 on 21-01-2015.
 */
public class MainActivityViewPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence mTitles[]; // This will Store the Titles of the Tabs which are Going to be passed when MainActivityViewPagerAdapter is created
    private int mNumberOfTabs; // Store the number of tabs, this will also be passed when the MainActivityViewPagerAdapter is created
    private Context mContext;

    private int[] mDrawableIds;

    public int getDrawableId(int position){
        //Here is only example for getting tab drawables
        return mDrawableIds[position];
    }

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public MainActivityViewPagerAdapter(FragmentManager fm, CharSequence titles[], int numberOfTabs, int[] drawablesIds, Context context) {
        super(fm);

        this.mTitles = titles;
        this.mNumberOfTabs = numberOfTabs;
        this.mDrawableIds = drawablesIds;
        this.mContext = context;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        boolean isAppInGuestMode = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE).getBoolean(IS_APP_IN_GUEST_MODE, false);
        Fragment tab = null ;
        if(position == 0) { // if the position is 0 we are returning the First tab
            if(isAppInGuestMode) {
                tab = new GuestNavigationFragment();
            } else if(((MainActivity)mContext).mNavigateToOnVoiceResultReceived) {
                tab = new GoogleMatchedPoiListFragment();
            } else {
                tab = new NavigationFragment();
            }
        } else if(position == 1) {             // As we are having multiple tabs
            if(isAppInGuestMode) {
                tab = new GuestOBDFragment();
            } else {
                tab = new OBDFragment();
            }
        } else if(position == 2) {             // As we are having multiple tabs
            //check if vehicle setup is complete or not
            //check if vehicle data is empty or not
            Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();
            if(isAppInGuestMode) {
                tab = new GuestAnalyticsFragment();
            } else if(((MainActivity)mContext).mShowSetupVehicleFragment && vehicle == null) {
                tab = new SetupVehicleFragment();
            } else {
                tab = new AnalyticsFragment();
            }
        }

        return tab;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
        //return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return mNumberOfTabs;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
