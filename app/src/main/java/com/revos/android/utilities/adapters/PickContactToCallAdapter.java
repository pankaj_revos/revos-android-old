package com.revos.android.utilities.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.revos.android.R;
import com.revos.android.activities.PickContactToCallActivity;
import com.revos.android.jsonStructures.GenericPhoneEntry;
import com.revos.android.utilities.general.PhoneUtils;

import java.util.ArrayList;

import static com.revos.android.constants.Constants.ENTRY_TYPE_CONTACT_SEARCH_RESULT;
import static com.revos.android.constants.Constants.PHONE_NUMBER_REGEX;

/**
 * Created by mohit on 17/4/17.
 */

public class PickContactToCallAdapter extends RecyclerView.Adapter<PickContactToCallAdapter.ViewHolder> {
    private ArrayList<GenericPhoneEntry> mContactResultList;
    private Context mContext;

    public PickContactToCallAdapter(ArrayList<GenericPhoneEntry> matchedContactList, Context context) {
        mContactResultList = matchedContactList;
        mContext = context;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view viewHolder

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout innerContentLayout;
        public TextView contactHeadingTextView, contactDescTextView;
        public ImageView contactImageView;

        public ViewHolder(@NonNull View v) {
            super(v);

            innerContentLayout = (RelativeLayout) v.findViewById(R.id.pick_contact_card_inner_content_relative_layout);
            contactHeadingTextView = (TextView) v.findViewById(R.id.pick_contact_heading_text_view);
            contactDescTextView = (TextView) v.findViewById(R.id.pick_contact_desc_text_view);
            contactImageView = (ImageView) v.findViewById(R.id.pick_contact_image_view);
        }
    }

    @NonNull
    @Override
    public PickContactToCallAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pick_contact_list_item, parent, false);

        return new PickContactToCallAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        final GenericPhoneEntry currentEntry = mContactResultList.get(position);

        if(currentEntry.getEntryType() == null) {
            return;
        }

        //first obtain type of entry
        if(currentEntry.getEntryType().equals(ENTRY_TYPE_CONTACT_SEARCH_RESULT)) {

            viewHolder.contactImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_calls_light));
            //update the desc
            viewHolder.contactDescTextView.setText(currentEntry.getPhoneNumber());
        }

        viewHolder.contactHeadingTextView.setText(currentEntry.getName());

        viewHolder.innerContentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set this in case a number is clicked from call voice command search result
                PhoneUtils.getInstance(mContext).setNumberInAction(currentEntry.getPhoneNumber());

                String phoneNumberToBeDialled = "";

                //check if this is actually a phone number or a possible contact name as it is now possible with us using only notification listener
                if(currentEntry.getPhoneNumber().matches(PHONE_NUMBER_REGEX)) {
                    phoneNumberToBeDialled = currentEntry.getPhoneNumber();
                } else {
                    //this is most likely a contact, just try to do our best here and pick the first number we can find for this contact and dial it
                    ArrayList<String> contactPhoneNumbers = PhoneUtils.getInstance(mContext).findContactNumbers(currentEntry.getPhoneNumber());
                    if(contactPhoneNumbers != null && !contactPhoneNumbers.isEmpty()) {
                        phoneNumberToBeDialled = contactPhoneNumbers.get(0);
                    }
                }

                if(!phoneNumberToBeDialled.isEmpty()) {

                    PhoneUtils.getInstance((PickContactToCallActivity)mContext).callNumber(phoneNumberToBeDialled);
                    ((PickContactToCallActivity)mContext).finish();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContactResultList.size();
    }

}