package com.revos.android.utilities.vehicleDataTransfer;

public class VehicleParameters {
    //values that will be logged
    private long tripId;
    private long timeStamp;
    private float speed;
    private float rpm;
    private float throttle;
    private float voltage;
    private float current;
    private float odo;
    private float temperature;

    private int mode;

    private boolean isLocked;
    private boolean isOverLoad;
    private boolean isOverCurrent;
    private boolean isReverse;
    private boolean isParking;
    private boolean isAntiTheftEngaged;
    private boolean isEABSEngaged;
    private boolean isCharging;
    private boolean isRegenBraking;
    private boolean isBraking;
    private boolean isThrottleError;
    private boolean isControllerError;
    private boolean isMotorError;
    private boolean isHillAssistEngaged;

    private float soc;

    //vehicle peripheral values
    private boolean isHeadLampOn;
    private boolean isLeftIndicatorOn;
    private boolean isRightIndicatorOn;
    private float xAcc;
    private float yAcc;
    private float zAcc;
    private long antiTheftMode;
    private long antiTheftSensitivity;
    private float batteryAdcVoltage;
    private long followMeHomeHeadlampOnTime;
    private boolean genericWheelLockEngagedStatus;
    private boolean genericWheelLockStatus;
    private boolean geoFencingLockStatus;

    //values for cross-validation
    private long controlCheckByte;
    private long speedCheckByte;
    private long currentCheckByte;
    private long underVoltageCheckByte;
    private long overVoltageCheckByte;
    private long throttleZeroRegenCheckByte;
    private long brakeRegenCheckByte;
    private long pickupPercentageCheckByte;
    private long displayedImageCode;
    private long checksumCode;

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getRpm() {
        return rpm;
    }

    public void setRpm(float rpm) {
        this.rpm = rpm;
    }

    public float getThrottle() {
        return throttle;
    }

    public void setThrottle(float throttle) {
        this.throttle = throttle;
    }

    public float getVoltage() {
        return voltage;
    }

    public void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    public float getCurrent() {
        return current;
    }

    public void setCurrent(float current) {
        this.current = current;
    }

    public float getOdo() {
        return odo;
    }

    public void setOdo(float odo) {
        this.odo = odo;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public boolean isOverLoad() {
        return isOverLoad;
    }

    public void setOverLoad(boolean overLoad) {
        isOverLoad = overLoad;
    }

    public boolean isOverCurrent() {
        return isOverCurrent;
    }

    public void setOverCurrent(boolean overCurrent) {
        isOverCurrent = overCurrent;
    }

    public boolean isReverse() {
        return isReverse;
    }

    public void setReverse(boolean reverse) {
        isReverse = reverse;
    }

    public boolean isParking() {
        return isParking;
    }

    public void setParking(boolean parking) {
        isParking = parking;
    }

    public boolean isAntiTheftEngaged() {
        return isAntiTheftEngaged;
    }

    public void setAntiTheftEngaged(boolean antiTheftEngaged) {
        isAntiTheftEngaged = antiTheftEngaged;
    }

    public float getBatteryAdcVoltage() {
        return batteryAdcVoltage;
    }

    public void setBatteryAdcVoltage(float batteryAdcVoltage) {
        this.batteryAdcVoltage = batteryAdcVoltage;
    }

    public long getFollowMeHomeHeadlampOnTime() {
        return followMeHomeHeadlampOnTime;
    }

    public void setFollowMeHomeHeadlampOnTime(long followMeHomeHeadlampOnTime) {
        this.followMeHomeHeadlampOnTime = followMeHomeHeadlampOnTime;
    }

    public boolean isGenericWheelLockEngaged() {
        return genericWheelLockEngagedStatus;
    }

    public void setGenericWheelLockEngagedStatus(boolean genericWheelLockEngagedStatus) {
        this.genericWheelLockEngagedStatus = genericWheelLockEngagedStatus;
    }

    public boolean isGenericWheelLockSet() {
        return genericWheelLockStatus;
    }

    public void setGenericWheelLockStatus(boolean genericWheelLockStatus) {
        this.genericWheelLockStatus = genericWheelLockStatus;
    }

    public boolean isGeoFencingLockSet() {
        return geoFencingLockStatus;
    }

    public void setGeoFencingLockStatus(boolean geoFencingLockStatus) {
        this.geoFencingLockStatus = geoFencingLockStatus;
    }

    public boolean isEABSEngaged() {
        return isEABSEngaged;
    }

    public void setEABSEngaged(boolean EABSEngaged) {
        isEABSEngaged = EABSEngaged;
    }

    public boolean isCharging() {
        return isCharging;
    }

    public void setCharging(boolean charging) {
        isCharging = charging;
    }

    public boolean isRegenBraking() {
        return isRegenBraking;
    }

    public void setRegenBraking(boolean regenBraking) {
        isRegenBraking = regenBraking;
    }

    public boolean isBraking() {
        return isBraking;
    }

    public void setBraking(boolean braking) {
        isBraking = braking;
    }

    public boolean isThrottleError() {
        return isThrottleError;
    }

    public void setThrottleError(boolean throttleError) {
        isThrottleError = throttleError;
    }

    public boolean isControllerError() {
        return isControllerError;
    }

    public void setControllerError(boolean controllerError) {
        isControllerError = controllerError;
    }

    public boolean isMotorError() {
        return isMotorError;
    }

    public void setMotorError(boolean motorError) {
        isMotorError = motorError;
    }

    public boolean isHillAssistEngaged() {
        return isHillAssistEngaged;
    }

    public void setHillAssistEngaged(boolean hillAssistEngaged) {
        isHillAssistEngaged = hillAssistEngaged;
    }

    public boolean isHeadLampOn() {
        return isHeadLampOn;
    }

    public void setHeadLampOn(boolean headLampOn) {
        isHeadLampOn = headLampOn;
    }

    public boolean isLeftIndicatorOn() {
        return isLeftIndicatorOn;
    }

    public void setLeftIndicatorOn(boolean leftIndicatorOn) {
        isLeftIndicatorOn = leftIndicatorOn;
    }

    public boolean isRightIndicatorOn() {
        return isRightIndicatorOn;
    }

    public void setRightIndicatorOn(boolean rightIndicatorOn) {
        isRightIndicatorOn = rightIndicatorOn;
    }

    public float getxAcc() {
        return xAcc;
    }

    public void setxAcc(float xAcc) {
        this.xAcc = xAcc;
    }

    public float getyAcc() {
        return yAcc;
    }

    public void setyAcc(float yAcc) {
        this.yAcc = yAcc;
    }

    public float getzAcc() {
        return zAcc;
    }

    public void setzAcc(float zAcc) {
        this.zAcc = zAcc;
    }

    public long getAntiTheftMode() {
        return antiTheftMode;
    }

    public void setAntiTheftMode(long antiTheftMode) {
        this.antiTheftMode = antiTheftMode;
    }

    public long getAntiTheftSensitivity() {
        return antiTheftSensitivity;
    }

    public void setAntiTheftSensitivity(long antiTheftSensitivity) {
        this.antiTheftSensitivity = antiTheftSensitivity;
    }

    public long getControlCheckByte() {
        return controlCheckByte;
    }

    public void setControlCheckByte(long controlCheckByte) {
        this.controlCheckByte = controlCheckByte;
    }

    public long getSpeedCheckByte() {
        return speedCheckByte;
    }

    public void setSpeedCheckByte(long speedCheckByte) {
        this.speedCheckByte = speedCheckByte;
    }

    public long getCurrentCheckByte() {
        return currentCheckByte;
    }

    public void setCurrentCheckByte(long currentCheckByte) {
        this.currentCheckByte = currentCheckByte;
    }

    public long getUnderVoltageCheckByte() {
        return underVoltageCheckByte;
    }

    public void setUnderVoltageCheckByte(long underVoltageCheckByte) {
        this.underVoltageCheckByte = underVoltageCheckByte;
    }

    public long getOverVoltageCheckByte() {
        return overVoltageCheckByte;
    }

    public void setOverVoltageCheckByte(long overVoltageCheckByte) {
        this.overVoltageCheckByte = overVoltageCheckByte;
    }

    public long getThrottleZeroRegenCheckByte() {
        return throttleZeroRegenCheckByte;
    }

    public void setThrottleZeroRegenCheckByte(long throttleZeroRegenCheckByte) {
        this.throttleZeroRegenCheckByte = throttleZeroRegenCheckByte;
    }

    public long getBrakeRegenCheckByte() {
        return brakeRegenCheckByte;
    }

    public void setBrakeRegenCheckByte(long brakeRegenCheckByte) {
        this.brakeRegenCheckByte = brakeRegenCheckByte;
    }

    public long getPickupPercentageCheckByte() {
        return pickupPercentageCheckByte;
    }

    public void setPickupPercentageCheckByte(long pickupPercentageCheckByte) {
        this.pickupPercentageCheckByte = pickupPercentageCheckByte;
    }

    public long getDisplayedImageCode() {
        return displayedImageCode;
    }

    public void setDisplayedImageCode(long displayedImageCode) {
        this.displayedImageCode = displayedImageCode;
    }

    public long getChecksumCode() {
        return checksumCode;
    }

    public void setChecksumCode(long checksumCode) {
        this.checksumCode = checksumCode;
    }

    public float getSoc() {
        return soc;
    }

    public void setSoc(float soc) {
        this.soc = soc;
    }
}
