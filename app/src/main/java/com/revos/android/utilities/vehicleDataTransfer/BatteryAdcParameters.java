package com.revos.android.utilities.vehicleDataTransfer;

public class BatteryAdcParameters {
    private long timestamp;
    private long isTimeStampTrue;
    private float batteryAdcVoltage;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long isTimeStampTrue() {
        return isTimeStampTrue;
    }

    public void setTimeStampTrue(long timeStampTrue) {
        isTimeStampTrue = timeStampTrue;
    }

    public float getBatteryAdcVoltage() {
        return batteryAdcVoltage;
    }

    public void setBatteryAdcVoltage(float batteryAdcVoltage) {
        this.batteryAdcVoltage = batteryAdcVoltage;
    }

}
