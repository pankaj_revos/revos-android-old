package com.revos.android.utilities.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.activities.UPIPaymentsActivity;
import com.revos.android.jsonStructures.PassbookTxn;
import com.revos.android.utilities.general.DateTimeUtils;
import com.revos.scripts.type.PassbookTxStatus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.revos.android.activities.UPIPaymentsActivity.RENTAL_AMOUNT_TO_BE_PAID;
import static com.revos.android.activities.UPIPaymentsActivity.RENTAL_EXISTING_REMARK;
import static com.revos.android.activities.UPIPaymentsActivity.RENTAL_PASSBOOK_TRANSACTION_ID;
import static com.revos.android.activities.UPIPaymentsActivity.RENTAL_PAYEE_NAME;
import static com.revos.android.activities.UPIPaymentsActivity.RENTAL_PAYEE_UPI_ID;

public class PassbookTransactionListAdapter extends RecyclerView.Adapter<PassbookTransactionListAdapter.ViewHolder> {

    private List<PassbookTxn> mPassbookList;
    private Context mContext;
    private String mPayeeName;

    public PassbookTransactionListAdapter(String payeeName, List<PassbookTxn> passbookList, Context context) {
        mPassbookList = passbookList;
        mContext = context;
        mPayeeName = payeeName;
    }

    @NonNull
    @Override
    public PassbookTransactionListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.passbook_transactions_list_item_view, viewGroup, false);

        return new PassbookTransactionListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout listItemLinearLayout, paidOnLinearLayout;
        TextView transactionVinTextView, statusTextView, amountDueTextView, dueDateTextView, paymentDateTextView;
        ImageView transactionStatusImageView;

        Button payNowButton;

        public ViewHolder(@NonNull View view) {

            super(view);

            listItemLinearLayout = view.findViewById(R.id.passbook_transactions_item_linear_layout);
            paidOnLinearLayout = view.findViewById(R.id.passbook_transactions_item_paid_on_linear_layout);

            transactionVinTextView = view.findViewById(R.id.passbooks_transaction_item_invoice_vin_value_text_view);
            statusTextView = view.findViewById(R.id.passbooks_transaction_item_status_text_view);
            dueDateTextView = view.findViewById(R.id.passbooks_transaction_item_due_date_value_text_view);
            amountDueTextView = view.findViewById(R.id.passbooks_transaction_item_amount_value_text_view);
            paymentDateTextView = view.findViewById(R.id.passbook_transactions_item_paid_on_date_text_view);
            payNowButton = view.findViewById(R.id.passbook_transactions_item_pay_now_button);

            transactionStatusImageView = view.findViewById(R.id.passbook_transactions_list_item_status_image_view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PassbookTransactionListAdapter.ViewHolder viewHolder, int position) {
        PassbookTxn passbook = mPassbookList.get(position);

        if(passbook == null) {
            return;
        }

        if(passbook.getVin() != null && !passbook.getVin().isEmpty()) {
            viewHolder.transactionVinTextView.setText(passbook.getVin());
        } else {
            viewHolder.transactionVinTextView.setText(mContext.getString(R.string.not_available));
        }

        if(passbook.getPassbookTxStatus() != null) {
            viewHolder.statusTextView.setText(passbook.getPassbookTxStatus().toString());
        } else {
            viewHolder.statusTextView.setText(mContext.getString(R.string.not_available));
        }

        if(passbook.getPassbookTxStatus().equals(PassbookTxStatus.COMPLETED) || passbook.getPassbookTxStatus().equals(PassbookTxStatus.PAID)) {
            viewHolder.statusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.revos_green));
            viewHolder.transactionStatusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_revos_check));
        } else {
            viewHolder.statusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.color_critical_error_background));
            viewHolder.transactionStatusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_error_icon));
        }

        if(passbook.getDueDate() != null && !passbook.getDueDate().isEmpty()) {
            viewHolder.dueDateTextView.setText(DateTimeUtils.getInstance(mContext).getDateFromTimeStamp(passbook.getDueDate()));
        } else {
            viewHolder.dueDateTextView.setText(mContext.getString(R.string.not_available));
        }

        if(passbook.getAmount() != null && !passbook.getAmount().isEmpty()) {
            viewHolder.amountDueTextView.setText(String.format("%s%s", mContext.getString(R.string.rupee), passbook.getAmount()));
        } else {
            viewHolder.amountDueTextView.setText(mContext.getString(R.string.not_available));
        }

        if(passbook.getPassbookTxStatus().equals(PassbookTxStatus.PAID)) {
            viewHolder.payNowButton.setVisibility(View.GONE);

            viewHolder.paidOnLinearLayout.setVisibility(View.VISIBLE);
            viewHolder.paymentDateTextView.setText(DateTimeUtils.getInstance(mContext).getDateFromTimeStamp(passbook.getPaymentDate()));
        } else {
            viewHolder.paidOnLinearLayout.setVisibility(View.GONE);

            viewHolder.payNowButton.setVisibility(View.VISIBLE);
        }

        viewHolder.payNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString(RENTAL_PASSBOOK_TRANSACTION_ID, passbook.getId());
                bundle.putString(RENTAL_PAYEE_UPI_ID, passbook.getPayeeUpiId());
                bundle.putString(RENTAL_PAYEE_NAME, mPayeeName);
                bundle.putString(RENTAL_AMOUNT_TO_BE_PAID, passbook.getAmount());
                bundle.putString(RENTAL_EXISTING_REMARK, passbook.getRemark());

                Intent intent = new Intent(mContext, UPIPaymentsActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPassbookList.size();
    }
}
