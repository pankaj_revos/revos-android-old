package com.revos.android.utilities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.utilities.general.DateTimeUtils;
import com.revos.android.utilities.general.PhoneUtils;
import com.revos.scripts.FetchLeaseForUserQuery;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RentalBookingHistoryAdapter extends RecyclerView.Adapter<RentalBookingHistoryAdapter.ViewHolder> {

    private List<FetchLeaseForUserQuery.GetLeasesForUser> mRentalHistoryList;
    private Context mContext;

    public RentalBookingHistoryAdapter(List<FetchLeaseForUserQuery.GetLeasesForUser> rentalHistoryList, Context context) {
        mRentalHistoryList = rentalHistoryList;
        mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout listItemLinearLayout;
        TextView bookingIdTextView, vehicleModelBooked, bookingAmountPaidTextView, bookingStartDateTextView,
                bookingEndDateTextView, vinTextView, bookingRentalProviderNameTextView, bookingRentalProviderPhoneTextView;

        public ViewHolder(@NonNull View view) {

            super(view);

            listItemLinearLayout = view.findViewById(R.id.booking_history_row_item_linear_layout);

            bookingIdTextView = view.findViewById(R.id.booking_history_row_item_booking_id_value_text_view);
            vehicleModelBooked = view.findViewById(R.id.booking_history_row_vehicle_model_value_text_view);
            bookingAmountPaidTextView = view.findViewById(R.id.booking_history_row_item_amount_paid_value_text_view);
            bookingStartDateTextView = view.findViewById(R.id.booking_history_row_item_start_date_text_view);
            bookingEndDateTextView = view.findViewById(R.id.booking_history_row_item_end_date_text_view);
            vinTextView = view.findViewById(R.id.booking_history_row_vehicle_vin_value_text_view);
            bookingRentalProviderNameTextView = view.findViewById(R.id.booking_history_rental_provider_name_text_view);
            bookingRentalProviderPhoneTextView = view.findViewById(R.id.booking_history_rental_provider_phone_text_view);
        }
    }

    @NonNull
    @Override
    public RentalBookingHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.booking_history_row_item, viewGroup, false);

        return new RentalBookingHistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RentalBookingHistoryAdapter.ViewHolder viewHolder, int position) {

        FetchLeaseForUserQuery.GetLeasesForUser getLeasesForUser = mRentalHistoryList.get(position);

        if(getLeasesForUser != null) {

            viewHolder.bookingIdTextView.setAllCaps(true);
            if(!getLeasesForUser.id().isEmpty()) {
                viewHolder.bookingIdTextView.setText(getLeasesForUser.id().substring(getLeasesForUser.id().length() - 8));
            } else {
                viewHolder.bookingIdTextView.setText(mContext.getString(R.string.not_available));
            }

            if(getLeasesForUser.contract() != null && getLeasesForUser.contract().txInfo() != null && getLeasesForUser.contract().txInfo().invoice() != null
                    && getLeasesForUser.contract().txInfo().invoice().passbook() != null && ! getLeasesForUser.contract().txInfo().invoice().passbook().isEmpty()) {

                List<FetchLeaseForUserQuery.Passbook> passbookList = getLeasesForUser.contract().txInfo().invoice().passbook();
                if(passbookList != null && !passbookList.isEmpty()) {

                    if(passbookList.get(0).amount() != null) {
                        viewHolder.bookingAmountPaidTextView.setText(mContext.getString(R.string.rupee) + String.valueOf(passbookList.get(0).amount()));
                    } else {
                        viewHolder.bookingAmountPaidTextView.setText(mContext.getString(R.string.not_available));
                    }
                } else {
                    viewHolder.bookingAmountPaidTextView.setText(mContext.getString(R.string.not_available));
                }
            } else {
                viewHolder.bookingAmountPaidTextView.setText(mContext.getString(R.string.not_available));
            }

            if(getLeasesForUser.leasor() != null && getLeasesForUser.leasor().company() != null) {
                if(getLeasesForUser.leasor().company().name() != null && !getLeasesForUser.leasor().company().name().isEmpty()) {
                    viewHolder.bookingRentalProviderNameTextView.setText(getLeasesForUser.leasor().company().name());
                } else {
                    viewHolder.bookingRentalProviderNameTextView.setText(mContext.getString(R.string.not_available));
                }
            } else {
                viewHolder.bookingRentalProviderNameTextView.setText(mContext.getString(R.string.not_available));
            }

            if(getLeasesForUser.leasor() != null && getLeasesForUser.leasor().company() != null) {
                if(getLeasesForUser.leasor().company().phone() != null && !getLeasesForUser.leasor().company().phone().isEmpty()) {
                    String sanitizedPhoneNumber = PhoneUtils.getInstance(mContext).sanitizePhoneNumber(getLeasesForUser.leasor().company().phone());

                    viewHolder.bookingRentalProviderPhoneTextView.setText(sanitizedPhoneNumber);
                } else {
                    viewHolder.bookingRentalProviderPhoneTextView.setText(mContext.getString(R.string.not_available));
                }
            } else {
                viewHolder.bookingRentalProviderPhoneTextView.setText(mContext.getString(R.string.not_available));
            }

            if(getLeasesForUser.asset() != null && getLeasesForUser.asset().vehicle() != null
                    && getLeasesForUser.asset().vehicle().model() != null && getLeasesForUser.asset().vehicle().model().name() != null && !getLeasesForUser.asset().vehicle().model().name().isEmpty()) {
                viewHolder.vehicleModelBooked.setText(getLeasesForUser.asset().vehicle().model().name());
            } else {
                viewHolder.vehicleModelBooked.setText(mContext.getString(R.string.not_available));
            }

            if(getLeasesForUser.asset() != null && getLeasesForUser.asset().vehicle() != null
                    && getLeasesForUser.asset().vehicle().vin() != null && !getLeasesForUser.asset().vehicle().vin().isEmpty()) {
                viewHolder.vinTextView.setText(getLeasesForUser.asset().vehicle().vin());
            } else {
                viewHolder.vinTextView.setText(mContext.getString(R.string.not_available));
            }

            //set trip date and time
            String dateFormat = "dd-MMM-yy";
            String timeFormat = "hh:mmaa";


            if(getLeasesForUser.bookingTime() != null && !getLeasesForUser.bookingTime().isEmpty()) {
                String bookingStartDate = DateTimeUtils.getInstance(mContext).generateDateFromFetchedDateTimeString(getLeasesForUser.bookingTime());
                DateTime tripStartDateTime = new DateTime(getLeasesForUser.bookingTime());
                Date tripStartDate = tripStartDateTime.toDate();

                String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
                String tripStartTime = new SimpleDateFormat(timeFormat).format(tripStartDate);
                viewHolder.bookingStartDateTextView.setText(tripDate + " | " + tripStartTime);
            } else {
                viewHolder.bookingStartDateTextView.setText(mContext.getString(R.string.not_available));
            }

            if(getLeasesForUser.endTime() != null && !getLeasesForUser.endTime().isEmpty()) {
                String bookingEndDate = DateTimeUtils.getInstance(mContext).generateDateFromFetchedDateTimeString(getLeasesForUser.endTime());
                DateTime tripEndDateTime = new DateTime(getLeasesForUser.endTime());
                Date tripEndDate = tripEndDateTime.toDate();

                String tripDate = new SimpleDateFormat(dateFormat).format(tripEndDate);
                String tripEndTime = new SimpleDateFormat(timeFormat).format(tripEndDate);
                viewHolder.bookingEndDateTextView.setText(tripDate + " | " + tripEndTime);
            } else {
                viewHolder.bookingEndDateTextView.setText(mContext.getString(R.string.not_available));
            }
        }
    }


    @Override
    public int getItemCount() {
        return mRentalHistoryList.size();
    }

}
