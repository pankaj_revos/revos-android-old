package com.revos.android.utilities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.jsonStructures.KycAssociation;
import com.revos.scripts.FetchAllKYCAssociationsQuery;
import com.revos.scripts.type.FileType;
import com.revos.scripts.type.KYCStatus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class KycAssociationPendingListAdapter extends RecyclerView.Adapter<KycAssociationPendingListAdapter.ViewHolder> {

    private List<KycAssociation> mKycAssociationList;
    private Context mContext;

    public KycAssociationPendingListAdapter(List<KycAssociation> kycAssociationList, Context context) {
        mKycAssociationList = kycAssociationList;
        mContext = context;
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {

        LinearLayout listItemLinearLayout, kycDocumentsDetailsLinearLayout;
        TextView providerNameTextView, selfieStatusTextView, dLSide1StatusTextView, dLSide2StatusTextView, kycAssociationStatusTextView;
        ImageView selfieStatusImageView, dLSide1StatusImageView, dLSide2StatusImageView;

        public ViewHolder(@NonNull View v) {
            super(v);

            listItemLinearLayout = v.findViewById(R.id.kyc_association_pending_list_item_linear_layout);
            kycDocumentsDetailsLinearLayout = v.findViewById(R.id.kyc_association_pending_list_item_document_details_linear_layout);

            providerNameTextView = v.findViewById(R.id.kyc_association_pending_list_item_provider_name_value_text_view);
            selfieStatusTextView = v.findViewById(R.id.kyc_association_pending_list_item_selfie_status_text_view);
            dLSide1StatusTextView = v.findViewById(R.id.kyc_association_pending_list_item_dl_side1_status_text_view);
            dLSide2StatusTextView = v.findViewById(R.id.kyc_association_pending_list_item_dl_side2_status_text_view);
            kycAssociationStatusTextView = v.findViewById(R.id.kyc_association_pending_list_item_status_value_text_view);

            selfieStatusImageView = v.findViewById(R.id.kyc_association_pending_list_item_selfie_status_image_view);
            dLSide1StatusImageView = v.findViewById(R.id.kyc_association_pending_list_item_dl_side1_status_image_view);
            dLSide2StatusImageView = v.findViewById(R.id.kyc_association_pending_list_item_dl_side2_status_image_view);

        }
    }

    @NonNull
    @Override
    public KycAssociationPendingListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kyc_association_pending_status_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  KycAssociationPendingListAdapter.ViewHolder viewHolder, int position) {

        KycAssociation kycAssociation = mKycAssociationList.get(position);

        String providerName = kycAssociation.getRentalProviderName();
        viewHolder.providerNameTextView.setText(providerName);

        viewHolder.kycAssociationStatusTextView.setText(kycAssociation.getKycAssociationStatus().toString());

        KYCStatus selfieStatus = null, dLSide1Status = null, dLSide2Status = null;

        List<FetchAllKYCAssociationsQuery.DocumentStatus> documentStatusList = mKycAssociationList.get(position).getDocumentList();

        for(int index = 0; index < documentStatusList.size() ; index++) {
            if (documentStatusList.get(index).document() != null && documentStatusList.get(index).document().type() != null) {

                if (documentStatusList.get(index).document().type().equals(FileType.KYC_PROFILE)) {

                    selfieStatus = documentStatusList.get(index).status();
                    if (selfieStatus != null && !selfieStatus.toString().isEmpty()) {
                        viewHolder.selfieStatusTextView.setText(selfieStatus.toString());
                    } else {
                        viewHolder.selfieStatusTextView.setText(R.string.not_available);
                    }

                    //set status image
                    if (documentStatusList.get(index).status() != null) {

                        viewHolder.selfieStatusImageView.setVisibility(View.VISIBLE);

                        if (documentStatusList.get(index).status().equals(KYCStatus.APPROVED)) {
                            viewHolder.selfieStatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_approved));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.REJECTED)) {
                            viewHolder.selfieStatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_rejected));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.REQUIRED)) {
                            viewHolder.selfieStatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_required));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.PENDING)) {
                            viewHolder.selfieStatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_pending));
                        }
                    }

                } else if (documentStatusList.get(index).document().type().equals(FileType.KYC_DL_SIDE_1)) {
                    dLSide1Status = documentStatusList.get(index).status();

                    if (dLSide1Status != null && !dLSide1Status.toString().isEmpty()) {
                        viewHolder.dLSide1StatusTextView.setText(dLSide1Status.toString());
                    } else {
                        viewHolder.dLSide1StatusTextView.setText(R.string.not_available);
                    }

                    //set status image
                    if (documentStatusList.get(index).status() != null) {

                        viewHolder.dLSide1StatusImageView.setVisibility(View.VISIBLE);

                        if (documentStatusList.get(index).status().equals(KYCStatus.APPROVED)) {
                            viewHolder.dLSide1StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_approved));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.REJECTED)) {
                            viewHolder.dLSide1StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_rejected));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.REQUIRED)) {
                            viewHolder.dLSide1StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_required));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.PENDING)) {
                            viewHolder.dLSide1StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_pending));
                        }
                    }

                } else if (documentStatusList.get(index).document().type().equals(FileType.KYC_DL_SIDE_2)) {
                    dLSide2Status = documentStatusList.get(index).status();

                    if (dLSide2Status != null && !dLSide2Status.toString().isEmpty()) {
                        viewHolder.dLSide2StatusTextView.setText(dLSide2Status.toString());
                    } else {
                        viewHolder.dLSide2StatusTextView.setText(R.string.not_available);
                    }

                    //set status image
                    if (documentStatusList.get(index).status() != null) {

                        viewHolder.dLSide2StatusImageView.setVisibility(View.VISIBLE);

                        if (documentStatusList.get(index).status().equals(KYCStatus.APPROVED)) {
                            viewHolder.dLSide2StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_approved));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.REJECTED)) {
                            viewHolder.dLSide2StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_rejected));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.REQUIRED)) {
                            viewHolder.dLSide2StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_required));
                        } else if (documentStatusList.get(index).status().equals(KYCStatus.PENDING)) {
                            viewHolder.dLSide2StatusImageView.setImageDrawable(mContext.getDrawable(R.drawable.ic_kyc_pending));
                        }
                    }
                }
            } else {
                viewHolder.selfieStatusTextView.setText(R.string.not_available);
                viewHolder.dLSide1StatusTextView.setText(R.string.not_available);
                viewHolder.dLSide2StatusTextView.setText(R.string.not_available);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mKycAssociationList.size();
    }
}
