package com.revos.android.utilities.general;

import android.content.Context;

import com.apollographql.apollo.ApolloClient;
import com.revos.android.R;
import com.revos.android.graphQL.GraphQLDateTimeCustomAdapter;
import com.revos.android.graphQL.GraphQLInterceptor;
import com.revos.scripts.type.CustomType;

import okhttp3.OkHttpClient;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_TOKEN_KEY;

public class ApolloClientUtils {

    //holds the single instance
    private static ApolloClientUtils mInstance = null;

    //private constructor
    private ApolloClientUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static ApolloClientUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new ApolloClientUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    public ApolloClient getApolloClient() {

        String userToken = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_TOKEN_KEY, null);

        if(userToken == null) {
            return null;
        }

        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .addInterceptor(new GraphQLInterceptor(userToken))
                .build();

        return ApolloClient.builder()
                .serverUrl(mContext.getString(R.string.server_link))
                .okHttpClient(okHttpClient)
                .addCustomTypeAdapter(CustomType.DATETIME, new GraphQLDateTimeCustomAdapter())
                .build();
    }
}