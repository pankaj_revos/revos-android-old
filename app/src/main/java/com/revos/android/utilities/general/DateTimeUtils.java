package com.revos.android.utilities.general;

import android.content.Context;

import com.revos.android.R;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {

    //holds the single instance
    private static DateTimeUtils mInstance = null;

    //private constructor
    private DateTimeUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static DateTimeUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new DateTimeUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    public String generateTimeAgoString(DateTime timeStamp) {

        DateTime now = new DateTime();
        Period period = new Period(timeStamp, now);

        if(period.getMonths() != 0) {
            return String.valueOf(period.getMonths()) + mContext.getString(R.string.months_ago);
        } else if(period.getWeeks() != 0) {
            return String.valueOf(period.getWeeks()) + mContext.getString(R.string.weeks_ago);
        } else if(period.getDays() != 0) {
            return String.valueOf(period.getDays()) + mContext.getString(R.string.days_ago);
        } else if(period.getHours() != 0) {
            return String.valueOf(period.getHours()) + mContext.getString(R.string.hours_ago);
        } else if(period.getMinutes() != 0) {
            return  String.valueOf(period.getMinutes()) + mContext.getString(R.string.minutes_ago);
        } else if(period.getSeconds() != 0) {
            return String.valueOf(period.getSeconds()) + mContext.getString(R.string.seconds_ago);
        } else {
            return "";
        }
    }

    public String generateTimeRemainingFromNowString(DateTime timeStamp) {

        DateTime currentTimeStamp = new DateTime();
        Period period = new Period(currentTimeStamp, timeStamp);
        String timeLeft = "";

        String days = "", months = "", years = "";
        if(period.getYears() <= 0 && period.getMonths() <= 0 && period.getDays() <= 0) {
            return timeLeft;
        }

        if(period.getYears() != 0) {
            years = String.valueOf(period.getYears());

            if(period.getMonths() != 0) {
                months = String.valueOf(period.getMonths());
                timeLeft = String.format("%s.%s %s", years, months, mContext.getString(R.string.years_remaining));
                return timeLeft;
            }

            if(period.getDays() != 0) {
                days = String.valueOf(period.getDays());
                timeLeft = years + " " + mContext.getString(R.string.years_remaining) + " " + days + " " + mContext.getString(R.string.days_remaining);
                return timeLeft;
            }

        } else if(period.getMonths() != 0) {
            months = String.valueOf(period.getMonths());

            if(period.getDays() != 0) {
                days = String.valueOf(period.getDays());
                timeLeft = months + " " + mContext.getString(R.string.months_remaining) + " " + days + " "  + mContext.getString(R.string.days_remaining);
                return timeLeft;
            }
        } else if(period.getDays() != 0) {
            days = String.valueOf(period.getDays());
            timeLeft = days  + " " + mContext.getString(R.string.days_remaining);
            return timeLeft;
        }
        return timeLeft;
    }

    public String getDateFromTimeStamp(String timestamp) {

        DateTime dateTime = new DateTime(timestamp);
        Date purchasedOnDate = dateTime.toDate();
        String dateFormat = "dd-MMM-yy";

        return new SimpleDateFormat(dateFormat).format(purchasedOnDate);
    }

    public String generateDateFromFetchedDateTimeString(String fetchedDate) {

        //convert fetched date to dd/MM/yy format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(fetchedDate);
        } catch (ParseException e) {
            //todo::handle exception
        }
        Format formatter = new SimpleDateFormat("dd/MM/yy");
        String dateInDDMMYYFormat = formatter.format(date);

        return dateInDDMMYYFormat;
    }
}
