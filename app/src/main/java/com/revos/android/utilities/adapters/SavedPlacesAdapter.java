package com.revos.android.utilities.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.constants.Constants;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.SavedPlace;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import static com.revos.android.constants.Constants.GOOGLE_MAPS_PACKAGE_NAME;
import static com.revos.android.constants.Constants.NAV_EVENT_SAVED_PLACE_LIST_UPDATED;

/**
 * Created by mohit on 17/4/17.
 */

public class SavedPlacesAdapter extends RecyclerView.Adapter<SavedPlacesAdapter.ViewHolder> {
    private ArrayList<SavedPlace> mSavedPlacesList;
    private Context mContext;

    public SavedPlacesAdapter(ArrayList<SavedPlace> savedPlacesList, Context context) {
        mSavedPlacesList = savedPlacesList;
        mContext = context;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout innerContentLayout;
        public TextView nameTextView, addressTextView, optionsTextView;

        public ViewHolder(View v) {
            super(v);
            innerContentLayout = (RelativeLayout) v.findViewById(R.id.saved_place_card_inner_content_relative_layout);
            nameTextView = (TextView) v.findViewById(R.id.saved_place_name_text_view);
            addressTextView = (TextView) v.findViewById(R.id.saved_place_address_text_view);
            optionsTextView = (TextView) v.findViewById(R.id.saved_place_options_text_view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.saved_places_list_view_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new SavedPlacesAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final SavedPlace savedPlace = mSavedPlacesList.get(position);

        holder.nameTextView.setText(savedPlace.getName());

        holder.addressTextView.setText(savedPlace.getAddress());

        holder.innerContentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startNavigation(savedPlace.getLatitude(), savedPlace.getLongitude());
                startNavigation(savedPlace.getName(), savedPlace.getAddress());
            }
        });

        holder.optionsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.optionsTextView);
                //inflating menu from xml resource
                popup.inflate(R.menu.saved_place_delete_options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete_option:
                                remove(position);
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSavedPlacesList.size();
    }

    public void add(int position, SavedPlace savedPlace) {
        if(mSavedPlacesList == null) {
            mSavedPlacesList = new ArrayList<>();
        }
        mSavedPlacesList.add(position, savedPlace);

        //Save the new list to prefs
        String savedPlacesListJSON = new Gson().toJson(mSavedPlacesList);
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(Constants.NAV_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.SAVED_PLACES_DATA_KEY, savedPlacesListJSON).apply();

        EventBus.getDefault().post(new EventBusMessage(NAV_EVENT_SAVED_PLACE_LIST_UPDATED));
        notifyItemInserted(position);
    }

    public void remove(int position) {
        if(mSavedPlacesList == null || position >= mSavedPlacesList.size()) {
            return;
        }

        mSavedPlacesList.remove(position);

        //Save the new list to prefs
        String placeListJSON = new Gson().toJson(mSavedPlacesList);
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(Constants.NAV_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.SAVED_PLACES_DATA_KEY, placeListJSON).apply();

        EventBus.getDefault().post(new EventBusMessage(NAV_EVENT_SAVED_PLACE_LIST_UPDATED));
        notifyItemRemoved(position);
    }

    public void reinitializeDataSet(ArrayList<SavedPlace> savedPlacesList) {
        mSavedPlacesList = savedPlacesList;
        notifyDataSetChanged();
    }

    private void startNavigation(String name, String address) {
        String queryString = name + ", " + address;
        String mapQueryString = "google.navigation:q=" + queryString + "&mode=d";

        Uri gmmIntentUri = Uri.parse(mapQueryString);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage(GOOGLE_MAPS_PACKAGE_NAME);
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(mapIntent);
    }
}
