package com.revos.android.utilities.ble;

import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.ble.data.Data;

public interface REVOSBleManagerCallbacks extends BleManagerCallbacks {
    void onImageServiceDataReceived(String text);

    void onOBDServiceDataReceived(Data data);

    void onHouseKeepingDataReceived(Data data);

    void onStmOtaServiceDataReceived(Data data);
}
