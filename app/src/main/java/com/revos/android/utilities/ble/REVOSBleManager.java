package com.revos.android.utilities.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Base64;

import com.google.gson.Gson;
import com.revos.android.activities.MainActivity;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.BatteryRegressionData;
import com.revos.android.jsonStructures.BatteryRegressionDataPoint;
import com.revos.android.jsonStructures.Device;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.general.Version;
import com.revos.android.utilities.networking.NetworkUtils;
import com.revos.android.utilities.security.EncryptionHelper;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.android.utilities.vehicleDataTransfer.VehicleControlHelper;
import com.revos.scripts.type.DeviceStatus;
import com.revos.scripts.type.ModelAccessType;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.callback.FailCallback;
import no.nordicsemi.android.ble.callback.SuccessCallback;
import no.nordicsemi.android.ble.data.Data;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.BATTERY_PREFS;
import static com.revos.android.constants.Constants.BATTERY_REGRESSION_DATA_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_BATTERY_REGRESSION_DATA_SYNC_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_CHANGE_PIN_REQUIRED;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_LOCKED;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_STATE_CHANGE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_SYNC_FAIL;
import static com.revos.android.constants.Constants.BT_EVENT_DEVICE_SYNC_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_IMG_CHAR_WRITE_FAIL;
import static com.revos.android.constants.Constants.BT_EVENT_IMG_CHAR_WRITE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_NO_PIN_FOUND;
import static com.revos.android.constants.Constants.BT_EVENT_OBD_CHAR_WRITE_FAIL;
import static com.revos.android.constants.Constants.BT_EVENT_OBD_CHAR_WRITE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_AUTHENTICATING;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_AUTHENTICATION_FAIL;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_AUTHENTICATION_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_CONNECTED;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_CONNECTING;
import static com.revos.android.constants.Constants.BT_EVENT_STATE_DISCONNECTED;
import static com.revos.android.constants.Constants.BT_EVENT_UNABLE_TO_READ_FIRMWARE_METADATA;
import static com.revos.android.constants.Constants.BT_EVENT_VEHICLE_BATTERY_VOLTAGE_CHANGE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_VEHICLE_CONSTANTS_CHANGE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_VEHICLE_IDS_CHANGE_SUCCESS;
import static com.revos.android.constants.Constants.BT_EVENT_WRONG_PIN;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.OBD_EVENT_BATTERY_ADC_OFFLINE_DATA;
import static com.revos.android.constants.Constants.OBD_EVENT_OFFLINE_DATA;
import static com.revos.android.constants.Constants.OBD_EVENT_LIVE_DATA;
import static com.revos.android.constants.Constants.STM_OTA_STATUS_MESSAGE;
import static com.revos.android.constants.Constants.VER_5_0_0;

public class REVOSBleManager extends BleManager<REVOSBleManagerCallbacks> implements REVOSBleManagerCallbacks{

    private Context mContext;

    private byte[] mSessionPassword;
    private byte[] mSentEncryptedBytes;

    private Device mCloudDevice;

    private String mDeviceMetadataString;

    private boolean mIsMetadataDecryptionSuccessful = false;

    int ACCESS_TYPE_KEY = 1;
    int ACCESS_TYPE_KEYLESS = 2;
    int ACCESS_TYPE_HYBRID = 3;

    private String mNordicFirmwareVersion, mStmInternalFirmwareVersion, mStmExternalFirmwareVersion;
    private DeviceStatus mDeviceStatus;
    private String mDeviceVin;
    private int mModelSpeedDivisor;
    private int mModelOdoDivisor;
    private int mMinBatteryVoltage;
    private int mMaxBatteryVoltage;
    private int mVehicleRange;
    private int mBatteryRegressionDataSetVersion;
    private ModelAccessType mModelAccessType;
    private int mControllerManufacturerId;
    private String mControllerFirmwareVersion;

    private String mDefaultAESKey = Base64.encodeToString("REVOS SHARED KEY".getBytes(), Base64.NO_WRAP | Base64.URL_SAFE);
    private String mAesKeyToBeUsed = null;

    private boolean mIsAuthenticationInProgress = false;
    private boolean mIsAuthenticationComplete = false;

    private boolean mIsBatteryRegressionDataSyncInProgress = false;
    private boolean mIsBatteryRegressionDataSyncComplete = false;

    private boolean mIsDeviceSyncInProgress = false;
    private boolean mIsDeviceSyncComplete = false;

    private boolean mIsVehicleIdsWriteInProgress = false;
    private boolean mIsVehicleIdsWriteComplete = false;

    private boolean mIsVehicleConstantsWriteInProgress = false;
    private boolean mIsVehicleConstantsWriteComplete = false;

    private boolean mIsBatteryVoltageWriteInProgress = false;
    private boolean mIsBatteryVoltageWriteComplete = false;

    private boolean mIsDeviceStateWriteInProgress = false;
    private boolean mIsDeviceStateWriteComplete = false;

    private boolean mIsLogSyncRequestInProgress = false;
    private boolean mIsLogSyncRequestComplete = false;

    private final int AUTHENTICATION_TIME_LIMIT = 30000;

    /**
     * STM ota related variables
     */

    private File mStmOtaFile;
    private byte[] mStmOtaFileBytes;

    private boolean mIsStmOtaInProgress = false;
    private boolean mIsStmOtaComplete = false;

    private boolean mIsStmInBootMode = false;
    private boolean mHasOtaInfoBeenSent = false;
    private boolean mIsStmExternalOta = false;
    private boolean mIsBufferFull = false;

    private int mCurrentBytePosition;
    private int mFileBytesSentInLastPacket = 0;

    private byte mInternalOtaMarkerByte = (byte)0xAA;
    private byte mExternalOtaMarkerByte = (byte)0xBB;

    /**
     *  STM OTA Service UUID
     */
    private final static UUID STM_OTA_SERVICE_UUID = UUID.fromString("653d3e34-885a-4324-8d35-a6eb1266e008");
    private final static UUID STM_OTA_WRITE_CHARACTERISTIC_UUID = UUID.fromString("653d3e35-885a-4324-8d35-a6eb1266e008");
    private final static UUID STM_OTA_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("653d3e36-885a-4324-8d35-a6eb1266e008");


    /** IMG Service UUID */
    private final static UUID IMG_SERVICE_UUID = UUID.fromString("653d1e34-885a-4324-8d35-a6eb1266e008");
    /** IMG characteristic UUID */
    private final static UUID UART_IMG_CHARACTERISTIC_UUID = UUID.fromString("653d1e35-885a-4324-8d35-a6eb1266e008");

    /** OBD Service UUID */
    private final static UUID OBD_SERVICE_UUID = UUID.fromString("653d2e34-885a-4324-8d35-a6eb1266e008");
    /** OBD characteristic UUID */
    private final static UUID UART_OBD_CHARACTERISTIC_UUID = UUID.fromString("653d2e35-885a-4324-8d35-a6eb1266e008");
    /** Firmware characteristic UUID */
    private final static UUID UART_FIRMWARE_CHARACTERISTIC_UUID = UUID.fromString("653d2e36-885a-4324-8d35-a6eb1266e008");
    /** House keeping characteristic UUID */
    private final static UUID UART_HOUSE_KEEPING_CHARACTERISTIC_UUID = UUID.fromString("653d2e37-885a-4324-8d35-a6eb1266e008");

    private BluetoothGattCharacteristic mImageCharacteristic, mOBDCharacteristic, mFirmwareCharacteristic, mHouseKeepingCharacteristic, mStmOtaWriteCharacteristic, mStmOtaNotifyCharacteristic;
    /**
     * A flag indicating whether Long Write can be used. It's set to false if the UART RX
     * characteristic has only PROPERTY_WRITE_NO_RESPONSE property and no PROPERTY_WRITE.
     * If you set it to false here, it will never use Long Write.
     *
     * TODO change this flag if you don't want to use Long Write even with Write Request.
     */
    private boolean mUseLongWrite = true;

    private SuccessCallback mDisconnectSuccessCallback = new SuccessCallback() {
        @Override
        public void onRequestCompleted(@NonNull BluetoothDevice device) {
            Timber.d("disconnect_successful");
        }
    };
    private FailCallback mDisconnectFailCallback = new FailCallback() {
        @Override
        public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
            Timber.d("disconnect_fail");
        }
    };

    /**
     * BluetoothGatt callbacks object.
     */
    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {
        @Override
        protected void initialize() {
            super.initialize();
            setNotificationCallback(mImageCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onImageServiceDataReceived(text);
                    });

            setNotificationCallback(mOBDCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onOBDServiceDataReceived(data);
                    });

            setNotificationCallback(mHouseKeepingCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onHouseKeepingDataReceived(data);
                    });

            setNotificationCallback(mStmOtaNotifyCharacteristic)
                    .with((device, data) -> {
                        final String text = data.getStringValue(0);
                        mCallbacks.onStmOtaServiceDataReceived(data);
                    });

            requestMtu(512).enqueue();

            enableNotifications(mOBDCharacteristic).enqueue();
            enableNotifications(mHouseKeepingCharacteristic).enqueue();
            enableNotifications(mStmOtaNotifyCharacteristic).enqueue();
        }

        @Override
        public boolean isRequiredServiceSupported(@NonNull final BluetoothGatt gatt) {
            final BluetoothGattService imgService = gatt.getService(IMG_SERVICE_UUID);
            if(imgService != null) {
                mImageCharacteristic = imgService.getCharacteristic(UART_IMG_CHARACTERISTIC_UUID);
            }

            final BluetoothGattService obdService = gatt.getService(OBD_SERVICE_UUID);
            if(obdService != null) {
                mOBDCharacteristic = obdService.getCharacteristic(UART_OBD_CHARACTERISTIC_UUID);
                mFirmwareCharacteristic = obdService.getCharacteristic(UART_FIRMWARE_CHARACTERISTIC_UUID);
                mHouseKeepingCharacteristic = obdService.getCharacteristic(UART_HOUSE_KEEPING_CHARACTERISTIC_UUID);
            }

            final BluetoothGattService stmOtaService = gatt.getService(STM_OTA_SERVICE_UUID);
            if(stmOtaService != null) {
                mStmOtaWriteCharacteristic = stmOtaService.getCharacteristic(STM_OTA_WRITE_CHARACTERISTIC_UUID);
                mStmOtaNotifyCharacteristic = stmOtaService.getCharacteristic(STM_OTA_NOTIFY_CHARACTERISTIC_UUID);
            }

            /*boolean writeRequest = false;
            boolean writeCommand = false;
            if(mImageCharacteristic != null) {
                final int rxProperties = mImageCharacteristic.getProperties();
                writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
                writeCommand = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) > 0;

                // Set the WRITE REQUEST type when the characteristic supports it.
                // This will allow to send long write (also if the characteristic support it).
                // In case there is no WRITE REQUEST property, this manager will divide texts
                // longer then MTU-3 bytes into up to MTU-3 bytes chunks.
                if(writeRequest)
                    mImageCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                else
                    mUseLongWrite = false;
            }*/

            return mImageCharacteristic != null && mOBDCharacteristic != null && mFirmwareCharacteristic != null && mHouseKeepingCharacteristic != null;// && (writeRequest || writeCommand);
        }

        @Override
        protected void onDeviceDisconnected() {
            mImageCharacteristic = null;
            mOBDCharacteristic = null;
            mUseLongWrite = true;
        }


    };

    private void initiateHandshake() {
        mIsAuthenticationInProgress = true;
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_STATE_AUTHENTICATING));
        mSessionPassword =  generateSessionPassword();
        byte[] handShakeBytes = generateHandshakeBytes(mSessionPassword);
        Timber.d("generating password");
        try {
            byte[] encryptedBytes = EncryptionHelper.encryptBytes(handShakeBytes, mAesKeyToBeUsed);
            mSentEncryptedBytes = encryptedBytes;
            byte[] arrayToBeSent = new byte[encryptedBytes.length + 1];
            arrayToBeSent[0] = (byte)0xDA;
            System.arraycopy(encryptedBytes, 0, arrayToBeSent, 1, encryptedBytes.length);
            //send this byte array to housekeeping service
            writeDataToHouseKeepingCharacteristic(arrayToBeSent);

        } catch (Exception e) {
            initiateAuthenticationFailedSequence();
        }
    }

    private byte[] generateSessionPassword() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] sessionPassword = new byte[16];
        secureRandom.nextBytes(sessionPassword);

        return sessionPassword;
    }

    private byte[] generateHandshakeBytes(byte[] sessionPassword) {
        //first obtain the timestamp byte array
        byte[] epochBytes = BleUtils.constructSetEpochTimeByteArray(System.currentTimeMillis());

        //calculate checksum
        int checkSum = 0;
        for(byte currentByte : mSessionPassword) {
            int unsignedInt = (int)currentByte & 0xFF;
            checkSum += unsignedInt;
        }

        for(byte currentByte : epochBytes) {
            int unsignedInt = (int)currentByte & 0xFF;
            checkSum += unsignedInt;
        }
        checkSum = checkSum & 0xFF;

        byte checkSumByte = (byte)checkSum;

        byte[] handshakeBytes = new byte[mSessionPassword.length + epochBytes.length + 1];

        System.arraycopy(mSessionPassword, 0, handshakeBytes, 0, mSessionPassword.length);
        System.arraycopy(epochBytes, 0, handshakeBytes, mSessionPassword.length, epochBytes.length);

        handshakeBytes[handshakeBytes.length - 1] = checkSumByte;

        return handshakeBytes;
    }

    public void disconnectFromDevice(SuccessCallback successCallback, FailCallback failCallback) {
        //disable notifications
        disableNotifications(mOBDCharacteristic).enqueue();
        //disconnect from device
        disconnect()
                .done(successCallback)
                .fail(failCallback)
                .enqueue();
    }

    public void writeDataToImgCharacteristic(byte[] data, SuccessCallback successCallback, FailCallback failCallback) {
        writeCharacteristic(mImageCharacteristic, data)
                .done(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        successCallback.onRequestCompleted(device);
                        Timber.d("img_write_successful");
                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_IMG_CHAR_WRITE_SUCCESS));
                    }
                })
                .fail(new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        failCallback.onRequestFailed(device, status);
                        Timber.d("img_write_fail");
                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_IMG_CHAR_WRITE_FAIL));

                    }
                })
                .enqueue();
    }

    private void writeDataToHouseKeepingCharacteristic(byte[] data) {
        writeCharacteristic(mHouseKeepingCharacteristic, data)
            .done(new SuccessCallback() {
                @Override
                public void onRequestCompleted(@NonNull BluetoothDevice device) {
                    Timber.d("house_keeping_write_successful");

                    if(mIsDeviceSyncInProgress) {
                        decideAndSendNextDeviceSyncCommand();
                    } else if(mIsBatteryRegressionDataSyncInProgress) {
                        decideAndSendNextBatteryDataSyncCommand();
                    }
                }
            })
            .fail(new FailCallback() {
                @Override
                public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                    Timber.d("house_keeping_write_fail");
                    if(mIsDeviceSyncInProgress) {
                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_SYNC_FAIL));
                        clearSyncVariables();
                    }
                }
            })
            .enqueue();
    }

    private void decideAndSendNextBatteryDataSyncCommand() {

        if(mIsBatteryRegressionDataSyncInProgress) {

            mIsBatteryRegressionDataSyncInProgress = false;
            mIsBatteryRegressionDataSyncComplete = true;

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    constructAndSendDeviceStateChangeCommand();
                }
            }, 500);

            if(mIsDeviceStateWriteInProgress) {
                mIsDeviceStateWriteInProgress = false;
                mIsDeviceStateWriteComplete = true;

                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_BATTERY_REGRESSION_DATA_SYNC_SUCCESS));
            }
        }
    }

    private void decideAndSendNextDeviceSyncCommand() {
        if(mIsDeviceSyncInProgress) {
            if (mIsVehicleIdsWriteInProgress) {
                mIsVehicleIdsWriteInProgress = false;
                mIsVehicleIdsWriteComplete = true;
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_VEHICLE_IDS_CHANGE_SUCCESS));
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        constructAndSendSetVehicleConstantsCommand();
                    }
                }, 500);

            } else if (mIsVehicleConstantsWriteInProgress) {
                mIsVehicleConstantsWriteInProgress = false;
                mIsVehicleConstantsWriteComplete = true;
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_VEHICLE_CONSTANTS_CHANGE_SUCCESS));
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        constructAndSendVehicleBatteryVoltageLimitCommand();
                    }
                }, 500);

            } else if(mIsBatteryVoltageWriteInProgress) {
                mIsBatteryVoltageWriteInProgress = false;
                mIsBatteryVoltageWriteComplete = true;
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_VEHICLE_BATTERY_VOLTAGE_CHANGE_SUCCESS));
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        constructAndSendDeviceStateChangeCommand();
                    }
                }, 500);

            } else if (mIsDeviceStateWriteInProgress) {
                mIsDeviceStateWriteInProgress = false;
                mIsDeviceStateWriteComplete = true;
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_STATE_CHANGE_SUCCESS));
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        extractAndStoreFirmwareMetadata();
                    }
                }, 500);

                //check if sync is successful
                if(mIsVehicleConstantsWriteComplete && mIsVehicleIdsWriteComplete && mIsBatteryVoltageWriteComplete && mIsDeviceStateWriteComplete) {
                    mIsDeviceSyncInProgress = false;
                    mIsDeviceSyncComplete = true;
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_SYNC_SUCCESS));
                }
            } else {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_SYNC_FAIL));
                clearSyncVariables();
            }
        }
    }

    public void writeDataToOBDCharacteristic(byte[] data) {
        if(mSessionPassword == null) {
            initiateAuthenticationFailedSequence();
            return;
        }

        VehicleControlHelper vehicleControlHelper = new VehicleControlHelper(mContext);
        if(data[0] == vehicleControlHelper.getStartLogSyncByte()) {
            if(mIsLogSyncRequestInProgress || mIsLogSyncRequestComplete) {
                return;
            }
            mIsLogSyncRequestInProgress = true;
            mIsLogSyncRequestComplete = false;
        }

        byte[] commandDataAndCheckSum = new byte[data.length + 1];
        System.arraycopy(data, 0, commandDataAndCheckSum, 0, data.length);

        //calculate checksum
        int checkSum = 0;
        for(int byteIndex = 0; byteIndex < data.length; ++byteIndex) {
            int unsignedInt = (int)data[byteIndex] & 0xFF;
            checkSum += unsignedInt;
        }
        checkSum = checkSum & 0xFF;

        commandDataAndCheckSum[commandDataAndCheckSum.length - 1] = (byte)checkSum;

        //encrypt the data
        try {
            byte[] encryptedCommand = EncryptionHelper.encryptBytes(commandDataAndCheckSum, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            writeCharacteristic(mOBDCharacteristic, encryptedCommand)
                    .done(new SuccessCallback() {
                        @Override
                        public void onRequestCompleted(@NonNull BluetoothDevice device) {
                            Timber.d("obd_write_successful");
                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_OBD_CHAR_WRITE_SUCCESS));
                            if(mIsLogSyncRequestInProgress) {
                                mIsLogSyncRequestInProgress = false;
                                mIsLogSyncRequestComplete = true;
                                Timber.d("log sync request complete");
                            }
                        }
                    })
                    .fail(new FailCallback() {
                        @Override
                        public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                            Timber.d("obd_write_fail");
                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_OBD_CHAR_WRITE_FAIL));
                        }
                    })
                    .enqueue();
        } catch (Exception e) {
            return;
        }

    }

    public void writeDataToIMGCharacteristic(byte[] data) {
        writeCharacteristic(mImageCharacteristic, data)
                .done(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        Timber.d("img_write_successful");
                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_IMG_CHAR_WRITE_SUCCESS));
                    }
                })
                .fail(new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        Timber.d("img_write_fail");
                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_IMG_CHAR_WRITE_FAIL));
                    }
                })
                .enqueue();
    }

    private void clearSessionRelatedVariables() {
        clearSyncVariables();

        mIsAuthenticationInProgress = false;
        mIsAuthenticationComplete = false;

        mIsLogSyncRequestInProgress = false;
        mIsLogSyncRequestComplete = false;

        mSessionPassword = null;
        mSentEncryptedBytes = null;

        mDeviceMetadataString = null;

        mIsStmInBootMode = false;
        mHasOtaInfoBeenSent = false;
        mIsStmExternalOta = false;
        mIsBufferFull = false;

        mIsStmOtaInProgress = false;
        mIsStmOtaComplete = false;

        Timber.d("clearing session related variables");
    }

    public REVOSBleManager(@NonNull final Context context) {
        super(context);
        mContext = context;
        setGattCallbacks(this);
    }

    public void connectToDevice(String deviceAddress, @Nullable String aesKey) {
        if(aesKey == null) {
            mAesKeyToBeUsed = mDefaultAESKey;
        } else {
            mAesKeyToBeUsed = aesKey;
        }

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!bluetoothAdapter.isEnabled()) {
            Timber.d("bt_adapter_disabled");
            return;
        }

        if(bluetoothAdapter.getState() != BluetoothAdapter.STATE_ON) {
            Timber.d("bt_adapter_not_on");
            return;
        }

        if(getConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            connect(bluetoothAdapter.getRemoteDevice(deviceAddress))
                    .retry(3, 100)
                    .done(new SuccessCallback() {
                        @Override
                        public void onRequestCompleted(@NonNull BluetoothDevice device) {
                            Timber.d("connection_successful");
                        }
                    })
                    .fail(new FailCallback() {
                        @Override
                        public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                            Timber.d("connection_unsuccessful");
                        }
                    })
                    .enqueue();
        }
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @Override
    public void onDeviceConnecting(@NonNull BluetoothDevice device) {
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_STATE_CONNECTING));
        Timber.d("device_connecting");
    }

    @Override
    public void onDeviceConnected(@NonNull BluetoothDevice device) {
        Timber.d("device_connected");

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                initiateHandshake();
            }
        }, 1000);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(mIsAuthenticationInProgress) {
                    initiateAuthenticationFailedSequence();
                } else if(!mIsAuthenticationComplete) {
                    initiateAuthenticationFailedSequence();
                }
            }
        }, AUTHENTICATION_TIME_LIMIT);
    }

    @Override
    public void onDeviceDisconnecting(@NonNull BluetoothDevice device) {
        clearSessionRelatedVariables();
        Timber.d("device_disconnecting");
    }

    @Override
    public void onDeviceDisconnected(@NonNull BluetoothDevice device) {
        clearSessionRelatedVariables();
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_STATE_DISCONNECTED));
        Timber.d("device_disconnected");
    }

    @Override
    public void onLinkLossOccurred(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onServicesDiscovered(@NonNull BluetoothDevice device, boolean optionalServicesFound) {

    }

    @Override
    public void onDeviceReady(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBondingRequired(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBonded(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBondingFailed(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onError(@NonNull BluetoothDevice device, @NonNull String message, int errorCode) {

    }

    @Override
    public void onDeviceNotSupported(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onImageServiceDataReceived(String text) {
        Timber.d("image service data received : %s", text);
    }



    @Override
    public void onOBDServiceDataReceived(Data data) {

        String dataString = data.getStringValue(0);

        Timber.e("onOBDServiceDataReceived");

        //check if we have cloud device or not
        String devicePin = mContext.getSharedPreferences(DEVICE_PREFS, Context.MODE_PRIVATE).getString(DEVICE_PIN_KEY, null);

        mCloudDevice = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();
        if(mCloudDevice == null) {
            Timber.e("onOBDServiceDataReceived mCloudDevice is null");
            return;
        }

        //check if this device is activated
        if (mCloudDevice.getStatus() != DeviceStatus.ACTIVATED) {
            Timber.e("onOBDServiceDataReceived mCloudDevice is not ACTIVATED");
            return;
        }

        //check if we have the device pin or not
        if(mCloudDevice.isPinResetRequired()){
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_CHANGE_PIN_REQUIRED + GEN_DELIMITER + mCloudDevice.getVehicle().getVin() + GEN_DELIMITER + mCloudDevice.getVehicle().getBuyerPhoneNo()));
            return;
        } else if(devicePin == null) {
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_NO_PIN_FOUND + GEN_DELIMITER + mNordicFirmwareVersion));
            return;
        }


        //check if BluetoothEnterPinActivity is active, if yes, then return
        if(MainActivity.mBluetoothEnterPinActivityLaunched || MainActivity.mSetPinActivityLaunched || MainActivity.mChangePinActivityLaunched) {
            return;
        }

        if(dataString == null || (dataString.length() >= "LOCKED".length() && dataString.substring(0, "LOCKED".length()).contains("LOCKED"))) {
            Timber.e("onOBDServiceDataReceived Sending " + BT_EVENT_DEVICE_LOCKED);
            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_LOCKED + GEN_DELIMITER + dataString));
        } else if(mIsAuthenticationComplete && NetworkUtils.getInstance(mContext).checkAndStartVehicleMetadataDownloadIfRequired(mNordicFirmwareVersion)) {

            //we need to now check if pin is correct or not
            MetadataDecryptionHelper metadataDecryptionHelper = MetadataDecryptionHelper.getInstance(mContext);

            if(!mIsMetadataDecryptionSuccessful) {
                if(metadataDecryptionHelper.decryptControllerReadMetadata() == null ||
                        metadataDecryptionHelper.decryptControllerLogMetadata() == null ||
                        metadataDecryptionHelper.decryptControllerWriteMetadata() == null ||
                        metadataDecryptionHelper.decryptVehicleControlMetadata() == null) {
                    EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN + GEN_DELIMITER + mNordicFirmwareVersion));
                    return;
                } else {
                    mIsMetadataDecryptionSuccessful = true;
                }
            }
            Timber.e("onOBDServiceDataReceived Sending " + OBD_EVENT_LIVE_DATA);
            EventBus.getDefault().post(new EventBusMessage(OBD_EVENT_LIVE_DATA + GEN_DELIMITER + BleUtils.ByteArraytoHex(data.getValue())));
        }
    }

    @Override
    public void onHouseKeepingDataReceived(Data data) {
        byte[] dataBytes = data.getValue();

        if(dataBytes == null || dataBytes.length < 1) {
            initiateAuthenticationFailedSequence();
            return;
        }

        //check the first byte
        if(dataBytes[0] == (byte)0x05) {
            initiateAuthenticationFailedSequence();
        } else if(dataBytes.length > 1 && dataBytes[0] == (byte)0xDA && mIsAuthenticationInProgress) {
            //this is a response packet
            byte[] responseBytes = new byte[dataBytes.length - 1];
            System.arraycopy(dataBytes, 1, responseBytes, 0, responseBytes.length);

            //first check if it is not equal to what we sent, if it is not equal then disconnect
            if(responseBytes.length != mSentEncryptedBytes.length) {
                //length mismatch, disconnect
                initiateAuthenticationFailedSequence();
            } else {
                //check if the the arrays are equal in content, if yes, then disconnect, we expect a different encrypted string as IV should change
                if(Arrays.equals(responseBytes, mSentEncryptedBytes)) {
                    initiateAuthenticationFailedSequence();
                } else {
                    //the arrays are equal in length but they differ in content, now check if decrypted string is the same or not
                    try {
                        byte[] originalDecryptedString = EncryptionHelper.decryptBytes(mSentEncryptedBytes, mAesKeyToBeUsed);
                        byte[] receivedDecryptedString = EncryptionHelper.decryptBytes(responseBytes, mAesKeyToBeUsed);

                        //check if these two arrays are equal if not, then disconnect
                        if(!Arrays.equals(originalDecryptedString, receivedDecryptedString)) {
                            initiateAuthenticationFailedSequence();
                        } else {
                            //yes both strings are equal, authentication complete
                            mIsAuthenticationInProgress = false;
                            mIsAuthenticationComplete = true;

                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_STATE_AUTHENTICATION_SUCCESS));
                            //now read and store firmware metadata string
                            readCharacteristic(mFirmwareCharacteristic).with(new DataReceivedCallback() {
                                @Override
                                public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
                                    try {
                                        mDeviceMetadataString = decryptBytesToString(data.getValue());
                                        if(mDeviceMetadataString == null) {
                                            EventBus.getDefault().post(new EventBusMessage(BT_EVENT_UNABLE_TO_READ_FIRMWARE_METADATA));
                                            disconnectFromDevice(mDisconnectSuccessCallback, mDisconnectFailCallback);
                                        } else {
                                            if(extractAndStoreFirmwareMetadata()) {
                                                if(mNordicFirmwareVersion != null && !mNordicFirmwareVersion.isEmpty()) {
                                                    NetworkUtils.getInstance(mContext).checkAndStartVehicleMetadataDownloadIfRequired(mNordicFirmwareVersion);
                                                }
                                                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_STATE_CONNECTED));
                                            } else {
                                                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_UNABLE_TO_READ_FIRMWARE_METADATA));
                                                disconnectFromDevice(mDisconnectSuccessCallback, mDisconnectFailCallback);
                                            }
                                        }
                                    } catch (Exception e) {
                                        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_UNABLE_TO_READ_FIRMWARE_METADATA));
                                        disconnectFromDevice(mDisconnectSuccessCallback, mDisconnectFailCallback);
                                    }
                                }
                            }).enqueue();
                        }

                    } catch (Exception e) {
                        initiateAuthenticationFailedSequence();
                    }
                }
            }

        } else if(dataBytes.length > 1 && dataBytes[0] == (byte)0xDB){
            String dataString = data.getStringValue(0);

            Timber.d("housekeeping service offline log data received : " + dataString);

            //this is a log packet
            if(mIsAuthenticationComplete && NetworkUtils.getInstance(mContext).checkAndStartVehicleMetadataDownloadIfRequired(mNordicFirmwareVersion)) {
                EventBus.getDefault().post(new EventBusMessage(OBD_EVENT_OFFLINE_DATA + GEN_DELIMITER + BleUtils.ByteArraytoHex(data.getValue())));
            }
        } else if(dataBytes.length > 1 && dataBytes[0] == (byte)0xDC) {
            String dataString = data.getStringValue(0);

            Timber.d("housekeeping service battery adc offline log data received : " + dataString);

            //this is a log packet
            if(mIsAuthenticationComplete && NetworkUtils.getInstance(mContext).checkAndStartVehicleMetadataDownloadIfRequired(mNordicFirmwareVersion)) {
                EventBus.getDefault().post(new EventBusMessage(OBD_EVENT_BATTERY_ADC_OFFLINE_DATA + GEN_DELIMITER + BleUtils.ByteArraytoHex(data.getValue())));
            }
        } else {
            initiateAuthenticationFailedSequence();
        }
    }

    @Override
    public void onStmOtaServiceDataReceived(Data data) {
        String dataString = data.getStringValue(0);

        byte[] dataBytes = data.getValue();

        if(dataBytes == null || dataBytes.length < 1) {
            return;
        }

        Timber.d("STM OTA byte received data : " + BleUtils.ByteArraytoHex(dataBytes));

        EventBus.getDefault().post(new EventBusMessage(STM_OTA_STATUS_MESSAGE + GEN_DELIMITER + "STM OTA byte received data : " + BleUtils.ByteArraytoHex(dataBytes)));


        if(dataBytes[0] == (byte)0xB0) {
            //file details have been successfully sent
            //now let's start sending the file part by part
            mCurrentBytePosition = 0;
            mIsStmOtaInProgress = true;
            mIsStmOtaComplete = false;


            sendStmOtaInformation();
        } else if(dataBytes[0] == (byte)0xB1) {
            mHasOtaInfoBeenSent = true;

            sendNextFilePartInBytes();
        } else if(dataBytes[0] == (byte)0xB2) {
            mCurrentBytePosition += mFileBytesSentInLastPacket;

            Timber.d("OTA Progress : " + mCurrentBytePosition * 100.0f / mStmOtaFileBytes.length + ", (" + mCurrentBytePosition + "/" + mStmOtaFileBytes.length + ")");

            EventBus.getDefault().post(new EventBusMessage(STM_OTA_STATUS_MESSAGE + GEN_DELIMITER + "OTA Progress : " + mCurrentBytePosition * 100.0f / mStmOtaFileBytes.length + ", (" + mCurrentBytePosition + "/" + mStmOtaFileBytes.length + ")"));

            float percentageCompleted = mCurrentBytePosition * 100.0f / mStmOtaFileBytes.length;

            if(mCurrentBytePosition < mStmOtaFileBytes.length) {
                sendNextFilePartInBytes();
            } else {
                sendStartStmOtaBytes();
                //mOtaStatusListener.onOtaComplete(true);
            }
        } else if(dataBytes[0] == (byte)0xB5) {

        } else if(dataBytes[0] == (byte)0xB6) {
            if(!mIsStmOtaInProgress) {
                return;
            }
            sendNextFilePartInBytes();
        }
    }

    private void sendStartStmOtaBytes() {
        byte[] byteArrayToBeSent = new byte[2];

        byteArrayToBeSent[0] = (byte)0xA7;
        if(mIsStmExternalOta) {
            byteArrayToBeSent[1] = mExternalOtaMarkerByte;
        } else {
            byteArrayToBeSent[1] = mInternalOtaMarkerByte;
        }
        Timber.d("Sending start ota bytes :  " + BleUtils.ByteArraytoHex(byteArrayToBeSent));

        EventBus.getDefault().post(new EventBusMessage(STM_OTA_STATUS_MESSAGE + GEN_DELIMITER + "Sending start ota bytes :  " + BleUtils.ByteArraytoHex(byteArrayToBeSent)));

        writeDataToStmOtaCharacteristic(byteArrayToBeSent);


        mIsStmOtaInProgress = false;
        mIsStmOtaComplete = true;
    }

    private void sendNextFilePartInBytes() {
        if(mFileBytesSentInLastPacket != 0 && mFileBytesSentInLastPacket < (512 - 9)) {
            //this means all 512 bytes were not utilized, hence it must have been the last packet
            sendStartStmOtaBytes();
            return;
        } else {
            byte[] completeByteArray;

            int numOfFileBytesToBeSent = Math.min((512 - 9), mStmOtaFileBytes.length - mCurrentBytePosition);

            if(numOfFileBytesToBeSent == 0) {
                sendStartStmOtaBytes();
                return;
            }

            mFileBytesSentInLastPacket = numOfFileBytesToBeSent;

            Timber.d("OTA file bytes sent : " + numOfFileBytesToBeSent);

            EventBus.getDefault().post(new EventBusMessage(STM_OTA_STATUS_MESSAGE + GEN_DELIMITER + "OTA file bytes sent : " + numOfFileBytesToBeSent));


            completeByteArray = new byte[1 + 1 + 4 + numOfFileBytesToBeSent]; //marker byte, packet type byte(internal/external), address bytes, check sum;

            //set the marker byte
            completeByteArray[0] = (byte)0xA6;

            if(mIsStmExternalOta) {
                completeByteArray[1] = mExternalOtaMarkerByte;
            } else {
                completeByteArray[1] = mInternalOtaMarkerByte;
            }

            //set the length including fileBytesToBeSent plus checksum
            byte[] addressPositionByteArray = BleUtils.hexStringToByteArray(StringUtils.leftPad(Integer.toHexString(mCurrentBytePosition), 8, '0'));
            System.arraycopy(addressPositionByteArray, 0, completeByteArray, 2, addressPositionByteArray.length);

            //set the file bytes array
            System.arraycopy(mStmOtaFileBytes, mCurrentBytePosition, completeByteArray, 6, numOfFileBytesToBeSent);

            writeDataToStmOtaCharacteristic(completeByteArray);
        }
    }


    public void startStmOta(String filePath, boolean isExternalOta) {
        if(!mIsAuthenticationComplete) {
            return;
        }

        //clear previous related variables
        mIsStmInBootMode = false;
        mHasOtaInfoBeenSent = false;
        mIsStmExternalOta = false;
        mIsBufferFull = false;

        mIsStmOtaInProgress = false;
        mIsStmOtaComplete = false;


        //set values again
        mIsStmExternalOta = isExternalOta;

        mStmOtaFile = new File(filePath);
        int size = (int) mStmOtaFile.length();
        mStmOtaFileBytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(mStmOtaFile));
            buf.read(mStmOtaFileBytes, 0, mStmOtaFileBytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            Timber.d("File not found");
            return;
        } catch (IOException e) {
            Timber.d("IO Exception");
            return;
        }

        sendStmToOtaMode();
    }

    private void sendStmOtaInformation() {
        byte[] byteArrayToBeSent = new byte[6];
        byteArrayToBeSent[0] = (byte)0xA5;
        if(mIsStmExternalOta) {
            byteArrayToBeSent[1] = mExternalOtaMarkerByte;
        } else {
            byteArrayToBeSent[1] = mInternalOtaMarkerByte;
        }

        int lengthInBytes = (int)mStmOtaFile.length();

        String hexStr = Long.toHexString(lengthInBytes);
        hexStr = StringUtils.leftPad(hexStr, 8, '0');

        byte[] fileLengthArray = BleUtils.hexStringToByteArray(hexStr);

        System.arraycopy(fileLengthArray, 0, byteArrayToBeSent, 2, 4);

        Timber.d("sending ota information :" + BleUtils.ByteArraytoHex(byteArrayToBeSent));
        writeDataToStmOtaCharacteristic(byteArrayToBeSent);
    }

    private void sendStmToOtaMode() {
        byte[] byteArrayToBeSent = new byte[] {(byte)0xA4, (byte)0xDE, (byte)0xAD, (byte)0xBE, (byte)0xAF};

        writeDataToStmOtaCharacteristic(byteArrayToBeSent);
    }

    private void writeDataToStmOtaCharacteristic(byte[] data) {
        writeCharacteristic(mStmOtaWriteCharacteristic, data)
                .done(new SuccessCallback() {
                    @Override
                    public void onRequestCompleted(@NonNull BluetoothDevice device) {
                        Timber.d("stm_ota_write_success");
                    }
                })
                .fail(new FailCallback() {
                    @Override
                    public void onRequestFailed(@NonNull BluetoothDevice device, int status) {
                        Timber.d("stm_ota_write_fail");
                    }
                })
                .enqueue();
    }

    private boolean extractAndStoreFirmwareMetadata() {

        if(mDeviceMetadataString == null) {
            return false;
        }

        String[] tokens = mDeviceMetadataString.split(",");

        //extract device firmware string
        try {
            mNordicFirmwareVersion = tokens[0];
        } catch (Exception e) {
            return false;
        }

        int OFFSET = 0;
        if(new Version(mNordicFirmwareVersion).compareTo(new Version(VER_5_0_0)) >= 0) {
            OFFSET = 2;

            try {
                mStmInternalFirmwareVersion = tokens[1];
                mStmExternalFirmwareVersion = tokens[2];
            } catch (Exception e) {
                return false;
            }
        }

        //extract device status
        try {
            int state = Integer.parseInt(tokens[OFFSET + 1]);

            if(state == 0) {
                mDeviceStatus = DeviceStatus.$UNKNOWN;
            } else if(state == 1) {
                mDeviceStatus = DeviceStatus.INITIALIZED;
            } else if(state == 2) {
                mDeviceStatus = DeviceStatus.ACTIVATED;
            } else if(state == 3) {
                mDeviceStatus = DeviceStatus.DISABLED;
            }
        } catch (Exception e) {
            return false;
        }

        //extract vin
        try {
            mDeviceVin = tokens[OFFSET + 2];
        } catch (Exception e) {
            return false;
        }

        //extract speed divisor
        try {
            byte speedDivisorValue = (byte)tokens[OFFSET + 3].charAt(0);
            mModelSpeedDivisor = speedDivisorValue & 0xFF;
        } catch (Exception e) {
            return false;
        }

        //extract odo divisor
        try {
            byte odoDivisorValue = (byte)tokens[OFFSET + 4].charAt(0);
            mModelOdoDivisor = odoDivisorValue & 0xFF;
        } catch (Exception e) {
            return false;
        }

        //extract model access type
        try {
            byte accessType = (byte)tokens[OFFSET + 5].charAt(0);
            if(accessType == ACCESS_TYPE_KEY) {
                mModelAccessType = ModelAccessType.KEY;
            } else if(accessType == ACCESS_TYPE_KEYLESS) {
                mModelAccessType = ModelAccessType.KEYLESS;
            } else if(accessType == ACCESS_TYPE_HYBRID) {
                mModelAccessType = ModelAccessType.HYBRID;
            }
        } catch (Exception e) {
            return false;
        }

        //extract controller manufacturer id
        try {
            byte[] byteArray = new byte[2];
            byte lsb = (byte)tokens[OFFSET + 6].charAt(0);
            byte msb = (byte)tokens[OFFSET + 7].charAt(0);

            byteArray[1] = lsb;
            byteArray[0] = msb;

            String hexString = BleUtils.byteArrayToHexString(byteArray);

            mControllerManufacturerId = Integer.parseInt(hexString, 16);
        } catch (Exception e) {
            return false;
        }

        //extract controller firmware version
        try {
            int controllerFirmwareVersion = (int)tokens[OFFSET + 8].charAt(0);
            mControllerFirmwareVersion = String.valueOf(controllerFirmwareVersion);
        } catch (Exception e) {
            return false;
        }

        //non essential metadata extraction

        //extracting battery voltage limit
        try {
            byte[] minBatteryVoltage = new byte[2];
            byte[] maxBatteryVoltage = new byte[2];

            byte batteryMinVoltageMSB = (byte)tokens[OFFSET + 9].charAt(0);
            byte batteryMinVoltageLSB = (byte)tokens[OFFSET + 10].charAt(0);

            byte batteryMaxVoltageMSB = (byte)tokens[OFFSET + 11].charAt(0);
            byte batteryMaxVoltageLSB = (byte)tokens[OFFSET + 12].charAt(0);

            minBatteryVoltage[1] = batteryMinVoltageLSB;
            minBatteryVoltage[0] = batteryMinVoltageMSB;

            maxBatteryVoltage[1] = batteryMaxVoltageLSB;
            maxBatteryVoltage[0] = batteryMaxVoltageMSB;

            String minVoltageHexString = BleUtils.byteArrayToHexString(minBatteryVoltage);
            String maxVoltageHexString = BleUtils.byteArrayToHexString(maxBatteryVoltage);

            mMinBatteryVoltage = Integer.parseInt(minVoltageHexString, 16);
            mMaxBatteryVoltage = Integer.parseInt(maxVoltageHexString, 16);

        } catch (Exception e) {
            //no need for sending false here as this information is not critical and is updated on every successful connection
        }

        //extract vehicle range
        try{
            byte[] vehicleRange = new byte[2];

            byte vehicleRangeMSB = (byte)tokens[OFFSET + 13].charAt(0);
            byte vehicleRangeLSB = (byte)tokens[OFFSET + 14].charAt(0);

            vehicleRange[1] = vehicleRangeLSB;
            vehicleRange[0] = vehicleRangeMSB;

            String vehicleRangeHexStr = BleUtils.byteArrayToHexString(vehicleRange);
            mVehicleRange = Integer.parseInt(vehicleRangeHexStr, 16);
        } catch (Exception e) {
            mVehicleRange = 0;
        }

        //extracting battery regression data id
        try {
            byte batteryRegressionDataSetId = (byte)tokens[OFFSET + 15].charAt(0);
            mBatteryRegressionDataSetVersion = batteryRegressionDataSetId & 0xFF;
        } catch (Exception e) {
            //set this as zero, if this is zero, then we have to sync battery regression data
            mBatteryRegressionDataSetVersion = 0;
        }

        return true;
    }

    public String getDeviceFirmwareVersion() {
        return mNordicFirmwareVersion;
    }

    public DeviceStatus getDeviceStatus() {
        return mDeviceStatus;
    }

    public String getDeviceVin() {
        return mDeviceVin;
    }

    public int getModelSpeedDivisor() {
        return mModelSpeedDivisor;
    }

    public int getModelOdoDivisor() {
        return mModelOdoDivisor;
    }

    public ModelAccessType getModelAccessType() {
        return mModelAccessType;
    }

    public int getControllerManufacturerId() {
        return mControllerManufacturerId;
    }

    public String getControllerFirmwareVersion() {
        return mControllerFirmwareVersion;
    }

    public int getBatteryRegressionDataSetVersion() {
        return mBatteryRegressionDataSetVersion;
    }

    public int getVehicleRange() {
        return mVehicleRange;
    }

    public String getDeviceMetadataString() {
        return mDeviceMetadataString;
    }

    public String decryptBytesToString(byte[] dataBytes) {
        String str = null;

        try {
            byte[] decryptedBytes = EncryptionHelper.decryptBytes(dataBytes, mAesKeyToBeUsed);
            str = new String(decryptedBytes);
        } catch (Exception e) {

        }

        return str;
    }

    public int getBatteryMinVoltage() {
        return mMinBatteryVoltage;
    }

    public int getBatteryMaxVoltage() {
        return mMaxBatteryVoltage;
    }

    public void syncDeviceConstants() {
        mCloudDevice = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();
        if(mCloudDevice == null) {
            return;
        }

        if(mIsDeviceSyncInProgress) {
            return;
        }

        clearSyncVariables();

        mIsDeviceSyncInProgress = true;
        mIsDeviceSyncComplete = false;

        constructAndSendSetVehicleIdsCommand();

    }

    public void syncBatteryRegressionData() {
        String regressionDataStr = mContext.getSharedPreferences(BATTERY_PREFS, MODE_PRIVATE).getString(BATTERY_REGRESSION_DATA_KEY, null);

        if(regressionDataStr == null) {
            return;
        }

        BatteryRegressionData batteryRegressionData = new Gson().fromJson(regressionDataStr, BatteryRegressionData.class);

        mIsBatteryRegressionDataSyncInProgress = true;
        mIsBatteryRegressionDataSyncComplete = false;

        constructAndSendBatteryRegressionDataCommand(batteryRegressionData);
    }

    private void constructAndSendBatteryRegressionDataCommand(BatteryRegressionData batteryRegressionData) {
        try {

            byte[] byteArrayToBeEncrypted = new byte[1  + 1
                                                        + batteryRegressionData.getDataPoints().size() * 5
                                                        + 1];
            byteArrayToBeEncrypted[0] = (byte)(batteryRegressionData.getVersion() & 0xFF);
            byteArrayToBeEncrypted[1] = (byte)(batteryRegressionData.getDataPoints().size());

            //iterate over the array and keep adding to the byte array
            for(int index = 0; index < batteryRegressionData.getDataPoints().size(); ++index) {
                BatteryRegressionDataPoint batteryRegressionDataPoint = batteryRegressionData.getDataPoints().get(index);
                long roundedVoltage = Math.round(batteryRegressionDataPoint.getVoltage() * 100);
                long roundedSoc = Math.round(batteryRegressionDataPoint.getSoc() * 10);
                long roundedDte = Math.round(batteryRegressionDataPoint.getDte());

                String voltageHexStr = Long.toHexString(roundedVoltage);
                String socHexStr = Long.toHexString(roundedSoc);
                String dteHexStr = Long.toHexString(roundedDte);

                voltageHexStr = StringUtils.leftPad(voltageHexStr, 4, '0');
                socHexStr = StringUtils.leftPad(socHexStr, 4, '0');
                dteHexStr = StringUtils.leftPad(dteHexStr, 2, '0');

                byte[] voltageByteArray = BleUtils.hexStringToByteArray(voltageHexStr);
                byte[] socByteArray = BleUtils.hexStringToByteArray(socHexStr);
                byte[] dteByteArray = BleUtils.hexStringToByteArray(dteHexStr);

                System.arraycopy(voltageByteArray, 0, byteArrayToBeEncrypted, 2 + index * 5, voltageByteArray.length);
                System.arraycopy(socByteArray, 0, byteArrayToBeEncrypted, 2 + index * 5 + voltageByteArray.length, socByteArray.length);
                System.arraycopy(dteByteArray, 0, byteArrayToBeEncrypted, 2 + index * 5 + voltageByteArray.length + socByteArray.length, dteByteArray.length);
            }

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, byteArrayToBeEncrypted.length - 1);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1];
            commandArrayToBeSent[0] = (byte)(0xD6);
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 1, encryptedBytes.length);

            Timber.d("Session password : " + BleUtils.byteArrayToHexString(mSessionPassword));
            Timber.d("String sent : " + BleUtils.byteArrayToHexString(byteArrayToBeEncrypted));
            Timber.d("Checksum : " + checkSum);
            Timber.d("Encrypted string : " + BleUtils.byteArrayToHexString(commandArrayToBeSent));

            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);

        } catch (Exception e) {
            //optional data point for now, so exception should not lead to a retry
            mIsBatteryRegressionDataSyncInProgress = false;
        }
    }

    private void clearSyncVariables() {
        clearDeviceSyncVariables();
        clearBatteryRegressionDataSyncVariables();
    }

    private void clearBatteryRegressionDataSyncVariables() {
        mIsBatteryRegressionDataSyncInProgress = false;
        mIsBatteryRegressionDataSyncComplete = false;
    }

    private void clearDeviceSyncVariables() {
        mIsDeviceSyncInProgress = false;
        mIsDeviceSyncComplete = false;

        mIsVehicleIdsWriteInProgress = false;
        mIsVehicleIdsWriteComplete = false;

        mIsVehicleConstantsWriteInProgress = false;
        mIsVehicleConstantsWriteComplete = false;

        mIsDeviceStateWriteInProgress = false;
        mIsDeviceSyncComplete = false;

    }

    private void constructAndSendSetVehicleIdsCommand() {
        mIsVehicleIdsWriteInProgress = true;

        try {
            byte[] vinByteArray = mCloudDevice.getVehicle().getVin().getBytes();
            byte[] deviceIdByteArray = mCloudDevice.getDeviceId().getBytes();

            //vin length, vin, deviceId length, deviceId, checksum
            byte[] byteArrayToBeEncrypted = new byte[1 + vinByteArray.length + 1 + deviceIdByteArray.length + 1];

            byteArrayToBeEncrypted[0] = (byte)vinByteArray.length;
            byteArrayToBeEncrypted[vinByteArray.length + 1] = (byte)deviceIdByteArray.length;

            System.arraycopy(vinByteArray, 0, byteArrayToBeEncrypted, 1, vinByteArray.length);
            System.arraycopy(deviceIdByteArray, 0, byteArrayToBeEncrypted, vinByteArray.length + 2, deviceIdByteArray.length);

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, byteArrayToBeEncrypted.length - 1);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1];
            commandArrayToBeSent[0] = (byte)(0xDD);
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 1, encryptedBytes.length);

            Timber.d("Session password : " + BleUtils.byteArrayToHexString(mSessionPassword));
            Timber.d("String sent : " + BleUtils.byteArrayToHexString(byteArrayToBeEncrypted));
            Timber.d("Checksum : " + checkSum);
            Timber.d("Encrypted string : " + BleUtils.byteArrayToHexString(commandArrayToBeSent));

            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);
        } catch (Exception e) {
            initiateDeviceSyncFailedSequence();
        }
    }

    private void constructAndSendSetVehicleConstantsCommand() {
        mIsVehicleConstantsWriteInProgress = true;

        try {
            //speed divisor, odo divisor, checksum
            byte[] byteArrayToBeEncrypted = new byte[1 + 1 + 1 + 1];
            byteArrayToBeEncrypted[0] = (byte)mCloudDevice.getVehicle().getModel().getSpeedDivisor();
            byteArrayToBeEncrypted[1] = (byte)mCloudDevice.getVehicle().getModel().getOdoDivisor();
            int accessType = mCloudDevice.getVehicle().getModel().getModelAccessType() == ModelAccessType.KEY ? ACCESS_TYPE_KEY : ACCESS_TYPE_KEYLESS;
            byteArrayToBeEncrypted[2] = (byte)accessType;

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, byteArrayToBeEncrypted.length - 1);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1];
            commandArrayToBeSent[0] = (byte)(0xDE);
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 1, encryptedBytes.length);

            Timber.d("Session password : " + BleUtils.byteArrayToHexString(mSessionPassword));
            Timber.d("String sent : " + BleUtils.byteArrayToHexString(byteArrayToBeEncrypted));
            Timber.d("Checksum : " + checkSum);
            Timber.d("Encrypted string : " + BleUtils.byteArrayToHexString(commandArrayToBeSent));

            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);
        } catch (Exception e) {
            initiateDeviceSyncFailedSequence();
        }
    }

    private void constructAndSendVehicleBatteryVoltageLimitCommand() {
        mIsBatteryVoltageWriteInProgress = true;

        try {
            long roundedCloudBatteryMinVoltage = Math.round(mCloudDevice.getVehicle().getModel().getBatteryMinVoltageLimit() * 100);
            long roundedCloudBatteryMaxVoltage = Math.round(mCloudDevice.getVehicle().getModel().getBatteryMaxVoltageLimit() * 100);

            String minVoltageHexStr = Long.toHexString(roundedCloudBatteryMinVoltage);
            String maxVoltageHexStr = Long.toHexString(roundedCloudBatteryMaxVoltage);

            if(minVoltageHexStr.length() % 2 != 0) {
                minVoltageHexStr = "0" + minVoltageHexStr;
            }

            if(maxVoltageHexStr.length() % 2 != 0) {
                maxVoltageHexStr = "0" + maxVoltageHexStr;
            }

            byte[] minVoltageByteArray = BleUtils.hexStringToByteArray(minVoltageHexStr);
            byte[] maxVoltageByteArray = BleUtils.hexStringToByteArray(maxVoltageHexStr);

            byte[] byteArrayToBeEncrypted = new byte[1 + 1 + 1 + 1 + 1];

            byteArrayToBeEncrypted[0] = minVoltageByteArray[0];
            byteArrayToBeEncrypted[1] = minVoltageByteArray[1];
            byteArrayToBeEncrypted[2] = maxVoltageByteArray[0];
            byteArrayToBeEncrypted[3] = maxVoltageByteArray[1];

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, byteArrayToBeEncrypted.length -1);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1];
            commandArrayToBeSent[0] = (byte)(0xD7);
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 1, encryptedBytes.length);

            Timber.d("Session password : " + BleUtils.byteArrayToHexString(mSessionPassword));
            Timber.d("String sent : " + BleUtils.byteArrayToHexString(byteArrayToBeEncrypted));
            Timber.d("Checksum : " + checkSum);
            Timber.d("Encrypted string : " + BleUtils.byteArrayToHexString(commandArrayToBeSent));

            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);

        } catch (Exception e) {
            initiateDeviceSyncFailedSequence();
        }
    }

    private void constructAndSendDeviceStateChangeCommand() {
        mIsDeviceStateWriteInProgress = true;

        try {
            //session password, aes key, activate/deactivate, checksum
            byte[] byteArrayToBeEncrypted = new byte[16 + 16 + 1 + 1];

            byte[] generatedAesKeyBytes = Base64.decode(mCloudDevice.getKey(), Base64.NO_WRAP | Base64.URL_SAFE);

            System.arraycopy(mSessionPassword, 0, byteArrayToBeEncrypted, 0, mSessionPassword.length);
            System.arraycopy(generatedAesKeyBytes, 0, byteArrayToBeEncrypted, mSessionPassword.length, generatedAesKeyBytes.length);
            byte deviceStateByte = 1;
            DeviceStatus deviceStatus = mCloudDevice.getStatus();
            if(deviceStatus == DeviceStatus.INITIALIZED) {
                deviceStateByte = (byte)1;
            } else if(deviceStatus == DeviceStatus.ACTIVATED) {
                deviceStateByte = (byte)2;
            } else if(deviceStatus == DeviceStatus.DISABLED) {
                deviceStateByte = (byte)3;
            }

            byteArrayToBeEncrypted[mSessionPassword.length + generatedAesKeyBytes.length] = deviceStateByte;

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, byteArrayToBeEncrypted.length - 1);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1];
            commandArrayToBeSent[0] = (byte)(0xDF);
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 1, encryptedBytes.length);

            Timber.d("String sent : %s", BleUtils.byteArrayToHexString(byteArrayToBeEncrypted));
            Timber.d("Checksum : %s", checkSum);
            Timber.d("Encrypted string : %s", BleUtils.byteArrayToHexString(commandArrayToBeSent));

            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);
        } catch (Exception e) {
            initiateDeviceSyncFailedSequence();
        }
    }

    public void performResetOdometer(int resetOdoTripId) {
        try {
            Timber.d("performing vehicle odo reset");

            //speed divisor, odo divisor, checksum
            byte[] byteArrayToBeEncrypted = new byte[4 + 4 + 1];

            byte[] odoByteArray = BleUtils.hexStringToByteArray(StringUtils.leftPad(Integer.toHexString(0), 8, '0'));
            byte[] tripIdByteArray = BleUtils.hexStringToByteArray(StringUtils.leftPad(Integer.toHexString(resetOdoTripId), 8, '0'));

            System.arraycopy(odoByteArray, 0, byteArrayToBeEncrypted, 0, odoByteArray.length);
            System.arraycopy(tripIdByteArray, 0, byteArrayToBeEncrypted, odoByteArray.length, tripIdByteArray.length);

            byte checkSum = calculateCheckSum(byteArrayToBeEncrypted, byteArrayToBeEncrypted.length - 1);
            byteArrayToBeEncrypted[byteArrayToBeEncrypted.length - 1] = checkSum;

            byte[] encryptedBytes = EncryptionHelper.encryptBytes(byteArrayToBeEncrypted, Base64.encodeToString(mSessionPassword, Base64.NO_WRAP | Base64.URL_SAFE));
            byte[] commandArrayToBeSent = new byte[encryptedBytes.length + 1];
            commandArrayToBeSent[0] = (byte)(0xD8);
            System.arraycopy(encryptedBytes, 0, commandArrayToBeSent, 1, encryptedBytes.length);

            Timber.d("String sent : " + BleUtils.byteArrayToHexString(byteArrayToBeEncrypted));
            Timber.d("Checksum : " + checkSum);
            Timber.d("Encrypted string : " + BleUtils.byteArrayToHexString(commandArrayToBeSent));

            writeDataToHouseKeepingCharacteristic(commandArrayToBeSent);
        } catch (Exception e) {

        }
    }

    private void initiateDeviceSyncFailedSequence() {
        clearSyncVariables();

        Timber.d("device sync failed");
        //todo::append error message in case of failure
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_DEVICE_SYNC_FAIL));
        disconnectFromDevice(mDisconnectSuccessCallback, mDisconnectFailCallback);
    }

    private byte calculateCheckSum(byte[] byteArray, int exclusiveEndPosition) {
        int checkSum = 0;
        for(int byteIndex = 0; byteIndex < exclusiveEndPosition; ++byteIndex) {
            byte currentByte = byteArray[byteIndex];
            int unsignedInt = (int)currentByte & 0xFF;
            checkSum += unsignedInt;
        }
        checkSum = checkSum & 0xFF;

        byte checkSumByte = (byte)checkSum;

        return checkSumByte;
    }

    private void initiateAuthenticationFailedSequence() {
        Timber.d("authentication failed");
        //todo::append error message in case of failure
        EventBus.getDefault().post(new EventBusMessage(BT_EVENT_STATE_AUTHENTICATION_FAIL));
        disconnectFromDevice(mDisconnectSuccessCallback, mDisconnectFailCallback);
    }


}
