package com.revos.android.utilities.general;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.LOG_PREFS;
import static com.revos.android.constants.Constants.LOG_TYPE_LIVE;
import static com.revos.android.constants.Constants.LOG_TYPE_OFFLINE;
import static com.revos.android.constants.Constants.LOG_TYPE_OFFLINE_BATTERY_ADC;
import static com.revos.android.constants.Constants.VEHICLE_LIVE_LOG_DIR_KEY;
import static com.revos.android.constants.Constants.VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY;
import static com.revos.android.constants.Constants.VEHICLE_OFFLINE_LOG_DIR_KEY;
import static com.revos.android.constants.Constants.WORKING_LIVE_LOG_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.WORKING_OFFLINE_LOG_FILE_NAME_KEY;

public class RevosFileUtils {

    //holds the single instance
    private static RevosFileUtils mInstance = null;


    //private constructor
    private RevosFileUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static RevosFileUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new RevosFileUtils(context);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    //stores the context
    private Context mContext = null;

    private void setContext(Context context) {
        mContext = context;
    }

    public int getStoredLogFileCount(String vehicleLogType) {
        if(mContext == null) {
            return 0;
        }

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(LOG_PREFS, MODE_PRIVATE);

        //check if there are any local files to sync, if not then abort
        String logDirKey = VEHICLE_OFFLINE_LOG_DIR_KEY;

        if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
            logDirKey = VEHICLE_LIVE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
            logDirKey = VEHICLE_OFFLINE_LOG_DIR_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)){
            logDirKey = VEHICLE_OFFLINE_BATTERY_ADC_LOG_DIR_KEY;
        }

        String logDirStr = sharedPreferences.getString(logDirKey, null);
        if(logDirStr == null) {
            return 0;
        }

        File logDir = new File(logDirStr);
        if(!logDir.exists()) {
            return 0;
        }

        File[] logFiles = logDir.listFiles();
        if(logFiles == null || logFiles.length == 0) {
            return 0;
        }

        ArrayList<String> logFileList = new ArrayList<>();

        //sort file with latest at position zero
        for (File logFile: logFiles) {
            logFileList.add(logFile.getName());
        }

        Collections.sort(logFileList);

        Collections.reverse(logFileList);

        File latestLogFile = new File(logDirStr, logFileList.get(0));

        //check if this file is not the one being written into
        String workingLogFileKey = WORKING_OFFLINE_LOG_FILE_NAME_KEY;
        if(vehicleLogType.equals(LOG_TYPE_LIVE)) {
            workingLogFileKey = WORKING_LIVE_LOG_FILE_NAME_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE)) {
            workingLogFileKey = WORKING_OFFLINE_LOG_FILE_NAME_KEY;
        } else if(vehicleLogType.equals(LOG_TYPE_OFFLINE_BATTERY_ADC)){
            workingLogFileKey = WORKING_OFFLINE_BATTERY_ADC_LOG_FILE_NAME_KEY;
        }
        String workingLogFile = mContext.getSharedPreferences(LOG_PREFS, MODE_PRIVATE).getString(workingLogFileKey, null);

        //if yes then take the next file to sync
        if(workingLogFile != null && workingLogFile.equals(latestLogFile.getName()) && logFileList.size() > 1) {
            return (logFileList.size() - 1);
        } else {
            return logFileList.size();
        }
    }

}
