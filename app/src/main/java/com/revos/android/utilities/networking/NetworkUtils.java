package com.revos.android.utilities.networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.boltCore.android.constants.Constants;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.revos.android.events.EventBusMessage;
import com.revos.android.jsonStructures.BatteryRegressionData;
import com.revos.android.jsonStructures.BatteryRegressionDataPoint;
import com.revos.android.jsonStructures.DataFeedVehicleLiveLog;
import com.revos.android.jsonStructures.DataFeedVehicleSnapshot;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.GeoFencing;
import com.revos.android.jsonStructures.TripDataFeed;
import com.revos.android.jsonStructures.User;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.ApolloClientUtils;
import com.revos.android.retrofit.DataFeedApiClient;
import com.revos.android.retrofit.DataFeedApiInterface;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.android.utilities.general.Version;
import com.revos.android.utilities.general.VersionUtils;
import com.revos.android.utilities.security.MetadataDecryptionHelper;
import com.revos.scripts.DownloadControllerMetadataQuery;
import com.revos.scripts.DownloadFileQuery;
import com.revos.scripts.FetchBatteryRegressionDataByVinQuery;
import com.revos.scripts.FetchDeviceQuery;
import com.revos.scripts.FetchGeoFenceQuery;
import com.revos.scripts.FetchModelImageQuery;
import com.revos.scripts.FetchUserDetailsQuery;
import com.revos.scripts.MarkOTAStatusMutation;
import com.revos.scripts.SendEmergencySMSMutation;
import com.revos.scripts.SetOdoResetFlagMutation;
import com.revos.scripts.type.ControllerInput;
import com.revos.scripts.type.DeviceUpdateInput;
import com.revos.scripts.type.DeviceWhereUniqueInput;
import com.revos.scripts.type.DownloadInput;
import com.revos.scripts.type.FenceType;
import com.revos.scripts.type.InputOtaStatus;
import com.revos.scripts.type.InputSMS;
import com.revos.scripts.type.ModelWhereUniqueInput;
import com.revos.scripts.type.UpdateVehicleInput;
import com.revos.scripts.type.VehicleWhereUniqueInput;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.revos.android.constants.Constants.APP_PREFS;
import static com.revos.android.constants.Constants.BATTERY_PREFS;
import static com.revos.android.constants.Constants.BATTERY_REGRESSION_DATA_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_READ_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY;
import static com.revos.android.constants.Constants.BLUETOOTH_PREFS;
import static com.revos.android.constants.Constants.BLUETOOTH_VEHICLE_CONTROL_JSON_KEY;
import static com.revos.android.constants.Constants.BT_EVENT_WRONG_PIN;
import static com.revos.android.constants.Constants.CRASH_TIMESTAMP_KEY;
import static com.revos.android.constants.Constants.DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS;
import static com.revos.android.constants.Constants.DEVICE_PIN_KEY;
import static com.revos.android.constants.Constants.DEVICE_PREFS;
import static com.revos.android.constants.Constants.DL_SIDE_1_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_1_TEMP_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_2_PREFIX;
import static com.revos.android.constants.Constants.DL_SIDE_2_TEMP_PREFIX;
import static com.revos.android.constants.Constants.GEN_DELIMITER;
import static com.revos.android.constants.Constants.GEO_FENCING_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.GEO_FENCING_PREFS;
import static com.revos.android.constants.Constants.HISTORICAL_DATA_IS_REFRESHING_KEY;
import static com.revos.android.constants.Constants.HISTORICAL_DATA_LIST_KEY;
import static com.revos.android.constants.Constants.IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.IS_SOS_SMS_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.IS_SOS_SMS_SENT_KEY;
import static com.revos.android.constants.Constants.IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY;
import static com.revos.android.constants.Constants.LAST_LIVE_PACKET_REFRESHED_TIMESTAMP;
import static com.revos.android.constants.Constants.LIVE_PACKET_JSON_KEY;
import static com.revos.android.constants.Constants.NETWORKING_PREFS;
import static com.revos.android.constants.Constants.NETWORK_EVENT_BATTERY_REGRESSION_DATA_REFRESHED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_CONTROLLER_METADATA_FILE_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_DEVICE_DATA_REFRESHED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_TRIP_DATA_REFRESHED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DETAILS_DOWNLOAD_SUCCESS;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_LAST_KNOWN_LOCATION_UPDATED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED;
import static com.revos.android.constants.Constants.NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED;
import static com.revos.android.constants.Constants.PIC_DIR_PATH;
import static com.revos.android.constants.Constants.PROFILE_PIC_PREFIX;
import static com.revos.android.constants.Constants.PROFILE_PIC_TEMP_PREFIX;
import static com.revos.android.constants.Constants.SMS_SENT_TIMESTAMP_KEY;
import static com.revos.android.constants.Constants.TRACKING_PREFS;
import static com.revos.android.constants.Constants.TRIP_DATA_IS_REFRESHING_KEY;
import static com.revos.android.constants.Constants.TRIP_DATA_KEY;
import static com.revos.android.constants.Constants.TRIP_DATA_NEEDS_REFRESH_KEY;
import static com.revos.android.constants.Constants.TRIP_PREFS;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.USER_JSON_DATA_KEY;
import static com.revos.android.constants.Constants.USER_KYC_STATUS_DATA_KEY;
import static com.revos.android.constants.Constants.USER_PREFS;
import static com.revos.android.constants.Constants.USER_PROFILE_LOCAL_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.USER_PROFILE_UPLOAD_FILE_NAME_KEY;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LATITUDE;
import static com.revos.android.constants.Constants.VEHICLE_LAST_KNOWN_LONGITUDE;
import static com.revos.android.constants.Constants.VEHICLE_MODEL_IMAGE_DIR_PATH;
import static com.revos.android.constants.Constants.VEHICLE_MODEL_IMAGE_FILE_PATH_KEY;
import static com.revos.android.constants.Constants.VEHICLE_PREFS;
import static com.revos.android.constants.Constants.VER_1_0;

public class NetworkUtils {

    private String TAG_READ = "read";
    private String TAG_LOG = "log";
    private String TAG_WRITE = "write";
    private String TAG_VEHICLE_CONTROL = "vehicleControl";
    private String TAG_BATTERY_ADC_LOG = "batteryAdcLog";
    private String FIRMWARE_METADATA_KEY = "firmwareMetadata";
    private String VERSION_NO_KEY = "versionNo";

    private boolean mProfilePicDownloadInProgress, mDLPic1DownloadInProgress, mDLPic2DownloadInProgress;
    public boolean mTripDataDownloadInProgress = false;

    private Context mContext;
    //holds the single instance
    private static NetworkUtils mInstance = null;

    //private constructor
    private NetworkUtils(Context context) {
        // no instantiation should be possible for this class
        mContext = context;
    }

    public static NetworkUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new NetworkUtils(context);
            //clear download flags
            mInstance.setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, false);
            mInstance.setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, false);
        } else {
            mInstance.setContext(context);
        }
        return mInstance;
    }

    private void setContext(Context context) {
        mContext = context;
    }

    private void downloadAndStoreControllerMetadata(String tag, String version) {
        if(tag == null || version == null || tag.isEmpty() || version.isEmpty()) {
            return;
        }


        Device cloudDevice = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();

        if(cloudDevice == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        DownloadControllerMetadataQuery downloadControllerMetadataQuery = DownloadControllerMetadataQuery
                .builder()
                .controllerInputVar(ControllerInput.builder()
                        .version(version)
                        .tag(tag)
                        .clientId(cloudDevice.getId())
                        .build())
                .build();

        if(tag.equals(TAG_READ)) {
            setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, true);
        } else if(tag.equals(TAG_LOG)) {
            setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, true);
        } else if(tag.equals(TAG_WRITE)) {
            setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, true);
        } else if(tag.equals(TAG_VEHICLE_CONTROL)) {
            setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, true);
        } else if(tag.equals(TAG_BATTERY_ADC_LOG)) {
            setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, true);
        }

        apolloClient.query(downloadControllerMetadataQuery).enqueue(new ApolloCall.Callback<DownloadControllerMetadataQuery.Data>() {
            @Override
            public void onResponse(@NotNull com.apollographql.apollo.api.Response<DownloadControllerMetadataQuery.Data> response) {
                String controllerMetadataJSON = response.getData().metadata().controller();

                //save this string in sharedPref
                if(tag.equals(TAG_READ)) {
                    mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, controllerMetadataJSON).apply();
                    setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                    Timber.d("read_data_downloaded");
                } else if(tag.equals(TAG_LOG)) {
                    mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, controllerMetadataJSON).apply();
                    setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                    Timber.d("log_data_downloaded");
                } else if(tag.equals(TAG_WRITE)) {
                    mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, controllerMetadataJSON).apply();
                    setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                    Timber.d("write_data_downloaded");
                } else if(tag.equals(TAG_VEHICLE_CONTROL)) {
                    mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, controllerMetadataJSON).apply();
                    setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                    Timber.d("vehicle_control_data_downloaded");
                } else if(tag.equals(TAG_BATTERY_ADC_LOG)) {
                    mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).edit().putString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, controllerMetadataJSON).apply();
                    setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                    Timber.d("battery_adc_log_data_downloaded");
                }

                EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_CONTROLLER_METADATA_FILE_DOWNLOADED + GEN_DELIMITER + tag));
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                if(tag.equals(TAG_READ)) {
                    setBooleanNetworkPref(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                } else if(tag.equals(TAG_LOG)) {
                    setBooleanNetworkPref(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                } else if(tag.equals(TAG_WRITE)) {
                    setBooleanNetworkPref(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                } else if(tag.equals(TAG_VEHICLE_CONTROL)) {
                    setBooleanNetworkPref(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                } else if(tag.equals(TAG_BATTERY_ADC_LOG)) {
                    setBooleanNetworkPref(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
                }
            }
        });
    }

    public void refreshControllerMetadata(String version) {
        String noPatchVersion = VersionUtils.removePatchVersion(version);

        downloadAndStoreControllerMetadata(TAG_READ, noPatchVersion);
        downloadAndStoreControllerMetadata(TAG_WRITE, noPatchVersion);
        downloadAndStoreControllerMetadata(TAG_LOG, noPatchVersion);
        downloadAndStoreControllerMetadata(TAG_VEHICLE_CONTROL, noPatchVersion);

        if(noPatchVersion != null && !noPatchVersion.isEmpty() && new Version(noPatchVersion).equals(new Version(VER_1_0))) {
            return;
        }

        downloadAndStoreControllerMetadata(TAG_BATTERY_ADC_LOG, noPatchVersion);
    }

    public void refreshGeoFenceData() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        String vin = vehicle.getVin();

        if(vin == null || vin.isEmpty()) {
            return;
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput.builder().vin(vin).build();
        FetchGeoFenceQuery fetchGeoFenceQuery = FetchGeoFenceQuery.builder().where(vehicleWhereUniqueInput).build();
        apolloClient.query(fetchGeoFenceQuery).enqueue(new ApolloCall.Callback<FetchGeoFenceQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchGeoFenceQuery.Data> response) {
                if(response != null && response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().getFence() != null) {
                    //clear prefs before saving
                    mContext.getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply();

                    GeoFencing geoFencing = new GeoFencing();

                    //check type
                    FenceType fenceType = response. getData().vehicles().getFence().type();

                    if(fenceType.equals(FenceType.RADIAL)) {
                        geoFencing.setGeoFenceType(fenceType);
                        geoFencing.setCircleRadius(response. getData().vehicles().getFence().radius());
                        LatLng centre = new LatLng(response. getData().vehicles().getFence().centre().latitude(), response.getData().vehicles().getFence().centre().longitude());
                        geoFencing.setCircleGeoFenceCentre(centre);

                    } else {
                        geoFencing.setGeoFenceType(fenceType);
                        List<FetchGeoFenceQuery.Vertex> getGeoFenceVerticesList = response.getData().vehicles().getFence().vertices();
                        ArrayList<LatLng> polygonVerticesArrayList = new ArrayList<>();
                        for(int index = 0 ; index < getGeoFenceVerticesList.size() ; index++) {
                            double latitude = getGeoFenceVerticesList.get(index).latitude();
                            double longitude = getGeoFenceVerticesList.get(index).longitude();
                            polygonVerticesArrayList.add(new LatLng(latitude, longitude));
                        }
                        geoFencing.setPolygonVerticesList(polygonVerticesArrayList);
                    }

                    geoFencing.setGeoFenceId(response.getData().vehicles().getFence().id());

                    //save to prefs
                    mContext.getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, new Gson().toJson(geoFencing)).apply();
                } else {
                    mContext.getSharedPreferences(GEO_FENCING_PREFS, MODE_PRIVATE).edit().putString(GEO_FENCING_JSON_DATA_KEY, null).apply();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    public void refreshVehicleLiveLogData() {

        String userToken = mContext.getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }

                    String dataStr = jsonObject.get("data").toString();

                    DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                    if (vehicleSnapshot != null && vehicleSnapshot.getBattery() != null) {

                        DataFeedVehicleSnapshot liveLogSnapshot = PrefUtils.getInstance(mContext).getLivePacketObjectFromPrefs();

                        if(liveLogSnapshot == null) {
                            liveLogSnapshot = new DataFeedVehicleSnapshot();
                        }

                        liveLogSnapshot = vehicleSnapshot;

                        mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().putString(LIVE_PACKET_JSON_KEY, new Gson().toJson(liveLogSnapshot)).apply();
                        mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().putLong(LAST_LIVE_PACKET_REFRESHED_TIMESTAMP, System.currentTimeMillis()).apply();

                        EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_VEHICLE_LIVE_LOG_DATA_UPDATED));
                    }

                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void updateDeviceFirmwareOnServer(String firmwareVersion) {
        if(firmwareVersion == null || firmwareVersion.isEmpty()) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        Device device = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();

        if(device == null) {
            return;
        }

        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS, true).apply();

        DeviceWhereUniqueInput deviceWhereUniqueInput = DeviceWhereUniqueInput.builder()
                .deviceId(device.getDeviceId())
                .build();

        InputOtaStatus inputOtaStatus = InputOtaStatus.builder()
                .version(firmwareVersion)
                .build();

        MarkOTAStatusMutation markOTAStatusMutation = MarkOTAStatusMutation.builder()
                .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
                .inputOtaStatusVar(inputOtaStatus)
                .build();

        apolloClient.mutate(markOTAStatusMutation).enqueue(new ApolloCall.Callback<MarkOTAStatusMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<MarkOTAStatusMutation.Data> response) {
                mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS, false).apply();
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putBoolean(DEVICE_FIRMWARE_VERSION_UPDATE_IN_PROGRESS, false).apply();
            }
        });
    }


    public void refreshVehicleHistoricalData() {
        String userToken = mContext.getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().putBoolean(HISTORICAL_DATA_IS_REFRESHING_KEY, true).apply();


        if(vehicle == null) {
            return;
        }

        dataFeedApiInterface.getVehicleLiveLogForCount(vehicle.getVin(), "", "", "", 2000, true, bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().putBoolean(HISTORICAL_DATA_IS_REFRESHING_KEY, false).apply();

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }

                    String dataStr = jsonObject.get("data").toString();

                    ArrayList<DataFeedVehicleLiveLog> vehicleLiveLogList = new Gson().fromJson(dataStr, new TypeToken<ArrayList<DataFeedVehicleLiveLog>>(){}.getType());

                    ArrayList<DataFeedVehicleLiveLog> vehicleLiveLogListToBeSaved = new ArrayList<>();

                    if(vehicleLiveLogList != null && !vehicleLiveLogList.isEmpty()) {
                        for(DataFeedVehicleLiveLog liveLog: vehicleLiveLogList) {
                            if(liveLog != null) {
                                vehicleLiveLogListToBeSaved.add(liveLog);
                            }
                        }
                    }

                    mContext.getSharedPreferences(TRACKING_PREFS, MODE_PRIVATE).edit().putString(HISTORICAL_DATA_LIST_KEY, new Gson().toJson(vehicleLiveLogListToBeSaved)).apply();
                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_VEHICLE_HISTORICAL_DATA_UPDATED));

                } catch (Exception e) {}

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void refreshTripData() {
        if(mTripDataDownloadInProgress) {
            return;
        }

        String userToken = mContext.getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        mContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putBoolean(TRIP_DATA_IS_REFRESHING_KEY, true).apply();

        mTripDataDownloadInProgress = true;

        dataFeedApiInterface.getAllTripsForVehicle(vehicle.getVin(), 10, 0, bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                mTripDataDownloadInProgress = false;
                mContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putBoolean(TRIP_DATA_IS_REFRESHING_KEY, false).apply();

                if(response.code() != 200 || response.body() == null) {
                    return;
                }

                try {

                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    JsonArray jsonArray = jsonObject.getAsJsonArray("trips");

                    if(jsonArray != null && jsonArray.size() > 0) {

                        String dataStr = jsonArray.toString();

                        ArrayList<TripDataFeed> tripArrayList = new Gson().fromJson(dataStr, new TypeToken<ArrayList<TripDataFeed>>(){}.getType());

                        ArrayList<TripDataFeed> tripListToBeSaved = new ArrayList<>();
                        for(TripDataFeed fetchedTrip : tripArrayList) {

                            if(fetchedTrip != null) {
                                tripListToBeSaved.add(fetchedTrip);
                            }
                        }

                        mContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putBoolean(TRIP_DATA_NEEDS_REFRESH_KEY, false).apply();
                        mContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putString(TRIP_DATA_KEY, new Gson().toJson(tripListToBeSaved)).apply();
                        EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_TRIP_DATA_REFRESHED));
                    }

                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTripDataDownloadInProgress = false;
                mContext.getSharedPreferences(TRIP_PREFS, MODE_PRIVATE).edit().putBoolean(TRIP_DATA_IS_REFRESHING_KEY, false).apply();
            }
        });
    }

    public void refreshBatteryRegressionData() {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        if(vehicle.getVin() == null || vehicle.getVin().isEmpty()) {
            return;
        }

        FetchBatteryRegressionDataByVinQuery fetchBatteryRegressionDataByVinQuery = FetchBatteryRegressionDataByVinQuery
                                                                            .builder()
                                                                            .vehicleWhereUniqueInput(VehicleWhereUniqueInput
                                                                                    .builder()
                                                                                    .vin(vehicle.getVin())
                                                                                    .build())
                                                                            .build();

        apolloClient.query(fetchBatteryRegressionDataByVinQuery).enqueue(new ApolloCall.Callback<FetchBatteryRegressionDataByVinQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchBatteryRegressionDataByVinQuery.Data> response) {
                
                if (response.getData() != null && response.getData().vehicles() != null && response.getData().vehicles().batteryRegressionData() != null) {

                    try {

                        String valueStr = response.getData().vehicles().batteryRegressionData();

                        JsonObject valueJsonObject = new JsonParser().parse(valueStr).getAsJsonObject();
                        JsonArray dataJsonArray = valueJsonObject.getAsJsonArray("data");

                        //save this data in JSON structure
                        BatteryRegressionData batteryRegressionData = new BatteryRegressionData();

                        ArrayList<BatteryRegressionDataPoint> regressionDataPoints = new ArrayList<>();

                        batteryRegressionData.setVersion(valueJsonObject.get("version").getAsInt());

                        for (JsonElement dataJsonElement : dataJsonArray) {
                            BatteryRegressionDataPoint batteryRegressionDataPoint = new BatteryRegressionDataPoint();

                            batteryRegressionDataPoint.setVoltage(dataJsonElement.getAsJsonObject().get("voltage").getAsDouble());
                            batteryRegressionDataPoint.setSoc(dataJsonElement.getAsJsonObject().get("soc").getAsDouble());
                            batteryRegressionDataPoint.setDte(dataJsonElement.getAsJsonObject().get("dte").getAsDouble());

                            regressionDataPoints.add(batteryRegressionDataPoint);
                        }

                        batteryRegressionData.setDataPoints(regressionDataPoints);

                        mContext.getSharedPreferences(BATTERY_PREFS, MODE_PRIVATE).edit().putString(BATTERY_REGRESSION_DATA_KEY, new Gson().toJson(batteryRegressionData)).apply();

                        EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_BATTERY_REGRESSION_DATA_REFRESHED));
                    } catch (Exception e) {
                        //nothing to do here
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
            }
        });
    }

    public void refreshDeviceData(String deviceId) {
        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null || deviceId == null || deviceId.isEmpty()) {
            return;
        }

        DeviceWhereUniqueInput deviceWhereUniqueInput = DeviceWhereUniqueInput
                .builder()
                .deviceId(deviceId)
                .build();

        FetchDeviceQuery fetchDeviceQuery = FetchDeviceQuery
                .builder()
                .deviceWhereUniqueInputVar(deviceWhereUniqueInput)
                .build();

        apolloClient.query(fetchDeviceQuery).enqueue(new ApolloCall.Callback<FetchDeviceQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchDeviceQuery.Data> response) {
                if (response.getData() != null && response.getData().device() != null && response.getData().device().get() != null) {

                    //check if we have a cleared devices from prefs, if there's no device present in prefs, then we should not store this value
                    //as this might be running in background and the user might have disconnected from the device manually. In that case
                    //when this call returns then it will write the device details in prefs, even though user has manually disconnected
                    Device savedDevice = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();
                    if(savedDevice == null) {
                        return;
                    }

                    FetchDeviceQuery.Device cloudDevice = response.getData().device();

                    Device device = PrefUtils.getInstance(mContext)
                                             .saveDeviceVariablesInPrefs(cloudDevice.get());

                    //save device pin, no need to set it null if it's not provided as it is just a refresh
                    if(cloudDevice.get().pin() != null) {
                        mContext.getSharedPreferences(DEVICE_PREFS, MODE_PRIVATE).edit().putString(DEVICE_PIN_KEY, cloudDevice.get().pin()).apply();
                    }

                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_DEVICE_DATA_REFRESHED));
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    public void downloadVehicleModelImageIfRequired(String modelId) {
        if(modelId == null) {
            return;
        }

        try {
            //check if file is already present
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(VEHICLE_PREFS, Context.MODE_PRIVATE);
            String filePath = sharedPreferences.getString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, null);

            if (filePath == null) {
                downloadVehicleModelImage(modelId);
            } else {

                File imageFile = new File(filePath);
                //the "." is a metacharacter hence we need to split using this [.]
                //another approach could have been to use "\\." as the regex character
                if (imageFile.exists() && imageFile.getName().split("[.]")[0].equals(modelId)) {
                    return;
                }

                downloadVehicleModelImage(modelId);
            }
        } catch (Exception e) {

        }
    }

    private void downloadVehicleModelImage(String modelId) {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        ModelWhereUniqueInput modelWhereUniqueInput = ModelWhereUniqueInput.builder().id(modelId).build();
        FetchModelImageQuery fetchModelImageQuery = FetchModelImageQuery.builder().modelWhereUniqueInput(modelWhereUniqueInput).build();

        apolloClient.query(fetchModelImageQuery).enqueue(new ApolloCall.Callback<FetchModelImageQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<FetchModelImageQuery.Data> response) {
                if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                    return;
                }

                if(response.getData() != null && response.getData().models() != null && response.getData().models().get() != null
                    && response.getData().models().get().image() != null) {

                    String fileDataUri = response.getData().models().get().image();
                    if(!fileDataUri.isEmpty()) {
                        try {
                            //extract file extension from data uri
                            String processedString = fileDataUri.split(";")[0];
                            processedString = processedString.split(":")[1];
                            processedString = processedString.split("/")[1];
                            processedString = "."+ processedString;
                            File imageFile = new File(mContext.getExternalFilesDir(VEHICLE_MODEL_IMAGE_DIR_PATH), modelId + processedString);
                            byte[] byteArray = Base64.decode(fileDataUri.substring(fileDataUri.indexOf(",")), Base64.DEFAULT);
                            writeByteArrayAsFile(byteArray, imageFile);

                            EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_VEHICLE_MODEL_IMAGE_DOWNLOADED));
                            mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, imageFile.getAbsolutePath()).apply();
                        } catch (Exception e) {
                        }
                    } else {
                        mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_MODEL_IMAGE_FILE_PATH_KEY, null).apply();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    private void writeByteArrayAsFile(byte[] data, File file) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (IOException e) {
        }
    }

    public void sendEmergencySMS(String message) {

        setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, true);

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        SendEmergencySMSMutation sendEmergencySMSMutation = SendEmergencySMSMutation
                                                            .builder()
                                                            .inputSMSVar(InputSMS.builder().message(message).build())
                                                            .build();

        if(apolloClient == null) {
            return;
        }

        apolloClient.mutate(sendEmergencySMSMutation).enqueue(new ApolloCall.Callback<SendEmergencySMSMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SendEmergencySMSMutation.Data> response) {
                setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, false);
                if(response.getErrors() == null || response.getErrors().isEmpty()) {
                    //clear crash timestamp
                    mContext.getSharedPreferences(APP_PREFS, MODE_PRIVATE).edit().putLong(CRASH_TIMESTAMP_KEY, 0).apply();
                    setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, true);
                    //set sms sent timestamp
                    mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE).edit().putLong(SMS_SENT_TIMESTAMP_KEY, System.currentTimeMillis()).apply();
                } else {
                    setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, false);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                setBooleanNetworkPref(IS_SOS_SMS_IN_PROGRESS_KEY, false);
                setBooleanNetworkPref(IS_SOS_SMS_SENT_KEY, false);
            }
        });
    }

    public boolean isSosSMSInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_SOS_SMS_IN_PROGRESS_KEY, false);
    }

    public boolean isSosSMSSent() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_SOS_SMS_SENT_KEY, false);
    }

    public boolean isReadDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_READ_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isReadDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_READ_FORMAT_KEY, null);
        return (jsonData != null);
    }

    public boolean isLogDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isLogDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_LOG_FORMAT_KEY, null);
        return (jsonData != null);
    }

    public boolean isParamWriteDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_PARAM_WRITE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isParamWriteDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_CONTROLLER_WRITE_FORMAT_KEY, null);
        return (jsonData != null);
    }

    public boolean isVehicleControlDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_VEHICLE_CONTROL_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isVehicleControlDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_VEHICLE_CONTROL_JSON_KEY, null);
        return (jsonData != null);
    }

    public boolean isBatteryAdcLogDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_BATTERY_ADC_LOG_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isDeviceDataDownloadInProgress() {
        if(mContext == null) {
            return false;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_DEVICE_DATA_DOWNLOAD_IN_PROGRESS_KEY, false);
    }

    public boolean isBatteryAdcLogDataDownloadComplete() {
        if(mContext == null) {
            return false;
        }
        String jsonData = mContext.getSharedPreferences(BLUETOOTH_PREFS, MODE_PRIVATE).getString(BLUETOOTH_BATTERY_ADC_LOG_JSON_KEY, null);
        return (jsonData != null);
    }

    private void setBooleanNetworkPref(String prefKey, boolean prefValue) {
        if(mContext == null) {
            return;
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORKING_PREFS, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(prefKey, prefValue).apply();
    }

    public void markOdometerResetFlag(boolean resetOdometerSuccessful) {
        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        if(vehicle == null) {
            return;
        }

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        VehicleWhereUniqueInput vehicleWhereUniqueInput = VehicleWhereUniqueInput
                .builder()
                .vin(vehicle.getVin())
                .build();

        UpdateVehicleInput updateVehicleInputVar = UpdateVehicleInput
                .builder()
                .device(DeviceUpdateInput.builder()
                        .odoResetRequired(resetOdometerSuccessful)
                        .build())
                .build();

        SetOdoResetFlagMutation setOdoResetFlagMutation = SetOdoResetFlagMutation
                .builder()
                .inputResetOdoFlagVar(updateVehicleInputVar)
                .vehicleWhereUniqueInputVar(vehicleWhereUniqueInput)
                .build();

        apolloClient.mutate(setOdoResetFlagMutation).enqueue(new ApolloCall.Callback<SetOdoResetFlagMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SetOdoResetFlagMutation.Data> response) {

                SetOdoResetFlagMutation.Update odoResetFlagMutationResponse = response.getData().vehicles().update();

                Device device = PrefUtils.getInstance(mContext).saveDeviceVariablesInPrefs(odoResetFlagMutationResponse);

                Timber.d("odo reset flag set successfully set");
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

                Timber.d("odo reset flag set failed");

            }
        });
    }

    public boolean checkAndStartVehicleMetadataDownloadIfRequired(String firmwareVersion) {
        if(firmwareVersion == null) {
            return false;
        }

        String noPatchFirmwareVersion = VersionUtils.removePatchVersion(firmwareVersion);

        if(noPatchFirmwareVersion == null) {
            return false;
        }


        boolean mIsReadDataPresentAndVerified, mIsLogDataPresentAndVerified, mIsWriteDataPresentAndVerified, mIsControlDataPresentAndVerified, mIsBatteryAdcLogDataPresentAndVerified;
        mIsReadDataPresentAndVerified = mIsLogDataPresentAndVerified = mIsWriteDataPresentAndVerified = mIsControlDataPresentAndVerified = mIsBatteryAdcLogDataPresentAndVerified = false;

        MetadataDecryptionHelper metadataDecryptionHelper = MetadataDecryptionHelper.getInstance(mContext);

        String parameterReadJSON = metadataDecryptionHelper.decryptControllerReadMetadata();
        String parameterLogJSON = metadataDecryptionHelper.decryptControllerLogMetadata();
        String parameterWriteJSON = metadataDecryptionHelper.decryptControllerWriteMetadata();
        String vehicleControlJSON = metadataDecryptionHelper.decryptVehicleControlMetadata();

        if(parameterReadJSON == null) {
            if(!isReadDataDownloadInProgress() && !isReadDataDownloadComplete()) {
                downloadAndStoreControllerMetadata(TAG_READ, noPatchFirmwareVersion);
            }
        } else if(!isReadDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject parameterReadJsonObject = null;
            try {
                parameterReadJsonObject = new JsonParser().parse(parameterReadJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(parameterReadJsonObject != null && parameterReadJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(parameterReadJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = parameterReadJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        downloadAndStoreControllerMetadata(TAG_READ, noPatchFirmwareVersion);
                    } else {
                        mIsReadDataPresentAndVerified = true;
                    }
                } else {
                    downloadAndStoreControllerMetadata(TAG_READ, noPatchFirmwareVersion);
                }
            } else {
                downloadAndStoreControllerMetadata(TAG_READ, noPatchFirmwareVersion);
            }
        }

        if(parameterLogJSON == null) {
            if(!isLogDataDownloadInProgress() && !isLogDataDownloadComplete()) {
                downloadAndStoreControllerMetadata(TAG_LOG, noPatchFirmwareVersion);
            }
        } else if(!isLogDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject parameterLogJsonObject = null;
            try {
                parameterLogJsonObject = new JsonParser().parse(parameterLogJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(parameterLogJsonObject != null && parameterLogJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(parameterLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = parameterLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        downloadAndStoreControllerMetadata(TAG_LOG, noPatchFirmwareVersion);
                    } else {
                        mIsLogDataPresentAndVerified = true;
                    }
                } else {
                    downloadAndStoreControllerMetadata(TAG_LOG, noPatchFirmwareVersion);
                }
            } else {
                downloadAndStoreControllerMetadata(TAG_LOG, noPatchFirmwareVersion);
            }
        }

        if(parameterWriteJSON == null) {
            if(!isParamWriteDataDownloadInProgress() && !isParamWriteDataDownloadComplete()) {
                downloadAndStoreControllerMetadata(TAG_WRITE, noPatchFirmwareVersion);
            }
        } else if(!isParamWriteDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject parameterWriteJsonObject = null;
            try {
                parameterWriteJsonObject = new JsonParser().parse(parameterWriteJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(parameterWriteJsonObject != null && parameterWriteJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(parameterWriteJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = parameterWriteJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        downloadAndStoreControllerMetadata(TAG_WRITE, noPatchFirmwareVersion);
                    } else {
                        mIsWriteDataPresentAndVerified = true;
                    }
                } else {
                    downloadAndStoreControllerMetadata(TAG_WRITE, noPatchFirmwareVersion);
                }
            } else {
                downloadAndStoreControllerMetadata(TAG_WRITE, noPatchFirmwareVersion);
            }
        }

        if(vehicleControlJSON == null) {
            if(!isVehicleControlDataDownloadInProgress() && !isVehicleControlDataDownloadComplete()) {
                downloadAndStoreControllerMetadata(TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
            }
        } else if(!isVehicleControlDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject vehicleControlJsonObject = null;
            try {
                vehicleControlJsonObject = new JsonParser().parse(vehicleControlJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(vehicleControlJsonObject != null && vehicleControlJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(vehicleControlJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = vehicleControlJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        downloadAndStoreControllerMetadata(TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
                    } else {
                        mIsControlDataPresentAndVerified = true;
                    }
                } else {
                    downloadAndStoreControllerMetadata(TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
                }
            } else {
                downloadAndStoreControllerMetadata(TAG_VEHICLE_CONTROL, noPatchFirmwareVersion);
            }
        }

        if(noPatchFirmwareVersion != null && !noPatchFirmwareVersion.isEmpty() && new Version(noPatchFirmwareVersion).equals(new Version(VER_1_0))) {
            return mIsReadDataPresentAndVerified && mIsLogDataPresentAndVerified &&mIsWriteDataPresentAndVerified && mIsControlDataPresentAndVerified;
        }

        String batteryAdcLogJSON =  metadataDecryptionHelper.decryptBatteryAdcLogMetadata();

        if(batteryAdcLogJSON == null) {
            if(!isBatteryAdcLogDataDownloadInProgress() && !isBatteryAdcLogDataDownloadComplete()) {
                downloadAndStoreControllerMetadata(TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
            }
        } else if(!isBatteryAdcLogDataDownloadInProgress()){
            //check if the firmware version matches, if not then download again
            JsonObject batteryAdcLogJsonObject = null;
            try {
                batteryAdcLogJsonObject = new JsonParser().parse(batteryAdcLogJSON).getAsJsonObject();
            } catch (Exception e) {
                EventBus.getDefault().post(new EventBusMessage(BT_EVENT_WRONG_PIN));
                return false;
            }
            if(batteryAdcLogJsonObject != null && batteryAdcLogJsonObject.has(FIRMWARE_METADATA_KEY)) {
                if(batteryAdcLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).has(VERSION_NO_KEY)) {
                    String versionNumber = batteryAdcLogJsonObject.getAsJsonObject(FIRMWARE_METADATA_KEY).get(VERSION_NO_KEY).getAsString();
                    String noPatchVersionNumber = VersionUtils.removePatchVersion(versionNumber);
                    if(!noPatchVersionNumber.equals(noPatchFirmwareVersion)) {
                        downloadAndStoreControllerMetadata(TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
                    } else {
                        mIsBatteryAdcLogDataPresentAndVerified = true;
                    }
                } else {
                    downloadAndStoreControllerMetadata(TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
                }
            } else {
                downloadAndStoreControllerMetadata(TAG_BATTERY_ADC_LOG, noPatchFirmwareVersion);
            }
        }

        return mIsReadDataPresentAndVerified && mIsLogDataPresentAndVerified &&mIsWriteDataPresentAndVerified && mIsControlDataPresentAndVerified && mIsBatteryAdcLogDataPresentAndVerified;

    }

    private void downloadProfilePic() {
        if(!mProfilePicDownloadInProgress) {
            mProfilePicDownloadInProgress = true;

            ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();
            if(apolloClient == null) {
                return;
            }

            String fileName = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, null);

            DownloadInput downloadInput = DownloadInput.builder().name(fileName).build();

            DownloadFileQuery downloadFileQuery = DownloadFileQuery
                    .builder()
                    .downloadInputVar(downloadInput)
                    .build();

            apolloClient.query(downloadFileQuery).enqueue(new ApolloCall.Callback<DownloadFileQuery.Data>() {
                @Override
                public void onResponse(com.apollographql.apollo.api.@NotNull Response<DownloadFileQuery.Data> response) {
                    mProfilePicDownloadInProgress = false;
                    String fileAsBase64Str = response.getData().files().download().imageBuffer();
                    byte[] decodedFileByteArray = Base64.decode(fileAsBase64Str, Base64.NO_WRAP);

                    //write file to storage
                    File profilePicFile = new File(mContext.getExternalFilesDir(PIC_DIR_PATH), PROFILE_PIC_PREFIX + fileName + ".jpg");
                    writeByteArrayAsFile(decodedFileByteArray, profilePicFile);

                    //clear previous files
                    File tempDir = profilePicFile.getParentFile();
                    File[] tempFiles = tempDir.listFiles();
                    for (int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(PROFILE_PIC_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(profilePicFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    //write this data to prefs
                    SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    if(profilePicFile.exists()) {
                        editor.putString(USER_PROFILE_LOCAL_FILE_PATH_KEY, profilePicFile.getAbsolutePath());
                        editor.apply();
                    }

                    //send out a broadcast for others to listen
                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_USER_PROFILE_PIC_DOWNLOADED));
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {
                    mProfilePicDownloadInProgress = false;
                }
            });
        }
    }

    private void downloadDLSide1Pic() {
        if(!mDLPic1DownloadInProgress) {
            mDLPic1DownloadInProgress = true;

            ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();
            if(apolloClient == null) {
                return;
            }

            String fileName = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, null);

            DownloadInput downloadInput = DownloadInput.builder().name(fileName).build();

            DownloadFileQuery downloadFileQuery = DownloadFileQuery
                    .builder()
                    .downloadInputVar(downloadInput)
                    .build();

            apolloClient.query(downloadFileQuery).enqueue(new ApolloCall.Callback<DownloadFileQuery.Data>() {
                @Override
                public void onResponse(com.apollographql.apollo.api.@NotNull Response<DownloadFileQuery.Data> response) {
                    mDLPic1DownloadInProgress = false;
                    String fileAsBase64Str = response.getData().files().download().imageBuffer();
                    byte[] decodedFileByteArray = Base64.decode(fileAsBase64Str, Base64.NO_WRAP);

                    //write file to storage
                    File dlSide1PicFile = new File(mContext.getExternalFilesDir(PIC_DIR_PATH), DL_SIDE_1_PREFIX + fileName + ".jpg");
                    writeByteArrayAsFile(decodedFileByteArray, dlSide1PicFile);

                    //clear previous files
                    File tempDir = dlSide1PicFile.getParentFile();
                    File[] tempFiles = tempDir.listFiles();
                    for (int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(DL_SIDE_1_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(dlSide1PicFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    //write this data to prefs
                    SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    if(dlSide1PicFile.exists()) {
                        editor.putString(USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY, dlSide1PicFile.getAbsolutePath());
                        editor.apply();
                    }

                    //send out a broadcast for others to listen
                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_USER_DL_SIDE_1_DOWNLOADED));
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {
                    mDLPic1DownloadInProgress = false;
                }
            });
        }
    }

    private void downloadDLSide2Pic() {
        if(!mDLPic2DownloadInProgress) {
            mDLPic2DownloadInProgress = true;

            ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();
            if(apolloClient == null) {
                return;
            }

            String fileName = mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).getString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, null);

            DownloadInput downloadInput = DownloadInput.builder().name(fileName).build();

            DownloadFileQuery downloadFileQuery = DownloadFileQuery
                    .builder()
                    .downloadInputVar(downloadInput)
                    .build();

            apolloClient.query(downloadFileQuery).enqueue(new ApolloCall.Callback<DownloadFileQuery.Data>() {
                @Override
                public void onResponse(com.apollographql.apollo.api.@NotNull Response<DownloadFileQuery.Data> response) {
                    mDLPic2DownloadInProgress = false;
                    String fileAsBase64Str = response.getData().files().download().imageBuffer();
                    byte[] decodedFileByteArray = Base64.decode(fileAsBase64Str, Base64.NO_WRAP);

                    //write file to storage
                    File dlSide2PicFile = new File(mContext.getExternalFilesDir(PIC_DIR_PATH), DL_SIDE_2_PREFIX + fileName + ".jpg");
                    writeByteArrayAsFile(decodedFileByteArray, dlSide2PicFile);

                    //clear previous files
                    File tempDir = dlSide2PicFile.getParentFile();
                    File[] tempFiles = tempDir.listFiles();
                    for (int fileIndex = 0; fileIndex < tempFiles.length; ++fileIndex) {
                        if(tempFiles[fileIndex].getName().contains(DL_SIDE_2_TEMP_PREFIX) && !tempFiles[fileIndex].getName().equals(dlSide2PicFile.getName())) {
                            tempFiles[fileIndex].delete();
                        }
                    }

                    //write this data to prefs
                    SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    if(dlSide2PicFile.exists()) {
                        editor.putString(USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY, dlSide2PicFile.getAbsolutePath());
                        editor.apply();
                    }

                    //send out a broadcast for others to listen
                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_USER_DL_SIDE_2_DOWNLOADED));
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {
                    mDLPic2DownloadInProgress = false;
                }
            });
        }
    }

    public void downloadProfilePicIfRequired() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        if(sharedPreferences.getString(USER_PROFILE_UPLOAD_FILE_NAME_KEY, null) != null) {
            //load user profile pic
            String profilePicFileName = sharedPreferences.getString(USER_PROFILE_LOCAL_FILE_PATH_KEY, null);
            if(profilePicFileName == null) {
                //getTokenAndDownloadUserProfilePic(false);
                downloadProfilePic();
            }
        }
    }

    public void downloadDLPicIfRequired() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        String userDLSide1UploadFileName = sharedPreferences.getString(USER_DL_SIDE_1_UPLOAD_FILE_NAME_KEY, null);
        String userDLSide2UploadFileName = sharedPreferences.getString(USER_DL_SIDE_2_UPLOAD_FILE_NAME_KEY, null);

        if(userDLSide1UploadFileName != null) {
            //load user dl pic
            String dlPicFileName = sharedPreferences.getString(USER_DL_SIDE_1_LOCAL_FILE_PATH_KEY, null);
            if(dlPicFileName == null) {
                //getTokenAndDownloadDLSide1Pic(false);
                downloadDLSide1Pic();
            }
        }

        if(userDLSide2UploadFileName != null) {
            String dlPicFileName = sharedPreferences.getString(USER_DL_SIDE_2_LOCAL_FILE_PATH_KEY, null);
            if(dlPicFileName == null) {
                //getTokenAndDownloadDLSide2Pic(false);
                downloadDLSide2Pic();
            }
        }
    }

    public void refreshUserDetails() {

        ApolloClient apolloClient = ApolloClientUtils.getInstance(mContext).getApolloClient();

        if(apolloClient == null) {
            return;
        }

        FetchUserDetailsQuery refreshUserDetails = FetchUserDetailsQuery.builder().build();
        apolloClient.query(refreshUserDetails).enqueue(new ApolloCall.Callback<FetchUserDetailsQuery.Data>() {

            @Override
            public void onResponse(@NotNull Response<FetchUserDetailsQuery.Data> response) {
                if(response.getErrors() != null && !response.getErrors().isEmpty()) {
                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL));
                    return;
                }

                if(response.getData() != null && response.getData().account() != null && response.getData().account().me() != null){

                    User user = new User();
                    user.setId(response.getData().account().me().id());
                    user.setRole(response.getData().account().me().role());
                    user.setFirstName(response.getData().account().me().firstName());
                    user.setLastName(response.getData().account().me().lastName());
                    user.setEmail(response.getData().account().me().email());
                    user.setPhone(response.getData().account().me().phone());
                    user.setAltPhone1(response.getData().account().me().altPhone1());
                    user.setAltPhone2(response.getData().account().me().altPhone2());
                    user.setDob(response.getData().account().me().dob());
                    user.setStatusType(response.getData().account().me().status());

                    if(response.getData().account().me().kyc() != null && response.getData().account().me().kyc().status() != null) {
                        mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit()
                                .putString(USER_KYC_STATUS_DATA_KEY, new Gson().toJson(response.getData().account().me().kyc().status())).apply();
                    }

                    mContext.getSharedPreferences(USER_PREFS, MODE_PRIVATE).edit().putString(USER_JSON_DATA_KEY, new Gson().toJson(user)).apply();

                    EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_USER_DETAILS_DOWNLOAD_SUCCESS));
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_USER_DETAILS_DOWNLOAD_FAIL));
            }
        });
    }

    public void refreshVehicleLastKnownLocation() {

        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        String userToken = mContext.getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_TOKEN_KEY, null);

        if(userToken == null) {
            return;
        }

        String bearerToken = "Bearer " + userToken;

        DataFeedApiInterface dataFeedApiInterface = DataFeedApiClient.getClient().create(DataFeedApiInterface.class);

        dataFeedApiInterface.getVehicleLocationSnapshot(vehicle.getVin(), bearerToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    String responseBody = response.body().string();

                    JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

                    int status = jsonObject.get("status").getAsInt();

                    if (status != 200) {
                        return;
                    }

                    String dataStr = jsonObject.get("data").toString();

                    DataFeedVehicleSnapshot vehicleSnapshot = new Gson().fromJson(dataStr, DataFeedVehicleSnapshot.class);

                    if(vehicleSnapshot != null && vehicleSnapshot.getLocation() != null
                            && vehicleSnapshot.getLocation().getLatitude() != 0 && vehicleSnapshot.getLocation().getLatitude() != 0) {

                        String latStr = String.valueOf(vehicleSnapshot.getLocation().getLatitude());
                        String lngStr = String.valueOf(vehicleSnapshot.getLocation().getLongitude());

                        mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_LAST_KNOWN_LATITUDE, latStr).apply();
                        mContext.getSharedPreferences(VEHICLE_PREFS, MODE_PRIVATE).edit().putString(VEHICLE_LAST_KNOWN_LONGITUDE, lngStr).apply();

                        EventBus.getDefault().post(new EventBusMessage(NETWORK_EVENT_VEHICLE_LAST_KNOWN_LOCATION_UPDATED));
                    }

                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {}
        });
    }
}