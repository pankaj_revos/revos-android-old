package com.revos.android.utilities.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.revos.android.R;
import com.revos.android.activities.MainActivity;

import java.util.List;

public class GoogleMatchedPoiAdapter extends RecyclerView.Adapter<GoogleMatchedPoiAdapter.ViewHolder> {

    private List<AutocompletePrediction> mPoiList;
    private Context mContext;

    public GoogleMatchedPoiAdapter(List<AutocompletePrediction> poiList, Context context){
        mPoiList = poiList;
        mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout matchedPoiRelativeLayout;
        public TextView poiPlaceNameTextView, poiPlaceAddressTextView;

        public ViewHolder(View v) {
            super(v);
            matchedPoiRelativeLayout = (RelativeLayout) v.findViewById(R.id.matched_poi_list_relative_layout);
            poiPlaceNameTextView = (TextView) v.findViewById(R.id.poi_place_name_text_view);
            poiPlaceAddressTextView = (TextView) v.findViewById(R.id.poi_place_address_text_view);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.matched_poi_list_item, viewGroup, false);

        return new GoogleMatchedPoiAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final AutocompletePrediction matchedPlace = mPoiList.get(position);

        if(matchedPlace == null){
            return;
        }

        holder.poiPlaceNameTextView.setText(matchedPlace.getPrimaryText(null).toString());
        holder.poiPlaceAddressTextView.setText(matchedPlace.getSecondaryText(null).toString());
        holder.matchedPoiRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start navigation for selected place
                startNavigation(matchedPlace);
            }
        });
    }

    private void startNavigation(AutocompletePrediction autocompletePrediction) {
        ((MainActivity)mContext).mNavigateToOnVoiceResultReceived = false;
        ((MainActivity)mContext).startGoogleMapsNavigation(autocompletePrediction.getPrimaryText(null).toString() + "," + autocompletePrediction.getSecondaryText(null).toString());
        ((MainActivity)mContext).refreshFragments();
    }

    @Override
    public int getItemCount() {
        return mPoiList.size();
    }

}
