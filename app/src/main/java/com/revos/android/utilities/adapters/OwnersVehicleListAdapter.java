package com.revos.android.utilities.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.revos.android.R;
import com.revos.android.activities.VehiclesAndDevicesListActivity;
import com.revos.android.fragments.VehicleDetailsFragment;
import com.revos.android.jsonStructures.Vehicle;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.revos.android.fragments.VehicleDetailsFragment.VEHICLE_DETAILS_OBJECT;
import static com.revos.android.fragments.VehicleDetailsFragment.VEHICLE_PIN_KEY;

public class OwnersVehicleListAdapter extends RecyclerView.Adapter<OwnersVehicleListAdapter.ViewHolder> {

    private Context mContext;
    private List<Pair<Vehicle, String>> mOwnersVehiclesList;
    private HashMap<String, String> mModelImagesHashMap;

    public OwnersVehicleListAdapter(List<Pair<Vehicle, String>> vehiclesList, HashMap<String, String> modelImagesHashMap, Context context) {

        mContext = context;
        mOwnersVehiclesList = vehiclesList;
        mModelImagesHashMap = modelImagesHashMap;
    }

    @NonNull
    @Override
    public OwnersVehicleListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.owners_vehicle_list_item_layout, viewGroup, false);

        return new OwnersVehicleListAdapter.ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView vehicleVinTextView, vehicleModelNameTextView;
        CircleImageView vehicleModelCircleImageView;
        CardView vehicleDetailsItemCardView;
        LinearLayout vehicleDetailsItemLinearLayout;
        Button vehicleViewDetailsButton;

        public ViewHolder(@NonNull View view) {

            super(view);

            vehicleDetailsItemCardView = view.findViewById(R.id.owners_vehicle_list_item_card_view);
            vehicleModelCircleImageView = view.findViewById(R.id.owners_vehicle_list_item_vehicle_circle_image_view);
            vehicleVinTextView = view.findViewById(R.id.owners_vehicle_list_item_vehicle_vin_text_view);
            vehicleModelNameTextView = view.findViewById(R.id.owners_vehicles_list_item_vehicle_model_text_view);
            vehicleDetailsItemLinearLayout = view.findViewById(R.id.owners_vehicle_item_card_linear_layout);
            vehicleViewDetailsButton = view.findViewById(R.id.owners_vehicles_view_details_button);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull OwnersVehicleListAdapter.ViewHolder viewHolder, int position) {

        Pair<Vehicle, String> vehicleStringPair = mOwnersVehiclesList.get(position);

        Vehicle ownersVehiclesDetails =vehicleStringPair.first;
        String vehiclePin = vehicleStringPair.second;

        if(ownersVehiclesDetails.getVin() != null && !ownersVehiclesDetails.getVin().isEmpty()) {
            viewHolder.vehicleVinTextView.setText(ownersVehiclesDetails.getVin());
        } else {
            viewHolder.vehicleVinTextView.setText(mContext.getString(R.string.not_available));
        }

        if(ownersVehiclesDetails.getModel().getName() != null && !ownersVehiclesDetails.getModel().getName().isEmpty()) {
            viewHolder.vehicleModelNameTextView.setText(ownersVehiclesDetails.getModel().getName());
        } else {
            viewHolder.vehicleModelNameTextView.setText(mContext.getString(R.string.not_available));
        }


        //populate model image
        String modelId = ownersVehiclesDetails.getModel().getId();

        if(mModelImagesHashMap != null && !mModelImagesHashMap.isEmpty()) {

            String modelImageUri = mModelImagesHashMap.get(modelId);

            if(modelImageUri != null) {
                Glide.with(mContext)
                        .load(modelImageUri)
                        .fitCenter()
                        .into(viewHolder.vehicleModelCircleImageView);
            } else {
                viewHolder.vehicleModelCircleImageView.setImageResource(R.drawable.ic_scooter_front_view);
            }
        } else {
            viewHolder.vehicleModelCircleImageView.setImageResource(R.drawable.ic_scooter_front_view);
        }

        viewHolder.vehicleViewDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((VehiclesAndDevicesListActivity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        VehicleDetailsFragment vehicleDetailsFragment = new VehicleDetailsFragment();

                        Bundle bundle = new Bundle();
                        bundle.putString(VEHICLE_DETAILS_OBJECT, new Gson().toJson(ownersVehiclesDetails));
                        bundle.putString(VEHICLE_PIN_KEY, vehiclePin);
                        vehicleDetailsFragment.setArguments(bundle);

                        ((VehiclesAndDevicesListActivity)mContext).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(android.R.id.content, vehicleDetailsFragment)
                                .addToBackStack(null)
                                .commit();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOwnersVehiclesList.size();
    }

    public void updateImageHashMap(HashMap<String, String> modelImageHashMap) {
        mModelImagesHashMap = modelImageHashMap;
        notifyDataSetChanged();
    }
}
