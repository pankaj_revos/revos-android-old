package com.revos.android.utilities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.jsonStructures.KycAssociation;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class KycAssociationApprovedListAdapter extends RecyclerView.Adapter<KycAssociationApprovedListAdapter.ViewHolder> {

    private List<KycAssociation> mKycAssociationList;
    private Context mContext;

    public KycAssociationApprovedListAdapter(List<KycAssociation> kycAssociationList, Context context) {
        mKycAssociationList = kycAssociationList;
        mContext = context;
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {

        LinearLayout listItemLinearLayout;
        TextView providerNameTextView, kycAssociationStatusTextView;
        ImageView kycApprovedStatusImageView;

        public ViewHolder(@NonNull View v) {
            super(v);

            listItemLinearLayout = v.findViewById(R.id.kyc_association_approved_list_item_linear_layout);

            providerNameTextView = v.findViewById(R.id.kyc_association_approved_list_item_provider_name_value_text_view);
            kycAssociationStatusTextView = v.findViewById(R.id.kyc_association_approved_list_item_status_value_text_view);

            kycApprovedStatusImageView = v.findViewById(R.id.kyc_association_approved_list_item_status_image_view);
        }
    }

    @NonNull
    @Override
    public KycAssociationApprovedListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kyc_association_approved_status_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  KycAssociationApprovedListAdapter.ViewHolder viewHolder, int position) {

        KycAssociation kycAssociation = mKycAssociationList.get(position);

        viewHolder.providerNameTextView.setText(kycAssociation.getRentalProviderName());
        viewHolder.kycAssociationStatusTextView.setText(kycAssociation.getKycAssociationStatus().toString());
    }

    @Override
    public int getItemCount() {
        return mKycAssociationList.size();
    }
}
