package com.revos.android.utilities.general;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.IOException;
import java.io.InputStream;

import androidx.exifinterface.media.ExifInterface;

public class ImageUtils {
    //requires two bitmaps of same width and height
    public static double findBitmapPercentageDiff(Bitmap bmp1, Bitmap bmp2) {
        int width1 = bmp1.getWidth();
        int width2 = bmp2.getWidth();
        int height1 = bmp1.getHeight();
        int height2 = bmp2.getHeight();
        if(width1 != width2 || height1 != height2) {
            return 100.0D;
        } else {
            long diff = 0;
            for (int y = 0; y < height1; y++) {
                for (int x = 0; x < width1; x++) {
                    int argb1 = bmp1.getPixel(x, y);
                    int argb2 = bmp2.getPixel(x, y);
                    int a1 = (argb1 >> 24) & 0xff;
                    int r1 = (argb1 >> 16) & 0xff;
                    int g1 = (argb1 >>  8) & 0xff;
                    int b1 = (argb1      ) & 0xff;
                    int a2 = (argb2 >> 24) & 0xff;
                    int r2 = (argb2 >> 16) & 0xff;
                    int g2 = (argb2 >>  8) & 0xff;
                    int b2 = (argb2      ) & 0xff;
                    diff += Math.abs(a1 - a2);
                    diff += Math.abs(r1 - r2);
                    diff += Math.abs(g1 - g2);
                    diff += Math.abs(b1 - b2);
                }
            }
            double n = width1 * height1 * 3;
            double p = diff / (n * 255.0);
            return p * 100.0D;
        }
    }

    public static boolean isBitmapSymmetricAtBaseline(Bitmap bitmap) {
        if(bitmap.getWidth() == 0 || bitmap.getHeight() == 0) {
            return true;
        }

        int xMinAtYMax = bitmap.getWidth();
        int xMaxAtYMax = 0;

        //process the bitmap first
        for(int x = 0; x < bitmap.getWidth(); ++x) {
            int pixel = bitmap.getPixel(x, bitmap.getHeight() - 1);

            if(Color.alpha(pixel) > 0) {

                if(x <= xMinAtYMax) {
                    xMinAtYMax = x;
                }

                if(x > xMaxAtYMax) {
                    xMaxAtYMax = x;
                }
            }
        }

        if(xMinAtYMax + xMaxAtYMax == bitmap.getWidth() - 1) {
            return true;
        } else {
            return false;
        }
    }

    public static Bitmap createScaledAndProcessedBitmap(Bitmap sourceBitmap, int width, int height) {
        Bitmap scaledAndBinaryAlphaBitmap = Bitmap.createScaledBitmap(sourceBitmap, width, height, false);
        scaledAndBinaryAlphaBitmap.setHasAlpha(true);

        //process the bitmap first
        for(int x = 0; x < scaledAndBinaryAlphaBitmap.getWidth(); ++x) {
            for(int y = 0; y < scaledAndBinaryAlphaBitmap.getHeight(); ++y) {
                int pixel = scaledAndBinaryAlphaBitmap.getPixel(x,y);

                //if the alpha is not 255 then set the alpha to zero
                if(Color.alpha(pixel) != 0xFF) {
                    scaledAndBinaryAlphaBitmap.setPixel(x,y, 0x00000000);
                } else {
                    scaledAndBinaryAlphaBitmap.setPixel(x,y, 0xFF000000);
                }
            }
        }

        return scaledAndBinaryAlphaBitmap;
    }

    public static void copyExif(InputStream originalInputStream, String newFilePath) throws IOException {

        String[] attributes = new String[]
                {
                        ExifInterface.TAG_DATETIME,
                        ExifInterface.TAG_DATETIME_DIGITIZED,
                        ExifInterface.TAG_EXPOSURE_TIME,
                        ExifInterface.TAG_FLASH,
                        ExifInterface.TAG_FOCAL_LENGTH,
                        ExifInterface.TAG_GPS_ALTITUDE,
                        ExifInterface.TAG_GPS_ALTITUDE_REF,
                        ExifInterface.TAG_GPS_DATESTAMP,
                        ExifInterface.TAG_GPS_LATITUDE,
                        ExifInterface.TAG_GPS_LATITUDE_REF,
                        ExifInterface.TAG_GPS_LONGITUDE,
                        ExifInterface.TAG_GPS_LONGITUDE_REF,
                        ExifInterface.TAG_GPS_PROCESSING_METHOD,
                        ExifInterface.TAG_GPS_TIMESTAMP,
                        ExifInterface.TAG_MAKE,
                        ExifInterface.TAG_MODEL,
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.TAG_SUBSEC_TIME,
                        ExifInterface.TAG_WHITE_BALANCE
                };

        ExifInterface oldExif = new ExifInterface(originalInputStream);
        ExifInterface newExif = new ExifInterface(newFilePath);

        if (attributes.length > 0) {
            for (int i = 0; i < attributes.length; i++) {
                String value = oldExif.getAttribute(attributes[i]);
                if (value != null)
                    newExif.setAttribute(attributes[i], value);
            }
            newExif.saveAttributes();
        }
    }

}
