package com.revos.android.utilities.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.activities.PassbookTransactionsListActivity;
import com.revos.android.jsonStructures.Invoice;
import com.revos.android.jsonStructures.PassbookTxn;
import com.revos.android.utilities.general.DateTimeUtils;
import com.revos.scripts.type.PassbookTxStatus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.revos.android.activities.PassbookTransactionsListActivity.RENTAL_INVOICE_ID;

public class RentalInvoiceInfoAdapter extends RecyclerView.Adapter<RentalInvoiceInfoAdapter.ViewHolder> {

    private List<Invoice> mRentalInvoicesList;
    private Context mContext;

    public RentalInvoiceInfoAdapter(List<Invoice> rentalInvoicesList, Context context) {
        mRentalInvoicesList = rentalInvoicesList;
        mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout listItemLinearLayout;
        TextView invoiceNumberTextView, invoiceCreationDateTextView, invoiceAmountTextView,
                 invoiceStatusTextView;
        Button viewDetailsButton;
        ImageView invoiceStatusImageView;

        public ViewHolder(@NonNull View view) {

            super(view);

            listItemLinearLayout = view.findViewById(R.id.rental_invoice_list_item_linear_layout);

            invoiceNumberTextView = view.findViewById(R.id.rental_invoice_list_item_invoice_number_value_text_view);
            invoiceCreationDateTextView = view.findViewById(R.id.rental_invoice_list_item_created_at_date_value_text_view);
            invoiceAmountTextView = view.findViewById(R.id.rental_invoice_list_item_amount_value_text_view);
            invoiceStatusTextView = view.findViewById(R.id.rental_invoice_list_item_payment_status_value_text_view);
            invoiceStatusImageView = view.findViewById(R.id.rental_invoice_list_item_status_image_view);
            viewDetailsButton = view.findViewById(R.id.rental_invoice_item_list_view_details_button);
        }
    }

    @NonNull
    @Override
    public RentalInvoiceInfoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rental_invoice_info_list_item_view, viewGroup, false);

        return new RentalInvoiceInfoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RentalInvoiceInfoAdapter.ViewHolder viewHolder, int position) {

        Invoice rentalInvoiceReceiptObject = mRentalInvoicesList.get(position);

        if(rentalInvoiceReceiptObject.getInvoiceNumber() != null && !rentalInvoiceReceiptObject.getInvoiceNumber().isEmpty()) {
            viewHolder.invoiceNumberTextView.setText(rentalInvoiceReceiptObject.getInvoiceNumber());
        } else {
            viewHolder.invoiceNumberTextView.setText(mContext.getString(R.string.not_available));
        }

        if(rentalInvoiceReceiptObject.getInvoiceCreationDate() != null && !rentalInvoiceReceiptObject.getInvoiceCreationDate().isEmpty()) {
            viewHolder.invoiceCreationDateTextView.setText(DateTimeUtils.getInstance(mContext).getDateFromTimeStamp(rentalInvoiceReceiptObject.getInvoiceCreationDate()));
        } else {
            viewHolder.invoiceCreationDateTextView.setText(mContext.getString(R.string.not_available));
        }

        float totalAmountPayable = 0.0f;
        try {
            if (rentalInvoiceReceiptObject.getPassbooksList() != null && rentalInvoiceReceiptObject.getPassbooksList().size() > 0) {
                for (int index = 0; index < rentalInvoiceReceiptObject.getPassbooksList().size(); index++) {
                    PassbookTxn passbook = rentalInvoiceReceiptObject.getPassbooksList().get(index);
                    totalAmountPayable += Float.parseFloat(passbook.getAmount());
                }
            }
            viewHolder.invoiceAmountTextView.setText(String.format("%s%s", mContext.getString(R.string.rupee), String.valueOf(totalAmountPayable)));
        } catch (Exception e) {
            viewHolder.invoiceAmountTextView.setText(mContext.getString(R.string.not_available));
        }

        String invoiceStatus = mContext.getString(R.string.invoice_status_paid);
        if(rentalInvoiceReceiptObject.getPassbooksList() != null && rentalInvoiceReceiptObject.getPassbooksList().size() > 0) {
            for(int index = 0 ; index < rentalInvoiceReceiptObject.getPassbooksList().size() ; index++) {
                PassbookTxn passbook = rentalInvoiceReceiptObject.getPassbooksList().get(index);
                if(passbook.getPassbookTxStatus().equals(PassbookTxStatus.PENDING)) {
                    invoiceStatus = mContext.getString(R.string.invoice_status_pending);
                    break;
                }
            }
        }

        viewHolder.invoiceStatusTextView.setText(String.valueOf(invoiceStatus));
        if(invoiceStatus.equals(PassbookTxStatus.COMPLETED.toString()) || invoiceStatus.equals(PassbookTxStatus.PAID.toString())) {
            viewHolder.invoiceStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.revos_green));
            viewHolder.invoiceStatusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_revos_check));
        } else {
            viewHolder.invoiceStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.color_critical_error_background));
            viewHolder.invoiceStatusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_error_icon));
        }

        viewHolder.listItemLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString(RENTAL_INVOICE_ID, rentalInvoiceReceiptObject.getInvoiceId());

                Intent intent = new Intent(mContext, PassbookTransactionsListActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

        viewHolder.viewDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString(RENTAL_INVOICE_ID, rentalInvoiceReceiptObject.getInvoiceId());

                Intent intent = new Intent(mContext, PassbookTransactionsListActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mRentalInvoicesList.size();
    }
}