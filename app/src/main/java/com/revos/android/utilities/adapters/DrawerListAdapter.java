package com.revos.android.utilities.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.revos.android.R;

import java.util.ArrayList;

/**
 * Created by mohit on 31/3/17.
 */

public class DrawerListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> itemNames;

    //view holder class for our drawer
    private static class DrawerViewHolder {
        private TextView itemTextView;
    }


    public DrawerListAdapter(Activity context, ArrayList<String> itemName) {
        super(context, R.layout.drawer_list_view_row, itemName);

        this.context = context;
        this.itemNames = itemName;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        // reuse views
        if(rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.drawer_list_view_row, null);
            // configure view holder
            DrawerViewHolder viewHolder = new DrawerViewHolder();
            viewHolder.itemTextView = (TextView) rowView.findViewById(R.id.row_text);
            rowView.setTag(viewHolder);
        }

        // fill data
        DrawerViewHolder holder = (DrawerViewHolder) rowView.getTag();

        holder.itemTextView.setText(itemNames.get(position));

        return rowView;
    }
}
