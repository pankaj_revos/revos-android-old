/*
 * Copyright (C) 2015 Pedramrn@gmail.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.revos.android.utilities.progressBars;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.revos.android.R;

/**
 * A subclass of {@link View} class for creating a custom circular progressBar
 *
 * Created by Pedram on 2015-01-06.
 */
public class DirectionProgressBar extends View {

    /**ProgressBar's direction of progress*/
    private boolean isDirectionClockWise = true;

    /**Are we drawing a directional arc*/
    private boolean drawDirectionalArc = false;


    /**
     * ProgressBar's line thickness
     */
    private float strokeWidth = 4;
    private float progress = 0;
    private int min = 0;
    private int max = 100;

    public float getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(float startAngle) {
        this.startAngle = startAngle;
    }

    /**
     * Start the progress at 12 o'clock
     */
    private float startAngle = 0;
    private int progressBarColor, rimColor;
    private RectF rectF, paddedRect;
    private Paint backgroundPaint;
    private Paint foregroundPaint;
    private Bitmap arrowBitmap;

    //todo::ensure all these values are converted to pixels according to screen density

    public float getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        backgroundPaint.setStrokeWidth(strokeWidth);
        foregroundPaint.setStrokeWidth(strokeWidth);
        invalidate();
        requestLayout();//Because it should recalculate its bounds
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
        invalidate();
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
        invalidate();
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
        invalidate();
    }

    public DirectionProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        isDirectionClockWise = true;
        rectF = new RectF();
        paddedRect = new RectF();

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DirectionProgressBar,
                0, 0);
        //Reading values from the XML layout
        try {
            strokeWidth = typedArray.getDimension(R.styleable.DirectionProgressBar_dpb_progressBarThickness, strokeWidth);
            progress = typedArray.getFloat(R.styleable.DirectionProgressBar_dpb_progress, progress);
            progressBarColor = typedArray.getInt(R.styleable.DirectionProgressBar_dpb_progressbarColor, progressBarColor);
            rimColor = typedArray.getInt(R.styleable.DirectionProgressBar_dpb_rimColor, rimColor);
            min = typedArray.getInt(R.styleable.DirectionProgressBar_dpb_min, min);
            max = typedArray.getInt(R.styleable.DirectionProgressBar_dpb_max, max);
            startAngle = typedArray.getFloat(R.styleable.DirectionProgressBar_dpb_startAngle, startAngle);
            int indicatorDrawableId = typedArray.getResourceId(R.styleable.DirectionProgressBar_dpb_indicatorDrawable, 0);
            arrowBitmap = BitmapFactory.decodeResource(getResources(), indicatorDrawableId);
        } finally {
            typedArray.recycle();
        }

        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(rimColor);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setStrokeWidth(strokeWidth);

        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(progressBarColor);
        foregroundPaint.setStyle(Paint.Style.STROKE);
        foregroundPaint.setStrokeWidth(strokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawOval(paddedRect, backgroundPaint);
        if(progress == 0) {
            //we have nothing to draw except for the skeleton, return
            return;
        }

        float angle = 360 * progress / max;
        if(!drawDirectionalArc) {
            if(isDirectionClockWise) {
                canvas.drawArc(paddedRect, startAngle, angle - 4, false, foregroundPaint);
            } else {
                canvas.drawArc(paddedRect, startAngle, 4 - angle, false, foregroundPaint);
            }
        } else {
            canvas.drawArc(paddedRect, startAngle, angle, false, foregroundPaint);
        }

        float radius = Math.min(paddedRect.width(), paddedRect.height())/2;
        if(drawDirectionalArc) {
            radius += 50;
        }

        float cx = getWidth() / 2f;
        float cy = getHeight() / 2f;

        float endAngle = isDirectionClockWise ? startAngle + angle : startAngle - angle;

        if(drawDirectionalArc) {
            endAngle = startAngle + angle / 2;
        }

        double endAngleInRadian = Math.toRadians(endAngle);

        double stopX;
        double stopY;

        stopX = cx + (radius * Math.cos(endAngleInRadian) - arrowBitmap.getWidth() / 2);
        stopY = cy + (radius * Math.sin(endAngleInRadian) - arrowBitmap.getHeight() / 2);

        Matrix rotateMatrix = new Matrix();
        float bitmapRotation = isDirectionClockWise ? 180 + endAngle : endAngle;
        if(drawDirectionalArc) {
            bitmapRotation = 90 + endAngle;
        }

        rotateMatrix.postRotate(bitmapRotation, arrowBitmap.getWidth()/2, arrowBitmap.getHeight()/2);
        rotateMatrix.postTranslate((float)stopX, (float)stopY);

        canvas.drawBitmap(arrowBitmap, rotateMatrix, null);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);
        setMeasuredDimension(min, min);
        rectF.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2);
        paddedRect.set( 0 + strokeWidth / 2 + getPaddingLeft(), 0 + strokeWidth / 2 + getPaddingTop(),
                        min - strokeWidth / 2 - getPaddingRight(), min - strokeWidth / 2 - getPaddingBottom());
    }

    /**
     * Lighten the given color by the factor
     *
     * @param color  The color to lighten
     * @param factor 0 to 4
     * @return A brighter color
     */
    public int lightenColor(int color, float factor) {
        float r = Color.red(color) * factor;
        float g = Color.green(color) * factor;
        float b = Color.blue(color) * factor;
        int ir = Math.min(255, (int) r);
        int ig = Math.min(255, (int) g);
        int ib = Math.min(255, (int) b);
        int ia = Color.alpha(color);
        return (Color.argb(ia, ir, ig, ib));
    }

    /**
     * Transparent the given color by the factor
     * The more the factor closer to zero the more the color gets transparent
     *
     * @param color  The color to transparent
     * @param factor 1.0f to 0.0f
     * @return int - A transplanted color
     */
    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link ObjectAnimator} Class automatically set the progress
     * so don't call the {@link DirectionProgressBar#setProgress(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     */
    public void setProgressWithAnimation(float progress, long duration, boolean isDirectionClockWise) {

        this.isDirectionClockWise = isDirectionClockWise;
        this.drawDirectionalArc = false;

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress);
        objectAnimator.setDuration(duration);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();
    }

    /**position refers to one of the eight possible positions starting at 0 degrees in line with x-axis*/
    public void drawDirectionalArc(int position) {
        this.drawDirectionalArc = true;

        startAngle = position * 45 - 22.5f;
        progress = 12.5f;

        invalidate();
    }
}