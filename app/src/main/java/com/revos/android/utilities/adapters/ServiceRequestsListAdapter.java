package com.revos.android.utilities.adapters;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revos.android.R;
import com.revos.android.activities.ServiceRequestActivity;
import com.revos.android.fragments.ServiceRequestDetailsFragment;
import com.revos.scripts.ListServiceRequestQuery;


import static com.revos.android.fragments.ServiceRequestDetailsFragment.SERVICE_REQUEST_ID;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ServiceRequestsListAdapter extends RecyclerView.Adapter<ServiceRequestsListAdapter.ViewHolder> {

    private List<ListServiceRequestQuery.GetAll> mServiceRequestList;
    private Context mContext;

    public ServiceRequestsListAdapter(List<ListServiceRequestQuery.GetAll> serviceRequestList, Context context) {
        mServiceRequestList = serviceRequestList;
        mContext = context;
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{

        LinearLayout listItemLinearLayout, listItemStatusLinearLayout;
        TextView ticketNumberTextView, ticketCreationDateTextView, ticketTitleTextView, ticketDescTextView, ticketStatusTextView;

        public ViewHolder(@NonNull View v) {

            super(v);

            listItemLinearLayout = (LinearLayout) v.findViewById(R.id.service_request_list_item_linear_layout);
            listItemStatusLinearLayout = (LinearLayout) v.findViewById(R.id.service_request_item_list_status_linear_layout);

            ticketNumberTextView = (TextView) v.findViewById(R.id.service_request_list_item_ticket_id_value_text_view);
            ticketCreationDateTextView = (TextView) v.findViewById(R.id.service_request_list_item_date_text_view);
            ticketTitleTextView = (TextView) v.findViewById(R.id.service_request_list_item_title_value_text_view);
            ticketDescTextView = (TextView) v.findViewById(R.id.service_request_list_item_description_value_text_view);
            ticketStatusTextView = (TextView) v.findViewById(R.id.service_request_item_list_status_value_text_view);
        }
    }

    @NonNull
    @Override
    public ServiceRequestsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_request_list_item, parent, false);

        return new ServiceRequestsListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        ListServiceRequestQuery.GetAll serviceRequestObject = mServiceRequestList.get(position);

        String serviceRequestNumber = serviceRequestObject.number();
        String serviceRequestDate = serviceRequestObject.createdAt();
        String serviceRequestTitle = serviceRequestObject.title();
        String serviceRequestDesc = serviceRequestObject.description();
        String serviceRequestStatus = serviceRequestObject.status().toString();

        //convert Service Request date to dd/MM/yy format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(serviceRequestDate);
        } catch (ParseException e) {
            //todo::handle exception
        }
        Format formatter = new SimpleDateFormat("dd/MM/yy");
        String serviceTicketCreationDate = formatter.format(date);

        viewHolder.ticketNumberTextView.setText(serviceRequestNumber);
        viewHolder.ticketCreationDateTextView.setText(serviceTicketCreationDate);
        viewHolder.ticketTitleTextView.setText(serviceRequestTitle);
        viewHolder.ticketDescTextView.setText(serviceRequestDesc);

        switch (serviceRequestStatus) {
            case "RAISED" : viewHolder.ticketStatusTextView.setText(R.string.service_request_status_raised);
                viewHolder.listItemStatusLinearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.capsule_background_status_raised));
                break;

            case "PENDING" : viewHolder.ticketStatusTextView.setText(R.string.service_request_status_pending);
                viewHolder.listItemStatusLinearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.capsule_background_status_pending));
                break;

            case "INPROGRESS" : viewHolder.ticketStatusTextView.setText(R.string.service_request_status_in_progress);
                viewHolder.listItemStatusLinearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.capsule_background_status_inprogress));
                break;

            case "COMPLETED" : viewHolder.ticketStatusTextView.setText(R.string.service_request_status_completed);
                viewHolder.listItemStatusLinearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.capsule_background_status_completed));
                break;

            case "CLOSED" : viewHolder.ticketStatusTextView.setText(R.string.service_request_status_closed);
                viewHolder.listItemStatusLinearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.capsule_background_status_closed));
                break;
        }

        viewHolder.listItemLinearLayout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Fragment mServiceRequestDetailsFragment = new ServiceRequestDetailsFragment();

                Bundle arguments = new Bundle();
                arguments.putString(SERVICE_REQUEST_ID, serviceRequestObject.id());

                mServiceRequestDetailsFragment.setArguments(arguments);

                ((ServiceRequestActivity)mContext).getSupportFragmentManager().beginTransaction()
                        .replace(android.R.id.content, mServiceRequestDetailsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {

        return mServiceRequestList.size();
    }
}