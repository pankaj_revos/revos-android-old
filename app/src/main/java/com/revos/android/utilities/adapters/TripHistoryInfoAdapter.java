package com.revos.android.utilities.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.revos.android.R;
import com.revos.android.activities.MainActivity;
import com.revos.android.activities.TripPlayBackActivity;
import com.revos.android.jsonStructures.DataFeedBatteryVoltageAdc;
import com.revos.android.jsonStructures.Device;
import com.revos.android.jsonStructures.DataFeedAllTripsLocation;
import com.revos.android.jsonStructures.Model;
import com.revos.android.jsonStructures.TripDataFeed;
import com.revos.android.jsonStructures.TripModeDataFeed;
import com.revos.android.jsonStructures.Vehicle;
import com.revos.android.utilities.general.PrefUtils;
import com.revos.scripts.type.ModelProtocol;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.revos.android.activities.TripPlayBackActivity.ARG_END_TIME;
import static com.revos.android.activities.TripPlayBackActivity.ARG_START_TIME;
import static com.revos.android.activities.TripPlayBackActivity.ARG_TRIP_ID;
import static com.revos.android.activities.TripPlayBackActivity.ARG_VIN;

public class TripHistoryInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<TripDataFeed> mTripInformationList;
    private Context mContext;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public TripHistoryInfoAdapter(ArrayList<TripDataFeed> tripInfoList, Context context) {

        mTripInformationList = tripInfoList;
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.trip_history_info_list_item_view, viewGroup, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.loading_layout, viewGroup, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ItemViewHolder) {

            populateItemRows((ItemViewHolder) viewHolder, position);

        } else if (viewHolder instanceof LoadingViewHolder) {

            showLoadingView((LoadingViewHolder) viewHolder, position);

        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback{

        MapView googleMapView;
        GoogleMap mGoogleMap;

        ImageView playImageView;

        LinearLayout tripListItemLinearLayout, tripDateTimeDurationLinearLayout, tripMileageLinearLayout,
                     tripExpandedInfoLinearLayout, tripIdLinearLayout, tripExpandDetailsCardLinearLayout,
                     tripCollapseDetailsCardLinearLayout, tripAvgSpeedLinearLayout, noLatLngFoundLinearLayout,
                     tripEnergyConsumedLinearLayout, tripModeLinearLayout, tripBatteryConsumptionLinearLayout;

        TextView tripIdTextView, tripDateTimeTextView, tripDurationTextView, tripDistanceTextView,
                 tripBatteryConsumptionTextView, tripMileageTextView, tripMaxSpeedTextView,
                 tripEnergyConsumedTextView, tripModeTextView, tripAvgSpeedTextView,
                 tripEconomyModeValueTextView, tripRideModeValueTextView, tripSportModeValueTextView,
                 tripExpandMoreDetailsTextView, tripCollapseDetailsTextView;

        ImageView    tripExpandDetailsCardImageView, tripCollapseDetailsCardImageView;

        PieChart     pieChartForModeUsage;

        private ArrayList<LatLng> latLngArrayList;


        public ItemViewHolder(@NonNull View view) {

            super(view);

            //linear layouts
            tripListItemLinearLayout = view.findViewById(R.id.trip_list_item_linear_layout);
            noLatLngFoundLinearLayout = view.findViewById(R.id.th_no_latlng_data_found_linear_layout);
            tripDateTimeDurationLinearLayout = view.findViewById(R.id.th_journey_date_time_duration_linear_layout);
            tripBatteryConsumptionLinearLayout = view.findViewById(R.id.th_battery_consumption_linear_layout);
            tripMileageLinearLayout = view.findViewById(R.id.th_mileage_linear_layout);
            tripAvgSpeedLinearLayout = view.findViewById(R.id.th_avg_speed_linear_layout);
            tripExpandedInfoLinearLayout = view.findViewById(R.id.th_expanded_info_linear_layout);
            tripIdLinearLayout = view.findViewById(R.id.th_trip_id_linear_layout);
            tripExpandDetailsCardLinearLayout = view.findViewById(R.id.th_expand_card_linear_layout);
            tripCollapseDetailsCardLinearLayout = view.findViewById(R.id.th_collapse_card_linear_layout);
            tripEnergyConsumedLinearLayout = view.findViewById(R.id.th_energy_consumed_linear_layout);
            tripModeLinearLayout = view.findViewById(R.id.th_mode_linear_layout);

            //map view
            googleMapView = (MapView) view.findViewById(R.id.th_journey_map_view_lite);

            //text views
            tripIdTextView = view.findViewById(R.id.trip_id_value_text_view);
            tripDateTimeTextView = view.findViewById(R.id.th_journey_date_time_text_view);
            tripDurationTextView = view.findViewById(R.id.th_duration_text_view);
            tripDistanceTextView = view.findViewById(R.id.trip_distance_value_text_view);
            tripBatteryConsumptionTextView = view.findViewById(R.id.trip_battery_consumption_value_text_view);
            tripMileageTextView = view.findViewById(R.id.trip_mileage_value_text_view);
            tripAvgSpeedTextView = view.findViewById(R.id.th_avg_speed_value_text_view);
            tripMaxSpeedTextView = view.findViewById(R.id.th_max_speed_value_text_view);
            tripEnergyConsumedTextView = view.findViewById(R.id.trip_energy_consumed_value_text_view);
            tripModeTextView = view.findViewById(R.id.th_mode_text_view);
            tripEconomyModeValueTextView = view.findViewById(R.id.th_economy_mode_usage_value);
            tripRideModeValueTextView = view.findViewById(R.id.th_ride_mode_usage_value);
            tripSportModeValueTextView = view.findViewById(R.id.th_sport_mode_usage_value);
            tripExpandMoreDetailsTextView = view.findViewById(R.id.th_expand_more_details_text_view);
            tripCollapseDetailsTextView = view.findViewById(R.id.th_collapse_more_details_text_view);

            //image view
            tripExpandDetailsCardImageView = view.findViewById(R.id.th_expand_card_image_view);
            tripCollapseDetailsCardImageView = view.findViewById(R.id.th_collapse_card_image_view);

            //pie chart
            pieChartForModeUsage = view.findViewById(R.id.th_ride_mode_distribution_pie_chart);

            //button
            playImageView = view.findViewById(R.id.th_playback_image_view);

            if (googleMapView != null) {

                // Initialise the MapView
                googleMapView.onCreate(null);

                googleMapView.onResume();

                // Set the map ready callback to receive the GoogleMap object
                googleMapView.getMapAsync(this);
            }

            //hide mileage view if protocol is IOT
            Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();
            if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {

                tripMileageLinearLayout.setVisibility(View.GONE);
                tripBatteryConsumptionLinearLayout.setVisibility(View.VISIBLE);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, R.id.th_battery_consumption_linear_layout);
                params.setMargins(convertDipToPixels(20), convertDipToPixels(5), convertDipToPixels(20), convertDipToPixels(4));
                tripAvgSpeedLinearLayout.setLayoutParams(params);

            } else {

                tripBatteryConsumptionLinearLayout.setVisibility(View.GONE);
                tripMileageLinearLayout.setVisibility(View.VISIBLE);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, R.id.th_mileage_linear_layout);
                params.setMargins(convertDipToPixels(20), convertDipToPixels(5), convertDipToPixels(20), convertDipToPixels(4));
                tripAvgSpeedLinearLayout.setLayoutParams(params);
            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {

            MapsInitializer.initialize(mContext);
            mGoogleMap = googleMap;

            if(latLngArrayList == null || latLngArrayList.isEmpty()) {
                return;
            }

            updateMap(mGoogleMap, latLngArrayList);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.loading_layout_progress_bar);
        }
    }


    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {

        TripDataFeed tripItem = mTripInformationList.get(position);

        Device device= PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs();
        int speedDivisor = device.getVehicle().getModel().getSpeedDivisor();
        Vehicle vehicle = PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs();

        //trip info details
        float distanceInMeters = (float) tripItem.getDistance();
        float distanceInKilometers = (float) tripItem.getDistance()/1000;
        float energy = 0, mileage = 0;
        double batteryPercentage = 0;


        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            List<DataFeedBatteryVoltageAdc> batteryVoltageADC = tripItem.getAdcVoltageArrayList();
            double batteryTripEnd = batteryVoltageADC.get(0).getVoltage();
            double batteryTripStart  = batteryVoltageADC.get(batteryVoltageADC.size() - 1).getVoltage();
            Model model = PrefUtils.getInstance(mContext).getDeviceObjectFromPrefs().getVehicle().getModel();

            if(model != null && model.getBatteryMaxVoltageLimit() != 0 && model.getBatteryMinVoltageLimit() != 0) {
                batteryPercentage = ((batteryTripStart - batteryTripEnd) / (model.getBatteryMaxVoltageLimit() - model.getBatteryMinVoltageLimit())) * 100;
            }
        } else {
            energy = (float) tripItem.getEnergy();
            mileage = 0.0f;
            if(energy > 0) {
                mileage = distanceInKilometers/energy;
            }
        }

        long maxSpeed = (long) Math.floor(tripItem.getMaxWheelRPM()/speedDivisor);

        //use gps speed if vehicle is of PNP type
        if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
            maxSpeed = Math.round(tripItem.getMaxGpsSpeed());
        }

        TripModeDataFeed tripMode = tripItem.getMode();
        double economyMode = 0, rideMode = 0, sportMode = 0;
        long economyModeValue = 0, rideModeValue = 0, sportModeValue = 0;
        if(tripMode != null) {
            economyMode = tripMode.getECONOMY();
            rideMode = tripMode.getRIDE();
            sportMode = tripMode.getSPORT();
            economyModeValue = (long) Math.floor(economyMode);
            rideModeValue = (long) Math.floor(rideMode);
            sportModeValue = 100 - (economyModeValue + rideModeValue);
        }

        //set trip date and time
        String dateFormat = "dd-MMM-yy";
        String timeFormat = "HH:mm:ss";

        DateTime tripStartDateTime = new DateTime(tripItem.getStartTime());
        Date tripStartDate = tripStartDateTime.toDate();

        String tripDate = new SimpleDateFormat(dateFormat).format(tripStartDate);
        String tripStartTime = new SimpleDateFormat(timeFormat).format(tripStartDate);

        //calculate trip duration
        DateTime tripEndDateTime = new DateTime(tripItem.getEndTime());
        Date tripEndTime = tripEndDateTime.toDate();

        long diff = tripEndTime.getTime() - tripStartDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        String tripDuration = null;
        if(diffHours <= 0 && diffMinutes <= 0 && diffSeconds > 0) {
            tripDuration = diffSeconds + mContext.getString(R.string.seconds);
        } else if(diffHours <= 0 && diffMinutes > 0) {
            tripDuration = diffMinutes + mContext.getString(R.string.minutes) + " "
                    + diffSeconds + mContext.getString(R.string.seconds);
        } else if(diffHours > 0) {
            tripDuration = diffHours + mContext.getString(R.string.hours) + " "
                    + diffMinutes + mContext.getString(R.string.minutes) + " "
                    + diffSeconds + mContext.getString(R.string.seconds);
        }

        double avgSpeed = tripItem.getAvgGpsSpeed();

        //set a pie graph for showing mode usage
        ArrayList<PieEntry> modeUsageArrayList = new ArrayList<PieEntry>();
        modeUsageArrayList.add(new PieEntry((float)economyMode, ""));
        modeUsageArrayList.add(new PieEntry((float)rideMode, ""));
        modeUsageArrayList.add(new PieEntry((float)sportMode, ""));
        PieDataSet pieDataSet = new PieDataSet(modeUsageArrayList, "");

        pieDataSet.setDrawValues(false);

        PieData pieData = new PieData(pieDataSet);
        viewHolder.pieChartForModeUsage.setData(pieData);

        //custom colors for pie chart
        final int[] pieChartColors = { ContextCompat.getColor(mContext, R.color.revos_green),
                ContextCompat.getColor(mContext, R.color.color_ride_mode),
                ContextCompat.getColor(mContext, R.color.color_ic_battery_low_light)};
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for(int color: pieChartColors) colors.add(color);
        pieDataSet.setColors(colors);       //set custom colors

        viewHolder.pieChartForModeUsage.setHoleRadius(85f);
        viewHolder.pieChartForModeUsage.getDescription().setEnabled(false);
        viewHolder.pieChartForModeUsage.getLegend().setEnabled(false);
        viewHolder.pieChartForModeUsage.setTransparentCircleAlpha(30);
        viewHolder.pieChartForModeUsage.setNoDataText(mContext.getString(R.string.no_data_text_mode_value_chart));
        viewHolder.pieChartForModeUsage.setDrawHoleEnabled(true);

        //set pie chart center hole color as per app theme
        if(MainActivity.mCurrentThemeID != R.style.AppFullScreenThemeDark) {
            viewHolder.pieChartForModeUsage.setHoleColor(ContextCompat.getColor(mContext, R.color.color_card_background_light));
        } else {
            viewHolder.pieChartForModeUsage.setHoleColor(ContextCompat.getColor(mContext, R.color.color_card_background_dark));
        }

        viewHolder.pieChartForModeUsage.animateXY(1000, 1000);
        viewHolder.pieChartForModeUsage.invalidate();

        //populate views
        viewHolder.tripIdTextView.setText(tripItem.getTripId());
        viewHolder.tripDateTimeTextView.setText(tripDate + " | " + tripStartTime);
        viewHolder.tripDurationTextView.setText(tripDuration);
        if(distanceInMeters >= 1000) {
            viewHolder.tripDistanceTextView.setText(String.format("%.2f ", distanceInKilometers) + mContext.getString(R.string.kilometers));
        } else {
            viewHolder.tripDistanceTextView.setText(String.format("%.0f ", distanceInMeters) + mContext.getString(R.string.meters));
        }
        viewHolder.tripMileageTextView.setText(String.format("%.1f", mileage) + " km/kWh");
        viewHolder.tripAvgSpeedTextView.setText(String.format("%.1f", avgSpeed) + " km/h");
        viewHolder.tripBatteryConsumptionTextView.setText(String.format("%.1f", batteryPercentage) + "%");
        viewHolder.tripEnergyConsumedTextView.setText(String.format("%.2f", energy) + " kWh");
        viewHolder.tripEconomyModeValueTextView.setText(String.valueOf(economyModeValue) + "%");
        viewHolder.tripRideModeValueTextView.setText(String.valueOf(rideModeValue) + "%");
        viewHolder.tripSportModeValueTextView.setText(String.valueOf(sportModeValue) + "%");
        viewHolder.tripMaxSpeedTextView.setText(String.valueOf(maxSpeed) + " km/h");

        viewHolder.latLngArrayList = new ArrayList<>();
        if(tripItem.getLocation() != null && !tripItem.getLocation().isEmpty()) {

            for(int index = 0 ; index < tripItem.getLocation().size() ; index++) {
                DataFeedAllTripsLocation fetchedLatLng = tripItem.getLocation().get(index);

                if(fetchedLatLng != null) {
                    LatLng latLng = new LatLng(fetchedLatLng.getLat(), fetchedLatLng.getLng());
                    viewHolder.latLngArrayList.add(latLng);
                }
            }
        }

        //hide mapView if latLngArrayList is empty
        if(viewHolder.latLngArrayList == null || viewHolder.latLngArrayList.isEmpty()) {
            viewHolder.googleMapView.setVisibility(View.GONE);
            viewHolder.noLatLngFoundLinearLayout.setVisibility(View.VISIBLE);

            //set trip duration below th_no_latlng_data_found_text_view
            RelativeLayout.LayoutParams tripDateTimeParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            tripDateTimeParams.addRule(RelativeLayout.BELOW, R.id.th_no_latlng_data_found_linear_layout);
            viewHolder.tripDateTimeDurationLinearLayout.setLayoutParams(tripDateTimeParams);

            //set trip id above th_no_latlng_data_found_text_view
            RelativeLayout.LayoutParams params= new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.BELOW, R.id.th_trip_id_linear_layout);
            viewHolder.noLatLngFoundLinearLayout.setLayoutParams(params);

            viewHolder.playImageView.setVisibility(View.INVISIBLE);

        } else {
            viewHolder.googleMapView.setVisibility(View.VISIBLE);
            viewHolder.noLatLngFoundLinearLayout.setVisibility(View.GONE);

            RelativeLayout.LayoutParams params= new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.BELOW, R.id.th_journey_map_view_lite);
            viewHolder.tripDateTimeDurationLinearLayout.setLayoutParams(params);

            if(viewHolder.mGoogleMap != null) {
                updateMap(viewHolder.mGoogleMap, viewHolder.latLngArrayList);
            }
        }

        //expand/collapse trip extra details
        viewHolder.tripListItemLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.tripExpandedInfoLinearLayout.getVisibility() == View.VISIBLE) {
                    viewHolder.tripExpandDetailsCardLinearLayout.setVisibility(View.VISIBLE);
                    viewHolder.tripExpandedInfoLinearLayout.setVisibility(View.GONE);
                    viewHolder.tripCollapseDetailsCardLinearLayout.setVisibility(View.GONE);

                } else {
                    viewHolder.tripExpandDetailsCardLinearLayout.setVisibility(View.GONE);
                    viewHolder.tripExpandedInfoLinearLayout.setVisibility(View.VISIBLE);
                    viewHolder.tripCollapseDetailsCardLinearLayout.setVisibility(View.VISIBLE);

                    if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                        viewHolder.tripEnergyConsumedLinearLayout.setVisibility(View.GONE);
                        viewHolder.tripModeLinearLayout.setVisibility(View.GONE);
                        viewHolder.tripModeTextView.setVisibility(View.GONE);
                    } else {
                        viewHolder.tripEnergyConsumedLinearLayout.setVisibility(View.VISIBLE);
                        viewHolder.tripModeLinearLayout.setVisibility(View.VISIBLE);
                        viewHolder.tripModeTextView.setVisibility(View.VISIBLE);
                        viewHolder.pieChartForModeUsage.animateXY(1000, 1000);
                        viewHolder.pieChartForModeUsage.invalidate();
                    }
                }
            }
        });

        viewHolder.tripExpandDetailsCardImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.tripExpandDetailsCardLinearLayout.setVisibility(View.GONE);
                viewHolder.tripExpandedInfoLinearLayout.setVisibility(View.VISIBLE);
                viewHolder.tripCollapseDetailsCardLinearLayout.setVisibility(View.VISIBLE);

                if(vehicle != null && vehicle.getModel().getProtocol() != null && vehicle.getModel().getProtocol() == ModelProtocol.PNP) {
                    viewHolder.tripEnergyConsumedLinearLayout.setVisibility(View.GONE);
                    viewHolder.tripModeLinearLayout.setVisibility(View.GONE);
                    viewHolder.tripModeTextView.setVisibility(View.GONE);

                } else {
                    viewHolder.tripEnergyConsumedLinearLayout.setVisibility(View.VISIBLE);
                    viewHolder.tripModeLinearLayout.setVisibility(View.VISIBLE);
                    viewHolder.tripModeTextView.setVisibility(View.VISIBLE);
                    viewHolder.pieChartForModeUsage.animateXY(1000, 1000);
                    viewHolder.pieChartForModeUsage.invalidate();
                }
            }
        });

        viewHolder.tripCollapseDetailsCardLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.tripExpandDetailsCardLinearLayout.setVisibility(View.VISIBLE);
                viewHolder.tripExpandedInfoLinearLayout.setVisibility(View.GONE);
                viewHolder.tripCollapseDetailsCardLinearLayout.setVisibility(View.GONE);
            }
        });

        viewHolder.playImageView.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, TripPlayBackActivity.class);
            intent.putExtra(ARG_VIN, PrefUtils.getInstance(mContext).getVehicleObjectFromPrefs().getVin());
            intent.putExtra(ARG_TRIP_ID, tripItem.getTripId());
            intent.putExtra(ARG_START_TIME, tripItem.getStartTime());
            intent.putExtra(ARG_END_TIME, tripItem.getEndTime());
            mContext.startActivity(intent);
        });
    }

    private void updateMap(GoogleMap googleMap, ArrayList<LatLng> latLngArrayList) {
        Marker startingPoint, endPoint;

        int latLngArraySize = latLngArrayList.size();

        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);


        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        PolylineOptions polylineOptions = new PolylineOptions().clickable(false);
        polylineOptions.addAll(latLngArrayList);
        polylineOptions.width(6.0f);
        polylineOptions.color(ContextCompat.getColor(mContext, R.color.revos_green_dark));
        googleMap.addPolyline(polylineOptions);

        endPoint = googleMap.addMarker(new MarkerOptions()
                .position(latLngArrayList.get(0))
                .anchor(0.33f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_flag_end, convertDipToPixels(28), convertDipToPixels(28)))));

        startingPoint = googleMap.addMarker(new MarkerOptions()
                .position(latLngArrayList.get(latLngArraySize-1))
                .anchor(0.33f, 1)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.ic_flag_start, convertDipToPixels(28), convertDipToPixels(28)))));

        //LatLng builder
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int index = 0 ; index < latLngArrayList.size() ; index++) {
            builder.include(latLngArrayList.get(index));
        }
        LatLngBounds bounds = builder.build();

        int padding = 80; // offset from edges of the map in pixels
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);


        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                googleMap.animateCamera(cameraUpdate);
            }
        });

    }

    private Bitmap resizeMapIcons(int resourceId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(mContext.getResources(), resourceId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private int convertDipToPixels(float dips) {
        return (int) (dips * mContext.getResources().getDisplayMetrics().density + 0.5f);
    }

    @Override
    public int getItemCount() {
        //return mTripInformationList.size();
        return mTripInformationList == null ? 0 : mTripInformationList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mTripInformationList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder viewHolder) {
        super.onViewRecycled(viewHolder);

        if(viewHolder instanceof ItemViewHolder) {

            if(((ItemViewHolder) viewHolder).mGoogleMap != null) {
                ((ItemViewHolder) viewHolder).mGoogleMap.clear();
            }

            ((ItemViewHolder) viewHolder).tripExpandDetailsCardLinearLayout.setVisibility(View.VISIBLE);
            ((ItemViewHolder) viewHolder).tripExpandedInfoLinearLayout.setVisibility(View.GONE);
            ((ItemViewHolder) viewHolder).tripCollapseDetailsCardLinearLayout.setVisibility(View.GONE);
        }
    }

    public void updateTripList(ArrayList<TripDataFeed> updatedTripList) {
        mTripInformationList = updatedTripList;
        notifyDataSetChanged();
    }
}
