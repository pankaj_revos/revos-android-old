package com.c;

/**
 * Created by ChenShouYin on 2018\7\4 0004.
 */

public class NativeController {
    // Used to load the 'csy-native-lib' library on application startup.
    static {
        System.loadLibrary("csy-native-lib");
    }

    /**
     * tes加密
     * 返回数组
     * @param cmd
     * @return
     */
    public static native String encode8String(byte[] cmd);


}
