package com.android.internal.telephony;

/**
 * Created by moyadav on 5/25/2016.
 */
public interface ITelephony {
    boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}
